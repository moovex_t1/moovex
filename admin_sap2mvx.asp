<!--#include file="include.asp"-->
<%'serverfunctions
  rr("<table class=form_table cellspacing=5>")
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update>")
  rr("<input type=hidden name=id value=" & request_id & "><input type=hidden name=token value=" & session("token") & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><!img src='icons/communication.png' ~iconsize~> ~sap2mvx_sap2mvx~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------

  rr("<tr valign=top id=date1_tr0><td class=form_item1 id=date1_tr1><span id=date1_caption>~sap2mvx_date1~</span><td class=form_item2 id=date1_tr2>")
  rr(SelectDate("f1.date1:", thedate1))

  rr("<tr valign=top id=ordertypeid_tr0><td class=form_item1 id=ordertypeid_tr1><span id=ordertypeid_caption>~sap2mvx_ordertypeid~</span><td class=form_item2 id=ordertypeid_tr2>")
  rr("<input type=text maxlength=0 name=ordertypeid onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theordertypeid & """ dir=~langdir~>")

  rr("<tr valign=top id=status_tr0><td class=form_item1 id=status_tr1><span id=status_caption>~sap2mvx_status~</span><td class=form_item2 id=status_tr2>")
  rr("<input type=text maxlength=50 name=status onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thestatus & """ dir=~langdir~>")

  rr("<tr valign=top id=direction_tr0><td class=form_item1 id=direction_tr1><span id=direction_caption>~sap2mvx_direction~</span><td class=form_item2 id=direction_tr2>")
  rr("<input type=text maxlength=10 name=direction onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedirection & """ dir=~langdir~>")

  rr("<tr valign=top id=from1_tr0><td class=form_item1 id=from1_tr1><span id=from1_caption>~sap2mvx_from1~</span><td class=form_item2 id=from1_tr2>")
  rr("<input type=text maxlength=0 name=from1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thefrom1 & """ dir=~langdir~>")

  rr("<tr valign=top id=to1_tr0><td class=form_item1 id=to1_tr1><span id=to1_caption>~sap2mvx_to1~</span><td class=form_item2 id=to1_tr2>")
  rr("<input type=text maxlength=50 name=to1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theto1 & """ dir=~langdir~>")

  rr("<tr valign=top id=dealnumber_tr0><td class=form_item1 id=dealnumber_tr1><span id=dealnumber_caption>~sap2mvx_dealnumber~</span><td class=form_item2 id=dealnumber_tr2>")
  rr("<input type=text maxlength=50 name=dealnumber onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedealnumber & """ dir=~langdir~>")

  rr("<tr valign=top id=dealitem_tr0><td class=form_item1 id=dealitem_tr1><span id=dealitem_caption>~sap2mvx_dealitem~</span><td class=form_item2 id=dealitem_tr2>")
  rr("<input type=text maxlength=50 name=dealitem onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedealitem & """ dir=~langdir~>")

  rr("<tr valign=top id=costcenter_tr0><td class=form_item1 id=costcenter_tr1><span id=costcenter_caption>~sap2mvx_costcenter~</span><td class=form_item2 id=costcenter_tr2>")
  rr("<input type=text maxlength=50 name=costcenter onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thecostcenter & """ dir=~langdir~>")

  rr("<tr valign=top id=sapuser_tr0><td class=form_item1 id=sapuser_tr1><span id=sapuser_caption>~sap2mvx_sapuser~</span><td class=form_item2 id=sapuser_tr2>")
  rr("<input type=text maxlength=0 name=sapuser onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thesapuser & """ dir=~langdir~>")

  rr("<tr valign=top id=pids_tr0><td class=form_item1 id=pids_tr1><span id=pids_caption>~sap2mvx_pids~</span><td class=form_item2 id=pids_tr2>")
  rr("<textarea name=pids style='width:301; height:100;' dir=~langdir~>" & thepids & "</textarea>")
  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  rr(checkformadd)

  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  rr("<script language=vbscript>on error resume next : f1.ordertypeid.focus : on error goto 0</script>")

%>
