<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 09/01/2013 15:56:29

function budesigndelitem(i)
  budesigndelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function budesigninsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("lable") <> "" then t = getfield("lable")
  f = f & "lable," : v = v & "'" & t & "',"

  t = "" : if getfield("fieldname") <> "" then t = getfield("fieldname")
  f = f & "fieldname," : v = v & "'" & t & "',"

  t = "" : if getfield("description") <> "" then t = getfield("description")
  f = f & "description," : v = v & "'" & t & "',"

  t = "" : if getfield("value1") <> "" then t = getfield("value1")
  f = f & "value1," : v = v & "'" & t & "',"

  t = "" : if getfield("file1") <> "" then t = getfield("file1")
  f = f & "file1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into budesign(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from budesign"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  budesigninsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table budesign (id autoincrement primary key,lable text(50),fieldname text(50),description memo,value1 text(50),file1 text(100))"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "budesign" : session("tablename") = "budesign"
tablefields = "id,lable,fieldname,description,value1,file1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 0
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = budesigninsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("lable") then s = s & "lable='" & getfield("lable") & "',"
  if okedit("fieldname") then s = s & "fieldname='" & getfield("fieldname") & "',"
  if okedit("description") then s = s & "description='" & getfield("description") & "',"
  if okedit("value1") then s = s & "value1='" & getfield("value1") & "',"
  if okedit("file1") then s = s & "file1='" & getfield("file1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = budesigndelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if



'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","budesign.id,budesign.lable,budesign.fieldname,budesign.description,budesign.value1,budesign.file1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thelable = rs("lable") : if isnull(Thelable) then Thelable = ""
  Thefieldname = rs("fieldname") : if isnull(Thefieldname) then Thefieldname = ""
  Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
  Thedescription = replace(thedescription,"<br>",vbcrlf)
  Thevalue1 = rs("value1") : if isnull(Thevalue1) then Thevalue1 = ""
  Thefile1 = rs("file1") : if isnull(Thefile1) then Thefile1 = ""
  formcontrols = "lable,fieldname,description,value1,file1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/drawings.png' ~iconsize~> ~budesign_budesign~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("lable") then'form:lable
    rr("<tr valign=top id=lable_tr0><td class=form_item1 id=lable_tr1><span id=lable_caption>~budesign_lable~<img src=must.gif></span><td class=form_item2 id=lable_tr2>")
    rr("<input type=text maxlength=50 name=lable onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thelable & """ dir=~langdir~>")
  end if'form:lable

  if oksee("fieldname") then'form:fieldname
    rr("<tr valign=top id=fieldname_tr0><td class=form_item1 id=fieldname_tr1><span id=fieldname_caption>~budesign_fieldname~<img src=must.gif></span><td class=form_item2 id=fieldname_tr2>")
    rr("<input type=text maxlength=50 name=fieldname onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thefieldname & """ dir=ltr>")
  end if'form:fieldname
  if oksee("fieldname") then rr(" <img src=icons/i_help.gif style='cursor:hand;' alt='~budesign_fieldname_help~'>")

  if oksee("description") then'form:description
    rr("<tr valign=top id=description_tr0><td class=form_item1 id=description_tr1><span id=description_caption>~budesign_description~</span><td class=form_item2 id=description_tr2>")
    rr("<textarea name=description style='width:301; height:100;' dir=~langdir~>" & thedescription & "</textarea>")
  end if'form:description

  if oksee("value1") then'form:value1
    rr("<tr valign=top id=value1_tr0><td class=form_item1 id=value1_tr1><span id=value1_caption>~budesign_value1~<img src=must.gif></span><td class=form_item2 id=value1_tr2>")
    rr("<input type=text maxlength=50 name=value1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thevalue1 & """ dir=ltr>")
  end if'form:value1

  if oksee("file1") then'form:file1
    rr("<tr valign=top id=file1_tr0><td class=form_item1 id=file1_tr1><span id=file1_caption>~budesign_file1~</span><td class=form_item2 id=file1_tr2>")
    rr("<table border=0 cellpadding=0 cellspacing=0 align=~langside2~><tr><td><div id=file1_preview>")
    if lcase(right(thefile1,4)) = ".jpg" or lcase(right(thefile1,4)) = ".gif" or lcase(right(thefile1,4)) = ".png" then
      rr("<a href='" & thefile1 & "' target=_blank><img src='" & thefile1 & "' width=200 border=0></a>")
    else
      rr("<a href='" & thefile1 & "' target=_blank><font style='font-size:18;'><b>" & ucase(mid(thefile1,instrrev(thefile1,".")+1)) & "</a>")
    end if
    rr("</div></table>")
    if okwritehere then rr("<nobr><table align=~langside~><tr><td><iframe id=file1_upload frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=file1&previewdiv=file1_preview></iframe></table>")
    rr("<input type=text maxlength=100 id=file1 name=file1 style='width:200;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' value=""" & Thefile1 & """ dir=ltr><br>")
  end if'form:file1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn style='width:80;' value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave style='width:80;' value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete style='width:80;' value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback style='width:80;' value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("lable") then rr("    if f1.lable.value <> """" then validfield(""lable"") else invalidfield(""lable"") : ok = false" & vbcrlf)
  if okedit("fieldname") then rr("    if f1.fieldname.value <> """" then validfield(""fieldname"") else invalidfield(""fieldname"") : ok = false" & vbcrlf)
  if okedit("value1") then rr("    if f1.value1.value <> """" then validfield(""value1"") else invalidfield(""value1"") : ok = false" & vbcrlf)
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.lable.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".lable" : session(tablename & "_hidelistcolumns") = "fieldname,description,file1,"
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".lable"
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=1 cellpadding=5 align=~langside~>" & listtopbar)

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/drawings.png' ~iconsize~> ~budesign_budesign~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd style='width:80;' value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)

  '-------------sql------------------------------
  session(tablename & "_lines") = 1000000
  s = "select " & tablename & ".* from (" & tablename & ")"

  '-sqladd
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","budesign.id,budesign.lable,budesign.fieldname,budesign.description,budesign.value1,budesign.file1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("lable") then f = "budesign.lable" : f2 = "lable" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("lable") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~budesign_lable~</a> " & a2)
  if oksee("fieldname") then f = "budesign.fieldname" : f2 = "fieldname" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("fieldname") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~budesign_fieldname~</a> " & a2)
  if oksee("description") then rr("<td class=list_title><nobr>~budesign_description~")
  if oksee("value1") then f = "budesign.value1" : f2 = "value1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("value1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~budesign_value1~</a> " & a2)
  if oksee("file1") then f = "budesign.file1" : f2 = "file1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("file1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~budesign_file1~</a> " & a2)

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thelable = rs("lable") : if isnull(Thelable) then Thelable = ""
      Thefieldname = rs("fieldname") : if isnull(Thefieldname) then Thefieldname = ""
      Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
      Thevalue1 = rs("value1") : if isnull(Thevalue1) then Thevalue1 = ""
      Thefile1 = rs("file1") : if isnull(Thefile1) then Thefile1 = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      firstcoltitle = thelable
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/drawings.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("fieldname") then rr("<td class=list_item valign=top align=><nobr>" & thefieldname)
      thedescription = thedescription : if isnull(thedescription) then thedescription = ""
      thedescription = replace(thedescription,"<br>"," ") : thedescription = replace(thedescription,"<","[") : thedescription = replace(thedescription,">","]") : if len(thedescription) > 30 then thedescription = left(thedescription,30) & "..."
      if oksee("description") then rr("<td class=list_item valign=top align=><nobr>" & thedescription)
      if oksee("value1") then rr("<td class=list_item valign=top align=><nobr>" & thevalue1)
      if oksee("file1") then
        rr("<td class=list_item valign=top align=><nobr>")
        p = thefile1 : p1 = p : if instr(p1,"/") > 0 then p1 = mid(p1,instr(p1,"/")+1)
        rr("<a href='" & thefile1 & "' target=_blank>")
        if lcase(right(p,4)) = ".jpg" or lcase(right(p,4)) = ".gif" or lcase(right(p,4)) = ".png" then rr("<img src=""" & p & """ style='width:100; border:1px solid #000000;'></a>") else rr(p1 & "</a>")
      end if
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("fieldname") and session(tablename & "containerfield") <> "fieldname" then rr("<td class=list_total1>") 'fieldname
  if oksee("description") and session(tablename & "containerfield") <> "description" then rr("<td class=list_total1>") 'description
  if oksee("value1") and session(tablename & "containerfield") <> "value1" then rr("<td class=list_total1>") 'value1
  if oksee("file1") and session(tablename & "containerfield") <> "file1" then rr("<td class=list_total1>") 'file1

  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  rr("~All Results~ (" & rsrc & ")")
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
