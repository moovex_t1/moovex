<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 16/04/2015 20:27:47

function ordertypesdelitem(i)
  ordertypesdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function ordertypesinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "on" : if getfield("active") <> "" then t = getfield("active")
  f = f & "active," : v = v & "'" & t & "',"

  t = "" : if getfield("hidefields") <> "" then t = getfield("hidefields")
  f = f & "hidefields," : v = v & "'" & t & "',"

  t = "" : if getfield("internalfields") <> "" then t = getfield("internalfields")
  f = f & "internalfields," : v = v & "'" & t & "',"

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  t = "" : if getfield("data2") <> "" then t = getfield("data2")
  f = f & "data2," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
  if getfield("siteid") <> "" then t = getfield("siteid")
  f = f & "siteid," : v = v & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into ordertypes(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from ordertypes"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  ordertypesinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table ordertypes (id autoincrement primary key,title text(50),active text(2),hidefields memo,internalfields memo,data1 memo,data2 memo,siteid number)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "ordertypes" : session("tablename") = "ordertypes"
tablefields = "id,title,active,hidefields,internalfields,data1,data2,siteid,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 1
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
'locked = "1," '''production set this order type
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = ordertypesinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("active") then s = s & "active='" & getfield("active") & "',"
  if okedit("hidefields") then s = s & "hidefields='" & getfield("hidefields") & "',"
  if okedit("internalfields") then s = s & "internalfields='" & getfield("internalfields") & "',"
  if okedit("data1") then s = s & "data1='" & getformdata("data1") & "',"
'[  if okedit("data2") then s = s & "data2='" & getfield("data2") & "',"
  sq = "select data2 from " & tablename & " where id = " & request_id
  set rs1 = conn.execute(sqlfilter(sq))
  data2 = "" & rs1("data2")
  data2 = setval(data2, "courseids", getfield("courseids"))
  data2 = setval(data2, "direction", getfield("direction"))
  data2 = setval(data2, "courseid", getfield("courseid"))
  data2 = setval(data2, "mustpids", getfield("mustpids"))
  data2 = setval(data2, "autovalidate", getfield("autovalidate"))
  data2 = setval(data2, "costtype", getfield("costtype"))
  if okedit("data2") then s = s & "data2='" & data2 & "',"
']
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(title,active,hidefields,internalfields,data1,data2,siteid)"
  s = s & " SELECT title,active,hidefields,internalfields,data1,data2,siteid"
  s = s & " FROM " & tablename & " where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  request_action = "form"
  request_id = theid
  msg = msg & " ~Record duplicated~"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = ordertypesdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = ordertypesdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~ordertypes_title~^"
  if oksee("active") then csv(0) = csv(0) & "~ordertypes_active~^"
  if oksee("hidefields") then csv(0) = csv(0) & "~ordertypes_hidefields~^"
  if oksee("internalfields") then csv(0) = csv(0) & "~ordertypes_internalfields~^"
'[  if oksee("data1") then csv(0) = csv(0) & "^"
  if oksee("data1") then csv(0) = csv(0) & "data1^"
']
  if oksee("data2") then csv(0) = csv(0) & "~ordertypes_data2~^"
  if oksee("siteid") then csv(0) = csv(0) & "~ordertypes_siteid~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thehidefields = rs("hidefields") : if isnull(Thehidefields) then Thehidefields = ""
  Theinternalfields = rs("internalfields") : if isnull(Theinternalfields) then Theinternalfields = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thedata2 = rs("data2") : if isnull(Thedata2) then Thedata2 = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("active") then csv(rc) = csv(rc) & replace(theactive,vbcrlf," ") & "^"
    if oksee("hidefields") then csv(rc) = csv(rc) & replace(thehidefields,vbcrlf," ") & "^"
    if oksee("internalfields") then csv(rc) = csv(rc) & replace(theinternalfields,vbcrlf," ") & "^"
    t = thedata1 : t = ctext(t) : t = replace(t,"^<br>", "^") : t = replace(t,vbcrlf," ")
    if oksee("data1") then csv(rc) = csv(rc) & t & "^"
    if oksee("data2") then csv(rc) = csv(rc) & replace(thedata2,vbcrlf," ") & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
      s = "" : thisid = "0"
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
'[        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
'[          thisid = b
'[        elseif csvff(i) = "title" then
'[          if len(b) > 50 then b = left(b,50)
'[          if okedit("title") then s = s & "title='" & b & "',"
        if csvff(i) = "title" then
          if len(b) > 50 then b = left(b,50)
          if okedit("title") then s = s & "title='" & b & "',"
          x = getonefield("select id from " & tablename & " where lcase(title) = '" & lcase(b) & "'")
          if x <> "" then thisid = x
']
        elseif csvff(i) = "active" then
          if len(b) > 2 then b = left(b,2)
          if okedit("active") then s = s & "active='" & b & "',"
        elseif csvff(i) = "hidefields" then
          if okedit("hidefields") then s = s & "hidefields='" & b & "',"
        elseif csvff(i) = "internalfields" then
          if okedit("internalfields") then s = s & "internalfields='" & b & "',"
'[        elseif csvff(i) = "data1" then
        elseif csvff(i) = "data1" then
          if okedit("data1") then s = s & "data1='" & b & "',"
']
        elseif csvff(i) = "data2" then
          if okedit("data2") then s = s & "data2='" & b & "',"
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("siteid") then s = s & "siteid=" & v & ","
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
'[      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
']
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = ordertypesinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","ordertypes.id,ordertypes.title,ordertypes.active,ordertypes.hidefields,ordertypes.internalfields,ordertypes.data1,ordertypes.data2,ordertypes.siteid",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thehidefields = rs("hidefields") : if isnull(Thehidefields) then Thehidefields = ""
  Theinternalfields = rs("internalfields") : if isnull(Theinternalfields) then Theinternalfields = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thedata2 = rs("data2") : if isnull(Thedata2) then Thedata2 = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  formcontrols = "title,active,hidefields,internalfields,data1,data2,siteid,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/order-1.png' ~iconsize~> ~ordertypes_ordertypes~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~ordertypes_title~</span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  end if'form:title

  if oksee("active") then'form:active
    rr(" <span id=active_caption class=form_item3>~ordertypes_active~</span> ")
    rr("<input type=checkbox name=active")
    if theactive = "on" then rr(" checked")
    rr(">")
  end if'form:active

  if oksee("hidefields") then'form:hidefields
'[    rr("<tr valign=top id=hidefields_tr0><td class=form_item1 id=hidefields_tr1><span id=hidefields_caption>~ordertypes_hidefields~</span><td class=form_item2 id=hidefields_tr2>")
'[    c = 0 : s = "" : a = "xxx,"
'[    cc = countstring(a,",")
'[    do until a = ""
'[      c = c + 1
'[      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
'[      rr("<input type=checkbox name=hidefields_c" & c)
'[      if instr("," & thehidefields & ",","," & b & ",") > 0 then rr(" checked")
'[      rr(" onclick='vbscript:populatehidefields'>")
'[      rr(b & " ")
'[      s = s & "if f1.hidefields_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
'[    loop
'[    rr("<script language=vbscript>" & vbcrlf)
'[    rr("  sub populatehidefields" & vbcrlf)
'[    rr("    t = """"" & vbcrlf)
'[    rr(s & vbcrlf)
'[    rr("    f1.hidefields.value = t" & vbcrlf)
'[    rr("  end sub" & vbcrlf)
'[    rr("</script>" & vbcrlf)
'[    rr("<input type=hidden name=hidefields value=""" & Thehidefields & """>")
    rr "<tr valign=top id=hidefields_tr0><td class=form_item1 id=hidefields_tr1><span id=hidefields_caption>~ordertypes_hidefields~</span>"
    rr " <img src=icons/i_help.gif alt='~Will not appear in order form~'>"
    rr "<td class=form_item2 id=hidefields_tr2>"

    c = 0 : s = ""
    a = ""
    if specs = "tnuva" then a = a & "alternativeorderworkerstations,"
    a = a & "reasonid,courseid,from1,to1,via1,direction,price,production,additionids,pids,supplierid,carid,driverid,km,remarks,updateduserid,updateddate,userid,siteid,"
    a = a & "buttonsavereturn,buttonduplicate,buttondelete,buttonprint,buttonmakereturn,buttonsend,buttonapprove,buttonreject,buttoncancelorder,buttonmove,buttonmovepaste,buttonupdatemypassengers,buttonsupplierupdate,"
    rr "<div style='background:#ffffff; border:1px solid #aaaaaa; width:300; height:136; overflow:auto;'>"
    cc = countstring(a,",")
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
      if left(b,6) = "button" and wrotebuttonstitle = "" then wrotebuttonstitle = "on" : rr "<br><div style='background:eeeeee; padding:3; text-align:center;'><b>Buttons</b></div><br>"
      rr("<input type=checkbox name=hidefields_c" & c)
      if instr("," & thehidefields & ",","," & b & ",") > 0 then rr(" checked")
      rr(" onclick='vbscript:populatehidefields'>")
      if left(b,6) = "button" then
        rr(mid(b,7) & "<br>")
      else
        rr("~orders_" & b & "~<br>")
      end if
      s = s & "if f1.hidefields_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
    loop
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub populatehidefields" & vbcrlf)
    rr("    t = """"" & vbcrlf)
    rr(s & vbcrlf)
    rr("    f1.hidefields.value = t" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("</script>" & vbcrlf)
    rr("<input type=hidden name=hidefields value=""" & Thehidefields & """>")
    rr "</div>"
']
  end if'form:hidefields

'[  if oksee("internalfields") then'form:internalfields
'[    rr("<tr valign=top id=internalfields_tr0><td class=form_item1 id=internalfields_tr1><span id=internalfields_caption>~ordertypes_internalfields~</span><td class=form_item2 id=internalfields_tr2>")
'[    c = 0 : s = "" : a = "iii,"
'[    cc = countstring(a,",")
'[    do until a = ""
'[      c = c + 1
'[      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
'[      rr("<input type=checkbox name=internalfields_c" & c)
'[      if instr("," & theinternalfields & ",","," & b & ",") > 0 then rr(" checked")
'[      rr(" onclick='vbscript:populateinternalfields'>")
'[      rr(b & " ")
'[      s = s & "if f1.internalfields_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
'[    loop
'[    rr("<script language=vbscript>" & vbcrlf)
'[    rr("  sub populateinternalfields" & vbcrlf)
'[    rr("    t = """"" & vbcrlf)
'[    rr(s & vbcrlf)
'[    rr("    f1.internalfields.value = t" & vbcrlf)
'[    rr("  end sub" & vbcrlf)
'[    rr("</script>" & vbcrlf)
'[    rr("<input type=hidden name=internalfields value=""" & Theinternalfields & """>")
'[  end if'form:internalfields

  if oksee("internalfields") then'form:internalfields
    rr "<tr valign=top id=internalfields_tr0><td class=form_item1 id=internalfields_tr1><span id=internalfields_caption>~ordertypes_internalfields~</span>"
    rr " <img src=icons/i_help.gif alt='~Will not be sent to supplier`s system~'>"
    rr "<td class=form_item2 id=internalfields_tr2>"
    c = 0 : s = "" : a = ""
    ff = thedata1 : if ff <> "" and right(ff,1) <> "|" then ff = ff & "|"
    do until ff = ""
      f = left(ff,instr(ff,"|")-1) : ff = mid(ff,instr(ff,"|")+1) : if instr(f,"=") = 0 then exit do
      fdef = left(f,instr(f,"=")-1)
      ftype = lcase(left(f,1)) : f = mid(f,2)
      fname = trim(left(f,instr(f,"=")-1))
      fname = replace(fname,"'","`") : fname = replace(fname,"""","``") : fname = replace(fname," ","_")
      fname = replace(fname,";","")  : fname = replace(fname,"*","")
      fdisplay = replace(fname,"_"," ") : if instr(fdisplay, ",") > 0 then fdisplay = left(fdisplay, instr(fdisplay,",")-1)
      fdisplay = replace(fdisplay, "*", "<span style='color:#ff0000;'>*</span>")
      a = a & encrypt(fdef,"1") & "^" & fdisplay & ","
    loop

    rr "<div style='background:#ffffff; border:1px solid #aaaaaa; width:300; height:136; overflow:auto;'>"
    cc = countstring(a,",")
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
      rr("<input type=checkbox name=internalfields_c" & c)
      v = left(b, instr(b,"^") - 1) : d = mid(b, instr(b,"^") + 1)
      if instr("," & theinternalfields & ",","," & v & ",") > 0 then rr(" checked")
      rr(" onclick='vbscript:populateinternalfields'>")
      rr(d & "<br>")
      s = s & "if f1.internalfields_c" & c & ".checked then t = t & """ & v & ",""" & vbcrlf
    loop
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub populateinternalfields" & vbcrlf)
    rr("    t = """"" & vbcrlf)
    rr(s & vbcrlf)
    rr("    f1.internalfields.value = t" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("</script>" & vbcrlf)
    rr("<input type=hidden name=internalfields value=""" & Theinternalfields & """>")
    rr "</div>"
  end if'form:internalfields

  if specs = "tnuva" then
    thecourseids = getval(thedata2, "courseids")
    rr("<tr valign=top><td class=form_item1>������� ������<td class=form_item2>")
    't = ":select id,code,title from courses where id in(" & thecourseids & "0) or active = 'on'"
    'if thesiteid <> "0" then t = t & " and siteid = " & thesiteid
    't = t & " order by code,title"
    rr(selectlookupmultiauto("f1.courseids",thecourseids, "icons/upcoming-work.png", "courses,code,title,", 300, okwritehere))
  end if

  thedirection = getval(thedata2, "direction")
  rr("<tr valign=top><td class=form_item1>~Default~ ~Dircetion~<td class=form_item2>")
  a = ",A,B,E,"
  rr("<select name=direction dir=~langdir~>")
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if Thedirection = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select>")

  thecourseid = getval(thedata2, "courseid")
  rr("<tr valign=top><td class=form_item1>~Default~ ~Course~<td class=form_item2>")
  rr "<select name=courseid dir=~langdir~>"
  rr "<option value=''" : if Thecourseid = "" then rr " selected"
  rr ">"
  rr "<option value='0'" : if Thecourseid = "0" then rr " selected"
  rr ">����� ���� ���"
  rr "<option value='-9'" : if Thecourseid = "-9" then rr " selected"
  rr ">����� �������"
  rr "</select>"

  themustpids = getval(thedata2, "mustpids")
  rr("<tr valign=top><td class=form_item1>~Mandatory Passengers~<td class=form_item2>")
  rr "<input type=checkbox name=mustpids" : if themustpids = "on" then rr " checked"
  rr ">"

  if specs = "rafael" then
    theautovalidate = getval(thedata2, "autovalidate")
    rr("<tr valign=top><td class=form_item1>������� ��������<td class=form_item2>")
    rr "<input type=checkbox name=autovalidate" : if theautovalidate = "on" then rr " checked"
    rr ">"
    'thecosttype = getval(thedata2, "costtype")
    'rr("<tr valign=top><td class=form_item1>��� �����<td class=form_item2>")
    'rr "<input type=text name=costtype value='" & thecosttype & "'>"
  end if

']

  if oksee("data1") then'form:data1
'[    rr("<tr valign=top id=data1_tr0><td class=form_item1 id=data1_tr1><span id=data1_caption>~ordertypes_data1~</span><td class=form_item2 id=data1_tr2>")
'[    rr(formdata("data1-+",ctxt(thedata1)))

    rr("<tr valign=top id=data1_tr0><td class=form_item1 id=data1_tr1><span id=data1_caption>~ordertypes_data1~</span><td class=form_item2 id=data1_tr2 bgcolor=#cccccc>")
    rr(formdata("data1-+",ctxt(thedata1)))
']
  end if'form:data1

  if oksee("data2") then'form:data2
    rr("<span id=data2_caption></span> ")
    rr("<input type=hidden name=data2 value=""" & Thedata2 & """>")
  end if'form:data2

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~ordertypes_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~ordertypes_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonduplicate value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)

  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = "hidefields,internalfields,data1,data2,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=left>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/order-1.png' ~iconsize~> ~ordertypes_ordertypes~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~ordertypes_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
  end if'search:siteid
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","ordertypes.id,ordertypes.title,ordertypes.active,ordertypes.hidefields,ordertypes.internalfields,ordertypes.data1,ordertypes.data2,ordertypes.siteid",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".hidefields like '%" & w & "%' or"
        s = s & " " & tablename & ".internalfields like '%" & w & "%' or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
        s = s & " " & tablename & ".data2 like '%" & w & "%' or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("title") then f = "ordertypes.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~ordertypes_title~</a> " & a2)
  if oksee("active") then f = "ordertypes.active" : f2 = "active" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("active") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~ordertypes_active~</a> " & a2)
  if oksee("hidefields") then rr("<td class=list_title><nobr>~ordertypes_hidefields~")
  if oksee("internalfields") then rr("<td class=list_title><nobr>~ordertypes_internalfields~")
  if oksee("data1") then rr("<td class=list_title><nobr>~ordertypes_data1~")
  if oksee("data2") then rr("<td class=list_title><nobr>~ordertypes_data2~")
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~ordertypes_siteid~</a> " & a2)
  end if

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
      Thehidefields = rs("hidefields") : if isnull(Thehidefields) then Thehidefields = ""
      Theinternalfields = rs("internalfields") : if isnull(Theinternalfields) then Theinternalfields = ""
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thedata2 = rs("data2") : if isnull(Thedata2) then Thedata2 = ""
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/order-1.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("active") then rr("<td class=list_item valign=top align=><nobr>") : if theactive = "on" then rr("v")
'[      thehidefields = thehidefields : if isnull(thehidefields) then thehidefields = ""
'[      thehidefields = replace(thehidefields,"<br>"," ") : thehidefields = replace(thehidefields,"<","[") : thehidefields = replace(thehidefields,">","]") : if len(thehidefields) > 30 then thehidefields = left(thehidefields,30) & "..."
'[      if oksee("hidefields") then rr("<td class=list_item valign=top align=><nobr>" & thehidefields)
      if oksee("hidefields") then
        rr("<td class=list_item valign=top align=><nobr>")
        a = thehidefields
        do until a = ""
          b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
          if left(b,6) = "button" then
            rr mid(b,7)
          else
            rr "~orders_" & b & "~"
          end if
          if a <> "" then rr ", "
        loop
      end if
']
'[      theinternalfields = theinternalfields : if isnull(theinternalfields) then theinternalfields = ""
'[      theinternalfields = replace(theinternalfields,"<br>"," ") : theinternalfields = replace(theinternalfields,"<","[") : theinternalfields = replace(theinternalfields,">","]") : if len(theinternalfields) > 30 then theinternalfields = left(theinternalfields,30) & "..."
'[      if oksee("internalfields") then rr("<td class=list_item valign=top align=><nobr>" & theinternalfields)
      if oksee("internalfields") then rr("<td class=list_item valign=top align=><nobr>" & countstring(theinternalfields,","))
']
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
      thedata2 = thedata2 : if isnull(thedata2) then thedata2 = ""
      thedata2 = replace(thedata2,"<br>"," ") : thedata2 = replace(thedata2,"<","[") : thedata2 = replace(thedata2,">","]") : if len(thedata2) > 30 then thedata2 = left(thedata2,30) & "..."
      if oksee("data2") then rr("<td class=list_item valign=top align=><nobr>" & thedata2)
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_total1>") 'active
  if oksee("hidefields") and session(tablename & "containerfield") <> "hidefields" then rr("<td class=list_total1>") 'hidefields
  if oksee("internalfields") and session(tablename & "containerfield") <> "internalfields" then rr("<td class=list_total1>") 'internalfields
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1
  if oksee("data2") and session(tablename & "containerfield") <> "data2" then rr("<td class=list_total1>") 'data2
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
