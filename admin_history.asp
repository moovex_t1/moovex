<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 20/02/2017 15:20:03

function historydelitem(i)
  historydelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function historyinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "1/1/1900" : if getfield("date1") <> "" then t = getfield("date1")
  f = f & "date1," : v = v & sqldate(t) & ","

  t = "" : if getfield("item") <> "" then t = getfield("item")
  f = f & "item," : v = v & "'" & t & "',"

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into history(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from history"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  historyinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table history (id autoincrement primary key,date1 datetime,item text(10),data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "history" : session("tablename") = "history"
tablefields = "id,date1,item,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 4
if session("usergroup") = "adminassistant" then ulev = 4
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'['-------------start--------------------------------------
if specs = "shufersal" and session("usergroup") = "supplier" then ulev = 1
if specs = "shufersal" and session("usergroup") = "supplieradmin" then ulev = 1

'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = historyinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("date1") then s = s & "date1=" & sqldate(getfield("date1")) & ","
  if okedit("item") then s = s & "item='" & getfield("item") & "',"
  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = historydelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = historydelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if

'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","history.id,history.date1,history.item,history.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Theitem = rs("item") : if isnull(Theitem) then Theitem = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "date1,date1_day,date1_month,date1_year,date1_hour,date1_insertdate,date1_removedate,item,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><!img src='icons/current-work.png' ~iconsize~> ~history_history~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("date1") then'form:date1
    rr("<tr valign=top id=date1_tr0><td class=form_item1 id=date1_tr1><span id=date1_caption>~history_date1~</span><td class=form_item2 id=date1_tr2>")
    rr(SelectDate("f1.date1:", thedate1))
  end if'form:date1

  if oksee("item") then'form:item
    rr("<tr valign=top id=item_tr0><td class=form_item1 id=item_tr1><span id=item_caption>~history_item~</span><td class=form_item2 id=item_tr2>")
    rr("<input type=text maxlength=10 name=item onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theitem & """ dir=~langdir~>")
  end if'form:item

  if oksee("data1") then'form:data1
    rr("<tr valign=top id=data1_tr0><td class=form_item1 id=data1_tr1><span id=data1_caption>~history_data1~</span><td class=form_item2 id=data1_tr2>")
    rr("<input type=text maxlength=50 name=data1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedata1 & """ dir=~langdir~>")
  end if'form:data1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  rr(checkformadd)

  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  rr("<script language=vbscript>on error resume next : f1.item.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : du = rred(a & "&containerid=0&boxid=" & request_id)
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date1" : session(tablename & "_hidelistcolumns") = ""
'[  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if getfield("action2") = "clear" then session(tablename & "_page") = 0
']

  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date1"
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  '-------------listform-------------
'[  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
'[  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
'[  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  t1 = "" : t2 = ""
']
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><!img src='icons/current-work.png' ~iconsize~> ~history_history~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
'[  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
']
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
'[  '-sqladd
  session(tablename & "containerfield") = ""
  session(tablename & "containerid") = "0"
  if getfield("containerfield") <> "" then session("history_item") = getfield("containerfield")
  hidecontrols = hidecontrols & "item,buttonmultiupdate,"
  if session("userlevel") < "5" then hidecontrols = hidecontrols & "buttonmultidelete,"
  session(tablename & "_order") = "date1 desc"
  if specs = "jerusalem" then session(tablename & "_order") = "date1"
  s = s & " and history.item = '" & session("history_item") & "'"
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","history.id,history.date1,history.item,history.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------
  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
'[  if oksee("date1") then f = "history.date1" : f2 = "date1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("date1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~history_date1~</a> " & a2)
  rr("<td class=list_title><nobr>~history_date1~ <img src=up.gif>")
']
  if oksee("item") then f = "history.item" : f2 = "item" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("item") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~history_item~</a> " & a2)
'[  if oksee("data1") then rr("<td class=list_title><nobr>~history_data1~")
  if left(session("history_item"), 1) = "o" then
    rr "<td class=list_title>~User~"
    rr "<td class=list_title>~Date~"
    rr "<td class=list_title>~Status~"
    rr "<td class=list_title>~Supplier~"
    rr "<td class=list_title>~Course~"
    rr "<td class=list_title>~Direction~"
    rr "<td class=list_title>~From~"
    rr "<td class=list_title>~To ~"
    rr "<td class=list_title>~Passengers~"
    rr "<td class=list_title>~KM~"
    rr "<td class=list_title>~Duration~"
    if hasmatch(session("userperm"), "prices") then rr "<td class=list_title>~Price~"
    if hasmatch(session("userperm"), "prices") and specs = "iec" then rr "<td class=list_title>~Details~"
  end if
  if left(session("history_item"), 1) = "w" then
    rr "<td class=list_title>~User~"
    rr "<td class=list_title>~workers_firstname~"
    rr "<td class=list_title>~workers_lastname~"
    rr "<td class=list_title>~workers_code~"
    rr "<td class=list_title>~workers_email~"
    rr "<td class=list_title>~workers_phone1~"
    rr "<td class=list_title>~workers_eligible~"
    rr "<td class=list_title>~workers_active~"
    rr "<td class=list_title>~workers_specialrides~"
    rr "<td class=list_title>~workers_workertypeid~"
    rr "<td class=list_title>~workers_unitid~"
    rr "<td class=list_title>~workers_address~"
    rr "<td class=list_title>~workers_courseid1~"
    rr "<td class=list_title>~workers_from1~"
    rr "<td class=list_title>~workers_to1~"
    rr "<td class=list_title>~workers_citystationid~"
    rr "<td class=list_title>~workers_color~"
    if specs = "jerusalem" then
      'if session("workersview") = "escort" then
      '  jerusalemhistoryadditionals = nomatch(jerusalemhistoryadditionals, "needescort,��� ��� 1,�� ��� ��� 1,����� ��� ��� 1,��� ��� 2,�� ��� ��� 2,����� ��� ��� 2,")
      'else
      '  jerusalemhistoryadditionals = nomatch(jerusalemhistoryadditionals, "����� �����,�����,")
      'end if
      z = 0 : tt = jerusalemhistoryadditionals
      do until tt = ""
        z = z + 1
        t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
        if t = "needescort" then t = "���� �����"
        rr "<td class=list_title>" & t
      loop
    end if
  end if

  if left(session("history_item"), 1) = "s" then
    rr "<td class=list_title>~User~"
    rr "<td class=list_title>~Title~"
    rr "<td class=list_title>~Code~"
    rr "<td class=list_title>~Active~"
    rr "<td class=list_title>~Site~"
    if specs = "jerusalem" then
      z = 0 : tt = jerusalemcourseshistoryadditionals
      do until tt = ""
        z = z + 1
        t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
        if t = "supplierid" then t = "���� ����"
        if t = "supplierid2" then t = "��� ����"
        if t = "unitids" then t = "������"
        if t = "ways" then t = "�����"
        if t = "hourd" then t = "��� �����"
        if t = "houra" then t = "��� ����"
        if t = "hourdur" then t = "���"
        if t = "dh1" then t = "��� ��� �"
        if t = "dh2" then t = "��� ��� �"
        if t = "dh3" then t = "��� ��� �"
        if t = "dh4" then t = "��� ��� �"
        if t = "dh5" then t = "��� ��� �"
        if t = "dh6" then t = "��� ��� �"
        if t = "dh7" then t = "��� ��� �"
        if t = "drivername" then t = "���"
        if t = "driverphone" then t = "����� ���"
        if t = "driverid" then t = "����� �����"
        if t = "neighborhood1" then t = "����� ����"
        if t = "neighborhood2" then t = "����� ���"
        if t = "pricing" then t = "����� ��"
        rr "<td class=list_title>" & t
      loop
    end if
  end if

  if left(session("history_item"), 1) = "c" then
    rr "<td class=list_title>~User~"
    rr "<td class=list_title>~complains_date1~"
    rr "<td class=list_title>~complains_status~"
    rr "<td class=list_title>~complains_userid~"
    rr "<td class=list_title>~complains_orderid~"
    rr "<td class=list_title>~complains_ordersupplierid~"
    rr "<td class=list_title>~complains_orderdriverid~"
    rr "<td class=list_title>~complains_title~"
    rr "<td class=list_title>~complains_description~"
    rr "<td class=list_title>~complains_supplierremarks~"
    rr "<td class=list_title>~complains_solution~"
  end if

  if left(session("history_item"), 1) = "r" then
    rr "<td class=list_title>~User~"
    rr "<td class=list_title>~courserates_courseid~"
    rr "<td class=list_title>~courserates_from1~"
    rr "<td class=list_title>~courserates_to1~"
    rr "<td class=list_title>~courserates_areaid1~"
    rr "<td class=list_title>~courserates_areaid2~"
    rr "<td class=list_title>~courserates_price~"
    rr "<td class=list_title>~courserates_activeproduction~"
    rr "<td class=list_title>~courserates_active~"
    rr "<td class=list_title>~courserates_special~"
    rr "<td class=list_title>~courserates_duration~"
    rr "<td class=list_title>~courserates_supplierid~"
    rr "<td class=list_title>~courserates_passengers~"
    rr "<td class=list_title>~courserates_addnight~"
    rr "<td class=list_title>~courserates_addweekend~"
    rr "<td class=list_title>~courserates_returndiscount~"
    rr "<td class=list_title>~courserates_siteid~"
    rr "<td class=list_title>~Extra station addition~"'entrance
    rr "<td class=list_title>~Extra area addition~"'entrancearea
    rr "<td class=list_title>~Extra passenger addition~"'stop
    rr "<td class=list_title>~Rush hour addition~"'addtoll
    rr "<td class=list_title>~Collect addition~"'addcollect
    rr "<td class=list_title>~Distribute addition~"'adddistribute
    if specs = "rail" then
      rr "<td class=list_title>�������"'category
      rr "<td class=list_title>����� ���� ���"'addstop
      rr "<td class=list_title>���� ��� ���"'weekdayreduction
    end if
    rr "<td class=list_title>������ ������"
  end if

']
  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
      Thedate1 = replace(thedate1,".","/")
      Theitem = rs("item") : if isnull(Theitem) then Theitem = ""
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
'[    if showthis then
    if showthis then
      lastdata1 = thedata1orig : thedata1orig = thedata1
']

      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thedate1
      if isnull(firstcoltitle) then firstcoltitle = "1/1/1900"
      if year(firstcoltitle) = 1900 then firstcoltitle = ""
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
'[      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
']
      rr("<img src=icons/current-work.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("item") then rr("<td class=list_item valign=top align=><nobr>" & theitem)
'[      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
'[      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
'[      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
      if left(session("history_item"), 1) = "o" then
        rr "<td class=list_item><nobr>" & getuser(getval(thedata1, "us"))
        rr "<td class=list_item><nobr>" : t = getval(thedata1, "da") : if isnumeric(t) then rr dateadd("s", t, "1/1/2000")
        rr "<td class=list_item><nobr>" & getval(thedata1, "st")
        rr "<td class=list_item><nobr>" & gettitle("suppliers", getval(thedata1, "su"))
        t = getval(thedata1, "ts") : if t = "" then t = getcode("courses", getval(thedata1, "co"))
        rr "<td class=list_item><nobr>" & t
        rr "<td class=list_item><nobr>" & getval(thedata1, "di")
        rr "<td class=list_item><nobr>" & gettitle("stations", getval(thedata1, "fr"))
        rr "<td class=list_item><nobr>" & gettitle("stations", getval(thedata1, "to"))
        rr "<td class=list_item>"
        tt = getval(thedata1, "pi") : a = "" : rr "(" & countstring(tt, ",") & ") "
        do until tt = ""
          t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
          a = a & ", " & getworkername(t)
        loop
        if a <> "" then a = mid(a,3)
        if len(a) < 4000 then rr a else rr "<div style='display:inline; cursor:hand; color:#0000aa;' title='" & a & "'>" & left(a, 40) & "...</div>"
        rr "<td class=list_item><nobr>" & getval(thedata1, "km")
        rr "<td class=list_item><nobr>" & getval(thedata1, "du")
        if hasmatch(session("userperm"), "prices") then rr "<td class=list_item><nobr>" & fornum(getval(thedata1, "pr"))
        if hasmatch(session("userperm"), "prices") and specs = "iec" then rr "<td class=list_item><nobr>" & getval(thedata1, "de")
      end if
      if left(session("history_item"), 1) = "w" then
        if specs = "jerusalem" then
          tt = "fi,la,co,em,ph,el,ac,sp,wo,un,ad,cr,fr,to,ci,cl," : for t = 1 to 50 : tt = tt & "z" & t & "," : next
          do until tt = ""
            t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
            if getval(lastdata1, t) = getval(thedata1, t) then thedata1 = setval(thedata1, t, "")
          loop
        end if
        rr "<td class=list_item><nobr>" & getuser(getval(thedata1, "us"))
        rr "<td class=list_item><nobr>" & getval(thedata1, "fi")
        rr "<td class=list_item><nobr>" & getval(thedata1, "la")
        rr "<td class=list_item><nobr>" & getval(thedata1, "co")
        rr "<td class=list_item><nobr>" & getval(thedata1, "em")
        rr "<td class=list_item><nobr>" & getval(thedata1, "ph")
        rr "<td class=list_item><nobr>" & getval(thedata1, "el")
        rr "<td class=list_item><nobr>" & getval(thedata1, "ac")
        rr "<td class=list_item><nobr>" & getval(thedata1, "sp")
        rr "<td class=list_item><nobr>" & getval(thedata1, "wo")
        rr "<td class=list_item><nobr>" & gettitle("units", getval(thedata1, "un"))
        rr "<td class=list_item><nobr>" & getval(thedata1, "ad")
        rr "<td class=list_item><nobr>" & getcode("courses", getval(thedata1, "cr"))
        rr "<td class=list_item><nobr>" & gettitle("stations", getval(thedata1, "fr"))
        rr "<td class=list_item><nobr>" & gettitle("stations", getval(thedata1, "to"))
        rr "<td class=list_item><nobr>" & gettitle("stations", getval(thedata1, "ci"))
        rr "<td class=list_item><nobr>" & getval(thedata1, "cl")
        if specs = "jerusalem" then
          z = 0 : tt = jerusalemhistoryadditionals
          do until tt = ""
            z = z + 1
            t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
            v = getval(thedata1, "z" & z)
            rr "<td class=list_item><nobr>" & v
          loop
        end if
      end if

      if left(session("history_item"), 1) = "s" then
        rr "<td class=list_item><nobr>" & getuser(getval(thedata1, "us"))
        rr "<td class=list_item><nobr>" & getval(thedata1, "ti")
        rr "<td class=list_item><nobr>" & getval(thedata1, "co")
        rr "<td class=list_item><nobr>" & getval(thedata1, "ac")
        rr "<td class=list_item><nobr>" & gettitle("sites", getval(thedata1, "si"))
        if specs = "jerusalem" then
          z = 0 : tt = jerusalemcourseshistoryadditionals
          do until tt = ""
            z = z + 1
            t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
            v = getval(thedata1, "z" & z)
            if t = "supplierid" or t = "supplierid2" then v = gettitle("suppliers", v)
            if t = "unitids" then v = gettitles("units", v)
            rr "<td class=list_item><nobr>" & v
          loop
        end if
      end if

      if left(session("history_item"), 1) = "c" then
        rr "<td class=list_item><nobr>" & getuser(getval(thedata1, "us"))
        rr "<td class=list_item><nobr>" & getval(thedata1, "da")
        rr "<td class=list_item><nobr>" & getval(thedata1, "st")
        rr "<td class=list_item><nobr>" & getval(thedata1, "ui")
        rr "<td class=list_item><nobr>" & getval(thedata1, "or")
        rr "<td class=list_item><nobr>" & gettitle("suppliers", getval(thedata1, "su"))
        rr "<td class=list_item><nobr>" & gettitle("drivers", getval(thedata1, "dr"))
        rr "<td class=list_item><nobr>" & getval(thedata1, "ti")
        rr "<td class=list_item><nobr>" & getval(thedata1, "de")
        rr "<td class=list_item><nobr>" & getval(thedata1, "sr")
        rr "<td class=list_item><nobr>" & getval(thedata1, "so")
      end if

      if left(session("history_item"), 1) = "r" then
        rr "<td class=list_item><nobr>" & getuser(getval(thedata1, "us"))
        rr "<td class=list_item><nobr>" & getcode("courses", getval(thedata1, "co")) & " " & gettitle("courses", getval(thedata1, "co"))
        rr "<td class=list_item><nobr>" & gettitle("stations", getval(thedata1, "fr"))
        rr "<td class=list_item><nobr>" & gettitle("stations", getval(thedata1, "to"))
        rr "<td class=list_item><nobr>" & gettitle("areas", getval(thedata1, "a1"))
        rr "<td class=list_item><nobr>" & gettitle("areas", getval(thedata1, "a2"))
        rr "<td class=list_item><nobr>" & getval(thedata1, "pr")
        rr "<td class=list_item><nobr>" & getval(thedata1, "ap")
        rr "<td class=list_item><nobr>" & getval(thedata1, "ac")
        rr "<td class=list_item><nobr>" & getval(thedata1, "as")
        rr "<td class=list_item><nobr>" & getval(thedata1, "du")
        rr "<td class=list_item><nobr>" & gettitle("suppliers", getval(thedata1, "su"))
        rr "<td class=list_item><nobr>" & getval(thedata1, "pa")
        rr "<td class=list_item><nobr>" & getval(thedata1, "ni")
        rr "<td class=list_item><nobr>" & getval(thedata1, "we")
        rr "<td class=list_item><nobr>" & getval(thedata1, "re")
        rr "<td class=list_item><nobr>" & gettitle("sites", getval(thedata1, "si"))
        d = getval(thedata1, "da") : d = replace(d, "\", "|")
        rr "<td class=list_item><nobr>" & getval(d, "entrance")
        rr "<td class=list_item><nobr>" & getval(d, "entrancearea")
        rr "<td class=list_item><nobr>" & getval(d, "stop")
        rr "<td class=list_item><nobr>" & getval(d, "addtoll")
        rr "<td class=list_item><nobr>" & getval(d, "addcollect")
        rr "<td class=list_item><nobr>" & getval(d, "adddistribute")
        if specs = "rail" then
          rr "<td class=list_item><nobr>" & getval(d, "category")
          rr "<td class=list_item><nobr>" & getval(d, "addstop")
          rr "<td class=list_item><nobr>" & getval(d, "weekdayreduction")
        end if
        rr "<td class=list_item><nobr>"
        if getval(d, "relativeprice") = "on" then
          ii = getidlist("select stationid from coursestations where courseid = 0" & getval(thedata1, "co") & " order by minutes")
          do until ii = ""
            i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
            if ii <> "" then rr gettitle("stations", i) & " - " & getval(d, "r" & i) & "<br>"
          loop
        end if
      end if

']
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("item") and session(tablename & "containerfield") <> "item" then rr("<td class=list_total1>") 'item
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
