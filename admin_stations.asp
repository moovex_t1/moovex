<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 24/04/2014 10:18:50

function stationsdelitem(i)
  stationsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function stationsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "" : if getfield("inmap") <> "" then t = getfield("inmap")
  f = f & "inmap," : v = v & "'" & t & "',"

'[  t = "on" : if getfield("special") <> "" then t = getfield("special")
  t = "" : if getparam("DefaultSpecialStation") = "y" then t = "on"
  if getfield("special") <> "" then t = getfield("special")
']
  f = f & "special," : v = v & "'" & t & "',"

  t = "" : if getfield("main") <> "" then t = getfield("main")
  f = f & "main," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "areaid" then t = session(tablename & "containerid")
  if getfield("areaid") <> "" then t = getfield("areaid")
  f = f & "areaid," : v = v & t & ","

  t = "" : if getfield("markerx") <> "" then t = getfield("markerx")
  f = f & "markerx," : v = v & "'" & t & "',"

  t = "" : if getfield("markery") <> "" then t = getfield("markery")
  f = f & "markery," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  t = session("usersiteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
']
  f = f & "siteid," : v = v & t & ","

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into stations(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from stations"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  stationsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table stations (id autoincrement primary key,title text(50),inmap text(50),special text(2),main text(2),areaid number,markerx text(20),markery text(20),siteid number,data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "stations" : session("tablename") = "stations"
tablefields = "id,title,inmap,special,main,areaid,markerx,markery,siteid,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
hidecontrols = hidecontrols & "main,"
if session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"   '''buttondelete,buttonmultidelete,
if session("userlevel") <> "5" then hidecontrols = hidecontrols & "buttondelete,buttonmultidelete,"
session("stationcoursecache") = ""
session("stationareas") = ""
UseNeighborhoods = false : if hasmatch(specs, "jerusalem,muni,brener,iec,") then UseNeighborhoods = true
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = stationsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
'[  if okedit("inmap") then s = s & "inmap='" & getfield("inmap") & "',"
  t = getfield("inmap") : if t = "" then t = getfield("title")
  if okedit("inmap") then s = s & "inmap='" & t & "',"
']
  if okedit("special") then s = s & "special='" & getfield("special") & "',"
  if okedit("main") then s = s & "main='" & getfield("main") & "',"
  if okedit("areaid") then s = s & "areaid=0" & getfield("areaid") & ","
'[  if okedit("markerx") then s = s & "markerx='" & getfield("markerx") & "',"
'[  if okedit("markery") then s = s & "markery='" & getfield("markery") & "',"
  markerx = getfield("markerx")
  markery = getfield("markery")
  if markerx = "" and getparam("UseGoogleMaps") = "y" then
    t = getfield("inmap") : if t = "" then t = getfield("title")
    tt = getaddresslatlon(t)
    if tt <> "" then markerx = getlistitem(tt,2) : markery = getlistitem(tt,1)
  end if
  if okedit("markerx") then s = s & "markerx='" & markerx & "',"
  if okedit("markery") then s = s & "markery='" & markery & "',"
']
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'['-------------afterupdate-------------
if request_action = "update" then setstationareaid(request_id)
']
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = stationsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = stationsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~stations_title~^"
  if oksee("inmap") then csv(0) = csv(0) & "~stations_inmap~^"
  if oksee("special") then csv(0) = csv(0) & "~stations_special~^"
  if oksee("main") then csv(0) = csv(0) & "~stations_main~^"
  if oksee("areaid") then csv(0) = csv(0) & "~stations_areaid~^"
  if oksee("markerx") then csv(0) = csv(0) & "~stations_markerx~^"
  if oksee("markery") then csv(0) = csv(0) & "~stations_markery~^"
  if oksee("siteid") then csv(0) = csv(0) & "~stations_siteid~^"
'[  if oksee("data1") then csv(0) = csv(0) & "~stations_data1~^"
  if UseNeighborhoods then csv(0) = csv(0) & "�����^" 'neighborhood
  if oksee("data1") then csv(0) = csv(0) & "~stations_data1~^"
']
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theinmap = rs("inmap") : if isnull(Theinmap) then Theinmap = ""
  Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
  Themain = rs("main") : if isnull(Themain) then Themain = ""
  Theareaid = rs("areaid") : if isnull(Theareaid) then Theareaid = 0
  Theareaid = replace(theareaid,",",".")
  Themarkerx = rs("markerx") : if isnull(Themarkerx) then Themarkerx = ""
  Themarkery = rs("markery") : if isnull(Themarkery) then Themarkery = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Theareaidlookup = rs("areaidlookup") : if isnull(Theareaidlookup) then Theareaidlookup = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("inmap") then csv(rc) = csv(rc) & replace(theinmap,vbcrlf," ") & "^"
    if oksee("special") then csv(rc) = csv(rc) & replace(thespecial,vbcrlf," ") & "^"
    if oksee("main") then csv(rc) = csv(rc) & replace(themain,vbcrlf," ") & "^"
    if oksee("areaid") then csv(rc) = csv(rc) & theareaidlookup & "^"
    if oksee("markerx") then csv(rc) = csv(rc) & replace(themarkerx,vbcrlf," ") & "^"
    if oksee("markery") then csv(rc) = csv(rc) & replace(themarkery,vbcrlf," ") & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
'[    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    if UseNeighborhoods then csv(rc) = csv(rc) & tocsv(getval(thedata1, "neighborhood")) & "^"
    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
']
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      siteid = session("usersiteid")
      data1 = ""
      neighborhood = ""
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "title" then
'[          if len(b) > 50 then b = left(b,50)
'[          if okedit("title") then s = s & "title='" & b & "',"
          x = 50 : if specs = "sheba" then x = 200
          if len(b) > x then b = left(b,x)
          if okedit("title") then s = s & "title='" & b & "',"
          sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "' and siteid = " & siteid
          closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
          if not rs1.eof then thisid = rs1("id")
']
        elseif csvff(i) = "inmap" then
'[          if len(b) > 50 then b = left(b,50)
'[          if okedit("inmap") then s = s & "inmap='" & b & "',"
          x = 50 : if specs = "sheba" then x = 200
          if len(b) > x then b = left(b,x)
          if okedit("inmap") then s = s & "inmap='" & b & "',"
']
        elseif csvff(i) = "special" then
          if len(b) > 2 then b = left(b,2)
          if okedit("special") then s = s & "special='" & b & "',"
        elseif csvff(i) = "main" then
          if len(b) > 2 then b = left(b,2)
          if okedit("main") then s = s & "main='" & b & "',"
'[        elseif csvff(i) = "areaid" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from areas where lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("areaid") then s = s & "areaid=" & v & ","
        elseif csvff(i) = "areaid" then
          if b = "" then
            v = 0
          else
            sq = "select * from areas where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            if v = 0 then
              sq = "insert into areas(title,areaids) values('" & b & "','')"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from areas"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              v = rs1("id")
            end if
          end if
          if okedit("areaid") then s = s & "areaid=" & v & ","
']
        elseif csvff(i) = "markerx" then
          if len(b) > 20 then b = left(b,20)
          if okedit("markerx") then s = s & "markerx='" & b & "',"
        elseif csvff(i) = "markery" then
          if len(b) > 20 then b = left(b,20)
          if okedit("markery") then s = s & "markery='" & b & "',"
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("siteid") then s = s & "siteid=" & v & ","
          if okedit("siteid") then s = s & "siteid=" & v & ","
          if session("usersiteid") = "0" then siteid = v
']
'[        elseif csvff(i) = "data1" then
'[          if okedit("data1") then s = s & "data1='" & b & "',"
        elseif csvff(i) = "�����" or csvff(i) = "neighborhood" then
          neighborhood = b
          if hasmatch(specs, "jerusalem,muni,brener,") and b <> "" then
            sq = "select id from misc where type1 = 'neighborhood' and title = '" & b & "'"
            if siteid <> "" then sq = sq & " and (instr(cstr('' & contents), '|siteid=" & siteid & "|') + instr(cstr('' & contents), '|siteid=0|') > 0 or instr(cstr('' & contents), '|siteid=') = 0)"
            ii = getidlist(sq)
            if ii = "" then
              sq = "insert into misc(title,contents,type1) values("
              sq = sq & "'" & b & "',"
              t = "" : if siteid <> "" then t = "|siteid=" & siteid & "|"
              sq = sq & "'" & t & "',"
              sq = sq & "'neighborhood')"
              set rs1 = conn.execute(sqlfilter(sq))
            end if
          end if
        elseif csvff(i) = "data1" then
          data1 = b
']
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
'[        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = stationsinsertrecord
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = stationsinsertrecord
        if data1 = "" then data1 = getonefield("select data1 from " & tablename & " where id = 0" & theid)
        if neighborhood <> "" then data1 = setval(data1, "neighborhood", neighborhood)
        if okedit("data1") then s = s & "data1='" & data1 & "',"
']
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'['-------------more actions-------------------------------------

'----------------map
if request_action = "map" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  session("map_stationids") = ids
  x = rred("map.asp")
end if

'--------createcourse
if request_action = "createcourse" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  ii = ids
  ii = uniquelist(ii) : ii = nomatch(ii, "0,")
  if ii = "" then du = errorend("_back~Please choose some items~")
  dest = getfield("createcousedestination") : if dest = "0" then du = errorend("_back~Please choose destination station~")
  courseid = createcourse(0, ii & dest & ",")
  rr "<center><br><br>OK<br>"

  t = getonefield("select top 1 siteid from stations where id in(" & ii & "-1) order by siteid desc")
  if t <> "" then
    s = "update courses set siteid = " & t & " where id = " & courseid
    set rs1 = conn.execute(sqlfilter(s))
  end if 
  
  session("map_courseids") = courseid & ","
  session("map_workerids") = ids
  session("map_stationids") = ii & dest & ","
  'rr "<br><a href=map.asp>~Map~</a>"
  rr "<script language=vbscript> document.location = ""map.asp"" </script>"
  rend
end if

if request_action = "workerscreenupdate" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  s = "select id,data1 from " & tablename & " where id in(" & ids & "-1)"
  set rs = conn.execute(sqlfilter(s))
  c = 0
  do until rs.eof
    c = c + 1
    v = getfield("workerscreen" & rs("id"))
    d = setval(rs("data1"), "workerscreen", tocsv(v))
    s = "update " & tablename & " set data1 = '" & d & "' where id = " & rs("id")
    set rs1 = conn.execute(sqlfilter(s))
    rs.movenext
  loop
  msg = msg & c & " ����� ������"
end if

if request_action = "updateneighborhood" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  s = "select id,data1 from " & tablename & " where id in(" & ids & "-1)"
  set rs = conn.execute(sqlfilter(s))
  c = 0
  do until rs.eof
    c = c + 1
    v = gettitle("misc", getfield("multineighborhoodid"))
    d = rs("data1") & ""
    d = setval(d, "neighborhood", tocsv(v))
    s = "update " & tablename & " set data1 = '" & d & "' where id = " & rs("id")
    set rs1 = conn.execute(sqlfilter(s))
    rs.movenext
  loop
  msg = msg & c & " ����� ������"
end if

if getfield("action") = "getxy" or getfield("action2") = "getxy" then
  server.scripttimeout = 100000 'seconds
  if getfield("action2") = "getxy" then
    ids = getfield("id") & ","
    request_action = "form"
  else
    ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  end if
  s = "select id,inmap,title from stations where id in(" & ids & "-1)"
  s = s & okreadsql("stations")
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    a = trim("" & rs("inmap")) : if a = "" then a = trim("" & rs("title"))
    if a <> "" then
      rr a
      t = GetAddressXY(a)
      if instr(t, ",") > 0 then
        x = left(t, instr(t,",")-1)
        y = mid(t, instr(t,",")+1)
        rr " - " & x & "," & y
        s = "update stations set"
        s = s & " markerx = '" & x & "'"
        s = s & ",markery = '" & y & "'"
        s = s & " where id = " & rs("id")
        set rs1 = conn.execute(sqlfilter(s))
      end if
    end if
    rr "<br>"
    rs.movenext
  loop
end if

if getfield("action") = "findarea" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  du = findarea(ids)
  msg = " ~Areas~ ~Updated~"
end if

if getfield("action") = "cleararea" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  s = "update stations set areaid = 0 where id in(" & ids & "-1)"
  set rs = conn.execute(sqlfilter(s))
end if

if getfield("action") = "updatearea" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  s = "update stations set areaid = 0" & getfield("multiareaid") & " where id in(" & ids & "-1)"
  set rs = conn.execute(sqlfilter(s))
end if

if getfield("action") = "updatename" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  s = "select id,markerx,markery from stations where id in(" & ids & "-1)"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    title = getxyaddress(rs("markery") & "", rs("markerx") & "")
    if title <> "" then
      s = "update stations set title = '" & tocsv(title) & "', inmap = '' where id = " & rs("id")
      set rs1 = conn.execute(sqlfilter(s))
    end if
    rs.movenext
  loop
end if

']
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","stations.id,stations.title,stations.inmap,stations.special,stations.main,stations.areaid,stations.markerx,stations.markery,stations.siteid,stations.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theinmap = rs("inmap") : if isnull(Theinmap) then Theinmap = ""
  Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
  Themain = rs("main") : if isnull(Themain) then Themain = ""
  Theareaid = rs("areaid") : if isnull(Theareaid) then Theareaid = 0
  Theareaid = replace(theareaid,",",".")
  Themarkerx = rs("markerx") : if isnull(Themarkerx) then Themarkerx = ""
  Themarkery = rs("markery") : if isnull(Themarkery) then Themarkery = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "title,inmap,special,main,areaid,markerx,markery,siteid,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_home.gif' ~iconsize~> ~stations_stations~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~stations_title~<img src=must.gif></span><td class=form_item2 id=title_tr2>")
'[    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
    x = 50 : if specs = "sheba" then x = 200
    rr("<input type=text maxlength=" & x & " name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
']
  end if'form:title

  if oksee("inmap") then'form:inmap
    rr("<tr valign=top id=inmap_tr0><td class=form_item1 id=inmap_tr1><span id=inmap_caption>~stations_inmap~</span><td class=form_item2 id=inmap_tr2>")
'[    rr("<input type=text maxlength=50 name=inmap onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theinmap & """ dir=~langdir~>")
'[  end if'form:inmap
    x = 50 : if specs = "sheba" then x = 200
    rr("<input type=text maxlength=" & x & " name=inmap onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theinmap & """ dir=~langdir~>")
  end if'form:inmap

  if hasmatch(specs, "jerusalem,muni,") then
    rr("<tr valign=top><td class=form_item1>�� ������<td class=form_item2>")
    v = ctxt(getval(thedata1, "arabic"))
    rr "<input type=text value='" & v & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""arabic"", me.value)'><br>"
  end if

  if hasmatch("rafael,clal,", specs) then
    rr("<tr valign=top><td class=form_item1>���� ����� ������<td class=form_item2>")
    v = getval(thedata1, "workerscreen")
    rr "<input type=checkbox name=workerscreen" : if v = "on" then rr " checked"
    rr " onclick='vbscript: v = iif(me.checked, ""on"", """") : f1.data1.value = setval(f1.data1.value, ""workerscreen"", v)'>"
  end if

  if hasmatch("shebain,", specs) then
    rr("<tr valign=top><td class=form_item1>�����<td class=form_item2>")
    rr "<input type=text value='" & getval(thedata1, "phone1") & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""phone1"", me.value)'><br>"
    rr("<tr valign=top><td class=form_item1>������ ����<td class=form_item2>")
    thesubstations = getval(thedata1, "substations")
    t = "select * from stations where id <> " & theid & " order by title"
    rr(selectlookupmultiauto("f1.substations",thesubstations, "icons/home.png", "stations,title,:" & t, 300, okwritehere))
  end if

  if hasmatch("rafael,", specs) then
    rr("<tr valign=top><td class=form_item1>��� ����<td class=form_item2>")
    rr "<input type=text value='" & getval(thedata1, "stationcode") & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""stationcode"", me.value)'><br>"
    rr("<tr valign=top><td class=form_item1>������ �������<td class=form_item2>")
    theshiftids = getval(thedata1, "shiftids")
    t = "select * from shifts where (siteid = " & thesiteid & " or siteid = 0 or id in(" & theshiftids & "-1)) order by title,hour1,hour1b"
    rr(selectlookupmultiauto("f1.shiftids",theshiftids, "icons/refresh.png", "shifts,title,:" & t, 300, okwritehere))
  end if

']

  if oksee("special") then'form:special
    rr("<tr valign=top id=special_tr0><td class=form_item1 id=special_tr1><span id=special_caption>~stations_special~</span><td class=form_item2 id=special_tr2>")
    rr("<input type=checkbox name=special")
    if thespecial = "on" then rr(" checked")
    rr(">")
  end if'form:special

  if oksee("main") then'form:main
    rr("<tr valign=top id=main_tr0><td class=form_item1 id=main_tr1><span id=main_caption>~stations_main~</span><td class=form_item2 id=main_tr2>")
    rr("<input type=checkbox name=main")
    if themain = "on" then rr(" checked")
    rr(">")
  end if'form:main

  if oksee("areaid") and session(tablename & "containerfield") <> "areaid" then
    rr("<tr valign=top id=areaid_tr0><td class=form_item1 id=areaid_tr1><span id=areaid_caption>~stations_areaid~</span><td class=form_item2 id=areaid_tr2>")
    if getfield("box") = "areaid" then theareaid = getfield("boxid")
    rr("<select name=areaid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~stations_areaid~")
    sq = "select * from areas order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theareaid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:areaid
  elseif okedit("areaid") then
    rr("<input type=hidden name=areaid value=" & theareaid & ">")
'[  end if'form:areaid
  end if'form:areaid

  if UseNeighborhoods then
    rr("<tr valign=top><td class=form_item1>�����<td class=form_item2>")
    v = getval(thedata1, "neighborhood")
    rr "<select name=neighborhood onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""neighborhood"", me.value)'>"
    rr("<option value='' style='color:bbbbfe;'>�����")
    sq = "select id,title from misc where type1 = 'neighborhood'"
    if specs <> "iec" then sq = sq & " and (instr(cstr('' & contents), '|siteid=" & thesiteid & "|') + instr(cstr('' & contents), '|siteid=0|') > 0 or instr(cstr('' & contents), '|siteid=') = 0)"
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    had = 0
    do until rs1.eof
      rr("<option value=""" & rs1("title") & """")
      if v = cstr(rs1("title")) then rr(" selected") : had = 1
      rr(">")
      rr(rs1("title"))
      rs1.movenext
    loop
    if had = 0 then rr "<option value=""" & v & """ selected>" & v
    rr("</select></nobr>")
  end if
']

  if oksee("markerx") then'form:markerx
    rr("<span id=markerx_caption></span> ")
    rr("<input type=hidden name=markerx value=""" & Themarkerx & """>")
  end if'form:markerx

  if oksee("markery") then'form:markery
    rr("<span id=markery_caption></span> ")
    rr("<input type=hidden name=markery value=""" & Themarkery & """>")
  end if'form:markery

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~stations_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~stations_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
  end if'form:data1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
'[  rr("</tr></form>" & bottombar)'form
  if getparam("UseGoogleMaps") = "y" then
    rr(" <input type=button class=groovybutton id=buttongetxy value='~Find Geo Location~' onclick='vbscript: f1.action2.value = ""getxy"" : f1.submit'>")
    rr "<br><br><br>"
    u = "map.asp?editstation=on&courseids=-&workerids=-&stationids=" & theid & ","
    rr "<a href=" & u & " target=_new>~Show in full screen~</a><br>"
    'rr "<iframe width=800 height=500 frameborder=0 style='border:1px solid #000000;' src=" & u & "&embed=on></iframe><br><br><br>"

    '---------osm------
    rr "<div id=map1 style='width:800; height:550; border:1px solid #aaaaaa;'></div>"
    rr "<script src=https://openlayers.org/api/OpenLayers.js></script>"
    rr "<script>"
    rr "  map = new OpenLayers.Map(""map1"");"
    rr "  map.addLayer(new OpenLayers.Layer.OSM());"
    rr "  epsg4326 =  new OpenLayers.Projection(""EPSG:4326"");"
    rr "  projectTo = map.getProjectionObject();"
    rr "  var lonLat = new OpenLayers.LonLat(" & themarkerx & "," & themarkery & ").transform(epsg4326, projectTo);"
    rr "  map.setCenter (lonLat, 13);"
    rr "  var markers = new OpenLayers.Layer.Markers(""Markers"");"
    rr "  map.addLayer(markers);"
    rr "  var size = new OpenLayers.Size(22,35);"
    rr "  var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);"
    rr "  var icon = new OpenLayers.Icon('marker.png', size, offset);"
    rr "  markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(" & themarkerx & "," & themarkery & ").transform(epsg4326, projectTo),icon));"
    rr "  //markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(33.977429,32.833265).transform(epsg4326, projectTo),icon.clone()));"
    rr "</script>"
    '-----------------------

  end if
  rr("</tr></form>" & bottombar)'form
']

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
'[  rr("    ok = true" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if specs = "rafael" then rr " f1.data1.value = setval(f1.data1.value, ""shiftids"", f1.shiftids.value)" & vbcrlf
  if specs = "shebain" then rr " f1.data1.value = setval(f1.data1.value, ""substations"", f1.substations.value)" & vbcrlf
  if specs = "egged" then rr("if f1.areaid.value <> ""0"" then validfield(""areaid"") else invalidfield(""areaid"") : ok = false" & vbcrlf)
']
  if okedit("title") then rr("    if f1.title.value <> """" then validfield(""title"") else invalidfield(""title"") : ok = false" & vbcrlf)
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = "markerx,markery,data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/i_home.gif' ~iconsize~> ~stations_stations~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  rr(" <nobr><img id=special_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=special dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~stations_special~")
  rr("<option value=on") : if lcase(session(tablename & "_special")) = "on" then rr(" selected")
  rr("> ~stations_special~: ~Yes~")
  rr("<option value=no") : if lcase(session(tablename & "_special")) = "no" then rr(" selected")
  rr("> ~stations_special~: ~No~")
  rr("</select></nobr>")'search:special
  rr(" <nobr><img id=main_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=main dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~stations_main~")
  rr("<option value=on") : if lcase(session(tablename & "_main")) = "on" then rr(" selected")
  rr("> ~stations_main~: ~Yes~")
  rr("<option value=no") : if lcase(session(tablename & "_main")) = "no" then rr(" selected")
  rr("> ~stations_main~: ~No~")
  rr("</select></nobr>")'search:main
  if session(tablename & "containerfield") <> "areaid" then
    rr(" <nobr><img id=areaid_filtericon src=icons/design.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=areaid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~stations_areaid~")
    rr("<option value='' style='color:bbbbfe;'>~stations_areaid~")
    rr "<option value='0'" : if cstr(session(tablename & "_areaid")) = "0" then rr " selected"
    rr ">(~None~)"
']
    sq = "select * from areas order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_areaid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:areaid
  end if'search:areaid
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~stations_siteid~")
    rr("<option value='' style='color:bbbbfe;'>~stations_siteid~")
    rr "<option value='0'" : if cstr(session(tablename & "_siteid")) = "0" then rr " selected"
    rr ">(~None~)"
']
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
'[  end if'search:siteid
  end if'search:siteid
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"

  if specs = "rafael" then
    rr " <select name=workerscreen dir=~langdir~>"
    rr "<option value='' style='color:bbbbfe;'>���� ����� ������"
    rr "<option value=on" : if lcase(session(tablename & "_workerscreen")) = "on" then rr " selected"
    rr "> ���� ����� ������: ~Yes~"
    rr "<option value=no" : if lcase(session(tablename & "_workerscreen")) = "no" then rr " selected"
    rr "> ���� ����� ������: ~No~"
    rr "</select></span>"
  end if

  if getparam("UseGoogleMaps") = "y" then
    rr(" <nobr><img id=hasxy_filtericon src=marker.png style='filter:alpha(opacity=50); margin-top:8; width:16; height:16;'>")
    rr(" <select style='width:150;' name=hasxy dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~Map Location~")
    rr("<option value=y")
    if cstr(session(tablename & "_hasxy")) = "y" then rr(" selected")
    rr(">~Map Location~ - ~Yes~")
    rr("<option value=n")
    if cstr(session(tablename & "_hasxy")) = "n" then rr(" selected")
    rr(">~Map Location~ - ~No~")
    rr("</select></nobr>")
  end if

  rr(" <nobr><img id=connected_filtericon src=flat_course.png style='filter:alpha(opacity=50); margin-top:8; width:16; height:16;'>")
  rr(" <select style='width:150;' name=connected dir=~langdir~>")
  rr("<option value='' style='color:bbbbfe;'>~coursestations_coursestations~")
  rr("<option value=y")
  if cstr(session(tablename & "_connected")) = "y" then rr(" selected")
  rr(">~coursestations_coursestations~ - ~Yes~")
  rr("<option value=n")
  if cstr(session(tablename & "_connected")) = "n" then rr(" selected")
  rr(">~coursestations_coursestations~ - ~No~")
  rr("</select></nobr>")

']
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & areas_1.title) as areaidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join areas areas_1 on " & tablename & ".areaid = areas_1.id)"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

'[  '-sqladd
  if session(tablename & "_workerscreen") = "on" then s = s & " and instr('' & cstr('' & " & tablename & ".data1), '|workerscreen=on|') > 0"
  if session(tablename & "_workerscreen") = "no" then s = s & " and instr('' & cstr('' & " & tablename & ".data1), '|workerscreen=on|') = 0"
  if session(tablename & "_hasxy") = "y" then s = s & " and markerx <> ''"
  if session(tablename & "_hasxy") = "n" then s = s & " and (markerx = '' or isnull(markerx))"

  if session(tablename & "_connected") <> "" then du = getconnectedstations()
  if session(tablename & "_connected") = "y" then s = s & " and stations.id in(" & session("connectedstations") & "0)"
  if session(tablename & "_connected") = "n" then s = s & " and stations.id not in(" & session("connectedstations") & "0)"

']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","stations.id,stations.title,stations.inmap,stations.special,stations.main,stations.areaid,stations.markerx,stations.markery,stations.siteid,stations.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_special") = "on" then s = s & " and lcase('' & " & tablename & ".special) = 'on'"
  if session(tablename & "_special") = "no" then s = s & " and lcase('' & " & tablename & ".special) <> 'on'"
  if session(tablename & "_main") = "on" then s = s & " and lcase('' & " & tablename & ".main) = 'on'"
  if session(tablename & "_main") = "no" then s = s & " and lcase('' & " & tablename & ".main) <> 'on'"
'[  if session(tablename & "_areaid") <> "" and session(tablename & "containerfield") <> "areaid" then s = s & " and " & tablename & ".areaid = " & session(tablename & "_areaid")
  w = session(tablename & "_areaid")
  if w = "0" then
    tt = getidlist("select id from areas")
    s = s & " and (" & tablename & ".areaid not in(" & tt & "-1) or isnull(" & tablename & ".areaid))"
  elseif w <> "" then
    s = s & " and " & tablename & ".areaid = " & session(tablename & "_areaid")
  end if
']
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
'[    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
'[    ww = session(tablename & "_string") : ww = ctxt(ww) : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
'[    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
'[    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = session(tablename & "_string") : ww = ctxt(ww) : ww = replacechars(ww, "!@#$%^&()_+-=<>{};':""|\/?.,<>", "                              ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789���������������������������* ")
']
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
'[      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      if left(session(tablename & "_string"), 5) = "&#34;" then w = trim (w & " " & ww): ww = ""
']
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".inmap like '%" & w & "%' or"
'[        s = s & " cstr('' & areas_1.title) & ' ' like '%" & w & "%' or"
']
'[        s = s & " " & tablename & ".markerx like '%" & w & "%' or"
']
'[        s = s & " " & tablename & ".markery like '%" & w & "%' or"
']
'[        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
']
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("title") then f = "stations.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_title~</a> " & a2)
  if oksee("inmap") then f = "stations.inmap" : f2 = "inmap" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("inmap") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_inmap~</a> " & a2)
'[  if oksee("special") then f = "stations.special" : f2 = "special" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("special") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_special~</a> " & a2)
  if specs = "rafael" then rr "<td class=list_title><nobr>���� ����� ������" 'workerscreen
  if oksee("special") then f = "stations.special" : f2 = "special" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("special") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_special~</a> " & a2)
']
  if oksee("main") then f = "stations.main" : f2 = "main" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("main") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_main~</a> " & a2)
'[  if oksee("areaid") and session(tablename & "containerfield") <> "areaid" then
'[    f = "areas_1.title" : f2 = "areaidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_areaid~</a> " & a2)
'[  end if
  if oksee("areaid") and session(tablename & "containerfield") <> "areaid" then
    f = "areas_1.title" : f2 = "areaidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_areaid~</a> " & a2)
  end if
  if UseNeighborhoods then rr("<td class=list_title><nobr>�����") 'neighborhood
']
'[  if oksee("markerx") then f = "stations.markerx" : f2 = "markerx" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("markerx") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_markerx~</a> " & a2)
'[  if oksee("markery") then f = "stations.markery" : f2 = "markery" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("markery") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_markery~</a> " & a2)
  f = "stations.markerx" : f2 = "markerx" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~Map Location~</a> " & a2)
']
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~stations_siteid~</a> " & a2)
  end if
  if oksee("data1") then rr("<td class=list_title><nobr>~stations_data1~")

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Theinmap = rs("inmap") : if isnull(Theinmap) then Theinmap = ""
      Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
      Themain = rs("main") : if isnull(Themain) then Themain = ""
      Theareaid = rs("areaid") : if isnull(Theareaid) then Theareaid = 0
      Theareaid = replace(theareaid,",",".")
      Themarkerx = rs("markerx") : if isnull(Themarkerx) then Themarkerx = ""
      Themarkery = rs("markery") : if isnull(Themarkery) then Themarkery = ""
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Theareaidlookup = rs("areaidlookup") : if isnull(Theareaidlookup) then Theareaidlookup = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : t = "" : if instr("," & request_ids, "," & i & ",") > 0 then t = " checked"
      if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & t & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/i_home.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("inmap") then rr("<td class=list_item valign=top align=><nobr>" & theinmap)
'[      if oksee("special") then rr("<td class=list_item valign=top align=><nobr>") : if thespecial = "on" then rr("v")
      if specs = "rafael" then
        rr "<td class=list_item>"
        v = getval(thedata1, "workerscreen")
        if session("usergroup") = "admin" then
          rr "<input type=checkbox name=workerscreen" & theid : if v = "on" then rr " checked"
          rr " onclick='vbscript: c" & theid & ".checked = true'>"
        else
          if v = "on" then rr "v"
        end if
      end if
      if oksee("special") then rr("<td class=list_item valign=top align=><nobr>") : if thespecial = "on" then rr("v")
']
      if oksee("main") then rr("<td class=list_item valign=top align=><nobr>") : if themain = "on" then rr("v")
'[      if oksee("areaid") and session(tablename & "containerfield") <> "areaid" then rr("<td class=list_item valign=top align=><nobr>" & theareaidlookup)
      if oksee("areaid") and session(tablename & "containerfield") <> "areaid" then rr("<td class=list_item valign=top align=><nobr>" & theareaidlookup)
      if UseNeighborhoods then rr "<td class=list_item>" & getval(thedata1, "neighborhood")
']
'[      if oksee("markerx") then rr("<td class=list_item valign=top align=><nobr>" & themarkerx)
'[      if oksee("markery") then rr("<td class=list_item valign=top align=><nobr>" & themarkery)
      rr "<td class=list_item valign=top align=><nobr>" : if themarkerx <> "" then rr "<img src=marker.png width=16 height=16>"
']
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("inmap") and session(tablename & "containerfield") <> "inmap" then rr("<td class=list_total1>") 'inmap
'[  if oksee("special") and session(tablename & "containerfield") <> "special" then rr("<td class=list_total1>") 'special
  if specs = "rafael" then
    rr "<td>"
    if session("usergroup") = "admin" then rr "<input type=button value='���� ������' style='width:50; height:22; font-size:9;' onclick='vbscript: me.disabled = true : flist.action.value = ""workerscreenupdate"" : flist.action2.value = """" : flist.submit'>"
  end if
  if oksee("special") and session(tablename & "containerfield") <> "special" then rr("<td class=list_total1>") 'special
']
  if oksee("main") and session(tablename & "containerfield") <> "main" then rr("<td class=list_total1>") 'main
  if oksee("areaid") and session(tablename & "containerfield") <> "areaid" then rr("<td class=list_total1>") 'areaid
  if oksee("markerx") and session(tablename & "containerfield") <> "markerx" then rr("<td class=list_total1>") 'markerx
  if oksee("markery") and session(tablename & "containerfield") <> "markery" then rr("<td class=list_total1>") 'markery
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

'[    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
    if getparam("UseGoogleMaps") = "y" then
      rr(" <input type=button class=groovybutton style='width:120;' id=buttonmap value='~Show~ ~map~' onclick='vbscript: me.disabled = true : flist.action.value = ""map"" : flist.action2.value = """" : flist.token.value = window.name : flist.submit'>")
    end if
    if getparam("UseGoogleMaps") = "y" and session("usergroup") = "admin" then
      rr(" <input type=button class=groovybutton value='~Find Geo Location~' onclick='vbscript:if msgbox(""~Find Geo Location~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""getxy"" : flist.action2.value = """" : flist.submit'>")
      rr(" <input type=button class=groovybutton value='~Find~ ~Area~' onclick='vbscript:if msgbox(""~Area will be set according to closest station area. ~Find Area~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""findarea"" : flist.action2.value = """" : flist.submit'>")
      rr(" <input type=button class=groovybutton value='~Clear~ ~Area~' onclick='vbscript:if msgbox(""~Clear~ ~Area~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""cleararea"" : flist.action2.value = """" : flist.submit'>")
      rr(" <input type=button class=groovybutton value='~Update~ ~Name~' onclick='vbscript:if msgbox(""~Update station name by map location~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""updatename"" : flist.action2.value = """" : flist.submit'>")
      rr(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")

      rr("<select name=multiareaid dir=~langdir~>")
      rr("<option value=0 style='color:bbbbfe;'>~stations_areaid~")
      sq = "select * from areas order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        rr "<option value=""" & rs1("id") & """>" & rs1("title")
        rs1.movenext
      loop
      rr("</select>")
      rr(" <input type=button class=groovybutton value='~Update~ ~Area~' onclick='vbscript:if msgbox(""~Update~ ~Area~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""updatearea"" : flist.action2.value = """" : flist.submit'>")

      if UseNeighborhoods then
        rr(" <select name=multineighborhoodid dir=~langdir~>")
        rr("<option value=0 style='color:bbbbfe;'>�����")
        sq = "select * from misc where type1 = 'neighborhood' order by title"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
        do until rs1.eof
          rr "<option value=""" & rs1("id") & """>" & rs1("title")
          rs1.movenext
        loop
        rr("</select>")
        rr(" <input type=button class=groovybutton value='���� ������' onclick='vbscript:if msgbox(""?����� ������"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""updateneighborhood"" : flist.action2.value = """" : flist.submit'>")
      end if

      rr " <input type=button class=groovybutton style='width:120;' id=buttonfindcloseststation value='~Create~ ~course~' onclick='vbscript:if msgbox(""~Create~ ~course~?"", vbyesno) = ""6"" then me.disabled = true : flist.action.value = ""createcourse"" : flist.submit'>"
	    a = getval(session("mainstations"), "a")
	    rr " ~To ~ <select name=createcousedestination>"
	    rr "<option value=0>"
	    do until a = ""
	      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
	      rr "<option value=" & b & ">" & gettitle("stations", b)
	    loop
	    rr "</select>"
      rr "<br>"
    end if

']
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
