<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 12/07/2015 10:35:20

function miscdelitem(i)
  miscdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function miscinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "0" : if getfield("num1") <> "" then t = getfield("num1")
  f = f & "num1," : v = v & t & ","

  t = now : if getfield("date1") <> "" then t = getfield("date1")
  f = f & "date1," : v = v & sqldate(t) & ","

  t = "" : if getfield("contents") <> "" then t = getfield("contents")
  f = f & "contents," : v = v & "'" & t & "',"

  t = "" : if getfield("file1") <> "" then t = getfield("file1")
  f = f & "file1," : v = v & "'" & t & "',"

'[  t = "" : if getfield("type1") <> "" then t = getfield("type1")
  t = session("miscview") : if getfield("type1") <> "" then t = getfield("type1")
']
  f = f & "type1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into misc(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from misc"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  miscinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'['-------------declarations-------------------------------
if getfield("miscview") <> "" then session("miscview") = getfield("miscview") : if session("miscview") = "-" then session("miscview") = ""
if getfield("miscview") <> "" then session("misc_monthview") = ""
if getfield("miscdisplay") <> "" then session("miscdisplay") = getfield("miscdisplay") : if session("miscdisplay") = "-" then session("miscdisplay") = translate("~misc_misc~")
if getfield("mischidefields") <> "" then session("mischidefields") = getfield("mischidefields") : if session("mischidefields") = "-" then session("mischidefields") = ""
if getfield("misctrans") <> "" then session("misctrans") = getfield("misctrans")
if session("miscdisplay") = "" then session("miscdisplay") = "�����"
hidecontrols = hidecontrols & "type1," & session("mischidefields")
session("misc_date1withhour") = ""
if instr(session("miscview"), "_documents") > 0 then session("misc_date1withhour") = "on"
if session("miscview") = "produceduntil" then session("misc_date1withhour") = "on"
if session("miscview") = "driverevent" then session("misc_date1withhour") = "on"
if session("miscview") = "madads" then session("misc_hidelistcolumns") = ""
if session("misctrans") <> "" then
  a = "" : bb = session("misctrans")
  do until bb = ""
    b = left(bb,instr(bb,",")-1) : bb = mid(bb,instr(bb,",")+1)
    v = left(bb,instr(bb,",")-1) : bb = mid(bb,instr(bb,",")+1)
    a = a & vbcrlf & "~misc_" & b & "~" & v
  loop
  trans = a & trans
end if
']
's = "create table misc (id autoincrement primary key,title text(100),num1 number,date1 datetime,contents memo,file1 text(100),type1 text(50))"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "misc" : session("tablename") = "misc"
tablefields = "id,title,num1,date1,contents,file1,type1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
'[admin_top = true : admin_login = true
admin_top = true : admin_login = true
if left(session("miscview"),4) = "car_" then emptytop = "on"
if left(session("miscview"),7) = "worker_" then emptytop = "on"
if left(session("miscview"),7) = "course_" then emptytop = "on"
']
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 4
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = miscinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("num1") then s = s & "num1=0" & getfield("num1") & ","
  if okedit("date1") then s = s & "date1=" & sqldate(getfield("date1")) & ","
  if okedit("contents") then s = s & "contents='" & getfield("contents") & "',"
  if okedit("file1") then s = s & "file1='" & getfield("file1") & "',"
  if okedit("type1") then s = s & "type1='" & getfield("type1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = miscdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = miscdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~misc_title~^"
  if oksee("num1") then csv(0) = csv(0) & "~misc_num1~^"
  if oksee("date1") then csv(0) = csv(0) & "~misc_date1~^"
  if oksee("contents") then csv(0) = csv(0) & "~misc_contents~^"
  if oksee("file1") then csv(0) = csv(0) & "~misc_file1~^"
  if oksee("type1") then csv(0) = csv(0) & "~misc_type1~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thenum1 = rs("num1") : if isnull(Thenum1) then Thenum1 = 0
  Thenum1 = replace(thenum1,",",".") : Thenum1 = formatnumber(thenum1,2,true,false,false)
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Thecontents = rs("contents") : if isnull(Thecontents) then Thecontents = ""
  Thefile1 = rs("file1") : if isnull(Thefile1) then Thefile1 = ""
  Thetype1 = rs("type1") : if isnull(Thetype1) then Thetype1 = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("num1") then csv(rc) = csv(rc) & replace(thenum1,vbcrlf," ") & "^"
    t = "" : if isdate(thedate1) then if year(thedate1) <> 1900 then t = thedate1
    if oksee("date1") then csv(rc) = csv(rc) & t & "^"
    if oksee("contents") then csv(rc) = csv(rc) & replace(thecontents,vbcrlf," ") & "^"
    if oksee("file1") then csv(rc) = csv(rc) & replace(thefile1,vbcrlf," ") & "^"
    if oksee("type1") then csv(rc) = csv(rc) & replace(thetype1,vbcrlf," ") & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
'[    if countstring(a, ",") > 2 and t <> "" then
    if right(a,1) <> "," then a = a & ","
    if t <> "" then
'[      
      s = "" : thisid = "0"
      for i = 1 to countstring(a,",")

        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "title" then
          if len(b) > 100 then b = left(b,100)
          if okedit("title") then s = s & "title='" & b & "',"
        elseif csvff(i) = "num1" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("num1") then s = s & "num1=" & b & ","
        elseif csvff(i) = "date1" then
          if not isdate(b) then b = "1/1/1900"
          if okedit("date1") then s = s & "date1=" & sqldate(b) & ","
        elseif csvff(i) = "contents" then
          if okedit("contents") then s = s & "contents='" & b & "',"
        elseif csvff(i) = "file1" then
          if len(b) > 100 then b = left(b,100)
          if okedit("file1") then s = s & "file1='" & b & "',"
        elseif csvff(i) = "type1" then
          if len(b) > 50 then b = left(b,50)
          if okedit("type1") then s = s & "type1='" & b & "',"
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = miscinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","misc.id,misc.title,misc.num1,misc.date1,misc.contents,misc.file1,misc.type1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thenum1 = rs("num1") : if isnull(Thenum1) then Thenum1 = 0
  Thenum1 = replace(thenum1,",",".") : Thenum1 = formatnumber(thenum1,2,true,false,false)
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Thecontents = rs("contents") : if isnull(Thecontents) then Thecontents = ""
  Thecontents = replace(thecontents,"<br>",vbcrlf)
  Thefile1 = rs("file1") : if isnull(Thefile1) then Thefile1 = ""
  Thetype1 = rs("type1") : if isnull(Thetype1) then Thetype1 = ""
  formcontrols = "title,num1,date1,date1_day,date1_month,date1_year,date1_hour,date1_insertdate,date1_removedate,contents,file1,type1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
'[  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_v.gif' ~iconsize~> ~misc_misc~ - ~Edit~")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_v.gif' ~iconsize~> " & session("miscdisplay") & " - ~Edit~")
  if specs = "elbit" and session("miscview") = "wbs" and session("userlevel") <> "5" then disablecontrols = disablecontrols & "contents,"
  if session("miscview") = "carpool" and session("userlevel") <> "5" then disablecontrols = disablecontrols & "contents,"
  if session("miscview") = "carpoolboard" and session("userlevel") <> "5" then disablecontrols = disablecontrols & "contents,"
']
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~misc_title~</span><td class=form_item2 id=title_tr2>")
'[    rr("<input type=text maxlength=100 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
    if instr(session("miscview"), "worker_remarks") > 0 and not hasmatch(session("escortworkerids"), mid(session("miscview"),15)) then
      ii = getfieldlist("select title from misc where type1 = 'studentremarks'") & tocsv(thetitle) & ","
      ii = uniquelist(ii) : ii = sortlist(ii, "text")
      rr "<select name=title><option value=''>"
      do until ii = ""
        i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
        rr "<option value='" & i & "'" : if thetitle = i then rr " selected"
        rr ">" & i
      loop
      rr "</select>"
    elseif instr(session("miscview"), "worker_remarks") > 0 and hasmatch(session("escortworkerids"), mid(session("miscview"),15)) then
      ii = getfieldlist("select title from misc where type1 = 'escortremarks'") & tocsv(thetitle) & ","
      ii = uniquelist(ii) : ii = sortlist(ii, "text")
      rr "<select name=title><option value=''>"
      do until ii = ""
        i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
        rr "<option value='" & i & "'" : if thetitle = i then rr " selected"
        rr ">" & i
      loop
      rr "</select>"
    else
      rr("<input type=text maxlength=100 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
    end if

']
  end if'form:title

  if oksee("num1") then'form:num1
    rr("<tr valign=top id=num1_tr0><td class=form_item1 id=num1_tr1><span id=num1_caption>~misc_num1~<img src=must.gif></span><td class=form_item2 id=num1_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=num1 value=""" & Thenum1 & """>")
  end if'form:num1

  if oksee("date1") then'form:date1
    rr("<tr valign=top id=date1_tr0><td class=form_item1 id=date1_tr1><span id=date1_caption>~misc_date1~</span><td class=form_item2 id=date1_tr2>")
'[    rr(SelectDate("f1.date1:", thedate1))
    t = "" : if session("misc_date1withhour") = "on" then t = ":"
    rr(SelectDate("f1.date1" & t, thedate1))
']
  end if'form:date1

  if oksee("contents") then'form:contents
    rr("<tr valign=top id=contents_tr0><td class=form_item1 id=contents_tr1><span id=contents_caption>~misc_contents~</span><td class=form_item2 id=contents_tr2>")
'[    rr("<textarea name=contents style='width:301; height:100;' dir=~langdir~>" & thecontents & "</textarea>")

    if left(session("miscview"), 8) = "car_fuel" then
      rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=contents value=""" & Thecontents & """>")

    elseif session("miscview") = "disability" then
      'rr "<input type=checkbox name=contents" : if thecontents = "on" then rr " checked"
      'rr ">"
      rr "<input type=hidden name=contents value='" & thecontents & "'>"
	    rr "<input type=checkbox id=needescort name=needescort"
	    if getval(thecontents, "needescort") = "on" then rr " checked"
	    rr " onclick='vbscript: if document.getelementbyid(""needescort"").checked then f1.contents.value = setval(f1.contents.value, ""needescort"", ""on"") : else f1.contents.value = setval(f1.contents.value, ""needescort"", """")'"
	    rr ">"
      rr("<tr valign=top><td class=form_item1><span>��� ����� ��</span><td class=form_item2>")

      'rr "<input type=text value='" & getval(thecontents, "foes") & "' style='width:105;' onchange='vbscript: f1.contents.value = setval(f1.contents.value, ""foes"", me.value)'><br>"
	    t = ":select id,title from misc where type1 = 'disability' and id <> " & theid & " order by title"
	    rr(selectlookupmultiauto("f1.foes", getval(thecontents, "foes"), "icons/i_x.gif", "misc,title," & t, 300, okwritehere))

    elseif specs = "iec" or session("miscview") = "neighborhood" then
      rr "<input type=hidden name=contents value='" & thecontents & "'>"
	    rr("<select name=siteid onchange='vbscript: t = document.getelementbyid(""siteid"").options(document.getelementbyid(""siteid"").selectedindex).value : f1.contents.value = setval(f1.contents.value, ""siteid"", t)'>")
	    rr("<option value=0 style='color:bbbbfe;'>~orders_siteid~")
	    sq = "select * from sites order by title"
	    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
	    do until rs1.eof
	      rr("<option value=""" & rs1("id") & """")
	      if getval(thecontents, "siteid") = cstr(rs1("id")) then rr(" selected")
	      rr(">")
	      rr(rs1("title") & " ")
	      rs1.movenext
	    loop
	    rr("</select>")
      if specs = "iec" then
	      rr("<tr valign=top><td class=form_item1>�����<td class=form_item2>")
	      rr "<input type=text value='" & getval(thecontents, "description") & "' style='width:300;' onchange='vbscript: f1.contents.value = setval(f1.contents.value, ""description"", me.value)'><br>"
      end if
    else
      rr("<textarea name=contents style='width:601; height:300;' dir=~langdir~>" & thecontents & "</textarea>")
    end if
']
  end if'form:contents

  if oksee("file1") then'form:file1
    rr("<tr valign=top id=file1_tr0><td class=form_item1 id=file1_tr1><span id=file1_caption>~misc_file1~</span><td class=form_item2 id=file1_tr2>")
    rr("<table border=0 cellpadding=0 cellspacing=0 align=~langside2~><tr><td><div id=file1_preview>")
    if lcase(right(thefile1,4)) = ".jpg" or lcase(right(thefile1,4)) = ".gif" or lcase(right(thefile1,4)) = ".png" then
      rr("<a href='" & thefile1 & "' target=_blank><img src='" & thefile1 & "' width=200 border=0></a>")
    else
      rr("<a href='" & thefile1 & "' target=_blank><font style='font-size:18;'><b>" & ucase(mid(thefile1,instrrev(thefile1,".")+1)) & "</a>")
    end if
    rr("</div></table>")
    if okwritehere then rr("<nobr><table align=~langside~><tr><td><iframe id=file1_upload frameborder=0 marginwidth=0 marginheight=0 width=77 height=32 scrolling=no src=upload.asp?box=file1&previewdiv=file1_preview></iframe></table>")
    rr("<input type=text maxlength=100 id=file1 name=file1 style='width:200;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' value=""" & Thefile1 & """ dir=ltr><br>")
  end if'form:file1

  if oksee("type1") then'form:type1
    rr("<tr valign=top id=type1_tr0><td class=form_item1 id=type1_tr1><span id=type1_caption>~misc_type1~</span><td class=form_item2 id=type1_tr2>")
    rr("<input type=text maxlength=50 name=type1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetype1 & """ dir=~langdir~>")
  end if'form:type1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
'[  rr("    ok = true" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if session("miscview") = "disability" then rr(" f1.contents.value = setval(f1.contents.value, ""foes"", f1.foes.value)" & vbcrlf)
']
  if okedit("num1") and session(tablename & "containerfield") <> "num1" then
    rr("    if isnumeric(f1.num1.value) then" & vbcrlf)
    rr("      if cdbl(f1.num1.value) >= -4000000000 and cdbl(f1.num1.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""num1"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""num1"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""num1"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")
'-------------monthview----------------------------------------
elseif session(tablename & "_monthview") = "on" then
  if getfield("m") <> "" then session(tablename & "_monthview_m") = getfield("m")
  if getfield("y") <> "" then session(tablename & "_monthview_y") = getfield("y")
  if getfield("chosen") <> "" then session(tablename & "_monthview_chosen") = getfield("chosen")
  if session(tablename & "_monthview_chosen") = "" then session(tablename & "_monthview_chosen") = date
  rr("<table width=80% cellspacing=5 align=center>")
  rr("<form method=post action=? name=f2>")
  rr("<input type=hidden name=action value=search>")
  rr("<tr><td class=list_name colspan=99>")
  rr(" <img src=icons/i_v.gif ~iconsize~> ~misc_misc~ - ~Month view~ ")
  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & "&date1=" & session(tablename & "_monthview_chosen") & """'> ")
  m = session(tablename & "_monthview_m")
  y = session(tablename & "_monthview_y")
  if m = "" then m = month(date)
  if y = "" then y = year(date)
  rr(" <SELECT NAME=m>")
  for i = 1 to 12
    rr("<option value=" & i)
    if cstr(m) = cstr(i) then rr(" selected")
    rr(">" & i)
  next
  rr("</SELECT>")
  rr("<SELECT NAME=y>")
  for i = year(now) - 20 to year(now) + 20
    rr("<option value=" & i)
    if cstr(y) = cstr(i) then rr(" selected")
    rr(">" & i)
  next
  rr("</SELECT>")
  rr(" <input type=submit class=groovybutton value=~Show~>")
  prevy = y : prevm = m - 1 : if prevm < 1 then prevm = 12 : prevy = prevy - 1
  nexty = y : nextm = m + 1 : if nextm > 12 then nextm = 1 : nexty = nexty + 1
  rr(" <a href=?y=" & prevy & "&m=" & prevm & "><img src=~langside~arrow.png ~iconsize~ border=0></a>")
  rr(" <a href=?y=" & nexty & "&m=" & nextm & "><img src=~langside2~arrow.png ~iconsize~ border=0></a>")
  rr(" &nbsp;&nbsp;&nbsp;&nbsp;<a href=?monthview=no>~Switch to list view~</a>")
  rr("<table></form></table>")
  '-------------showmonthview-------------
  rr("<tr><td class=monthdaytitle>~Sunday~<td class=monthdaytitle>~Monday~<td class=monthdaytitle>~Tuesday~<td class=monthdaytitle>~Wednesday~<td class=monthdaytitle>~Thursday~<td class=monthdaytitle>~Friday~<td class=monthdaytitle>~Saturday~")
  d = weekday(1 & "/" & m & "/" & y)
  rr("<tr>") : for i = 1 to d - 1: rr("<td class=monthempty>") : next
  for i = 1 to cint(lastday(m & "/" & y))
    d = i & "/" & m & "/" & y
    wd = weekday(d)
    if wd = 1 then rr("<tr>")
    t = "" : if wd = 6 or wd = 7 then t = "weekend"
    if cdate(d) = cdate(session(tablename & "_monthview_chosen")) then t = t & "chosen"
    rr("<td class=monthday" & t & " onclick='vbscript: document.location = ""?chosen=" & d & """' style='cursor:hand;'>")
    rr("<div style='background:eeeeee;'><font style='font-size:18; font-weight:bold; color:404040;'>" & i & "</font>")
    if cdate(d) = date then rr(" <font style='font-size:15; color:ff8000;'>(~Today~)</font>")
    rr("</div>")
    s = "select * from " & tablename
'[    s = s & okreadsql(tablename)'onecell
    s = s & okreadsql(tablename)'onecell
    if session("miscview") <> "" then s = s & " and type1 = '" & session("miscview") & "'"
']
    s = s & " and date1 >= " & sqldate(d & " 00:00:00") & " and date1 <= " & sqldate(d & " 23:59:59")
    s = replace(s," and "," where ",1,1,1)
    s = replace(s,"(" & tablename & ")",tablename,1,1,1)
    closers(rs) : set rs = conn.execute(sqlfilter(s))
    rc = 0
    do until rs.eof
      rc = rc + 1
      rr "<div id=d" & rs("id") & " style='position:absolute; visibility:hidden;'></div>"
      rr "<a href=?action=form&id=" & rs("id") & "><font style='white-space: nowrap;'>"
'[      rr "<img src=icons/i_v.gif border=0> " & rs("id") & " " & rs("title") & "<br>"
      rr "<img src=icons/i_v.gif border=0> " & rs("title") & "<br>"
']
      rr "</a>"
      rs.movenext
    loop
  next
  for i = wd + 1 to 7: rr("<td class=monthempty>") : next
  rr("</table>")
  x = disablecontrolsnow("",disablecontrols,hidecontrols)

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
'[    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = ""
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date1 desc" : session(tablename & "_hidelistcolumns") = ""
']      
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
'[  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/i_v.gif' ~iconsize~> ~misc_misc~ " & t2)
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/i_v.gif' ~iconsize~> " & session("miscdisplay") & " " & t2)
']
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")

  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  if okedit("buttonswitchmonth") then rr(" &nbsp;&nbsp;&nbsp;&nbsp;<a href=?monthview=on>~Switch to month view~</a>")
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"

'[  '-sqladd
  if session("miscview") <> "" then s = s & " and type1 = '" & session("miscview") & "'"
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","misc.id,misc.title,misc.num1,misc.date1,misc.contents,misc.file1,misc.type1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql


'[  if session(tablename & "_string") <> "" then
'[    s = s & " and ("
'[    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
'[    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
  v = session(tablename & "_string")
  if left(v, 5) = "num1_" and isnumeric(mid(v,6)) then
    s = s & " and misc.num1 = " & mid(v,6)
    v = ""
  end if
  if v <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-<>{};':""|\/?.,<>", "                              ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789���������������������������= ")
']

    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".num1 = " & w & " or"
        s = s & " " & tablename & ".contents like '%" & w & "%' or"
        s = s & " " & tablename & ".file1 like '%" & w & "%' or"
        s = s & " " & tablename & ".type1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------
'[  if oksee("num1") then
'[    s = session(tablename & "_lastlistsql") : s = left(s,instrrev(s," order by"))
'[    s = "select sum(" & tablename & ".num1) as sumnum1 " & mid(s,instr(s," from "))
'[    set rs1 = conn.execute(sqlfilter(s))
'[    thesum_num1 = rs1("sumnum1") : if isnull(thesum_num1) then thesum_num1 = 0
'[    thesum_num1 = formatnumber(thesum_num1,2,true,false,true)
'[  end if

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("title") then f = "misc.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~misc_title~</a> " & a2)
  if oksee("num1") then f = "misc.num1" : f2 = "num1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("num1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~misc_num1~</a> " & a2)
  if oksee("date1") then f = "misc.date1" : f2 = "date1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("date1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~misc_date1~</a> " & a2)
  if oksee("contents") then rr("<td class=list_title><nobr>~misc_contents~")
  if oksee("file1") then f = "misc.file1" : f2 = "file1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("file1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~misc_file1~</a> " & a2)
  if oksee("type1") then f = "misc.type1" : f2 = "type1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("type1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~misc_type1~</a> " & a2)

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Thenum1 = rs("num1") : if isnull(Thenum1) then Thenum1 = 0
      Thenum1 = replace(thenum1,",",".") : Thenum1 = formatnumber(thenum1,2,true,false,false)
      Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
      Thedate1 = replace(thedate1,".","/")
      Thecontents = rs("contents") : if isnull(Thecontents) then Thecontents = ""
      Thefile1 = rs("file1") : if isnull(Thefile1) then Thefile1 = ""
      Thetype1 = rs("type1") : if isnull(Thetype1) then Thetype1 = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/i_v.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
'[      if oksee("num1") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(thenum1,2,true,false,true))
      if oksee("num1") then
        t = fornum(thenum1)
        if hasmatch(session("miscview"), "produceduntil,driverevent,") then t = formatnumber(t, 0, true, false, false)
        rr "<td class=list_item valign=top align=right dir=ltr><nobr>" & t
      end if
']
'[      if oksee("date1") then rr("<td class=list_item valign=top align=><nobr>") : if year(thedate1) <> 1900 then rr(thedate1)
		  if oksee("date1") then
		    rr("<td class=list_item>")
		    t = thedate1
		    if left(session("miscview"), 10) = "car_ituran" then t = month(t) & "/" & year(t)
		    if year(thedate1) <> 1900 then rr(t)
		  end if
']

'[      thecontents = thecontents : if isnull(thecontents) then thecontents = ""
'[      thecontents = replace(thecontents,"<br>"," ") : thecontents = replace(thecontents,"<","[") : thecontents = replace(thecontents,">","]") : if len(thecontents) > 30 then thecontents = left(thecontents,30) & "..."
'[      if oksee("contents") then rr("<td class=list_item valign=top align=><nobr>" & thecontents)
      thecontents = replace(thecontents & "","<br>"," ") : thecontents = replace(thecontents,"<","[") : thecontents = replace(thecontents,">","]")
      if session("miscview") = "neighborhood" then thecontents = gettitle("sites", getval(thecontents, "siteid"))
      if oksee("contents") then rr("<td class=list_item valign=top align=><nobr>" & thecontents)
']
      if oksee("file1") then
        rr("<td class=list_item valign=top align=><nobr>")
        p = thefile1 : p1 = p : if instr(p1,"/") > 0 then p1 = mid(p1,instr(p1,"/")+1)
        rr("<a href='" & thefile1 & "' target=_blank>")
        if lcase(right(p,4)) = ".jpg" or lcase(right(p,4)) = ".gif" or lcase(right(p,4)) = ".png" then rr("<img src=""" & p & """ style='width:100; border:1px solid #000000;'></a>") else rr(p1 & "</a>")
      end if
      if oksee("type1") then rr("<td class=list_item valign=top align=><nobr>" & thetype1)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
'[  rr("<tr id=tr0_totals>")
'[  rr("<td class=list_total1><b>~Total~</b>")
'[  if oksee("num1") then rr("<td class=list_total2 dir=ltr align=right>" & thesum_num1)
'[  if oksee("date1") and session(tablename & "containerfield") <> "date1" then rr("<td class=list_total1>") 'date1
'[  if oksee("contents") and session(tablename & "containerfield") <> "contents" then rr("<td class=list_total1>") 'contents
'[  if oksee("file1") and session(tablename & "containerfield") <> "file1" then rr("<td class=list_total1>") 'file1
'[  if oksee("type1") and session(tablename & "containerfield") <> "type1" then rr("<td class=list_total1>") 'type1
']

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
