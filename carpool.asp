<!--#include file="include.asp"-->
<%
request_act = getfield("act")
if getfield("mode") <> "" then session("carpool_mode") = getfield("mode")
if session("carpool_mode") = "mob" then
  %><!--#include file="mob_top.asp"--><%
else
  admin_login = true : admin_top = true ': emptytop = "on"
  %><!--#include file="top.asp"--><%
end if

tablename = "carpool"
currentuserid = session("userid") : if currentuserid = "" then currentuserid = "-" & session("workerid")
currentname = getuserorworker(currentuserid)
currentphone = getuserphone(currentuserid)
currentemail = getuseremail(currentuserid)

if currentuserid = "" then rr "access denied" : rend
'------------------start-----------------------
if session("carpool_mode") = "mob" then
  rr "<div class=z-depth-2 style='background:#ffffff; width:90%;'><table cellpadding=7 style='font-size:30;'>"
  rr "<div style='background:#eeeeee; border-bottom:1px solid #dddddd; padding:20; text-align:~langside~; color:#808080;'>~Carpool~</div>"
else
  rr "<center><br><br><table class=form_table cellpadding=7 width=1020 style='font-size:14;'>"
  rr "<tr><td colspan=9 bgcolor=#dddddd><center><b>~Carpool~</b>"
end if
if hasmatch(request_act, "find,insert,board,") then 
  if getfield("d_hour") = "" then du = showerror("��� ���� ���") : rr "<center><br><a href=carpool.asp>~Back~</a>" : rend
  For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
  an = session("carpool_an") : bn = session("carpool_bn")
  ax = "" : ay = "" : t = GetAddressXY(an) : if instr(t, ",") > 0 then ax = left(t, instr(t,",")-1) : ay = mid(t, instr(t,",")+1)
  bx = "" : by = "" : t = GetAddressXY(bn) : if instr(t, ",") > 0 then bx = left(t, instr(t,",")-1) : by = mid(t, instr(t,",")+1)
  session("carpool_ax") = ax
  session("carpool_ay") = ay
  session("carpool_bx") = bx
  session("carpool_by") = by
  session("carpool_d") = getfield("d")
  session("carpool_d2") = getfield("d2")
end if


'------insert-----------------
if request_act = "insert" or request_act = "board" then
  type1 = "carpool" : if request_act = "board" then type1 = "carpoolboard"
  rr "<tr><td colspan=9>"
  if session("carpool_an") = "" or session("carpool_bn") = "" or session("carpool_ax") = "" or session("carpool_bx") = "" then du = showerror("~Missing From/To~") : rr "<center><br><a href=carpool.asp>~Back~</a>" : rend
  if cdate(session("carpool_d")) < now then du = showerror("~Time passed~") : rr "<center><br><a href=carpool.asp>~Back~</a>" : rend

  aaa = ""
  aaa = setval(aaa, "an", tocsv(session("carpool_an")))
  aaa = setval(aaa, "bn", tocsv(session("carpool_bn")))
  aaa = setval(aaa, "ax", tocsv(session("carpool_ax")))
  aaa = setval(aaa, "ay", tocsv(session("carpool_ay")))
  aaa = setval(aaa, "bx", tocsv(session("carpool_bx")))
  aaa = setval(aaa, "by", tocsv(session("carpool_by")))
  aaa = setval(aaa, "via", tocsv(session("carpool_via")))
  aaa = setval(aaa, "rem", tocsv(session("carpool_rem")))
  aaa = setval(aaa, "pa", tocsv(session("carpool_pa")))'passengers count (pi = workerids)
  aaa = setval(aaa, "ui", tocsv(currentuserid))
  if type1 = "carpoolboard" then
    aaa = setval(aaa, "mm", tocsv(session("carpool_maxminutes")))
    aaa = setval(aaa, "mk", tocsv(session("carpool_maxkm")))
  end if
  d1 = getdatedatestring(session("carpool_d")) : d2 = session("carpool_d2") : if not isdate(d2) then d2 = d1
  h = getdatehourstring(session("carpool_d"))
  if cdate(d2) < cdate(d1) then du = showerror("~Invalid~ ~Date~") : rr "<center><br><a href=carpool.asp>~Back~</a>" : rend
  did = 0
  do
    if cdate(d1) > cdate(d2) then exit do
    if getfield("selectweekdays") = "" or getfield("wd" & getweekday(d1)) = "on" then
		  f = "" : v = ""
		  f = f & "title," : v = v & "'',"
		  f = f & "date1," : v = v & sqldate(d1 & " " & h) & ","
		  f = f & "contents," : v = v & "'" & aaa & "',"
		  f = f & "type1," : v = v & "'" & type1 & "',"
		  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
		  s = "insert into misc(" & f & ") values(" & v & ")"
		  closers(rs) : set rs = conn.execute(sqlfilter(s))
      did = did + 1
		  '---check for hitchhikers
		  if specs = "mizrahi" and type1 = "carpool" then
			  s = "select id,date1,contents,type1 from misc where type1 = 'carpoolboard'"
			  s = s & " and date1 >= " & sqldate(dateadd("d", -1 , date))
			  du = showlist(s, 999999992, 999999992)
			  a = showlistuis
			  do until a = ""
			    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
			    if instr(b,"-") > 0 then e = getworkeremail(replace(b,"-","")) else e = getuseremail(b)
			    subject = "Moovex - Carpool - ������� ����� ������� ��"
			    body = ""
			    body = body & " ���� ��,<br>"
			    body = body & " " & currentname
			    body = body & " ���� ����� " & currentphone
			    body = body & " ���� " & currentemail & "<br>"
			    body = body & " <b>����</b> ������ " & d1 & " " & h
			    body = body & " �" & session("carpool_an")
			    body = body & " �" & session("carpool_bn")
	        body = body & "<br><br>����� ���� ������� ������ ������ ���� ���� ����� ������ �� ��� ����� '����-���'"
		      du = sendemail(getparam("siteemail"), e, "", "", subject, body)
		      du = sendsms(getuserphone(currentuserid), getuserphone(b), ctxt(body))
			  loop
		  end if
    end if
    d1 = dateadd("d", 1, d1)
  loop
  if did = 0 then
    rr "<br><br><center><b>�� ��� ������� ������� ���� ����� �����!</b>"
  else
    rr "<br><br><center><b>~Your request has been submitted~!</b>"
  end if
  rr "<br><br><center><a href=?>~Back~</a>"
  rend
end if

'-------------------delete
if request_act = "delete" then
  rr "<tr><td colspan=9>"
  a = getonefield("select contents from misc where id = " & getfield("id"))
  date1 = getonefield("select date1 from misc where id = " & getfield("id"))
  ui = getval(a, "ui")
  pi = getval(a, "pi")
  pp = pi
  do until pp = ""
    p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
    e1 = getuseremail(ui) : if not isemail(e1) then e = e & "~Driver`s email is not defined in the system~<br>"
    e2 = getuseremail(p) : if not isemail(e2) then e = e & "~Your email is not defined in the system~<br>"
    subject = "Moovex - Carpool"
    body = getuserorworker(ui) & " ~Canceled his ride on~ " & date1

	  if specs = "mizrahi" then
	    subject = "Moovex - Carpool - ����� �����"
	    body = ""
	    body = body & " ���� ��,<br>"
	    body = body & " " & currentname
	    body = body & " ���� ����� " & currentphone
	    body = body & " ���� " & currentemail & "<br>"
	    body = body & " <b>���� �� ������</b> ������ " & date1
	    body = body & " �" & getval(a, "an")
	    body = body & " �" & getval(a, "bn")
	  end if

    du = sendemail(e1, e2, "", "", subject, body)
    if specs = "mizrahi" then du = sendsms(getuserphone(ui), getuserphone(p), ctxt(body))
  loop
  if currentuserid = ui then
    s = "delete * from misc where id = " & getfield("id")
    set rs = conn.execute(sqlfilter(s))
    du = showerror("~Item Deleted~")
  end if
end if

'-------------------enroll
if request_act = "enroll" then
  rr "<tr><td colspan=9>"

  s = "select date1,contents from misc where id = " & getfield("id")
  set rs = conn.execute(sqlfilter(s))
  a = rs("contents")
  pi = getval(a, "pi") & currentuserid & "," : pi = uniquelist(pi)
  a = setval(a, "pi", pi)
  ui = getval(a, "ui")
  date1 = rs("date1")
  set rs = conn.execute(sqlfilter("update misc set contents = '" & a & "' where id = " & getfield("id")))

  e1 = getuseremail(currentuserid) : if not isemail(e1) then e = e & "~Your email is not defined in the system~<br>"
  e2 = getuseremail(ui) : if not isemail(e2) then e = e & "~Driver`s email is not defined in the system~<br>"
  subject = "Moovex - Carpool"
  body = currentname & " ~Registered on your ride at~ " & date1 & "<br>"
  body = body & "~Phone~: " & currentphone & "<br>"
	body = body & "~Email~: " & currentemail & "<br>"

  if specs = "mizrahi" then
    subject = "Moovex - Carpool - ����� �������"
    body = ""
    body = body & " ���� ��,<br>"
    body = body & " " & currentname
    body = body & " ���� ����� " & currentphone
    body = body & " ���� " & currentemail & "<br>"
    body = body & " ���� ������ ������ " & date1
    body = body & " �" & getval(a, "an")
    body = body & " �" & getval(a, "bn")
    body = body & " <br><b>�� ��� ������� ������ �����!</b><br>"
  end if

  du = sendemail(e1, e2, "", "", subject, body)
  if specs = "mizrahi" then du = sendsms(getuserphone(currentuserid), getuserphone(ui), ctxt(body))
  du = showerror("~You have been enrolled successfuly~")
  rr "<br><br><center><a href=?act=find>~Back~</a>"
  rend
end if

if request_act = "unroll" then
  rr "<tr><td colspan=9>"
  s = "select date1,contents from misc where id = " & getfield("id")
  set rs = conn.execute(sqlfilter(s))
  a = rs("contents")
  pi = getval(a, "pi") : pi = nomatch(pi, currentuserid & ",")
  a = setval(a, "pi", pi)
  ui = getval(a, "ui")
  date1 = rs("date1")
  set rs = conn.execute(sqlfilter("update misc set contents = '" & a & "' where id = " & getfield("id")))
  e1 = getuseremail(currentuserid) : if not isemail(e1) then e = e & "~Your email is not defined in the system~<br>"
  e2 = getuseremail(ui) : if not isemail(e2) then e = e & "~Driver`s email is not defined in the system~<br>"
  subject = "Moovex - Carpool"
  body = currentname & " ~Canceled registration from your ride on~ " & date1
  if specs = "mizrahi" then
    subject = "Moovex - Carpool - ����� �������"
    body = ""
    body = body & " ���� ��,<br>"
    body = body & " " & currentname
    body = body & " ���� ����� " & currentphone
    body = body & " ���� " & currentemail & "<br>"
    body = body & " <b>���� �� ��������</b> ������ ������ " & date1
    body = body & " �" & getval(a, "an")
    body = body & " �" & getval(a, "bn")
  end if
  du = sendemail(e1, e2, "", "", subject, body)
  if specs = "mizrahi" then du = sendsms(getuserphone(currentuserid), getuserphone(ui), ctxt(body))
  du = showerror("~You have been removed successfuly~")
  rr "<br><br><center><a href=?act=find>~Back~</a>"
  rend
end if

if request_act = "remove" then
  rr "<tr><td colspan=9>"
  s = "select date1,contents from misc where id = " & getfield("id")
  set rs = conn.execute(sqlfilter(s))
  a = rs("contents")
  pi = getval(a, "pi") : pi = nomatch(pi, getfield("pid") & ",")
  a = setval(a, "pi", pi)
  ui = getval(a, "ui")
  date1 = rs("date1")
  set rs = conn.execute(sqlfilter("update misc set contents = '" & a & "' where id = " & getfield("id")))
  e1 = getuseremail(ui) : if not isemail(e1) then e = e & "~Driver`s email is not defined in the system~<br>"
  e2 = getuseremail(getfield("pid")) : if not isemail(e2) then e = e & "~Your email is not defined in the system~<br>"
  subject = "Moovex - Carpool"
  body = currentname & " ~Canceled your registration to his ride on~ " & date1
  if specs = "mizrahi" then
    subject = "Moovex - Carpool - ����� ����� ���"
    body = ""
    body = body & " ���� ��,<br>"
    body = body & " " & currentname
    body = body & " ���� ����� " & currentphone
    body = body & " ���� " & currentemail & "<br>"
    body = body & " <b>���� �� ������</b> ������ ������ " & date1
    body = body & " �" & getval(a, "an")
    body = body & " �" & getval(a, "bn")
  end if
  du = sendemail(e1, e2, "", "", subject, body)
  if specs = "mizrahi" then du = sendsms(getuserphone(ui), getuserphone(getfield("pid")), ctxt(body))
  du = showerror("~You removed a passenger successfuly. a message had been sent~")
  rr "<br><br><center><a href=?act=find>~Back~</a>"
  rend
end if

'-------------------message
if request_act = "message" then
  rr "<tr><td colspan=9>"
  e = ""
  e1 = currentemail : 'if not isemail(e1) then e = e & "~Your email is not defined in the system~<br>"
  e2 = getuseremail(getfield("to")) : if not isemail(e2) then e = e & "~Driver`s email is not defined in the system~<br>"
  if e <> "" then rr "<center><br><br><span style='color:#ff0000;'><b>" & e & "</b></span><br><a href=carpool.asp>~Back~</a>" : rend
  rr "<center><b>~To ~ " & getuserorworker(getfield("to")) & "</b><br><br>"
  rr "<form name=f1>"
  rr "<input type=hidden name=act value=messagesubmit>"
  rr "<input type=hidden name=to value=" & getfield("to") & ">"
  rr "<input type=hidden name=e1 value=" & e1 & ">"
  rr "<input type=hidden name=e2 value=" & e2 & ">"
  rr "<textarea name=a1 style='width:70%; height:300;'></textarea>"
  rr "<tr><td colspan=9 bgcolor=#dddddd align=~langside2~>"
  rr "<input type=button class=groovybutton value='~Send~' onclick='f1.submit();'>&nbsp;&nbsp;&nbsp;&nbsp;"
  rr "</form>"
  rend
end if
if request_act = "messagesubmit" then
  body = ""
  if specs = "mizrahi" then
    body = body & " ��� " & currentname
    body = body & " ���� ����� " & currentphone
    body = body & " ���� " & currentemail & "<br>"
  end if
  body = body & " " & getfield("a1")
  du = sendemail(getfield("e1"), getfield("e2"), "", "", "Moovex - Carpool - Message from " & currentname, body)
  if specs = "mizrahi" then du = sendsms(currentphone, getuserphone(getfield("to")), ctxt(body))
  rr "<script> alert(""~Sent~""); window.history.go(-2); </script>"
end if

'----------------form---------------------------
rr "<form name=f1>"
rr "<input type=hidden name=act>"
rr "<input type=hidden name=ax value='" & session("carpool_ax") & "'>"
rr "<input type=hidden name=ay value='" & session("carpool_ay") & "'>"
rr "<input type=hidden name=bx value='" & session("carpool_bx") & "'>"
rr "<input type=hidden name=by value='" & session("carpool_by") & "'>"
rr "<tr valign=top><td>~When do you ride~?<td>"
if session("carpool_mode") = "mob" then
  rr "<input type=hidden id=d name=d>"
  rr "<select id=d_day name=d_day class=browser-default style='display:inline-block; width:50%; height:70; background:#fafafa;' onchange=""buildd();"">"
  for i = 0 to 7
    d = dateadd("d", i, date) : rr "<option value='" & d & "'" & iif(getdatedatestring(session("carpool_d")) = cstr(d), " selected", "") & ">" & d & " " & translate("~weekday" & weekday(d) & "~") & "</option>"
  next
  rr "</select>"
  rr " <select id=d_hour name=d_hour class=browser-default style='display:inline-block; width:30%; height:70; background:#fafafa;' onchange=""buildd();"">"
  rr "<option value=''>���"
  for i = 0 to 24 * 60 - 1 step 15
    v = dateadd("n", i, date) : v = getdatehourstring(v)
    rr "<option value='" & v & "'" & iif(session("carpool_d") <> "" and getdatehourstring(session("carpool_d")) = v, " selected", "") & ">" & v & "</option>"
  next
  rr "</select>"
  'rr "<script> function buildd(){f1.d.value = f1.d_day.options[f1.d_day.selectedIndex].value + ' ' + f1.d_hour.value;} buildd();</script>"
  rr "<script> function buildd(){document.getElementById(""d"").value = document.getElementById(""d_day"").value + ' ' + document.getElementById(""d_hour"").value;} buildd();</script>"
else
  a = selectdate("f1.d*:", session("carpool_d"))
  a = replace(a, "<select", "<select style='font-size:15;'", 1, -1, 1)
  rr a
end if

if session("carpool_d") = "" then session("carpool_d") = dateadd("n",-30,now)
if session("carpool_d2") = "" then session("carpool_d2") = getdatedatestring(session("carpool_d"))
if hasmatch(specs, "mizrahi,lubinski,") then
  if cdate(session("carpool_d2")) < cdate(getdatedatestring(session("carpool_d"))) then session("carpool_d2") = session("carpool_d")
  rr "<input type=hidden name=selectweekdays value=on>"
	if session("carpool_mode") = "mob" then
    rr "<tr valign=top><td>�� �����<td>"
	  rr "<select id=d2 name=d2 class=browser-default style='display:inline-block; width:50%; height:70; background:#fafafa;'>"
	  for i = 0 to 31
	    d2 = dateadd("d", i, date) : rr "<option value='" & d2 & "'" & iif(getdatedatestring(session("carpool_d2")) = cstr(d2), " selected", "") & ">" & d2 & " " & translate("~weekday" & weekday(d2) & "~") & "</option>"
	  next
	  rr "</select>"
	  rr "<tr valign=top><td>�� �����<td>"
	  a = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays") : dd = "1,2,3,4,5,"
	  do until a = ""
	    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      t = "" : if hasmatch(dd, b) then t = "on"
      rr "<input type=hidden id=wd" & b & " name=wd" & b & " value=" & t & ">"
	    rr "<span style='width:60px; height:70px; padding:3px; display:inline-block; padding-bottom:15px; padding-right:10px; text-align:~langside~; cursor:hand;'"
      rr " onclick='t = document.getElementById(""v" & b & """).style.visibility; if(t==""hidden"") {document.getElementById(""v" & b & """).style.visibility = ""visible""; document.getElementById(""wd" & b & """).value = ""on"";} else {document.getElementById(""v" & b & """).style.visibility = ""hidden""; document.getElementById(""wd" & b & """).value = ""no"";}'"
      rr ">"
      t = "hidden" : if hasmatch(dd, b) then t = "visible"
	    rr "<img id=v" & b & " src=navvgray.png width=40px height=40px style='z-index:100; position:absolute; visibility:" & t & "; margin:5px;'>"
	    rr "<div style='display:inline-block; position:absolute; border:1px solid #cccccc; width:50px; height:50px;'></div>"
      rr "</span>"
      rr "<span style='position:relative; top:-30; right:10;'>~weekday" & b & "~</span> "
    loop
	else
    rr "<tr valign=top><td>�� �����<td>"
	  a = selectdate("f1.d2*", session("carpool_d2"))
	  a = replace(a, "<select", "<select style='font-size:15;'", 1, -1, 1)
    rr "<img src=blank.gif width=13 height=1>"
	  rr a
	  rr "<tr valign=top><td>~Days~<td>"
	  a = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
	  do until a = ""
	    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
	    t = "" : t = " checked "
	    rr "<input type=checkbox name=wd" & b & t & ">~weekday" & b & "~ &nbsp;"
	  loop
	end if
end if

if specs = "fibi" and session("carpool_an") = "" then session("carpool_an") = "���� ���� 5 ����� �����"

aa = "" : bb = ""
if specs = "lubinski" then
  sq = "select contents from misc where type1 in('carpool','carpoolboard') and instr(cstr(contents), '|ui=" & currentuserid & "|') > 0"
  set rs1 = conn.execute(sqlfilter(sq))
  aa = "" : bb = ""
  do until rs1.eof
    a = rs1("contents")
    t = getval(a, "an") : t = replace(t, ",", " ") : t = replace(t, "'", "") : if not hasmatch(aa, t) then aa = aa & t & ","
    t = getval(a, "bn") : t = replace(t, ",", " ") : t = replace(t, "'", "") : if not hasmatch(bb, t) then bb = bb & t & ","
    rs1.movenext
  loop
  aa = sortlist(aa, "text") : bb = sortlist(bb, "text")
end if
if aa <> "" then
	rr "<tr valign=top><td>~Where is your start point~?<td>"
  if session("carpool_mode") = "mob" then rr "<a href=#du style='position:absolute; margin-top:80;' onclick='var t = f1.an.value; f1.an.value = f1.bn.value; f1.bn.value = t;'><img src=updown.png border=0></a>"
  rr "<select id=an_select name=d_day class=browser-default style='display:inline-block; width:45%; height:70; background:#fafafa;'"
  rr " onchange='document.getElementById(""an"").value = document.getElementById(""an_select"").value; document.getElementById(""an_select"").selectedIndex = 0;'"
  rr "><option>"
  do until aa = ""
    t = left(aa,instr(aa,",")-1) : aa = mid(aa,instr(aa,",")+1)
    rr "<option value='" & t & "'>" & t
  loop
  rr "</select> "
	vv = "hidden" : if getfield("ax") <> "" then vv = "visible"
  t = "class=input1" : if session("carpool_mode") = "mob" then t = "style='font-size:30; background:#fafafa; padding:10; width:45%;'"
	rr "<input type=text " & t & " id=an name=an value='" & session("carpool_an") & "'>"
	rr " &nbsp;&nbsp;&nbsp; <span id=a_good style='position:absolute; visibility:" & vv & "; color:#aaaaaa;'><img src=navv.png></span>"
	rr "<span id=a_bad style='position:absolute; visibility:hidden; color:#aaaaaa;'><img src=navx.png></span>"

	rr "<tr valign=top><td>~Where do you want to go~?<td>"
  rr "<select id=bn_select name=d_day class=browser-default style='display:inline-block; width:45%; height:70; background:#fafafa;'"
  rr " onchange='document.getElementById(""bn"").value = document.getElementById(""bn_select"").value; document.getElementById(""bn_select"").selectedIndex = 0;'"
  rr "><option>"
  do until bb = ""
    t = left(bb,instr(bb,",")-1) : bb = mid(bb,instr(bb,",")+1)
    rr "<option value='" & t & "'>" & t
  loop
  rr "</select> "
	vv = "hidden" : if getfield("ax") <> "" then vv = "visible"
  t = "class=input1" : if session("carpool_mode") = "mob" then t = "style='font-size:30; background:#fafafa; padding:10; width:45%;'"
	rr "<input type=text " & t & " id=bn name=bn value='" & session("carpool_bn") & "'>"
	rr " &nbsp;&nbsp;&nbsp; <span id=b_good style='position:absolute; visibility:" & vv & "; color:#aaaaaa;'><img src=navv.png></span>"
	rr "<span id=b_bad style='position:absolute; visibility:hidden; color:#aaaaaa;'><img src=navx.png></span>"

else
	vv = "hidden" : if getfield("ax") <> "" then vv = "visible"
  t = "class=input1" : if session("carpool_mode") = "mob" then t = "style='font-size:30; background:#fafafa; padding:10; width:80%;'"
	rr "<tr valign=top><td>~Where is your start point~?"
	rr "<td><input type=text " & t & " name=an value='" & session("carpool_an") & "'>"
	rr " &nbsp;&nbsp;&nbsp; <span id=a_good style='position:absolute; visibility:" & vv & "; color:#aaaaaa;'><img src=navv.png></span>"
	rr "<span id=a_bad style='position:absolute; visibility:hidden; color:#aaaaaa;'><img src=navx.png></span>"
	vv = "hidden" : if getfield("bx") <> "" then vv = "visible"
	t = "class=input1" : if session("carpool_mode") = "mob" then t = "style='font-size:30; background:#fafafa; padding:10; width:80%;'"
	rr "<tr valign=top><td>~Where do you want to go~?<td><input type=text " & t & " name=bn value='" & session("carpool_bn") & "'>"
	rr " &nbsp;&nbsp;&nbsp; <span id=b_good style='position:absolute; visibility:" & vv & "; color:#aaaaaa;'><img src=navv.png></span>"
	rr "<span id=b_bad style='position:absolute; visibility:hidden; color:#aaaaaa;'><img src=navx.png></span>"
end if


if specs = "fibi" then
  t = "class=input1" : if session("carpool_mode") = "mob" then t = "style='font-size:30; background:#fafafa; padding:10; width:80%;'"
  rr "<tr valign=top><td>~orders_via1~<td><input type=text " & t & " name=via value='" & session("carpool_via") & "'>"
end if

rr "<tr valign=top><td>~Choose range of search~<td>"

t = "60" : if specs = "lubinski" then t = "10080"
v = session("carpool_maxminutes") : if v = "" then v = t
t = "width:70;" : if session("carpool_mode") = "mob" then t = "font-size:30; height:60; width:120;"
rr " <input type=text style='direction:ltr; border:1px solid #bbbbbb; height:30; padding:7;" & t & "' name=maxminutes value='" & v & "'><span style='color:#aaaaaa;'> ~Minutes~</span>"
rr "&nbsp;&nbsp;&nbsp;&nbsp;"

t = "10" : if specs = "lubinski" then t = "1000"
v = session("carpool_maxkm") : if v = "" then v = t
t = "width:70;" : if session("carpool_mode") = "mob" then t = "font-size:30; height:60; width:120;"
rr " <input type=text style='direction:ltr; border:1px solid #bbbbbb; height:30; padding:7;" & t & "' name=maxkm value='" & v & "'><span style='color:#aaaaaa;'> ~KM~</span>"

if specs = "fibi" then
  t = "class=input1" : if session("carpool_mode") = "mob" then t = "style='font-size:30; background:#fafafa; padding:10; width:80%;'"
  rr "<tr valign=top><td>~Remarks~<td><input type=text " & t & " name=rem value='" & session("carpool_rem") & "'>"
end if

rr "<tr valign=top><td>~Seats Available~<td>"
if session("carpool_mode") = "mob" then
  rr "<select id=pa name=pa class=browser-default style='display:inline-block; width:40%; height:70; background:#fafafa;'>"
else
  rr "<select id=pa name=pa style='display:inline-block; font-size:15;'>"
end if
for i = 1 to 4
  rr "<option value=" & i & iif(i = 4, " selected", "") & ">" & i & "</option>"
next
rr "</select>"
if specs = "lubinski" then rr " &nbsp;&nbsp;&nbsp; <span style='color:#aaaaaa;'>(�� ������ �����)</span>"

rr "<tr valign=top><td colspan=9 align=~langside2~><center>"
'rr "<img src=maps.png style='float:~langside~; top:4; width:30; height:30;'>&nbsp;&nbsp;&nbsp;&nbsp;"
sty = "class=groovybutton"
if session("carpool_mode") = "mob" then
  sty = "class='waves-effect red btn-large' style='height:70; padding-top:4; width:350;'"
  sty2 = "class='waves-effect blue btn-large' style='height:70; padding-top:4; width:350;'"
end if

'rr "<input type=button " & sty & " value='~Show~ ~Map~' onclick='f1.act.value = ""map""; f1.submit();'>&nbsp;&nbsp;&nbsp;&nbsp;"
rr "<input type=button " & sty2 & " value='~Find~ ~Ride~' onclick='f1.act.value = ""find""; f1.submit();'>&nbsp;&nbsp;&nbsp;&nbsp;"
'rr "<input type=button " & sty & " value='~Clear~' onclick='document.location = ""?"";'>&nbsp;&nbsp;&nbsp;&nbsp;"
rr "<input type=button " & sty & " value='~Publish~ ~Ride~' onclick='f1.act.value = ""insert""; f1.submit();'>"
if specs <> "fibi" then rr "<br><br><a href=#du onclick='f1.act.value = ""board""; f1.submit();'>~Leave a ride request~</a>"


if not hasmatch(request_act, "find,insert,board,") then
  s = "select id,date1,contents,type1 from misc where (type1 = 'carpool' or type1 = 'carpoolboard')"
  s = s & " and date1 >= " & sqldate(dateadd("d", -1 , date))
  ui = session("userid") : if session("workerid") <> "" then ui = "-" & session("workerid")
  s = s & " and instr(cstr('' & contents), '|ui=" & ui & "') > 0"
  s = s & " order by type1 desc, date1"
  rr showlist(s, 999999991, 999999991)
end if

'----------------find---------------------------
if request_act = "find" then
  x = 0
  rr "<script>"
  if ax = "" then
    rr "a_good.style.visibility = ""hidden"";" & vbcrlf
    rr "a_bad.style.visibility = ""visible"";" & vbcrlf
    rr "f1.ax.value = """"; f1.ay.value = """";" & vbcrlf
  else
    x = x + 1
    rr "a_good.style.visibility = ""visible"";" & vbcrlf
    rr "a_bad.style.visibility = ""hidden"";" & vbcrlf
    rr "f1.ax.value = """ & ax & """; f1.ay.value = """ & ay & """;" & vbcrlf
  end if
  if bx = "" then
    rr "b_good.style.visibility = ""hidden"";" & vbcrlf
    rr "b_bad.style.visibility = ""visible"";" & vbcrlf
    rr "f1.bx.value = """"; f1.by.value = """";" & vbcrlf
  else
    x = x + 1
    rr "b_good.style.visibility = ""visible"";" & vbcrlf
    rr "b_bad.style.visibility = ""hidden"";" & vbcrlf
    rr "f1.bx.value = """ & bx & """; f1.by.value = """ & by & """;" & vbcrlf
  end if
  rr "</script>"

  if x <> 2 then
    rr "<br><br>" : du = showerror("~Missing From/To~") : rend
  else
    n = session("carpool_maxminutes") : if not isnumeric(n) then n = "60"
    k = session("carpool_maxkm") : if not isnumeric(k) then k = "10"
    s = "select id,date1,contents,type1 from misc where (type1 = 'carpool' or type1 = 'carpoolboard')"
    's = "select id,date1,contents,type1 from misc where (type1 = 'carpool')"
    s = s & " and date1 >= " & sqldate(now)
    s = s & " and date1 >= " & sqldate(dateadd("n", - n, session("carpool_d")))
    s = s & " and date1 <= " & sqldate(dateadd("n", + n, session("carpool_d")))
    s = s & " order by type1 desc, date1"
    rr showlist(s,n,k)

    rr "<tr><td colspan=9>"
    rr "<div id=map_canvas style='width1:100%; height:700; border:1px solid #bbbbbb;'></div><br>" & vbcrlf
    lang = "en" : if session("elang") = "2" then lang = "iw"
    rr "<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&language=" & lang & "'></script>" & vbcrlf
    rr "<script type='text/javascript'>" & vbcrlf
    rr "window.onload = function() {" & vbcrlf
    rr "var mapOptions = {zoom: 7, center: new google.maps.LatLng(31.500000, 35.500000), mapTypeId: google.maps.MapTypeId.ROADMAP};" & vbcrlf
    rr "var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);" & vbcrlf
    rr "var marker = new google.maps.Marker({position: new google.maps.LatLng(" & ay & "," & ax & "), map: map, title:'" & an & "', icon: ""http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|ff80ff|000000""});" & vbcrlf
    rr "var marker = new google.maps.Marker({position: new google.maps.LatLng(" & by & "," & bx & "), map: map, title:'" & bn & "', icon: ""http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=B|ff80ff|000000""});" & vbcrlf
    '---
    rr "var directionsService = new google.maps.DirectionsService;" & vbcrlf
    rr "directionsService.route({" & vbcrlf
    rr "origin: new google.maps.LatLng(" & ay & "," & ax & "),destination: new google.maps.LatLng(" & by & "," & bx & "),optimizeWaypoints: false," & vbcrlf
    rr "travelMode: google.maps.DirectionsTravelMode.DRIVING" & vbcrlf
    rr "}, function(result) {" & vbcrlf
    rr "var directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true, polylineOptions: {strokeColor: '#FF0000', strokeOpacity: 0.5, strokeWeight: 5}});" & vbcrlf
    rr "directionsRenderer.setMap(map);" & vbcrlf
    rr "directionsRenderer.setDirections(result);" & vbcrlf
    rr "});" & vbcrlf
    '---
    rr "}" & vbcrlf
    rr "</script>" & vbcrlf
    'rr calcdist(ay, ax, by, bx)
  end if

end if

rr "</form></table>"

'-----------------------------------
dim showlistids, showlistuis
function showlist(byval s, byval n, byval k)
  dim rs, c, td, lasttype1, didtitlecarpool, didtitlecarpoolboard, t, orign, origk, a, b, dist1, dist2, dategap, firstcol, x, sty, r
  showlistids = "" : showlistuis = ""
  set rs = conn.execute(sqlfilter(s))
  c = 0
  r = r & "<tr><td colspan=9>"
  td = "<td style='border-bottom:1px solid #cccccc; padding:10;'><nobr>" : if session("carpool_mode") = "mob" then td = "<td style='border-bottom:1px solid #cccccc; padding:5; height:80; padding-top:10;'>"
  lasttype1 = "" : didtitlecarpool = "" : didtitlecarpoolboard = ""
  t = "" : if session("carpool_mode") = "mob" then t = "font-size:30;"
  r = r & "<table width=100% cellpadding=10 cellspacing=0 style='" & t & "'>"
  orign = n : origk = k
  do until rs.eof
    a = rs("contents") & ""
    if session("carpool_ay") = "" or request_act = "" then
      dist1 = 0
      dist2 = 0
      dategap = 0
    else
     dist1 = calcdist(session("carpool_ay"), session("carpool_ax"), getval(a, "ay"), getval(a, "ax")) : if not isnumeric(dist1) then dist1 = 100000
     dist2 = calcdist(session("carpool_by"), session("carpool_bx"), getval(a, "by"), getval(a, "bx")) : if not isnumeric(dist2) then dist2 = 100000
     dategap = datediff("n", session("carpool_d"), rs("date1"))
    end if
    if orign = 999999992 then n = getval(a, "mm") : if n = "" then n = "120"
    if origk = 999999992 then k = getval(a, "mk") : if k = "" then k = "10"

    if cdbl(dist1) <= cdbl(k) and cdbl(dist2) <= cdbl(k) then
      showlistids = showlistids & rs("id") & ","
      showlistuis = showlistuis & getval(a, "ui") & ","
      c = c + 1
      t = "~Rides~" : if n = 999999991 then t = "~My Orders~"
      if rs("type1") = "carpool" and didtitlecarpool = "" then r = r & "<tr><td colspan=9 style='color:#ff0000;'><b>" & t : didtitlecarpool = "on"
      if rs("type1") = "carpoolboard" and didtitlecarpoolboard = "" then r = r & "<tr><td colspan=9 style='color:#ff0000;'><b>������ ����" : didtitlecarpoolboard = "on"
      if c = 1 then
        t = "<tr bgcolor=#aaaaaa>"
	      if session("carpool_mode") <> "mob" then t = t & "<td><b>"
	      t = t & td & "<b>~Date~"
	      t = t & td & "<b>~orders_from1~"
	      t = t & td & "<b>~orders_to1~"
	      t = t & td & "<b>~Driver~ / ~Passengers~"
	      t = t & td & "<b>"
        firstcol = t
        r = r & firstcol
      end if
      if rs("type1") <> lasttype1 and lasttype1 <> "" then r = r & firstcol
      lasttype1 = rs("type1")
      t = "ffffff" : if hasmatch(getval(a, "pi"), currentuserid) then t = "ffdddd"
      r = r & "<tr valign=top bgcolor=#" & t & ">"
      if session("carpool_mode") <> "mob" then r = r & "<td><img src=bigicons/~langside2~.png width=32 height=32>"

      r = r & td & normalizedatestring(rs("date1"))
      if dategap <> 0 then r = r & " <span style='color:#bbbbbb; white-space:nowrap;'>" & dategap & " ~minutes~ ~gap~</span>"

      pi = getval(a, "pi") : pa = getval(a, "pa") : if pa = "" then pa = 4
      free = cdbl(pa) - countstring(pi, ",")
      if rs("type1") <> "carpoolboard" then r = r & " <span style='color:#000000; white-space:nowrap;'>(" & free & " ~Seats Available~)</span>"

      remarks = getval(a, "rem") : if remarks <> "" then r = r & "<br><span style='color:#aa0000;'>~Remarks~: " & remarks & "</span>"
      t = getval(a, "via") : if t <> "" then t = " (~orders_via1~ " & t & ")"

      r = r & td & "<b>" & getval(a, "an") & "</b>"
      x = fix(dist1) : if x <> 0 then r = r & " <span style='color:#bbbbbb; white-space:nowrap;'>" & x & " ~km~ ~gap~</span>"

      r = r & td & "<b>" & getval(a, "bn") & t & "</b>"
      x = fix(dist2) : if x <> 0 then r = r & " <span style='color:#bbbbbb; white-space:nowrap;'>" & x & " ~km~ ~gap~</span>"

      t = getuserorworker(getval(a, "ui"))
      r = r & td & t & " " & getuserphone(getval(a, "ui"))
      r = r & "<span style='color:#999999;'>"
      tt = getval(a, "pi") : t = ""
      do until tt = ""
        b = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
        r = r & "<br>"
        if getval(a, "ui") = currentuserid then r = r & "<a href=#du style='color:#ff0000;' onclick='if(confirm(""~Remove~ " & tocsv(getuserorworker(b)) & """)) document.location = ""?act=remove&id=" & rs("id") & "&pid=" & b & """;'>X</a> "
        r = r & getuserorworker(b)
      loop
      r = r & "</span>"

      r = r & td
      sty = "" : if session("carpool_mode") = "mob" then sty = "class='waves-effect waves-light btn-large' style='height:60; padding-top:4; width:200; margin-bottom:20;'"

      if getval(a, "ui") = currentuserid then
        r = r & " <a href=#du style='color:#ff0000;' onclick='if(confirm(""~Delete~ ~Order~"")) document.location = ""?act=delete&id=" & rs("id") & """;'> <nobr>~Delete~ ~Order~</nobr> </a>"
      else
       if rs("type1") = "carpool" and hasmatch(getval(a, "pi"), currentuserid) then
         r = r & "<input " & sty & " type=button value='~Remove~' onclick='document.location = ""?act=unroll&id=" & rs("id") & """;'>"
       elseif rs("type1") = "carpool" and free > 0 then
         r = r & "<input " & sty & " type=button value='~Enroll~' onclick='document.location = ""?act=enroll&id=" & rs("id") & """;'>"
       end if
       if specs <> "lubinski" then r = r & " <a href=#du onclick='document.location = ""?act=message&to=" & getval(a, "ui") & """;'> <nobr>~Send Message~</nobr> </a>"
      end if
    end if

    rs.movenext
  loop
  if c = 0 then r = r & "<center><b>~Cannot find similar rides~</b></center>"
  showlist = r
end function

'-------------calculate distance
function calcdist(byval lat1, byval lon1, byval lat2, byval lon2)
  'calcdist(32.836596, 35.271481, 32.835781, 35.350442) 'yodfat,avtalyon
  dim pi,r,dlat,dlon,a,c,d
  Pi = 3.14159265358979
  r = 6373 'km radius of the earth at 39 degrees from the equator
  lat1 = lat1 * pi / 180'to radians
  lon1 = lon1 * pi / 180
  lat2 = lat2 * pi / 180
  lon2 = lon2 * pi / 180
  dlat = lat2 - lat1 : dlon = lon2 - lon1
  a = sin(dlat/2) ^ 2 + cos(lat1) * cos(lat2) * sin(dlon/2) ^ 2
  c = 2 * atn2(a ^ 0.5 ,(1-a) ^ 0.5) 
  d = c * r
  calcdist = d
end function

Function Atn2(y, x)
  dim pi : Pi = 3.14159265358979
  If x > 0 Then
    Atn2 = Atn(y / x)
  ElseIf x < 0 Then
    Atn2 = Sgn(y) * (Pi - Atn(Abs(y / x)))
  ElseIf y = 0 Then
    Atn2 = 0
  Else
    Atn2 = Sgn(y) * Pi / 2
  End If
End Function

%>
