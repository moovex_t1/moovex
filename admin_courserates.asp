<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 06/07/2014 12:25:48

function courseratesdelitem(i)
  courseratesdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function courseratesinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "courseid" then t = session(tablename & "containerid")
  if getfield("courseid") <> "" then t = getfield("courseid")
  f = f & "courseid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "from1" then t = session(tablename & "containerid")
  if getfield("from1") <> "" then t = getfield("from1")
  f = f & "from1," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "to1" then t = session(tablename & "containerid")
  if getfield("to1") <> "" then t = getfield("to1")
  f = f & "to1," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "areaid1" then t = session(tablename & "containerid")
  if getfield("areaid1") <> "" then t = getfield("areaid1")
  f = f & "areaid1," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "areaid2" then t = session(tablename & "containerid")
  if getfield("areaid2") <> "" then t = getfield("areaid2")
  f = f & "areaid2," : v = v & t & ","

  t = "on" : if getfield("activeproduction") <> "" then t = getfield("activeproduction")
  f = f & "activeproduction," : v = v & "'" & t & "',"

  t = "on" : if getfield("active") <> "" then t = getfield("active")
  f = f & "active," : v = v & "'" & t & "',"

  t = "on" : if getfield("special") <> "" then t = getfield("special")
  f = f & "special," : v = v & "'" & t & "',"

  t = "" : if getfield("duration") <> "" then t = getfield("duration")
  f = f & "duration," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "supplierid" then t = session(tablename & "containerid")
  if getfield("supplierid") <> "" then t = getfield("supplierid")
  f = f & "supplierid," : v = v & t & ","

  t = "0" : if getfield("passengers") <> "" then t = getfield("passengers")
  f = f & "passengers," : v = v & t & ","

  t = "0" : if getfield("price") <> "" then t = getfield("price")
  f = f & "price," : v = v & t & ","

  t = "" : if getfield("addnight") <> "" then t = getfield("addnight")
  f = f & "addnight," : v = v & "'" & t & "',"

  t = "" : if getfield("addweekend") <> "" then t = getfield("addweekend")
  f = f & "addweekend," : v = v & "'" & t & "',"

  t = "" : if getfield("returndiscount") <> "" then t = getfield("returndiscount")
  f = f & "returndiscount," : v = v & "'" & t & "',"

  t = "" : if getfield("ordertypeidsv") <> "" then t = getfield("ordertypeidsv")
  f = f & "ordertypeidsv," : v = v & "'" & t & "',"

  t = "" : if getfield("ordertypeids") <> "" then t = getfield("ordertypeids")
  f = f & "ordertypeids," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  t = session("usersiteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
']
  f = f & "siteid," : v = v & t & ","

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into courserates(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from courserates"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  courseratesinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table courserates (id autoincrement primary key,courseid number,from1 number,to1 number,areaid1 number,areaid2 number,activeproduction text(2),active text(2),special text(2),duration text(20),supplierid number,passengers number,price number,addnight text(10),addweekend text(10),returndiscount text(10),ordertypeidsv memo,ordertypeids memo,siteid number,data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "courserates" : session("tablename") = "courserates"
tablefields = "id,courseid,from1,to1,areaid1,areaid2,activeproduction,active,special,duration,supplierid,passengers,price,addnight,addweekend,returndiscount,ordertypeidsv,ordertypeids,siteid,data1,"
request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if

if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 4
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
if session("usergroup") <> "admin" or session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"
if getparam("UseAreaRates") <> "y" then hidecontrols = hidecontrols & "areaid1,areaid2,"
session("courseratespassedgoodafter") = "" : session("courseratespassedgooduntil") = ""
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = courseratesinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  msg = msg & " ~Item Updated~"
  t = getfield("addnight") : t = ctxt(t) : t = replace(t,"%","") : if t <> "" and not isnumeric(t) then x = errorend("_back~Invalid~ ~courserates_addnight~")
  t = getfield("addweekend") : t = ctxt(t) : t = replace(t,"%","") : if t <> "" and not isnumeric(t) then x = errorend("_back~Invalid~ ~courserates_addweekend~")
  t = getfield("addother") : t = ctxt(t) : t = replace(t,"%","") : if t <> "" and not isnumeric(t) then x = errorend("_back~Invalid~ ~courserates_addother~")
  t = getfield("fineother") : t = ctxt(t) : t = replace(t,"%","") : if t <> "" and not isnumeric(t) then x = errorend("_back~Invalid~ ~courserates_fineother~")
  t = getfield("returndiscount") : t = ctxt(t) : t = replace(t,"%","") : if t <> "" and not isnumeric(t) then x = errorend("_back~Invalid~ ~courserates_returndiscount~")
  if hasmatch(specs, "jerusalem,muni,brener,") and getfield("courseid") <> "0" and left(request_action3,11) <> "justrefresh" then
    t = getonefield("select data1 from courses where id = 0" & getfield("courseid")) : t = getval(t, "supplierid")
    if t <> "" and t <> "0" and getfield("supplierid") <> t then
      du = errorend("_back������ ����� ��� ���� " & gettitle("suppliers", t) & ". ��� ���� �� �� �������.")
    end if
  end if
']
  s = "update " & tablename & " set "
  if okedit("courseid") then s = s & "courseid=0" & getfield("courseid") & ","
'[  if okedit("from1") then s = s & "from1=0" & getfield("from1") & ","
'[  if okedit("to1") then s = s & "to1=0" & getfield("to1") & ","
  if getfield("courseid") = "0" then
    if okedit("from1") then s = s & "from1=0" & getfield("from1") & ","
    if okedit("to1") then s = s & "to1=0" & getfield("to1") & ","
  else
    if okedit("from1") then s = s & "from1=0,"
    if okedit("to1") then s = s & "to1=0,"
  end if
']
  if okedit("areaid1") then s = s & "areaid1=0" & getfield("areaid1") & ","
  if okedit("areaid2") then s = s & "areaid2=0" & getfield("areaid2") & ","
  if okedit("activeproduction") then s = s & "activeproduction='" & getfield("activeproduction") & "',"
  if okedit("active") then s = s & "active='" & getfield("active") & "',"
  if okedit("special") then s = s & "special='" & getfield("special") & "',"
  if okedit("duration") then s = s & "duration='" & getfield("duration") & "',"
  if okedit("supplierid") then s = s & "supplierid=0" & getfield("supplierid") & ","
  if okedit("passengers") then s = s & "passengers=0" & getfield("passengers") & ","
'[  if okedit("price") then s = s & "price=0" & getfield("price") & ","
  price = getfield("price") : d = getfield("data1") : max = 0
  if getval(d, "relativeprice") = "on" and cdbl(price) = 0 then
    ii = getidlist("select stationid from coursestations where courseid = " & getfield("courseid"))
    do until ii = ""
      i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
      v = getval(d, "r" & i) : v = cdbl("0" & strfilter(v, "0123456789."))
      if v > max then max = v
    loop
    price = max
  end if
  if okedit("price") then s = s & "price=0" & price & ","
']
  if okedit("addnight") then s = s & "addnight='" & getfield("addnight") & "',"
  if okedit("addweekend") then s = s & "addweekend='" & getfield("addweekend") & "',"
  if okedit("returndiscount") then s = s & "returndiscount='" & getfield("returndiscount") & "',"
  if okedit("ordertypeidsv") then s = s & "ordertypeidsv='" & getfield("ordertypeidsv") & "',"
  if okedit("ordertypeids") then s = s & "ordertypeids='" & getfield("ordertypeids") & "',"
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
'[  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  d = getfield("data1")

  'if specs = "rafael" then
	'  set rs1 = conn.execute(sqlfilter("select stationid from coursestations where courseid = " & getfield("courseid")))
	'  do until rs1.eof
  '    t = getval(d, "ratecode" & rs1("stationid"))
  '    if t <> "" then
  '      v = getonefield("select price from courserates where siteid = 0" & getfield("siteid") & " and supplierid = 0" & getfield("supplierid") & " and passengers = 0" & getfield("passengers") & " and instr(cstr('' & data1), '|ratecode=" & t & "|') > 0")
  '      if v = "" then du = showerror(t & " - �� ���� ���� ����� ����� �������")
  '      d = setval(d, "r" & rs1("stationid"), v)
  '    end if
	'    rs1.movenext
	'  loop
  'end if

  if okedit("data1") then s = s & "data1='" & d & "',"
']
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'['-------------afterupdate-------------
if hasmatch(specs, "leumi,tikshoov,") and request_action = "update" then
  ee = getfieldlist("select email from buusers where usergroup = 'Admin' and siteid = 0" & getfield("siteid"))
  subject = "������ " & request_id & " �����"
  body = "������ " & request_id & " ����� �� ��� ������ " & session("username")
  body = body & "<br><br><a href=" & siteurl & "admin_courserates.asp?action=form&id=" & request_id & ">����� ������� ���� ���</a>"
  x = sendemail(getparam("siteemail"), ee, "", "", subject, body)
end if

'-----------duplicate2 - rafael
if request_action = "duplicate2" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  if ids = "" then du = errorend("_back~Please choose some items~")
  rr("<table class=form_table width=100% cellspacing=5 align=center>")
  rr "<form method=post name=f1>"
  rr "<input type=hidden name=action value=duplicate2submit>"
  rr "<input type=hidden name=token value=" & session("token") & ">"
  rr "<input type=hidden name=ids value=" & ids & ">"
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_home.gif' ~iconsize~> ����� �������� ������ ����� ������ - ����� " & countstring(ids,",") & " �������� ������")

  rr "<tr valign=top><td class=form_item1>��� �����<td class=form_item2>"
  s = "select id,title from suppliers where 0=0"
  if session("usersiteid") <> "0" then s = s & " and siteid in(" & session("usersiteid") & ",0)"
  s = s & " order by title"
  set rs = conn.execute(sqlfilter(s))
  sc = ""
  rr "<br><table width=500 cellpadding=3>"
  rr "<tr style='background:#dddddd;'><td><input type=checkbox id=checkall onclick='vbscript: du = checkallsuppliers(me.checked)'> ��� ���"
  rr "<td><nobr><b>���� �������<td><nobr>����� ����<td><nobr>����� ����<td><nobr>����� ����� (���� ��� ������ ��� ������ �����)"
  checkallsuppliersscript = ""
  do until rs.eof
    i = rs("id")
    checkallsuppliersscript = checkallsuppliersscript & " f1.supplier_" & i & ".checked = v" & vbcrlf
    rr "<tr><td><nobr><b><input type=checkbox name=supplier_" & i & " onclick='vbscript:populatesuppliers'>" & rs("title")
    rr "<td><input type=text name=reduction" & i & " value='0' style='font-size:10; width:50; direction:ltr; border:1px solid #aaaaaa;' onfocus1='vbscript: supplier_" & i & ".checked = true : populatesuppliers'>"
    rr "<td><input type=text name=addnight" & i & " value='' style='font-size:10; width:50; direction:ltr; border:1px solid #aaaaaa;' onfocus1='vbscript: supplier_" & i & ".checked = true : populatesuppliers'>"
    rr "<td><input type=text name=addweekend" & i & " value='' style='font-size:10; width:50; direction:ltr; border:1px solid #aaaaaa;' onfocus1='vbscript: supplier_" & i & ".checked = true : populatesuppliers'>"
    rr "<td><input type=text name=entrance" & i & " value='' style='font-size:10; width:50; direction:ltr; border:1px solid #aaaaaa;' onfocus1='vbscript: supplier_" & i & ".checked = true : populatesuppliers'>"
    sc = sc & "if f1.supplier_" & i & ".checked then t = t & """ & i & ",""" & vbcrlf
    rs.movenext
  loop
  rr "</table>"
  rr "<script language=vbscript>"
  rr "function checkallsuppliers(byval v)" & vbcrlf
  rr checkallsuppliersscript
  rr "end function" & vbcrlf
  rr "</script>"
  rr("<input type=hidden name=suppliers>")
  rr("<script language=vbscript>" & vbcrlf)
  rr("  sub populatesuppliers" & vbcrlf)
  rr("    t = """"" & vbcrlf)
  rr(sc & vbcrlf)
  rr("    f1.suppliers.value = t" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  populatesuppliers" & vbcrlf)
  rr("</script>" & vbcrlf)

  rr "<tr><td colspan=2 class=update_bar>"
  rr " <input type=submit class=groovybutton value='����'>"
  rr " <input type=button class=groovybutton value=~Cancel~ onclick='vbscript: document.location = ""?""'>"
  rend
end if
if request_action = "duplicate2submit" then
  ii = getfield("ids") : did = ""
  do until ii = ""
    i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
    ss = getfield("suppliers")
    do until ss = ""
      supplierid = left(ss,instr(ss,",")-1) : ss = mid(ss,instr(ss,",")+1)
      pp = getfieldlist("select passengers from cars where supplierid = " & supplierid)
      pp = uniquelist(pp)
      s = "select courseid,from1,to1,areaid1,areaid2,supplierid,passengers from courserates where id = " & i
      set rs = conn.execute(sqlfilter(s))
      if instr("," & pp, "," & rs("passengers") & ",") = 0 then
        du = showerror("���� " & gettitle("suppliers", supplierid) & " ��� ��� �� " & rs("passengers") & " ������")
      else
        s = "select id from courserates where 0=0"
        s = s & " and courseid = " & rs("courseid")
        s = s & " and from1 = " & rs("from1")
        s = s & " and to1 = " & rs("to1")
        s = s & " and areaid1 = " & rs("areaid1")
        s = s & " and areaid2 = " & rs("areaid2")
        s = s & " and supplierid = " & supplierid
        s = s & " and passengers = " & rs("passengers")
        this = getonefield(s)

        if this = "" then
          s = "insert into " & tablename & "(courseid,from1,to1,areaid1,areaid2,duration,supplierid,passengers,price,addnight,addweekend,returndiscount,ordertypeidsv,ordertypeids,siteid,data1)"
          s = s & " SELECT courseid,from1,to1,areaid1,areaid2,duration," & supplierid & ",passengers,price,addnight,addweekend,returndiscount,ordertypeidsv,ordertypeids,siteid,data1"
          s = s & " FROM " & tablename & " where id = " & i
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          s = "select max(id) as id from " & tablename
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          this = rs("id")
        end if

        did = did & this & ","
        t = getfield("reduction" & supplierid) : if not isnumeric(t) then x = errorend("���� ���� �� ����")
        du = increaserates(this & ",", 0 - cdbl(t), "")

        a = ""
        t = getfield("addnight" & supplierid) : if t <> "" then a = a & ",addnight='" & t & "'"
        t = getfield("addweekend" & supplierid) : if t <> "" then a = a & ",addweekend='" & t & "'"
        t = getfield("entrance" & supplierid)
        if t <> "" then
          d = getonefield("select data1 from courserates where id = " & this)
          d = setval(d, "entrance", t)
          a = a & ",data1='" & d & "'"
        end if
        if a <> "" then
          s = "update courserates set " & mid(a,2) & " where id = " & this
          set rs1 = conn.execute(sqlfilter(s))
        end if
      end if
    loop
  loop
  du = showerror(countstring(did, ",") & " �������� ������ � " & countstring(getfield("suppliers"), ",") & " �����")
  session(tablename & "_showids") = did
end if

'---------------increase--------------
if request_action2 = "increase" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  du = increaserates(ids, getfield("increase"), getfield("roundprices"))
  msg = msg & "������ " & countstring(ids, ",")  & " ��������"
end if

function increaserates(byval ids, byval increase, byval roundprices)
  dim s,rs,sq,rs1,c,x,price,addnight,addweekend,returndiscount,a,b,r,data1,t,f,i
  dim ss()
  s = "select id, price, addnight, addweekend, returndiscount, data1 from courserates where id in(" & ids & "-1)"
  set rs = conn.execute(sqlfilter(s))
  c = 0
  if not isnumeric(increase) then du = errorend("_back��� �������� �� ����")
  do until rs.eof
    c = c + 1
    x = rs("price") : x = x * (100 + increase) / 100 : if roundprices = "on" then x = round(x)
    x = formatnumber(x,2,true,false,false) : price = x
    x = rs("addnight")
    if isnumeric(x) then
      x = x * (100 + increase) / 100 : if roundprices = "on" then x = round(x)
      x = formatnumber(x,2,true,false,false)
    end if
    addnight = x

    x = rs("addweekend")
    if isnumeric(x) then
      x = x * (100 + increase) / 100 : if roundprices = "on" then x = round(x)
      x = formatnumber(x,2,true,false,false)
    end if
    addweekend = x

    x = rs("returndiscount")
    if isnumeric(x) then
      x = x * (100 + increase) / 100 : if roundprices = "on" then x = round(x)
      x = formatnumber(x,2,true,false,false)
    end if
    returndiscount = x

    data1 = rs("data1") : data1 = ctxt(data1)
    a = data1 & "|" : r = ""
    do until a = ""
      b = left(a,instr(a,"|")-1) : a = mid(a,instr(a,"|")+1)
      if instr(b,"=") > 0 then
        f = left(b, instr(b,"=")-1) : x = mid(b,instr(b,"=")+1)
        if instr(f, "stop") > 0 or instr(f, "entrance") > 0 or (left(f,1) = "r" and isnumeric(mid(f,2,1))) then
          x = cdbl("0" & strfilter(x, "0123456789."))
          x = x * (100 + increase) / 100
          if roundprices = "on" then x = round(x)
          x = formatnumber(x,2,true,false,false)
          data1 = setval(data1, f, x)
        end if
      end if
    loop
    s = "update courserates set"
    s = s & " price = " & price
    s = s & ",addnight = '" & addnight & "'"
    s = s & ",addweekend = '" & addweekend & "'"
    s = s & ",returndiscount = '" & returndiscount & "'"
    s = s & ",data1 = '" & data1 & "'"
    s = s & " where id = " & rs("id")

    redim preserve ss(c) : ss(c) = s
    rs.movenext
  loop
  for i = 1 to c
    s = ss(i)
    set rs = conn.execute(sqlfilter(s))
  next
end function

'---------------retro-----------------
if getfield("retro") = "on" then request_action2 = "retro" : request_ids = request_id & ","
if request_action2 = "retro" then
  set e = Server.CreateObject("ADODB.Stream") : e.open
  '-----disable mid-day reports
  hh = getparam("DisableReportHours")
  if isnumeric(hh) and len(hh) = 4 then
    h1 = cdbl(left(hh,2)) : h2 = cdbl(right(hh,2))
    if hour(now) >= h1 and hour(now) < h2 then x = errorend("~Please use this function beyond main working hours~: " & h1 & ":00 - " & h2 & ":00")
  end if

  server.scripttimeout = 900 'seconds
  rr "<script language=vbscript> progress.style.top = 200 </script>" : response.flush

  Set adict = Server.CreateObject("Scripting.Dictionary")
  s = "select id,areaid from stations"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    a = "" & rs("areaid") : if a <> "" then adict.item(a) = adict(a) & rs("id") & ","
    rs.movenext
  loop

  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  rsc = 0 : rsrc = countstring(ids, ",")
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    rsc = rsc + 1 : rr "<script language=vbscript> progresstext.innerhtml = ""<b>~Updating Order Prices~..</b> " & rsc & "/" & rsrc & """ : progressbar.style.width = " & fix(400 * rsc / rsrc) & " </script>" : response.flush
    s = "select id,supplierid,courseid,from1,to1,areaid1,areaid2 from courserates where id = " & b
    set rs = conn.execute(sqlfilter(s))

    s = "select id,price,supplierid from orders"
    s = s & " where date1 >= " & sqldate(getfield("d1"))
    if getfield("retroallstatuses") <> "on" then s = s & " and status = 'New'"
    s = s & " and supplierid = " & rs("supplierid")
    if cstr("" & rs("courseid")) = "0" and cstr("" & rs("from1")) <> "0" then
      s = s & " and courseid = 0 and (from1 = " & rs("from1") & " and to1 = " & rs("to1") & " or from1 = " & rs("to1") & " and to1 = " & rs("from1") & ")"
    elseif cstr("" & rs("courseid")) = "0" and cstr("" & rs("areaid1")) <> "0" then
      a1 = adict(cstr(rs("areaid1"))) : a2 = adict(cstr(rs("areaid2")))
      s = s & " and courseid = 0 and (from1 in(" & a1 & "-1) and to1 in(" & a2 & "-1) or from1 in(" & a2 & "-1) and to1 in(" & a1 & "-1))"
    else
      s = s & " and courseid = " & rs("courseid")
    end if
    s = s & " order by id"

    c = 0
    if rs1.state = 1 then rs1.close
    rs1.open sqlfilter(s), conn, 3, 1, 1
    do until rs1.eof
      pricewas = rs1("price")
      price = getrideprice("|orderid=" & rs1("id") & "|locksupplierid=on|")
      if price <= 0 then
        'du = showerror("~Order~ " & rs1("id") & " - ~Rate not found~. ~Price not changed~ - " & pricewas)
      elseif cdbl(price) = cdbl(pricewas) then
        'du = showerror("~Order~ " & rs1("id") & " - ~Price not changed~ - " & pricewas)
      else
        t = translate("~Course Rate~ " & b & " ~Order~ " & rs1("id") & " - ~Price changed from~ " & formatnumber(pricewas,2,true,false,false) & " ~To ~ " & formatnumber(price,2,true,false,false) & "<br>")
        rr t : e.writetext(t)
        c = c + 1
      end if
      rs1.movenext
    loop
    t = translate("~Course Rate~ " & b & ": " & c & " ~Orders Updated~<br>")
    rr t : e.writetext(t)
  loop
  rr "<script language=vbscript> progress.style.top = -1000 </script>" : response.flush
  t = "OK " & c & "<br>" : rr t : e.writetext(t)
  e.position = 0 : e1 = e.readtext(e.size) : e.close
  x = sendemail(getparam("siteemail"), getuseremail(session("userid")), "", "", getparam("applicationdisplay") & " - ��� ����� �������� ����������", e1)
  if getfield("retro") = "on" then request_action2 = "stay"
end if

'----------------------------------------------------------------------
if request_action = "updateactive" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      if getfield("multi_activeproduction") <> "" then s = s & ",activeproduction = '" & getfield("multi_activeproduction") & "'"
      if getfield("multi_active") <> "" then s = s & ",active = '" & getfield("multi_active") & "'"
      if getfield("multi_special") <> "" then s = s & ",special = '" & getfield("multi_special") & "'"
      if getfield("multi_supplierid") <> "" then s = s & ",supplierid = " & getfield("multi_supplierid")
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'----------------------------------------------------------------------
']
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(courseid,from1,to1,areaid1,areaid2,activeproduction,active,special,duration,supplierid,passengers,price,addnight,addweekend,returndiscount,ordertypeidsv,ordertypeids,siteid,data1)"
  s = s & " SELECT courseid,from1,to1,areaid1,areaid2,activeproduction,active,special,duration,supplierid,passengers,price,addnight,addweekend,returndiscount,ordertypeidsv,ordertypeids,siteid,data1"
  s = s & " FROM " & tablename & " where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  request_action = "form"
  request_id = theid
  msg = msg & " ~Record duplicated~"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = courseratesdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = courseratesdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      if okedit("activeproduction") then s = s & ",activeproduction = '" & getfield("listupdate_activeproduction_" & b) & "'"
      if okedit("active") then s = s & ",active = '" & getfield("listupdate_active_" & b) & "'"
      if okedit("special") then s = s & ",special = '" & getfield("listupdate_special_" & b) & "'"
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
'[  dim csv() : redim preserve csv(1)
  dim csv() : redim preserve csv(1)
  server.scripttimeout = 7200 'seconds
']
  csv(0) = "_ID^"
  if oksee("courseid") then csv(0) = csv(0) & "~courserates_courseid~^"
  if oksee("from1") then csv(0) = csv(0) & "~courserates_from1~^"
  if oksee("to1") then csv(0) = csv(0) & "~courserates_to1~^"
  if oksee("areaid1") then csv(0) = csv(0) & "~courserates_areaid1~^"
  if oksee("areaid2") then csv(0) = csv(0) & "~courserates_areaid2~^"
  if oksee("activeproduction") then csv(0) = csv(0) & "~courserates_activeproduction~^"
  if oksee("active") then csv(0) = csv(0) & "~courserates_active~^"
  if oksee("special") then csv(0) = csv(0) & "~courserates_special~^"
  if oksee("duration") then csv(0) = csv(0) & "~courserates_duration~^"
  if oksee("supplierid") then csv(0) = csv(0) & "~courserates_supplierid~^"
  if oksee("passengers") then csv(0) = csv(0) & "~courserates_passengers~^"
  if oksee("price") then csv(0) = csv(0) & "~courserates_price~^"
  if oksee("addnight") then csv(0) = csv(0) & "~courserates_addnight~^"
  if oksee("addweekend") then csv(0) = csv(0) & "~courserates_addweekend~^"
  if oksee("returndiscount") then csv(0) = csv(0) & "~courserates_returndiscount~^"
  if oksee("ordertypeidsv") then csv(0) = csv(0) & "~courserates_ordertypeidsv~^"
  if oksee("ordertypeids") then csv(0) = csv(0) & "~courserates_ordertypeids~^"
  if oksee("siteid") then csv(0) = csv(0) & "~courserates_siteid~^"
'[  if oksee("data1") then csv(0) = csv(0) & "~courserates_data1~^"
  if specs = "rail" then csv(0) = csv(0) & "weekdayreduction^"
  if specs = "rail" then csv(0) = csv(0) & "�������^"'category
  if showratecode = "on" then csv(0) = csv(0) & "ratecode^"
  if specs = "kmc" then
    csv(0) = csv(0) & "specificway^"
  end if
  if specs = "iec" then
    csv(0) = csv(0) & "areaids1^"
    csv(0) = csv(0) & "areaids2^"
    csv(0) = csv(0) & "dealnumber^"
    csv(0) = csv(0) & "dealitem^"
    csv(0) = csv(0) & "kmrate^"
    csv(0) = csv(0) & "goodafter^"
    csv(0) = csv(0) & "gooduntil^"
  end if
  if showdriverrate = "on" then
    csv(0) = csv(0) & "���� ���^"'driverrate
    csv(0) = csv(0) & "����� ���� ����^"'driverratenight
    csv(0) = csv(0) & "����� ���� ����^"'driverrateweekend
  end if
  if specs = "shufersal" then
    csv(0) = csv(0) & "nearareas^"
  end if
  if oksee("data1") then csv(0) = csv(0) & "~courserates_data1~^"
']
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
  Thefrom1 = replace(thefrom1,",",".")
  Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
  Theto1 = replace(theto1,",",".")
  Theareaid1 = rs("areaid1") : if isnull(Theareaid1) then Theareaid1 = 0
  Theareaid1 = replace(theareaid1,",",".")
  Theareaid2 = rs("areaid2") : if isnull(Theareaid2) then Theareaid2 = 0
  Theareaid2 = replace(theareaid2,",",".")
  Theactiveproduction = rs("activeproduction") : if isnull(Theactiveproduction) then Theactiveproduction = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
  Theduration = rs("duration") : if isnull(Theduration) then Theduration = ""
  Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
  Thesupplierid = replace(thesupplierid,",",".")
  Thepassengers = rs("passengers") : if isnull(Thepassengers) then Thepassengers = 0
  Thepassengers = replace(thepassengers,",",".") : Thepassengers = formatnumber(thepassengers,0,true,false,false)
  Theprice = rs("price") : if isnull(Theprice) then Theprice = 0
  Theprice = replace(theprice,",",".") : Theprice = formatnumber(theprice,2,true,false,false)
  Theaddnight = rs("addnight") : if isnull(Theaddnight) then Theaddnight = ""
  Theaddweekend = rs("addweekend") : if isnull(Theaddweekend) then Theaddweekend = ""
  Thereturndiscount = rs("returndiscount") : if isnull(Thereturndiscount) then Thereturndiscount = ""
  Theordertypeidsv = rs("ordertypeidsv") : if isnull(Theordertypeidsv) then Theordertypeidsv = ""
  Theordertypeids = rs("ordertypeids") : Theordertypeids = formatidlist(Theordertypeids)
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
  Thefrom1lookup = rs("from1lookup") : if isnull(Thefrom1lookup) then Thefrom1lookup = ""
  Theto1lookup = rs("to1lookup") : if isnull(Theto1lookup) then Theto1lookup = ""
  Theareaid1lookup = rs("areaid1lookup") : if isnull(Theareaid1lookup) then Theareaid1lookup = ""
  Theareaid2lookup = rs("areaid2lookup") : if isnull(Theareaid2lookup) then Theareaid2lookup = ""
  Thesupplieridlookup = rs("supplieridlookup") : if isnull(Thesupplieridlookup) then Thesupplieridlookup = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("courseid") then csv(rc) = csv(rc) & thecourseidlookup & "^"
    if oksee("from1") then csv(rc) = csv(rc) & thefrom1lookup & "^"
    if oksee("to1") then csv(rc) = csv(rc) & theto1lookup & "^"
    if oksee("areaid1") then csv(rc) = csv(rc) & theareaid1lookup & "^"
    if oksee("areaid2") then csv(rc) = csv(rc) & theareaid2lookup & "^"
    if oksee("activeproduction") then csv(rc) = csv(rc) & replace(theactiveproduction,vbcrlf," ") & "^"
    if oksee("active") then csv(rc) = csv(rc) & replace(theactive,vbcrlf," ") & "^"
    if oksee("special") then csv(rc) = csv(rc) & replace(thespecial,vbcrlf," ") & "^"
    if oksee("duration") then csv(rc) = csv(rc) & replace(theduration,vbcrlf," ") & "^"
    if oksee("supplierid") then csv(rc) = csv(rc) & thesupplieridlookup & "^"
    if oksee("passengers") then csv(rc) = csv(rc) & replace(thepassengers,vbcrlf," ") & "^"
    if oksee("price") then csv(rc) = csv(rc) & replace(theprice,vbcrlf," ") & "^"
    if oksee("addnight") then csv(rc) = csv(rc) & replace(theaddnight,vbcrlf," ") & "^"
    if oksee("addweekend") then csv(rc) = csv(rc) & replace(theaddweekend,vbcrlf," ") & "^"
    if oksee("returndiscount") then csv(rc) = csv(rc) & replace(thereturndiscount,vbcrlf," ") & "^"
    if oksee("ordertypeidsv") then csv(rc) = csv(rc) & replace(theordertypeidsv,vbcrlf," ") & "^"
      t = "" : d = formatidlist(theordertypeids)
      sq = "select * from ordertypes where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    if oksee("ordertypeids") then csv(rc) = csv(rc) & t & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
'[    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    if specs = "rail" then csv(rc) = csv(rc) & tocsv(getval(thedata1, "weekdayreduction")) & "^"
    if specs = "rail" then csv(rc) = csv(rc) & tocsv(getval(thedata1, "category")) & "^"
    if showratecode = "on" then csv(rc) = csv(rc) & tocsv(getval(thedata1, "ratecode")) & "^"
    if specs = "kmc" then
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "specificway")) & "^"
    end if
    if specs = "iec" then
      for i = 1 to 2
        v = ""
        sq = "select title from areas where id in(" & getval(thedata1, "areaids" & i) & "0)"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
        do until rs1.eof
          v = v & rs1("title") & ";"
          rs1.movenext
        loop
        csv(rc) = csv(rc) & tocsv(v) & "^"
      next
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "dealnumber")) & "^"
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "dealitem")) & "^"
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "kmrate")) & "^"
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "goodafter")) & "^"
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "gooduntil")) & "^"
    end if
    if showdriverrate = "on" then
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "driverrate")) & "^"
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "driverratenight")) & "^"
      csv(rc) = csv(rc) & tocsv(getval(thedata1, "driverrateweekend")) & "^"
    end if
    if getfield("add_area_doupdate") = "on" then
      a = "|" : x = 1
      do
        x = instr(x, thedata1, "|add_area_") : if x = 0 then exit do
        y = instr(x, thedata1, "=")
        i = mid(thedata1,x + 10, y - x - 10)
        a = a & tocsv(gettitle("areas", i)) & "=" & getval(thedata1, "add_area_" & i) & "|"
        x = x + 1
      loop
      csv(rc) = csv(rc) & tocsv(a) & "^"
    end if
    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
']
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      siteid = session("usersiteid")
      course = ""
      data1 = "" : data1field = ""
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
'[        elseif csvff(i) = "courseid" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("courseid") then s = s & "courseid=" & v & ","
        elseif csvff(i) = "courseid" then
          course = b
          if b = "" then
            v = 0
          else
            sq = "select * from courses where 0=0"
            if cstr(siteid) <> "0" then sq = sq & " and (siteid = 0 or siteid = " & siteid & ")"
            sq = sq & " and ("
            sq = sq & " lcase(cstr('' & code)) = '" & lcase(b) & "'"
            sq = sq & " or lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "'"
            if instr(1,b,"id",1) > 0 then t = replace(b,"id","",1,1,1) : if isnumeric(t) then sq = sq & " or id = " & t
            sq = sq & ")"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            mc = "" 'multiple courses
            do until rs1.eof
              mc = mc & rs1("id") & ","
              rs1.movenext
            loop
            if mc = "" then mc = "0,"
            v = "#COURSEID#"
          end if
          if mc = "0," then rr "~Not found~: " & b & "<br>"
          if okedit("courseid") then s = s & "courseid=" & v & ","
']
'[        elseif csvff(i) = "from1" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("from1") then s = s & "from1=" & v & ","
        elseif csvff(i) = "from1" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            if cstr(siteid) <> "0" then sq = sq & " and siteid = " & siteid
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            'if v = 0 then
            '  sq = "insert into stations(title,siteid) values('" & b & "'," & siteid & ")"
            '  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            '  sq = "select max(id) as id from stations"
            '  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            '  v = rs1("id")
            'end if
          end if
          if okedit("from1") then s = s & "from1=" & v & ","
']
'[        elseif csvff(i) = "to1" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("to1") then s = s & "to1=" & v & ","

        elseif csvff(i) = "to1" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            if cstr(siteid) <> "0" then sq = sq & " and siteid = " & siteid
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            'if v = 0 then
            '  sq = "insert into stations(title,siteid) values('" & b & "'," & siteid & ")"
            '  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            '  sq = "select max(id) as id from stations"
            '  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            '  v = rs1("id")
            'end if
          end if
          if okedit("to1") then s = s & "to1=" & v & ","
']
        elseif csvff(i) = "areaid1" then
          if b = "" then
            v = 0
          else
            sq = "select * from areas where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("areaid1") then s = s & "areaid1=" & v & ","
          if okedit("areaid1") then s = s & "areaid1=" & v & ","
          if specs = "iec" then
            addareaids1 = getidlist("select id from areas where instr(',' & title, '" & b & "') > 0")
            data1 = setval(data1, "areaids1", addareaids1)
          end if
']
        elseif csvff(i) = "areaid2" then
          if b = "" then
            v = 0
          else
            sq = "select * from areas where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("areaid2") then s = s & "areaid2=" & v & ","
          if okedit("areaid2") then s = s & "areaid2=" & v & ","
          if specs = "iec" then
            addareaids1 = getidlist("select id from areas where instr(',' & title, '" & b & "') > 0")
            data1 = setval(data1, "areaids2", addareaids1)
          end if
']
        elseif csvff(i) = "activeproduction" then
          if len(b) > 2 then b = left(b,2)
          if okedit("activeproduction") then s = s & "activeproduction='" & b & "',"
        elseif csvff(i) = "active" then
          if len(b) > 2 then b = left(b,2)
          if okedit("active") then s = s & "active='" & b & "',"
        elseif csvff(i) = "special" then
          if len(b) > 2 then b = left(b,2)
          if okedit("special") then s = s & "special='" & b & "',"
        elseif csvff(i) = "duration" then
          if len(b) > 20 then b = left(b,20)
          if okedit("duration") then s = s & "duration='" & b & "',"
        elseif csvff(i) = "supplierid" then
          if b = "" then
            v = 0
          else
'[            sq = "select * from suppliers where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            sq = "select * from suppliers where lcase(cstr('' & title)) = '" & lcase(b) & "' and (siteid = 0 or siteid = " & siteid & ") order by siteid desc"
']
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("supplierid") then s = s & "supplierid=" & v & ","
        elseif csvff(i) = "passengers" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("passengers") then s = s & "passengers=" & b & ","
        elseif csvff(i) = "price" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("price") then s = s & "price=" & b & ","
        elseif csvff(i) = "addnight" then
          if len(b) > 10 then b = left(b,10)
          if okedit("addnight") then s = s & "addnight='" & b & "',"
        elseif csvff(i) = "addweekend" then
          if len(b) > 10 then b = left(b,10)
          if okedit("addweekend") then s = s & "addweekend='" & b & "',"
        elseif csvff(i) = "returndiscount" then
          if len(b) > 10 then b = left(b,10)
          if okedit("returndiscount") then s = s & "returndiscount='" & b & "',"
        elseif csvff(i) = "ordertypeidsv" then
          if okedit("ordertypeidsv") then s = s & "ordertypeidsv='" & b & "',"
        elseif csvff(i) = "ordertypeids" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from ordertypes where lcase(cstr('' & title)) = '" & lcase(t) & "'"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("ordertypeids") then s = s & "ordertypeids='" & v & "',"
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("siteid") then s = s & "siteid=" & v & ","
          if okedit("siteid") then s = s & "siteid=" & v & ","
          if session("usersiteid") = "0" then siteid = v
']
'[        elseif csvff(i) = "data1" then
'[          if okedit("data1") then s = s & "data1='" & b & "',"
        elseif csvff(i) = "data1" then
          data1field = b
        elseif csvff(i) = "specificway" then
          data1 = setval(data1, "specificway", b)
        elseif csvff(i) = "weekdayreduction" then
          data1 = setval(data1, "weekdayreduction", b)
        elseif csvff(i) = "category" or csvff(i) = "�������" then
          data1 = setval(data1, "category", b)
        elseif csvff(i) = "ratecode" then
          data1 = setval(data1, "ratecode", b)

        elseif csvff(i) = "areaids1" then
          tt = b : tt =replace(tt, ";", ",") & "," : v =""
          do until tt = ""
            t = trim(left(tt,instr(tt,",")-1)) : tt = mid(tt,instr(tt,",")+1)
            t = getonefield("select id from areas where title = '" & t & "'")
            if t <> "" then v = v & t & ","
          loop
          data1 = setval(data1, "areaids1", v)
          v = "af" & replace(v, ",", "af") : data1 = setval(data1, "af", v)
        elseif csvff(i) = "areaids2" then
          tt = b : tt =replace(tt, ";", ",") & "," : v =""
          do until tt = ""
            t = trim(left(tt,instr(tt,",")-1)) : tt = mid(tt,instr(tt,",")+1)
            t = getonefield("select id from areas where title = '" & t & "'")
            if t <> "" then v = v & t & ","
          loop
          data1 = setval(data1, "areaids2", v)
          v = "at" & replace(v, ",", "at") : data1 = setval(data1, "at", v)
        elseif csvff(i) = "dealnumber" then
          data1 = setval(data1, "dealnumber", b)
        elseif csvff(i) = "dealitem" then
          data1 = setval(data1, "dealitem", b)
        elseif csvff(i) = "kmrate" then
          data1 = setval(data1, "kmrate", b)
        elseif csvff(i) = "goodafter" then
          data1 = setval(data1, "goodafter", b)
        elseif csvff(i) = "gooduntil" then
          data1 = setval(data1, "gooduntil", b)
        elseif csvff(i) = "driverrate" or csvff(i) = "���� ���" then
          data1 = setval(data1, "driverrate", b)
        elseif csvff(i) = "driverratenight" or csvff(i) = "����� ���� ����" then
          data1 = setval(data1, "driverratenight", b)
        elseif csvff(i) = "driverrateweekend" or csvff(i) = "����� ���� ����" then
          data1 = setval(data1, "driverrateweekend", b)
        elseif csvff(i) = "nearareas" then
          aa = b & "|"
          do until aa = ""
            a = left(aa,instr(aa,"|")-1) : aa = mid(aa,instr(aa,"|")+1)
            if instr(a, "=") > 0 then
              t = left(a,instr(a,"=")-1) : v = mid(a,instr(a,"=")+1)
              j = getonefield("select id from areas where title = '" & t & "'")
              if j <> "" then data1 = setval(data1, "add_area_" & j, v)
            end if
          loop
']
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
'[      if s <> "" then
'[        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = courseratesinsertrecord
'[        s = "update " & tablename & " set " & s
'[        s = left(s, len(s) - 1) & " "
'[        s = s & "where id = " & theid
'[        if okwrite(tablename,theid) then
'[          closers(rs) : set rs = conn.execute(sqlfilter(s))
'[          c = c + 1
'[        end if

      if data1 <> "" or data1field <> "" then
        currentdata1 = getonefield("select data1 from " & tablename & " where id = 0" & theid)
        if data1field <> "" then currentdata1 = data1field
        data1 = mergeval(currentdata1, data1)
        s = s & "data1='" & data1 & "',"
      end if

      if s <> "" then
        if course = "" then
	        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = courseratesinsertrecord
	        s = "update " & tablename & " set " & s
	        s = left(s, len(s) - 1) & " "
	        s = s & "where id = " & theid
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        elseif ulev > 1 and (instr(s,"#COURSEID#") = 0 or thisid <> "0" and thisid <> "") then
	        theid = thisid
          if okwrite(tablename,theid) then
		        s = "update " & tablename & " set " & s
		        s = left(s, len(s) - 1) & " "
		        s = s & "where id = " & theid
	          if countstring(mc,",") = 1 then s = replace(s, "#COURSEID#,", mc) else s = replace(s, "courseid=#COURSEID#,", "")
	          closers(rs) : set rs = conn.execute(sqlfilter(s))
	          c = c + 1
          end if
        elseif ulev > 1 then
	        s = "update " & tablename & " set " & s
	        s = left(s, len(s) - 1) & " "
	        s = s & "where id = " & theid
          a = mc
          do until a = ""
            b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
            sq = replace(s, "#COURSEID#", b)
            i = courseratesinsertrecord : sq = left(sq,instrrev(sq,"=")) & i
            closers(rs) : set rs = conn.execute(sqlfilter(sq))
            c = c + 1
          loop
        end if

']
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'['-------------more actions-------------------------------------

if request_action3 = "avaadd" then
  s = "select data1 from " & tablename & " where id = " & request_id
  set rs = conn.execute(sqlfilter(s))
  d = rs("data1")
  a = getval(d, "ava")
  a = a & "1,2,3,4,5,6,7," & getparam("ExtraWeekDays") & ";00:00;23:59;"
  d = setval(d, "ava", a)
  s = "update " & tablename & " set data1 = '" & d & "' where id = " & request_id
  set rs1 = conn.execute(sqlfilter(s))
end if

if left(request_action3,6) = "avadel" then
  j = mid(request_action3, 7)
  s = "select data1 from " & tablename & " where id = " & request_id
  set rs = conn.execute(sqlfilter(s))
  d = rs("data1")
  a = getval(d, "ava") : r = ""
  i = 0
  do until a = ""
    i = i + 1
    b1 = left(a,instr(a,";")-1) : a = mid(a,instr(a,";")+1)
    b2 = left(a,instr(a,";")-1) : a = mid(a,instr(a,";")+1)
    b3 = left(a,instr(a,";")-1) : a = mid(a,instr(a,";")+1)
    if cstr(i) <> cstr(j) then r = r & b1 & ";" & b2 & ";" & b3 & ";"
  loop
  d = setval(d, "ava", r)
  s = "update " & tablename & " set data1 = '" & d & "' where id = " & request_id
  set rs1 = conn.execute(sqlfilter(s))
end if


'---------------------------
if request_action = "ratecodeupdate" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  s = "select id,data1 from " & tablename & " where id in(" & ids & "-1)"
  set rs = conn.execute(sqlfilter(s))
  c = 0
  do until rs.eof
    c = c + 1
    v = getfield("ratecode" & rs("id"))
    d = setval(rs("data1"), "ratecode", tocsv(v))
    s = "update " & tablename & " set data1 = '" & d & "' where id = " & rs("id")
    set rs1 = conn.execute(sqlfilter(s))
    rs.movenext
  loop
  msg = msg & c & " ������ ������"
end if
']
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","courserates.id,courserates.courseid,courserates.from1,courserates.to1,courserates.areaid1,courserates.areaid2,courserates.activeproduction,courserates.active,courserates.special,courserates.duration,courserates.supplierid,courserates.passengers,courserates.price,courserates.addnight,courserates.addweekend,courserates.returndiscount,courserates.ordertypeidsv,courserates.ordertypeids,courserates.siteid,courserates.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
  Thefrom1 = replace(thefrom1,",",".")
  Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
  Theto1 = replace(theto1,",",".")
  Theareaid1 = rs("areaid1") : if isnull(Theareaid1) then Theareaid1 = 0
  Theareaid1 = replace(theareaid1,",",".")
  Theareaid2 = rs("areaid2") : if isnull(Theareaid2) then Theareaid2 = 0
  Theareaid2 = replace(theareaid2,",",".")
  Theactiveproduction = rs("activeproduction") : if isnull(Theactiveproduction) then Theactiveproduction = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
  Theduration = rs("duration") : if isnull(Theduration) then Theduration = ""
  Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
  Thesupplierid = replace(thesupplierid,",",".")
  Thepassengers = rs("passengers") : if isnull(Thepassengers) then Thepassengers = 0
  Thepassengers = replace(thepassengers,",",".") : Thepassengers = formatnumber(thepassengers,0,true,false,false)
  Theprice = rs("price") : if isnull(Theprice) then Theprice = 0
  Theprice = replace(theprice,",",".") : Theprice = formatnumber(theprice,2,true,false,false)
  Theaddnight = rs("addnight") : if isnull(Theaddnight) then Theaddnight = ""
  Theaddweekend = rs("addweekend") : if isnull(Theaddweekend) then Theaddweekend = ""
  Thereturndiscount = rs("returndiscount") : if isnull(Thereturndiscount) then Thereturndiscount = ""
  Theordertypeidsv = rs("ordertypeidsv") : if isnull(Theordertypeidsv) then Theordertypeidsv = ""
  Theordertypeids = rs("ordertypeids") : Theordertypeids = formatidlist(Theordertypeids)
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "courseid,from1,to1,areaid1,areaid2,activeproduction,active,special,duration,supplierid,passengers,price,addnight,addweekend,returndiscount,ordertypeidsv,ordertypeids,ordertypeids_select,ordertypeids_trigger,siteid,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
'[  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  if hasmatch(specs, "iai,shufersal,rail,imi,") and not childpage and getfield("printversion") <> "on" then
    rr("<div class=subtablinks>")
    t = "000000" : if request_action2 <> "showchildpage" then t = "bb0000"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "> <span style=" & t & "> ~" & tablename & "_" & tablename & "~ " & request_id & "</font></a>")
    t = "000000" : if getfield("childpage") = "history" then t = "bb0000"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=history&containerfield=r" & theid & "> <font color=" & t & "> ~History~ </font></a>")
    rr("<span id=childchildren></span></div>")
    if getfield("action2") = "showchildpage" then
      t = "admin_" & getfield("childpage") & ".asp?containerfield=" & getfield("containerfield") & "&containerid=" & request_id & "&action=search&action2=clear"
      if getfield("childpage") = "misc" then t = "admin_misc.asp?miscview=" & curl(getfield("miscview")) & "&containerid=0&miscdisplay=" & curl(getfield("miscdisplay")) & "&mischidefields=" & curl(getfield("mischidefields")) & "&misctrans=" & curl(getfield("misctrans"))
      rr("<br><iframe id=fr" & getfield("childpage") & " name=fr" & getfield("childpage") & " style='width:200%; height:1000;' frameborder=0 scrolling=no src=" & t & "></iframe>")
      rrbottom : rend
    end if
  end if

']
  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/cost.png' ~iconsize~> ~courserates_courserates~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  rr("<tr valign=top><td class=form_item1><span id=id_caption>~ID~</span><td class=form_item2 height=27>")
'[  rr("<font style='position:relative; top:3; background:#000000; color:ffffff; font-weight:bold; padding:2;'>" & request_id & "</font>")
  rr("<font style='position:relative; top:3; background:#000000; color:ffffff; font-weight:bold; padding:2;'>" & request_id & "</font>")

  if showratecode = "on" then
    rr("<tr valign=top><td class=form_item1>��� ������<td class=form_item2>")
    rr "<input type=text value='" & getval(thedata1, "ratecode") & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""ratecode"", me.value)'><br>"
  end if

  if specs = "iec" then
    rr("<tr valign=top><td class=form_item1>���� ����<td class=form_item2>")
    rr "<input type=text value='" & getval(thedata1, "dealnumber") & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""dealnumber"", me.value)'><br>"
    rr("<tr valign=top><td class=form_item1>���� ����<td class=form_item2>")
    rr "<input type=text value='" & getval(thedata1, "dealitem") & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""dealitem"", me.value)'><br>"
    rr("<tr valign=top><td class=form_item1>������� �����<td class=form_item2>")
    rr "<input type=text value='" & getval(thedata1, "kmrate") & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""kmrate"", me.value)'><br>"

    rr "<tr valign=top><td class=form_item1>���� �<td class=form_item2>"
    v = getval(thedata1, "goodafter") : if v = "" then v = "1/1/1900"
    rr selectdate("f1.goodafter", v)
    checkformadd = checkformadd & " f1.data1.value = setval(f1.data1.value, ""goodafter"", f1.goodafter.value)" & vbcrlf

    rr "<tr valign=top><td class=form_item1>���� ��<td class=form_item2>"
    v = getval(thedata1, "gooduntil") : if v = "" then v = "1/1/1900"
    rr selectdate("f1.gooduntil", v)
    checkformadd = checkformadd & " f1.data1.value = setval(f1.data1.value, ""gooduntil"", f1.gooduntil.value)" & vbcrlf

  end if

']

'[  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
'[    rr("<tr valign=top id=courseid_tr0><td class=form_item1 id=courseid_tr1><span id=courseid_caption>~courserates_courseid~</span><td class=form_item2 id=courseid_tr2>")
'[    if getfield("box") = "courseid" then thecourseid = getfield("boxid")
'[    rr("<select name=courseid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~courserates_courseid~")
'[    sq = "select * from courses order by code,title"
  if oksee("courseid") then
    rr("<tr valign=top id=courseid_tr0><td class=form_item1 id=courseid_tr1><span id=courseid_caption>~courserates_courseid~</span><td class=form_item2 id=courseid_tr2>")
    if getfield("box") = "courseid" then thecourseid = getfield("boxid")
    rr("<select name=courseid dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : checkform'>")
    rr("<option value=0 style='color:bbbbfe;'>~courserates_courseid~")
    sq = "select * from courses where (id = " & thecourseid & " or siteid = " & thesiteid & " or siteid = 0) order by code,title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thecourseid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:courseid
'[  elseif okedit("courseid") then
  else
']
    rr("<input type=hidden name=courseid value=" & thecourseid & ">")
  end if'form:courseid

  if oksee("from1") and session(tablename & "containerfield") <> "from1" then
'[    rr("<tr valign=top id=from1_tr0><td class=form_item1 id=from1_tr1><span id=from1_caption>~courserates_from1~</span><td class=form_item2 id=from1_tr2>")
'[    if getfield("box") = "from1" then thefrom1 = getfield("boxid")
'[    rr("<select name=from1 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~courserates_from1~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Thefrom1) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:from1
    rr("<tr valign=top id=from1_tr0><td class=form_item1 id=from1_tr1>(~Or~) <span id=from1_caption>~courserates_from1~</span><td class=form_item2 id=from1_tr2>")
    if getfield("box") = "from1" then thefrom1 = getfield("boxid")
    rr("<select name=from1 dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~courserates_from1~")
    sq = "select id,title,siteid from stations where (siteid = " & thesiteid & " or siteid = 0) and special = 'on' or id = " & thefrom1 & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thefrom1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      if cstr("" & rs1("siteid")) = "0" then rr " (~All Sites~)"
      rs1.movenext
    loop
    rr("</select>")'form:from1
']
  elseif okedit("from1") then
    rr("<input type=hidden name=from1 value=" & thefrom1 & ">")
  end if'form:from1

  if oksee("to1") and session(tablename & "containerfield") <> "to1" then
    rr(" <span id=to1_caption class=form_item3>~courserates_to1~</span> ")
    if getfield("box") = "to1" then theto1 = getfield("boxid")
'[    rr("<select name=to1 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~courserates_to1~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Theto1) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:to1
    rr("<select name=to1 dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~courserates_to1~")
    sq = "select id,title,siteid from stations where (siteid = " & thesiteid & " or siteid = 0) and special = 'on' or id = " & theto1 & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theto1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      if cstr("" & rs1("siteid")) = "0" then rr " (~All Sites~)"
      rs1.movenext
    loop
    rr("</select>")'form:to1
']
  elseif okedit("to1") then
    rr("<input type=hidden name=to1 value=" & theto1 & ">")
  end if'form:to1

  if oksee("areaid1") and session(tablename & "containerfield") <> "areaid1" then
'[    rr("<tr valign=top id=areaid1_tr0><td class=form_item1 id=areaid1_tr1><span id=areaid1_caption>~courserates_areaid1~</span><td class=form_item2 id=areaid1_tr2>")
    rr("<tr valign=top id=areaid1_tr0><td class=form_item1 id=areaid1_tr1><span id=areaid1_caption>(~Or~) ~courserates_areaid1~</span><td class=form_item2 id=areaid1_tr2>")
']
    if getfield("box") = "areaid1" then theareaid1 = getfield("boxid")
    rr("<select name=areaid1 dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~courserates_areaid1~")
    sq = "select * from areas order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theareaid1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:areaid1
  elseif okedit("areaid1") then
    rr("<input type=hidden name=areaid1 value=" & theareaid1 & ">")
  end if'form:areaid1

  if oksee("areaid2") and session(tablename & "containerfield") <> "areaid2" then
    rr(" <span id=areaid2_caption class=form_item3>~courserates_areaid2~</span> ")
    if getfield("box") = "areaid2" then theareaid2 = getfield("boxid")
    rr("<select name=areaid2 dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~courserates_areaid2~")
    sq = "select * from areas order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theareaid2) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:areaid2
  elseif okedit("areaid2") then
    rr("<input type=hidden name=areaid2 value=" & theareaid2 & ">")
'[  end if'form:areaid2
  end if'form:areaid2

  if specs = "iec" then
    f = "areaids1"
    rr("<tr valign=top><td class=form_item1>(~Or~) �������<td class=form_item2>")
    rr(selectlookupmultiauto("f1." & f, getval(thedata1, f), "icons/project.png", "areas,title,", 300, okwritehere))
    checkformadd = checkformadd & " f1.data1.value = setval(f1.data1.value, """ & f & """, f1." & f & ".value)" & vbcrlf
    checkformadd = checkformadd & " vv = f1." & f & ".value : vv = ""af"" & replace(vv, "","", ""af"") : f1.data1.value = setval(f1.data1.value, ""af"", vv)" & vbcrlf
    f = "areaids2"
    rr("<tr valign=top><td class=form_item1>�������<td class=form_item2>")
    rr(selectlookupmultiauto("f1." & f, getval(thedata1, f), "icons/project.png", "areas,title,", 300, okwritehere))
    checkformadd = checkformadd & " f1.data1.value = setval(f1.data1.value, """ & f & """, f1." & f & ".value)" & vbcrlf
    checkformadd = checkformadd & " vv = f1." & f & ".value : vv = ""at"" & replace(vv, "","", ""at"") : f1.data1.value = setval(f1.data1.value, ""at"", vv)" & vbcrlf
  end if

  if specs = "kmc" then
    rr("<tr valign=top><td class=form_item1>����� ������<td class=form_item2>")
    rr "<input type=checkbox name=specificway" : if getval(thedata1, "specificway") = "on" then rr " checked"
    rr " onchange='vbscript: t = iif(me.checked, ""on"", """") : f1.data1.value = setval(f1.data1.value, ""specificway"", t)'"
    rr ">���� ��� ������� �������� ������ ����� ����, ����� ���� ������ ����"
  end if
']

  if oksee("activeproduction") then'form:activeproduction
    rr("<tr valign=top id=activeproduction_tr0><td class=form_item1 id=activeproduction_tr1><span id=activeproduction_caption>~courserates_activeproduction~</span><td class=form_item2 id=activeproduction_tr2>")
    rr("<input type=checkbox name=activeproduction")
    if theactiveproduction = "on" then rr(" checked")
    rr(">")
  end if'form:activeproduction

  if oksee("active") then'form:active
    rr("<tr valign=top id=active_tr0><td class=form_item1 id=active_tr1><span id=active_caption>~courserates_active~</span><td class=form_item2 id=active_tr2>")
    rr("<input type=checkbox name=active")
    if theactive = "on" then rr(" checked")
    rr(">")
  end if'form:active

  if oksee("special") then'form:special
    rr("<tr valign=top id=special_tr0><td class=form_item1 id=special_tr1><span id=special_caption>~courserates_special~</span><td class=form_item2 id=special_tr2>")
    rr("<input type=checkbox name=special")
    if thespecial = "on" then rr(" checked")
    rr(">")
  end if'form:special

  if oksee("duration") then'form:duration
'[    rr("<tr valign=top id=duration_tr0><td class=form_item1 id=duration_tr1><span id=duration_caption>~courserates_duration~</span><td class=form_item2 id=duration_tr2>")
'[    rr("<input type=text maxlength=20 name=duration onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theduration & """ dir=ltr>")
    if cstr(thecourseid) = "0" then
      rr("<tr valign=top id=duration_tr0><td class=form_item1 id=duration_tr1><span id=duration_caption>~courserates_duration~</span><td class=form_item2 id=duration_tr2>")
      rr("<input type=text maxlength=20 name=duration onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & Theduration & """ dir=ltr>")
    else
      rr "<input type=hidden name=duration value=" & theduration & ">"
    end if
    if specs = "egged" then
      rr " <b>�����</b> <input type=text value='" & getval(thedata1, "duration_a") & "' style='direction:ltr; width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""duration_a"", t)'>"
      rr " <b>�����</b> <input type=text value='" & getval(thedata1, "duration_b") & "' style='direction:ltr; width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""duration_b"", t)'>"
    end if
']
  end if'form:duration

  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then
    rr("<tr valign=top id=supplierid_tr0><td class=form_item1 id=supplierid_tr1><span id=supplierid_caption>~courserates_supplierid~</span><td class=form_item2 id=supplierid_tr2>")
    if getfield("box") = "supplierid" then thesupplierid = getfield("boxid")
'[    rr("<select name=supplierid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~courserates_supplierid~")
'[    sq = "select * from suppliers order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Thesupplierid) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:supplierid
'[  elseif okedit("supplierid") then
    rr "<select name=supplierid dir=~langdir~"
    if specs = "elbit" then rr " onchange='vbscript: f1.action2.value = ""stay"" : checkform'"
    rr ">"
    rr("<option value=0 style='color:bbbbfe;'>~courserates_supplierid~")
    sq = "select * from suppliers where id = " & thesupplierid & " or 0=0"
    sq = sq & " and siteid in(" & thesiteid & ",0)"
    sq = sq & " and (instr(cstr(',' & data1), 'activeproduction=on') + instr(cstr(',' & data1), 'activespecial=on') > 0)"
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesupplierid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      d = rs1("data1") : if specs = "elbit" and getval(d, "activespecial") = "" then rr "(�� ����)"
      rs1.movenext
    loop
    rr("</select>")'form:supplierid

    if specs = "elbit" then
      rr("<tr valign=top><td class=form_item1>~orders_carid~<td class=form_item2>")
      s = "select id,title,code from cars where id = 0" & getval(thedata1, "carid") & " or supplierid in(0" & thesupplierid & ",0) order by title, code"
      set rs1 = conn.execute(sqlfilter(s))
      rr "<select name=carid id=additionals_migdar onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""carid"", me.value)'>"
      rr "<option value=''>"
      do until rs1.eof
        rr "<option value=" & rs1("id") : if getval(thedata1, "carid") = cstr(rs1("id")) then rr " selected"
        rr ">" & rs1("title") & " " & rs1("code")
        rs1.movenext
      loop
      rr "</select>"
    end if
  else
']
    rr("<input type=hidden name=supplierid value=" & thesupplierid & ">")
  end if'form:supplierid

  if oksee("passengers") then'form:passengers
    rr("<tr valign=top id=passengers_tr0><td class=form_item1 id=passengers_tr1><span id=passengers_caption>~courserates_passengers~<img src=must.gif></span><td class=form_item2 id=passengers_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=passengers value=""" & Thepassengers & """>")
  end if'form:passengers

'[  if oksee("price") then'form:price
'[    rr("<tr valign=top id=price_tr0><td class=form_item1 id=price_tr1><span id=price_caption>~courserates_price~<img src=must.gif></span><td class=form_item2 id=price_tr2>")
'[    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=price value=""" & Theprice & """>")
'[  end if'form:price

  if specs = "rail" then
    rr "<tr valign=top><td class=form_item1>�������<td class=form_item2>"
    rr "<input type=text id=category style='width:100; direction:ltr;' value='" & getval(thedata1, "category") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""category"", me.value)'>"    
  end if

  '-----------availability
  rr "<tr valign=top><td class=form_item1>~Availability~<td class=form_item2>"
  aa = getval(thedata1, "ava")
  'rr "<input type=text name=ava value='" & aa & "' dir=ltr style='width:700;'><br>"
  rr "<input type=hidden name=ava value='" & aa & "'>"
  i = 0 : ss = ""
  do until aa = ""
    i = i + 1
    days = left(aa,instr(aa,";")-1) : aa = mid(aa,instr(aa,";")+1)
    hour1 = left(aa,instr(aa,";")-1) : aa = mid(aa,instr(aa,";")+1)
    hour1b = left(aa,instr(aa,";")-1) : aa = mid(aa,instr(aa,";")+1)
    rr "<div style='background:#eeeeee; padding:6; border:1px solid #dddddd; margin-bottom:5;'>"
    rr "<a href=#aa onclick='vbscript: f1.action2.value = ""stay"" : f1.action3.value = ""avadel" & i & """ : checkform'><img src=minus.gif border=0></a> "
    rr("&nbsp;&nbsp;&nbsp;<input type=text maxlength=5 name=hour1" & i & " onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & hour1 & """ dir=ltr onchange='vbscript:updateava'>")
    rr(" ~To~ <input type=text maxlength=5 name=hour1b" & i & " onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & hour1b & """ dir=ltr onchange='vbscript:updateava'>")
    c = 0 : a = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
    rr "&nbsp;&nbsp;"' <b>~Days~</b> "
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
      rr("<input type=checkbox name=days" & i & "_c" & c)
      if instr("," & days & ",","," & b & ",") > 0 then rr(" checked")
      rr(" onclick='vbscript:updateava'>")
      rr("~weekday" & b & "~ &nbsp;&nbsp;&nbsp;")
      ss = ss & "if f1.days" & i & "_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
    loop
    rr "</div>"
    ss = ss & " t = t & "";"" & replace(f1.hour1" & i & ".value, "";"", "":"") & "";"" & replace(f1.hour1b" & i & ".value, "";"", "":"") & "";""" & vbcrlf
  loop
  rr "&nbsp;&nbsp;<a href=#aa onclick='vbscript: f1.action2.value = ""stay"" : f1.action3.value = ""avaadd"" : checkform' style='color:#0000aa;'><img src=plus.gif border=0>&nbsp;&nbsp;&nbsp; ~Add~ ~availability~</a>"
  rr("<script language=vbscript>" & vbcrlf)
  rr("  sub updateava" & vbcrlf)
  rr("    t = """"" & vbcrlf)
  rr(ss & vbcrlf)
  rr("    f1.ava.value = t" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</script>" & vbcrlf)

  laststationid = ""
  if hasmatch(specs, "brom,dsw,") then
    rr("<tr valign=top><td class=form_item1>���� ������ ������ ����<td class=form_item2>")
    rr "<input type=text value='" & getval(thedata1, "budgettype") & "' style='width:250;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""budgettype"", me.value)'><br>"
  end if

  rr("<tr valign=top><td class=form_item1>~Relative price~<td class=form_item2>")
  if cstr(thecourseid) = "0" then
    rr "<input type=checkbox disabled>"
  else
    t = getval(thedata1, "relativeprice") : if t = "on" then t = "checked" else t = ""
    rr "<input type=checkbox id=relativeprice " & t & " onclick='vbscript: showhiderelativeprices'>"
    rr "<div id=relativeprices style='width:500;'>"
    rr "<span style='width:250; overflow:hidden; color:#800000;'>~Extra station addition~</span>"
    rr "<input type=text value='" & getval(thedata1, "entrance") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""entrance"", t)'><br>"
    rr "<span style='width:250; overflow:hidden; color:#800000;'>~Extra area addition~</span>"
    rr "<input type=text value='" & getval(thedata1, "entrancearea") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""entrancearea"", t)'><br>"
    if specs = "laufer" then
      rr "<span style='width:250; overflow:hidden; color:#800000;'>���� ����� ����� ����</span>"
      rr "<input type=text value='" & getval(thedata1, "stop2") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""stop2"", t)'><br>"
      rr "<span style='width:250; overflow:hidden; color:#800000;'>���� ����� ����� ������ ����� - ��� ���</span>"
      rr "<input type=text value='" & getval(thedata1, "stop3") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""stop3"", t)'><br>"
      rr "<span style='width:250; overflow:hidden; color:#800000;'>���� ����� ����� ������ - ��� �����</span>"
      rr "<input type=text value='" & getval(thedata1, "stop3b") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""stop3b"", t)'><br>"
      rr "<span style='width:250; overflow:hidden; color:#800000;'>���� ����� ����� ������ �����  - ��� �����</span>"
      rr "<input type=text value='" & getval(thedata1, "stop4b") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""stop4b"", t)'><br>"
    elseif specs = "metropolin" then
      rr "<span style='width:250; overflow:hidden; color:#800000;'>���� ����� ����� ����� �����</span>"
      rr "<input type=text value='" & getval(thedata1, "stop1") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""stop1"", t)'><br>"
      rr "<span style='width:250; overflow:hidden; color:#800000;'>���� ����� ����� ����� ���</span>"
      rr "<input type=text value='" & getval(thedata1, "stop2") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""stop2"", t)'><br>"
    else
      rr "<span style='width:250; overflow:hidden; color:#800000;'>~Extra passenger addition~</span>"
      rr "<input type=text value='" & getval(thedata1, "stop") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""stop"", t)'><br>"
    end if
    if hasmatch("tm,hatama,", specs) then
      rr "<span style='width:250; overflow:hidden; color:#800000;'>���� ���� ���� ������</span>"
      rr "<input type=text value='" & getval(thedata1, "addsta") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""addsta"", t)'><br>"
      rr "<span style='width:250; overflow:hidden; color:#800000;'>���� ���� ���� ���� ������</span>"
      rr "<input type=text value='" & getval(thedata1, "addsta2") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""addsta2"", t)'><br>"
      rr "<span style='width:250; overflow:hidden; color:#800000;'>����� ���� ����� ����� �����</span>"
      rr "<input type=text value='" & getval(thedata1, "threestations") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""threestations"", t)'><br>"
    end if
    show_special_duration = ""

    'if instr(",laufer,shufersal,iai,sheleg,ap,tm,hatama,dexcel,kla,rafael,imi,metropolin,", "," & specs & ",") > 0 then
      show_special_duration = "on"
      rr "<span style='width:250; overflow:hidden; color:#000080;'>~Minutes per station~</span>"
      rr "<input type=text value='" & getval(thedata1, "dur_s") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""dur_s"", t)'><br>"
      'rr "<span style='width:250; overflow:hidden; color:#000080;'>���� �����</span>"
      'rr "<input type=text value='" & getval(thedata1, "dur_a") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""dur_a"", t)'><br>"
      rr "<span style='width:250; overflow:hidden; color:#000080;'>~Minutes per passenger~</span>"
      rr "<input type=text value='" & getval(thedata1, "dur_p") & "' style='width:50;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""dur_p"", t)'><br>"
    'end if

    rr "<span style='width:250; overflow:hidden; color:#bbbbbb;'>~Collect-from / Distribute-to station price~</span>"
    rr "<span style='width:63; overflow:hidden; color:#bbbbbb;'>~Price~</span>"
    if hasmatch(specs, "brom,dsw,") then rr "<span style='width:100; overflow:hidden; color:#bbbbbb;'>���� ������</span>" 'budgettype
    if showratecode = "on" then rr "<span style='width:63; overflow:hidden; color:#bbbbbb;'>��� ������</span>"
    if show_special_duration = "on" then
      rr "<span style='width:63; overflow:hidden; color:#bbbbbb;'>~courserates_duration~</span>"
      rr "<span style='width:63; overflow:hidden; color:#bbbbbb;'>~additions_addition~</span>"
    end if
    if specs = "metropolin" then
      rr "<span style='width:63; overflow:hidden; color:#bbbbbb;'>~Night or weekend~</span>" 'night_
    end if

		if specs = "rafael" then du = getpricebyratecode()
		function getpricebyratecode() 'for rafael only
      'USES thedata1, thecourseid, thesiteid, thesupplierid, thepassengers
      'SETS thedata1, theprice
		  dim t,v,i,maxprice,s,rs
		  s = "select stationid from coursestations where courseid = " & thecourseid & " order by minutes"
		  set rs = conn.execute(sqlfilter(s))
		  maxprice = 0
		  do until rs.eof
		    i = rs("stationid")
		    t = getval(thedata1, "ratecode" & i)
		    v = getonefield("select price from courserates where siteid = 0" & thesiteid & " and supplierid = 0" & thesupplierid & " and passengers = 0" & thepassengers & " and instr(cstr('' & data1), '|relativeprice=on|') = 0 and instr(cstr('' & data1), '|ratecode=" & t & "|') > 0")
		    v = cdbl("0" & strfilter(v, "0123456789.")) + cdbl("0" & strfilter(getval(thedata1, "ent_" & i), "0123456789."))
		    thedata1 = setval(thedata1, "r" & i, v)
		    if cdbl(v) > maxprice then maxprice = cdbl(v)
		    rs.movenext
		  loop
		  if maxprice > 0 then theprice = maxprice
		end function

    rr "<img src=blank.gif width=50 height=18><br>"
    s = "select coursestations.stationid as id, stations.title as title from coursestations, stations where coursestations.courseid = " & thecourseid & " and coursestations.stationid = stations.id order by coursestations.minutes"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      i = rs("id") : t = rs("title") : a = getidlist("select areaid from stations where id = " & i) : if a <> "" then a = left(a,instr(a,",")-1) : a = gettitle("areas", a) : if a <> "" then t = "(" & a & ") " & t
      rs.movenext : if rs.eof then laststationid = i : exit do
      rr "<span style='width:250; overflow:hidden;'>" & t & "</span>"

      if specs = "rafael" then
        v = getval(thedata1, "r" & i)
        rr "<input type=text disabled value='" & v & "' style='width:60;'": 
        if v = "" then rr " style='background:#ff8080;'"
        rr ">"
      else
        rr "<input type=text value='" & getval(thedata1, "r" & i) & "' style='width:60;' onchange='vbscript: t = me.value : t = strfilter(t, ""0123456789."") : me.value = t : f1.data1.value = setval(f1.data1.value, ""r" & i & """, t)'>"
      end if

      if hasmatch(specs, "brom,dsw,") then rr " <input type=text value='" & getval(thedata1, "budgettype" & i) & "' style='width:150;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""budgettype" & i & """, me.value)'>"
      if showratecode = "on" then rr " <input type=text value='" & getval(thedata1, "ratecode" & i) & "' style='width:60;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""ratecode" & i & """, me.value)'>"
      if show_special_duration = "on" then
        rr " <input type=text value='" & getval(thedata1, "dur_" & i) & "' style='width:60;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""dur_" & i & """, me.value)'>"
        rr " <input type=text value='" & getval(thedata1, "ent_" & i) & "' style='width:60;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""ent_" & i & """, me.value)'>"
      end if
      if specs = "metropolin" then
        rr " <input type=text value='" & getval(thedata1, "night_" & i) & "' style='width:60;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""night_" & i & """, me.value)'>"
      end if
      rr "<BR>"
    loop

    rr "</div>"
    rr "<script language=vbscript>" & vbcrlf
    rr "sub showhiderelativeprices" & vbcrlf
    rr "  if document.getelementbyid(""relativeprice"").checked then" & vbcrlf
    rr "    document.getelementbyid(""relativeprices"").style.visibility = ""visible""" & vbcrlf
    rr "    document.getelementbyid(""relativeprices"").style.position = ""relative""" & vbcrlf
    rr "    f1.data1.value = setval(f1.data1.value, ""relativeprice"", ""on"")" & vbcrlf
    'if specs = "iai" then rr " price_tr0.style.visibility = ""hidden"": price_tr0.style.position = ""absolute""" & vbcrlf
    if childpage then rr "    document.getelementbyid(""strechmyiframe"").click" & vbcrlf
    rr "  else" & vbcrlf
    rr "    document.getelementbyid(""relativeprices"").style.visibility = ""hidden""" & vbcrlf
    rr "    document.getelementbyid(""relativeprices"").style.position = ""absolute""" & vbcrlf
    rr "    f1.data1.value = setval(f1.data1.value, ""relativeprice"", """")" & vbcrlf
    'if specs = "iai" then rr " price_tr0.style.visibility = ""visible"": price_tr0.style.position = ""relative""" & vbcrlf
    rr "  end if" & vbcrlf
    rr "end sub" & vbcrlf
    rr "showhiderelativeprices" & vbcrlf
    rr "</script>" & vbcrlf
  end if

  if specs = "rafael" and (session("usergroup") <> "finance" and session("userlevel") <> "5") then disablecontrols = disablecontrols & "price,"
  if oksee("price") then'form:price
    rr("<tr valign=top id=price_tr0><td class=form_item1 id=price_tr1><span id=price_caption>~courserates_price~<img src=must.gif></span><td class=form_item2 id=price_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=price value=""" & Theprice & """>")
  end if'form:price

  if specs = "osem" then
    rr("&nbsp; ��� GLOB ��� &nbsp;")
    rr "<input type=text value='" & getval(thedata1, "glob1") & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""glob1"", me.value)'><br>"
  end if

  if showdriverrate = "on" then
    rr("<tr valign=top><td class=form_item1>���� ���<td class=form_item2>")
    rr "<input type=text value='" & getval(thedata1, "driverrate") & "' style='width:70; direction:ltr;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""driverrate"", me.value)'>"
    rr " <img src=icons/i_help.gif alt='���� ����� ���� ���� (20.40) �� ������ ������ ����� ������� (80%)'>"
    rr " ����� ���� ���� "
    rr "<input type=text value='" & getval(thedata1, "driverratenight") & "' style='width:70; direction:ltr;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""driverratenight"", me.value)'>"
    rr " <img src=icons/i_help.gif alt='���� ����� ���� ���� (20.40) �� ������ ������ ����� ������� (80%)'>"
    rr " ����� ���� ���� "
    rr "<input type=text value='" & getval(thedata1, "driverrateweekend") & "' style='width:70; direction:ltr;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""driverrateweekend"", me.value)'>"
    rr " <img src=icons/i_help.gif alt='���� ����� ���� ���� (20.40) �� ������ ������ ����� ������� (80%)'>"
  end if

  'if cstr(thecourseid) = "0" and getparam("showordertaxi") = "y" then
  if getparam("UseGoogleMaps") = "y" then
    rr "<tr valign=top><td class=form_item1 style='color:#0000aa;'>����� ����� �����<td class=form_item2>"
    rr "<input type=text id=add_area style='width:100; direction:ltr;' value='" & getval(thedata1, "add_area") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""add_area"", me.value)'>"
    rr "<tr valign=top><td class=form_item1 style='color:#0000aa;'>����� ����� �����<td class=form_item2>"
    rr "<input type=text id=add_station style='width:100; direction:ltr;' value='" & getval(thedata1, "add_station") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""add_station"", me.value)'>"
    rr "<tr valign=top><td class=form_item1 style='color:#0000aa;'>����� ��� ����<td class=form_item2>"
    rr "<input type=text id=add_passenger style='width:100; direction:ltr;' value='" & getval(thedata1, "add_passenger") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""add_passenger"", me.value)'>"
    'if instr(siteurl, "/bbalev.moovex.net/") + instr(siteurl, "/127.0.0.1/") + instr(siteurl, "/localhost/") > 0 then
	    rr "<tr valign=top><td class=form_item1 style='color:#0000aa;'>����� ����� ���� ����<td class=form_item2>"
	    rr "<input type=text id=add_city style='width:100; direction:ltr;' value='" & getval(thedata1, "add_city") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""add_city"", me.value)'>"
    'end if
  end if

  if hasmatch(specs, "shufersal,philips,iwi,egged,merkazny,") and cstr(theareaid1) <> "0" and cstr(theareaid2) <> "0" and cstr(theareaid1) <> "" and cstr(theareaid2) <> "" then
    ii = getfieldlist("select areaids from areas where id in(" & theareaid1 & "," & theareaid2 & ")")
    ii = replace(ii, ",", "") : ii = replace(ii, ";", ",")
    do until ii = ""
      i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
      rr "<tr valign=top><td class=form_item1 style='color:#0000aa;'>- ���� �� ����� �" & gettitle("areas", i) & "<td class=form_item2>"
      rr "<input type=text id=add_area_" & i & " style='width:100; direction:ltr;' value='" & getval(thedata1, "add_area_" & i) & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""add_area_" & i & """, me.value)'>"
    loop
    rr "<input type=hidden name=add_area_doupdate value=on>"
  end if

  rr "<tr valign=top><td class=form_item1>~Rush hour addition~<td class=form_item2>"
  rr "<input type=text id=addtoll style='width:70; direction:ltr;' value='" & getval(thedata1, "addtoll") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""addtoll"", me.value)'>"
  rr " ~disablehours_hour1~  <input type=textg id=addtollh1 style='width:70; direction:ltr;' value='" & getval(thedata1, "addtollh1") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""addtollh1"", me.value)'>"
  rr " ~disablehours_hour2~ <input type=textg id=addtollh2 style='width:70; direction:ltr;' value='" & getval(thedata1, "addtollh2") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""addtollh2"", me.value)'>"

  if instr(",rail,", "," & specs & ",") > 0 then
    rr "<tr valign=top><td class=form_item1>����� ������ �� ���� ��� �����<td class=form_item2>"
    rr "<input type=text id=addstop style='width:100; direction:ltr;' value='" & getval(thedata1, "addstop") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""addstop"", me.value)'>"
    rr "<tr valign=top><td class=form_item1>���� ����� ��� ���<td class=form_item2>"
    rr "<input type=text id=weekdayreduction style='width:100; direction:ltr;' value='" & getval(thedata1, "weekdayreduction") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""weekdayreduction"", me.value)'>"
    rr " <img src=icons/i_help.gif alt='~���� ����� ���� ������ (20.40) �� ������� (12%)~'>"
  end if

  rr "<tr valign=top><td class=form_item1>~Collect addition~<td class=form_item2>"
  rr "<input type=text id=addcollect style='width:100; direction:ltr;' value='" & getval(thedata1, "addcollect") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""addcollect"", me.value)'>"
  rr "<tr valign=top><td class=form_item1>~Distribute addition~<td class=form_item2>"
  rr "<input type=text id=adddistribute style='width:100; direction:ltr;' value='" & getval(thedata1, "adddistribute") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""adddistribute"", me.value)'>"

']
  if oksee("addnight") then'form:addnight
    rr("<tr valign=top id=addnight_tr0><td class=form_item1 id=addnight_tr1><span id=addnight_caption>~courserates_addnight~</span><td class=form_item2 id=addnight_tr2>")
'[    rr("<input type=text maxlength=10 name=addnight onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theaddnight & """ dir=ltr>")
'[  end if'form:addnight
    rr("<input type=text maxlength=10 name=addnight onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & Theaddnight & """ dir=ltr>")
    rr " <img src=icons/i_help.gif alt='~Can specify fixed price(20.40) or additional percent (12%)~. �� ����� ����� ������ ���� - ��� ������.'>"
  end if'form:addnight

  if specs = "osem" then
    rr("&nbsp; ��� GLOB ���� &nbsp;")
    rr "<input type=text value='" & getval(thedata1, "glob2") & "' style='width:100;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""glob2"", me.value)'><br>"
  end if

']


  if oksee("addweekend") then'form:addweekend
    rr("<tr valign=top id=addweekend_tr0><td class=form_item1 id=addweekend_tr1><span id=addweekend_caption>~courserates_addweekend~</span><td class=form_item2 id=addweekend_tr2>")
'[    rr("<input type=text maxlength=10 name=addweekend onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theaddweekend & """ dir=ltr>")
    rr("<input type=text maxlength=10 name=addweekend onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & Theaddweekend & """ dir=ltr>")
    rr " <img src=icons/i_help.gif alt='~Can specify fixed price(20.40) or additional percent (12%)~ - ~If ride is on weekend, night rate will not apply~. �� ����� ����� ������ ���� - ��� ������.'>"
']
  end if'form:addweekend

  if oksee("returndiscount") then'form:returndiscount
    rr("<tr valign=top id=returndiscount_tr0><td class=form_item1 id=returndiscount_tr1><span id=returndiscount_caption>~courserates_returndiscount~</span><td class=form_item2 id=returndiscount_tr2>")
'[    rr("<input type=text maxlength=10 name=returndiscount onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thereturndiscount & """ dir=ltr>")
    rr("<input type=text maxlength=10 name=returndiscount onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & Thereturndiscount & """ dir=ltr>")
    rr " <img src=icons/i_help.gif alt='~Can specify fixed price(20.40) or additional percent (12%)~. �� ����� ����� ������ ���� - ��� ������.'>"
']
  end if'form:returndiscount

  if oksee("ordertypeidsv") then'form:ordertypeidsv
    rr("<span id=ordertypeidsv_caption></span> ")
    rr("<input type=hidden name=ordertypeidsv value=""" & Theordertypeidsv & """>")
  end if'form:ordertypeidsv

  if oksee("ordertypeids") then'form:ordertypeids
    rr("<tr valign=top id=ordertypeids_tr0><td class=form_item1 id=ordertypeids_tr1><span id=ordertypeids_caption>~courserates_ordertypeids~</span><td class=form_item2 id=ordertypeids_tr2>")
'[    rr(selectlookupmultiauto("f1.ordertypeids",theordertypeids, "icons/order-1.png", "ordertypes,title,", 300, okwritehere))
'[  end if'form:ordertypeids
    rr "<span style='position:absolute;'><img src=blank.gif width=370 height=1> <img src=icons/i_help.gif style='margin-top:2;' alt='������� ������� �� ����� ������ �������� ���. �� �� ���� ���, ������� ���� ���� ��� ���� ������. ��� ��� ����� ���� ����� ����� ���� (20.40) �� ����� ����� (12%). �� ����� ����� ������ ���� - ��� ������.'></span>"
    t = ":select id,title from ordertypes where id in(" & theordertypeids & "-1) or active = 'on' order by title"
    rr(selectlookupmultiauto("f1.ordertypeids",theordertypeids, "icons/order-1.png", "ordertypes,title," & t, 300, okwritehere))
  end if'form:ordertypeids
  rr "<tr valign=top><td class=form_item1>~Automatic Additions~<td class=form_item2>"
  rr("<input type=hidden name=additionidsv value=""" & replace(getval(thedata1, "additionidsv"), "\", "|") & """>")
  rr(selectlookupmultiauto("f1.additionids", getval(thedata1, "additionids"), "icons/plus.png", "additions,title,", 300, okwritehere))
']

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~courserates_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid onchange='vbscript: f1.action2.value = ""stay"" : f1.action3.value = ""justrefreshsiteid"" : checkform'>")
    rr("<option value=0 style='color:bbbbfe;'>~courserates_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
'[  elseif okedit("siteid") then
'[    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
'[  end if'form:siteid
  else
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

  rr("<tr valign=top id=retro_tr0><td class=form_item1 id=retro_tr1><span id=retro_caption></span><td class=form_item2 id=retro_tr2>")
  rr "<input type=checkbox name=retro> ~Update orders retroactively~"
  rr(" ~From~" & SelectDate("f1.d1*:", date & " 00:00:00"))
  rr " <input type=checkbox name=retroallstatuses>~Orders in all statuses (not only `New`)~"
']

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
'[    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
    if laststationid <> "" then thedata1 = setval(thedata1, "dur_" & laststationid, "0")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
    'rr("<br><input type=text style='width:500;' name=data1 value=""" & Thedata1 & """>")
']
  end if'form:data1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonduplicate value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

'[  rr("</table>")
  rr("</table>")
  rr "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>" 'for the relative prices div when inside container course form..

']

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
'[  rr(checkformadd)
  rr(checkformadd)
  rr("    ok = true" & vbcrlf)
  rr "f1.data1.value = setval(f1.data1.value, ""additionids"", f1.additionids.value)" & vbcrlf
  rr "f1.data1.value = setval(f1.data1.value, ""additionidsv"", replace(f1.additionidsv.value,""|"",""\""))" & vbcrlf
  rr "f1.data1.value = setval(f1.data1.value, ""ava"", f1.ava.value)" & vbcrlf
']
  if okedit("passengers") and session(tablename & "containerfield") <> "passengers" then
    rr("    if isnumeric(f1.passengers.value) then" & vbcrlf)
    rr("      if cdbl(f1.passengers.value) >= 0 and cdbl(f1.passengers.value) <= 1000 then" & vbcrlf)
    rr("        validfield(""passengers"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""passengers"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""passengers"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("price") and session(tablename & "containerfield") <> "price" then
    rr("    if isnumeric(f1.price.value) then" & vbcrlf)
    rr("      if cdbl(f1.price.value) >= 0 and cdbl(f1.price.value) <= 10000 then" & vbcrlf)
    rr("        validfield(""price"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""price"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""price"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.activeproduction.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".courseid" : session(tablename & "_hidelistcolumns") = "ordertypeidsv,data1,"
'[    session(tablename & "_firstcome") = "y"
    session(tablename & "_firstcome") = "y"
    session("courserates_type1") = "hideinactivecourses"
']
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".courseid"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/cost.png' ~iconsize~> ~courserates_courserates~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
'[  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")

  if showratecode = "on" then
    rr " <nobr> ��� <input type=text name=ratecode value='" & session(tablename & "_ratecode") & "' style='width:100;'>"
  end if

  rr(" <nobr><img id=type1_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr " <select name=type1>"
  rr("<option value='' style='color:bbbbfe;'>~Course Rate~")
  rr "<option value=allcourses" : if session("courserates_type1") = "allcourses" then rr " selected"
  rr ">~All Courses~"
  rr "<option value=activecourses" : if session("courserates_type1") = "activecourses" then rr " selected"
  rr ">~Only active courses~"
  rr "<option value=inactivecourses" : if session("courserates_type1") = "inactivecourses" then rr " selected"
  rr ">~Inactive and deleted courses~"
  rr "<option value=hideinactivecourses" : if session("courserates_type1") = "hideinactivecourses" then rr " selected"
  rr ">~Active courses and from-to rates~"
  rr "<option value=fromto" : if session("courserates_type1") = "fromto" then rr " selected"
  rr ">~From-to rides~"
  rr "</select></nobr>"

']
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  if session(tablename & "containerfield") <> "courseid" then
    rr(" <nobr><img id=courseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=courseid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~courserates_courseid~")
'[    sq = "select * from courses order by code,title"
    rr("<option value='' style='color:bbbbfe;'>~courserates_courseid~")
    rr "<option value='0'" : if cstr(session(tablename & "_courseid")) = "0" then rr " selected"
    rr ">(~None~)"
    sq = "select * from courses"
    if session("usersiteid") <> "0" then sq = sq & " where siteid = " & session("usersiteid")
    sq = sq & " order by code,title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_courseid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:courseid
  end if'search:courseid
  if session(tablename & "containerfield") <> "areaid1" then
    rr(" <nobr><img id=areaid1_filtericon src=icons/design.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=areaid1 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~courserates_areaid1~")
    rr("<option value='' style='color:bbbbfe;'>~courserates_areaid1~")
    rr "<option value='0'" : if cstr(session(tablename & "_areaid1")) = "0" then rr " selected"
    rr ">(~None~)"
']
    sq = "select * from areas order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_areaid1")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:areaid1
  end if'search:areaid1
  if session(tablename & "containerfield") <> "areaid2" then
    rr(" <nobr><img id=areaid2_filtericon src=icons/design.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=areaid2 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~courserates_areaid2~")
    rr("<option value='' style='color:bbbbfe;'>~courserates_areaid2~")
    rr "<option value='0'" : if cstr(session(tablename & "_areaid2")) = "0" then rr " selected"
    rr ">(~None~)"
']
    sq = "select * from areas order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_areaid2")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:areaid2
  end if'search:areaid2
  rr(" <nobr><img id=active_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=active dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~courserates_active~")
  rr("<option value=on") : if lcase(session(tablename & "_active")) = "on" then rr(" selected")
  rr("> ~courserates_active~: ~Yes~")
  rr("<option value=no") : if lcase(session(tablename & "_active")) = "no" then rr(" selected")
  rr("> ~courserates_active~: ~No~")
  rr("</select></nobr>")'search:active
  rr(" <nobr><img id=special_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=special dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~courserates_special~")
  rr("<option value=on") : if lcase(session(tablename & "_special")) = "on" then rr(" selected")
  rr("> ~courserates_special~: ~Yes~")
  rr("<option value=no") : if lcase(session(tablename & "_special")) = "no" then rr(" selected")
  rr("> ~courserates_special~: ~No~")
  rr("</select></nobr>")'search:special
  if session(tablename & "containerfield") <> "supplierid" then
    rr(" <nobr><img id=supplierid_filtericon src=icons/i_key.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=supplierid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~courserates_supplierid~")
'[    sq = "select * from suppliers order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_supplierid")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:supplierid
    rr("<option value='' style='color:bbbbfe;'>~courserates_supplierid~")
    rr "<option value='0'" : if cstr(session(tablename & "_supplierid")) = "0" then rr " selected"
    rr ">(~None~)"
    sq = "select * from suppliers"
    if session("usersiteid") <> "0" then sq = sq & " where (siteid = " & session("usersiteid") & " or siteid = 0)"
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_supplierid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      d = rs1("data1") : if specs <> "elbit" and getval(d, "activespecial") = "" then rr "(�� ����)"
      rs1.movenext
    loop
    rr("</select></nobr>")'search:supplierid
']
  end if'search:supplierid
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~courserates_siteid~")
    rr("<option value='' style='color:bbbbfe;'>~courserates_siteid~")
    rr "<option value='0'" : if cstr(session(tablename & "_siteid")) = "0" then rr " selected"
    rr ">(~None~)"
']
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
'[  end if'search:siteid
  end if'search:siteid
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"

  rr " ~Type~ <select name=courseratetype>"
  rr "<option value=''>"
  rr "<option value=relative" : if session("courserates_courseratetype") = "relative" then rr " selected>~Relative price~" else rr ">~Relative price~"
  rr "<option value=notrelative" : if session("courserates_courseratetype") = "notrelative" then rr " selected>~No~ ~Relative price~" else rr ">~No~ ~Relative price~"
  rr "</select>"

  if specs = "rail" then
    rr " ������� <input type=text name=category style='width:150;' value='" & session(tablename & "_category") & "'>"
  end if

']
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_1.title) as from1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_1 on " & tablename & ".from1 = stations_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_2.title) as to1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_2 on " & tablename & ".to1 = stations_2.id)"
  s = replace(s, " from ", ", cstr('' & areas_1.title) as areaid1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join areas areas_1 on " & tablename & ".areaid1 = areas_1.id)"
  s = replace(s, " from ", ", cstr('' & areas_2.title) as areaid2lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join areas areas_2 on " & tablename & ".areaid2 = areas_2.id)"
  s = replace(s, " from ", ", cstr('' & suppliers_1.title) as supplieridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join suppliers suppliers_1 on " & tablename & ".supplierid = suppliers_1.id)"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

'[  '-sqladd
  if session("usersiteid") <> "0" then s = s & " and courseid in(" & getidlist("select id from courses where siteid = " & session("usersiteid")) & "0)"
  if session("courserates_courseratetype") = "relative" then s = s & " and instr(cstr(courserates.data1), 'relativeprice=on') > 0"
  if session("courserates_courseratetype") = "notrelative" then s = s & " and instr(cstr(courserates.data1), 'relativeprice=on') = 0"
  if session("courserates_type1") = "allcourses" then s = s & " and courserates.courseid > 0"
  if session("courserates_type1") = "activecourses" then s = s & " and courserates.courseid in(" & getidlist("select id from courses where active = 'on'") & "-1)"
  if session("courserates_type1") = "inactivecourses" then s = s & " and courserates.courseid not in(" & getidlist("select id from courses where active = 'on'") & "0)"
  if session("courserates_type1") = "hideinactivecourses" then s = s & " and courserates.courseid in(" & getidlist("select id from courses where active = 'on'") & "0)"
  if session("courserates_type1") = "fromto" then s = s & " and courserates.courseid = 0"
  t = session(tablename & "_ratecode") : if t = "���" then s = s & " and instr('' & cstr('' & courserates.data1), '|ratecode=') = 0" else if t <> "" then s = s & " and instr('' & cstr('' & courserates.data1), '|ratecode=" & t & "|') > 0"
  t = session(tablename & "_category") : if t = "���" then s = s & " and instr('' & cstr('' & courserates.data1), '|category=') = 0" else if t <> "" then s = s & " and instr('' & cstr('' & courserates.data1), '|category=" & t & "') > 0"
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","courserates.id,courserates.courseid,courserates.from1,courserates.to1,courserates.areaid1,courserates.areaid2,courserates.activeproduction,courserates.active,courserates.special,courserates.duration,courserates.supplierid,courserates.passengers,courserates.price,courserates.addnight,courserates.addweekend,courserates.returndiscount,courserates.ordertypeidsv,courserates.ordertypeids,courserates.siteid,courserates.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_courseid") <> "" and session(tablename & "containerfield") <> "courseid" then s = s & " and " & tablename & ".courseid = " & session(tablename & "_courseid")
  if session(tablename & "_areaid1") <> "" and session(tablename & "containerfield") <> "areaid1" then s = s & " and " & tablename & ".areaid1 = " & session(tablename & "_areaid1")
  if session(tablename & "_areaid2") <> "" and session(tablename & "containerfield") <> "areaid2" then s = s & " and " & tablename & ".areaid2 = " & session(tablename & "_areaid2")
  if session(tablename & "_active") = "on" then s = s & " and lcase('' & " & tablename & ".active) = 'on'"
  if session(tablename & "_active") = "no" then s = s & " and lcase('' & " & tablename & ".active) <> 'on'"
  if session(tablename & "_special") = "on" then s = s & " and lcase('' & " & tablename & ".special) = 'on'"
  if session(tablename & "_special") = "no" then s = s & " and lcase('' & " & tablename & ".special) <> 'on'"
  if session(tablename & "_supplierid") <> "" and session(tablename & "containerfield") <> "supplierid" then s = s & " and " & tablename & ".supplierid = " & session(tablename & "_supplierid")
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_2.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & areas_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & areas_2.title) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".duration like '%" & w & "%' or"
        s = s & " cstr('' & suppliers_1.title) & ' ' like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".passengers = " & w & " or"
        if isnumeric(w) then s = s & " " & tablename & ".price = " & w & " or"
        s = s & " " & tablename & ".addnight like '%" & w & "%' or"
        s = s & " " & tablename & ".addweekend like '%" & w & "%' or"
        s = s & " " & tablename & ".returndiscount like '%" & w & "%' or"
        s = s & " " & tablename & ".ordertypeidsv like '%" & w & "%' or"
        t = "select top 50 id from ordertypes where (cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".ordertypeids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
'[        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
			  if specs = "iec" then
			    aa = getidlist("select areaid from stations where title like '%" & w & "%'")
			    s = s & " courserates.areaid1 in(" & aa & "-1) or"
          s = s & " courserates.areaid2 in(" & aa & "-1) or"
			  end if
']
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  f = tablename & ".id" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperator1=on&groupseperator=no>~ID~</a> " & a2)
'[  f = "courses_1.code" : f2 = "courseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_courseid~</a> " & a2)
  if showratecode = "on" then rr "<td class=list_title><nobr>��� ������"
  if specs = "iec" then
    rr "<td class=list_title><nobr>���� ����" 'dealnumber
    rr "<td class=list_title><nobr>���� ����" 'dealitem
    rr "<td class=list_title><nobr>������� �����" 'kmrate
    rr "<td class=list_title><nobr>���� �" 'goodafter
    rr "<td class=list_title><nobr>���� ��" 'gooduntil
  end if
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    f = "courses_1.code" : f2 = "courseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_courseid~</a> " & a2)
  end if
']
  if oksee("from1") and session(tablename & "containerfield") <> "from1" then
    f = "stations_1.title" : f2 = "from1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_from1~</a> " & a2)
  end if
  if oksee("to1") and session(tablename & "containerfield") <> "to1" then
    f = "stations_2.title" : f2 = "to1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_to1~</a> " & a2)
  end if
  if oksee("areaid1") and session(tablename & "containerfield") <> "areaid1" then
    f = "areas_1.title" : f2 = "areaid1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_areaid1~</a> " & a2)
  end if
'[  if oksee("areaid2") and session(tablename & "containerfield") <> "areaid2" then
'[    f = "areas_2.title" : f2 = "areaid2lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_areaid2~</a> " & a2)
'[  end if
  if oksee("areaid2") and session(tablename & "containerfield") <> "areaid2" then
    f = "areas_2.title" : f2 = "areaid2lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_areaid2~</a> " & a2)
  end if
  if specs = "iec" then
    rr "<td class=list_title><nobr>�������" 'areaids1
    rr "<td class=list_title><nobr>�������" 'areaids2
  end if
  if specs = "kmc" then
    rr "<td class=list_title><nobr>����� ������" 'specificway
  end if
']
  if oksee("activeproduction") then f = "courserates.activeproduction" : f2 = "activeproduction" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("activeproduction") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_activeproduction~</a> " & a2)
  if oksee("active") then f = "courserates.active" : f2 = "active" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("active") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_active~</a> " & a2)
  if oksee("special") then f = "courserates.special" : f2 = "special" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("special") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_special~</a> " & a2)
'[  if oksee("duration") then f = "courserates.duration" : f2 = "duration" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("duration") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_duration~</a> " & a2)
  if oksee("duration") then f = "courserates.duration" : f2 = "duration" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("duration") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_duration~</a> " & a2)
  if specs = "egged" then
    rr "<td class=list_title><nobr>��� �����"'duration_a
    rr "<td class=list_title><nobr>��� �����"'duration_b
  end if
']

  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then
    f = "suppliers_1.title" : f2 = "supplieridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_supplierid~</a> " & a2)
  end if
  if oksee("passengers") then f = "courserates.passengers" : f2 = "passengers" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("passengers") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_passengers~</a> " & a2)
'[  if oksee("price") then f = "courserates.price" : f2 = "price" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("price") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_price~</a> " & a2)
  'if specs = "rafael" then
	'  if oksee("price") then rr("<td class=list_title><nobr>~courserates_price~")
  'else
	  if oksee("price") then f = "courserates.price" : f2 = "price" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
	  if oksee("price") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_price~</a> " & a2)
  'end if
  if specs = "rail" then rr "<td class=list_title><nobr>�������"'category
']
  if oksee("addnight") then f = "courserates.addnight" : f2 = "addnight" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addnight") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_addnight~</a> " & a2)
  if oksee("addweekend") then f = "courserates.addweekend" : f2 = "addweekend" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addweekend") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_addweekend~</a> " & a2)
  if oksee("returndiscount") then f = "courserates.returndiscount" : f2 = "returndiscount" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("returndiscount") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_returndiscount~</a> " & a2)
  if oksee("ordertypeidsv") then rr("<td class=list_title><nobr>~courserates_ordertypeidsv~")
  if oksee("ordertypeids") then rr("<td class=list_title><nobr>~courserates_ordertypeids~")
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courserates_siteid~</a> " & a2)
  end if
  if oksee("data1") then rr("<td class=list_title><nobr>~courserates_data1~")

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
      Thecourseid = replace(thecourseid,",",".")
      Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
      Thefrom1 = replace(thefrom1,",",".")
      Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
      Theto1 = replace(theto1,",",".")
      Theareaid1 = rs("areaid1") : if isnull(Theareaid1) then Theareaid1 = 0
      Theareaid1 = replace(theareaid1,",",".")
      Theareaid2 = rs("areaid2") : if isnull(Theareaid2) then Theareaid2 = 0
      Theareaid2 = replace(theareaid2,",",".")
      Theactiveproduction = rs("activeproduction") : if isnull(Theactiveproduction) then Theactiveproduction = ""
      Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
      Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
      Theduration = rs("duration") : if isnull(Theduration) then Theduration = ""
      Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
      Thesupplierid = replace(thesupplierid,",",".")
      Thepassengers = rs("passengers") : if isnull(Thepassengers) then Thepassengers = 0
      Thepassengers = replace(thepassengers,",",".") : Thepassengers = formatnumber(thepassengers,0,true,false,false)
      Theprice = rs("price") : if isnull(Theprice) then Theprice = 0
      Theprice = replace(theprice,",",".") : Theprice = formatnumber(theprice,2,true,false,false)
      Theaddnight = rs("addnight") : if isnull(Theaddnight) then Theaddnight = ""
      Theaddweekend = rs("addweekend") : if isnull(Theaddweekend) then Theaddweekend = ""
      Thereturndiscount = rs("returndiscount") : if isnull(Thereturndiscount) then Thereturndiscount = ""
      Theordertypeidsv = rs("ordertypeidsv") : if isnull(Theordertypeidsv) then Theordertypeidsv = ""
      Theordertypeids = rs("ordertypeids") : Theordertypeids = formatidlist(Theordertypeids)
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
      Thefrom1lookup = rs("from1lookup") : if isnull(Thefrom1lookup) then Thefrom1lookup = ""
      Theto1lookup = rs("to1lookup") : if isnull(Theto1lookup) then Theto1lookup = ""
      Theareaid1lookup = rs("areaid1lookup") : if isnull(Theareaid1lookup) then Theareaid1lookup = ""
      Theareaid2lookup = rs("areaid2lookup") : if isnull(Theareaid2lookup) then Theareaid2lookup = ""
      Thesupplieridlookup = rs("supplieridlookup") : if isnull(Thesupplieridlookup) then Thesupplieridlookup = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid
      if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = theid
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
'[      rr("<img src=icons/cost.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      rr("<img src=icons/cost.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if showratecode = "on" then
        rr "<td class=list_item>"
        ratecode = getval(thedata1, "ratecode") : ratecode = tocsv(ratecode)
        if session("usergroup") = "admin" then
          rr "<input type=text name=ratecode" & theid & " style='width:70;' value='" & ratecode & "' onchange='vbscript: c" & theid & ".checked = true'>"
        else
          rr ratecode
        end if
      end if
      if specs = "iec" then
        rr "<td class=list_item>" & getval(thedata1, "dealnumber")
        rr "<td class=list_item>" & getval(thedata1, "dealitem")
        rr "<td class=list_item>" & getval(thedata1, "kmrate")
        rr "<td class=list_item>" & getval(thedata1, "goodafter")
        rr "<td class=list_item>" & getval(thedata1, "gooduntil")
      end if
']

      if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_item valign=top align=><nobr>" & thecourseidlookup)
'[      if oksee("from1") and session(tablename & "containerfield") <> "from1" then rr("<td class=list_item valign=top align=><nobr>" & thefrom1lookup)
      if oksee("from1") then rr("<td class=list_item valign=top align=><nobr>" & thefrom1lookup)
      if specs = "iec" and oksee("from1") then t = gettitle("areas", getonefield("select areaid from stations where id = " & thefrom1)) : if t <> "" then rr " <span style='color:#aaaaaa;'>(" & t & ")</span>"
']
'[      if oksee("to1") and session(tablename & "containerfield") <> "to1" then rr("<td class=list_item valign=top align=><nobr>" & theto1lookup)
      if oksee("to1") then rr("<td class=list_item valign=top align=><nobr>" & theto1lookup)
      if specs = "iec" and oksee("from1") then t = gettitle("areas", getonefield("select areaid from stations where id = " & theto1)) : if t <> "" then rr " <span style='color:#aaaaaa;'>(" & t & ")</span>"
']
      if oksee("areaid1") and session(tablename & "containerfield") <> "areaid1" then rr("<td class=list_item valign=top align=><nobr>" & theareaid1lookup)
'[      if oksee("areaid2") and session(tablename & "containerfield") <> "areaid2" then rr("<td class=list_item valign=top align=><nobr>" & theareaid2lookup)
      if oksee("areaid2") and session(tablename & "containerfield") <> "areaid2" then rr("<td class=list_item valign=top align=><nobr>" & theareaid2lookup)

      if specs = "iec" then
        for i = 1 to 2
          v = ""
          sq = "select title from areas where id in(" & getval(thedata1, "areaids" & i) & "0)"
          closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
          do until rs1.eof
            v = v & "<nobr>" & rs1("title") & ",</nobr> "
            rs1.movenext
          loop
          rr "<td class=list_item>" & v
        next
      end if

      if specs = "kmc" then
        rr "<td class=list_item>" & getval(thedata1, "specificway")
      end if
']
      if oksee("activeproduction") and session(tablename & "containerfield") <> "activeproduction" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("activeproduction") then
      elseif session(tablename & "containerfield") = "activeproduction" then
        rr("<input type=hidden name=listupdate_activeproduction_" & theid & " value=" & theactiveproduction & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "activeproduction" then
        rr("<input type=checkbox name=listupdate_activeproduction_" & theid & "")
        if theactiveproduction = "on" then rr(" checked")
        rr(">")
      else
        if theactiveproduction = "on" then rr("v")
      end if
      if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("active") then
      elseif session(tablename & "containerfield") = "active" then
        rr("<input type=hidden name=listupdate_active_" & theid & " value=" & theactive & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "active" then
        rr("<input type=checkbox name=listupdate_active_" & theid & "")
        if theactive = "on" then rr(" checked")
        rr(">")
      else
        if theactive = "on" then rr("v")
      end if
      if oksee("special") and session(tablename & "containerfield") <> "special" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("special") then
      elseif session(tablename & "containerfield") = "special" then
        rr("<input type=hidden name=listupdate_special_" & theid & " value=" & thespecial & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "special" then
        rr("<input type=checkbox name=listupdate_special_" & theid & "")
        if thespecial = "on" then rr(" checked")
        rr(">")
      else
        if thespecial = "on" then rr("v")
      end if
'[      if oksee("duration") then rr("<td class=list_item valign=top align=><nobr>" & theduration)
      if oksee("duration") then rr("<td class=list_item valign=top align=><nobr>" & theduration)
      if specs = "egged" then
        rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1, "duration_a"))
        rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1, "duration_b"))
      end if
'[      if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then rr("<td class=list_item valign=top align=><nobr>" & thesupplieridlookup)
      if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then rr("<td class=list_item valign=top align=><nobr>" & thesupplieridlookup)
      if specs = "elbit" and cstr(thesupplierid) <> "0" then t = getonefield("select data1 from suppliers where id = 0" & thesupplierid) : if getval(t, "activeproduction") <> "on" and getval(t, "activespecial") <> "on" then rr " (�� ����)"
']
      if oksee("passengers") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(thepassengers,0,true,false,true))
'[      if oksee("price") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(theprice,2,true,false,true))
      if oksee("price") then
        'if specs = "rafael" then du = getpricebyratecode()
        rr "<td class=list_item valign=top align=right dir=ltr><nobr>"
        if getval(thedata1, "relativeprice") <> "on" or specs <> "rafael" then rr formatnumber(theprice,2,true,false,true)
        if getval(thedata1, "relativeprice") = "on" then rr " (~Relative price~)"
      end if
      if specs = "rail" then rr "<td class=list_item valign=top align=right dir=ltr><nobr>" & getval(thedata1, "category")
']
      if oksee("addnight") then rr("<td class=list_item valign=top align=><nobr>" & theaddnight)
      if oksee("addweekend") then rr("<td class=list_item valign=top align=><nobr>" & theaddweekend)
      if oksee("returndiscount") then rr("<td class=list_item valign=top align=><nobr>" & thereturndiscount)
      theordertypeidsv = theordertypeidsv : if isnull(theordertypeidsv) then theordertypeidsv = ""
      theordertypeidsv = replace(theordertypeidsv,"<br>"," ") : theordertypeidsv = replace(theordertypeidsv,"<","[") : theordertypeidsv = replace(theordertypeidsv,">","]") : if len(theordertypeidsv) > 30 then theordertypeidsv = left(theordertypeidsv,30) & "..."
      if oksee("ordertypeidsv") then rr("<td class=list_item valign=top align=><nobr>" & theordertypeidsv)
      t = "" : d = formatidlist(theordertypeids)
      sq = "select * from ordertypes where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("title")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("ordertypeids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
'[  rr("<td class=list_total1>") 'id
  if showratecode = "on" then
    rr "<td>"
    if session("usergroup") = "admin" then rr "<input type=button value='���� ��� ������' style='width:70; height:22; font-size:9;' onclick='vbscript: me.disabled = true : flist.action.value = ""ratecodeupdate"" : flist.action2.value = """" : flist.submit'>"
  end if
']
  if oksee("from1") and session(tablename & "containerfield") <> "from1" then rr("<td class=list_total1>") 'from1
  if oksee("to1") and session(tablename & "containerfield") <> "to1" then rr("<td class=list_total1>") 'to1
  if oksee("areaid1") and session(tablename & "containerfield") <> "areaid1" then rr("<td class=list_total1>") 'areaid1
  if oksee("areaid2") and session(tablename & "containerfield") <> "areaid2" then rr("<td class=list_total1>") 'areaid2
  if oksee("activeproduction") and session(tablename & "containerfield") <> "activeproduction" then rr("<td class=list_total1>") 'activeproduction
  if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_total1>") 'active
  if oksee("special") and session(tablename & "containerfield") <> "special" then rr("<td class=list_total1>") 'special
  if oksee("duration") and session(tablename & "containerfield") <> "duration" then rr("<td class=list_total1>") 'duration
  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then rr("<td class=list_total1>") 'supplierid
  if oksee("passengers") and session(tablename & "containerfield") <> "passengers" then rr("<td class=list_total1>") 'passengers
  if oksee("price") and session(tablename & "containerfield") <> "price" then rr("<td class=list_total1>") 'price
  if oksee("addnight") and session(tablename & "containerfield") <> "addnight" then rr("<td class=list_total1>") 'addnight
  if oksee("addweekend") and session(tablename & "containerfield") <> "addweekend" then rr("<td class=list_total1>") 'addweekend
  if oksee("returndiscount") and session(tablename & "containerfield") <> "returndiscount" then rr("<td class=list_total1>") 'returndiscount
  if oksee("ordertypeidsv") and session(tablename & "containerfield") <> "ordertypeidsv" then rr("<td class=list_total1>") 'ordertypeidsv
  if oksee("ordertypeids") and session(tablename & "containerfield") <> "ordertypeids" then rr("<td class=list_total1>") 'ordertypeids
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
'[  rr("</tr></form>" & bottombar & "</table>")'list

  'if instr(",rafael,elbit,shufersal,metropolin,", "," & specs & ",") > 0 then
    rr "<input type=hidden name=containerid value=''>"
    rr " <input type=button class=groovybutton id=buttonduplicate2 style='width:100; color:#aa0000;' value=""~Duplicate~.."" onclick='vbscript: flist.target = ""_top"" : flist.action.value=""duplicate2"" : flist.containerid.value = ""0"" : flist.submit'>"
  'end if

  rr "<hr>"
  if okedit("buttonmultiupdate") then
    rr "<br>"
    rr " ~courserates_activeproduction~ <select name=multi_activeproduction><option value='' style='color:#bbbbbb;'>~Unchanged~<option value='on'>~Yes~<option value='no'>~No~</select>"
    rr " ~courserates_active~ <select name=multi_active><option value='' style='color:#bbbbbb;'>~Unchanged~<option value='on'>~Yes~<option value='no'>~No~</select>"
    rr " ~courserates_special~ <select name=multi_special><option value='' style='color:#bbbbbb;'>~Unchanged~<option value='on'>~Yes~<option value='no'>~No~</select>"

	  rr " ~Supplier~ <select name=multi_supplierid><option value=''>"
	  s = "select id,title from suppliers order by title"
	  set rs = conn.execute(sqlfilter(s))
	  do until rs.eof
	    rr "<option value=" & rs("id") & ">" & rs("title")
	    rs.movenext
	  loop
	  rr "</select>"

    rr " <input type=button value='~Update~' style='width:80;' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""updateactive"" : flist.action2.value = """" : flist.submit'>"
  end if

  rr "<br><br>~Update orders retroactively~, ~For the checkmarked rates only~, ~From~" & SelectDate("flist.d1*:", date & " 00:00:00")
  rr " <input type=checkbox name=retroallstatuses>~Orders in all statuses (not only `New`)~"
  rr " <input type=button value='~Update orders retroactively~' onclick='vbscript:if msgbox(""~Update orders retroactively~?"",vbyesno) = 6 then flist.action2.value = ""retro"" : flist.submit'>"

  rr "<br><br>~Increase all checked rates by percent~ <input type=text name=increase style='width:50;'>"
  rr " <input type=checkbox name=roundprices>~Round prices~"
  rr " <input type=button value='~Increase rates~' onclick='vbscript:if msgbox(""~Increase rates~?"",vbyesno) = 6 then flist.action2.value = ""increase"" : flist.submit'>"

  rr("</tr></form>" & bottombar & "</table>")'list
']
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
