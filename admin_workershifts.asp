<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 16/06/2013 23:53:15

function workershiftsdelitem(i)
  workershiftsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function workershiftsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
'[  f = "" : v = ""
  f = "" : v = ""
  's = "delete * from workershifts where workerid = " & session(tablename & "containerid") & " and date1 = " & sqldate(getfield("date1"))
  'set rs1 = conn.execute(sqlfilter(s))
']

  t = "1/1/1900" : if getfield("date1") <> "" then t = getfield("date1")
  f = f & "date1," : v = v & sqldate(t) & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "workerid" then t = session(tablename & "containerid")
  if getfield("workerid") <> "" then t = getfield("workerid")
  f = f & "workerid," : v = v & t & ","

  t = "X" : if getfield("ways") <> "" then t = getfield("ways")
  f = f & "ways," : v = v & "'" & t & "',"

  t = "00:00" : if getfield("hour1") <> "" then t = getfield("hour1")
  f = f & "hour1," : v = v & "'" & t & "',"

  t = "00:00" : if getfield("hour1b") <> "" then t = getfield("hour1b")
  f = f & "hour1b," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "foodsupplierid" then t = session(tablename & "containerid")
  if getfield("foodsupplierid") <> "" then t = getfield("foodsupplierid")
  f = f & "foodsupplierid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "from1" then t = session(tablename & "containerid")
  if getfield("from1") <> "" then t = getfield("from1")
  f = f & "from1," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "to1" then t = session(tablename & "containerid")
  if getfield("to1") <> "" then t = getfield("to1")
  f = f & "to1," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "courseid" then t = session(tablename & "containerid")
  if getfield("courseid") <> "" then t = getfield("courseid")
  f = f & "courseid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "shiftid" then t = session(tablename & "containerid")
  if getfield("shiftid") <> "" then t = getfield("shiftid")
  f = f & "shiftid," : v = v & t & ","

  t = now : if getfield("date0") <> "" then t = getfield("date0")
  f = f & "date0," : v = v & sqldate(t) & ","

'[  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  t = "manually=on" : if getfield("data1") <> "" then t = getfield("data1")
']
  f = f & "data1," : v = v & "'" & t & "',"

'[  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "userid0" then t = session(tablename & "containerid")
'[  if getfield("userid0") <> "" then t = getfield("userid0")
  t = session("userid")
']
  f = f & "userid0," : v = v & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into workershifts(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from workershifts"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  workershiftsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table workershifts (id autoincrement primary key,date1 datetime,workerid number,ways text(2),hour1 text(5),hour1b text(5),foodsupplierid number,from1 number,to1 number,courseid number,shiftid number,date0 datetime,data1 memo,userid0 number)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "workershifts" : session("tablename") = "workershifts"
tablefields = "id,date1,workerid,ways,hour1,hour1b,foodsupplierid,from1,to1,courseid,shiftid,date0,data1,userid0,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
'[if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
if session(tablename & "_firstcome") = "" then session(tablename & "_firstcome") = "y" : session(tablename & "_monthview") = "on"
']
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 4
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'['-------------start--------------------------------------
'['locked = "1,2,3,"
if session("userlevel") <> "5" then disablecontrols = disablecontrols & "date0,"
if getfield("containerid") <> "" then session(tablename & "_monthview") = "on"
if getfield("action") = "undraft" then
  d = getonefield("select data1 from workershifts where id = 0" & getfield("id"))
  d = setval(d, "draft", "")
  ex "update workershifts set data1 = '" & d & "' where id = 0" & getfield("id")
end if
']

'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = workershiftsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("date1") then s = s & "date1=" & sqldate(getfield("date1")) & ","
  if okedit("workerid") then s = s & "workerid=0" & getfield("workerid") & ","
  if okedit("ways") then s = s & "ways='" & getfield("ways") & "',"
  if okedit("hour1") then s = s & "hour1='" & getfield("hour1") & "',"
  if okedit("hour1b") then s = s & "hour1b='" & getfield("hour1b") & "',"
  if okedit("foodsupplierid") then s = s & "foodsupplierid=0" & getfield("foodsupplierid") & ","
  if okedit("from1") then s = s & "from1=0" & getfield("from1") & ","
  if okedit("to1") then s = s & "to1=0" & getfield("to1") & ","
'[  if okedit("courseid") then s = s & "courseid=0" & getfield("courseid") & ","
  if hasmatch(specs, "jerusalem,muni,brener,") and getfield("courseid") = "0" then du = errorend("_back�� ����� �����")
  if okedit("courseid") then s = s & "courseid=0" & getfield("courseid") & ","
']
  if okedit("shiftid") then s = s & "shiftid=0" & getfield("shiftid") & ","
'[  if okedit("date0") then s = s & "date0=" & sqldate(getfield("date0")) & ","
  if okedit("date0") then s = s & "date0=" & sqldate(getfield("date0")) & "," else s = s & "date0=" & sqldate(now) & ","
']
'[  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  if okedit("data1") then
    data1 = getfield("data1")
  else
    data1 = getonefield("select data1 from workershifts where id = " & request_id)
  end if
  data1 = setval(data1, "manually", "on")
  if specs = "iec" then data1 = setval(data1, "altaddress", getfield("altaddress"))
  s = s & "data1='" & data1 & "',"
']
'[  if okedit("userid0") then s = s & "userid0=0" & getfield("userid0") & ","
  if session("userlevel") <> "5" then s = s & "userid0=0" & session("userid") & ","
']
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = workershiftsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = workershiftsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if

'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","workershifts.id,workershifts.date1,workershifts.workerid,workershifts.ways,workershifts.hour1,workershifts.hour1b,workershifts.foodsupplierid,workershifts.from1,workershifts.to1,workershifts.courseid,workershifts.shiftid,workershifts.date0,workershifts.data1,workershifts.userid0",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Theworkerid = rs("workerid") : if isnull(Theworkerid) then Theworkerid = 0
  Theworkerid = replace(theworkerid,",",".")
  Theways = rs("ways") : if isnull(Theways) then Theways = ""
  Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
  Thehour1b = rs("hour1b") : if isnull(Thehour1b) then Thehour1b = ""
  Thefoodsupplierid = rs("foodsupplierid") : if isnull(Thefoodsupplierid) then Thefoodsupplierid = 0
  Thefoodsupplierid = replace(thefoodsupplierid,",",".")
  Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
  Thefrom1 = replace(thefrom1,",",".")
  Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
  Theto1 = replace(theto1,",",".")
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Theshiftid = rs("shiftid") : if isnull(Theshiftid) then Theshiftid = 0
  Theshiftid = replace(theshiftid,",",".")
  Thedate0 = rs("date0") : if isnull(Thedate0) then Thedate0 = "1/1/1900"
  Thedate0 = replace(thedate0,".","/")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Theuserid0 = rs("userid0") : if isnull(Theuserid0) then Theuserid0 = 0
  Theuserid0 = replace(theuserid0,",",".")
  formcontrols = "date1,date1_day,date1_month,date1_year,date1_hour,date1_insertdate,date1_removedate,workerid,ways,hour1,hour1b,foodsupplierid,from1,to1,courseid,shiftid,date0,date0_day,date0_month,date0_year,date0_hour,date0_insertdate,date0_removedate,data1,userid0,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/refresh.png' ~iconsize~> ~workershifts_workershifts~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("date1") then'form:date1
    rr("<tr valign=top id=date1_tr0><td class=form_item1 id=date1_tr1><span id=date1_caption>~workershifts_date1~<img src=must.gif></span><td class=form_item2 id=date1_tr2>")
    rr(SelectDate("f1.date1*", thedate1))
  end if'form:date1

  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then
    rr("<tr valign=top id=workerid_tr0><td class=form_item1 id=workerid_tr1><span id=workerid_caption>~workershifts_workerid~</span><td class=form_item2 id=workerid_tr2>")
    if getfield("box") = "workerid" then theworkerid = getfield("boxid")
    rr("<select name=workerid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~workershifts_workerid~")
    sq = "select * from workers order by firstname,lastname"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theworkerid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("firstname") & " ")
      rr(rs1("lastname") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:workerid
  elseif okedit("workerid") then
    rr("<input type=hidden name=workerid value=" & theworkerid & ">")
  end if'form:workerid

  if oksee("ways") then'form:ways
    rr("<tr valign=top id=ways_tr0><td class=form_item1 id=ways_tr1><span id=ways_caption>~workershifts_ways~</span><td class=form_item2 id=ways_tr2>")
    a = "X,AB,A,B,"
    rr("<select name=ways dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
'[      if Theways = b then rr(" selected")
'[      rr(">" & b)
      if b = "" then rr " style='color:ff0000;'"
      if Theways = b then rr(" selected")
      rr(">" & b)
      'if b = "AB" then rr " ~Collect and Distribute~"
      'if b = "A" then rr " ~Collect Only~"
      'if b = "B" then rr " ~Distribute Only~"
      'if b = "" or b = "X" then rr " ~No Ride~"
']
    loop
    rr("</select>")'form:ways
  end if'form:ways

  if oksee("hour1") then'form:hour1
    rr("<tr valign=top id=hour1_tr0><td class=form_item1 id=hour1_tr1><span id=hour1_caption>~workershifts_hour1~<img src=must.gif></span><td class=form_item2 id=hour1_tr2>")
'[    rr("<input type=text maxlength=5 name=hour1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thehour1 & """ dir=ltr>")
    rr("<input type=text maxlength=5 name=hour1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & Thehour1 & """ dir=ltr>")

    's = "select courseid1 from workers where id = " & theworkerid
    'set rs1 = conn.execute(sqlfilter(s))
    'courseid = 0 : if not rs1.eof then courseid = rs1("courseid1")
    'if cdbl(courseid) = 0 then
    '  rr "<input type=hidden name=hour1><font color=ff0000>~Worker does not have Course~"
    'else
    '  rr "<select name=hour1>"
    '  s = "select hour1 from coursehours where courseid = " & courseid & " and direction = 'A'"
    '  set rs1 = conn.execute(sqlfilter(s))
    '  existed = ""
    '  do until rs1.eof
    '    rr "<option value='" & rs1("hour1") & "'" : if thehour1 = rs1("hour1") then rr " selected" : existed = "on"
    '    rr ">" & rs1("hour1")
    '    rs1.movenext
    '  loop
    '  rr "</select>"
    '  if existed = "" and request_action2 <> "newitem" then rr " * ~Hour for this day was~ " & thehour1 & " ~and is already not valid for the worker`s course~"
    'end if
']
  end if'form:hour1

  if oksee("hour1b") then'form:hour1b
    rr("<tr valign=top id=hour1b_tr0><td class=form_item1 id=hour1b_tr1><span id=hour1b_caption>~workershifts_hour1b~<img src=must.gif></span><td class=form_item2 id=hour1b_tr2>")
'[    rr("<input type=text maxlength=5 name=hour1b onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thehour1b & """ dir=ltr>")
'[  end if'form:hour1b
    rr("<input type=text maxlength=5 name=hour1b onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & Thehour1b & """ dir=ltr>")

    'if cdbl(courseid) = 0 then
    '  rr "<input type=hidden name=hour1b>"
    'else
    '  rr "<select name=hour1b>"
    '  s = "select hour1 from coursehours where courseid = " & courseid & " and direction = 'B'"
    '  set rs1 = conn.execute(sqlfilter(s))
    '  existed = ""
    '  do until rs1.eof
    '    rr "<option value='" & rs1("hour1") & "'" : if thehour1b = rs1("hour1") then rr " selected" : existed = "on"
    '    rr ">" & rs1("hour1")
    '    rs1.movenext
    '  loop
    '  rr "</select>"
    '  if existed = "" and request_action2 <> "newitem" then rr " * ~Hour for this day was~ " & thehour1b & " ~and is already not valid for the worker`s course~"
    'end if
  end if'form:hour1b
  if specs = "iec" then
    rr("<tr valign=top id=hour1b_tr0><td class=form_item1 id=hour1b_tr1><span id=hour1b_caption>����� ������<img src=must.gif></span><td class=form_item2 id=hour1b_tr2>")
    rr "<input type=text name=altaddress style='width:300' value=""" & getval(thedata1, "altaddress") & """>"
  end if
']


  if oksee("foodsupplierid") and session(tablename & "containerfield") <> "foodsupplierid" then
    rr("<tr valign=top id=foodsupplierid_tr0><td class=form_item1 id=foodsupplierid_tr1><span id=foodsupplierid_caption>~workershifts_foodsupplierid~</span><td class=form_item2 id=foodsupplierid_tr2>")
    if getfield("box") = "foodsupplierid" then thefoodsupplierid = getfield("boxid")
    rr("<select name=foodsupplierid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~workershifts_foodsupplierid~")
    sq = "select * from foodsuppliers order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thefoodsupplierid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:foodsupplierid
  elseif okedit("foodsupplierid") then
    rr("<input type=hidden name=foodsupplierid value=" & thefoodsupplierid & ">")
  end if'form:foodsupplierid

  if oksee("from1") and session(tablename & "containerfield") <> "from1" then
    rr("<tr valign=top id=from1_tr0><td class=form_item1 id=from1_tr1><span id=from1_caption>~workershifts_from1~</span><td class=form_item2 id=from1_tr2>")
    if getfield("box") = "from1" then thefrom1 = getfield("boxid")
    rr("<select name=from1 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workershifts_from1~")
'[    sq = "select * from stations order by title"
    rr("<option value=0 style='color:bbbbfe;'>~Worker`s Default~")
    sq = "select siteid from workers where id = " & theworkerid
    set rs1 = conn.execute(sqlfilter(sq))
    thesiteid = 0 : if not rs1.eof then thesiteid = rs1("siteid") : if isnull(thesiteid) then thesiteid = 0
    sq = "select * from stations where id = " & thefrom1 & " or siteid in (0" & thesiteid & ",0)"
    if specs = "kvish6" then sq = sq & " and special <> 'on'"
    sq = sq & " order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thefrom1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:from1
  elseif okedit("from1") then
    rr("<input type=hidden name=from1 value=" & thefrom1 & ">")
  end if'form:from1

  if oksee("to1") and session(tablename & "containerfield") <> "to1" then
    rr("<tr valign=top id=to1_tr0><td class=form_item1 id=to1_tr1><span id=to1_caption>~workershifts_to1~</span><td class=form_item2 id=to1_tr2>")
    if getfield("box") = "to1" then theto1 = getfield("boxid")
    rr("<select name=to1 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workershifts_to1~")
'[    sq = "select * from stations order by title"
    rr("<option value=0 style='color:bbbbfe;'>~Worker`s Default~")
    sq = "select * from stations where id = " & theto1 & " or siteid in (0" & thesiteid & ",0)"
    if specs = "kvish6" then sq = sq & " and special <> 'on'"
    sq = sq & " order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theto1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:to1
  elseif okedit("to1") then
    rr("<input type=hidden name=to1 value=" & theto1 & ">")
  end if'form:to1

  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    rr("<tr valign=top id=courseid_tr0><td class=form_item1 id=courseid_tr1><span id=courseid_caption>~workershifts_courseid~</span><td class=form_item2 id=courseid_tr2>")
    if getfield("box") = "courseid" then thecourseid = getfield("boxid")
    rr("<select name=courseid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~workershifts_courseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thecourseid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:courseid
  elseif okedit("courseid") then
    rr("<input type=hidden name=courseid value=" & thecourseid & ">")
  end if'form:courseid

  if oksee("shiftid") and session(tablename & "containerfield") <> "shiftid" then
    rr("<tr valign=top id=shiftid_tr0><td class=form_item1 id=shiftid_tr1><span id=shiftid_caption>~workershifts_shiftid~</span><td class=form_item2 id=shiftid_tr2>")
    if getfield("box") = "shiftid" then theshiftid = getfield("boxid")
'[    rr("<select name=shiftid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workershifts_shiftid~")
'[    sq = "select * from shifts order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Theshiftid) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:shiftid
    rr("<select name=shiftid dir=~langdir~ onchange='vbscript:sethours'>")
    rr("<option value=0 style='color:bbbbfe;'>~workershifts_shiftid~")
    sq = "select id,title,hour1,hour1b,ways,days from shifts where 0=0"
    if session("usersiteid") <> "0" then sq = sq & " and siteid = " & session("usersiteid")
    if session("managershiftids") <> "" then sq = sq & " and id in(" & session("managershiftids") & "-1)"
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theshiftid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      sethourscode = sethourscode & " if x = """ & rs1("id") & """ then" & vbcrlf
      sethourscode = sethourscode & "   f1.hour1.value = """ & rs1("hour1") & """ : f1.hour1b.value = """ & rs1("hour1b") & """" & vbcrlf
      sethourscode = sethourscode & "   f1.ways.value = """ & rs1("ways") & """" & vbcrlf
      sethourscode = sethourscode & " end if" & vbcrlf
      rs1.movenext
    loop
    rr("</select>")'form:shiftid
    rr "<script language=vbscript>" & vbcrlf
    rr "function sethours" & vbcrlf
    rr "  f1.hour1.value = """" : f1.hour1b.value = """"" & vbcrlf
    rr "  x = f1.shiftid.value" & vbcrlf
    rr sethourscode
    rr "end function" & vbcrlf
    rr "</script>"
']
  elseif okedit("shiftid") then
    rr("<input type=hidden name=shiftid value=" & theshiftid & ">")
  end if'form:shiftid

  if oksee("date0") then'form:date0
    rr("<tr valign=top id=date0_tr0><td class=form_item1 id=date0_tr1><span id=date0_caption>~workershifts_date0~<img src=must.gif></span><td class=form_item2 id=date0_tr2>")
    rr(SelectDate("f1.date0*:", thedate0))
  end if'form:date0

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
  end if'form:data1

  if oksee("userid0") and session(tablename & "containerfield") <> "userid0" then
    rr("<tr valign=top id=userid0_tr0><td class=form_item1 id=userid0_tr1><span id=userid0_caption>~workershifts_userid0~</span><td class=form_item2 id=userid0_tr2>")
'[    if getfield("box") = "userid0" then theuserid0 = getfield("boxid")
'[    rr("<select name=userid0 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workershifts_userid0~")
'[    sq = "select * from buusers order by username"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Theuserid0) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("username") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:userid0
    rr("<input type=hidden name=userid0 value=" & theuserid0 & ">")
    rr  getuserorworker(theuserid0)
']
  elseif okedit("userid0") then
    rr("<input type=hidden name=userid0 value=" & theuserid0 & ">")
  end if'form:userid0

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

'[  '-------------checkform-------------
  '-------------checkform-------------
  rr "<div style='color:#eeeeee;'>=data1=" & thedata1 & "</div>"
']
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("date1") then rr("    if instr(f1.date1.value,""1/1/1900"") = 0 and f1.date1.value <> """" then validfield(""date1"") else invalidfield(""date1"") : ok = false" & vbcrlf)
'[  if okedit("hour1") then rr("    if f1.hour1.value <> """" then validfield(""hour1"") else invalidfield(""hour1"") : ok = false" & vbcrlf)
'[  if okedit("hour1b") then rr("    if f1.hour1b.value <> """" then validfield(""hour1b"") else invalidfield(""hour1b"") : ok = false" & vbcrlf)
  if okedit("hour1") then rr("    if len(f1.hour1.value) = 4 then f1.hour1.value = ""0"" & f1.hour1.value" & vbcrlf)
  if okedit("hour1") then rr("    if len(f1.hour1.value) = 5 and mid(f1.hour1.value,3,1) = "":"" and isnumeric(left(f1.hour1.value,2)) and isnumeric(right(f1.hour1.value,2)) and instr(f1.hour1.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) <= 59 then validfield(""hour1"") else invalidfield(""hour1"") : ok = false" & vbcrlf)
  if okedit("hour1b") then rr("    if len(f1.hour1b.value) = 4 then f1.hour1b.value = ""0"" & f1.hour1b.value" & vbcrlf)
  if okedit("hour1b") then rr("    if len(f1.hour1b.value) = 5 and mid(f1.hour1b.value,3,1) = "":"" and isnumeric(left(f1.hour1b.value,2)) and isnumeric(right(f1.hour1b.value,2)) and instr(f1.hour1b.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) <= 59 then validfield(""hour1b"") else invalidfield(""hour1b"") : ok = false" & vbcrlf)
']
  if okedit("date0") then rr("    if instr(f1.date0.value,""1/1/1900"") = 0 and f1.date0.value <> """" then validfield(""date0"") else invalidfield(""date0"") : ok = false" & vbcrlf)
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
'[  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.hour1.focus : on error goto 0</script>")
']
'-------------monthview----------------------------------------
'[elseif session(tablename & "_monthview") = "on" then
elseif session(tablename & "_monthview") = "on" then
  rr "<iframe name=frpicked style='width:500; height:0; position:absolute; top:10; left:10;' frameborder=0></iframe>"
  pickedids = ""
']
  if getfield("m") <> "" then session(tablename & "_monthview_m") = getfield("m")
  if getfield("y") <> "" then session(tablename & "_monthview_y") = getfield("y")
  if getfield("chosen") <> "" then session(tablename & "_monthview_chosen") = getfield("chosen")
  if session(tablename & "_monthview_chosen") = "" then session(tablename & "_monthview_chosen") = date
'[  rr("<table width=80% cellspacing=5 align=center>")
  rr("<table cellspacing=5 align=center>")
']
  rr("<form method=post action=? name=f2>")
  rr("<input type=hidden name=action value=search>")
  rr("<tr><td class=list_name colspan=99>")
  rr(" <img src=icons/refresh.png ~iconsize~> ~workershifts_workershifts~ - ~Month view~ ")
  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & "&date1=" & session(tablename & "_monthview_chosen") & """'> ")
  m = session(tablename & "_monthview_m")
  y = session(tablename & "_monthview_y")
  if m = "" then m = month(date)
  if y = "" then y = year(date)
  rr(" <SELECT NAME=m>")
  for i = 1 to 12
    rr("<option value=" & i)
    if cstr(m) = cstr(i) then rr(" selected")
    rr(">" & i)
  next
  rr("</SELECT>")
  rr("<SELECT NAME=y>")
  for i = year(now) - 20 to year(now) + 20
    rr("<option value=" & i)
    if cstr(y) = cstr(i) then rr(" selected")
    rr(">" & i)
  next
  rr("</SELECT>")
  rr(" <input type=submit class=groovybutton value=~Show~>")
  prevy = y : prevm = m - 1 : if prevm < 1 then prevm = 12 : prevy = prevy - 1
  nexty = y : nextm = m + 1 : if nextm > 12 then nextm = 1 : nexty = nexty + 1
  rr(" <a href=?y=" & prevy & "&m=" & prevm & "><img src=~langside~arrow.png ~iconsize~ border=0></a>")
  rr(" <a href=?y=" & nexty & "&m=" & nextm & "><img src=~langside2~arrow.png ~iconsize~ border=0></a>")
  rr(" &nbsp;&nbsp;&nbsp;&nbsp;<a href=?monthview=no>~Switch to list view~</a>")
  rr("<table></form></table>")
  '-------------showmonthview-------------
  rr("<tr><td class=monthdaytitle>~Sunday~<td class=monthdaytitle>~Monday~<td class=monthdaytitle>~Tuesday~<td class=monthdaytitle>~Wednesday~<td class=monthdaytitle>~Thursday~<td class=monthdaytitle>~Friday~<td class=monthdaytitle>~Saturday~")
  d = weekday(1 & "/" & m & "/" & y)
  rr("<tr>") : for i = 1 to d - 1: rr("<td class=monthempty>") : next
  for i = 1 to cint(lastday(m & "/" & y))
    d = i & "/" & m & "/" & y
    wd = weekday(d)
    if wd = 1 then rr("<tr>")
'[    t = "" : if wd = 6 or wd = 7 then t = "weekend"
    t = "" : tt = getweekday(d)
    if tt = 6 or tt >= 9 then t = "weekend2"
    if tt = 7 or tt = 8 then t = "weekend"
']
    if cdate(d) = cdate(session(tablename & "_monthview_chosen")) then t = t & "chosen"
'[    rr("<td class=monthday" & t & " onclick='vbscript: document.location = ""?chosen=" & d & """' style='cursor:hand;'>")
    if hasmatch(specs, "jerusalem,muni,brener,") then
      rr("<td class=monthday" & t & " style='cursor:hand;'>")
    else
      rr("<td class=monthday" & t & " onclick='vbscript: document.location = ""?chosen=" & d & """' style='cursor:hand;'>")
    end if
']
    rr("<div style='background:eeeeee;'><font style='font-size:18; font-weight:bold; color:404040;'>" & i & "</font>")
    if cdate(d) = date then rr(" <font style='font-size:15; color:ff8000;'>(~Today~)</font>")
    rr("</div>")
'[    s = "select * from " & tablename
'[    s = s & okreadsql(tablename)'onecell

    '-----orders--------
    if hasmatch(specs, "jerusalem,muni,brener,") then
      p = session(tablename & "containerid")
      s = "select id,status,courseid,direction,date1,pids,carid,history from orders where 0=0"
      s = s & " and date1 >= " & sqldate(d & " 00:00:00") & " and date1 <= " & sqldate(d & " 23:59:59")
      s = s & " and status not in('Canceled','Draft','')"
      s = s & " and instr(cstr(',' & pids), '," & p & ",') > 0"
      s = s & " order by date1"
      closers(rs) : set rs = conn.execute(sqlfilter(s))
      rc = 0
      do until rs.eof
        history = rs("history") & ""
        rc = rc + 1
        rr "<div id=d" & rs("id") & " style='position:absolute; visibility:hidden;'></div><nobr>"
        rr "<a target=_top href=admin_orders.asp?containerid=0&action=form&id=" & rs("id") & " style='color:#808080;'>"
        rr "<img src=" & statusicon(rs("status")) & " border=0 width=20 style='vertical-align:top; margin-bottom:1;'> "
        rr "<span style='display:inline-block; margin-top:3; cursor:hand;'>"
        rr rs("id")
        t = getcode("courses",rs("courseid")) : if t <> "" then rr " ~Course~ " & t
        if rs("direction") = "A" then rr " ~Collect~" else if rs("direction") = "B" then rr " ~Distribute~"
        rr " " & getdatehourstring(rs("date1"))
        rr "</span></a>"
        '---picked
        if hasmatch(session("rakazcourseids"), rs("courseid")) or session("usergroup0") <> "rakaz" then
          rr "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=checkbox name=picked_" & rs("id") & "_" & p
          if getval(history, p & "picked") = "on" then rr " checked"
          rr " onclick='vbscript: frpicked.document.location = ""admin_coursecal.asp?action=picked&v="" & me.checked & ""&orderid=" & rs("id") & "&workerid=" & p & """'"
          rr "><span style='color:#808080;'>���</span>"
          pickedids = pickedids & rs("id") & ","
        end if
        '---
        rr "<br>"
        rs.movenext
      loop
    end if

    '---weekplan
    s = "select id,ways,hour1,hour1b,shiftid,courseid from weekplan where day1 = '" & weekday(d) & "'"
    if session(tablename & "containerid") <> "0" then s = s & " and " & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
    s = s & " and (year(date1) = 1900 or date1 <= " & sqldate(d) & ")"
    s = s & " and (year(date2) = 1900 or date2 >= " & sqldate(d) & ")"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      tar = "" : u = "admin_weekplan.asp?containerfield=id&containerid=" & session(tablename & "containerid") & "&action=form&id=" & rs("id")
      if hasmatch(specs, "jerusalem,muni,brener,") then tar = "_top" : u = "admin_courses.asp?containerid=0&action=form&id=" & rs("courseid")
      rr "<a target=""" & tar & """ href=" & u & ">"
      rr "<img src=icons/i_go2.gif border=0 style='position:relative; top:2;'> <b>~Fixed~</b>"
      if hasmatch(specs, "jerusalem,muni,brener,") and not brenerclasses then
        tt = " " & getcode("courses", rs("courseid")) & " " & gettitle("courses", rs("courseid"))
        if cstr("" & rs("courseid")) <> "" and cstr("" & rs("courseid")) <> "0" then
          t = getonefield("select data1 from courses where id = 0" & rs("courseid")) : t = getval(t, "hour1")
          if t <> "" then tt = " " & t & " " & tt
        end if
        if trim(tt) = "" then tt = " (��� �����)"
        rr tt
      else
	      if instr(rs("ways"), "A") > 0 then rr " ~Collect~ " & rs("hour1")
	      if instr(rs("ways"), "B") > 0 then rr " ~Distribute~ " & rs("hour1b")
      end if
      rr "</a><br>"
      rs.movenext
    loop

    s = "select * from " & tablename
    if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
    s = s & okreadsql(tablename)'onecell
']
    s = s & " and date1 >= " & sqldate(d & " 00:00:00") & " and date1 <= " & sqldate(d & " 23:59:59")
    s = replace(s," and "," where ",1,1,1)
    s = replace(s,"(" & tablename & ")",tablename,1,1,1)
    closers(rs) : set rs = conn.execute(sqlfilter(s))
    rc = 0
    do until rs.eof
      data1 =  rs("data1")
      rc = rc + 1
      rr "<div id=d" & rs("id") & " style='position:absolute; visibility:hidden;'></div>"
      tar = "" : u = "?action=form&id=" & rs("id")
      if hasmatch(specs, "jerusalem,muni,brener,") then tar = "_top" : u = "admin_courses.asp?containerid=0&action=form&id=" & rs("courseid")
      co = "000000" : if getval(data1, "draft") = "on" then co = "aaaaaa"
      rr "<a target=""" & tar & """ href=" & u & " style='color:#" & co & ";'>"
      rr "<font style='white-space: nowrap;'>"
'[      rr "<img src=icons/refresh.png border=0> " & rs("id") & " " & rs("date1") & "<br>"
'[      rr "</a>"
      rr "<img src=icons/refresh.png border=0> "
      if hasmatch(specs, "jerusalem,muni,brener,") then
        tt = " " & getcode("courses", rs("courseid")) & " " & gettitle("courses", rs("courseid"))
        if cstr("" & rs("courseid")) <> "" and cstr("" & rs("courseid")) <> "0" then
          t = getonefield("select data1 from courses where id = 0" & rs("courseid")) : t = getval(t, "hour1")
          if t <> "" then tt = " " & t & " " & tt
        end if
        if trim(tt) = "" then tt = " (��� �����)"
        rr tt
      else
	      if instr(rs("ways"),"A") > 0 then rr " ~Collect~ " & rs("hour1") '& "," else rr " ~Without~ ~Collect~,"
	      if instr(rs("ways"),"B") > 0 then rr " ~Distribute~ " & rs("hour1b") 'else rr " ~Without~ ~Distribute~"
	      if rs("ways") = "B" then 'if its only dist then swap from/to
	        if cdbl("0" & rs("to1")) > 0 then rr ", ~From~" & gettitle("stations",rs("to1"))
	        if cdbl("0" & rs("from1")) > 0 then rr ", ~To ~" & gettitle("stations",rs("from1"))
	      else
	        if cdbl("0" & rs("from1")) > 0 then rr ", ~From~" & gettitle("stations",rs("from1"))
	        if cdbl("0" & rs("to1")) > 0 then rr ", ~To ~" & gettitle("stations",rs("to1"))
	      end if
      end if
      if cdbl("0" & rs("foodsupplierid")) <> 0 then rr " <img src=icons/basket.png border=0>"
      rr "</a>"
      if getval(data1, "draft") = "on" then
        rr " <a href=?action=undraft&id=" & rs("id") & ">(���)</a>"
      end if
      rr "<br>"
']

      rs.movenext
    loop
  next
  for i = wd + 1 to 7: rr("<td>") : next
  rr("</table>")
'[  x = disablecontrolsnow("",disablecontrols,hidecontrols)
  x = disablecontrolsnow("",disablecontrols,hidecontrols)

  if pickedids <> "" then rr "<div><a href=admin_coursecal.asp?action=picked&ref=admin_workershifts.asp&v=on&orderids=" & pickedids & "&workerid=" & session(tablename & "containerid") & ">��� ������ ��� �����</a></div>"
  'if session(tablename & "_containerid") <> "" then
  '  rr "<script language=vbscript>"
  '  rr "parent.document.getelementbyid(""frworkershifts"").style.width = document.body.clientWidth + 500" & vbcrlf
  '  rr "</script>"
  'end if
']    
'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date1" : session(tablename & "_hidelistcolumns") = "data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
'[  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 100
']
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date1"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/refresh.png' ~iconsize~> ~workershifts_workershifts~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  if session(tablename & "containerfield") <> "workerid" then
    rr(" <nobr><img id=workerid_filtericon src=icons/suppliers.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=workerid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workershifts_workerid~")
    sq = "select * from workers order by firstname,lastname"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_workerid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("firstname") & " ")
      rr(rs1("lastname") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:workerid
  end if'search:workerid
  rr(" <nobr><img id=ways_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=ways dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~workershifts_ways~")
  aa = "X,AB,A,B," : a = aa
  'sq = "select distinct ways from workershifts order by ways"
  'set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof : a = a & rs1("ways") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if session(tablename & "_ways") = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select></nobr>")'search:ways
  if session(tablename & "containerfield") <> "foodsupplierid" then
    rr(" <nobr><img id=foodsupplierid_filtericon src=icons/basket.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=foodsupplierid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workershifts_foodsupplierid~")
    sq = "select * from foodsuppliers order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_foodsupplierid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:foodsupplierid
  end if'search:foodsupplierid
  if session(tablename & "containerfield") <> "from1" then
    rr(" <nobr><img id=from1_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=from1 dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workershifts_from1~")
    sq = "select * from stations order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_from1")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:from1
  end if'search:from1
  if session(tablename & "containerfield") <> "to1" then
    rr(" <nobr><img id=to1_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=to1 dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workershifts_to1~")
    sq = "select * from stations order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_to1")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:to1
  end if'search:to1
  if session(tablename & "containerfield") <> "courseid" then
    rr(" <nobr><img id=courseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=courseid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workershifts_courseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_courseid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:courseid
  end if'search:courseid
  if session(tablename & "containerfield") <> "shiftid" then
    rr(" <nobr><img id=shiftid_filtericon src=icons/refresh.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=shiftid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workershifts_shiftid~")
    sq = "select * from shifts order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_shiftid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:shiftid
  end if'search:shiftid
  if session(tablename & "containerfield") <> "userid0" then
'[    rr(" <nobr><img id=userid0_filtericon src=icons/hire-me.png style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=userid0 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~workershifts_userid0~")
'[    sq = "select * from buusers order by username"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_userid0")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("username") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:userid0
']
  end if'search:userid0
  rr(" <input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  if okedit("buttonswitchmonth") then rr(" &nbsp;&nbsp;&nbsp;&nbsp;<a href=?monthview=on>~Switch to month view~</a>")
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & workers_1.firstname) & ' ' & cstr('' & workers_1.lastname) as workeridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join workers workers_1 on " & tablename & ".workerid = workers_1.id)"
  s = replace(s, " from ", ", cstr('' & foodsuppliers_1.title) as foodsupplieridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join foodsuppliers foodsuppliers_1 on " & tablename & ".foodsupplierid = foodsuppliers_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_1.title) as from1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_1 on " & tablename & ".from1 = stations_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_2.title) as to1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_2 on " & tablename & ".to1 = stations_2.id)"
  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & shifts_1.title) as shiftidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join shifts shifts_1 on " & tablename & ".shiftid = shifts_1.id)"
  s = replace(s, " from ", ", cstr('' & buusers_1.username) as userid0lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join buusers buusers_1 on " & tablename & ".userid0 = buusers_1.id)"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","workershifts.id,workershifts.date1,workershifts.workerid,workershifts.ways,workershifts.hour1,workershifts.hour1b,workershifts.foodsupplierid,workershifts.from1,workershifts.to1,workershifts.courseid,workershifts.shiftid,workershifts.date0,workershifts.data1,workershifts.userid0",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_workerid") <> "" and session(tablename & "containerfield") <> "workerid" then s = s & " and " & tablename & ".workerid = " & session(tablename & "_workerid")
  if session(tablename & "_ways") <> "" then s = s & " and lcase(" & tablename & ".ways) = '" & lcase(session(tablename & "_ways")) & "'"
  if session(tablename & "_foodsupplierid") <> "" and session(tablename & "containerfield") <> "foodsupplierid" then s = s & " and " & tablename & ".foodsupplierid = " & session(tablename & "_foodsupplierid")
  if session(tablename & "_from1") <> "" and session(tablename & "containerfield") <> "from1" then s = s & " and " & tablename & ".from1 = " & session(tablename & "_from1")
  if session(tablename & "_to1") <> "" and session(tablename & "containerfield") <> "to1" then s = s & " and " & tablename & ".to1 = " & session(tablename & "_to1")
  if session(tablename & "_courseid") <> "" and session(tablename & "containerfield") <> "courseid" then s = s & " and " & tablename & ".courseid = " & session(tablename & "_courseid")
  if session(tablename & "_shiftid") <> "" and session(tablename & "containerfield") <> "shiftid" then s = s & " and " & tablename & ".shiftid = " & session(tablename & "_shiftid")
  if session(tablename & "_userid0") <> "" and session(tablename & "containerfield") <> "userid0" then s = s & " and " & tablename & ".userid0 = " & session(tablename & "_userid0")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " cstr('' & workers_1.firstname) & ' ' & cstr('' & workers_1.lastname) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".ways like '%" & w & "%' or"
        s = s & " " & tablename & ".hour1 like '%" & w & "%' or"
        s = s & " " & tablename & ".hour1b like '%" & w & "%' or"
        s = s & " cstr('' & foodsuppliers_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_2.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & shifts_1.title) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
        s = s & " cstr('' & buusers_1.username) & ' ' like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("date1") then f = "workershifts.date1" : f2 = "date1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("date1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_date1~</a> " & a2)
  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then
    f = "workers_1.firstname" : f2 = "workeridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_workerid~</a> " & a2)
  end if
  if oksee("ways") then f = "workershifts.ways" : f2 = "ways" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("ways") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_ways~</a> " & a2)
  if oksee("hour1") then f = "workershifts.hour1" : f2 = "hour1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("hour1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_hour1~</a> " & a2)
  if oksee("hour1b") then f = "workershifts.hour1b" : f2 = "hour1b" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("hour1b") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_hour1b~</a> " & a2)
  if oksee("foodsupplierid") and session(tablename & "containerfield") <> "foodsupplierid" then
    f = "foodsuppliers_1.title" : f2 = "foodsupplieridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_foodsupplierid~</a> " & a2)
  end if
  if oksee("from1") and session(tablename & "containerfield") <> "from1" then
    f = "stations_1.title" : f2 = "from1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_from1~</a> " & a2)
  end if
  if oksee("to1") and session(tablename & "containerfield") <> "to1" then
    f = "stations_2.title" : f2 = "to1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_to1~</a> " & a2)
  end if
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    f = "courses_1.code" : f2 = "courseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_courseid~</a> " & a2)
  end if
  if oksee("shiftid") and session(tablename & "containerfield") <> "shiftid" then
    f = "shifts_1.title" : f2 = "shiftidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_shiftid~</a> " & a2)
  end if
  if oksee("date0") then f = "workershifts.date0" : f2 = "date0" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("date0") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_date0~</a> " & a2)
  if oksee("data1") then rr("<td class=list_title><nobr>~workershifts_data1~")
  if oksee("userid0") and session(tablename & "containerfield") <> "userid0" then
    f = "buusers_1.username" : f2 = "userid0lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workershifts_userid0~</a> " & a2)
  end if

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
      Thedate1 = replace(thedate1,".","/")
      Theworkerid = rs("workerid") : if isnull(Theworkerid) then Theworkerid = 0
      Theworkerid = replace(theworkerid,",",".")
      Theways = rs("ways") : if isnull(Theways) then Theways = ""
      Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
      Thehour1b = rs("hour1b") : if isnull(Thehour1b) then Thehour1b = ""
      Thefoodsupplierid = rs("foodsupplierid") : if isnull(Thefoodsupplierid) then Thefoodsupplierid = 0
      Thefoodsupplierid = replace(thefoodsupplierid,",",".")
      Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
      Thefrom1 = replace(thefrom1,",",".")
      Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
      Theto1 = replace(theto1,",",".")
      Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
      Thecourseid = replace(thecourseid,",",".")
      Theshiftid = rs("shiftid") : if isnull(Theshiftid) then Theshiftid = 0
      Theshiftid = replace(theshiftid,",",".")
      Thedate0 = rs("date0") : if isnull(Thedate0) then Thedate0 = "1/1/1900"
      Thedate0 = replace(thedate0,".","/")
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Theuserid0 = rs("userid0") : if isnull(Theuserid0) then Theuserid0 = 0
      Theuserid0 = replace(theuserid0,",",".")
      Theworkeridlookup = rs("workeridlookup") : if isnull(Theworkeridlookup) then Theworkeridlookup = ""
      Thefoodsupplieridlookup = rs("foodsupplieridlookup") : if isnull(Thefoodsupplieridlookup) then Thefoodsupplieridlookup = ""
      Thefrom1lookup = rs("from1lookup") : if isnull(Thefrom1lookup) then Thefrom1lookup = ""
      Theto1lookup = rs("to1lookup") : if isnull(Theto1lookup) then Theto1lookup = ""
      Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
      Theshiftidlookup = rs("shiftidlookup") : if isnull(Theshiftidlookup) then Theshiftidlookup = ""
      Theuserid0lookup = rs("userid0lookup") : if isnull(Theuserid0lookup) then Theuserid0lookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thedate1
      if isnull(firstcoltitle) then firstcoltitle = "1/1/1900"
      if year(firstcoltitle) = 1900 then firstcoltitle = ""
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/refresh.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then rr("<td class=list_item valign=top align=><nobr>" & theworkeridlookup)
      if oksee("ways") then rr("<td class=list_item valign=top align=><nobr>" & theways)
      if oksee("hour1") then rr("<td class=list_item valign=top align=><nobr>" & thehour1)
      if oksee("hour1b") then rr("<td class=list_item valign=top align=><nobr>" & thehour1b)
      if oksee("foodsupplierid") and session(tablename & "containerfield") <> "foodsupplierid" then rr("<td class=list_item valign=top align=><nobr>" & thefoodsupplieridlookup)
      if oksee("from1") and session(tablename & "containerfield") <> "from1" then rr("<td class=list_item valign=top align=><nobr>" & thefrom1lookup)
      if oksee("to1") and session(tablename & "containerfield") <> "to1" then rr("<td class=list_item valign=top align=><nobr>" & theto1lookup)
      if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_item valign=top align=><nobr>" & thecourseidlookup)
      if oksee("shiftid") and session(tablename & "containerfield") <> "shiftid" then rr("<td class=list_item valign=top align=><nobr>" & theshiftidlookup)
      if oksee("date0") then rr("<td class=list_item valign=top align=><nobr>") : if year(thedate0) <> 1900 then rr(thedate0)
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
'[      if oksee("userid0") and session(tablename & "containerfield") <> "userid0" then rr("<td class=list_item valign=top align=><nobr>" & theuserid0lookup)
      if oksee("userid0") and session(tablename & "containerfield") <> "userid0" then rr("<td class=list_item valign=top align=><nobr>" & getuserorworker(theuserid0))
']
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then rr("<td class=list_total1>") 'workerid
  if oksee("ways") and session(tablename & "containerfield") <> "ways" then rr("<td class=list_total1>") 'ways
  if oksee("hour1") and session(tablename & "containerfield") <> "hour1" then rr("<td class=list_total1>") 'hour1
  if oksee("hour1b") and session(tablename & "containerfield") <> "hour1b" then rr("<td class=list_total1>") 'hour1b
  if oksee("foodsupplierid") and session(tablename & "containerfield") <> "foodsupplierid" then rr("<td class=list_total1>") 'foodsupplierid
  if oksee("from1") and session(tablename & "containerfield") <> "from1" then rr("<td class=list_total1>") 'from1
  if oksee("to1") and session(tablename & "containerfield") <> "to1" then rr("<td class=list_total1>") 'to1
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_total1>") 'courseid
  if oksee("shiftid") and session(tablename & "containerfield") <> "shiftid" then rr("<td class=list_total1>") 'shiftid
  if oksee("date0") and session(tablename & "containerfield") <> "date0" then rr("<td class=list_total1>") 'date0
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1
  if oksee("userid0") and session(tablename & "containerfield") <> "userid0" then rr("<td class=list_total1>") 'userid0

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if

  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
