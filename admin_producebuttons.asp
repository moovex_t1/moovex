<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 19/07/2017 12:38:54

function producebuttonsdelitem(i)
  producebuttonsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function producebuttonsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "1,2,3,4,5,6,7,8,9," : if getfield("days") <> "" then t = getfield("days")
  f = f & "days," : v = v & "'" & t & "',"

  t = "00:00" : if getfield("hour1") <> "" then t = getfield("hour1")
  f = f & "hour1," : v = v & "'" & t & "',"

  t = "23:59" : if getfield("hour2") <> "" then t = getfield("hour2")
  f = f & "hour2," : v = v & "'" & t & "',"

  t = "" : if getfield("onlyregulars") <> "" then t = getfield("onlyregulars")
  f = f & "onlyregulars," : v = v & "'" & t & "',"

  t = "AB" : if getfield("ways") <> "" then t = getfield("ways")
  f = f & "ways," : v = v & "'" & t & "',"

  t = "" : if getfield("shiftids") <> "" then t = getfield("shiftids")
  f = f & "shiftids," : v = v & "'" & t & "',"

  t = "" : if getfield("unitids") <> "" then t = getfield("unitids")
  f = f & "unitids," : v = v & "'" & t & "',"

  t = "0" : if getfield("d1") <> "" then t = getfield("d1")
  f = f & "d1," : v = v & "'" & t & "',"

  t = "00:00" : if getfield("h1") <> "" then t = getfield("h1")
  f = f & "h1," : v = v & "'" & t & "',"

  t = "0" : if getfield("d2") <> "" then t = getfield("d2")
  f = f & "d2," : v = v & "'" & t & "',"

  t = "23:59" : if getfield("h2") <> "" then t = getfield("h2")
  f = f & "h2," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
  if session("usersiteid") <> "0" then t = session("usersiteid")
']
  f = f & "siteid," : v = v & t & ","

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into producebuttons(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from producebuttons"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  producebuttonsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table producebuttons (id autoincrement primary key,title text(50),days memo,hour1 text(5),hour2 text(5),onlyregulars text(10),ways text(50),shiftids memo,unitids memo,d1 text(2),h1 text(5),d2 text(50),h2 text(5),siteid number,data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "producebuttons" : session("tablename") = "producebuttons"
tablefields = "id,title,days,hour1,hour2,onlyregulars,ways,shiftids,unitids,d1,h1,d2,h2,siteid,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'['-------------start--------------------------------------
if session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"
']
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = producebuttonsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  msg = msg & " ~Item Updated~"
  if not ishour(getfield("hour1")) then du = errorend("_back~Invalid~ ~producebuttons_hour1~")
  if not ishour(getfield("hour2")) then du = errorend("_back~Invalid~ ~producebuttons_hour2~")
  if not ishour(getfield("h1")) then du = errorend("_back~Invalid~ ~producebuttons_h1~")
  if not ishour(getfield("h1")) then du = errorend("_back~Invalid~ ~producebuttons_h2~")
']
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("days") then s = s & "days='" & getfield("days") & "',"
  if okedit("hour1") then s = s & "hour1='" & getfield("hour1") & "',"
  if okedit("hour2") then s = s & "hour2='" & getfield("hour2") & "',"
  if okedit("onlyregulars") then s = s & "onlyregulars='" & getfield("onlyregulars") & "',"
  if okedit("ways") then s = s & "ways='" & getfield("ways") & "',"
  if okedit("shiftids") then s = s & "shiftids='" & getfield("shiftids") & "',"
  if okedit("unitids") then s = s & "unitids='" & getfield("unitids") & "',"
  if okedit("d1") then s = s & "d1='" & getfield("d1") & "',"
  if okedit("h1") then s = s & "h1='" & getfield("h1") & "',"
  if okedit("d2") then s = s & "d2='" & getfield("d2") & "',"
  if okedit("h2") then s = s & "h2='" & getfield("h2") & "',"
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = producebuttonsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = producebuttonsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~producebuttons_title~^"
  if oksee("days") then csv(0) = csv(0) & "~producebuttons_days~^"
  if oksee("hour1") then csv(0) = csv(0) & "~producebuttons_hour1~^"
  if oksee("hour2") then csv(0) = csv(0) & "~producebuttons_hour2~^"
  if oksee("onlyregulars") then csv(0) = csv(0) & "~producebuttons_onlyregulars~^"
  if oksee("ways") then csv(0) = csv(0) & "~producebuttons_ways~^"
  if oksee("shiftids") then csv(0) = csv(0) & "~producebuttons_shiftids~^"
  if oksee("unitids") then csv(0) = csv(0) & "~producebuttons_unitids~^"
  if oksee("d1") then csv(0) = csv(0) & "~producebuttons_d1~^"
  if oksee("h1") then csv(0) = csv(0) & "~producebuttons_h1~^"
  if oksee("d2") then csv(0) = csv(0) & "~producebuttons_d2~^"
  if oksee("h2") then csv(0) = csv(0) & "~producebuttons_h2~^"
  if oksee("siteid") then csv(0) = csv(0) & "~producebuttons_siteid~^"
  if oksee("data1") then csv(0) = csv(0) & "~producebuttons_data1~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
  Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
  Thehour2 = rs("hour2") : if isnull(Thehour2) then Thehour2 = ""
  Theonlyregulars = rs("onlyregulars") : if isnull(Theonlyregulars) then Theonlyregulars = ""
  Theways = rs("ways") : if isnull(Theways) then Theways = ""
  Theshiftids = rs("shiftids") : Theshiftids = formatidlist(Theshiftids)
  Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
  Thed1 = rs("d1") : if isnull(Thed1) then Thed1 = ""
  Theh1 = rs("h1") : if isnull(Theh1) then Theh1 = ""
  Thed2 = rs("d2") : if isnull(Thed2) then Thed2 = ""
  Theh2 = rs("h2") : if isnull(Theh2) then Theh2 = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("days") then csv(rc) = csv(rc) & replace(thedays,vbcrlf," ") & "^"
    if oksee("hour1") then csv(rc) = csv(rc) & replace(thehour1,vbcrlf," ") & "^"
    if oksee("hour2") then csv(rc) = csv(rc) & replace(thehour2,vbcrlf," ") & "^"
    if oksee("onlyregulars") then csv(rc) = csv(rc) & replace(theonlyregulars,vbcrlf," ") & "^"
    if oksee("ways") then csv(rc) = csv(rc) & replace(theways,vbcrlf," ") & "^"
      t = "" : d = formatidlist(theshiftids)
      sq = "select * from shifts where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    if oksee("shiftids") then csv(rc) = csv(rc) & t & "^"
      t = "" : d = formatidlist(theunitids)
      sq = "select * from units where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    if oksee("unitids") then csv(rc) = csv(rc) & t & "^"
    if oksee("d1") then csv(rc) = csv(rc) & replace(thed1,vbcrlf," ") & "^"
    if oksee("h1") then csv(rc) = csv(rc) & replace(theh1,vbcrlf," ") & "^"
    if oksee("d2") then csv(rc) = csv(rc) & replace(thed2,vbcrlf," ") & "^"
    if oksee("h2") then csv(rc) = csv(rc) & replace(theh2,vbcrlf," ") & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
      s = "" : thisid = "0"
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "title" then
          if len(b) > 50 then b = left(b,50)
          if okedit("title") then s = s & "title='" & b & "',"
        elseif csvff(i) = "days" then
          if okedit("days") then s = s & "days='" & b & "',"
        elseif csvff(i) = "hour1" then
          if len(b) > 5 then b = left(b,5)
          if okedit("hour1") then s = s & "hour1='" & b & "',"
        elseif csvff(i) = "hour2" then
          if len(b) > 5 then b = left(b,5)
          if okedit("hour2") then s = s & "hour2='" & b & "',"
        elseif csvff(i) = "onlyregulars" then
          if len(b) > 10 then b = left(b,10)
          if okedit("onlyregulars") then s = s & "onlyregulars='" & b & "',"
        elseif csvff(i) = "ways" then
          if len(b) > 50 then b = left(b,50)
          if okedit("ways") then s = s & "ways='" & b & "',"
        elseif csvff(i) = "shiftids" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from shifts where lcase(cstr('' & title)) = '" & lcase(t) & "'"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("shiftids") then s = s & "shiftids='" & v & "',"
        elseif csvff(i) = "unitids" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from units where lcase(cstr('' & title)) = '" & lcase(t) & "'"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("unitids") then s = s & "unitids='" & v & "',"
        elseif csvff(i) = "d1" then
          if len(b) > 2 then b = left(b,2)
          if okedit("d1") then s = s & "d1='" & b & "',"
        elseif csvff(i) = "h1" then
          if len(b) > 5 then b = left(b,5)
          if okedit("h1") then s = s & "h1='" & b & "',"
        elseif csvff(i) = "d2" then
          if len(b) > 50 then b = left(b,50)
          if okedit("d2") then s = s & "d2='" & b & "',"
        elseif csvff(i) = "h2" then
          if len(b) > 5 then b = left(b,5)
          if okedit("h2") then s = s & "h2='" & b & "',"
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("siteid") then s = s & "siteid=" & v & ","
        elseif csvff(i) = "data1" then
          if okedit("data1") then s = s & "data1='" & b & "',"
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = producebuttonsinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","producebuttons.id,producebuttons.title,producebuttons.days,producebuttons.hour1,producebuttons.hour2,producebuttons.onlyregulars,producebuttons.ways,producebuttons.shiftids,producebuttons.unitids,producebuttons.d1,producebuttons.h1,producebuttons.d2,producebuttons.h2,producebuttons.siteid,producebuttons.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
  Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
  Thehour2 = rs("hour2") : if isnull(Thehour2) then Thehour2 = ""
  Theonlyregulars = rs("onlyregulars") : if isnull(Theonlyregulars) then Theonlyregulars = ""
  Theways = rs("ways") : if isnull(Theways) then Theways = ""
  Theshiftids = rs("shiftids") : Theshiftids = formatidlist(Theshiftids)
  Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
  Thed1 = rs("d1") : if isnull(Thed1) then Thed1 = ""
  Theh1 = rs("h1") : if isnull(Theh1) then Theh1 = ""
  Thed2 = rs("d2") : if isnull(Thed2) then Thed2 = ""
  Theh2 = rs("h2") : if isnull(Theh2) then Theh2 = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "title,days,hour1,hour2,onlyregulars,ways,shiftids,shiftids_select,shiftids_trigger,unitids,unitids_select,unitids_trigger,d1,h1,d2,h2,siteid,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><!img src='icons/settings.png' ~iconsize~> ~producebuttons_producebuttons~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~producebuttons_title~<img src=must.gif></span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  end if'form:title

  if oksee("days") then'form:days
    rr("<tr valign=top id=days_tr0><td class=form_item1 id=days_tr1><span id=days_caption>~producebuttons_days~</span><td class=form_item2 id=days_tr2>")
'[    c = 0 : s = "" : a = "1,2,3,4,5,6,7,8,9,"
    c = 0 : s = "" : a = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
']
    cc = countstring(a,",")
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
      rr("<input type=checkbox name=days_c" & c)
      if instr("," & thedays & ",","," & b & ",") > 0 then rr(" checked")
      rr(" onclick='vbscript:populatedays'>")
'[      rr(b & " ")
      rr("~weekday" & b & "~&nbsp;&nbsp;&nbsp;")
']
      s = s & "if f1.days_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
    loop
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub populatedays" & vbcrlf)
    rr("    t = """"" & vbcrlf)
    rr(s & vbcrlf)
    rr("    f1.days.value = t" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("</script>" & vbcrlf)
    rr("<input type=hidden name=days value=""" & Thedays & """>")
  end if'form:days

  if oksee("hour1") then'form:hour1
'[    rr(" <span id=hour1_caption class=form_item3>~producebuttons_hour1~</span> ")
'[    rr("<input type=text maxlength=5 name=hour1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thehour1 & """ dir=ltr>")
    rr(" <span id=hour1_caption class=form_item3></span> ")
    rr("<input type=text maxlength=5 name=hour1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & Thehour1 & """ dir=ltr>")
']
  end if'form:hour1

  if oksee("hour2") then'form:hour2
'[    rr(" <span id=hour2_caption class=form_item3>~producebuttons_hour2~</span> ")
'[    rr("<input type=text maxlength=5 name=hour2 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thehour2 & """ dir=ltr>")
    rr(" <span id=hour2_caption class=form_item3>~To~</span> ")
    rr("<input type=text maxlength=5 name=hour2 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & Thehour2 & """ dir=ltr>")
']
  end if'form:hour2

  if oksee("onlyregulars") then'form:onlyregulars
'[    rr("<tr valign=top id=onlyregulars_tr0><td class=form_item1 id=onlyregulars_tr1><span id=onlyregulars_caption>~producebuttons_onlyregulars~</span><td class=form_item2 id=onlyregulars_tr2>")
'[    rr("<input type=text maxlength=10 name=onlyregulars onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theonlyregulars & """ dir=~langdir~>")
    rr("<tr valign=top id=onlyregulars_tr0><td class=form_item1 id=onlyregulars_tr1><span id=onlyregulars_caption>~Production Type~</span><td class=form_item2 id=onlyregulars_tr2>")
    rr "<select name=onlyregulars>"
    rr "<option value=''" : if theonlyregulars = "" then rr " selected"
    rr ">~With~ ~Regular rides~"
    rr "<option value='on'" : if theonlyregulars = "on" then rr " selected"
    rr ">~Regular rides~ ~Only~"
    rr "<option value='no'" : if theonlyregulars = "no" then rr " selected"
    rr ">~Without~ ~Regular rides~"
    rr "<option value='ordertaxi'" : if theonlyregulars = "ordertaxi" then rr " selected"
    rr ">~Geo Production~"
    rr "</select>"
']
  end if'form:onlyregulars

  if oksee("ways") then'form:ways
    rr("<tr valign=top id=ways_tr0><td class=form_item1 id=ways_tr1><span id=ways_caption>~producebuttons_ways~</span><td class=form_item2 id=ways_tr2>")
    a = "AB,A,B,"
    rr("<select name=ways dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Theways = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:ways
  end if'form:ways

  if oksee("shiftids") then'form:shiftids
    rr("<tr valign=top id=shiftids_tr0><td class=form_item1 id=shiftids_tr1><span id=shiftids_caption>~producebuttons_shiftids~</span><td class=form_item2 id=shiftids_tr2>")
'[    rr(selectlookupmultiauto("f1.shiftids",theshiftids, "icons/refresh.png", "shifts,title,", 300, okwritehere))
    t = "" : if cstr(thesiteid) <> "0" then t = ":select id,title from shifts where siteid in(" & thesiteid & ",0) order by title"
    rr(selectlookupmultiauto("f1.shiftids",theshiftids, "icons/refresh.png", "shifts,title," & t, 300, okwritehere))
']
  end if'form:shiftids

  if oksee("unitids") then'form:unitids
    rr("<tr valign=top id=unitids_tr0><td class=form_item1 id=unitids_tr1><span id=unitids_caption>~producebuttons_unitids~</span><td class=form_item2 id=unitids_tr2>")
'[    rr(selectlookupmultiauto("f1.unitids",theunitids, "icons/i_shield.gif", "units,title,", 300, okwritehere))
    t = "" : if cstr(thesiteid) <> "0" then t = ":select id,title from units where siteid in(" & thesiteid & ",0) order by title"
    rr(selectlookupmultiauto("f1.unitids",theunitids, "icons/i_shield.gif", "units,title," & t, 300, okwritehere))
']
  end if'form:unitids

  if oksee("d1") then'form:d1
'[    rr("<tr valign=top id=d1_tr0><td class=form_item1 id=d1_tr1><span id=d1_caption>~producebuttons_d1~</span><td class=form_item2 id=d1_tr2>")
    rr("<tr valign=top id=d1_tr0><td class=form_item1 id=d1_tr1><span id=d1_caption>~Production~ ~producebuttons_d1~</span><td class=form_item2 id=d1_tr2>")
']
    a = "0,1,2,3,4,5,6,"
    rr("<select name=d1 dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Thed1 = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:d1
  end if'form:d1

  if oksee("h1") then'form:h1
'[    rr(" <span id=h1_caption class=form_item3>~producebuttons_h1~</span> ")
'[    rr("<input type=text maxlength=5 name=h1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theh1 & """ dir=ltr>")
    rr(" <span id=h1_caption class=form_item3>~Hour~</span> ")
    rr("<input type=text maxlength=5 name=h1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & Theh1 & """ dir=ltr>")
    rr " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:10;'>0 = ~Today~ , 1 = ~Tomorrow~ ...</span>"
']
  end if'form:h1

  if oksee("d2") then'form:d2
    rr("<tr valign=top id=d2_tr0><td class=form_item1 id=d2_tr1><span id=d2_caption>~producebuttons_d2~</span><td class=form_item2 id=d2_tr2>")
    a = "0,1,2,3,4,5,6,7,"
    rr("<select name=d2 dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Thed2 = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:d2
  end if'form:d2

  if oksee("h2") then'form:h2
'[    rr(" <span id=h2_caption class=form_item3>~producebuttons_h2~</span> ")
'[    rr("<input type=text maxlength=5 name=h2 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theh2 & """ dir=ltr>")
    rr(" <span id=h2_caption class=form_item3>~Hour~</span> ")
    rr("<input type=text maxlength=5 name=h2 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & Theh2 & """ dir=ltr>")
']
  end if'form:h2

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~producebuttons_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
'[    rr("<select name=siteid dir=~langdir~>")
    rr("<select name=siteid dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : checkform'>")
']
    rr("<option value=0 style='color:bbbbfe;'>~producebuttons_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
'[  end if'form:data1
  end if'form:data1

  if hasmatch(specs, "iec,phibro,") then
    rr("<tr valign=top><td class=form_item1>��� ���� ������<td class=form_item2>")
    rr "<input type=checkbox name=joining" : if getval(thedata1, "joining") = "on" then rr " checked"
    rr " onclick='vbscript: if me.checked then f1.data1.value = setval(f1.data1.value, ""joining"", ""on"") else f1.data1.value = setval(f1.data1.value, ""joining"", """")'"
    rr ">"

  end if
']
  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
'[  rr("</tr></form>" & bottombar)'form
  rr("</tr></form>" & bottombar)'form

  '--schedule example
  if session("userlevel") = "5" then
    rr "<tr><td colspan=9 style='color:#aaaaaa; direction:ltr; font-size:10;'>"
    u = ""
    u = u & "sched54321.asp?action=produce"
    u = u & "&siteid=" & thesiteid
    u = u & "&ways=" & theways
    u = u & "&shiftids=" & theshiftids
    u = u & "&unitids=" & theunitids
    u = u & "&producebuttonid=" & theid
    u = u & "&onlyregulars=" & theonlyregulars
    u = u & "&d1=" & thed1 & "/" & theh1
    u = u & "&d2=" & thed2 & "/" & theh2
    if getval(thedata1, "joining") = "on" then u = u & "&joining=on"
    rr u
  end if
']

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  rr(checkformadd)
  if okedit("title") then rr("    if f1.title.value <> """" then validfield(""title"") else invalidfield(""title"") : ok = false" & vbcrlf)
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : du = rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = "data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><!img src='icons/settings.png' ~iconsize~> ~producebuttons_producebuttons~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~producebuttons_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
  end if'search:siteid
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","producebuttons.id,producebuttons.title,producebuttons.days,producebuttons.hour1,producebuttons.hour2,producebuttons.onlyregulars,producebuttons.ways,producebuttons.shiftids,producebuttons.unitids,producebuttons.d1,producebuttons.h1,producebuttons.d2,producebuttons.h2,producebuttons.siteid,producebuttons.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".days like '%" & w & "%' or"
        s = s & " " & tablename & ".hour1 like '%" & w & "%' or"
        s = s & " " & tablename & ".hour2 like '%" & w & "%' or"
        s = s & " " & tablename & ".onlyregulars like '%" & w & "%' or"
        s = s & " " & tablename & ".ways like '%" & w & "%' or"
        t = "select top 50 id from shifts where (cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".shiftids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        t = "select top 50 id from units where (cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".unitids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        s = s & " " & tablename & ".d1 like '%" & w & "%' or"
        s = s & " " & tablename & ".h1 like '%" & w & "%' or"
        s = s & " " & tablename & ".d2 like '%" & w & "%' or"
        s = s & " " & tablename & ".h2 like '%" & w & "%' or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("title") then f = "producebuttons.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_title~</a> " & a2)
  if oksee("days") then rr("<td class=list_title><nobr>~producebuttons_days~")
  if oksee("hour1") then f = "producebuttons.hour1" : f2 = "hour1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("hour1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_hour1~</a> " & a2)
  if oksee("hour2") then f = "producebuttons.hour2" : f2 = "hour2" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("hour2") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_hour2~</a> " & a2)
  if oksee("onlyregulars") then f = "producebuttons.onlyregulars" : f2 = "onlyregulars" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("onlyregulars") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_onlyregulars~</a> " & a2)
  if oksee("ways") then f = "producebuttons.ways" : f2 = "ways" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("ways") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_ways~</a> " & a2)
  if oksee("shiftids") then rr("<td class=list_title><nobr>~producebuttons_shiftids~")
  if oksee("unitids") then rr("<td class=list_title><nobr>~producebuttons_unitids~")
  if oksee("d1") then f = "producebuttons.d1" : f2 = "d1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("d1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_d1~</a> " & a2)
  if oksee("h1") then f = "producebuttons.h1" : f2 = "h1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("h1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_h1~</a> " & a2)
  if oksee("d2") then f = "producebuttons.d2" : f2 = "d2" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("d2") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_d2~</a> " & a2)
  if oksee("h2") then f = "producebuttons.h2" : f2 = "h2" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("h2") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_h2~</a> " & a2)
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~producebuttons_siteid~</a> " & a2)
  end if
  if oksee("data1") then rr("<td class=list_title><nobr>~producebuttons_data1~")

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
      Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
      Thehour2 = rs("hour2") : if isnull(Thehour2) then Thehour2 = ""
      Theonlyregulars = rs("onlyregulars") : if isnull(Theonlyregulars) then Theonlyregulars = ""
      Theways = rs("ways") : if isnull(Theways) then Theways = ""
      Theshiftids = rs("shiftids") : Theshiftids = formatidlist(Theshiftids)
      Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
      Thed1 = rs("d1") : if isnull(Thed1) then Thed1 = ""
      Theh1 = rs("h1") : if isnull(Theh1) then Theh1 = ""
      Thed2 = rs("d2") : if isnull(Thed2) then Thed2 = ""
      Theh2 = rs("h2") : if isnull(Theh2) then Theh2 = ""
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/settings.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      thedays = thedays : if isnull(thedays) then thedays = ""
      thedays = replace(thedays,"<br>"," ") : thedays = replace(thedays,"<","[") : thedays = replace(thedays,">","]") : if len(thedays) > 30 then thedays = left(thedays,30) & "..."
'[      if oksee("days") then rr("<td class=list_item valign=top align=><nobr>" & thedays)
      t = thedays : if t <> "" then t = "," & t : t = left(t, len(t)-1) : t = replace(t, ",", "~, ~weekday") & "~" : t = mid(t,4)
      if oksee("days") then rr("<td class=list_item valign=top align=><nobr>" & t)
']
      if oksee("hour1") then rr("<td class=list_item valign=top align=><nobr>" & thehour1)
      if oksee("hour2") then rr("<td class=list_item valign=top align=><nobr>" & thehour2)
      if oksee("onlyregulars") then rr("<td class=list_item valign=top align=><nobr>" & theonlyregulars)
      if oksee("ways") then rr("<td class=list_item valign=top align=><nobr>" & theways)
      t = "" : d = formatidlist(theshiftids)
      sq = "select * from shifts where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("title")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("shiftids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      t = "" : d = formatidlist(theunitids)
      sq = "select * from units where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("title")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("unitids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      if oksee("d1") then rr("<td class=list_item valign=top align=><nobr>" & thed1)
      if oksee("h1") then rr("<td class=list_item valign=top align=><nobr>" & theh1)
      if oksee("d2") then rr("<td class=list_item valign=top align=><nobr>" & thed2)
      if oksee("h2") then rr("<td class=list_item valign=top align=><nobr>" & theh2)
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("days") and session(tablename & "containerfield") <> "days" then rr("<td class=list_total1>") 'days
  if oksee("hour1") and session(tablename & "containerfield") <> "hour1" then rr("<td class=list_total1>") 'hour1
  if oksee("hour2") and session(tablename & "containerfield") <> "hour2" then rr("<td class=list_total1>") 'hour2
  if oksee("onlyregulars") and session(tablename & "containerfield") <> "onlyregulars" then rr("<td class=list_total1>") 'onlyregulars
  if oksee("ways") and session(tablename & "containerfield") <> "ways" then rr("<td class=list_total1>") 'ways
  if oksee("shiftids") and session(tablename & "containerfield") <> "shiftids" then rr("<td class=list_total1>") 'shiftids
  if oksee("unitids") and session(tablename & "containerfield") <> "unitids" then rr("<td class=list_total1>") 'unitids
  if oksee("d1") and session(tablename & "containerfield") <> "d1" then rr("<td class=list_total1>") 'd1
  if oksee("h1") and session(tablename & "containerfield") <> "h1" then rr("<td class=list_total1>") 'h1
  if oksee("d2") and session(tablename & "containerfield") <> "d2" then rr("<td class=list_total1>") 'd2
  if oksee("h2") and session(tablename & "containerfield") <> "h2" then rr("<td class=list_total1>") 'h2
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
