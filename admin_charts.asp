<!--#include file="include.asp"-->
<%'serverfunctions
'[ Never try to build this ...

if session("usergroup") <> "admin" then x = errorend("~Access Denied~")

'-------------functions---------------------------------
if getfield("action") = "showchart" then

  '==================special charts===========================
  'rr amchart("Beer consumption by country", "NRG,Walla,Ynet,Haaretz,noam,kiki,", "Benjamin:90,112,75,15,1,44,|Government:100,122,85,25,11,54,|", "line")
  if getfield("id") = "a" then
    title1 = translate("~Cost Analysis~") : titles = "" : values = ""
    titles = "" : y = year(date) - 1 : m = month(date) + 1 : if m = 13 then m = 1 : y = y + 1
    vv1 = translate("~Cost (K)~:") : vv2 = translate("~No. of Workers~:") : vv3 = translate("~Worker Average~:") : vv4 = translate("~Car Usage~ (%):")
    for i = 1 to 12
      t = "" : if i = 12 then t = "Pt"
      titles = titles & m & "/" & right(y,2) & t & ","

      s = "select sum(price) as x from orders where (status = 'Done' or status = 'Done Approved') and month(date1) = " & m & " and year(date1) = " & y
      set rs = conn.execute(sqlfilter(s))
      x = rs("x") : if isnull(x) then x = 0
      vv1 = vv1 & fix(x / 1000) & ","

      s = "select pids,carid from orders where (status = 'Done' or status = 'Done Approved') and month(date1) = " & m & " and year(date1) = " & y
      set rs = conn.execute(sqlfilter(s))
      ww = "" : c = 0 : workercount = 0 : seatcount = 0

      do until rs.eof
        c = c + 1
        ww = ww & rs("pids")
        workercount = workercount + countstring(rs("pids"),",")
        seatcount = seatcount + cdbl("0" & getval(session("carpassengers"), rs("carid")))
        rs.movenext
      loop
      ww = formatidlistunique(ww) : w = countstring(ww, ",")
      vv2 = vv2 & w & ","

      av = 0 : if w > 0 then av = fix(x / w)
      vv3 = vv3 & av & ","

      if seatcount = 0 then su = 0 else su = formatnumber(workercount / seatcount * 100,2,true,false,false)
      vv4 = vv4 & su & ","

      m = m + 1 : if m = 13 then m = 1 : y = y + 1
    next
    values = vv1 & "|" & vv2 & "|" & vv3 & "|" & vv4 & "|"

    rr amchart(title1,titles,values,"linemultipleaxis")
    rend
  end if


  if getfield("id") = "b" then
    title1 = "~Supplier Complains~" : titles = "" : values = ""
    titles = "" : y = year(date) - 1 : m = month(date) + 1 : if m = 13 then m = 1 : y = y + 1
    uuu = getidlist("select id from buusers order by username")
    for i = 1 to 12
      t = "" : if i = 12 then t = "Pt"
      titles = titles & m & "/" & right(y,2) & t & ","
      uu = uuu
      do until uu = ""
        u = left(uu,instr(uu,",")-1) : uu = mid(uu,instr(uu,",")+1)
        s = "select count(id) as x from complains where userid = " & u & " and month(date1) = " & m & " and year(date1) = " & y
        set rs1 = conn.execute(sqlfilter(s))
        x = rs1("x") : if isnull(x) then x = 0
        vv = setval(vv, u, getval(vv,u) & x & ",")
      loop
      m = m + 1 : if m = 13 then m = 1 : y = y + 1
    next
    uu = uuu : values = ""
    do until uu = ""
      u = left(uu,instr(uu,",")-1) : uu = mid(uu,instr(uu,",")+1)
      s = "select username from buusers where id = " & u
      set rs1 = conn.execute(sqlfilter(s))
      n = "" : if not rs1.eof then n = rs1("username")
      t = getval(vv,u) : t = replace(t,"0","") : t = replace(t,",","")
      'if t <> "" then values = values & n & ":" & getval(vv,u) & "|"
      values = values & n & ":" & getval(vv,u) & "|"
    loop
    rr amchart(title1,titles,values,"column")
    rend
  end if

  '================from ready-made request=================
  if getfield("title1") <> "" then
    title1 = getfield("title1") : if left(title1,7) = "session" then title1 = session(mid(title1,8))
    titles = getfield("titles") : if left(titles,7) = "session" then titles = session(mid(titles,8))
    values = getfield("values") : if left(values,7) = "session" then values = session(mid(values,8))
    design = getfield("design") : if left(design,7) = "session" then design = session(mid(design,8))
    rr amchart(title1, titles, values, design)
    rend
  end if

  '========================================================

  rr("<table cellpadding=20 border=0>")

  s = "select id,title,design,what,queries,timeline,days,date1,date2 from charts where id = " & getfield("id")
  set rs0 = conn.execute(sqlfilter(s))
  q = 0
  if rs0.eof then errorend("Not Found")

  q = q + 1
  chartid = rs0("id")
  title1 = rs0("title")
  design = rs0("design")
  what = rs0("what")
  queries = ctxt(rs0("queries"))
  timeline = rs0("timeline")
  days = rs0("days")
  date1 = rs0("date1") : if year(date1) = 1900 then date1 = ""
  date2 = rs0("date2") : if year(date2) = 1900 then date2 = ""
  if cstr(days) <> "0" then	date1 = dateadd("d", cdbl("-" & days) + 1, date) : date2 = ""
  redim chartvalues(10000,3)
  titles = "" : values = "" : links = ""

  '---
  if instr(what,"/Query") then
    dates = ""
    if timeline = "Months" then
      if date1 = "" then date1 = dateadd("yyyy",-2,date)
      if date2 = "" then date2 = date
      y = year(date1) : m = month(date1)
      c = 0
      do
        c = c + 1 : if c > 100 then exit do
        dates = dates & "1/" & m & "/" & y & " 00:00:00,"
        dates = dates & lastday(m) & "/" & m & "/" & y & " 23:59:59,"
        m = m + 1 : if m = 13 then m = 1 : y = y + 1
        if y > year(date2) or y = year(date2) and m > month(date2) then exit do
      loop
    elseif timeline = "Days" then
      if date1 = "" then date1 = dateadd("d",-30,date)
      if date2 = "" then date2 = date
      d = date1
      c = 0
      do
        c = c + 1 : if c > 100 then exit do
        dates = dates & d & " 00:00:00,"
        dates = dates & d & " 23:59:59,"
        d = dateadd("d",1,d)
        if d > cdate(date2) then exit do
      loop
    else
      dates = ",,"
    end if
    a = queries
    do until a = ""
      b = left(a,instr(a,"<br>")-1) : a = mid(a,instr(a,"<br>")+4)
      if timeline = "None" then 
        titles = titles & getval(b,"title") & ","
      else
        values = values & getval(b,"title") & ":"
        links = links & getval(b,"title") & ":"
      end if
      s = getval(b,"s") : s = decrypt(s,chartid)
      if what = "Items/Query" then s = "select count(orders.id) as x" & mid(s,instr(s," from "))
      if what = "Price/Query" then s = "select sum(orders.price) as x" & mid(s,instr(s," from "))
      s = left(s,instrrev(s,"order by ")-1)
      dd = dates
      do until dd = ""
        d1 = left(dd,instr(dd,",")-1) : dd = mid(dd,instr(dd,",")+1)
        d2 = left(dd,instr(dd,",")-1) : dd = mid(dd,instr(dd,",")+1)
        if d1 <> "" then
          s = replacefromto(s & " and", "", "and orders.date1 >=", " and", " and") : s = left(s,len(s)-4)
          s = replacefromto(s & " and", "", "and orders.date1 <=", " and", " and") : s = left(s,len(s)-4)
          s = s & " and orders.date1 >= " & sqldate(d1)
          s = s & " and orders.date1 <= " & sqldate(d2)
        elseif getval(b,"minutes") <> "" then
          s = replacefromto(s & " and", "", "and orders.date1 >=", " and", " and") : s = left(s,len(s)-4)
          s = replacefromto(s & " and", "", "and orders.date1 <=", " and", " and") : s = left(s,len(s)-4)
          s = s & " and orders.date1 >= " & sqldate(dateadd("n",-cdbl(getval(b,"minutes")),now))
        end if
        if instr(s," where ") = 0 then s = replace(s," and "," where ",1,1,1)
        set rs1 = conn.execute(sqlfilter(s))
        x = rs1("x") : if isnull(x) then x = 0
        t = ""
        if timeline = "Months" then t = month(d1) & "/" & right(year(d1),2)
        if timeline = "Days" then t = day(d1)
        if instr(values,"|") = 0 and timeline <> "None" then titles = titles & t & ","
        values = values & x & ","
        link = getval(b,"link")
        link = replacefromto(link, "", "&advanced=", "&", "&")
        link = replacefromto(link, "", "&containerid=", "&", "&")
        link = replace(link,"?containerid=0&","?")
        link = replace(link,",",";") & "&advanced=on&containerid=0"
        if timeline <> "None" then
          d1 = left(d1,instr(d1," ")-1)
          d2 = left(d2,instr(d2," ")-1)
          link = replacefromto(link, "", "&date1from=", "&", "&date1from=" & d1 & "&")
          link = replacefromto(link, "", "&date1to=", "&", "&date1to=" & d2 & "&")
          link = replacefromto(link, "", "&minutes=", "&", "&")
        end if
       links = links & siteurl & link & ","
      loop
      if timeline <> "None" then values = values & "|" : links = links & "|"
    loop
  end if

  rr amchart(title1, titles, values, design)

  rr("</table>")
  rend
end if
'=================================================================

']
'BU generated 21/02/2010 23:24:00

function chartsdelitem(i)
  chartsdelitem = 0
  dim rs, s
  if instr(locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id=" & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function chartsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""
  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"
  t = "" : if getfield("active") <> "" then t = getfield("active")
  f = f & "active," : v = v & "'" & t & "',"
  t = "0"
  sq = "select max(priority) as t from " & tablename
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  if not rs1.eof then t = rs1("t") : if isnull(t) then t = "0"
  t = cdbl(t) + 10
  if getfield("priority") <> "" then t = getfield("priority")
  f = f & "priority," : v = v & t & ","
  t = "" : if getfield("userids") <> "" then t = getfield("userids")
  f = f & "userids," : v = v & "'" & t & "',"
  t = "Items/Query" : if getfield("what") <> "" then t = getfield("what")
  f = f & "what," : v = v & "'" & t & "',"
  t = "" : if getfield("queries") <> "" then t = getfield("queries")
  f = f & "queries," : v = v & "'" & t & "',"
  t = "" : if getfield("timeline") <> "" then t = getfield("timeline")
  f = f & "timeline," : v = v & "'" & t & "',"
  t = "0" : if getfield("days") <> "" then t = getfield("days")
  f = f & "days," : v = v & t & ","
  t = "1/1/1900" : if getfield("date1") <> "" then t = getfield("date1")
  f = f & "date1," : v = v & sqldate(t) & ","
  t = "1/1/1900" : if getfield("date2") <> "" then t = getfield("date2")
  f = f & "date2," : v = v & sqldate(t) & ","
  t = "T X axis title=|<br>T Y axis title=|<br>S Type,Column,Bar,Line,Spline,Pyramid,Funnel,Pie,Doughnut,=Column|<br>S Drawing Style,Emboss,Cylinder,Wedge,LightToDark,=Cylinder|<br>C 3D=|<br>T Color=|<br>S Palette,Default,GrayScale,Excel,Light,Pastel,SeaGreen,Dundas,EarthTones,Fire,SemiTransparent,Berry,AcidWash,ArtDeco,DundasDark,Kindergarten,MilkShake,MidDay,RainbowVapors,Tanzanite,Vegas,WaterLilies,Tan,=Dundas|<br>" : if getfield("design") <> "" then t = getfield("design")
  f = f & "design," : v = v & "'" & t & "',"
  t = "" : if getfield("useridsc") <> "" then t = getfield("useridsc")
  f = f & "useridsc," : v = v & "'" & t & "',"
  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into " & tablename & "(" & f & ") values(" & v & ")"
  's = "insert into " & tablename & "(id," & f & ") select max(id) + 1," & v & " from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  chartsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table charts (id autoincrement,title text(100),active text(2),priority number,userids memo,what text(50),queries memo,timeline text(50),days number,date1 datetime,date2 datetime,design memo,useridsc memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "charts"

admin_top = true : admin_login = true
request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then session(tablename & "containerfield") = "" : childpage = false else childpage = true
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'['-------------start--------------------------------------
'hidecontrols = hidecontrols & "what,timeline,days,date1,date2,"
if getfield("action") = "showall" then

  rr "<table width=100% >"
  if ulev > 1 then rr "<tr><td colspan=2><br><center><a href=admin_charts.asp?containerid=0><b>~Click here to manage charts~</b></a><br>"

  s = "select id from charts where active = 'on' and (cstr(userids) = '' or instr(',' & cstr(userids), '," & session("userid") & ",') > 0) order by priority"
  set rs = conn.execute(sqlfilter(s))
  q = 0
  do until rs.eof
    q = q + 1
    'if cdbl(q / 2) <> cdbl(fix(q / 2)) then rr "<tr valign=top>"
    u = "admin_charts.asp?action=showchart&id=" & rs("id")
 		rr "<tr><td><center><iframe frameborder=0 scrolling=no width=610 height=410 src=" & u & "></iframe>"
    rs.movenext
  loop
  rrbottom
  rend
end if
']

'locked = ",1,2,3,"

'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 Then
  request_id = chartsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = "~Item Updated~"
  s = "update " & tablename & " set "
  s = s & "title='" & getfield("title") & "',"
  s = s & "active='" & getfield("active") & "',"
  s = s & "priority=" & getfield("priority") & ","
  s = s & "userids='" & getfield("userids") & "',"
  s = s & "what='" & getfield("what") & "',"
  s = s & "queries='" & getfield("queries") & "',"
  s = s & "timeline='" & getfield("timeline") & "',"
'[  s = s & "days=" & getfield("days") & ","
  s = s & "days=0" & getfield("days") & ","
']
  s = s & "date1=" & sqldate(getfield("date1")) & ","
  s = s & "date2=" & sqldate(getfield("date2")) & ","
  ''s = s & "design='" & getformdata("design") & "',"
  s = s & "design='" & getfield("design") & "',"
  s = s & "useridsc='" & getfield("useridsc") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id=" & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))

  '-------------afterupdate-------------
  if instr(request_action2,"addlookup,") > 0 then
    a = request_action2
    action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
    rred(url)

  elseif instr(request_action2,"editlookup,") > 0 then
    a = request_action2
    action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
    rred(url)

  elseif request_action2 = "stay" then
    request_action = "form"
    msg = "~Record was saved~"
  else
    request_action = request_action2
  end if
'-------------delete-------------------------------------
elseif request_action = "delete" then
  x = chartsdelitem(request_id)
  msg = "~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = chartsdelitem(b)
    c = c + 1
  loop
  msg = c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      s = s & ",active = '" & getfield("listupdate_active_" & b) & "'"
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id=" & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = c & " ~Items Updated~"
end if

'['-------------more actions-------------------------------------
if request_action2 = "cancelbuild" then
  session("buildchartid") = ""
end if
if request_action2 = "bringquery" then
  rred("admin_orders.asp?buildchartid=" & request_id & "&querytitle=-")
end if
if request_action2 = "addquery" and session("addquery") <> "" then
  s = "select queries from charts where id = " & request_id
  set rs = conn.execute(sqlfilter(s))
  queries = "" : if not rs.eof then queries = rs("queries") : queries = ctxt(queries)
  queries = removelines(queries,"|title=" & getval(session("addquery"),"title") & "|")
  queries = queries & session("addquery") & "<br>"
  session("addquery") = ""
  s = "update charts set queries = '" & queries & "' where id = " & request_id
  set rs = conn.execute(sqlfilter(s))
  session("buildchartid") = ""
end if
if request_action2 = "delquery" then
  s = "select queries from charts where id = " & request_id
  set rs = conn.execute(sqlfilter(s))
  queries = "" : if not rs.eof then queries = rs("queries") : queries = ctxt(queries)
  queries = removelines(queries,"|id=" & getfield("queryid") & "|")
  s = "update charts set queries = '" & queries & "' where id = " & request_id
  set rs = conn.execute(sqlfilter(s))
end if
']
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = getfield("backto")
  s = "select * from " & tablename & " where id=" & request_id
  if usingsql = 1 then s = replace(s,"*","charts.id,charts.title,charts.active,charts.priority,charts.userids,charts.what,charts.queries,charts.timeline,charts.days,charts.date1,charts.date2,charts.design,charts.useridsc",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
  Thepriority = replace(thepriority,",",".")
  Theuserids = rs("userids") : Theuserids = formatidlist(Theuserids)
  Thewhat = rs("what") : if isnull(Thewhat) then Thewhat = ""
  Thequeries = rs("queries") : if isnull(Thequeries) then Thequeries = ""
'[  Thequeries = replace(thequeries,"<br>",vbcrlf)
']
  Thetimeline = rs("timeline") : if isnull(Thetimeline) then Thetimeline = ""
  Thedays = rs("days") : if isnull(Thedays) then Thedays = 0
  Thedays = replace(thedays,",",".") : thedays = formatnumber(thedays,0,true,false,false)
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Thedate2 = rs("date2") : if isnull(Thedate2) then Thedate2 = "1/1/1900"
  Thedate2 = replace(thedate2,".","/")
  Thedesign = rs("design") : if isnull(Thedesign) then Thedesign = ""
  Theuseridsc = rs("useridsc") : Theuseridsc = formatidlist(Theuseridsc)
  formcontrols = "title,active,priority,userids,userids_select,userids_trigger,what,queries,timeline,days,date1,date1_day,date1_month,date1_year,date1_hour,date1_insertdate,date1_removedate,date2,date2_day,date2_month,date2_year,date2_hour,date2_insertdate,date2_removedate,design,useridsc,useridsc_select,useridsc_trigger,"
  if instr(locked,"," & request_id & ",") > 0 then locker = textlock

  rr("<center><table class=form_table>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name colspan=99><img src='icons/i_graph.gif' ~iconsize~> ~charts_charts~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~charts_title~</span><td class=form_item2 id=title_tr2>")
'[  rr("<input type=text maxlength=100 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  rr("<input type=text maxlength=100 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:200;' value=""" & Thetitle & """ dir=~langdir~>")
']

  rr(" <span id=active_caption>~charts_active~</span> ")
  rr("<input type=checkbox name=active")
  if theactive = "on" then rr(" checked")
  rr(">")

  rr(" <span id=priority_caption>~charts_priority~<img src=must.gif></span> ")
  rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:right;' name=priority value=""" & Thepriority & """>")

  rr("<tr valign=top id=userids_tr0><td class=form_item1 id=userids_tr1><span id=userids_caption>~charts_userids~</span><td class=form_item2 id=userids_tr2>")
  rr(selectlookupmultiauto("f1.userids",theuserids, "icons/i_user.gif", "buusers,firstname,lastname,", 300, okwritehere))

  rr("<tr valign=top id=what_tr0><td class=form_item1 id=what_tr1><span id=what_caption>~charts_what~</span><td class=form_item2 id=what_tr2>")
  a = "Items/Query,Price/Query,"
'[  rr("<select name=what dir=~langdir~>")
  'rr("<select name=what dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : checkform'>")
  rr("<select name=what dir=~langdir~>")
']
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if Thewhat = b then rr(" selected")
    if b = "Items/Query" then b = "~Order Count~"
    if b = "Price/Query" then b = "~Order Cost~"
    rr(">" & b)
  loop
  rr("</select>")'form:what

  rr("<tr valign=top id=queries_tr0><td class=form_item1 id=queries_tr1><span id=queries_caption>~charts_queries~</span><td class=form_item2 id=queries_tr2>")
'[  rr("<textarea name=queries style='width:300; height:100;' dir=~langdir~>" & thequeries & "</textarea>")
    rr("<input type=button value='~Add Chart Query~' onclick='vbscript: f1.target = ""_top"" : f1.action2.value = ""bringquery"" : checkform'><br>")
    rr("<input type=hidden name=queries value=""" & thequeries & """>")
    a = ctxt(thequeries)
    do until a = ""
      b = left(a,instr(a,"<br>")-1) : a = mid(a,instr(a,"<br>")+4)
      i = getval(b,"id")
      rr("<a href=?action=form&id=" & request_id & "&action2=delquery&queryid=" & getval(b,"id") & ">")
      rr("<img src=minus.gif border=0></a>")
      t = getval(b,"link") & "&"
      t = replacefromto(t, "", "&buildchartid=", "&", "&")
      t = replacefromto(t, "", "&buildchartid=", "&", "&")
      t = replacefromto(t, "", "&containerid=", "&", "&") & "&containerid=0"
      t = left(t,len(t)-1)
      t = t & "&buildchartid=" & request_id
      rr(" <a target=_top href=""" & t & """><img border=0 src=icons/i_query.gif> " & getval(b,"title") & "</a><br>")
    loop
']

  rr("<tr valign=top id=timeline_tr0><td class=form_item1 id=timeline_tr1><span id=timeline_caption>~charts_timeline~</span><td class=form_item2 id=timeline_tr2>")
  a = "None,Months,Days,"
  rr("<select name=timeline dir=~langdir~>")
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if Thetimeline = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select>")'form:timeline

  rr("<tr valign=top id=days_tr0><td class=form_item1 id=days_tr1><span id=days_caption>~charts_days~<img src=must.gif></span><td class=form_item2 id=days_tr2>")
  rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:right;' name=days value=""" & Thedays & """>")
  rr(" <font style='font-size:10;'>* ~Leave on 0 to enter specific dates~</font>")

  rr("<tr valign=top id=date1_tr0><td class=form_item1 id=date1_tr1><span id=date1_caption>~charts_date1~</span><td class=form_item2 id=date1_tr2>")
  rr(SelectDate("f1.date1", thedate1))

  rr("<tr valign=top id=date2_tr0><td class=form_item1 id=date2_tr1><span id=date2_caption>~charts_date2~</span><td class=form_item2 id=date2_tr2>")
  rr(SelectDate("f1.date2", thedate2))

'  ''''rr(formdata("design",thedesign))

  rr("<tr valign=top id=design_tr0><td class=form_item1 id=design_tr1><span id=design_caption>~charts_design~</span><td class=form_item2 id=design_tr2>")
  rr "<select name=design>"
  rr "<option " : if thedesign = "Columns" then rr " selected"
  rr ">Columns"
  rr "<option " : if thedesign = "Line" then rr " selected"
  rr ">Line"
  rr "<option " : if thedesign = "Pie" then rr " selected"
  rr ">Pie"
  rr "</select>"

  '-------------updatebar--------------------
  rr("<tr><td style='padding:10; padding-top:20;' class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
'[    rr(" <input type=button id=buttonsavereturn style='width:80;' value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button id=buttonpreview style='width:80;' value=""~Preview~"" onclick='vbscript: f1.action2.value = ""stay"" : f1.action3.value = ""preview"" : checkform'>")
    rr(" <input type=button id=buttonsavereturn style='width:80;' value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
']
']
    rr(" <input type=button id=buttonsave style='width:80;' value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if session("userlevel") = "5" or (instr(locked,"," & request_id & ",") = 0 and getval(session("userpermissions"),tablename & "_delete") = "on") then rr(" <input type=button id=buttondelete style='width:80;' value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "?id=" & request_id : if request_action2 = "newitem" then t = "~Cancel~" : tt = "?action=delete&id=" & request_id & "&token=" & session("token")
  rr(" <input type=button id=buttonback style='width:80;' value=""" & t & """ onclick='document.location=""" & tt & """'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

'[  '-------------checkform-------------
  if request_action3 = "preview" then
    rr("<center><iframe frameborder=0 scrolling=no width=600 height=400 src=?action=showchart&action2=preview&id=" & request_id & "></iframe>")
  end if
']

  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if session(tablename & "containerfield") <> "priority" then
    rr("    if isnumeric(f1.priority.value) then" & vbcrlf)
    rr("      if cdbl(f1.priority.value) >= -4000000000 and cdbl(f1.priority.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""priority"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""priority"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""priority"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if session(tablename & "containerfield") <> "days" then
    rr("    if isnumeric(f1.days.value) then" & vbcrlf)
    rr("      if cdbl(f1.days.value) >= -4000000000 and cdbl(f1.days.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""days"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""days"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""days"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  rr("        document.getElementById(""buttonsavereturn"").disabled = true" & vbcrlf)
  rr("        document.getElementById(""buttonsave"").disabled = true" & vbcrlf)
  rr("        document.getElementById(""buttonduplicate"").disabled = true" & vbcrlf)
  rr("        document.getElementById(""buttondelete"").disabled = true" & vbcrlf)
  rr("        document.getElementById(""buttonback"").disabled = true" & vbcrlf)
  rr("        document.getElementById(""buttonprint"").disabled = true" & vbcrlf)
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
else
  if request_action = "priorityshift" then
    ff = getfield("field") : i1 = request_id
    s = "select * from " & tablename & " where id = " & i1
    closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
    t = "" : if session(tablename & "containerid") <> "0" then t = " and " & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
    p1 = rs1(ff) : t1 = " < " : t2 = " desc" : if request_action2 = "down" then t1 = " > " : t2 = ""
    s = "select * from " & tablename & " where id <> " & i1 & " and " & ff & t1 & p1 & t & " order by " & ff & t2
    closers(rs2) : set rs2 = conn.execute(sqlfilter(s))
    if not rs2.eof then
      i2 = rs2("id") : p2 = rs2(ff)
      s = "update " & tablename & " set " & ff & " = " & p2 & " where id = " & i1
      closers(rs) : set rs = conn.execute(sqlfilter(s))
      s = "update " & tablename & " set " & ff & " = " & p1 & " where id = " & i2
      closers(rs) : set rs = conn.execute(sqlfilter(s))
    end if
  end if
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    a = ""
    For Each sessionitem in Session.Contents
      t = tablename & "_" : if left(sessionitem,len(t)) = t then a = a & sessionitem & ","
    Next
    do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : session(b) = "" : loop
    session(tablename & "_firstcome") = "y"
    session(tablename & "_order") = tablename & ".priority"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = Request.Form.Item(i) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = Request.Querystring.Item(i) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  If session(tablename & "_order") = "" Then session(tablename & "_order") = tablename & ".priority"
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")

  if session(tablename & "_advanced") = "y" then
  end if
  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")
  rr("<tr><td class=list_name colspan=99><img src='icons/i_graph.gif' ~iconsize~> ~charts_charts~ ")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  rr("<tr><td colspan=99 class=search_bar>")
  if ulev > 1 then rr("<input type=button style='width:80;' value=""~Add~"" onclick=window.document.location=""?action=insert&token=" & session("token") & """> ")
  rr("<input type=hidden name=action value=search>")
  rr("~Keywords~ <input type=text style='width:101;' name=string value=""" & session(tablename & "_string") & """>")
  rr("<select name=stringoption>")
  rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  rr(">~All words~")
  rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  rr(">~Any Word~")
  rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  rr(">~Exact phrase~")
  rr("</select>")

  rr(" <input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~")
  rr("<input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  rr(" <input type=button value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit value="" ~Show~ ""> ")

  if session(tablename & "_advanced") = "y" then
    rr(" <a href=?advanced=n>~No~ ~Advanced Search~</a>")
    rr("<br><div style='background:dddddd;'>")
    rr("</div>")
  else
    'rr(" <a href=?advanced=y>~Advanced Search~</a>")
  end if
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"

  '-sqladd
  s = s & sqladditions
  if usingsql = 1 then s = replace(s,tablename & ".*","charts.id,charts.title,charts.active,charts.priority,charts.userids,charts.what,charts.queries,charts.timeline,charts.days,charts.date1,charts.date2,charts.design,charts.useridsc",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") & " "
    do until ww = ""
      w = left(ww,instr(ww," ")-1) : ww = mid(ww,instr(ww," ")+1)
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".priority = " & w & " or"
        t = "select top 50 id from buusers where (cstr('' & firstname) & ' ' & cstr('' & lastname)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(',' & cstr(" & tablename & ".userids),'," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        s = s & " " & tablename & ".what like '%" & w & "%' or"
        s = s & " " & tablename & ".queries like '%" & w & "%' or"
        s = s & " " & tablename & ".timeline like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".days = " & w & " or"
        s = s & " " & tablename & ".design like '%" & w & "%' or"
        t = "select top 50 id from buusers where (cstr('' & username) & ' ' & cstr('' & firstname) & ' ' & cstr('' & lastname)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(',' & cstr(" & tablename & ".useridsc),'," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount
  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  f = "charts.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~charts_title~</a> " & a2)
  f = "charts.active" : f2 = "active" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~charts_active~</a> " & a2)
  f = "charts.priority" : f2 = "priority" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~charts_priority~</a> " & a2)
  f = "charts.what" : f2 = "what" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~charts_what~</a> " & a2)
  f = "charts.timeline" : f2 = "timeline" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~charts_timeline~</a> " & a2)
  f = "charts.days" : f2 = "days" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~charts_days~</a> " & a2)
  f = "charts.date1" : f2 = "date1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~charts_date1~</a> " & a2)
  f = "charts.date2" : f2 = "date2" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~charts_date2~</a> " & a2)
  rr("<td class=list_title><nobr>~charts_userids~")

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
      Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
      Thepriority = replace(thepriority,",",".")
      Theuserids = rs("userids") : Theuserids = formatidlist(Theuserids)
      Thewhat = rs("what") : if isnull(Thewhat) then Thewhat = ""
      Thequeries = rs("queries") : if isnull(Thequeries) then Thequeries = ""
      Thetimeline = rs("timeline") : if isnull(Thetimeline) then Thetimeline = ""
      Thedays = rs("days") : if isnull(Thedays) then Thedays = 0
      Thedays = replace(thedays,",",".") : thedays = formatnumber(thedays,0,true,false,false)
      Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
      Thedate1 = replace(thedate1,".","/")
      Thedate2 = rs("date2") : if isnull(Thedate2) then Thedate2 = "1/1/1900"
      Thedate2 = replace(thedate2,".","/")
      Thedesign = rs("design") : if isnull(Thedesign) then Thedesign = ""
      Theuseridsc = rs("useridsc") : Theuseridsc = formatidlist(Theuseridsc)

      okwritehere = okwrite(tablename,theid)
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("�" & groupseperators, "�" & rs(t) & "�") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "�"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      thiscolor = "ffffff"
      list_item_hover = "f6f6f6"
      rr("<tr bgcolor=" & thiscolor)
      rr(" id=tr" & rc & " style=cursor:hand;")
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""" & list_item_hover & """ : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""" & thiscolor & """ : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr(locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"

      if session(tablename & "_order") = tablename & ".priority" then
        rr("<a href=?action=priorityshift&action2=up&field=priority&id=" & theid & "><img src=up.gif border=0></a>")
        rr("<a href=?action=priorityshift&action2=down&field=priority&id=" & theid & "><img src=down.gif border=0></a> ")
      end if

      rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/i_graph.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if session(tablename & "containerfield") <> "active" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if session(tablename & "containerfield") = "active" then
        rr("<input type=hidden name=active value=" & theactive & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "active" then
        rr("<input type=checkbox name=listupdate_active_" & theid & "")
        if theactive = "on" then rr(" checked")
        rr(">")
      else
        rr(theactive)
      end if
      rr("<td class=list_item valign=top align=><nobr>" & thepriority)
      rr("<td class=list_item valign=top align=><nobr>" & thewhat)
      rr("<td class=list_item valign=top align=><nobr>" & thetimeline)
      rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(thedays,0,true,false,true))
      rr("<td class=list_item valign=top align=><nobr>") : if year(thedate1) <> 1900 then rr(thedate1)
      rr("<td class=list_item valign=top align=><nobr>") : if year(thedate2) <> 1900 then rr(thedate2)
      t = "" : d = formatidlist(theuserids)
      sq = "select * from buusers where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("username")
        t = t & " " & rs1("firstname")
        t = t & " " & rs1("lastname")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      rr("<td class=list_item valign=top align=><nobr>" & t)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr><td colspan=99 class=list_multi>")
  if ulev > 1 then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 then rr(" <input type=checkbox name=allidscheck>~All Results~ (" & rsrc & ")")
  if session("userlevel") = "5" or (ulev > 1 and getval(session("userpermissions"),tablename & "_delete") = "on") then rr(" <input type=button onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")
  if ulev > 1 then

    rr(" <input type=button value='~Update~' style='width:80;' onclick='vbscript:if msgbox(""~Update All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if

  rr("</tr></form>" & bottombar & "</table>")'list
end if

if childpage then rr("<" & "script language=vbscript>parent.document.getelementbyid(""fr" & tablename & """).height = document.body.scrollheight + 20</" & "script>")
rrbottom()
rend()
%>

