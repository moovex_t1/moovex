<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 15/07/2014 13:34:25

function complainsdelitem(i)
  complainsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function complainsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = now : if getfield("date1") <> "" then t = getfield("date1")
  f = f & "date1," : v = v & sqldate(t) & ","

'[  t = "New" : if getfield("status") <> "" then t = getfield("status")
  t = "" : if getfield("status") <> "" then t = getfield("status")
']
  f = f & "status," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "userid" then t = session(tablename & "containerid")
'[  if getfield("userid") <> "" then t = getfield("userid")
  t = session("userid")
']
  f = f & "userid," : v = v & t & ","

  t = "3" : if getfield("severity") <> "" then t = getfield("severity")
  f = f & "severity," : v = v & "'" & t & "',"

  t = "0" : if getfield("orderid") <> "" then t = getfield("orderid")
  f = f & "orderid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "ordersupplierid" then t = session(tablename & "containerid")
  if getfield("ordersupplierid") <> "" then t = getfield("ordersupplierid")
  f = f & "ordersupplierid," : v = v & t & ","

  t = "1/1/1900" : if getfield("orderdate1") <> "" then t = getfield("orderdate1")
  f = f & "orderdate1," : v = v & sqldate(t) & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "orderordertypeid" then t = session(tablename & "containerid")
  if getfield("orderordertypeid") <> "" then t = getfield("orderordertypeid")
  f = f & "orderordertypeid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "ordercourseid" then t = session(tablename & "containerid")
  if getfield("ordercourseid") <> "" then t = getfield("ordercourseid")
  f = f & "ordercourseid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "orderdriverid" then t = session(tablename & "containerid")
  if getfield("orderdriverid") <> "" then t = getfield("orderdriverid")
  f = f & "orderdriverid," : v = v & t & ","

  t = "" : if getfield("pids") <> "" then t = getfield("pids")
  f = f & "pids," : v = v & "'" & t & "',"

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "" : if getfield("description") <> "" then t = getfield("description")
  f = f & "description," : v = v & "'" & t & "',"

  t = "" : if getfield("supplierremarks") <> "" then t = getfield("supplierremarks")
  f = f & "supplierremarks," : v = v & "'" & t & "',"

  t = "" : if getfield("solution") <> "" then t = getfield("solution")
  f = f & "solution," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  t = session("usersiteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
']
  f = f & "siteid," : v = v & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into complains(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from complains"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  complainsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table complains (id autoincrement primary key,date1 datetime,status text(50),userid number,severity text(2),orderid number,ordersupplierid number,orderdate1 datetime,orderordertypeid number,ordercourseid number,orderdriverid number,pids memo,title text(100),description memo,supplierremarks memo,solution memo,siteid number)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "complains" : session("tablename") = "complains"
tablefields = "id,date1,status,userid,severity,orderid,ordersupplierid,orderdate1,orderordertypeid,ordercourseid,orderdriverid,pids,title,description,supplierremarks,solution,siteid,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 4
if session("usergroup") = "supplieradmin" then ulev = 4
if session("usergroup") = "manager" then ulev = 4
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 4
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'['-------------start--------------------------------------
if session("usergroup") = "worker" then
  ulev = 4
  hidecontrols = hidecontrols & "buttoncreatecsv,buttonmultiupdate,"
  disablecontrols = disablecontrols & "status,"
end if

if session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"
if session("userlevel") <> "5" then disablecontrols = disablecontrols & "userid,date1,orderdate1,orderordertypeid,ordercourseid,orderdriverid,ordersupplierid,"
if specs = "dsw" and session("usergroup") = "admin" then
  'allow delete
elseif session("userlevel") <> "5" then
  hidecontrols = hidecontrols & "buttondelete,buttonmultidelete,"
end if

if session("usergroup") = "admin" then 
  'nothing
elseif instr(",supplier,supplieradmin,", "," & session("usergroup") & ",") > 0 then
  t = tablefields
  t = replace(t, ",supplierremarks,", ",")
  t = replace(t, ",orderid,", ",")
  disablecontrols = disablecontrols & t
else
  disablecontrols = disablecontrols & "supplierremarks,solution,"
end if

if specs = "rafael" and session("usergroup") = "manager" then disablecontrols = disablecontrols & "pids,"

function loadorderdata(byval id, byval orderid)
  dim s,rs,a, date1,ordertypeid,courseid,driverid,supplierid
  s = "select date1,ordertypeid,courseid,driverid,supplierid,pids from orders where id = " & getfield("orderid")
  set rs = conn.execute(sqlfilter(s))
  date1 = "1/1/1900"
  ordertypeid = 0
  courseid = 0
  driverid = 0
  supplierid = 0
  pids = ""
  if not rs.eof then
    date1 = rs("date1") : if isnull(date1) then date1 = "1/1/1900"
    ordertypeid = rs("ordertypeid") : if isnull(ordertypeid) then ordertypeid = 0
    courseid = rs("courseid") : if isnull(courseid) then courseid = 0
    driverid = rs("driverid") : if isnull(driverid) then driverid = 0
    supplierid = rs("supplierid") : if isnull(supplierid) then supplierid = 0
    pids = rs("pids") & ""
  end if
  s = "update complains set "
  s = s & " orderdate1 = " & sqldate(date1)
  s = s & ",orderordertypeid = " & ordertypeid
  s = s & ",ordercourseid = " & courseid
  s = s & ",orderdriverid = " & driverid
  s = s & ",ordersupplierid = " & supplierid
  s = s & ",pids = '" & pids & "'"
  s = s & " where id = " & id
  set rs = conn.execute(sqlfilter(s))
end function
']
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = complainsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("date1") then s = s & "date1=" & sqldate(getfield("date1")) & ","
'[  if okedit("status") then s = s & "status='" & getfield("status") & "',"
  status = getfield("statuswas") : if okedit("status") then status = getfield("status")
  if status = "" then status = "New"
  s = s & "status='" & status & "',"
']
  if okedit("userid") then s = s & "userid=0" & getfield("userid") & ","
  if okedit("severity") then s = s & "severity='" & getfield("severity") & "',"
  if okedit("orderid") then s = s & "orderid=0" & getfield("orderid") & ","
  if okedit("ordersupplierid") then s = s & "ordersupplierid=0" & getfield("ordersupplierid") & ","
  if okedit("orderdate1") then s = s & "orderdate1=" & sqldate(getfield("orderdate1")) & ","
  if okedit("orderordertypeid") then s = s & "orderordertypeid=0" & getfield("orderordertypeid") & ","
  if okedit("ordercourseid") then s = s & "ordercourseid=0" & getfield("ordercourseid") & ","
  if okedit("orderdriverid") then s = s & "orderdriverid=0" & getfield("orderdriverid") & ","
  if okedit("pids") then s = s & "pids='" & getfield("pids") & "',"
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("description") then s = s & "description='" & getfield("description") & "',"
  if okedit("supplierremarks") then s = s & "supplierremarks='" & getfield("supplierremarks") & "',"
'[  if okedit("solution") then s = s & "solution='" & getfield("solution") & "',"
  if okedit("solution") then
    d = getfield("solution")
    if specs = "rail" and getfield("status") = "Closed" and getfield("status") <> getfield("statuswas") and instr(d, "���� �� ���") = 0 then d = d & "<br>���� �� ��� " & session("username")
    s = s & "solution='" & d & "',"
  end if
']
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'['-------------afterupdate-------------
if getfield("action") = "update" and getfield("action3") = "loadorderdata" then x = loadorderdata(request_id, getfield("orderid"))
if getfield("action") = "insert" and getfield("orderid") <> ""             then x = loadorderdata(request_id, getfield("orderid"))
if request_action = "update" and getfield("newitem") = "on" then
  du = sendemail(getparam("siteemail"), getparam("productionlogemail"), "", "", "����� ���� ������ ��� " & session("username") & " - ����� ���� " & request_id, "")
end if
']
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = complainsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = complainsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("date1") then csv(0) = csv(0) & "~complains_date1~^"
  if oksee("status") then csv(0) = csv(0) & "~complains_status~^"
  if oksee("userid") then csv(0) = csv(0) & "~complains_userid~^"
  if oksee("severity") then csv(0) = csv(0) & "~complains_severity~^"
  if oksee("orderid") then csv(0) = csv(0) & "~complains_orderid~^"
  if oksee("ordersupplierid") then csv(0) = csv(0) & "~complains_ordersupplierid~^"
  if oksee("orderdate1") then csv(0) = csv(0) & "~complains_orderdate1~^"
  if oksee("orderordertypeid") then csv(0) = csv(0) & "~complains_orderordertypeid~^"
  if oksee("ordercourseid") then csv(0) = csv(0) & "~complains_ordercourseid~^"
  if oksee("orderdriverid") then csv(0) = csv(0) & "~complains_orderdriverid~^"
  if oksee("pids") then csv(0) = csv(0) & "~complains_pids~^"
  if oksee("title") then csv(0) = csv(0) & "~complains_title~^"
  if oksee("description") then csv(0) = csv(0) & "~complains_description~^"
  if oksee("supplierremarks") then csv(0) = csv(0) & "~complains_supplierremarks~^"
  if oksee("solution") then csv(0) = csv(0) & "~complains_solution~^"
  if oksee("siteid") then csv(0) = csv(0) & "~complains_siteid~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Thestatus = rs("status") : if isnull(Thestatus) then Thestatus = ""
  Theuserid = rs("userid") : if isnull(Theuserid) then Theuserid = 0
  Theuserid = replace(theuserid,",",".")
  Theseverity = rs("severity") : if isnull(Theseverity) then Theseverity = ""
  Theorderid = rs("orderid") : if isnull(Theorderid) then Theorderid = 0
  Theorderid = replace(theorderid,",",".") : Theorderid = formatnumber(theorderid,0,true,false,false)
  Theordersupplierid = rs("ordersupplierid") : if isnull(Theordersupplierid) then Theordersupplierid = 0
  Theordersupplierid = replace(theordersupplierid,",",".")
  Theorderdate1 = rs("orderdate1") : if isnull(Theorderdate1) then Theorderdate1 = "1/1/1900"
  Theorderdate1 = replace(theorderdate1,".","/")
  Theorderordertypeid = rs("orderordertypeid") : if isnull(Theorderordertypeid) then Theorderordertypeid = 0
  Theorderordertypeid = replace(theorderordertypeid,",",".")
  Theordercourseid = rs("ordercourseid") : if isnull(Theordercourseid) then Theordercourseid = 0
  Theordercourseid = replace(theordercourseid,",",".")
  Theorderdriverid = rs("orderdriverid") : if isnull(Theorderdriverid) then Theorderdriverid = 0
  Theorderdriverid = replace(theorderdriverid,",",".")
  Thepids = rs("pids") : Thepids = formatidlist(Thepids)
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
  Thesupplierremarks = rs("supplierremarks") : if isnull(Thesupplierremarks) then Thesupplierremarks = ""
  Thesolution = rs("solution") : if isnull(Thesolution) then Thesolution = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Theuseridlookup = rs("useridlookup") : if isnull(Theuseridlookup) then Theuseridlookup = ""
  Theordersupplieridlookup = rs("ordersupplieridlookup") : if isnull(Theordersupplieridlookup) then Theordersupplieridlookup = ""
  Theorderordertypeidlookup = rs("orderordertypeidlookup") : if isnull(Theorderordertypeidlookup) then Theorderordertypeidlookup = ""
  Theordercourseidlookup = rs("ordercourseidlookup") : if isnull(Theordercourseidlookup) then Theordercourseidlookup = ""
  Theorderdriveridlookup = rs("orderdriveridlookup") : if isnull(Theorderdriveridlookup) then Theorderdriveridlookup = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    t = "" : if isdate(thedate1) then if year(thedate1) <> 1900 then t = thedate1
    if oksee("date1") then csv(rc) = csv(rc) & t & "^"
    if oksee("status") then csv(rc) = csv(rc) & replace(thestatus,vbcrlf," ") & "^"
    if oksee("userid") then csv(rc) = csv(rc) & theuseridlookup & "^"
    if oksee("severity") then csv(rc) = csv(rc) & replace(theseverity,vbcrlf," ") & "^"
    if oksee("orderid") then csv(rc) = csv(rc) & replace(theorderid,vbcrlf," ") & "^"
    if oksee("ordersupplierid") then csv(rc) = csv(rc) & theordersupplieridlookup & "^"
    t = "" : if isdate(theorderdate1) then if year(theorderdate1) <> 1900 then t = theorderdate1
    if oksee("orderdate1") then csv(rc) = csv(rc) & t & "^"
    if oksee("orderordertypeid") then csv(rc) = csv(rc) & theorderordertypeidlookup & "^"
    if oksee("ordercourseid") then csv(rc) = csv(rc) & theordercourseidlookup & "^"
    if oksee("orderdriverid") then csv(rc) = csv(rc) & theorderdriveridlookup & "^"
      t = "" : d = formatidlist(thepids)
      sq = "select * from workers where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("firstname")
        t = t & " " & rs1("lastname")
        t = t & " " & rs1("code")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    if oksee("pids") then csv(rc) = csv(rc) & t & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("description") then csv(rc) = csv(rc) & replace(thedescription,vbcrlf," ") & "^"
    if oksee("supplierremarks") then csv(rc) = csv(rc) & replace(thesupplierremarks,vbcrlf," ") & "^"
    if oksee("solution") then csv(rc) = csv(rc) & replace(thesolution,vbcrlf," ") & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'['-------------more actions-------------------------------------
if request_action = "cancel" and okwrite(tablename, request_id) then
  s = "update complains set status = 'Canceled' where id = " & request_id
  set rs = conn.execute(sqlfilter(s))
end if


']
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","complains.id,complains.date1,complains.status,complains.userid,complains.severity,complains.orderid,complains.ordersupplierid,complains.orderdate1,complains.orderordertypeid,complains.ordercourseid,complains.orderdriverid,complains.pids,complains.title,complains.description,complains.supplierremarks,complains.solution,complains.siteid",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Thestatus = rs("status") : if isnull(Thestatus) then Thestatus = ""
  Theuserid = rs("userid") : if isnull(Theuserid) then Theuserid = 0
  Theuserid = replace(theuserid,",",".")
  Theseverity = rs("severity") : if isnull(Theseverity) then Theseverity = ""
  Theorderid = rs("orderid") : if isnull(Theorderid) then Theorderid = 0
  Theorderid = replace(theorderid,",",".") : Theorderid = formatnumber(theorderid,0,true,false,false)
  Theordersupplierid = rs("ordersupplierid") : if isnull(Theordersupplierid) then Theordersupplierid = 0
  Theordersupplierid = replace(theordersupplierid,",",".")
  Theorderdate1 = rs("orderdate1") : if isnull(Theorderdate1) then Theorderdate1 = "1/1/1900"
  Theorderdate1 = replace(theorderdate1,".","/")
  Theorderordertypeid = rs("orderordertypeid") : if isnull(Theorderordertypeid) then Theorderordertypeid = 0
  Theorderordertypeid = replace(theorderordertypeid,",",".")
  Theordercourseid = rs("ordercourseid") : if isnull(Theordercourseid) then Theordercourseid = 0
  Theordercourseid = replace(theordercourseid,",",".")
  Theorderdriverid = rs("orderdriverid") : if isnull(Theorderdriverid) then Theorderdriverid = 0
  Theorderdriverid = replace(theorderdriverid,",",".")
  Thepids = rs("pids") : Thepids = formatidlist(Thepids)
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
  Thedescription = replace(thedescription,"<br>",vbcrlf)
  Thesupplierremarks = rs("supplierremarks") : if isnull(Thesupplierremarks) then Thesupplierremarks = ""
  Thesupplierremarks = replace(thesupplierremarks,"<br>",vbcrlf)
  Thesolution = rs("solution") : if isnull(Thesolution) then Thesolution = ""
  Thesolution = replace(thesolution,"<br>",vbcrlf)
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  formcontrols = "date1,date1_day,date1_month,date1_year,date1_hour,date1_insertdate,date1_removedate,status,userid,severity,orderid,ordersupplierid,orderdate1,orderdate1_day,orderdate1_month,orderdate1_year,orderdate1_hour,orderdate1_insertdate,orderdate1_removedate,orderordertypeid,ordercourseid,orderdriverid,pids,pids_select,pids_trigger,title,description,supplierremarks,solution,siteid,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
'[  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  if instr(",supplier,supplieradmin,", "," & session("usergroup") & ",") > 0 and cstr(theorderid) <> "0" then disablecontrols = disablecontrols & "orderid,"

  '----history tab
  if specs = "rail" then
    if true then'not childpage and getfield("printversion") <> "on" then
      rr("<div class=subtablinks>")
      t = "000000" : if request_action2 <> "showchildpage" then t = "bb0000"
      rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "> <span style=" & t & "> ~" & tablename & "_" & tablename & "~ " & request_id & "</font></a>")
      t = "000000" : if getfield("childpage") = "history" then t = "bb0000"
      rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=history&containerfield=c" & theid & "> <font color=" & t & "> ~History~ </font></a>")
    end if
    rr("<span id=childchildren></span></div>")
    if getfield("action2") = "showchildpage" then
      t = "admin_" & getfield("childpage") & ".asp?containerfield=" & getfield("containerfield") & "&containerid=" & request_id & "&action=search&action2=clear"
      if getfield("childpage") = "misc" then t = "admin_misc.asp?miscview=" & curl(getfield("miscview")) & "&containerid=0&miscdisplay=" & curl(getfield("miscdisplay")) & "&mischidefields=" & curl(getfield("mischidefields")) & "&misctrans=" & curl(getfield("misctrans"))
      rr("<br><iframe id=fr" & getfield("childpage") & " name=fr" & getfield("childpage") & " style='width:100%; height:1000;' frameborder=0 scrolling=no src=" & t & "></iframe>")
      rrbottom : rend
    end if
  end if

']


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_baloon.gif' ~iconsize~> ~complains_complains~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  rr("<tr valign=top><td class=form_item1><span id=id_caption>~ID~</span><td class=form_item2 height=27>")
  rr("<font style='position:relative; top:3; background:#000000; color:ffffff; font-weight:bold; padding:2;'>" & request_id & "</font>")

  if oksee("date1") then'form:date1
    rr("<tr valign=top id=date1_tr0><td class=form_item1 id=date1_tr1><span id=date1_caption>~complains_date1~<img src=must.gif></span><td class=form_item2 id=date1_tr2>")
    rr(SelectDate("f1.date1*:", thedate1))
  end if'form:date1

  if oksee("status") then'form:status
'[    rr("<tr valign=top id=status_tr0><td class=form_item1 id=status_tr1><span id=status_caption>~complains_status~</span><td class=form_item2 id=status_tr2>")
    rr("<tr valign=top id=status_tr0><td class=form_item1 id=status_tr1><span id=status_caption>~complains_status~</span><td class=form_item2 id=status_tr2>")
    rr "<input type=hidden name=statuswas value='" & thestatus & "'>"
']
    a = "New,Pending,Closed,Canceled,"
    rr("<select name=status dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Thestatus = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:status
  end if'form:status

'[  if oksee("userid") and session(tablename & "containerfield") <> "userid" then
'[    rr("<tr valign=top id=userid_tr0><td class=form_item1 id=userid_tr1><span id=userid_caption>~complains_userid~</span><td class=form_item2 id=userid_tr2>")
'[    if getfield("box") = "userid" then theuserid = getfield("boxid")
'[    rr("<select name=userid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~complains_userid~")
'[    sq = "select * from buusers order by username"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Theuserid) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("username") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:userid
'[  elseif okedit("userid") then
'[    rr("<input type=hidden name=userid value=" & theuserid & ">")
'[  end if'form:userid
  rr("<input type=hidden name=userid value=" & theuserid & ">")
  if oksee("userid") then
    rr("<tr valign=top id=userid_tr0><td class=form_item1 id=userid_tr1><span id=userid_caption>~complains_userid~</span><td class=form_item2 id=userid_tr2>")
    rr "<span style='width:300px; height:20px; padding:2px; border:1px solid #cccccc; background:#f5f5f5;'>" & getuserorworker(theuserid) & "</span>"
  end if
']

  if oksee("severity") then'form:severity
    rr("<tr valign=top id=severity_tr0><td class=form_item1 id=severity_tr1><span id=severity_caption>~complains_severity~</span><td class=form_item2 id=severity_tr2>")
    a = "1,2,3,4,5,"
    rr("<select name=severity dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Theseverity = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:severity
  end if'form:severity

  if oksee("orderid") then'form:orderid
    rr("<tr valign=top id=orderid_tr0><td class=form_item1 id=orderid_tr1><span id=orderid_caption>~complains_orderid~<img src=must.gif></span><td class=form_item2 id=orderid_tr2>")
'[    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=orderid value=""" & Theorderid & """>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=orderid value=""" & Theorderid & """>")
    if okedit("orderid") then rr " <input type=button id=buttonloadorderdata value='~Load order data~' onclick='vbscript: f1.action.value = ""update"" : f1.action2.value = ""stay"" : f1.action3.value = ""loadorderdata"" : checkform'>"
']
  end if'form:orderid

  if oksee("ordersupplierid") and session(tablename & "containerfield") <> "ordersupplierid" then
    rr("<tr valign=top id=ordersupplierid_tr0><td class=form_item1 id=ordersupplierid_tr1><span id=ordersupplierid_caption>~complains_ordersupplierid~</span><td class=form_item2 id=ordersupplierid_tr2>")
    if getfield("box") = "ordersupplierid" then theordersupplierid = getfield("boxid")
    rr("<select name=ordersupplierid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~complains_ordersupplierid~")
    sq = "select * from suppliers order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theordersupplierid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:ordersupplierid
  elseif okedit("ordersupplierid") then
    rr("<input type=hidden name=ordersupplierid value=" & theordersupplierid & ">")
  end if'form:ordersupplierid

  if oksee("orderdate1") then'form:orderdate1
    rr("<tr valign=top id=orderdate1_tr0><td class=form_item1 id=orderdate1_tr1><span id=orderdate1_caption>~complains_orderdate1~</span><td class=form_item2 id=orderdate1_tr2>")
    rr(SelectDate("f1.orderdate1:", theorderdate1))
  end if'form:orderdate1

  if oksee("orderordertypeid") and session(tablename & "containerfield") <> "orderordertypeid" then
    rr("<tr valign=top id=orderordertypeid_tr0><td class=form_item1 id=orderordertypeid_tr1><span id=orderordertypeid_caption>~complains_orderordertypeid~</span><td class=form_item2 id=orderordertypeid_tr2>")
    if getfield("box") = "orderordertypeid" then theorderordertypeid = getfield("boxid")
    rr("<select name=orderordertypeid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~complains_orderordertypeid~")
    sq = "select * from ordertypes order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theorderordertypeid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:orderordertypeid
  elseif okedit("orderordertypeid") then
    rr("<input type=hidden name=orderordertypeid value=" & theorderordertypeid & ">")
  end if'form:orderordertypeid

  if oksee("ordercourseid") and session(tablename & "containerfield") <> "ordercourseid" then
    rr("<tr valign=top id=ordercourseid_tr0><td class=form_item1 id=ordercourseid_tr1><span id=ordercourseid_caption>~complains_ordercourseid~</span><td class=form_item2 id=ordercourseid_tr2>")
    if getfield("box") = "ordercourseid" then theordercourseid = getfield("boxid")
    rr("<select name=ordercourseid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~complains_ordercourseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theordercourseid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:ordercourseid
  elseif okedit("ordercourseid") then
    rr("<input type=hidden name=ordercourseid value=" & theordercourseid & ">")
  end if'form:ordercourseid

  if oksee("orderdriverid") and session(tablename & "containerfield") <> "orderdriverid" then
    rr("<tr valign=top id=orderdriverid_tr0><td class=form_item1 id=orderdriverid_tr1><span id=orderdriverid_caption>~complains_orderdriverid~</span><td class=form_item2 id=orderdriverid_tr2>")
    if getfield("box") = "orderdriverid" then theorderdriverid = getfield("boxid")
    rr("<select name=orderdriverid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~complains_orderdriverid~")
    sq = "select * from drivers order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theorderdriverid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:orderdriverid
  elseif okedit("orderdriverid") then
    rr("<input type=hidden name=orderdriverid value=" & theorderdriverid & ">")
  end if'form:orderdriverid

  if oksee("pids") then'form:pids
    rr("<tr valign=top id=pids_tr0><td class=form_item1 id=pids_tr1><span id=pids_caption>~complains_pids~</span><td class=form_item2 id=pids_tr2>")
'[    rr(selectlookupmultiauto("f1.pids",thepids, "icons/suppliers.png", "workers,firstname,lastname,code,", 300, okwritehere))
    if instr(",,rail,teva,iai,leumi,rotem,icl,jerusalem,muni,brener,yoav,rafael,iscar,dsw,", "," & specs & ",") > 0 then
      t = "select top 30 id,firstname,lastname,code,phone1 from workers"
      rr selectsearch("f1.pids", thepids, "firstname,lastname,code,", "workers", t)
    else
      rr(selectlookupmultiauto("f1.pids",thepids, "icons/suppliers.png", "workers,firstname,lastname,code,", 300, okwritehere))
    end if
']
  end if'form:pids

  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~complains_title~</span><td class=form_item2 id=title_tr2>")
'[    rr("<input type=text maxlength=100 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
    'if specs = "rail" then
      rr "<select name=title><option value=''>"
      s = "select title from misc where type1 = 'complain_subjects' order by title"
      set rs1 = conn.execute(sqlfilter(s))
      found = 0
      do until rs1.eof
        t = rs1("title") : t = replace(t,"'","`")
        rr "<option value='" & t & "'" : if thetitle = t then rr " selected" : found = 1
        rr ">" & t
        rs1.movenext
      loop
      if found = 0 then rr "<option value='" & replace(thetitle, "'", "`") & "' selected>" & thetitle
      rr "</select>"
    'else
    '  rr("<input type=text maxlength=100 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
    'end if
']
  end if'form:title

  if oksee("description") then'form:description
    rr("<tr valign=top id=description_tr0><td class=form_item1 id=description_tr1><span id=description_caption>~complains_description~</span><td class=form_item2 id=description_tr2>")
    rr("<textarea name=description style='width:301; height:100;' dir=~langdir~>" & thedescription & "</textarea>")
  end if'form:description

  if oksee("supplierremarks") then'form:supplierremarks
    rr("<tr valign=top id=supplierremarks_tr0><td class=form_item1 id=supplierremarks_tr1><span id=supplierremarks_caption>~complains_supplierremarks~</span><td class=form_item2 id=supplierremarks_tr2>")
    rr("<textarea name=supplierremarks style='width:301; height:100;' dir=~langdir~>" & thesupplierremarks & "</textarea>")
  end if'form:supplierremarks

  if oksee("solution") then'form:solution
    rr("<tr valign=top id=solution_tr0><td class=form_item1 id=solution_tr1><span id=solution_caption>~complains_solution~</span><td class=form_item2 id=solution_tr2>")
'[    rr("<textarea name=solution style='width:301; height:100;' dir=~langdir~>" & thesolution & "</textarea>")
    'if specs = "rail" then
      if okedit("solution") then
        rr "<select name=solution_select onchange='vbscript: putsolution(me.value)' style='width:300;'><option value='' style='color:#bbbbff;'>~complains_solution~"
        s = "select id,title,contents from misc where type1 = 'complain_solutions' order by title"
        set rs1 = conn.execute(sqlfilter(s))
        aaa = ""
        do until rs1.eof
          t = rs1("contents") : t = ctxt(t) : t = replace(t,"'","`") : t = replace(t,"""","``")
          rr "<option value=" & rs1("id") & ">" & rs1("title")
          aaa = aaa & "if cstr(a) = cstr(" & rs1("id") & ") then f1.solution.value = replace(""" & t & """, ""<br>"", vbcrlf)" & vbcrlf
          rs1.movenext
        loop
        rr "</select><br>"
      end if
      rr "<script language=vbscript>" & vbcrlf
      rr "sub putsolution(byval a)" & vbcrlf
      rr aaa
      rr "  if trim(f1.solution.value) = """" then f1.solution.value = f1.solution_select.options(f1.solution_select.selectedindex).text" & vbcrlf
      rr "end sub" & vbcrlf
      rr "</script>" & vbcrlf
    'end if
    rr("<textarea name=solution style='width:301; height:100;' dir=~langdir~>" & thesolution & "</textarea>")
']
  end if'form:solution

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~complains_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~complains_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

'[  '-------------updatebar--------------------
  if request_action2 = "newitem" then rr "<input type=hidden name=newitem value=on>"
']
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
'[  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  if not oksee("buttondelete") then
    t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=cancel&id=" & request_id & "&token=" & session("token") & """"
  end if
']
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("date1") then rr("    if instr(f1.date1.value,""1/1/1900"") = 0 and f1.date1.value <> """" then validfield(""date1"") else invalidfield(""date1"") : ok = false" & vbcrlf)
  if okedit("orderid") and session(tablename & "containerfield") <> "orderid" then
    rr("    if isnumeric(f1.orderid.value) then" & vbcrlf)
    rr("      if cdbl(f1.orderid.value) >= 0 and cdbl(f1.orderid.value) <= 9999999 then" & vbcrlf)
    rr("        validfield(""orderid"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""orderid"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""orderid"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.orderid.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date1 desc" : session(tablename & "_hidelistcolumns") = "description,supplierremarks,solution,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date1 desc"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  if getfield("date1from") <> "" then session(tablename & "_date1from") = getfield("date1from")
  if getfield("date1to") <> "" then session(tablename & "_date1to") = getfield("date1to")
  if not isdate(session(tablename & "_date1from")) then session(tablename & "_date1from") = "1/1/1900"
  if not isdate(session(tablename & "_date1to")) then session(tablename & "_date1to") = "1/1/1900"
  if getfield("orderdate1from") <> "" then session(tablename & "_orderdate1from") = getfield("orderdate1from")
  if getfield("orderdate1to") <> "" then session(tablename & "_orderdate1to") = getfield("orderdate1to")
  if not isdate(session(tablename & "_orderdate1from")) then session(tablename & "_orderdate1from") = "1/1/1900"
  if not isdate(session(tablename & "_orderdate1to")) then session(tablename & "_orderdate1to") = "1/1/1900"
  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/i_baloon.gif' ~iconsize~> ~complains_complains~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
'[  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  if session(tablename & "containerid") = "0" and ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
']
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
'[if session(tablename & "_advanced") = "y" then
']
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  rr("<nobr> ~complains_date1~ ~From~ ")
  rr(SelectDate("f2.date1from", session(tablename & "_date1from")))
  rr(" ~To~ ")
  rr(SelectDate("f2.date1to", session(tablename & "_date1to")))
  rr("</nobr>")
'[  rr(" <nobr><img id=status_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
'[  rr(" <select name=status dir=~langdir~ style='width:150;'>")
'[  rr("<option value='' style='color:bbbbfe;'>~complains_status~")
'[  aa = "New,Pending,Closed,Canceled," : a = aa
'[  'sq = "select distinct status from complains order by status"
'[  'set rs1 = conn.execute(sqlfilter(sq))
'[  'do until rs1.eof : a = a & rs1("status") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
'[  do until a = ""
'[    b = left(a,instr(a,",")-1)
'[    a = mid(a,instr(a,",")+1)
'[    rr("<option value=""" & b & """")
'[    if session(tablename & "_status") = b then rr(" selected")
'[    rr(">" & b)
'[  loop
'[  rr("</select></nobr>")'search:status

  rr " <nobr><img id=status_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'> "
  a = "New,Pending,Closed,Canceled,"
  rr multiselect("statuses", "~orders_status~", a, session(tablename & "_statuses"), 115)
  rr " </nobr> &nbsp;&nbsp;&nbsp;"

if session(tablename & "_advanced") = "y" then
']
  if session(tablename & "containerfield") <> "userid" then
'[    rr(" <nobr><img id=userid_filtericon src=icons/hire-me.png style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=userid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~complains_userid~")
'[    sq = "select * from buusers order by username"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_userid")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("username") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:userid
    rr(" <nobr><img id=userid_filtericon src=icons/hire-me.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr " <input type=text name=userid style='width:100px;' value='" & session("complains_userid") & "'>"
']
  end if'search:userid
  rr(" <nobr><img id=severity_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=severity dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~complains_severity~")
  aa = "1,2,3,4,5," : a = aa
  'sq = "select distinct severity from complains order by severity"
  'set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof : a = a & rs1("severity") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if session(tablename & "_severity") = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select></nobr>")'search:severity
  if session(tablename & "containerfield") <> "ordersupplierid" then
    rr(" <nobr><img id=ordersupplierid_filtericon src=icons/i_key.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=ordersupplierid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~complains_ordersupplierid~")
    sq = "select * from suppliers order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_ordersupplierid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:ordersupplierid
  end if'search:ordersupplierid
  rr("<nobr> ~complains_orderdate1~ ~From~ ")
  rr(SelectDate("f2.orderdate1from", session(tablename & "_orderdate1from")))
  rr(" ~To~ ")
  rr(SelectDate("f2.orderdate1to", session(tablename & "_orderdate1to")))
  rr("</nobr>")
  if session(tablename & "containerfield") <> "orderordertypeid" then
    rr(" <nobr><img id=orderordertypeid_filtericon src=icons/order-1.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=orderordertypeid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~complains_orderordertypeid~")
    sq = "select * from ordertypes order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_orderordertypeid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:orderordertypeid
  end if'search:orderordertypeid
  if session(tablename & "containerfield") <> "ordercourseid" then
    rr(" <nobr><img id=ordercourseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=ordercourseid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~complains_ordercourseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_ordercourseid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:ordercourseid
  end if'search:ordercourseid
  if session(tablename & "containerfield") <> "orderdriverid" then
    rr(" <nobr><img id=orderdriverid_filtericon src=icons/special-offer.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=orderdriverid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~complains_orderdriverid~")
    sq = "select * from drivers order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_orderdriverid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:orderdriverid
  end if'search:orderdriverid
'[  rr(" <nobr><img id=pids_filtericon src=icons/suppliers.png style='filter:alpha(opacity=50); margin-top:8;'>")
'[  rr(" <select name=pidsfilter dir=~langdir~>")
'[  rr("<option value='' style='color:bbbbfe;'>~complains_pids~")
'[  sq = "select * from workers order by firstname"
'[  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[  do until rs1.eof
'[    rr("<option value=""" & rs1("id") & """")
'[    if cstr(session(tablename & "_pidsfilter")) = cstr(rs1("id")) then rr(" selected")
'[    rr(">")
'[    rr(rs1("firstname") & " ")
'[    rr(rs1("lastname") & " ")
'[    rr(rs1("code") & " ")
'[    rs1.movenext
'[  loop
'[  rr("</select></nobr>")'search:pidsfilter
  rr " <nobr><img src=icons/suppliers.png style='filter:alpha(opacity=50); margin-top:8;'> ~orders_pids~ "
  rr "<input type=text name=pidsfilter style='width:100;' value=""" & session(tablename & "_pidsfilter") & """></nobr>"
']
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~complains_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
'[  end if'search:siteid
  end if'search:siteid
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"
']
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & buusers_1.username) as useridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join buusers buusers_1 on " & tablename & ".userid = buusers_1.id)"
  s = replace(s, " from ", ", cstr('' & suppliers_1.title) as ordersupplieridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join suppliers suppliers_1 on " & tablename & ".ordersupplierid = suppliers_1.id)"
  s = replace(s, " from ", ", cstr('' & ordertypes_1.title) as orderordertypeidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join ordertypes ordertypes_1 on " & tablename & ".orderordertypeid = ordertypes_1.id)"
  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as ordercourseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".ordercourseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & drivers_1.title) as orderdriveridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join drivers drivers_1 on " & tablename & ".orderdriverid = drivers_1.id)"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

'[  '-sqladd
  if session("usergroup") <> "admin" then s = s & " and complains.status <> 'Canceled'"
  if session(tablename & "_pidsfilter") <> "" then
    w = trim(session(tablename & "_pidsfilter"))
    t = "select top 50 id from workers where (cstr('' & firstname) & ' ' & cstr('' & lastname) & ' ' & cstr('' & code)  like '%" & w & "%')"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
    a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".pids), '," & rs1("id") & ",')" : rs1.movenext : loop
    if a = "" then
      s = s & " and 0=1"
    else
      s = s & " and " & mid(a,3) & " > 0"
    end if
  end if

  if session(tablename & "containerfield") <> "" then session(tablename & "containerfield") = "orderid"
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","complains.id,complains.date1,complains.status,complains.userid,complains.severity,complains.orderid,complains.ordersupplierid,complains.orderdate1,complains.orderordertypeid,complains.ordercourseid,complains.orderdriverid,complains.pids,complains.title,complains.description,complains.supplierremarks,complains.solution,complains.siteid",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if year(session(tablename & "_date1from")) > 1900 then s = s & " and " & tablename & ".date1 >= " & sqldate(session(tablename & "_date1from") & " 00:00:00")
  if year(session(tablename & "_date1to")) > 1900 then s = s & " and " & tablename & ".date1 <= " & sqldate(session(tablename & "_date1to") & " 23:59:59")
'[  if session(tablename & "_status") <> "" then s = s & " and lcase(" & tablename & ".status) = '" & lcase(session(tablename & "_status")) & "'"
  s = s & " and complains.status <> ''"
  a = session(tablename & "_statuses")
  if a <> "" then
    add = ""
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      add = add & " or complains.status = '" & b & "'"
    loop
    if add <> "" then s = s & " and (" & mid(add,5) & ")"
  end if

']

'[  if session(tablename & "_userid") <> "" and session(tablename & "containerfield") <> "userid" then s = s & " and " & tablename & ".userid = " & session(tablename & "_userid")
  a = "" : w = session(tablename & "_userid")
  if w <> "" then
    sq = "select top 50 id from workers where 0=0"
    if session("usersiteid") <> "0" then sq = sq & " and siteid = " & session("usersiteid")
    sq = sq & " and (cstr('' & firstname) & ' ' & cstr('' & lastname) & ' ' & cstr('' & code) like '%" & w & "%')"
    a = a & getidlist(sq) : if a <> "" then a = "-" & replace(a, ",", ",-") : a = left(a, len(a) - 1)
    sq = "select top 50 id from buusers where 0=0"
    if session("usersiteid") <> "0" then sq = sq & " and siteid = " & session("usersiteid")
    sq = sq & " and (cstr('' & firstname) & ' ' & cstr('' & lastname) & ' ' & cstr('' & username) like '%" & w & "%')"
    a = a & getidlist(sq)
    if a = "" then a = "0,"
    s = s & " and userid in(" & left(a, len(a)-1) & ")"
  end if
']
  if session(tablename & "_severity") <> "" then s = s & " and lcase(" & tablename & ".severity) = '" & lcase(session(tablename & "_severity")) & "'"
  if session(tablename & "_ordersupplierid") <> "" and session(tablename & "containerfield") <> "ordersupplierid" then s = s & " and " & tablename & ".ordersupplierid = " & session(tablename & "_ordersupplierid")
  if year(session(tablename & "_orderdate1from")) > 1900 then s = s & " and " & tablename & ".orderdate1 >= " & sqldate(session(tablename & "_orderdate1from") & " 00:00:00")
  if year(session(tablename & "_orderdate1to")) > 1900 then s = s & " and " & tablename & ".orderdate1 <= " & sqldate(session(tablename & "_orderdate1to") & " 23:59:59")
  if session(tablename & "_orderordertypeid") <> "" and session(tablename & "containerfield") <> "orderordertypeid" then s = s & " and " & tablename & ".orderordertypeid = " & session(tablename & "_orderordertypeid")
  if session(tablename & "_ordercourseid") <> "" and session(tablename & "containerfield") <> "ordercourseid" then s = s & " and " & tablename & ".ordercourseid = " & session(tablename & "_ordercourseid")
  if session(tablename & "_orderdriverid") <> "" and session(tablename & "containerfield") <> "orderdriverid" then s = s & " and " & tablename & ".orderdriverid = " & session(tablename & "_orderdriverid")
'[  if session(tablename & "_pidsfilter") <> "" then s = s & " and ',' & cstr('' & " & tablename & ".pids) like '%," & session(tablename & "_pidsfilter") & ",%'"
']
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".status like '%" & w & "%' or"
        s = s & " cstr('' & buusers_1.username) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".severity like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".orderid = " & w & " or"
        s = s & " cstr('' & suppliers_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & ordertypes_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & drivers_1.title) & ' ' like '%" & w & "%' or"
        t = "select top 50 id from workers where (cstr('' & firstname) & ' ' & cstr('' & lastname) & ' ' & cstr('' & code)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".pids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".description like '%" & w & "%' or"
        s = s & " " & tablename & ".supplierremarks like '%" & w & "%' or"
        s = s & " " & tablename & ".solution like '%" & w & "%' or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  f = tablename & ".id" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperator1=on&groupseperator=no>~ID~</a> " & a2)
  if oksee("date1") then f = "complains.date1" : f2 = "date1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("date1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_date1~</a> " & a2)
  if oksee("status") then f = "complains.status" : f2 = "status" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("status") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_status~</a> " & a2)
  if oksee("userid") and session(tablename & "containerfield") <> "userid" then
    f = "buusers_1.username" : f2 = "useridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_userid~</a> " & a2)
  end if
  if oksee("severity") then f = "complains.severity" : f2 = "severity" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("severity") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_severity~</a> " & a2)
  if oksee("orderid") then f = "complains.orderid" : f2 = "orderid" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("orderid") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_orderid~</a> " & a2)
  if oksee("ordersupplierid") and session(tablename & "containerfield") <> "ordersupplierid" then
    f = "suppliers_1.title" : f2 = "ordersupplieridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_ordersupplierid~</a> " & a2)
  end if
  if oksee("orderdate1") then f = "complains.orderdate1" : f2 = "orderdate1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("orderdate1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_orderdate1~</a> " & a2)
  if oksee("orderordertypeid") and session(tablename & "containerfield") <> "orderordertypeid" then
    f = "ordertypes_1.title" : f2 = "orderordertypeidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_orderordertypeid~</a> " & a2)
  end if
  if oksee("ordercourseid") and session(tablename & "containerfield") <> "ordercourseid" then
    f = "courses_1.code" : f2 = "ordercourseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_ordercourseid~</a> " & a2)
  end if
  if oksee("orderdriverid") and session(tablename & "containerfield") <> "orderdriverid" then
    f = "drivers_1.title" : f2 = "orderdriveridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_orderdriverid~</a> " & a2)
  end if
  if oksee("pids") then rr("<td class=list_title><nobr>~complains_pids~")
  if oksee("title") then f = "complains.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_title~</a> " & a2)
  if oksee("description") then rr("<td class=list_title><nobr>~complains_description~")
  if oksee("supplierremarks") then rr("<td class=list_title><nobr>~complains_supplierremarks~")
  if oksee("solution") then rr("<td class=list_title><nobr>~complains_solution~")
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~complains_siteid~</a> " & a2)
  end if

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
      Thedate1 = replace(thedate1,".","/")
      Thestatus = rs("status") : if isnull(Thestatus) then Thestatus = ""
      Theuserid = rs("userid") : if isnull(Theuserid) then Theuserid = 0
      Theuserid = replace(theuserid,",",".")
      Theseverity = rs("severity") : if isnull(Theseverity) then Theseverity = ""
      Theorderid = rs("orderid") : if isnull(Theorderid) then Theorderid = 0
      Theorderid = replace(theorderid,",",".") : Theorderid = formatnumber(theorderid,0,true,false,false)
      Theordersupplierid = rs("ordersupplierid") : if isnull(Theordersupplierid) then Theordersupplierid = 0
      Theordersupplierid = replace(theordersupplierid,",",".")
      Theorderdate1 = rs("orderdate1") : if isnull(Theorderdate1) then Theorderdate1 = "1/1/1900"
      Theorderdate1 = replace(theorderdate1,".","/")
      Theorderordertypeid = rs("orderordertypeid") : if isnull(Theorderordertypeid) then Theorderordertypeid = 0
      Theorderordertypeid = replace(theorderordertypeid,",",".")
      Theordercourseid = rs("ordercourseid") : if isnull(Theordercourseid) then Theordercourseid = 0
      Theordercourseid = replace(theordercourseid,",",".")
      Theorderdriverid = rs("orderdriverid") : if isnull(Theorderdriverid) then Theorderdriverid = 0
      Theorderdriverid = replace(theorderdriverid,",",".")
      Thepids = rs("pids") : Thepids = formatidlist(Thepids)
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
      Thesupplierremarks = rs("supplierremarks") : if isnull(Thesupplierremarks) then Thesupplierremarks = ""
      Thesolution = rs("solution") : if isnull(Thesolution) then Thesolution = ""
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Theuseridlookup = rs("useridlookup") : if isnull(Theuseridlookup) then Theuseridlookup = ""
      Theordersupplieridlookup = rs("ordersupplieridlookup") : if isnull(Theordersupplieridlookup) then Theordersupplieridlookup = ""
      Theorderordertypeidlookup = rs("orderordertypeidlookup") : if isnull(Theorderordertypeidlookup) then Theorderordertypeidlookup = ""
      Theordercourseidlookup = rs("ordercourseidlookup") : if isnull(Theordercourseidlookup) then Theordercourseidlookup = ""
      Theorderdriveridlookup = rs("orderdriveridlookup") : if isnull(Theorderdriveridlookup) then Theorderdriveridlookup = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid
      if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = theid
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/i_baloon.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("date1") then rr("<td class=list_item valign=top align=><nobr>") : if year(thedate1) <> 1900 then rr(thedate1)
      if oksee("status") then rr("<td class=list_item valign=top align=><nobr>" & thestatus)
'[      if oksee("userid") and session(tablename & "containerfield") <> "userid" then rr("<td class=list_item valign=top align=><nobr>" & theuseridlookup)
      if oksee("userid") and session(tablename & "containerfield") <> "userid" then rr("<td class=list_item valign=top align=><nobr>" & getuserorworker(theuserid))
']
      if oksee("severity") then rr("<td class=list_item valign=top align=><nobr>" & theseverity)
'[      if oksee("orderid") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(theorderid,0,true,false,true))
      if oksee("orderid") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & theorderid)
']
      if oksee("ordersupplierid") and session(tablename & "containerfield") <> "ordersupplierid" then rr("<td class=list_item valign=top align=><nobr>" & theordersupplieridlookup)
      if oksee("orderdate1") then rr("<td class=list_item valign=top align=><nobr>") : if year(theorderdate1) <> 1900 then rr(theorderdate1)
      if oksee("orderordertypeid") and session(tablename & "containerfield") <> "orderordertypeid" then rr("<td class=list_item valign=top align=><nobr>" & theorderordertypeidlookup)
      if oksee("ordercourseid") and session(tablename & "containerfield") <> "ordercourseid" then rr("<td class=list_item valign=top align=><nobr>" & theordercourseidlookup)
      if oksee("orderdriverid") and session(tablename & "containerfield") <> "orderdriverid" then rr("<td class=list_item valign=top align=><nobr>" & theorderdriveridlookup)
      t = "" : d = formatidlist(thepids)
      sq = "select * from workers where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("firstname")
        t = t & " " & rs1("lastname")
        t = t & " " & rs1("code")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("pids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      if oksee("title") then rr("<td class=list_item valign=top align=><nobr>" & thetitle)
      thedescription = thedescription : if isnull(thedescription) then thedescription = ""
      thedescription = replace(thedescription,"<br>"," ") : thedescription = replace(thedescription,"<","[") : thedescription = replace(thedescription,">","]") : if len(thedescription) > 30 then thedescription = left(thedescription,30) & "..."
      if oksee("description") then rr("<td class=list_item valign=top align=><nobr>" & thedescription)
      thesupplierremarks = thesupplierremarks : if isnull(thesupplierremarks) then thesupplierremarks = ""
      thesupplierremarks = replace(thesupplierremarks,"<br>"," ") : thesupplierremarks = replace(thesupplierremarks,"<","[") : thesupplierremarks = replace(thesupplierremarks,">","]") : if len(thesupplierremarks) > 30 then thesupplierremarks = left(thesupplierremarks,30) & "..."
      if oksee("supplierremarks") then rr("<td class=list_item valign=top align=><nobr>" & thesupplierremarks)
      thesolution = thesolution : if isnull(thesolution) then thesolution = ""
      thesolution = replace(thesolution,"<br>"," ") : thesolution = replace(thesolution,"<","[") : thesolution = replace(thesolution,">","]") : if len(thesolution) > 30 then thesolution = left(thesolution,30) & "..."
      if oksee("solution") then rr("<td class=list_item valign=top align=><nobr>" & thesolution)
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  rr("<td class=list_total1>") 'id
  if oksee("status") and session(tablename & "containerfield") <> "status" then rr("<td class=list_total1>") 'status
  if oksee("userid") and session(tablename & "containerfield") <> "userid" then rr("<td class=list_total1>") 'userid
  if oksee("severity") and session(tablename & "containerfield") <> "severity" then rr("<td class=list_total1>") 'severity
  if oksee("orderid") and session(tablename & "containerfield") <> "orderid" then rr("<td class=list_total1>") 'orderid
  if oksee("ordersupplierid") and session(tablename & "containerfield") <> "ordersupplierid" then rr("<td class=list_total1>") 'ordersupplierid
  if oksee("orderdate1") and session(tablename & "containerfield") <> "orderdate1" then rr("<td class=list_total1>") 'orderdate1
  if oksee("orderordertypeid") and session(tablename & "containerfield") <> "orderordertypeid" then rr("<td class=list_total1>") 'orderordertypeid
  if oksee("ordercourseid") and session(tablename & "containerfield") <> "ordercourseid" then rr("<td class=list_total1>") 'ordercourseid
  if oksee("orderdriverid") and session(tablename & "containerfield") <> "orderdriverid" then rr("<td class=list_total1>") 'orderdriverid
  if oksee("pids") and session(tablename & "containerfield") <> "pids" then rr("<td class=list_total1>") 'pids
  if oksee("title") and session(tablename & "containerfield") <> "title" then rr("<td class=list_total1>") 'title
  if oksee("description") and session(tablename & "containerfield") <> "description" then rr("<td class=list_total1>") 'description
  if oksee("supplierremarks") and session(tablename & "containerfield") <> "supplierremarks" then rr("<td class=list_total1>") 'supplierremarks
  if oksee("solution") and session(tablename & "containerfield") <> "solution" then rr("<td class=list_total1>") 'solution
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
