<!--#include file="include.asp"-->
<%
tablename = "setshifts" : session("tablename") = "setshifts"
request_from1 = getfield("from1") : if instr(request_from1, ",") > 0 then request_from1 = getlistitem(request_from1, 1)
request_to1 = getfield("to1") : if instr(request_to1, ",") > 0 then request_to1 = getlistitem(request_to1, 1)

function checkalready(byval w, byval da, byval di)
  dim s, n, p, pp, t, rs, rs1, du
  checkalready = ""
  if specs = "jr" then
    s = "select id,pids,date2 from orders where status not in('Draft','Canceled','Done')"
    s = s & " and direction = '" & di & "'"
    s = s & " and date1 >= " & sqldate(dateadd("h", -6, da))
    s = s & " and date1 <= " & sqldate(dateadd("h", +6, da))
    s = s & " and instr(cstr(',' & pids), '," & w & ",') > 0"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      if normalizedatestring(rs("date2")) = normalizedatestring(da) then
        checkalready = checkalready & rs("id") & ","
      else
        errors = errors & getworkername(w) & " - ~direction" & di & "~ " & da & " - ~REMOVED from previous order~ " & rs("id") & "<br>"
        pp = rs("pids") : pp = nomatch(pp, w)
        if pp = "" then
          du = multicancel(rs("id") & ",")
        else
          s = "update orders set pids = '" & pp & "', pcount = " & countstring(pp, ",") & " where id = " & rs("id")
          set rs1 = conn.execute(sqlfilter(s))
          du = getorderstationcourse(rs("id"))
          du = getrideprice(rs("id"))
        end if
      end if
      rs.movenext
    loop
    exit function
  end if

  if itslocal then exit function
  n = 120 : if specs = "iec" then n = 300
  s = "select id from orders where status not in('Draft','Canceled')"
  s = s & " and date1 >= " & sqldate(dateadd("n", -n, da))
  s = s & " and date1 <= " & sqldate(dateadd("n", +n, da))
  s = s & " and instr(cstr(',' & pids), '," & w & ",') > 0"
  checkalready = getidlist(s)
end function

function checkjoining(byval a) 'iec
  dim t, s, central, city, i, dur, dest, date1, b, orig, ori, dd 'using main scope cm, ii
  checkjoining = a
  if getfield("joining") <> "on" then exit function
  orig = a : ori = replace(a, "|" & cm, "|")

  '---find central bus station
  city = "" : central = ""
  t = gettitle("stations", getval(ori, "fr")) : t = ctxt(t) : if instr(t, ";") > 0 then t = trim(mid(t, instr(t,";")+1)) else if instr(t, ",") > 0 then t = trim(mid(t, instr(t,",")+1))
  if t <> "" then city = getonefield("select id from stations where title = '" & t & "'") & ""
  if t <> "" and city <> "" then central = getonefield("select id from stations where title = '���� ������ " & t & "'") & ""
  if central = "" then
    city = getonefield("select citystationid from workers where id = 0" & getval(ori, "wo")) & ""
    t = gettitle("stations", t) : t = ctxt(t)
    if t <> "" and city <> "" then central = getonefield("select id from stations where title = '���� ������ " & t & "'") & ""
  end if

  '---find if shift-site has from-to courserate for these 2 stations
  if city <> "" and central <> "" then
    t = getval(ori, "dd") : t = replace(t, "\", "|") : t = getval(t, "shiftid")
    t = getonefield("select siteid from shifts where id = 0" & t)
    if t <> "" and t <> "0" then
      s = "select id from courserates where siteid = " & t & " and (from1 = " & city & " and to1 = " & central & " or from1 = " & central & " and to1 = " & city & ")"
      t = getidlist(s)
    end if
  end if

  if central <> "" and cstr(central) <> "0" and t <> "" then
    dest = getval(ori, "to")
    dur = iecteken(central & "," & dest & ",", "A") * 60
    if dur = 0 then t = stationcourse(central & "," & dest & ",", "", 0, "", "", "") : dur = cdbl("0" & getval(t, "legdura"))
    'change first job from home to middle station
    a = setval(a, cm & "to", central)
    date1 = dateadd("s", -dur, getval(ori, "da")) : if getval(ori, "di") = "B" then date1 = dateadd("s", +dur, getval(ori, "da"))
    a = setval(a, cm & "da", normalizedatestring(date1))
    a = setval(a, cm & "a2", getval(session("stationareas"), central))
    set rs1 = conn.execute(sqlfilter("select title,markerx,markery from stations where id = 0" & central))
    if rs1("markery") & "" = "" then du = errorend(getworkername(w) & " - Merging Point Station " & gettitle("stations", to1) & " - ~Missing Geo Location~")
    a = setval(a, cm & "tl", rs1("markery") & ";" & rs1("markerx"))
    t1 = getval(a, cm & "fl") : t2 = getval(a, cm & "tl") : angle = getlatlonangle(left(t1,instr(t1,";")-1), mid(t1,instr(t1,";")+1), left(t2,instr(t2,";")-1), mid(t2,instr(t2,";")+1))
    a = setval(a, cm & "an", fornum(angle))
    'a = setval(a, cm & "ne", tocsv(getval(session("stationneighborhoods"), getval(ori, "fr"))))
    a = setval(a, cm & "ty", "singledest")
    a = setval(a, cm & "fd", getval(ori, "to")) 'final destination

    'add another job - from middle station to work
    cm = cm + 1 : ii = ii & cm & "," : b = replace(orig, "|" & (cm-1), "|" & cm)
    b = setval(b, cm & "da", getval(ori, "da"))
    b = setval(b, cm & "fr", central)
    b = setval(b, cm & "a1", getval(session("stationareas"), central))
    b = setval(b, cm & "fl", getval(a, (cm-1) & "tl"))
    t1 = getval(b, cm & "fl") : t2 = getval(b, cm & "tl") : angle = getlatlonangle(left(t1,instr(t1,";")-1), mid(t1,instr(t1,";")+1), left(t2,instr(t2,";")-1), mid(t2,instr(t2,";")+1))
    b = setval(b, cm & "an", fornum(angle))
    'b = setval(b, cm & "ne", tocsv(getval(session("stationneighborhoods"), central)))
    b = setval(b, cm & "ty", "noble")

    '---replace current job with two jobs
    checkjoining = a & b
  end if
end function

if getfield("action") = "mapworkers" then
  sq = session("setshifts_lastlistsql")
  x = instr(sq, " order by") : if x > 0 then sq = left(sq, x - 1)
  x = instr(sq, " where ") : if x > 0 then sq = "select id from workers" & mid(sq, x)
  if sq = "" then sq = "select id from workers order by firstname,lastname" 'come from schedule
  ii = getidlist(sq)
  x = rred("map.asp?workerids=" & ii)
end if

enableretro = "" : myworkers = ""
if specs = "packer" and instr(",kiki,admin,����.����,����.���,", "," & session("username") & ",") > 0 then enableretro = "on"
if specs = "alliance" and hasmatch(session("username"), "���,���,") then enableretro = "on"
if instr(siteurl, "/tnuvamaadanot/") > 0 and getfield("ways") = "-" then enableretro = "on"
if hasmatch(session("userperm"), "setshiftsretro,") then enableretro = "on"
'if session("username") = "kiki" then enableretro = "on"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
admin_top = true : admin_login = true

%><!--#include file="top.asp"--><%
if session("userlevel") <> "5" and instr("," & session("userperm"), ",setshifts,") = 0 then x = errorend("��� ����� ����� ������")
request_ways = getfield("ways") : x = instr(request_ways, ":") : if x > 0 then xtype = mid(request_ways,x+1) : request_ways = left(request_ways, x-1)

if getfield("action") = "search" or getfield("action") = "shiftsubmit" then
  For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
  For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
  if getfield("action") = "search" then session(tablename & "_page") = 0
end if
if getfield("action") = "clear" then
  For Each sessionitem in Session.Contents
    if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
  Next
end if
if (instr(session("usergroup"), "admin") > 0 or hasmatch(specs, "dexcel,isp,")) and (session("setshifts_firstcome") = "" or getfield("action") = "clear") and session("userunitids") <> "" then session("setshifts_unitids") = session("userunitids")
if getfield("unitids_allchecked") = "on" then session(tablename & "_unitids") = ""

if getfield("shetach_allchecked") = "on" then session(tablename & "_shetach") = ""
if getfield("hativa_allchecked") = "on" then session(tablename & "_hativa") = ""
if getfield("contractor_company_allchecked") = "on" then session(tablename & "_contractor_company") = ""

siteidfilter = "0"
if session("usersiteid") <> "0" then siteidfilter = session("usersiteid")
if session("setshifts_siteid") <> "0" and session("setshifts_siteid") <> "" then siteidfilter = session("setshifts_siteid")
if session("setshifts_siteid2") <> "0" and session("setshifts_siteid2") <> "" then siteidfilter = session("setshifts_siteid2")

'------------fields to hide
a = getparam("SetShiftsHideFields") : a = lcase(a) : a = strfilter(a, "abcdefghijklmnopqrstuvwxyz0123456789,") : a = a & "," : hidecontrols = a

'---session
if getfield("span") <> "" then session("setshifts_span") = getfield("span")
if session("setshifts_span") = "" then session("setshifts_span") = "week"
if getfield("orderby") <> "" then session("setshifts_orderby") = getfield("orderby")
if session("setshifts_orderby") = "" then session("setshifts_orderby") = "abc"
if getfield("showshift") <> "" then session("setshifts_showshift") = getfield("showshift")
if session("setshifts_showshift") = "" then session("setshifts_showshift") = "hours"

'if (session("setshifts_showshift") = "shifts" or hasmatch(specs, "rotem,osem,")) and session("shiftnames") = "" then
if session("shiftnames") = "" then
  s = "select id,title,hour1,hour1b from shifts where 0=0"
  if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
  set rs2 = conn.execute(sqlfilter(s))
  do until rs2.eof
    session("shiftnames") = setval(session("shiftnames"), rs2("id"), rs2("title"))
    session("shifthours") = setval(session("shifthours"), rs2("hour1") & "-" & rs2("hour1b"), rs2("id"))
    rs2.movenext
  loop
end if
if getfield("action2") = "delall" and session("usergroup") <> "admin" then rr "~Access Denied~" : rend

'---------------------------- shemer
if left(request_action,10) = "workerstep" then
  'http://localhost/trans/setshifts.asp?username=1&pass=1&action=workerstepin&workercode=1111
  'http://localhost/trans/setshifts.asp?username=1&pass=1&action=workerstepout&workercode=1111
  rclear
  f = "upload/shemer_stepin.csv"  
  du = addlog(f & "", "����,�����,�����,")
  aa = ""
  s = "select id,data1,active,eligible from workers where code = '" & getfield("workercode") & "'"
  s = s & okreadsql("workers")
  set rs = conn.execute(sqlfilter(s))
  if getfield("workercode") = "" then
    t = "Invalid WorkerCode. " : rr t : aa = aa & t
  elseif rs.eof then
    t = "Worker Not Found. " : rr t : aa = aa & t
  elseif rs("active") <> "on" or rs("eligible") <> "on" then
    t = "Worker not Active or not Eligible. " : rr t : aa = aa & t
  else
    a = ""
    if request_action = "workerstepin" then a = "in"
    if request_action = "workerstepout" then a = "out"
    d = rs("data1") & "" : d = setval(d, "step", a) : d = setval(d, "steptime", now)
    s = "update workers set data1 = '" & d & "' where id = " & rs("id")
    set rs1 = conn.execute(sqlfilter(s))
    rr "OK-" & rs("id") & "-" & a
  end if
  aa = aa & getfield("workercode") & ","
  aa = aa & request_action & ","
  aa = aa & now & ","
  du = addlog(f, aa)
  rend
end if

'----------------------------
if getfield("act") = "popstations" then
  response.clear
  rr("<script language=vbscript>" & vbcrlf)
  for j = 1 to 2
    if j = 1 then box = "from1" : n = "~Worker`s station~"
    if j = 2 then box = "to1" : n = "~Work location~"

    if specs = "iai" and j = 1 then j = 2 '...disabled to1 field
    rr("  parent.document.f1." & box & ".options.length = 0" & vbcrlf)
    rr("  set o = document.CreateElement(""OPTION"")" & vbcrlf)
    rr("  o.Value = ""0""" & vbcrlf)
    rr("  o.Text = """ & n & """" & vbcrlf)
    'rr("  o.selected = true" & vbcrlf)
    rr("  o.style.color = ""#bbbbfe""" & vbcrlf)
    rr("  parent.document.f1." & box & ".add o" & vbcrlf)
    if session("splittingcourses") = "" then session("splittingcourses") = "-1," & getidlist("select id from courses where type1 = 'Splitting'")
    sp = 0 : if instr("," & session("splittingcourses"), "," & getfield("courseid") & ",") > 0 then sp = 1
    if crosssite = "on" and box = "to1" then
      s = "select id,title from stations where id in(" & getval(session("mainstations"),"a") & "-1) order by title"
      if specs = "imi" then
        s = "select id,title from stations where 0=0"
        's = s & " and title like '��� %'"
        if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
        if session("to1stations") = "" then session("to1stations") = getidlist("select distinct to1 from workers")
        s = s & " and id in(" & session("to1stations") & "-1)"
        s = s & " order by title"
      end if
    elseif specs = "metropolin" and box = "to1" then
      s = "select id,title from stations where title like '%����� %' order by title"
    elseif specs = "metropolin" and box = "from1" then
      s = "select id,title from stations where title not like '%����� %' order by title"
    elseif sp = 0 and oksee("courseid") and getfield("courseid") <> "" and getfield("courseid") <> "0" then
      s = "select coursestations.stationid as id, stations.title as title"
      s = s & " from coursestations left join stations on coursestations.stationid = stations.id"
      s = s & " where courseid = " & getfield("courseid")
      s = s & " order by minutes"
    elseif sp = 0 and getfield("shiftid") <> "" and getfield("shiftid") <> "0" then
      s = "select id,title from stations where 0=0"
      if siteidfilter <> "0" then s = s & " and (siteid = 0 or siteid = " & siteidfilter & ")"
      ii = getidlist("select courseids from shifts where id = " & getfield("shiftid"))
      ii = getidlist("select stationid from coursestations where courseid in(" & ii & "-1)")
      if ii <> "" then s = s & " and id in(" & ii & "-1)"
      s = s & " order by title"
    else
      s = "select id,title from stations where 0=0"
      if siteidfilter <> "0" then s = s & " and (siteid = 0 or siteid = " & siteidfilter & ")"
      if getparam("WorkerStationMustBeConnected") = "y" then
        ii = getidlist("select id from courses where active = 'on'")
        ii = getidlist("select stationid from coursestations where courseid in(" & ii & "-1)")
        ii = uniquelist(ii)
        connectedstations = ii
        s = s & " and id in(" & connectedstations & "-1)"
      else
        s = s & " and special = 'on'"
      end if
      s = s & " order by title"
    end if
    if specs = "rafael" then
      t1 = left(s, instrrev(s, " order by") - 1) : t2 = mid(s, instrrev(s, " order by"))
      s = t1 & " and instr(cstr('' & stations.data1), '|workerscreen=on|') > 0 " & t2
    end if
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      rr("  set o = document.CreateElement(""OPTION"")" & vbcrlf)
      rr("  o.Value = """ & rs("id") & """" & vbcrlf)
      t = rs("title") : if isnull(t) then t = ""
      t = replace(t,"""","``") : t = ctxt(t) : t = replace(t,"'","`") : t = replace(t,"""","``")
      rr("  o.Text = """ & t & """" & vbcrlf)
      if getfield(box) = cstr(rs("id")) then rr("  o.selected = true" & vbcrlf)
      rs.movenext
      if rs.eof and getfield("courseid") <> "" and getfield("courseid") <> "0" and box = "to1" then rr("  o.selected = true" & vbcrlf)
      rr("  parent.document.f1." & box & ".add o" & vbcrlf)
    loop
  next
  rr("</script>" & vbcrlf)
  rend
end if

if getfield("act") = "popcourses" then
  response.clear
  rr("<script language=vbscript>" & vbcrlf)
  box = "courseid"
  rr("  parent.document.f1." & box & ".options.length = 0" & vbcrlf)
  rr("  set o = document.CreateElement(""OPTION"")" & vbcrlf)
  rr("  o.Value = ""0""" & vbcrlf)
  rr("  o.Text = ""~Course~""" & vbcrlf)
  rr("  o.selected = true" & vbcrlf)
  rr("  o.style.color = ""#bbbbfe""" & vbcrlf)
  rr("  parent.document.f1." & box & ".add o" & vbcrlf)
  ii = getidlist("select courseids from shifts where id = " & getfield("shiftid"))
  if ii <> "" or specs = "iec" then
    s = "select id,code,title from courses where active = 'on' and id in(" & ii & "-1)"
  else
    s = "select id,code,title from courses where active = 'on'"
    if siteidfilter <> "0" then s = s & " and siteid = " & siteidfilter
    s = s & " order by code, title"
  end if
  set rs = conn.execute(s)
  do until rs.eof
    rr("  set o = document.CreateElement(""OPTION"")" & vbcrlf)
    rr("  o.Value = """ & rs("id") & """" & vbcrlf)
    t = rs("code") & " " & rs("title") : t = replace(t,"""","``") : t = ctxt(t) : t = replace(t,"'","`") : t = replace(t,"""","``")
    rr("  o.Text = """ & t & """" & vbcrlf)
    if getfield(box) = cstr(rs("id")) then rr("  o.selected = true" & vbcrlf)
    rr("  parent.document.f1." & box & ".add o" & vbcrlf)
    rs.movenext
  loop
  rr("</script>" & vbcrlf)
  rend
end if

'----------------------------
if session("setshifts_firstcome") = "" then
  session("setshifts_firstcome") = "y"
  session("setshifts_eligible") = "on"
  if instr(",brom,techjet,osem,packer,caesarstone,lageen,", "," & specs & ",") > 0 then session("setshifts_active") = "on"
  if instr(siteurl, "/tnuvamaadanot/") > 0 or specs = "alliance" then session("setshifts_active") = "on" : session("setshifts_eligible") = ""
  'if specs = "rafael" then session(tablename & "_person_visible") = "on"
  if specs = "imi" then session("setshifts_active") = "on" : session("setshifts_eligible") = ""
  if specs = "phoenicia" then session("setshifts_active") = "on" : session("setshifts_eligible") = ""
  if specs = "lumenis" then session("setshifts_active") = "on" : session("setshifts_eligible") = ""
  if specs = "tara" then session("setshifts_active") = "on" : session("setshifts_eligible") = ""
  if specs = "flextronics" and session("usersiteid") = "13" then session("setshifts_active") = "on" : session("setshifts_eligible") = "on"
  if instr(siteurl, "/flexofakim.moovex.net/") > 0 then session("setshifts_active") = "on" : session("setshifts_eligible") = ""
end if

if getfield("d1") <> "" and getfield("action") <> "clear" then session("setshifts_d1") = getfield("d1")
if getfield("d2") <> "" and getfield("action") <> "clear" then session("setshifts_d2") = getfield("d2")
d1 = session("setshifts_d1") : if d1 = "" then d1 = date
if getfield("d1") = "" and session("setshifts_span") = "week" then
  if specs = "brom" then do until weekday(d1) = 6 : d1 = dateadd("d", -1, d1) : loop
  if instr(siteurl, "/tnuvamilk/") > 0 and session("usersiteid") = "6" then do until weekday(d1) = 7 : d1 = dateadd("d", -1, d1) : loop '���� ����
end if

d2 = session("setshifts_d2") : if d2 = "" then d2 = dateadd("d",6,d1)
if specs = "iec" and cdate(d2) > dateadd("d", 11, date) then d2 = dateadd("d", 11, date)

if session("setshifts_orderby") = "shifthour" then
  if getfield("span") = "week" then
    session("setshifts_orderby") = "abc"
  else
    session("setshifts_span") = "day"
    session("setshifts_d1") = session("setshifts_d2")
    d1 = d2
  end if
  if instr(",brom,imi,", "," & specs & ",") = 0 and session("setshifts_byshift") <> "collect" and session("setshifts_byshift") <> "distribute" then session("setshifts_byshift") = "collect"
end if

d1 = cdate(d1) : d2 = cdate(d2)
if datediff("d",d1,d2) > 31 then x = errorend("_back~Cannot set range for more than a month~")
If getfield("page") <> "" Then session("setshifts_page") = getfield("page")
if isnumeric("-" & session("setshifts_page")) then session("setshifts_page") = cdbl(session("setshifts_page")) else session("setshifts_page") = 0
if not isnumeric("-" & session(tablename & "_lines")) then session(tablename & "_lines") = "30" : if getparam("DefaultSetshiftsLines") <> "" then session(tablename & "_lines") = getparam("DefaultSetshiftsLines")
if cdbl(session(tablename & "_lines")) > 400 then session(tablename & "_lines") = "400"
if cdbl(session(tablename & "_lines")) < 10 then session(tablename & "_lines") = "10"

'http://127.0.0.1/trans/setshifts.asp?action2=ordertaxi&source=produceform&pd1=03/1/2017%2000:10:00&pd2=03/1/2017%2000:20:00&siteids=&ways=B&separatesites=on&onlypids=1451,1621,876,1497,1769,
'----------------------------------
if request_action2 = "ordertaxi" then
  server.scripttimeout = 36000
  production = "" : sqladd = "" : shifthoursa = "" : shifthoursb = ""
  if session("onlypids") <> "" then sqladd = " and workerid in(" & session("onlypids") & "-1)" : deb "&onlypids=" & session("onlypids")
  dim debdata : set debdata = Server.CreateObject("ADODB.Stream") : debdata.open

  rr "<script language=vbscript> progress.style.top = 200 </script>" : response.flush : lastz = 0
  rr "<div style='background:#ffffff; direction:ltr;'>"

  production = getnewproduction
  du = setproduceduntil()

  '---
  if session("stationareas") = "" then
    if specs = "iec" then
      set st = Server.CreateObject("ADODB.Stream") : st.open
      s = "select distinct(from1), citystationid from workers where from1 > 0 and citystationid > 0"
      set rs1 = conn.execute(sqlfilter(s))
      do until rs1.eof
        st.writetext(rs1("from1") & "=" & rs1("citystationid") & "|")
        rs1.movenext
      loop
      st.position = 0 : stationcities = "|" & st.readtext(st.size) : st.close
    end if
    set st1 = Server.CreateObject("ADODB.Stream") : st1.open
    'set st2 = Server.CreateObject("ADODB.Stream") : st2.open
    s = "select id,areaid,data1 from stations where areaid > 0"
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      d = rs1("data1") & ""
      st1.writetext(rs1("id") & "=" & rs1("areaid") & "|")
      't = getval(d, "neighborhood") : if t = "" then t = gettitle("areas", rs1("areaid"))
      't = getonefield("select citystationid from workers where citystationid > 0 and from1 = 0" & rs1("id"))
      't = getval(stationcities, rs1("id"))
      'if t = "" then t = "a" & rs1("areaid")
      'st2.writetext(rs1("id") & "=" & t & "|")
      rs1.movenext
    loop
    st1.position = 0 : session("stationareas") = "|" & st1.readtext(st1.size) : st1.close
    'st2.position = 0 : session("stationneighborhoods") = "|" & st2.readtext(st2.size) : st2.close
    session("allstationids") = getidlist("select id from stations where title <> '' and markerx <> ''")
  end if
  '---
  s = "select workerid,date1,ways,hour1,hour1b,from1,to1,data1,shiftid from workershifts where 0=0"
  if getfield("source") = "produceform" then
    ways = getfield("ways") : if ways = "" then ways = "AB"
    pd1 = getfield("pd1")
    pd2 = getfield("pd2")
    siteids = session("usersiteid") & "," : if siteids = "0," or session("usersiteids") <> "" then siteids = getfield("siteids")
    if specs = "iec" and siteids <> "" then
      s = s & " and shiftid in(select id from shifts where siteid in(" & siteids & "-1))"
    elseif siteids <> "" then
      s = s & " and workerid in(select id from workers where siteid in(" & siteids & "-1))"
    end if
    if getfield("unitids") <> "" then s = s & " and workerid in(select id from workers where unitid in(" & getfield("unitids") & "-1))"
    s = s & " and ways in('A','B','AB')"
    s = s & sqladd
    ww = "select id from workers where lastname <> '_TEMPLATE_'"
    s = s & " and workerid in(" & ww & ")"
    d = pd1 & " " : d = left(d,instr(d," ")-1) : d = dateadd("d", -1, d)
    s = s & " and date1 >= " & sqldate(d)
    d = pd2 & " " : d = left(d,instr(d," ")-1)
    s = s & " and date1 <=" & sqldate(d)
    t = getfield("shiftids") : if t <> "" then s = s & " and shiftid in(" & t & "-1)"

  elseif getfield("source") = "set.asp" then
    ways = "AB"
    pd1 = "1/1/2000 00:00:00"
    pd2 = "1/1/2100 00:00:00"
    siteids = ""
    s = s & " and id in(" & getfield("ids") & "0)"
  end if

  cm = 0 : errors = "" : mode = "workershifts"
  set st = Server.CreateObject("ADODB.Stream") : st.open
  set rs = conn.execute(sqlfilter(s))
  noneligibles = getidlist("select id from workers where eligible <> 'on'")
  do
    if rs.eof and mode = "workershifts" then
      if getfield("source") = "produceform" and datediff("d", pd1, pd2) >= 7 then
        du = showerror("���� ���� ��� ���� - �� ���� ����� �����")
      elseif specs = "jerusalem" then
        du = showerror("�� ���� ����� �����")
      else
        s = "select workerid,day1,hour1,hour1b,ways,from1,to1,shiftid from weekplan where 0=0"
        s = s & sqladd
        if siteids <> "" then s = s & " and workerid in(select id from workers where siteid in(" & siteids & "-1))"
        s = s & " and (year(date1) = 1900 or date1 <= " & sqldate(pd1) & ")"
        s = s & " and (year(date2) = 1900 or date2 >= " & sqldate(pd1) & ")"
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        mode = "weekplan"
      end if
    end if
    if rs.eof then exit do
    e = ""
    w = rs("workerid")
    d = ""
    if mode = "workershifts" then
      d = rs("date1")
    elseif mode = "weekplan" then
      d = ""
      t1 = pd1 : t1 = left(t1 & " ", instr(t1 & " ", " ") -1)
      t2 = pd2 : t2 = left(t2 & " ", instr(t2 & " ", " ") -1)
      do
        if cstr(weekday(t1)) = cstr(rs("day1")) then d = t1 : exit do
        t1 = dateadd("d", 1, t1) : t1 = left(t1 & " ", instr(t1 & " ", " ") -1)
        if cdate(t1) > cdate(t2) then exit do
      loop
    end if
    if specs <> "cocacola" and hasmatch(noneligibles, w) or d = "" then w = 0
    h1 = rs("hour1")
    h2 = rs("hour1b")
    f1 = rs("from1")
    t1 = rs("to1")
    if specs = "brener" then if instr("," & session("allstationids"), "," & f1 & ",") = 0 then f1 = "0"
    if specs = "brener" then if instr("," & session("allstationids"), "," & t1 & ",") = 0 then t1 = "0"

    wa = rs("ways")
    data1 = "" : if mode = "workershifts" then data1 = rs("data1")
    data1 = setval(data1, "shiftid", rs("shiftid"))
    altaddress = getval(data1, "altaddress")
    if mode = "weekplan" and w > 0 then
      s = "select id from workershifts where workerid = 0" & w & " and date1 = " & sqldate(d)
      if specs = "phibro" then s = s & " and ways = 'X'"
      t = getonefield(s)
      if t <> "" then w = 0
    end if
    rs.movenext

    s = "select from1,from2,to1,siteid,data1,workertypeid,citystationid from workers where id = " & w
    set rs0 = conn.execute(sqlfilter(s))
    if not rs0.eof and isdate(d) then
      workerdata1 = rs0("data1")
      date1 = d & " " & h1 : date1b = d & " " & h2 : if cdate(date1) > cdate(date1b) then date1b = dateadd("d", 1, date1b)
      defaultfrom1 = rs0("from1")
      if specs = "barlev" and (instr(wa, "A") > 0 and h1 = "07:00" or instr(wa, "B") > 0 and h2 = "16:30") then defaultfrom1 = rs0("from2") 'not good for two direction shifts. specs=jr has it right in a few lines..
      'if specs = "jr" and (instr(wa, "A") > 0 and h1 = "03:00" or instr(wa, "B") > 0 and h2 = "03:00") then defaultfrom1 = rs0("from2") 'not good for two direction shifts
      from1 = f1 & "" : if from1 = "0" or from1 = "" then from1 = defaultfrom1 & ""

      to1 = t1 & "" : if to1 = "0" or to1 = "" then to1 = rs0("to1") & ""
      if from1 = "0" or to1 = "0" then e = e & getworkername(w) & " - ~no station registered~<br>"

      if e = "" then
        set rs1 = conn.execute(sqlfilter("select title,markerx,markery from stations where id = 0" & from1))
        if rs1.eof then e = e & getworkername(w) & " - ~Station~ - ~Not found~<br>"
        if e = "" then
          if rs1("markery") & "" = "" then
            e = e & getworkername(w) & " - ~Station~ " & gettitle("stations", from1) & " - ~Missing Geo Location~<br>"
          else
            fromtitle = rs1("title") : fromll = formatnumber(rs1("markery"),7,true,false,false) & ";" & formatnumber(rs1("markerx"),7,true,false,false)
          end if
        end if
      end if

      if e = "" then
        set rs1 = conn.execute(sqlfilter("select title,markerx,markery from stations where id = 0" & to1))
        if rs1.eof then e = e & getworkername(w) & " - ~Station~ - ~Not found~<br>"
        if e = "" then
          if rs1("markery") & "" = "" then
            e = e & getworkername(w) & " - ~Station~ " & gettitle("stations", to1) & " - ~Missing Geo Location~<br>"
          else
            totitle = rs1("title") : toll = formatnumber(rs1("markery"),7,true,false,false) & ";" & formatnumber(rs1("markerx"),7,true,false,false)
          end if
        end if
      end if

      if e = "" then
        t1 = fromll : t2 = toll : angle = getlatlonangle(left(t1,instr(t1,";")-1), mid(t1,instr(t1,";")+1), left(t2,instr(t2,";")-1), mid(t2,instr(t2,";")+1))
      end if

      if getval(workerdata1, "allowways") = "A" then wa = replace(wa, "B", "")
      if getval(workerdata1, "allowways") = "B" then wa = replace(wa, "A", "")

      '---round collect date to half hours
      if hasmatch(specs, "royalbeach,sheleg,") then
        t = cdbl(right(date1, 2))
        if t <= 15 then
          date1 = left(date1, len(date1)-2) & "00"
        elseif t >= 16 and t <= 45 then
          date1 = left(date1, len(date1)-2) & "30"
        else
          date1 = left(date1, len(date1)-2) & "00" : date1 = dateadd("h", 1, date1) : date1 = normalizedatestring(date1)
        end if
      end if
      if specs = "kmc" and instr(wa, "A") > 0 then
        date1 = dateadd("n", -30, date1) : date1 = normalizedatestring(date1)
      end if
      a1 = getval(session("stationareas"), from1)
      a2 = getval(session("stationareas"), to1)

      if e = "" then
        if cstr(from1) = cstr(to1) then e = e & getworkername(w) & " - ~From and To are the same~<br>"
      end if

      if getfield("areaids") <> "" and NOT hasmatch(getfield("areaids"), a1) then wa = ""
      if e <> "" then
        errors = errors & e
      elseif crosssite = "on" or (siteids = "" or hasmatch(siteids, rs0("siteid"))) then
        for i = 1 to 2
          if i = 1 then di = "A" : da = date1
          if i = 2 then di = "B" : da = date1b
          if instr(wa, di) > 0 and instr(ways, di) > 0 and cdate(da) >= cdate(pd1) and cdate(da) < cdate(pd2) then
            t = checkalready(w, da, di)
            if t <> "" then
              errors = errors & getworkername(w) & " - ~direction" & di & "~ " & da & " - ~Already on order~ " & t & "<br>"
            else
              a = "|" : cm = cm + 1 : ii = ii & cm & ","
              a = a & cm & "di=" & di & "|"
              a = a & cm & "da=" & normalizedatestring(da) & "|"
              a = a & cm & "wo=" & w & "|"
              a = a & cm & "fr=" & from1 & "|"
              a = a & cm & "to=" & to1 & "|"
              a = a & cm & "fl=" & fromll & "|"
              a = a & cm & "tl=" & toll & "|"
              a = a & cm & "a1=" & a1 & "|"
              a = a & cm & "a2=" & a2 & "|"
              a = a & cm & "an=" & fornum(angle) & "|"
              'a = a & cm & "ft=" & tocsv(fromtitle) & "|"
              'a = a & cm & "tt=" & tocsv(totitle) & "|"
              'a = a & cm & "ad=" & tocsv(altaddress) & "|"
              'if specs = "iec" then a = a & cm & "ne=" & tocsv(getval(session("stationneighborhoods"), from1)) & "|"
              if specs = "iec" then a = a & cm & "ci=" & rs0("citystationid") & "|"
              if crosssite = "on" or hasmatch(specs, "mormar,iec,phibro,") then a = a & cm & "dd=" & tocsv(replace(data1, "|", "\")) & "|"
              'a = a & cm & "wn=" & tocsv(getworkername(w)) & "|"
              a = a & cm & "si=" & rs0("siteid") & "|"
              if hasmatch(specs, "tenmore,merkazny,") then a = a & cm & "wt=" & rs0("workertypeid") & "|"

              '----------from2 special cases, appears also in admin_orders
              x = 0
              if specs = "phibro" then
                if di = "B" then x = 2
              end if
              if specs = "jr" then
                x = 2 : h = getdatehourstring(da) : si = cstr(rs0("siteid"))
                if shifthoursa = "" then shifthoursa = getfieldlist("select hour1 from shifts where instr(ways, 'A') > 0")
                if shifthoursb = "" then shifthoursb = getfieldlist("select hour1b from shifts where instr(ways, 'B') > 0")
                if di = "A" and hasmatch(shifthoursa, h) then x = 1
                if di = "B" and hasmatch(shifthoursb, h) then x = 1
                if x = 2 then a = setval(a, cm & "ot", "special")
                if h >= "02:40" and h <= "03:20" then x = 2
                if si = "4" and di = "B" and h = "23:00" then x = 2'������ 1
                if si = "4" and di = "B" and h = "23:15" then x = 2'������ 1
                if si = "4" and di = "A" and h = "05:50" then x = 2'������ 1
                if si = "5" then x = 1'����
              end if
              if x = 2 then
                v = rs0("from2")
                if cstr(v) <> "" and cstr(v) <> "0" then
                  set rs1 = conn.execute(sqlfilter("select title,markerx,markery from stations where id = 0" & v))
                  if rs1.eof then
                    errors = errors & getworkername(w) & " - Secondary station ID" & v & " Not found<br>"
                  else
                    if rs1("markery") & "" = "" then
                      errors = errors & getworkername(w) & " - Secondary Station ID" & v & " - " & rs1("title") & " - Missing Geo Location<br>"
                    else
                      t1 = rs1("markery") & ";" & rs1("markerx") : t2 = toll : t = getlatlonangle(left(t1,instr(t1,";")-1), mid(t1,instr(t1,";")+1), left(t2,instr(t2,";")-1), mid(t2,instr(t2,";")+1))
                      a = setval(a, cm & "fr", v)
                      a = setval(a, cm & "a1", getval(session("stationareas"), v))
                      a = setval(a, cm & "fl", t1)
                      a = setval(a, cm & "ft", tocsv(rs1("title")))
                      a = setval(a, cm & "an", t)
                    end if
                  end if
                end if
              end if

              '----------
              if hasmatch(specs, "iec,phibro,") then a = checkjoining(a)
              st.writetext(a)
            end if
          end if
        next
      end if
      if getfield("limitjobs") <> "" then if cm >= cdbl(getfield("limitjobs")) then exit do
    end if
  loop

  st.position = 0 : buf = st.readtext(st.size) : st.close
  if buf = "" then rr errors & "<br>" : du = errorend("_back~No Match~")
  bb = buf : bb = replace(mid(bb,2) & "|", "||", "<br>")
  tt = bb : c = 0 : bb = ""
  do until tt = ""
    c = c + 1
    t = left(tt, instr(tt,"<br>")-1) : tt = mid(tt,instr(tt,"<br>")+4)
    w = trim(getval(t, c & "wo")) : t = t & " - " & getworkername(w)
    bb = bb & t & "<br>"
  loop
  bb = replace(bb, "|", "&nbsp;&nbsp;&nbsp;&nbsp;|")
't = replace(t,"<br>", "<tr><td>") : t = replace(t,"|","<td><nobr>")
't = "<table><tr>" & t & "</table>"
  debit siteurl & " ... " & scriptname & "?" & querystring
  debit bb & "<hr>TIMER " & timer - timer0

  '----disabilities
  if instr(siteurl, "/gederatest") > 0 then
    set st = Server.CreateObject("ADODB.Stream") : st.open
    s = "select id, data1 from workers"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      data1 = rs("data1") & ""
       t = ""
       t = t & getval(data1, "disabilityid") & ","
       t = t & getval(data1, "disabilityid2") & ","
       t = t & getval(data1, "disabilityid3") & ","
       t = uniquelist(t)
      if t <> "" and t <> "0," then st.writetext(rs("id") & "=" & t & "|")
      rs.movenext
    loop
    st.position = 0 : workerdisabilities = "|" & st.readtext(st.size) : st.close
    alonedisabilityid = getonefield("select id from misc where type1 = 'disability' and title = '��� ����'") : if alonedisabilityid = "" then alonedisabilityid = "-1"
    liftdisabilityid = getonefield("select id from misc where type1 = 'disability' and title = '�����'") : if liftdisabilityid = "" then liftdisabilityid = "-1"
    taxidisabilityid = getonefield("select id from misc where type1 = 'disability' and title = '�����'") : if taxidisabilityid = "" then taxidisabilityid = "-1"
  end if

  '-----------push to existing rides
  if hasmatch(specs, "metropolin,royalbeach,carlsberg,egged,flextronics,mamillahotel,") then
    jj = ii
    do until jj = ""
      j = left(jj,instr(jj,",")-1) : jj = mid(jj,instr(jj,",")+1)
      di = getval(buf, j & "di") : da = getval(buf, j & "da") : wo = getval(buf, j & "wo")
      t1 = getval(buf, j & "fr") : t2 = getval(buf, j & "to") : if di = "B" then t = t1 : t1 = t2 : t2 = t
      s = "select orders.id, orders.pids, orders.history, orders.altstations, orders.date1, orders.date2 from orders left join cars on orders.carid = cars.id where 0=0"
      s = s & " and status = 'New'"
      if specs = "egged" then s = s & " and production <> 0" & production
      s = s & " and direction = '" & di & "'"
      if di = "A" then
        s = s & " and date2 >= " & sqldate(dateadd("n", -30, da))
        s = s & " and date2 <= " & sqldate(dateadd("n",   0, da))
      else
        s = s & " and date2 >= " & sqldate(dateadd("n",   0, da))
        s = s & " and date2 <= " & sqldate(dateadd("n", +30, da))
      end if
      s = s & " and instr(cstr(',' & pids), '," & wo & ",') = 0" 'for local because no checkalready..
      s = s & " and cars.passengers > orders.pcount"
      s = s & " order by date2"
      set rs = conn.execute(sqlfilter(s))
      do until rs.eof
        his = rs("history") : tt = getval(his, "stationcourse")
        if getlistposition(tt, t2) > getlistposition(tt, t1) and getlistposition(tt, t1) > 0 then
          a = rs("altstations")
          a = setval(a, wo & "from1", getval(buf, j & "fr"))
          a = setval(a, wo & "to1", getval(buf, j & "to"))
          pp = rs("pids") & wo & ","
          s = "update orders set pids = '" & pp & "', pcount = " & countstring(pp, ",") & ", altstations = '" & a & "' where id = " & rs("id")
          set rs1 = conn.execute(sqlfilter(s))
          t = "~Worker~ " & getworkername(wo) & " - " & da & " ~direction" & di & "~ - added to order " & rs("id") : debit t : du = showerror(t)
          ii = nomatch(ii, j) : exit do
        end if
        rs.movenext
      loop
    loop
  end if

  '-----------jj
  rsrc = countstring(ii,",") : orderscount = 0 : alreadyescorted = "" : bannedareas = ""
  maxgap = getval(session("usersitedata1"), "GeoProdMaxShiftGap") : if not isnumeric(maxgap) then maxgap = getparam("GeoProdMaxShiftGap")
  if maxgap = "" then
    maxgap = 30
    if specs = "cocacola" then maxgap = 0
    if specs = "ap" then maxgap = 20 : if weekday(getval(buf, j & "da")) = 7 then maxgap = 30
  end if
  do until ii = ""
    rsc = rsrc - countstring(ii,",") : z = fix(400 * rsc / rsrc) : if z > lastz then rr "<script language=vbscript> progresstext.innerhtml = ""<b>~Processing~..</b> " & rsc & "/" & rsrc & """ : progressbar.style.width = " & z & " </script>" : response.flush : lastz = z
    jj = ii : kk = ""
    do until jj = ""
      j = left(jj,instr(jj,",")-1) : jj = mid(jj,instr(jj,",")+1)
      e = ""
      if kk = "" then
        direction = getval(buf, j & "di")
        times = "" : stids = "" : factories = "" : kkww = ""
      else
        if e = "" then
          tt = times : da = getval(buf, j & "da")
          do until tt = ""
            t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
            if abs(datediff("n", t, da)) > cdbl(maxgap) then e = e & "time gap," : exit do
          loop
        end if

        if e = "" and hasmatch(kkww, getval(buf, j & "wo")) then e = e & "double booking,"
        if e = "" and getval(buf, j & "di") <> direction then e = e & "dif dir,"
        if e = "" and getfield("separatesites") = "on" then
          if getval(buf, getlistitem(kk,1) & "si") <> getval(buf, j & "si") then e = e & "dif site,"
        end if

        if e = "" and specs = "iec" then
          if getval(buf, j & "ty") <> getval(buf, getlistitem(kk,1) & "ty") then
            e = e & "job types mismatch,"
          end if

          if getval(buf, getlistitem(kk,1) & "ty") = "noble" then
            if getval(buf, getlistitem(kk,1) & "fr") <> getval(buf, j & "fr") or getval(buf, getlistitem(kk,1) & "to") <> getval(buf, j & "to") then
              e = e & "no match to noble job,"
            end if
          end if
          if getval(buf, getlistitem(kk,1) & "ty") = "singledest" then
            if getval(buf, getlistitem(kk,1) & "to") <> getval(buf, j & "to") then
              e = e & "no match single destination,"
            end if
          end if
        end if

        if e = "" and specs = "metropolin" then
          z = getdatehourstring(getval(buf, j & "da"))
          if z >= "19:30" and z <= "20:59" then t = strfilter(getparam("StretchEvening"), "0123456789") : if t <> "" then maxgap = cdbl(t)
          if z >= "21:00" then t = strfilter(getparam("StretchNight"), "0123456789") : if t <> "" then maxgap = cdbl(t)
          if e = "" then
            tt = uniquelist(factories & getval(buf, j & "to") & ",")
            if countstring(tt, ",") > 2 then e = e & "3rd factory,"
          end if
        end if

        if e = "" then
          a = getval(session("sitesdata1"), getval(buf, j & "si")) : a = desimp(a)
          sameto = getval(a, "sameto") : if sameto = "" then sameto = getparam("sameto")
          if sameto = "y" then if getval(buf, j & "to") <> getval(buf, getlistitem(kk,1) & "to") then e = e & "dif to,"
        end if

        if e = "" and (instr(siteurl,"/kono.moovex.net/") + instr(siteurl,"/holon.moovex.net/") > 0) then
          'if getval(buf, j & "to") <> getval(buf, getlistitem(kk,1) & "to") then e = e & "dif to,"
          'if getval(buf, j & "a1") <> getval(buf, getlistitem(kk,1) & "a1") then e = e & "dif from area,"
          'if getval(buf, j & "a2") <> getval(buf, getlistitem(kk,1) & "a2") then e = e & "dif to area,"
          tt = uniquelist(factories & getval(buf, j & "to") & ",")
          if countstring(tt, ",") > 3 then e = e & "4th factory,"
        end if

        if e = "" and specs = "iec" then
          'if getval(buf, j & "ci") <> getval(buf, getlistitem(kk,1) & "ci") then e = e & "dif city,"
        end if

        'if e = "" then
        '  maxsta = getparam("GeoProdMaxStations") : if not isnumeric(maxsta) then maxsta = 23
        '  t = uniquelist(stids & getval(buf, j & "fr") & "," & getval(buf, j & "to") & ",")
        '  if countstring(t, ",") > maxsta then e = e & "exceeded stations," : jj = ""
        'end if

        'if NOT hasmatch(specs, "laufer,bontour,ozrot,rail,modiin,clalit,iec,mormar,police,royalbeach,sheleg,barlev,") then
        '  if e = "" and not (specs = "ap" and weekday(getval(buf, j & "da")) = 7) then
        '    x = calcdist(getval(buf, getlistitem(kk,1) & "fl"), getval(buf, j & "fl"))
        '    if x > 10000 then e = e & "far home " & x & ","
        '  end if
        '  if e = "" then
        '    x = calcdist(getval(buf, getlistitem(kk,1) & "tl"), getval(buf, j & "tl"))
        '    if x > 15000 then e = e & "far work " & x & ","
        '  end if
        'end if

      end if

      if e = "" then
        kk = kk & j & ","
        times = uniquelist(times & getval(buf, j & "da") & ",")
        stids = uniquelist(stids & getval(buf, j & "fr") & "," & getval(buf, j & "to") & ",")
        factories = uniquelist(factories & getval(buf, j & "to") & ",")
        kkww = kkww & getval(buf, j & "wo") & ","
      else
        debit "j" & getlistitem(kk,1) & "j" & j & " w" & getval(buf, getlistitem(kk,1) & "wo") & "w" & getval(buf, j & "wo") & " " & e
      end if
    loop

    '-------------kk
    historyadd = ""

    '---needs
    needs = ""
    'if specs = "modiin" then 'escort
    '  tt = kk : needs = ""
    '  do until tt = ""
    '    t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
    '    if needescort(getval(buf, t & "wo") & ",") then needs = needs & t & ","
    '  loop
    'end if

    tt = kk : kkareas = "" : do until tt = "" : t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1) : kkareas = kkareas & getval(buf, t & "a1") & "," : loop

    '---find first
    tt = kk : ww = "" : do until tt = "" : t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1) : ww = ww & getval(buf, t & "wo") & "," : loop 'just for show
    debit "KK (" & countstring(kk, ",") & ") " & kk & "<br>(WW " & ww & ")<br>TIMER " & timer - timer0

    '----find actual farthest from center - slower
    t = 0
    if hasmatch(specs, "iec,egged,mamilla,") then t = 1
    if instr(siteurl, "/gushetzion.moovex.net/") + instr(siteurl, "/gushspecial.moovex.net/") > 0 then t = 1
    if t = 1 then
      tt = kk : yy = "" : froms = "" '--get the geometrically farthest 10 people
      do until countstring(yy, ",") >= 10 or tt = ""
        t = findfarthest(tt, findcenter(tt))
        tt = nomatch(tt, t)
        if NOT hasmatch(froms, getval(buf, t & "fr")) then yy = yy & t & ","
        froms = froms & getval(buf, t & "fr") & ","
      loop
      m = 0
      for x = 1 to countstring(yy,",")
        q = getlistitem(yy, x) : tt = getval(buf, q & "fr") & "," & getval(buf, q & "to") & "," : if direction = "B" then tt = reverselist(tt)
        t = stationcourse(direction & tt , "", 1, now, "", "") : t = getval(t, "legdura") : t = cdbl("0" & t)
        if t >= m then m = t : j = q
      next
    else '---geometrically only
      j = findfarthest(kk, findcenter(kk))
      'if needs <> "" then j = findfarthest(needs, findcenter(needs)) : needs = nomatch(needs,j)
    end if

    kk = nomatch(kk,j) : jj = j & ","
    stationids = getval(buf, j & "fr") & "," & getval(buf, j & "to") & ","
    dest = getval(buf, j & "to")
    maxdis = calcdist(getval(buf, j & "fl"), getval(buf, j & "tl"))
    via1 = ""

    '----parameters
    si  = ""
    if crosssite = "on" then
      t = getval(buf, j & "dd") : t = replace(t, "\", "|") : t = getval(t, "shiftid")
      if t <> "" then si = getonefield("select siteid from shifts where id = " & t)
    end if
    if si = "" then si = getval(buf, j & "si")
    a = getval(session("sitesdata1"), si) : a = desimp(a)
    duratio = getval(a, "PassengerDurationRatio") : if not isnumeric(duratio) then duratio = getparam("PassengerDurationRatio")
    sameareafrom = getval(a, "sameareafrom") : if sameareafrom = "" then sameareafrom = getparam("sameareafrom")
    nearareafrom = getval(a, "nearareafrom") : if nearareafrom = "" then nearareafrom = getparam("nearareafrom")
    sameareato = getval(a, "sameareato") : if sameareato = "" then sameareato = getparam("sameareato")
    nearareato = getval(a, "nearareato") : if nearareato = "" then nearareato = getparam("nearareato")
    maxdu = getval(a, "GeoProdMaxDuration") : if maxdu = "" then maxdu = getparam("GeoProdMaxDuration")
    useratedur = false : if instr(maxdu, "r") > 0 then maxdu = replace(maxdu, "r", "") : useratedur = true
    maxpa = getval(a, "GeoProdMaxPassengers") : if maxpa = "" then maxpa = getparam("GeoProdMaxPassengers")
    minpassengers = getval(a, "MinimumPassengersForProductionOrder") : if minpassengers = "" then minpassengers = getparam("MinimumPassengersForProductionOrder")

    '---mates
    mates = ""
    tt = kk
    do until tt = ""
      t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
      if specs = "bbalev" then
        if getonefield("select citystationid from workers where id = " & getval(buf, j & "wo")) = getonefield("select citystationid from workers where id = " & getval(buf, t & "wo")) then mates = mates & t & ","
      elseif instr(siteurl, "/jerusalem18.moovex.net/") + instr(siteurl, "/gushetzion.moovex.net/") > 0 then
        if getval(buf, j & "a2") = getval(buf, t & "a2") then mates = mates & t & ","
      'elseif specs = "modiin" then
      '  if getval(buf, j & "to") = getval(buf, t & "to") then mates = mates & t & ","
      'elseif specs = "iec" then
      '  if getval(buf, j & "ne") = getval(buf, t & "ne") and getval(buf, j & "ne") <> "" then mates = mates & t & ","
      else
        if getval(buf, j & "a1") = getval(buf, t & "a1") then mates = mates & t & ","
      end if
    loop

    t = "" : if mates <> "" then t = " (Mates " & mates & ")"
    debit "J" & j & " - FIRST area " & gettitle("areas", getval(buf, j & "a1")) & t
    debit "TIMER " & timer - timer0

    if maxpa = "" then
      maxpa = 4
      if specs = "edu" then maxpa = 10
      if specs = "tm" then maxpa = 14
      if specs = "routes" then maxpa = 16
      if specs = "moovex" then maxpa = 50
      if specs = "cocacola" then maxpa = 50
      if hasmatch(specs, "jerusalem,muni,brener,") then maxpa = 50
      if specs = "ozrot" then maxpa = 20
      if hasmatch(specs, "cocacola,laufer,clalit,bontour,babcom,iec,") or instr(siteurl,"/holon.moovex.net/") + instr(siteurl,"/kono.moovex.net/") + instr(siteurl,"/bmw.moovex.net/") > 0 then maxpa = "r"
    end if

    if instr(maxpa, "r") > 0 then
      def = strfilter(maxpa, "0123456789") : maxpa = "" : if def = "" then def = "50"
      if specs = "iec" and maxpa = "" then maxpa = getonefield("select passengers from courserates where active = 'on' and (from1 = 0" & getval(buf, j & "fr") & " and to1 = 0" & getval(buf, j & "to") & " or from1 = 0" & getval(buf, j & "to") & " and to1 = 0" & getval(buf, j & "fr") & ") order by passengers desc")
      'a1 = getval(buf, getlistitem(jj,1) & "a1") : a2 = getval(buf, getlistitem(jj,1) & "a2")
      a1 = getval(buf, j & "a1") : a2 = getval(buf, j & "a2")
      if maxpa = "" then maxpa = getonefield("select passengers from courserates where active = 'on' and (areaid1 = 0" & a1 & " and areaid2 = 0" & a2 & " or areaid1 = 0" & a2 & " and areaid2 = 0" & a1 & ") order by passengers desc")
      if specs = "iec" and maxpa = "" then maxpa = getonefield("select passengers from courserates where active = 'on' and (instr(data1, 'af" & a1 & "af') > 0 and instr(data1, 'at" & a2 & "at') > 0 or instr(data1, 'af" & a2 & "af') > 0 and instr(data1, 'at" & a1 & "at') > 0) order by passengers desc")'areaids1,areaids2
      maxpa = cdbl("0" & strfilter(maxpa, "0123456789")) : if maxpa <= 0 then maxpa = cdbl(def)
    end if

    a1 = getval(buf, j & "a1") : a2 = getval(buf, j & "a2")
    areasfrom = a1 & "," & getonefield("select areaids from areas where id = 0" & a1)
    areasto = a2 & "," & getonefield("select areaids from areas where id = 0" & a2)
    aa1 = a1 & ","
    aa2 = a2 & ","
    if specs = "iec" then
      v = getonefield("select data1 from areas where id = 0" & a1)
      if hasmatch(getval(v, "sitespecificneighbors"), si) then areasfrom = a1 & "," & getval(v, "sitespecificneighbors" & si)
      v = getonefield("select data1 from areas where id = 0" & a2)
      if hasmatch(getval(v, "sitespecificneighbors"), si) then areasto = a2 & "," & getval(v, "sitespecificneighbors" & si)
    end if

    '---throw rest
    exccount = 0
    do until countstring(jj,",") >= cdbl(maxpa) or kk = ""
      exc = 0 : angles = ""
      j = findclosest(kk, getval(buf, getlistitem(jj,1) & "fl"))
      if needs <> "" then
        j = findclosest(needs, getval(buf, getlistitem(jj,1) & "fl")) : needs = nomatch(needs,j)
      elseif mates <> "" then
        j = findclosest(mates, getval(buf, getlistitem(jj,1) & "fl")) : mates = nomatch(mates,j)
      end if
      jda = getval(buf, j & "da")
      ja1 = getval(buf, j & "a1")
      ja2 = getval(buf, j & "a2")
      jfr = getval(buf, j & "fr")
      jto = getval(buf, j & "to")
      jfl = getval(buf, j & "fl")
      jtl = getval(buf, j & "tl")
      jan = getval(buf, j & "an")

      '---exceeded stations
      'if instr(siteurl, "/rachel.moovex.net/") = 0 then
        maxsta = getparam("GeoProdMaxStations") : if not isnumeric(maxsta) then maxsta = 23
        t = uniquelist(stationids & jfr & "," & jto & ",")
        if countstring(t, ",") > cdbl(maxsta) then exc = 1 : debit "J" & j & " - exceeded stations"
      'end if

      '----angles
      x = 1
      if hasmatch(specs, "bontour,ozrot,rail,shufersal,metropolin,barlev,tenmore,iec,") then x = 0
      if instr(siteurl, "/gushetzion.moovex.net/") + instr(siteurl, "/gushspecial.moovex.net/") > 0 then x = 0
      if exc = 0 and x = 1 then
        angle = jan : tt = getval(buf, getlistitem(jj,1) & "an") & "," : 'if specs = "egged" then tt = angles
        dis = 1500
        if specs = "mormar" then dis = 5000
        if hasmatch(specs, "egged,jerusalem,") then dis = 1000
        if specs = "dan" then dis = 500
        if instr(siteurl, "/merkazny.moovex.net/") > 0 then dis = 5000
        x = calcdist(getval(buf, getlistitem(jj,1) & "fl"), jfl) : if x <= dis then tt = ""
        x = calcdist(getval(buf, getlistitem(jj,1) & "tl"), jfl) : if x <= dis then tt = ""
        x = calcdist(jfl, jtl) : if x <= dis then tt = ""
        x = calcdist(getval(buf, getlistitem(jj,1) & "fl"), getval(buf, getlistitem(jj,1) & "tl")) : if x <= dis then tt = ""
        do until tt = ""
          t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
          t = abs(t-angle)
          z = 100
          if specs = "dan" then z = 80
          if specs = "kavim" then z = 80
          if t > z and t < (360-z) then exc = 1 : debit "J" & j & " - dif angle " & t & ","
        loop
        angles = angles & jan & ","
      end if

      ''----maxdu
      'if hasmatch(specs, "airlink_,") then
      '  t1 = getparam("GeoProdMaxDurationRushHour1") : t2 = getparam("GeoProdMaxDurationRushHour2")
      '  if not isnumeric(t1) then t1 = 13
      '  if not isnumeric(t2) then t2 = 17
      '  maxdu = getparam("GeoProdMaxDuration")
      '  maxdulastleg = getparam("GeoProdMaxDurationLastLeg")
      '  t = hour(jda)
      '  if t >= cdbl(t1) and t <= cdbl(t2) then
      '    maxdu = getparam("GeoProdMaxDurationRushHour")
      '    maxdulastleg = getparam("GeoProdMaxDurationLastLegRushHour")
      '  end if
      '  if not isnumeric(maxdu) then maxdu = 50
      '  if not isnumeric(maxdurationlastleg) then maxdurationlastleg = 60
      'end if

      if exc = 0 and hasmatch(specs, "tenmore,merkazny,") then
        tt = jj & j & "," : bb = ""
        do until tt = ""
          t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
          bb = bb & getval(buf, t & "wt") & ","
        loop
        if specs = "tenmore" and hasmatch(bb, "1,2,") and countstring(jj, ",") >= 13 then exc = 1 : debit "J" & j & " - exceeded maxpa for maalon"
        if specs = "merkazny" and hasmatch(bb, "7,") and countstring(jj, ",") >= 4 then exc = 1 : debit "J" & j & " - exceeded maxpa for taxi workers" '�����
      end if

      if exc = 0 and instr(siteurl, "/gederatest") > 0 then
        bb = jj
        do until bb = ""
          b = left(bb,instr(bb,",")-1): bb = mid(bb,instr(bb,",")+1)
          t1 = getval(buf, b & "wo") : t1 = getval(workerdisabilities, t1)
          t2 = getval(buf, j & "wo") : t2 = getval(workerdisabilities, t2)
          tt = t1
          do until tt = ""
            t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
            t = getval(session("disabilityfoes"), t)
            if hasmatch(t, t2) then exc = 1 : debit "J" & j & " - disabilityfoes," : exit do
          loop
          'if hasmatch(t1, alonedisabilityid) then exc = 1 : debit "J" & j & " - i alone"
          'if hasmatch(t2, alonedisabilityid) then exc = 1 : debit "J" & j & " - he alone"
          if exc = 1 then exit do
        loop
      end if

      '---areas
      if exc = 0 then
        'if specs = "ozrot" and instr(jda, " 07:00") > 0 then sameareafrom = "on"
        'if specs = "ozrot" and instr(jda, " 13:00") > 0 then nearareafrom = "on" : nearareato = "on"
        'if specs = "ozrot" and instr(jda, " 15:00") > 0 then sameareato = "on"
        'if specs = "iec" then
        '  t = jto : t = getlav(session("mainstations") ,t)
        '  if hasmatch(getparam("IECSameAreaSites"), t) then sameareafrom = "on"
        'end if

        'if specs = "egged" and session("usersiteid") = "13" and getdatehourstring(jda) <= "12:00" then sameareato = "y"'��� ������� 'tomer ����
        '-------
        'if specs = "bontour" then 'up to 1 neighbor
        '  if areasfrom <> "" and not hasmatch(areasfrom, ja1) then e = e & "dif area," else t = uniquelist(areasfrom & ja1 & ",") : if countstring(t, ",") > 2 then e = e & "3rd area,"
        if exc = 0 and sameareafrom = "y" then
          if ja1 <> getval(buf, getlistitem(jj,1) & "a1") then debit "J" & j & " - dif from area" : exc = 1
          if countstring(tt, ",") > 1 then e = e & "dif from area,"
        elseif exc = 0 and nearareafrom = "y" then
          if instr(siteurl, "/gushetzion.moovex.net/") + instr(siteurl, "/gushspecial.moovex.net/") > 0 then
            t = ja1 & "," & getval(session("areaneighbors"), ja1) : if bothmatch(aa1, t) <> aa1 then debit "J" & j & " - dif from near area" : exc = 1
          else
            if not hasmatch(areasfrom, ja1) then debit "J" & j & " - dif from near area" : exc = 1
          end if
        end if
        if exc = 0 and sameareato = "y" then
          if ja2 <> getval(buf, getlistitem(jj,1) & "a2") then debit "J" & j & " - dif to area" : exc = 1
          if countstring(tt, ",") > 1 then e = e & "dif to area,"
        elseif exc = 0 and nearareato = "y" then
          if instr(siteurl, "/gushetzion.moovex.net/") + instr(siteurl, "/gushspecial.moovex.net/") > 0 then
            t = ja2 & "," & getval(session("areaneighbors"), ja2) : if bothmatch(aa2, t) <> aa2 then debit "J" & j & " - dif to near area" : exc = 1
          else
            if not hasmatch(areasto, ja2) then debit "J" & j & " - dif to near area" : exc = 1
          end if
        end if
      end if

      '---durations
      if exc = 0 and (maxdu <> "" or useratedur or duratio <> "") then
        mm = uniquelist(stationids)
        mm = mm & jfr & "," & jto & ","
        dis1 = calcdist(getval(buf, getlistitem(jj,1) & "fl"), jtl)
        if dis1 > maxdis then dest = jto : mm = nomatch(mm, dest & ",") & dest & ","
        if direction = "B" then mm = reverselist(mm)

        tt = jj & j & "," : t1 = "" : t2 = "" : pids = "" : altstations = ""
        do until tt = ""
          t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
          w = getval(buf, t & "wo") : pids = pids & w & ","
          altstations = setval(altstations, w & "from1", getval(buf, t & "fr"))
          altstations = setval(altstations, w & "to1", getval(buf, t & "to"))
          t1 = t1 & getval(buf, t & "fr") & "," : t2 = t2 & getval(buf, t & "to") & ","
        loop
        mm = stationcourse(direction & mm, "", 1, getval(buf, getlistitem(jj,1) & "da"), pids, altstations)
        legdura1 = cdbl("0" & getval(mm, "legdura"))

        '-------------see if all requests are met within five minutes
        z = getparam("GeoProdMaxArriveGap") : z = cdbl("0" & strfilter(z, "0123456789"))
        if exc = 0 and z > 0 then
          d = getval(buf, getlistitem(jj,1) & "da")
          if direction = "A" then d = dateadd("s", -getval(mm, "legdura"), d)
          tt = getval(mm, "stationcourse")
          for i = 1 to countstring(tt, ",")
            x = getval(mm, "legdur" & (i-1)) : if x = "" then x = "0"
            d = dateadd("s", cdbl(x), d)
            ee = jj & j & ","
            do until ee = ""
              e = left(ee,instr(ee,",")-1) : ee = mid(ee,instr(ee,",")+1)
              if getval(buf, e & "to") = getlistitem(tt,i) then
                x = datediff("s", getval(buf, e & "da"), d)
                if x > z * 60 or x < -(z * 60) then
                  exc = 1 : debit "J" & j & " - j" & e & " more than " & z & " minutes gap" : exit for
                end if
              end if
            loop
          next
        end if
        '-----------

        'if multidestinations = "on" then 'just for the duration checks
        '  t1 = uniquelist(t1) : t2 = uniquelist(t2) : bb = ""
        '  if countstring(t2, ",") > 1 then
        '    m = 999999999
        '    for x = 1 to countstring(t1, ",")
        '      for y = 1 to countstring(t2, ",")
        '        tt = getlistitem(t1, x) & "," & nomatch(t1 & t2, getlistitem(t1, x) & "," & getlistitem(t2, y) & ",") & getlistitem(t2, y) & ","
        '        tt = uniquelist(tt) : if direction = "B" then tt = reverselist(tt)
        '        tt = stationcourse(tt , "", 1, now, pids, altstations) : t = getval(tt, "legdura") : t = cdbl("0" & t)
        '        tt = getval(tt, "stationcourse")
        '        if t < m then m = t : bb = iif(direction = "B", reverselist(tt), tt)
        '      next
        '    next
        '    if bb <> "" then mm = stationcourse(direction & bb, "", 1, getval(buf, getlistitem(jj,1) & "da"), pids, altstations)
        '  end if
        'end if

        ''---just between the workers, without the last leg on collect or first leg on distribute
        'if instr(siteurl, "/natalia.moovex.net/") > 0 and isnumeric(maxdu) then
        '  n = countstring(getval(mm, "stationcourse"), ",")
        '  z = cdbl("0" & getval(mm, "legdura"))
        '  if direction = "A" then z = z - cdbl("0" & getval(mm, "legdur" & (n-1)))
        '  if direction = "B" then z = z - cdbl("0" & getval(mm, "legdur1"))
        '  if z > cdbl(maxdu) * 60 then exc = 1 : debit "J" & j & " - exceeded max duration between workers (" & dur & ")"
        'end if

        if isnumeric(maxdu) and NOT useratedur then
          if legdura1 > cdbl(maxdu) * 60 then debit "J" & j & " - exceeded max duration " & maxdu : exc = 1
        end if

        '---by duration of area courserates
        if specs = "iec" and exc = 0 and useratedur then
          dur = iecteken(getval(mm, "stationcourse"), direction)
          if dur = 0 and isnumeric(maxdu) then dur = maxdu
          if dur > 0 and legdura1 > cdbl(dur) * 60 then debit "J" & j & " - exceeded max duration by teken " & fornum(legdura1/60) & " / " & dur : exc = 1
        end if

        '---mydur each passenger have his own duration
        if exc = 0 and (useratedur or duratio <> "") then
          yy = jj & j & ","
          do until yy = ""
            y = left(yy,instr(yy,",")-1) : yy = mid(yy,instr(yy,",")+1)
            m1 = getval(buf, y & "fr") : m2 = getval(buf, y & "to") : if direction = "B" then t = m1 : m1 = m2 : m2 = t
            tt = getval(mm, "stationcourse") : x = 0 : c = 0 : mydur = 0
            do until tt = ""
              t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
              if x = 1 then mydur = mydur + cdbl("0" & getval(mm, "legdur" & c))
              if t = m1 then x = 1
              if t = m2 then exit do
              c = c + 1
            loop
            if duratio <> "" then 'new style 200%
              z = duratio : z = cdbl("0" & z)
              t = stationcourse(m1 & "," & m2 & ",", "", 0, getval(buf, y & "da"), "", "") : alone = getval(t, "legdura") : alone = cdbl("0" & alone)
              if alone > 0 and mydur > 960 and mydur > alone * z then debit "J" & j & " - not added because - J" & y & " mydur " & fornum(mydur/60) & " above " & (z*100) & "% - " & m1 & "-" & m2 & "=" & fornum(alone/60) : exc = 1 : exit do
            elseif useratedur and specs <> "iec" then
              m1 = getval(session("stationareas"), m1) : if m1 = "" then m1 = "-1"
              m2 = getval(session("stationareas"), m2) : if m2 = "" then m2 = "-1"
              x = ""
              if specs = "egged" then
                t = "select data1 from courserates where instr(data1, '|duration_" & lcase(direction) & "=') > 0 and (areaid1 = " & m1 & " and areaid2 = " & m2 & " or areaid1 = " & m2 & " and areaid2 = " & m1 & ")"
                t = getonefield(t) : x = getval(t, "duration_" & direction)
              end if
              t = "select duration from courserates where duration <> '' and (areaid1 = " & m1 & " and areaid2 = " & m2 & " or areaid1 = " & m2 & " and areaid2 = " & m1 & ") order by duration"
              if x = "" then x = getonefield(t)
              dur = cdbl("0" & strfilter(x, "0123456789."))
              if dur = 0 and isnumeric(maxdu) then dur = maxdu
              if dur > 0 and mydur > cdbl(dur) * 60 then debit "J" & j & " - mydur " & fornum(mydur/60) & " exceeded max duration by areas " & m1 & "-" & m2 & "=" & dur : exc = 1
            end if
          loop

        'elseif specs = "egged" then '---simple style
        '  dur = 0
        '  tt = getval(mm, "stationcourse") : m1 = getlistitem(tt,1) : m2 = getlistitem(tt,-1)
        '  m1 = getonefield("select areaid from stations where id = 0" & m1)
        '  m2 = getonefield("select areaid from stations where id = 0" & m2)
        '  t = "select duration from courserates where duration <> '' and (areaid1 = " & m1 & " and areaid2 = " & m2 & " or areaid1 = " & m2 & " and areaid2 = " & m1 & ") order by duration"
        '  t = getonefield(t) : dur = cdbl("0" & strfilter(t, "0123456789"))
        '  if dur = 0 and isnumeric(maxdu) then dur = maxdu
        '  if dur > 0 and legdura1 > cdbl(dur) * 60 then debit "J" & j & " - exceeded max duration by areas " & dur : exc = 1

        'elseif useratedur then '---old and bad
        '  ''---just between the workers, without the last leg on collect or first leg on distribute
        '  'n = countstring(getval(mm, "stationcourse"), ",")
        '  'if direction = "A" then legdura1 = legdura1 - cdbl("0" & getval(mm, "legdur" & (n-1)))
        '  'if direction = "B" then legdura1 = legdura1 - cdbl("0" & getval(mm, "legdur1"))
        '  'if legdura1 > cdbl(maxdur) * 60 then exc = 1 : debit "J" & j & " - exceeded max duration by areas (" & dur & ")"
        '  for bb = 1 to 2
        '    if bb = 1 then tt = getval(mm, "stationcourse") : z = "" : dur = 0
        '    if bb = 2 then
        '      if dur > 0 then exit for
        '      tt = getval(mm, "stationcourse")
        '      tt = getlistitem(tt,1) & "," & getlistitem(tt,-1) & ","
        '      z = ""
        '    end if
        '    do until tt = ""
        '      m = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
        '      m = getonefield("select areaid from stations where id = 0" & m)
        '      if m = "" then m = "-1"
        '      if cstr(z) <> "" and cstr(z) <> "0" and cstr(m) <> "0" and (cstr(z) <> cstr(m) or tt = "" and dur = 0) then
        '        x = ""
        '        'if specs = "egged" then
        '        '  t = "select data1 from courserates where instr(data1, '|duration_" & lcase(direction) & "=') > 0 and (areaid1 = " & z & " and areaid2 = " & m & " or areaid1 = " & m & " and areaid2 = " & z & ")"
        '        '  t = getonefield(t)
        '        '  x = getval(t, "duration_" & direction)
        '        'end if
        '        t = "select duration from courserates where duration <> '' and (areaid1 = " & z & " and areaid2 = " & m & " or areaid1 = " & m & " and areaid2 = " & z & ") order by duration"
        '        if x = "" then x = getonefield(t)
        '        x = strfilter(x, "0123456789") : if x <> "" then dur = dur + cdbl(x)
        '      end if
        '      z = m
        '    loop
        '  next
        '  if dur = 0 and isnumeric(maxdu) then dur = maxdu
        '  if dur > 0 and legdura1 > cdbl(dur) * 60 then debit "J" & j & " - exceeded max duration by areas " & dur : exc = 1
        end if

        'if hasmatch(specs, "airlink_,") then
        '  '---last leg max 60 minutes
        '  n = countstring(getval(mm, "stationcourse"), ",")
        '  t = cdbl("0" & getval(mm, "legdur" & (n-1)))
        '  if t > maxdulastleg then exit do
        '  '---max 6 stations, 300 meters away does not count as another station
        '  maxsta = getparam("GeoProdMaxStations") : if not isnumeric(maxsta) then maxsta = 6
        '  okmeters = getparam("GeoProdMaxStationsDontCountMeters") : if not isnumeric(okmeters) then okmeters = 300
        '  tt = getval(mm, "stationcourse") : xys = "" : z = 1
        '  sq = "select id,markerx,markery from stations where id in(" & tt & "0)"
        '  set rs1 = conn.execute(sqlfilter(sq))
        '  do until rs1.eof
        '    xys = setval(xys, rs1("id") & "x", rs1("markerx"))
        '    xys = setval(xys, rs1("id") & "y", rs1("markery"))
        '    rs1.movenext
        '  loop
        '  lastlat = getval(xys, getlistitem(tt,1) & "y")
        '  lastlon = getval(xys, getlistitem(tt,1) & "x")
        '  for i = 2 to countstring(tt, ",") - 1
        '    lat = getval(xys, getlistitem(tt,i) & "y")
        '    lat = getval(xys, getlistitem(tt,i) & "x")
        '    if calcdistance(lat, lon, lastlat, lastlon) > cdbl(okmeters) then z = z + 1 : lastlat = lat : lastlon = lon
        '    if z >= cdbl(maxsta) then debit "J" & j & " - exceeded " & maxsta & " stations" : exit do
        '  next
        'end if

      end if

      ''---iec maxdu
      'if exc = 0 and specs = "iec_" then
      '  mm = uniquelist(stationids)
      '  mm = mm & jfr & "," & jto & ","
      '  dis1 = calcdist(getval(buf, getlistitem(jj,1) & "fl"), jtl)
      '  if dis1 > maxdis then dest = jto
      '  mm = nomatch(mm, dest & ",") & dest & "," :  if direction = "B" then mm = reverselist(mm)
      '  mm = stationcourse(mm, "", 1, jda, "", "")
      '  'mm = getval(mm, "stationcourse") : z = "" : dur = 0
      '  'do until mm = ""
      '  '  m = left(mm,instr(mm,",")-1) : mm = mid(mm,instr(mm,",")+1)
      '  '  '---by area
      '  '  'm = getonefield("select areaid from stations where id = " & m)
      '  '  'if cstr(z) <> "" and cstr(z) <> "0" and cstr(m) <> "0" then
      '  '  '  x = getonefield("select duration from courserates where duration <> '' and (areaid1 = " & z & " and areaid2 = " & m & " or areaid1 = " & m & " and areaid2 = " & z & ")")
      '  '  '  x = strfilter(x, "0123456789") : if x <> "" then dur = dur + cdbl(x)
      '  '  'end if
      '  '  '---by station
      '  '  'if cstr(z) <> "" and cstr(z) <> "0" and cstr(m) <> "0" then
      '  '  '  x = getonefield("select duration from courserates where duration <> '' and (from1 = " & z & " and to1 = " & m & " or from1 = " & m & " and to1 = " & z & ")")
      '  '  '  x = strfilter(x, "0123456789") : if x = "" then du = showerror("�� ���� ����� ����� ��� " & gettitle("stations", m) & " � " & gettitle("stations", z)) else dur = dur + cdbl(x)
      '  '  'end if
      '  '  '---by citystationid of worker
      '  '  if cstr(z) <> "" and cstr(z) <> "0" and cstr(m) <> "0" then
      '  '    t1 = m : t = getonefield("select citystationid from workers where citystationid > 0 and from1 = 0" & m) : if t <> "" then t1 = t
      '  '    t2 = z : t = getonefield("select citystationid from workers where citystationid > 0 and from1 = 0" & z) : if t <> "" then t2 = t
      '  '    if t1 = t2 then x = 0 else x = getonefield("select duration from courserates where duration <> '' and (from1 = " & t1 & " and to1 = " & t2 & " or from1 = " & t2 & " and to1 = " & t1 & ")")
      '  '    x = strfilter(x, "0123456789")
      '  '    if x = "" then
      '  '      errors = errors & "�� ���� ����� ����� ��� " & gettitle("stations", t1) & " � " & gettitle("stations", t2) & "<br>"
      '  '    else
      '  '      if session("debug") = "on" then du = showerror(x & " MINUTES - " & gettitle("stations", t1) & " TO " & gettitle("stations", t2))
      '  '      dur = dur + cdbl(x)
      '  '    end if
      '  '  end if
      '  '  z = m
      '  'loop
      '  'if dur > 97 then debit "J" & j & " - exceeded max duration" : exc = 1
      '  z = getval(mm, "legdura") : mm = getval(mm, "stationcourse") : t1 = getlistitem(mm, 1) : t2 = getlistitem(mm, -1)
      '  if z <> "" and t1 <> "" and t2 <> "" and t1 <> t2 then
      '    t = getonefield("select citystationid from workers where citystationid > 0 and from1 = 0" & t1) : if t <> "" then t1 = t
      '    t = getonefield("select citystationid from workers where citystationid > 0 and from1 = 0" & t2) : if t <> "" then t2 = t
      '    x = getonefield("select duration from courserates where duration <> '' and (from1 = " & t1 & " and to1 = " & t2 & " or from1 = " & t2 & " and to1 = " & t1 & ")")
      '    if x = "" then
      '      errors = errors & "�� ���� ����� ����� ��� " & gettitle("stations", t1) & " � " & gettitle("stations", t2) & "<br>"
      '    else
      '      z = cdbl(z) / 60
      '      if cdbl(x)+1 < cdbl(z) then debit "J" & j & " - real duration " & z & " exceeded max allowed duration " & x : exc = 1
      '    end if
      '  end if
      'end if

      '----remove my area if not all of us. mitsui
      firstarea = getval(buf, getlistitem(jj,1) & "a1")
      x = 0
      if hasmatch(specs, "bbalev,iec,philips,tenmore,police,royalbeach,sheleg,barlev,hamlet,carmelforest,shufersal_,afikim,shamir,jr,mamillahotel,waldorfastoria,superbus,plasson,") then x = 1
      if instr(siteurl, "/merkazny.moovex.net/") > 0 then x = 1
      if instr(siteurl, "/golanplastic.moovextest.net/") > 0 then x = 1
      if x = 1 and ja1 <> firstarea then
        if exc = 0 then
          seatsleft = maxpa - countstring(jj, ",")
'deb "J" & j & " A" & ja1 & " KKA " & kkareas
          mymates = countstringoverlap("," & kkareas, "," & ja1 & ",")
'deb "MAXPA" & maxpa & " SEATS" & seatsleft & " MYMATES" & mymates
          if mymates > seatsleft and mymates <= maxpa then 
            debit "J" & j & " - area " & gettitle("areas", ja1) & " rather go alone" : exc = 1 : exccount = exccount - 1
          end if
        end if
        if exc = 1 then
          tt = jj : maxdis = 0 : stationids = "" : via1 = ""
          do until tt = ""
            p = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
            if getval(buf, p & "a1") = ja1 then
              jj = nomatch(jj, p) ': kk = kk & p & ","
            else
              dis1 = calcdist(getval(buf, getlistitem(jj,1) & "fl"), getval(buf, p & "tl"))
              if dis1 > maxdis then maxdis = dis1 : dest = getval(buf, p & "to")
              stationids = stationids & getval(buf, p & "fr") & "," & getval(buf, p & "to") & ","
              't = ";" & getval(buf, p & "ft") : t = mid(t, instrrev(t,";")+1) : if not hasmatch(via1, t) then via1 = via1 & t & ", "
            end if
          loop
          tt = kk
          do until tt = ""
            p = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
            if getval(buf, p & "a1") = ja1 then kk = nomatch(kk, p)
          loop
          debit "J" & j & " remove all my area"
        end if
      end if

      '----remove some if can manage with only taxi 4
      z = 0
      if specs = "carlsberg" then z = 4
      if z > 0 then 'and exc = 1 
        if countstring(jj, ",") > z and countstring(jj & kk, ",") <= z*2 then
          tt = jj : maxdis = 0 : stationids = "" : via1 = ""
          do until tt = ""
            p = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
            if getlistposition(jj, p) <= z then
              dis1 = calcdist(getval(buf, getlistitem(jj,1) & "fl"), getval(buf, p & "tl"))
              if dis1 > maxdis then maxdis = dis1 : dest = getval(buf, p & "to")
              stationids = stationids & getval(buf, p & "fr") & "," & getval(buf, p & "to") & ","
              't = ";" & getval(buf, p & "ft") : t = mid(t, instrrev(t,";")+1) : if not hasmatch(via1, t) then via1 = via1 & t & ", "
              t = ";" & gettitle("stations", getval(buf, p & "fr")) : t = mid(t, instrrev(t,";")+1) : if not hasmatch(via1, t) then via1 = via1 & t & ", "
            else
              jj = nomatch(jj, p) : kk = kk & p & ","
            end if
          loop
          debit "J" & j & " remove all passengers above taxi" : kk = "" : exit do
        end if
      end if

      '--------
      if exc = 1 then
        kk = nomatch(kk,j)
        z = 7
        if hasmatch(specs, "kimberlyclark,tenmore,afikim,") then z = 10
        if instr(siteurl, "/gushetzion.moovex.net/") + instr(siteurl, "/gushspecial.moovex.net/") > 0 then z = 15
        if specs = "jerusalem" then z = 12
        if specs = "phibro" then z = 15
        exccount = exccount + 1 : if exccount >= z then debit "Excluded enough" : exit do
      else
        debit "J" & j & " - ADDED area " & gettitle("areas", ja1) & "TIMER " & timer - timer0
        '---add
        kk = nomatch(kk,j) : jj = jj & j & ","
        tt = "," & kkareas : tt = replace(tt, "," & ja1 & ",", ",",1 ,1 ,1) : tt = mid(tt,2) : kkareas = tt
        dis1 = calcdist(getval(buf, getlistitem(jj,1) & "fl"), jtl)
        if dis1 > maxdis then maxdis = dis1 : dest = jto
        stationids = stationids & jfr & "," & jto & ","
        aa1 = aa1 & ja1 & ","
        aa2 = aa2 & ja2 & ","
        '---via1
        t = gettitle("stations", jfr) : if instr(t, ";") > 0 then t = mid(t, instrrev(t,";")+1)
        if not hasmatch(via1, t) then via1 = via1 & t & ", "
        '----balance 2 cars instead of 10+2 will be 6+6
        if hasmatch(specs, "shamir,shalag,yes,egged,shufersal,") then
          if countstring(jj & kk, ",") >= cdbl(maxpa) and countstring(jj, ",") >= countstring(kk, ",") then
            debit "J" & j & " remove all passengers above balance" : kk = "" : exit do
          end if
        end if
      end if
    loop

    if jj <> "" then
      '----geo it
      debit "JJ " & jj
      if hasmatch(stationids, dest) then stationids = uniquelist(stationids) : stationids = nomatch(stationids, dest & ",") & dest & ","
      firstj = "" : lockstations = false

      bannedareas = ""
      '---find real actual farthest orig from dest
      z = 0
      'if specs = "barlev" and getval(buf, getlistitem(jj,1) & "si")) = "" then z = 99
      if hasmatch(specs, "sheba,police,royalbeach,sheleg,barlev,mormar,cocacola,shufersal,bbalev,bontour,hamlet,metropolin,tenmore,hatama,airlink,egged,") then z = 99
      if z > 0 and countstring(stationids, ",") <= z then
        mm = "" : m = 99999999
        for x = 1 to countstring(stationids,",") - 1
          t = getlistitem(stationids, x) : tt = t & "," & nomatch(stationids, t & "," & dest & ",") & dest & ","
          if direction = "B" then tt = reverselist(tt)
          t = stationcourse(direction & tt , "", 1, now, "", "") : t = getval(t, "legdura") : t = cdbl("0" & t)
          if t < m then m = t : mm = x
        next
        t = getlistitem(stationids, mm) : if t <> "" then stationids = t & "," & nomatch(stationids, t)

      'elseif multidestinations = "on" then
      '  tt = jj : t1 = "" : t2 = "" : pids = "" : altstations = ""
      '  do until tt = ""
      '    t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
      '    pids = pids & getval(buf, t & "wo") & ","
      '    altstations = setval(altstations, w & "from1", getval(buf, t & "fr"))
      '    altstations = setval(altstations, w & "to1", getval(buf, t & "to"))
      '    t1 = t1 & getval(buf, t & "fr") & "," : t2 = t2 & getval(buf, t & "to") & ","
      '  loop
      '  t1 = uniquelist(t1) : t2 = uniquelist(t2)
      '  m = 999999999
      '  for x = 1 to countstring(t1, ",")
      '    for y = 1 to countstring(t2, ",")
      '      tt = getlistitem(t1, x) & "," & nomatch(t1 & t2, getlistitem(t1, x) & "," & getlistitem(t2, y) & ",") & getlistitem(t2, y) & ","
      '      tt = uniquelist(tt) : if direction = "B" then tt = reverselist(tt)
      '      tt = stationcourse(tt , "", 0, now, pids, altstations) : t = getval(tt, "legdura") : t = cdbl("0" & t)
      '      tt = getval(tt, "stationcourse")
      '      if t < m then m = t : stationids = iif(direction = "B", reverselist(tt), tt)
      '    next
      '  next
      '  lockstations = true

      elseif direction = "A" then
        t = findfarthest(jj, findcenter(jj)) : firstj = t
        t = getval(buf, t & "fr")
        stationids = t & "," & nomatch(stationids, t)
      end if

      '---B departure from earliest shift
      forcedate1 = ""
      if specs = "babcom" then
        tt = jj : mm = getlistitem(stationids, -1)
        m = "01/01/2000" : if direction = "B" then m = "31/12/2099"
        do until tt = ""
          t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
          x = getval(buf, t & "da") : if direction = "A" and cdate(x) > cdate(m) or direction = "B" and cdate(x) < cdate(m) then m = x : mm = getval(buf, t & "to") : forcedate1 = m
        loop
        stationids = nomatch(stationids, mm) & mm & ","
      end if

      '---
      if direction = "B" then stationids = reverselist(stationids)
      ee = jj : ww = "" : altstations = "" : dest = "" : maxdis = 0
      do until ee = ""
        e = left(ee,instr(ee,",")-1) : ee = mid(ee,instr(ee,",")+1)
        w = getval(buf, e & "wo") : ww = ww & w & ","
        altstations = setval(altstations, w & "from1", getval(buf, e & "fr"))
        altstations = setval(altstations, w & "to1", getval(buf, e & "to"))
        altstations = setval(altstations, w & "shifthour", getval(buf, e & "da"))

        '---multi destinations
        if firstj <> "" then
          dis1 = calcdist(getval(buf, firstj & "fl"), getval(buf, e & "tl"))
          if dis1 > maxdis then maxdis = dis1 : dest = getval(buf, e & "to") : stationids = nomatch(stationids, dest & ",") & dest & ","
        end if

        if specs = "mormar" then
          t = getval(buf, e & "dd") : t = replace(t, "\", "|") : t = getval(t, "shiftid")
          altstations = setval(altstations, w & "shiftid", t)
        end if
        'altstations = setval(altstations, w & "altaddress", getval(buf, e & "ad"))
        if specs = "iec" then
          qq = "wbs,internal,costcenter,"
          do until qq = ""
            q = left(qq,instr(qq,",")-1) : qq = mid(qq,instr(qq,",")+1)
            t = getval(buf, e & "dd") : t = replace(t, "\", "|") : t = getval(t, q) : if t <> "" then historyadd = setval(historyadd, w & q, t)
          loop
        end if
      loop

      ww = ww & addescort
      debit "WW " & ww & "<BR>TIMER " & timer - timer0

      t = getval(buf, getlistitem(jj,1) & "da")
      'if specs = "iec_" and direction = "A" then 'set the time back to near the eventual start because google only accepts departure_time
      '  tt = stationcourse(direction & stationids, "", 0, "", ww, altstations)
      '  x = getval(tt, "legdura")
      '  t = dateadd("s", -x, t)
      'end if
      op = 1 : if lockstations then op = 0
      aa = stationcourse(direction & stationids, "", op, t, ww, altstations) : stationids = getval(aa, "stationcourse")

      '---find maxpa again for final stations in case it's different areas
      if hasmatch(specs, "flextronics,") then 'modiin,iec,
        t1 = getval(session("stationareas"), getlistitem(stationids, 1))
        t2 = getval(session("stationareas"), getlistitem(stationids, -1))
        maxpa = getonefield("select passengers from courserates where active = 'on' and (areaid1 = 0" & t1 & " and areaid2 = 0" & t2 & " or areaid1 = 0" & t2 & " and areaid2 = 0" & t1 & ") order by passengers desc")
        maxpa = cdbl("0" & strfilter(maxpa, "0123456789")) : if maxpa <= 0 then maxpa = 50
        'if specs = "modiin" then
        '  tt = jj : dd = ""
        '  do until tt = ""
        '    t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
        '    dd = dd & getval(workerdisabilities, getval(buf, t & "wo"))
        '    if hasmatch(dd, "taxidisabilityid") or hasmatch(dd, "liftdisabilityid") then maxpa = 4 : jj = t & "," & nomatch(jj, t) : exit do
        '  loop
        '  if hasmatch(dd, "liftdisabilityid") then historyadd = setval(historyadd, "needlift", "on")
        'end if
      end if

      ''---divide if zero passengers within route
      'if specs = "egged" or instr(siteurl, "/netanya.moovex.net/") + instr(siteurl, "/brener.moovex.net/") > 0 then
      '  ss = stationids : ss2 = "" : aboard = "" : wasaboard = ""
      '  do until ss = ""
      '    si = left(ss,instr(ss,",")-1) : ss = mid(ss,instr(ss,",")+1)
      '    ss2 = ss2 & si & ","
      '    ee = jj
      '    do until ee = ""
      '      e = left(ee,instr(ee,",")-1) : ee = mid(ee,instr(ee,",")+1)
      '      t1 = getval(buf, e & "fr") : t2 = getval(buf, e & "to") : if direction = "B" then t = t1 : t1 = t2 : t2 = t
      '      if si = t1 then aboard = aboard & e & ","
      '      if si = t2 then aboard = nomatch(aboard, e & ",") : wasaboard = wasaboard & e & ","
      '      if wasaboard <> "" and aboard = "" and nomatch(jj,wasaboard) <> "" then
      '        debit jj & " zero passengers within " & wasaboard
      '        jj = wasaboard : stationids = ss2 : ss = ""
      '        tt = jj : ww = "" : do until tt = "" : t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1) : ww = ww & getval(buf, t & "wo") & "," : loop
      '        aa = stationcourse(direction & stationids, "", 1, getval(buf, getlistitem(jj,1) & "da"), ww, altstations) : stationids = getval(aa, "stationcourse")
      '        exit do
      '      end if
      '    loop
      '  loop
      'end if

      ''----add escort
      'addescort = ""
      'if specs = "modiin" then
      '  ee = jj : es = ""
      '  do until ee = ""
      '    e = left(ee,instr(ee,",")-1) : ee = mid(ee,instr(ee,",")+1)
      '    if needescort(getval(buf, e & "wo") & ",") then
      '      s = "select data1,unitid from workers where id = " & getval(buf, e & "wo")
      '      set rs1 = conn.execute(sqlfilter(s))
      '      u = "" : d = "" : if not rs1.eof then u = rs1("unitid") & "" : d = rs1("data1") & ""
      '      es = getval(d, "escortids") : es = nomatch(es, alreadyescorted) : es = getlistitem(es, 1)
      '      if es = "" then
      '        s = "select id,data1,unitid from workers where workertypeid = " & escortid & " and firstname <> '' and id not in(" & alreadyescorted & "-1)"
      '        set rs1 = conn.execute(sqlfilter(s))
      '        do until rs1.eof
      '          d = rs1("data1") : uu = getval(d, "unitids") & rs1("unitid") & ","
      '          t = getval(d, "allowways") : if t = "" or t = "AB" or t = direction then t = "ok"
      '          if not isavailable(getval(buf, e & "da"), getval(d,"ava")) then t = ""
      '          if t = "ok" and hasmatch(uu, u) then es = rs1("id") : exit do
      '          rs1.movenext
      '        loop
      '      end if
      '      if es <> "" then exit do
      '      if es = "" then du = showerror("��� ����� ���� ���� - " & getworkername(getval(buf, e & "wo")))
      '    end if
      '  loop
      '  if es <> "" then
      '    addescort = es & "," : alreadyescorted = alreadyescorted & addescort
      '    debit "ADDED ESCORT ID " & es
      '    if countstring(jj, ",") + 1 > maxpa then : tt = jj : tt = nomatch(tt, e) : tt = nomatch(tt, getlistitem(tt, -1)) : tt = tt & e & "," : jj = tt
      '  end if
      'end if

      '----MinimumPassengersForProductionOrder ---minpassengers
      v = cdbl("0" & strfilter(minpassengers, "0123456789"))
      if countstring(ww, ",") < v then
        tt = ww
        do until tt = ""
          w = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
          t = "~Worker~ " & getworkername(w) & " - " & date1 & " ~direction" & direction & "~ - �� ����. " & countstring(ww, ",") & " ���� " & v & " ������ ������� ����."
          debit t
          du = showerror(t)
        loop
        ww = ""
      end if

      '---db
      if ww <> "" then
        debit "<b>" & aa & "</b>"
        from1 = getlistitem(stationids,1)
        to1 = getlistitem(stationids,-1)

        'w = getlistitem(addescort, 1)
        'if w <> "" then
        '  t1 = from1 : t2 = to1 : if direction = "B" then t1 = to1 : t2 = from1
        '  altstations = setval(altstations, w & "from1", t1)
        '  altstations = setval(altstations, w & "to1", t2)
        'end if

        ordertypeid = session("shifts_otid")
        'ordertypeid = session("special_otid") : if hasmatch(specs, "cocacola,iec,mormar,shufersal,hamlet,philips,bbalev,flextronics,egged,jr,brener,gett,hatama,") then ordertypeid = session("shifts_otid")
        'if specs = "bontour" and cstr(siteid) = "5" then ordertypeid = session("shifts_otid") '������

        ee = jj : date1 = "1/1/2100" : if direction = "B" then date1 = "1/1/2000"
        do until ee = ""
          e = left(ee,instr(ee,",")-1) : ee = mid(ee,instr(ee,",")+1)
          t = getval(buf, e & "da")
          if direction = "A" and cdate(t) < cdate(date1) then date1 = t
          if direction = "B" and cdate(t) > cdate(date1) then date1 = t
          t = getval(buf, e & "ot") : if t = "special" then ordertypeid = session("special_otid")
        loop
        if forcedate1 <> "" then date1 = forcedate1
        date2 = date1 : if direction = "A" then t = getval(aa,"legdura") : if isnumeric(t) then date1 = dateadd("s", -t, date1)
        if specs = "cocacola" and direction = "A" and getdatehourstring(date1) >= "06:01" and getdatehourstring(date1) <= "21:00" then date1 = dateadd("n", -15, date1)
        'if specs = "cocacola" and direction = "B" then date1 = dateadd("n", +15, date1)

        history = aa & historyadd
        km = "0" : t = getval(aa, "legdisa") : if isnumeric(t) then km = fix(t/1000)

        siteid = getlistitem(getidlist("select siteid from workers where id = " & getlistitem(ww,1)),1) : if siteid = "" then siteid = "0"
        'if specs = "iec" then : t = getval(buf, getlistitem(jj,1) & "dd") : t = replace(t, "\", "|") : t = getval(t, "shiftid") : if t <> "" and cstr(t) <> "0" then siteid = getonefield("select siteid from shifts where id = " & t)
        if specs = "iec" then
          e = to1 : if direction = "B" then e = from1
          t = getval(buf, getlistitem(jj,1) & "fd") : if t <> "" then e = t
          t = getlav(session("mainstations"), e) : if t <> "" then siteid = t
        end if

        supplierid = "" : if hasmatch(specs, "police,royalbeach,") then supplierid = getonefield("select id from suppliers where siteid = 0" & siteid & " order by title")
        carid = "" : if hasmatch(specs, "police,royalbeach,") then carid = getonefield("select id from cars where supplierid = 0" & supplierid & " order by title")

        if specs = "airlink" then
          x = datediff("s", date1, date2)
          date1 = date2
          date2 = dateadd("s", x, date1)
        end if

        f = "" : v = ""
        f = f & "date1," : v = v & sqldate(date1) & ","
        f = f & "date2," : v = v & sqldate(date2) & ","
        f = f & "status," : v = v & "'New',"
        f = f & "courseid," : v = v & "0,"
        f = f & "from1," : v = v & "0" & from1 & ","
        f = f & "to1," : v = v & "0" & to1 & ","
        f = f & "via1," : v = v & "'" & left(chtm(via1),50) & "',"
        f = f & "direction," : v = v & "'" & direction & "',"
        f = f & "supplierid," : v = v & "0" & supplierid & ","
        f = f & "carid," : v = v & "0" & carid & ","
        f = f & "km," : v = v & "0" & km & ","
        f = f & "ordertypeid," : v = v & ordertypeid & ","
        f = f & "production," : v = v & production & ","
        f = f & "price," : v = v & "0,"
        f = f & "pids," : v = v & "'" & ww & "',"
        f = f & "pcount," : v = v & countstring(ww, ",") & ","
        f = f & "returnorderid," : v = v & "0,"
        f = f & "userid," : v = v & session("userid") & ","
        f = f & "siteid," : v = v & siteid & ","
        f = f & "altstations," : v = v & "'" & altstations & "',"
        f = f & "updateduserid," : v = v & session("userid") & ","
        f = f & "updateddate," : v = v & sqldate(now) & ","
        f = f & "history," : v = v & "'" & history & "',"
        f = f & "data1," : v = v & "'',"
        f = f & "reasonid," : v = v & "0,"
        if shownorth then f = f & "cartypeid," : v = v & getorderlatitude(stationids, direction) & ","
        f = left(f, len(f) - 1): v = left(v, len(v) - 1)
        s = "insert into orders(" & f & ") values(" & v & ")"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
        orderscount = orderscount + 1
        s = "select max(id) as id from orders"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
        thisid = rs1("id") : productionids = productionids & thisid & ","
        debit "ORDERID " & thisid & " TIMER " & timer - timer0 & " REQ " & stationcourserequestcounter & " UNC " & stationcourserequestcounter2 & "<br>"
        price = getrideprice(thisid)
        if specs = "mormar" then du = findreturn(thisid)
        du = escortalert(thisid, ww, 0, history)
        if specs = "bbalev" then du = smsworkers(thisid)
        if getfield("limitorders") <> "" then if orderscount >= cdbl(getfield("limitorders")) then exit do
      end if
      '---

      ii = nomatch(ii, jj)
    end if
  loop
  rr "<script language=vbscript> progress.style.top = -1000 </script>" : response.flush
  rr "</div>"
  
  u = siteurl & "admin_orders.asp?action=search&action2=clear&production=" & production & "&date1from=1/1/1900&date1to=1/1/1900&ordersview=-"
  if production <> "" and errors = "" and session("debug") <> "on" and getfield("action3") <> "close" then
    rr "<script language=vbscript> document.location = """ & u & """ </script>"
  end if
  rr "<script language=vbscript> divpleasewait.style.top = -2000 </script>"

  a = "<a href=" & u & ">" & orderscount & " ~ORDERS~</a><br>"
  errors = "<div style='color:#ff0000;'>" & errors & "</div>" & a
  rr errors

  '------------setproduceduntil
  v = getparam("ProducedUntil")
  a = getfield("siteids") : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : v = setval(v, "site" & b, pd2) : loop
  a = getfield("shiftids") : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : v = setval(v, "shift" & b, pd2) : loop
  a = getfield("unitids") : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : v = setval(v, "unit" & b, pd2) : loop
  if getfield("siteids") = "" and session("usersiteid") <> "0" then
    v = setval(v, "site" & session("usersiteid"), pd2)
  elseif getfield("shiftids") = "" then
    v = setval(v, "general", pd2)
  end if
  du = putparam("ProducedUntil", v)
  'du = setproduceduntil() '..... moved up
  '--------------

  ee = getparam("ProductionLogEmail") & ","
  if specs = "egged" and getfield("siteids") <> "" then ee = ee & getfieldlist("select email from buusers where usergroup = 'Admin' and siteid in(" & getfield("siteids") & "0)")
  subject = translate(appname & " - ~production~ " & production & " - " & orderscount & " ~orders~")
  body = ""
  body = body & now & "<br>"
  body = body & "<div style='color:#aaaaaa; direction:ltr;'>" & siteurl & "</div><br>"
  body = body & "���� �� ��� ����� " & session("username") & "<br>"
  if instr(pd1, "/2000") = 0 then body = body & " ���� ���� " & pd1 & " �� " & pd2 & "<br>"
  body = body & translate(errors)
  debdata.position = 0 : t = debdata.readtext(debdata.size) : debdata.close
  'body = body & "<hr><br><br><br><div style='direction:ltr; text-align:left; font-size:9; color:#aaaaaa;'>" & t
  f = "upload/productionlog" & production & ".htm" : du = addlog(f, errors & vbcrlf & t)
  body = body & "<br><br><a href=" & siteurl & f & ">View Production Log</a>"
  if not itslocal then x = sendemail(getparam("siteemail"), ee, "", "", subject, body)' : if specs = "shufersal" then du = addlog("upload/1.111", now & "|" & ee & "|" & subject & "|" & len(body))
  du = addlog("upload/productionlog.txt", "-----" & vbcrlf & subject & vbcrlf & replace(body,"<br>",vbcrlf))
  if getfield("sendorders") = "on" then rr sendorders(productionids)
  REND
end if

sub debit(byval t)
  t = t & "<br>"
  debdata.writetext(t) : if session("debug") = "on" then rr t : response.flush
end sub

function findfarthest(byval ww, toll)
  dim c, w, x, y, m, t, tolat, tolon, fromlat, fromlon
  m = -1
  do until ww = ""
    w = left(ww,instr(ww,",")-1) : ww = mid(ww,instr(ww,",")+1)
    t = calcdist(getval(buf, w & "fl"), toll)
    if t > m then m = t : findfarthest = w
  loop
end function

function findclosest(byval ww, toll)
  dim c, w, x, y, m, t, tolat, tolon, fromlat, fromlon
  m = 9999999999
  do until ww = ""
    w = left(ww,instr(ww,",")-1) : ww = mid(ww,instr(ww,",")+1)
    t = calcdist(getval(buf, w & "fl"), toll)
    if t < m then m = t : findclosest = w
  loop
end function

function findcenter(byval ww)
  dim c, w, x, y, m, t, tolat, tolon, fromlat, fromlon, x1, y1
  c = 0
  do until ww = ""
    w = left(ww,instr(ww,",")-1) : ww = mid(ww,instr(ww,",")+1)
    ''---from
    't = getval(buf, w & "fl")
    'y = left(t, instr(t,";")-1)
    'x = mid(t, instr(t,";")+1)
    'y1 = y1 + cdbl(y) : x1 = x1 + cdbl(x)
    'c = c + 1
    '---to
    t = getval(buf, w & "tl")
    y = left(t, instr(t,";")-1)
    x = mid(t, instr(t,";")+1)
    y1 = y1 + cdbl(y) : x1 = x1 + cdbl(x)
    c = c + 1
  loop
  findcenter = (y1 / c) & ";" & (x1 / c)
end function

'===========================================================================================

if request_action = "draftreal" or request_action2 = "draftreal" then
  rclear
  rr "<script language=vbscript> parent.f1.target = """" </script>"
  approveways = getfield("approveways")
  'ii = uniquelist(getfield("ids") & getfield("id") & ",")
  if getfield("id") <> "" then 'workershiftid
    ii = getfield("id") & ","
  else
    ii = "" : a = getfield("ids") 'workerids
    do until a = ""
      wid = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      d = d1
      do until d > d2
        k = "u" & wid & "d" & replace(d,"/","_")
        if getfield(k) = "on" then
          ii = ii & getidlist("select id from workershifts where workerid = " & wid & " and date1 = " & sqldate(d))
        end if
        d = dateadd("d",1,d)
      loop
    loop
  end if
  do until ii = ""
    i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
    if approveways = "-" then
      s = "delete * from workershifts where id = " & i
      set rs = conn.execute(sqlfilter(s))
    elseif approveways = "r" then
      s = "select workerid,date1,data1 from workershifts where id = " & i
      set rs1 = conn.execute(sqlfilter(s))
      data1 = rs1("data1") & ""
      data1 = setval(data1, "rejected", "on")
      s = "update workershifts set data1 = '" & data1 & "' where id = " & i
      set rs = conn.execute(sqlfilter(s))
      if specs = "sanmina" then
        u = getonefield("select unitid from workers where id = 0" & rs1("workerid"))
        ee = getfieldlist("select email from buusers where usergroup = 'Manager' and instr(cstr(',' & unitids), '," & u & ",') > 0")
        subject = getusername(session("userid")) & " �� ���� ����� �" & getworkername(rs1("workerid")) & " ������ " & rs1("date1")
        du = sendemail(getparam("siteemail"), ee, "", "", subject, "")
      end if
    else
      data1 = getonefield("select data1 from workershifts where id = " & i)
      data1 = setval(data1, "draft", "")
      ways = "" : oldways = getonefield("select ways from workershifts where id = " & i)
      if instr(approveways, "A") > 0 and instr(oldways, "A") > 0 then ways = ways & "A"
      if instr(approveways, "B") > 0 and instr(oldways, "B") > 0 then ways = ways & "B"
      s = "update workershifts set data1 = '" & data1 & "', ways = '" & ways & "', date0 = " & sqldate(now) & " where id = " & i
      set rs = conn.execute(sqlfilter(s))
    end if
    rr "<script language=vbscript>" & vbcrlf
    if approveways = "r" then
      rr "  parent.draftactions" & i & ".style.color = ""#aa0000""" & vbcrlf
      rr "  parent.draftactions" & i & ".innerhtml = ""~Rejected~""" & vbcrlf
    else
      rr "  parent.ws" & i & ".style.color = ""#000000""" & vbcrlf
      rr "  parent.draftactions" & i & ".style.visibility = ""hidden""" & vbcrlf
    end if
    rr "  a = parent.ws" & i & ".innerhtml" & vbcrlf
    if approveways = "B" then rr "  a = mid(a, instr(a,""�����""))" & vbcrlf
    if approveways = "A" then rr "  a = left(a, instr(a,""�����"")-1)" & vbcrlf
    if approveways = "-" then rr "  a = """"" & vbcrlf
    rr "  parent.ws" & i & ".innerhtml = a" & vbcrlf
    rr "</script>" & vbcrlf
  loop
  rend
end if

dim ss(99) : ss(0) = "" : ss(1) = "" 'readiness reports
'---------------------readiness for iwi only
if specs = "iwi" and getfield("action") = "readiness" then
  rr "<br><br><br><center><table class=form_table cellspacing=12><tr><td class=form_name colspan=22>~Referent Readiness~"
  DaysAhead = 1
  rr "<tr style='height:40;'><td><b>~Unit~"
  for i = 0 to DaysAhead
    t = dateadd("d", i, date) : t = left(t, instrrev(t,"/")-1)
    rr "<td><b>" & t
  next

  for i = 0 to DaysAhead
    ss(i) = ss(i) & getfieldlist("select workerid from workershifts where date1 = " & sqldate(dateadd("d",i,date)))
  next

  s = "select id,title from units where 0=0"
  if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
  s = s & " order by title"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    mm = getidlist("select id from buusers where instr(cstr(',' & unitids), '," & rs("id") & ",') > 0")
    if mm <> "" then mm = "-" & replace(mm, ",", ",-") : mm = left(mm, len(mm)-1)
    rr "<tr valign=top><td>" & rs("title") & "</nobr>"
    ww = getidlist("select id from workers where active='on' and unitid = 0" & rs("id"))
    for i = 0 to DaysAhead
      t = "x.png"
      if ww = bothmatch(ww, ss(i)) or hasmatch(ss(i), mm) then
        t = "v.png"
      elseif a <> "" and a <> ww then
        t = "v-.png"
      end if
      if specs = "tnuva" then t = "x.png" : if instr("," & ss(i), ",-" & rs("id") & ",") > 0 then t = "v.png"
      rr "<td><img src=" & t & " style='margin-left:30;'>"
    next
    rs.movenext
    rr "<tr><td colspan=3 style='height:1px; background:#eeeeee;'>"
  loop

  rr "<tr><td bgcolor=#dddddd colspan=22><br>"
  rr "&nbsp;&nbsp;&nbsp;<img src=v.png width=10> ~All shifts are set~"
  rr "&nbsp;&nbsp;&nbsp;<img src=v-.png width=10> ~Shifts partly set~"
  rr "&nbsp;&nbsp;&nbsp;<img src=x.png width=10> ~None Set~"
  rr "<br><br>"
  rend
end if

'---------------------readiness
if getfield("action") = "readiness" then
  rr "<br><br><br><center><table class=form_table cellspacing=12><tr><td class=form_name colspan=22>~Referent Readiness~"
  DaysAhead = 1
  if specs = "leumi" then DaysAhead = 6
  if specs = "tnuva" then DaysAhead = 3
  rr "<tr><td><b>~Manager~"
  for i = 0 to DaysAhead
    t = dateadd("d", i, date) : t = left(t, instrrev(t,"/")-1)
    rr "<td>" & t
  next

  for i = 0 to DaysAhead
    ss(i) = ss(i) & getfieldlist("select workerid from workershifts where date1 = " & sqldate(dateadd("d",i,date)))
  next
  s = "select id,firstname,lastname,username,unitids from buusers where (usergroup = 'Manager' or usergroup = 'TeamLeader')"
  if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
  s = s & " order by firstname, lastname, username"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    t = rs("firstname") & " " & rs("lastname") : if trim(t) = "" then t = rs("username")
    t = "<B>" & t & "</B>"
    a = "" : uu = rs("unitids") : if isnull(uu) then uu = ""
    do until uu = ""
      u = left(uu,instr(uu,",")-1) : uu = mid(uu,instr(uu,",")+1)
      a = a & ", " & gettitle("units",u)
    loop
    if a <> "" then t = t & " " & mid(a,3)
    rr "<tr valign=top><td>" & t & "</nobr>"
    ww = getidlist("select id from workers where active='on' and unitid in(" & rs("unitids") & "-1)")
    for i = 0 to DaysAhead
      a = bothmatch(ww,ss(i))
      t = "x.png"
      if a = ww or instr("," & ss(i), ",-" & rs("id") & ",") > 0 then
        t = "v.png"
      elseif a <> "" and a <> ww then
        t = "v-.png"
      end if
      if specs = "tnuva" then t = "x.png" : if instr("," & ss(i), ",-" & rs("id") & ",") > 0 then t = "v.png"
      rr "<td><img src=" & t & " style='margin-left:30;'>"
    next
    rs.movenext
    rr "<tr><td colspan=3 style='height:1px; background:#eeeeee;'>"
  loop

  rr "<tr><td bgcolor=#dddddd colspan=22><br>"
  rr "&nbsp;&nbsp;&nbsp;<img src=v.png width=10> ~All shifts are set~"
  rr "&nbsp;&nbsp;&nbsp;<img src=v-.png width=10> ~Shifts partly set~"
  rr "&nbsp;&nbsp;&nbsp;<img src=x.png width=10> ~None Set~"
  rr "<br><br>"
  rend
end if


'---------------------shiftcounterreport
if getfield("action") = "shiftcounterreport" or getfield("action") = "medreport" or getfield("action") = "shiftworkersreport" then
  server.scripttimeout = 3600
  shiftgroups = ""
  showworkers = getidlist("select id from workers where 0=0" & okreadsql("workers"))

  if getfield("shiftcounterreportemail") <> "" then x = putparam("-1000|shiftcounterreportemail", getfield("shiftcounterreportemail"))
  set csv = Server.CreateObject("ADODB.Stream") : csv.open
  set doc = Server.CreateObject("ADODB.Stream") : doc.open
  doc.writetext("<body style='direction:rtl;'>")

  if getfield("action") = "medreport" then
    oo = getidlist("select id from workertypes where instr(title, '����') > 0")
    oo = getidlist("select id from workers where workertypeid in(" & oo & "-1)")
    title = "��� ������"
  else
    title = "~Workers~ / ~Shift~"
    'if specs = "techjet" then title = "��� ������"
  end if
  s = "select id,title,hour1,hour1b from shifts"
  if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
  a = session("userunitids") : ii = "" : e = ""
  if a <> "" then
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      e = e & " or instr(',' & cstr(unitids), '," & b & ",') > 0"
    loop
    ii = getidlist("select id from shifts where cstr(unitids) = '' " & e)
    s = s & " and id in(" & ii & "-1)"
  end if
  s = s & " order by hour1, title"
  s = replace(s," and "," where ",1,1,1)
  set rs = conn.execute(sqlfilter(s))

  rr "<script language=vbscript> progress.style.top = 200 </script>" : response.flush
  s = "select count(id) as x " & mid(s, instr(s, " from shifts")) : s = left(s,instrrev(s, " order by")-1)
  rsc = 0 : rsrc = getonefield(s)
  if rsrc = 0 then du = errorend("��� ������ ��������� ������ ������ ������")
  c = 0 : cc = 0
  do until rs.eof
    c = c + 1 : cc = cc + 1
    rsc = rsc + 1 : rr "<script language=vbscript> progresstext.innerhtml = ""<b>~Processing~..</b> " & rsc & "/" & rsrc & """ : progressbar.style.width = " & fix(400 * rsc / rsrc) & " </script>" : response.flush
    if c = 1 then
      t = "" : if cc <> 1 then t = "page-break-before:always;"
      doc.writetext("<table width=100% style='direction:rtl; " & t & "' cellpadding=4 cellspacing=1 bgcolor=#aaaaaa><tr valign=top bgcolor=dddddd><td><nobr><b>" & title)
      csv.writetext(tocsv(title) & ",")
      d = d1 : do until d > d2 : t = translate("~weekday" & weekday(d) & "~ " & d) : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",") : d = dateadd("d", 1, d) : loop
    end if
    t = rs("title") & " " & rs("hour1") & "-" & rs("hour1b") : doc.writetext("<tr valign=top bgcolor=ffffff><td bgcolor=#eeeeee><nobr><b>" & t) : csv.writetext(vbcrlf & tocsv(t) & ",")
    d = d1
    do until d > d2
      s = "select workerid from workershifts"
      if instr(",techjet,osem,btl,isp,egged,", "," & specs & ",") > 0 then
        s = s & " where shiftid = " & rs("id")
      else
        s = s & " where (hour1 = '" & rs("hour1") & "' and hour1b = '" & rs("hour1b") & "')"
      end if
      s = s & " and date1 = " & sqldate(d)
      if getfield("action") <> "medreport" then
        sq = session("setshifts_lastlistsql")
        x = instr(sq, " order by") : if x > 0 then sq = left(sq, x - 1)
        x = instr(sq, " where ") : if x > 0 then sq = "select id from workers" & mid(sq, x)
        if sq = "" then sq = "select id from workers order by firstname,lastname" 'come from schedule
        ii = getidlist(sq)
        s = s & " and workerid in(" & ii & "-1)"
      end if

      if getfield("action") = "shiftworkersreport" then
        if instr(",caesarstone,lageen,", "," & specs & ",") > 0 then s = s & " and workerid in(" & showworkers & "-1)"
        ii = getidlist(s)
        s = "select firstname,lastname,phone1 from workers where id in(" & ii & "-1) order by firstname, lastname"
        set rs1 = conn.execute(sqlfilter(s))
        doc.writetext("<td><nobr>")
        csv.writetext("""")
        do until rs1.eof
          t = rs1("firstname") & " " & rs1("lastname") & " " & strfilter(rs1("phone1"), "0123456789")
          doc.writetext(t & "<br>") : csv.writetext(tocsv(t))
          rs1.movenext
          if not rs1.eof then csv.writetext(chr(10))
        loop
        csv.writetext(""",")

      elseif getfield("action") = "medreport" then
        s = s & " and workerid in(" & oo & "-1)"
        ii = getidlist(s)

        s = "select firstname,lastname,phone1 from workers where id in(" & ii & "-1) order by firstname, lastname"
        set rs1 = conn.execute(sqlfilter(s))
        doc.writetext("<td><nobr>")
        csv.writetext("""")
        do until rs1.eof
          t = rs1("firstname") & " " & rs1("lastname") & " " & strfilter(rs1("phone1"), "0123456789")
          doc.writetext(t & "<br>") : csv.writetext(tocsv(t))
          rs1.movenext
          if not rs1.eof then csv.writetext(chr(10))
        loop
        csv.writetext(""",")
      else
        s = replace(s, "workerid", "count(id) as x", 1,1,1)
        'if instr(",techjet,", "," & specs & ",") > 0 then s = s & " and foodsupplierid <> 0"
        set rs1 = conn.execute(sqlfilter(s))
        x = rs1("x") : if isnull(x) then x = 0
        doc.writetext("<td><nobr>" & x) : csv.writetext(tocsv(x) & ",")

        if specs = "osem" then
          if instr(",15,11,9,8,7,1,2,12,", "," & rs("id") & ",") > 0 then shiftgroups = addnumval(shiftgroups, "a" & d, x)
          if instr(",3,10,", "," & rs("id") & ",") > 0 then shiftgroups = addnumval(shiftgroups, "b" & d, x)
          if instr(",4,5,6,", "," & rs("id") & ",") > 0 then shiftgroups = addnumval(shiftgroups, "c" & d, x)
          if instr(",15,2,5,", "," & rs("id") & ",") > 0 then shiftgroups = addnumval(shiftgroups, "b" & d, x * 0.5)
        end if

      end if
      d = dateadd("d", 1, d)
    loop

    rs.movenext
    if c = 30 and getfield("action2") = "doc" then doc.writetext("</table><br>") : c = 0
  loop

  if specs = "osem" then
    t = "�� ������ �����" : doc.writetext("<tr valign=top bgcolor=ffffff><td bgcolor=#eeeeee><nobr><b style='color:#ff0000;'>" & t) : csv.writetext(tocsv(t) & ",")
    d = d1 : do until d > d2 : x = getval(shiftgroups,"a" & d) : doc.writetext("<td><nobr>" & x) : csv.writetext(tocsv(x) & ",") : d = dateadd("d", 1, d) : loop
    t = "�� ������ �������" : doc.writetext("<tr valign=top bgcolor=ffffff><td bgcolor=#eeeeee><nobr><b style='color:#ff0000;'>" & t) : csv.writetext(tocsv(t) & ",")
    d = d1 : do until d > d2 : x = getval(shiftgroups,"b" & d) : doc.writetext("<td><nobr>" & x) : csv.writetext(tocsv(x) & ",") : d = dateadd("d", 1, d) : loop
    t = "�� ������ �����" : doc.writetext("<tr valign=top bgcolor=ffffff><td bgcolor=#eeeeee><nobr><b style='color:#ff0000;'>" & t) : csv.writetext(tocsv(t) & ",")
    d = d1 : do until d > d2 : x = getval(shiftgroups,"c" & d) : doc.writetext("<td><nobr>" & x) : csv.writetext(tocsv(x) & ",") : d = dateadd("d", 1, d) : loop
  end if

  rr "<script language=vbscript> progress.style.top = -1000 </script>" : response.flush
  doc.writetext("</table>")
  doc.position = 0 : doc1 = doc.readtext(doc.size) : doc.close
  csv.position = 0 : csv1 = csv.readtext(csv.size) : csv.close
  session("htm") = translate(doc1)
  session("csv") = translate(csv1)

  if getfield("shiftcounterreportemail") <> "" then
    'f = "upload/shiftcounterreportemail_" & createpassword(6) & ".csv"
    'x = addlog(server.mappath(f), csv1)
    'subject = getparam("applicationdisplay") & "- " & title
    'body = "<a href=" & siteurl & f & ">��� ������</a>"
    'x = sendemail(getparam("siteemail") & "/", getfield("shiftcounterreportemail"), "", "", subject, body)
    subject = getparam("applicationdisplay") & " - " & title
    x = sendemail(getparam("siteemail"), getfield("shiftcounterreportemail"), "", "", subject, doc1)
  end if
  if getfield("action3") = "close" then rend

  if getfield("action2") <> "doc" then
    rr "<script language=vbscript> document.location = ""admin_orders.asp?action=sessioncsv"" </script>"
  else
    rr "<script language=vbscript> document.location = ""admin_orders.asp?action=sessionhtm"" </script>"
  end if

end if

if isdate(getfield("reportd1")) and isdate(getfield("reportd2")) then if cdate(getfield("reportd1")) > cdate(getfield("reportd2")) then du = errorend("���� ������� ����")


'---------------------foodreport simple - imi
if getfield("action") = "foodreportimi" then
  rclear
  totals = "" : repdis = ""
  set csv = Server.CreateObject("ADODB.Stream") : csv.open
  s = "select workershifts.workerid, workershifts.date1, workershifts.hour1, workershifts.hour1b, shifts.title as shifttitle, workers.code, workers.firstname, workers.lastname, workers.data1 as workerdata1, workers.siteid, units.title as unit, foodsuppliers.title as food, workershifts.data1 as workershiftdata1"
  s = s & " from (((workershifts"
  s = s & " left join workers on workershifts.workerid = workers.id)"
  s = s & " left join foodsuppliers on workershifts.foodsupplierid = foodsuppliers.id)"
  s = s & " left join units on workers.unitid = units.id)"
  s = s & " left join shifts on workershifts.shiftid = shifts.id"
  s = s & " where foodsupplierid > 0"
  s = s & " and date1 >= " & sqldate(getfield("reportd1"))
  s = s & " and date1 <= " & sqldate(getfield("reportd2"))
  if siteidfilter <> "0" then s = s & " and workers.siteid = " & siteidfilter
  if session("usergroup") <> "admin" then
    ii = getidlist("select id from workers where 0=0" & okreadsql("workers"))
    s = s & " and workers.id in(" & ii & "-1)"
  end if

  sq = session("setshifts_lastlistsql")
  if sq <> "" then
    x = instr(sq, " order by") : if x > 0 then sq = left(sq, x - 1)
    x = instr(sq, " where ") : if x > 0 then sq = "select id from workers" & mid(sq, x)
    ii = getidlist(sq)
    s = s & " and workerid in(" & ii & "-1)"
  end if

  if hasmatch("elbit,imi,", specs) then
    s = s & " order by units.title, workers.firstname, workers.lastname, workershifts.date1, workershifts.hour1"
  else
    s = s & " order by workers.firstname, workers.lastname, workershifts.date1, workershifts.hour1"
  end if
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    workerdata1 = rs("workerdata1") & ""
    distzone = getval(workerdata1,"distzone")
    section = tocsv("%" & distzone & "%" & rs("shifttitle") & "%" & rs("date1") & "%")
    if not hasmatch(repdis, section) then repdis = repdis & section & ","
    food = rs("food")
    da = rs("workershiftdata1") & ""
    a = ""
    a = a & tocsv("�" & section & "�") & ","
    a = a & tocsv(rs("code")) & ","
    a = a & tocsv(rs("firstname") & " " & rs("lastname")) & ","
    a = a & tocsv(rs("unit")) & ","
    a = a & tocsv(rs("date1")) & ","
    a = a & tocsv(rs("hour1")) & ","
    a = a & tocsv(rs("hour1b")) & ","
    a = a & tocsv(rs("shifttitle")) & ","
    a = a & tocsv(food) & ","
    totalthis = 1 : q = 1
    if specs = "alliance" then if left(food,2) = "99" then totals = addnumval(totals, section & "�70- ����� ����", 1) : totals = addnumval(totals, section & "�73- ����� ����", 1) : totalthis = 0 : q = 2
    if specs = "imi" and instr(food, "�����") + instr(food, "+") > 0 then q = 2
    a = a & q & ","
    a = a & vbcrlf
    if specs = "imi" and getfield("distzone") <> "" and getval(workerdata1,"distzone") <> getfield("distzone") then
      'nothing
    elseif specs = "imi" and instr(food, "�����") > 0 then
      csv.writetext(a)
      food = replace(food, "�����", "�����") : food = trim(food)
      totals = addnumval(totals, section & "�" & ctxt(food), 2)
    elseif specs = "imi" and instr(food, "+") > 0 then
      csv.writetext(a)
      food1 = trim(left(food,instr(food,"+")-1)) : totals = addnumval(totals, section & "�" & ctxt(food1), 1)
      food2 = trim(mid(food,instr(food,"+")+1)) : totals = addnumval(totals, section & "�" & ctxt(food2), 1)
    else
      csv.writetext(a)
      if totalthis = 1 then totals = addnumval(totals, section & "�" & ctxt(food), 1)
    end if
    rs.movenext
  loop

  csv.position = 0 : csv1 = csv.readtext(csv.size) : csv.close
  if specs = "imi" and gettitle("sites", session("usersiteid")) = "66" then csv1 = replace(csv1, "(��� 66)", "")
  if specs = "imi" and gettitle("sites", session("usersiteid")) = "30" then csv1 = replace(csv1, "(��� 30)", "")
  if specs = "imi" and gettitle("sites", session("usersiteid")) = "72" then csv1 = replace(csv1, "(��� 72)", "")
  csv1 = vbcrlf & csv1
  csv1 = sortcsv(csv1, 5, "text")
  csv1 = sortcsv(csv1, 8, "text")
  csv1 = sortcsv(csv1, 1, "text")

  a = repdis
  do until a = ""
    section = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    e = totals : tot = ""
    do until e = ""
      f = left(e,instr(e,"|")-1) : e = mid(e,instr(e,"|")+1)
      if left(f, len(section) + 1) = section & "�" then
        f = mid(f, instr(f,"�")+1) : f = replace(f,"=",",")
        tot = tot & f & vbcrlf
      end if
    loop
    x = instr(csv1, "�" & section & "�")
    tit = section : tit = mid(tit,2) : tit = left(tit, len(tit)-1) : tit = replace(tit, "%", " - ")
    sep = ""
    sep = sep & "@@@" & tit & vbcrlf
    sep = sep & "���� �����,��� ����,�� ����,�����,�����,���,����,�����,����,����," & vbcrlf
    csv1 = left(csv1, x - 1) & sep & mid(csv1,x)
    x = instrrev(csv1, "�" & section & "�") : x = instr(x, csv1, vbcrlf)
    csv1 = left(csv1, x-1) & vbcrlf & tot & vbcrlf & mid(csv1, x)
    csv1 = replace(csv1, section, mid(section,2, instr(2,section,"%")-2))
  loop
  csv1 = replace(csv1, "�", "")
  r = csv2table(csv1)
  'r = replace(r, "<table", "<table dir=rtl align=right style='font-family:arial;' cellpadding=5 cellspacing=5", 1,1,1)
  r = replace(r, "<tr><td>@@@", "</table><table dir=rtl style='font-family:arial; page-break-before:always; width:100%;' cellpadding=5 cellspacing=5><tr bgcolor=#bbbbbb><td colspan=99 style='font-size:20;'>&nbsp;")
  csv1 = replace(csv1, "@@@", "")
  r = replace(r, "<tr><td>���� �����<td>", "<tr bgcolor=#dddddd><td>���� �����<td>")
  r = "<div dir=rtl style='font-family:arial;'><b>��� ������ ���� - " & getfield("reportd1") & " - " & getfield("reportd2") & "</b><br>" & r

  if getfield("email") <> "" then du = sendemail(getparam("siteemail"), getfield("email"), "", "", "Moovex - Food Report", r)
  if getfield("output") = "screen" then
    rr r
    rend
  else'csv
    response.ContentType = "application/csv"
    response.AddHeader "Content-Disposition", "filename=1.csv"
    response.write csv1
  end if
  rend
end if

'---------------------lumenis foodreport
if specs = "lumenis" and getfield("action") = "foodreport" then
  rclear
  totals = ""
  set csv = Server.CreateObject("ADODB.Stream") : csv.open
  s = "select workershifts.workerid, workershifts.date1, workershifts.hour1, workershifts.hour1b, shifts.title as shifttitle, workers.code, workers.firstname, workers.lastname, workers.data1 as workerdata1, workers.siteid, units.title as unit, foodsuppliers.title as food, workershifts.data1 as workershiftdata1"
  s = s & " from (((workershifts"
  s = s & " left join workers on workershifts.workerid = workers.id)"
  s = s & " left join foodsuppliers on workershifts.foodsupplierid = foodsuppliers.id)"
  s = s & " left join units on workers.unitid = units.id)"
  s = s & " left join shifts on workershifts.shiftid = shifts.id"
  s = s & " where foodsupplierid > 0"
  s = s & " and date1 >= " & sqldate(getfield("reportd1"))
  s = s & " and date1 <= " & sqldate(getfield("reportd2"))
  if siteidfilter <> "0" then s = s & " and workers.siteid = " & siteidfilter
  if session("usergroup") <> "admin" then
    ii = getidlist("select id from workers where 0=0" & okreadsql("workers"))
    s = s & " and workers.id in(" & ii & "-1)"
  end if
  s = s & " order by workers.firstname, workers.lastname, workershifts.date1, workershifts.hour1"
  set rs = conn.execute(sqlfilter(s))
  t = "���� �����,��� ����,�� ����,�����,�����,��� ���,�����,�����,�����,�����," & vbcrlf
  csv.writetext(t)
  do until rs.eof
    workerdata1 = rs("workerdata1") & ""
    food = rs("food")
    da = rs("workershiftdata1") & ""
    totalthis = 1 : q = 1
    a = ""
    a = a & tocsv(getval(workerdata1,"distzone")) & ","
    a = a & tocsv(rs("code")) & ","
    a = a & tocsv(rs("firstname") & " " & rs("lastname")) & ","
    a = a & tocsv(rs("unit")) & ","
    a = a & tocsv(rs("date1")) & ","
    a = a & tocsv(getval(da, "foodoptionsmain")) & ","
    a = a & tocsv(getval(da, "foodoptionscontents")) & ","
    a = a & tocsv(getval(da, "foodoptionssauces")) & ","
    a = a & tocsv(getval(da, "foodoptionsvegetables")) & ","
    a = a & tocsv(getval(da, "remarks")) & ","
    a = a & vbcrlf
    csv.writetext(a)
    'if totalthis = 1 then totals = addnumval(totals, ctxt(food), 1)
    if totalthis = 1 then totals = addnumval(totals, "��""�", 1)
    rs.movenext
  loop
  csv.position = 0 : csv1 = csv.readtext(csv.size) : csv.close
  csv1 = sortcsv(csv1, 1, "text")

  response.clear
  t = totals : t = replace(t, "|", vbcrlf) : t = replace(t,"=",",") : csv1 = csv1 & t
  if getfield("email") <> "" then
    r = csv2table(csv1)
    r = replace(r, "<tr", "<tr bgcolor=#dddddd", 1, 1, 1)
    du = sendemail(getparam("siteemail"), getfield("email"), "", "", "Moovex - Food Report", r)
  end if
  if getfield("output") = "screen" then
    rr "<div style='direction:rtl; font-family:arial;'><b>��� ������ ���� - " & getfield("reportd1") & " - " & getfield("reportd2") & "</b></div><br>"
    a = csv2table(csv1)
    a = replace(a, "<table", "<table align=right dir=rtl style='font-family:arial;' cellpadding=5 cellspacing=5", 1,1,1)
    a = replace(a, "<tr", "<tr bgcolor=#dddddd", 1,1,1)
    rr a
    rend
  else'csv
    response.ContentType = "application/csv"
    response.AddHeader "Content-Disposition", "filename=1.csv"
    response.write csv1
  end if
  rend
end if

'---------------------foodreport simple + alliance
if getfield("action") = "foodreport" then
  rclear
  totals = ""
  reportd1 = getfield("reportd1") : if left(reportd1,1) = "d" then reportd1 = dateadd("d", mid(reportd1,2), date)
  reportd2 = getfield("reportd2") : if left(reportd2,1) = "d" then reportd2 = dateadd("d", mid(reportd2,2), date)

  set csv = Server.CreateObject("ADODB.Stream") : csv.open
  s = "select workershifts.workerid, workershifts.date1, workershifts.hour1, workershifts.hour1b, shifts.title as shifttitle, workers.code, workers.firstname, workers.lastname, workers.data1 as workerdata1, workers.siteid, units.title as unit, foodsuppliers.title as food, workershifts.data1 as workershiftdata1"
  s = s & " from (((workershifts"
  s = s & " left join workers on workershifts.workerid = workers.id)"
  s = s & " left join foodsuppliers on workershifts.foodsupplierid = foodsuppliers.id)"
  s = s & " left join units on workers.unitid = units.id)"
  s = s & " left join shifts on workershifts.shiftid = shifts.id"
  s = s & " where foodsupplierid > 0"
  s = s & " and date1 >= " & sqldate(reportd1)
  s = s & " and date1 <= " & sqldate(reportd2)
  if siteidfilter <> "0" then s = s & " and workers.siteid = " & siteidfilter
  if session("userid") = "999999" and getfield("siteid") <> "" then s = s & " and workers.siteid = " & getfield("siteid")
  if session("usergroup") <> "admin" then
    ii = getidlist("select id from workers where 0=0" & okreadsql("workers"))
    s = s & " and workers.id in(" & ii & "-1)"
  end if
  if hasmatch("elbit,", specs) then
    s = s & " order by units.title, workers.firstname, workers.lastname, workershifts.date1, workershifts.hour1"
  else
    s = s & " order by workers.firstname, workers.lastname, workershifts.date1, workershifts.hour1"
  end if
  set rs = conn.execute(sqlfilter(s))
  if getfield("foodreportstyle") = "alliance" then
    t = "HLCARD,HLEMP,HLDTI,HLBSTR,HLSML," & vbcrlf & "Employee Card No.,Employee No.,Date,Start time,Semel," & vbcrlf
    csv.writetext(t)
    do until rs.eof
      a = ""
      a = a & tocsv(rs("code")) & ","
      a = a & tocsv(rs("code")) & "," 'they want it twice!
      a = a & tocsv(rs("date1")) & ","
      a = a & tocsv(rs("hour1")) & ","
      t = rs("food") : t = left(t,2) : if t = "99" then t = "��"
      a = a & tocsv(t) & ","
      a = a & vbcrlf
      if instr(rs("food"), "����") > 0 or t = "��" then a = a & a : a = replace(a, "��","70",1, 1, 1) : a = replace(a, "��","73",1, 1, 1)
      csv.writetext(a)
      rs.movenext
    loop
  else
    t = "��� ����,�� ����,�����,�����,���,����,�����,����,����,"
    if showdistzone = "on" then t = t & "���� �����,"
    if specs = "lumenis" then t = t & "��� ���,�����,�����,�����,�����,"
    t = t & vbcrlf
    csv.writetext(t)
    do until rs.eof
      workerdata1 = rs("workerdata1") & ""
      food = ctxt(trim(rs("food") & ""))
      da = rs("workershiftdata1") & ""
      a = ""
      a = a & tocsv(rs("code")) & ","
      a = a & tocsv(rs("firstname") & " " & rs("lastname")) & ","
      a = a & tocsv(rs("unit")) & ","
      a = a & tocsv(rs("date1")) & ","
      a = a & tocsv(rs("hour1")) & ","
      a = a & tocsv(rs("hour1b")) & ","
      a = a & tocsv(rs("shifttitle")) & ","
      a = a & tocsv(food) & ","
      totalthis = 1 : q = 1
      if specs = "alliance" then if left(food,2) = "99" then totals = addnumval(totals, "70- ����� ����", 1) : totals = addnumval(totals, "73- ����� ����", 1) : totalthis = 0 : q = 2
      if specs = "imi" and instr(food, "�����") + instr(food, "+") > 0 then q = 2
      a = a & q & ","
      if showdistzone = "on" then a = a & tocsv(getval(workerdata1,"distzone")) & ","
      if specs = "lumenis" then
        a = a & tocsv(getval(da, "foodoptionsmain")) & ","
        a = a & tocsv(getval(da, "foodoptionscontents")) & ","
        a = a & tocsv(getval(da, "foodoptionssauces")) & ","
        a = a & tocsv(getval(da, "foodoptionsvegetables")) & ","
        a = a & tocsv(getval(da, "remarks")) & ","
      end if
      a = a & vbcrlf
      if specs = "imi" and getfield("distzone") <> "" and getval(workerdata1,"distzone") <> getfield("distzone") then
        'nothing
      elseif specs = "imi" and instr(food, "�����") > 0 then
        csv.writetext(a)
        food = replace(food, "�����", "�����") : food = trim(food)
        totals = addnumval(totals, food, 2)
      elseif specs = "imi" and instr(food, "+") > 0 then
        csv.writetext(a)
        food1 = trim(left(food,instr(food,"+")-1)) : totals = addnumval(totals, food1, 1)
        food2 = trim(mid(food,instr(food,"+")+1)) : totals = addnumval(totals, food2, 1)
      else
        csv.writetext(a)
        if totalthis = 1 then totals = addnumval(totals, ctxt(food), 1)
      end if
      rs.movenext
    loop
  end if

  csv.position = 0 : csv1 = csv.readtext(csv.size) : csv.close
  response.clear
  t = totals : t = replace(t, "|", vbcrlf) : t = replace(t,"=",",") : csv1 = csv1 & t
  if getfield("email") <> "" then
    r = csv2table(csv1)
    r = replace(r, "<tr", "<tr bgcolor=#dddddd", 1, 1, 1)
    du = sendemail(getparam("siteemail"), getfield("email"), "", "", "Moovex - Food Report", r)
  end if
  if getfield("output") = "screen" then
    rr "<div style='direction:rtl; font-family:arial;'><b>��� ������ ���� - " & reportd1 & " - " & reportd2 & "</b></div><br>"
    a = csv2table(csv1)
    a = replace(a, "<table", "<table align=right dir=rtl style='font-family:arial;' cellpadding=5 cellspacing=5", 1,1,1)
    a = replace(a, "<tr", "<tr bgcolor=#dddddd", 1,1,1)
    rr a
    rend
  else'csv
    response.ContentType = "application/csv"
    response.AddHeader "Content-Disposition", "filename=1.csv"
    response.write csv1
  end if
  rend
end if

'----reportsiteid, foodsql
if instr(getfield("action"), "report") > 0 then
  s = "select id,title from foodsuppliers where 0=0"
  if specs = "imi" and siteidfilter <> "0" then s = s & " and title like '%��� " & gettitle("sites", siteidfilter) & "%'"
  s = s & " order by title"
  foodsql = s
end if

'---------------------foodreport2
if getfield("action") = "foodreport2" then
  rclear
  rr "<div style='direction:rtl; font-family:arial;'><b>��� ������ ������ - " & getfield("reportd1") & " - " & getfield("reportd2") & "</b></div><br>"
  doublefoods = getidlist("select id from foodsuppliers where instr(title, '����') > 0")
  if specs = "tadbik" then doublefoods = ""
  if getfield("foodreport2email") <> "" then x = putparam("-1000|foodreport2email", getfield("foodreport2email"))
  set csv = Server.CreateObject("ADODB.Stream") : csv.open
  set doc = Server.CreateObject("ADODB.Stream") : doc.open
  doc.writetext("<body style='direction:rtl;'>")
  title = "��� ������ ������ - " & getfield("d")
  s = "select units.id,units.title,bunits.title as bunit from units left join bunits on units.bunitid = bunits.id " & okreadsql("units")
  s = s & " order by bunits.title, units.title"
  s = replace(s," and "," where ",1,1,1)
  set rs = conn.execute(sqlfilter(s))
  c = 0 : cc = 0 : fff = "" : totals = "" : totalsb = ""
  do until rs.eof
    c = c + 1 : cc = cc + 1
    theid = rs("id")
    thetitle = rs("title")
    thebunit = rs("bunit")
    rs.movenext
    if c = 1 then
      t = "" : if cc <> 1 then t = "page-break-before:always;"
      doc.writetext("<table width=100% style='direction:rtl; " & t & "' cellpadding=4 cellspacing=1 bgcolor=#aaaaaa><tr bgcolor=#dddddd><td width=300><nobr><b>" & title & "<td><nobr><b>����� �����")
      csv.writetext(tocsv(title) & ",����� �����,")
      set rs1 = conn.execute(sqlfilter(foodsql))
      do until rs1.eof
        if cc = 1 then fff = fff & rs1("id") & ","
        t = rs1("title") : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",")
        rs1.movenext
      loop
      t = translate("~Total~") : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",")
    end if

    t = thetitle : add1 = "<tr bgcolor=#ffffff><td><nobr>" & t : add2 = vbcrlf & tocsv(t) & ","
    t = thebunit : add1 = add1 & "<td><nobr>" & t : add2 = add2 & tocsv(t) & ","
    ww = getidlist("select id from workers where unitid = " & theid)
    ff = fff : tot = 0
    do until ff = ""
      f = left(ff,instr(ff,",")-1) : ff = mid(ff, instr(ff,",")+1)
      s = "select count(id) as x from workershifts"
      s = s & " where foodsupplierid=" & f
      s = s & " and date1 >= " & sqldate(getfield("reportd1"))
      s = s & " and date1 <= " & sqldate(getfield("reportd2"))
      s = s & " and workerid in(" & ww & "-1)"
      set rs1 = conn.execute(sqlfilter(s))
      t = rs1("x") : if instr("," & doublefoods, "," & f & ",") > 0 then t = t * 2
      tot = tot + t : add1 = add1 & "<td><nobr>" & t : add2 = add2 & tocsv(t) & ","
      totals = setval(totals, f, cdbl("0" & getval(totals, f)) + t)
      totalsb = setval(totalsb, f, cdbl("0" & getval(totalsb, f)) + t)
      totb = totb + t
    loop
    add1 = add1 & "<td bgcolor1=#dddddd><nobr>" & tot : add2 = add2 & tocsv(tot) & ","
    if tot > 0 then doc.writetext(add1) : csv.writetext(add2)

    nextbunit = "." : if not rs.eof then nextbunit = rs("bunit")
    if thebunit <> nextbunit and cstr(totalsb) <> "0" and totb > 0 then
      t = "--- ��""� �����" : doc.writetext("<tr bgcolor=#eeeeee><td><b>" & t) : csv.writetext(vbcrlf & tocsv(t) & ",")
      t = thebunit : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",")
      ff = fff
      do until ff = ""
        f = left(ff,instr(ff,",")-1) : ff = mid(ff, instr(ff,",")+1)
        t = getval(totalsb, f) : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",")
      loop
      t = totb : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",")
      totalsb = "" : totb = 0
    end if
    lastbunit = thebunit

    if specs = "techjet" and c = 30 and getfield("action2") = "doc" then doc.writetext("</table><br>") : c = 0
  loop

  '---totals
  doc.writetext("<tr bgcolor=#dddddd><td><b>~Total~<td>") : csv.writetext(vbcrlf & tocsv(translate("~Total~")) & ",,")
  ff = fff : tot = 0
  do until ff = ""
    f = left(ff,instr(ff,",")-1) : ff = mid(ff, instr(ff,",")+1)
    t = getval(totals, f) : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",")
    tot = tot + t
  loop
  t = tot : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",")
  doc.writetext("</table>")

  doc.position = 0 : doc1 = doc.readtext(doc.size) : doc.close
  csv.position = 0 : csv1 = csv.readtext(csv.size) : csv.close
  rr doc1

  if getfield("foodreport2email") <> "" then
    'f = "upload/foodreport2email_" & createpassword(6) & ".csv"
    'x = addlog(server.mappath(f), csv1)
    'subject = getparam("applicationdisplay") & "- ��� ������"
    'body = "<a href=" & siteurl & f & ">��� ������</a>"
    'x = sendemail(getparam("siteemail") & "/", getfield("foodreport2email"), "", "", subject, body)
    subject = getparam("applicationdisplay") & "- ��� ������"
    x = sendemail(getparam("siteemail") & "/", getfield("foodreport2email"), "", "", subject, doc1)
  end if

  if getfield("action2") <> "doc" then
    response.clear
    response.ContentType = "application/csv"
    response.AddHeader "Content-Disposition", "filename=1.csv"
    response.write csv1
  end if

  rend
end if

'---------------------distzonereport - imi
if getfield("action") = "distzonereport" then
  rclear
  doublefoods = getidlist("select id from foodsuppliers where instr(title, '����') > 0")
  rr "<body style='direction:rtl; font-family:arial;'>"
  rr "<b>��� ������ ������ ����� - " & getfield("reportd1") & " �� " & getfield("reportd2") & "</b>"
  rr "<table cellpadding=5 cellspacing=1><tr bgcolor=cccccc><td>"
  a = getfieldlist(replace(foodsql, "id,title", "title",1,1,1)) : a = left(a,len(a)-1) : a = "," & a : a = replace(a,",","<td><b>") : rr a
  rr "<td><b>��""�"
  tots = "" : a = distzones
  if specs = "imi" then a = getval(distzonesparts, siteidfilter)
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr "<tr><td bgcolor=cccccc><b>" & b
    s = "select id from workers where instr('' & cstr('' & workers.data1), '|distzone=" & b & "|') > 0"
    if siteidfilter <> "0" then s = s & " and workers.siteid = " & siteidfilter
    ii = getidlist(s)
    ff = getidlist(foodsql)
    totline = 0
    do until ff = ""
      f = left(ff,instr(ff,",")-1) : ff = mid(ff,instr(ff,",")+1)
      s = "select count(id) as x from workershifts where workerid in(" & ii & "-99) and foodsupplierid = " & f
      s = s & " and date1 >= " & sqldate(getfield("reportd1"))
      s = s & " and date1 <= " & sqldate(getfield("reportd2"))
      x = cdbl("0" & getonefield(s)) : if instr("," & doublefoods, "," & f & ",") > 0 then x = x * 2
      totline = totline + x : tots = addnumval(tots, f, x)
      rr "<td>" & x
    loop
    rr "<td><b>" & totline
  loop
  rr "<tr bgcolor=#dddddd><td><b>��""�"
  ff = getidlist(foodsql)
  x = 0
  do until ff = ""
    f = left(ff,instr(ff,",")-1) : ff = mid(ff,instr(ff,",")+1)
    t = getval(tots,f) : if t = "" then t = "0"
    rr "<td><b>" & t
    x = x + cdbl(t)
  loop
  rr "<td style='color:#ff0000;'><b>" & x

  rr "</table>"
  rend
end if

'------------------countworkerfoods
if getfield("action") = "countworkerfoods" then
  doublefoods = getidlist("select id from foodsuppliers where instr(title, '����') > 0")
  s = "select workerid,foodsupplierid from workershifts where foodsupplierid > 0"
  s = s & " and date1 >= " & sqldate(getfield("reportd1"))
  s = s & " and date1 <= " & sqldate(getfield("reportd2"))
  set rs = conn.execute(sqlfilter(s))
  Set wfdict = Server.CreateObject("Scripting.Dictionary")
  do until rs.eof
    x = 1 : if instr("," & doublefoods, "," & rs("foodsupplierid") & ",") > 0 then x = 2
    wfdict(cstr(rs("workerid"))) = cdbl("0" & wfdict(cstr(rs("workerid")))) + x
    rs.movenext
  loop
  rclear
  response.ContentType = "application/csv"
  response.AddHeader "Content-Disposition", "filename=1.csv"
  rr "�� ����,�� �����,���� ����,�����,���� ������," & vbcrlf
  s = "select id,firstname,lastname,code,unitid from workers where lastname <> '_TEMPLATE_'" & okreadsql("workers") & " order by firstname, lastname, code"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    rr tocsv(rs("firstname")) & ","
    rr tocsv(rs("lastname")) & ","
    rr tocsv(rs("code")) & ","
    rr tocsv(gettitle("units", rs("unitid"))) & ","
    rr wfdict(cstr(rs("id"))) & ","
    rr vbcrlf
    rs.movenext
  loop
  rend
end if

'--------------------------reporttama
if getfield("action") = "reporttama" then
  rclear
  rr "<body style='direction:rtl; font-family:arial;'>"
  rr "<b>��� ������ - " & getfield("reportd1") & " �� " & getfield("reportd2") & "</b>"
  rr "<table cellpadding=5 cellspacing=1>"
  datesline = ""
  d = cdate(getfield("reportd1"))
  datesline = datesline & "<tr bgcolor=#cccccc><td><nobr>"
  do until d > cdate(getfield("reportd2"))
    datesline = datesline & "<td>" & d & " ~weekday" & weekday(d) & "~"
    d = dateadd("d", 1, d)
  loop
  s = "select distinct shiftid from workershifts where 0=0"
  s = s & " and date1 >= " & sqldate(getfield("reportd1"))
  s = s & " and date1 <= " & sqldate(getfield("reportd2"))
  ii = getidlist(s)

  s = "select id,title,hour1,hour1b from shifts where id in(" & ii & ") order by hour1, hour1b"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    rr datesline
    rr "<tr valign=top><td bgcolor=#cccccc><nobr>" & rs("title") & "<br>" & rs("hour1") & " - " & rs("hour1b")
    d = cdate(getfield("reportd1"))
    do until d > cdate(getfield("reportd2"))
      rr "<td>"
      ww = getidlist("select workerid from workershifts where date1 = " & sqldate(d) & " and shiftid = " & rs("id"))
      s = "select workers.color, workers.firstname, workers.lastname, units.title as unit"
      s = s & " from workers left join units on workers.unitid = units.id"
      s = s & " where workers.id in(" & ww & "-1)"
      if session("setshifts_workertypeids") <> "" then s = s & " and workers.workertypeid in(" & session("setshifts_workertypeids") & "-1)"
      s = s & " order by units.title, workers.firstname, workers.lastname"
      set rs1 = conn.execute(sqlfilter(s))
      rr "<table>"
      lastunit = "" : c = 0
      do until rs1.eof
        unit = rs1("unit")
        color = rs1("color")
        n = rs1("firstname") & " " & rs1("lastname")
        rs1.movenext
        c = c + 1
        color = lcase(color) : if color = "" or color = "ffffff" then color = "dddddd"
        rr "<tr>"
        rr "<td style='width:150; overflow:hidden; background:#" & color & "'>" & n
        if rs1.eof or (lastunit <> "" and unit <> lastunit) then rr "<tr><td><b>" & c & "</b> - " & unit & "<tr><td>&nbsp;" : c = 0
        lastunit = unit
      loop
      rr "</table>"
      d = dateadd("d", 1, d)
    loop
    rs.movenext
  loop
  rr "</table>"
  rend
end if

'--------------------loadcsv
if session("usergroup") = "admin" and trim(getfield("loadcsv")) <> "" then

  server.scripttimeout = 100000 'seconds
  set thefile = fs.opentextfile(server.mappath(getfield("loadcsv")))
  csvdd = thefile.readall & vbcrlf
  thefile.close

  '-------get worker codes
  wc = ""
  set st = Server.CreateObject("ADODB.Stream") : st.open
  sq = "select id,code from workers where firstname <> ''"
  sq = sq & okreadsql("workers")
  set rs1 = conn.execute(sqlfilter(sq))
  do until rs1.eof
    st.writetext(rs1("code") & "=" & rs1("id") & "|")
    rs1.movenext
  loop
  st.position = 0 : wc = "|" & st.readtext(st.size) : st.close

  '-------first column
  c = 0 : datecols = "" : codecol = ""
  a = left(csvdd,instr(csvdd,vbcrlf)-1) & "," : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
  do until a = ""
    c = c + 1
    b = trim(lcase(left(a,instr(a,",")-1))) : a = mid(a,instr(a,",")+1)
    if b = lcase(translate("~code~")) or instr(",���,��� ����,��� �����,code,worker code,workers code,worker`s code,", "," & b & ",") > 0 then codecol = c
    b = strfilter(b,"0123456789/.")
    if isdate(b) then datecols = setval(datecols, c, b)
  loop

  c = 0
  do until csvdd = ""
    a = left(csvdd,instr(csvdd,vbcrlf)-1) & ",,,," : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    col = 0 : date1 = "" : workerid = "" : hour1 = "" : hour1b = ""
    for i = 1 to countstring(a,",")
      col = col + 1
      if instr(a,",") = 0 then exit for
      if left(a,1) = """" and instr(2,a,"""") >= 2 then
        x = 1
        do until x >= len(a)
          x = x + 1
          if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
          if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
        loop
        b = left(a,x) : a = mid(a,x+2)
        b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
      else
        b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
      end if
      b = trim(chtm(b))
      '---------
      if cstr(col) = cstr(codecol) then
        if b <> "" then workerid = getval(wc, b)
        if workerid = "" and b <> "" then du = showerror("~Worker Not Found~ " & b)
      end if
      if getval(datecols, col) <> "" and workerid <> "" then
        date1 = getval(datecols, col)
        if instr(lcase(b),lcase(translate("~Fixed~"))) + instr(lcase(b),"fixed") + instr(lcase(b),"����") > 0 then b = ""
        if session("setshifts_addition") <> "on" then
          s = "delete * from workershifts where workerid = " & workerid & " and date1 = " & sqldate(date1)
          closers(rs) : set rs = conn.execute(sqlfilter(s))
        end if
        if b <> "" then
          ways = ""
          b = replace(b,"�����","��")
          hour1 = getfromto(" " & b & " ", "", translate("~From~ "), " ", false, false) : if ishour(hour1) then ways = ways & "A" else hour1 = "00:00"
          hour1b = getfromto(" " & b & " ", "", translate("~To~ "), " ", false, false) : if ishour(hour1b) then ways = ways & "B" else hour1b = "00:00"
          if instr(b,"��� ����") + instr(b,"����") + instr(b,"No Ride") > 0 then ways = "X"
          if ways <> "" then
            s = "insert into workershifts(date0,date1,workerid,ways,hour1,hour1b,foodsupplierid,from1,to1,courseid,shiftid,data1,userid0) values("
            s = s & sqldate(now) & "," & sqldate(date1) & "," & workerid & ",'" & ways & "','" & hour1 & "','" & hour1b & "',0,0,0,0," & getfield("shiftid") & ",''," & session("userid") & ")"
            closers(rs) : set rs = conn.execute(sqlfilter(s))
            c = c + 1
          end if
        end if
      end if
    next
  loop
  if msg = "" then msg = c & " ~Shifts were updated~"
end if

'----------------setdone---------------------------------------------------
if request_action = "setdone" then
  s = "insert into workershifts(date0,date1,workerid,data1,userid0) values("
  s = s & sqldate(now) & "," & sqldate(getfield("d")) & ", -#USERID#,''," & session("userid") & ")"
  t = replace(s, "#USERID#", session("userid"))
  closers(rs) : set rs = conn.execute(sqlfilter(t))

  if specs = "adama" then
    ii = getidlist("select id from buusers where id <> " & session("userid") & " and unitids = '" & session("userunitids") & "'")
    do until ii = ""
      i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
      t = replace(s, "#USERID#", i)
      closers(rs) : set rs = conn.execute(sqlfilter(t))
    loop
  end if

  if hasmatch(specs, "sanmina,isp,") then
    subject = getuser(session("userid")) & " ���� ���� ������ ������ " & getfield("d") & " ��� ����� ������ �� ��� ���� �� �����"
    body = ""
    s = "select email from buusers where usergroup in('AdminAssistant','Admin')"
    uu = session("userunitids") : a = ""
    do until uu = ""
      u = left(uu,instr(uu,",")-1) : uu = mid(uu,instr(uu,",")+1)
      a = a & " + instr(cstr(',' & unitids), '," & u & ",')"
    loop
    if a <> "" then s = s & " and " & mid(a,4) & " > 0"
    ee = getfieldlist(s)
    du = sendemail(getparam("siteemail"), ee, "", "", subject, body)
  end if

end if

'----------------submit---------------------------------------------------
if request_action = "shiftsubmit" then

  'if specs = "flextronics" then
  '  if getfield("foodsupplierid") <> "" and getfield("foodsupplierid") <> "0" and getfield("hour1b") <> "19:15" then
  '    du = errorend("_back���� ����� ��� ���� �� ������� ��������� � 19:15")
  '  end if
  'end if

  a = getfield("ids") : cc = 0 : cctry = 0 : reporteddays = ""
  ways = request_ways : if ways = "" then ways = "AB"
  shiftid = session("setshifts_shiftid") : mainshiftid = shiftid

  shiftdata1 = "" : mainshiftdata1 = shiftdata1
  shiftways = request_ways : mainshiftways = request_ways
  shiftdays = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays") : mainshiftdays = shiftdays
  s = "select data1,ways,days from shifts where id = 0" & shiftid
  set rs = conn.execute(sqlfilter(s))
  if not rs.eof then
    shiftdata1 = rs("data1") : mainshiftdata1 = shiftdata1
    shiftways = rs("ways") : mainshiftways = shiftways
    shiftdays = rs("days") : mainshiftdays = shiftdays
  end if

  if specs = "rafael" and request_from1 <> "0" and getfield("altaddress") = "" then du = errorend("_back���� ������ ����� ������ ��� ����� ���� ����")
  if specs = "rafael" and request_from1 = "0" and getfield("altaddress") <> "" then du = errorend("_back���� ������ ���� ���� ��� ����� ����� ������")

  do until a = ""
    wid = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)

    d = d1
    do until d > d2
      foodsupplierid = ""
      fromtemplate = "" : if session("setshifts_templateid") <> "" and session("setshifts_templateid") <> "0" then fromtemplate = "on"
      k = "u" & wid & "d" & replace(d,"/","_")

      if getfield("action2") = "delfood" then
        if getfield(k) = "on" then
          s = "update workershifts set foodsupplierid = 0 where workerid = " & wid & " and date1 = " & sqldate(d)
          set rs = conn.execute(sqlfilter(s))
        end if

      elseif getfield("action2") = "onlyfood" then
        if getfield(k) = "on" then
          t = getonefield("select id from workershifts where workerid = " & wid & " and date1 = " & sqldate(d))
          if t = "" then
            s = "insert into workershifts(date0,date1,workerid,ways,hour1,hour1b,foodsupplierid,from1,to1,courseid,shiftid,data1,userid0) values("
            s = s & sqldate(now) & "," & sqldate(d) & "," & wid & ",'X','00:00','00:00',0,0,0,0,0,''," & session("userid") & ")"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
          end if
          s = "update workershifts set foodsupplierid = " & getfield("foodsupplierid") & " where workerid = " & wid & " and date1 = " & sqldate(d)
          set rs = conn.execute(sqlfilter(s))
        end if

      elseif getfield("action2") = "onlyto1" then
        if getfield(k) = "on" then
          t = getonefield("select data1 from workershifts where workerid = " & wid & " and date1 = " & sqldate(d))
          t = setval(t, "manually", "on")
          s = "update workershifts set to1 = " & request_to1 & ", data1 = '" & t & "', date0 = " & sqldate(now) & " where workerid = " & wid & " and date1 = " & sqldate(d)
          set rs = conn.execute(sqlfilter(s))
        end if

      elseif getfield("action2") = "delall" or getfield(k) = "on" then
        cctry = cctry + 1
        shiftid = mainshiftid : shiftdata1 = mainshiftdata1 : shiftways = mainshiftways : shiftdays = mainshiftdays
        specificfood = ""
        ways = request_ways
        hour1 = "00:00" : if getfield("hour1") <> "" then hour1 = getfield("hour1")
        hour1b = "00:00" : if getfield("hour1b") <> "" then hour1b = getfield("hour1b")

        x = 0
        if getfield("workershift" & k) <> "" then shiftid = getfield("workershift" & k) : x = 1

        if request_ways = "X" or instr(request_ways, "-") > 0 then
          t = getonefield("select shiftid from workershifts where workerid = " & wid & " and date1 = " & sqldate(d))
          if t <> "" then shiftid = t : x = 1
        end if


'!!!
        if request_ways = "A" or request_ways = "-A" or request_ways = "/A" then
          t = getonefield("select shiftid from workershifts where workerid = " & wid & " and date1 = " & sqldate(d) & " and ways = 'A'")
          if t <> "" then shiftid = t : x = 1
        end if
        if request_ways = "B" or request_ways = "-B" or request_ways = "/B" then
          t = getonefield("select shiftid from workershifts where workerid = " & wid & " and date1 = " & sqldate(d) & " and ways = 'B'")
          if t <> "" then shiftid = t : x = 1
        end if



        if x = 1 then
          sq = "select hour1,hour1b,ways,days,data1 from shifts where id = " & shiftid
          set rs1 = conn.execute(sqlfilter(sq))
          if not rs1.eof then
            hour1 = rs1("hour1")
            hour1b = rs1("hour1b")
            if request_ways <> "X" and instr(request_ways, "-") = 0 then ways = rs1("ways")
            shiftdata1 = rs1("data1")
            shiftways = rs1("ways")
            shiftdays = rs1("days")
            t = getval(shiftdata1, "foodsupplierids") : foodsupplierid = getlistitem(t,1)
          end if
        end if

        if specs = "rafael" then
          if shiftid <> "" and shiftid <> "0" then
            if (ways = "AB" or ways = "A") and instr(shiftways, "A") = 0 then du = errorend("_back����� �� �� ����� ����� �����")
            if (ways = "AB" or ways = "B") and instr(shiftways, "B") = 0 then du = errorend("_back����� �� �� ����� ����� �����")
          end if
          if session("usergroup") = "manager" and instr(ways, "A") > 0 then du = checkphones(wid & ",")
        end if
        'if shiftid <> "" and shiftid <> "0" and instr(ways, "-") = 0 then t = isshiftava(shiftid, d) : if t <> "" then du = errorend("_back" & t)
        if shiftid <> "" and shiftid <> "0" then t = isshiftava(shiftid, d) : if t <> "" then du = errorend("_back" & t)

        if fromtemplate = "on" then
          shiftid = "0"
          session("setshifts_ways") = "AB"
          session("setshifts_addition") = ""
          x = dupshifts(wid & ",", session("setshifts_templateid"), "", "", d, d, "", "")
          cc = cc + 1

        elseif instr(shiftdays,getweekday(d)) = 0 and instr("X-", ways) = 0 then
          if instr(reporteddays,getweekday(d)) = 0 then
            reporteddays = reporteddays & weekday(d)
            du = showerror("~Cannot set the selected shift to~ ~weekday" & getweekday(d) & "~")
          end if

        else
          z = 1 : if enableretro = "on" or hasmatch(specs, "brom,alliance,") then z = 0
          t = ""
          if instr(ways, "A") then t = t & "A"
          if instr(ways, "B") then t = t & "B"
          getproduceduntilways = t
          getproduceduntilsiteid = getonefield("select siteid from workers where id = 0" & wid)
          if crosssite = "on" and session("setshifts_siteid2") <> "0" then getproduceduntilsiteid = session("setshifts_siteid2")
          getproduceduntilshiftid = shiftid
          getproduceduntilunitid = getonefield("select unitid from workers where id = 0" & wid)
          produceduntil = getproduceduntil

          if z = 1 and (hasmatch(ways, ",X,-,-A,-B,/A,/B,") or session("setshifts_addition") <> "on") then
            h1 = "23:59" : h2 = "23:59"
            s = "select hour1,hour1b,ways from workershifts where workerid = " & wid & " and date1 = " & sqldate(d)
            if ways = "-A" or ways = "/A" then
              s = s & " and instr(ways,'A') > 0"
            elseif ways = "-B" or ways = "/B" then
              s = s & " and instr(ways,'B') > 0"
            else
              s = s & " and instr(ways,'A') + instr(ways,'B') > 0"
            end if
            s = s & " order by hour1"
            set rs1 = conn.execute(sqlfilter(s))
            do until rs1.eof
              h1 = rs1("hour1") : h2 = rs1("hour1b")
              h1 = cdate(d & " " & h1) : h2 = cdate(d & " " & h2)
              if h1 > h2 then h2 = dateadd("d", 1, h2)
              if rs1("ways") = "A" then h2 = h1
              if rs1("ways") = "B" then h1 = h2
              if ways = "-A" then h2 = h1
              if ways = "-B" then h1 = h2
              if ways = "/A" then h2 = h1
              if ways = "/B" then h1 = h2
              if (h1 < produceduntil and h2 >= produceduntil) then du = errorend("_back~Cannot set shifts to a date~ " & day(produceduntil) & "/" & month(produceduntil) & "/" & year(produceduntil) & " ~to an hour before~ " & getdatehourstring(produceduntil) & ". ~the COLLECT hour has passed. Please select DISTRIBUTE ONLY~ (~A Shift already exists for this day~)")
              if (h1 < produceduntil and h2 < produceduntil) then du = errorend("_back~Cannot set shifts to a date before~ " & produceduntil & " (~A Shift already exists for this day~)")
              rs1.movenext
            loop
          end if
          if enableretro = "" then
            h1 = hour1  : if instr(h1,":") = 0 or instr(ways, "A") = 0 then h1 = "23:59"
            h2 = hour1b : if instr(h2,":") = 0 or instr(ways, "B") = 0 then h2 = "23:59"
            h1 = cdate(d & " " & h1) : h2 = cdate(d & " " & h2) : if hour1b < hour1 then h2 = dateadd("d", 1, h2)
            if (h1 < produceduntil and h2 >= produceduntil) then du = errorend("_back~Cannot set shifts to a date~ " & day(produceduntil) & "/" & month(produceduntil) & "/" & year(produceduntil) & " ~to an hour before~ " & getdatehourstring(produceduntil) & ". ~the COLLECT hour has passed. Please select DISTRIBUTE ONLY~")
            if (h1 < produceduntil and h2 < produceduntil) then du = errorend("_back~Cannot set shifts to a date before~ " & produceduntil)
            t = getval(shiftdata1, "lockhour")
            if ishour(t) and cdate(d) = date and getdatehourstring(now) > t and instr(ways, "-") = 0 then
              du = errorend("_back��� ����� ������ ���� " & t)
            end if

          end if

          if getparam("LockWorkerShifts") = "y" and shiftid <> "" and not hasmatch("X,-,-A,-B,", ways) then
            okshifts = getidlist("select shiftids from workers where id = " & wid)
            if specs = "rafael" and okshifts <> "" then
              if session("fridayshifts") = "" then session("fridayshifts") = "-1," & getidlist("select id from shifts where instr(title, '����') > 0")
              okshifts = okshifts & session("fridayshifts")
            end if

            if okshifts <> "" and instr("," & okshifts, "," & shiftid & ",") = 0 then
              if specs = "ravbariach" then
                ways = "X" 'enable set but just for info
              else
                du = showerror("~Cannot set the selected shift for worker~ " & getworkername(wid))
                exit do
              end if
            end if
          end if

          workerdata1 = getonefield("select data1 from workers where id = " & wid)
          t = getval(workerdata1, "forbiddenshifts") : if shiftid <> "" and hasmatch(t, shiftid) then du = errorend("_back�� ���� ������ �� ������ " & gettitle("shifts", shiftid) & " ����� " & getworkername(wid))

          if specs = "rafael" and session("usergroup") = "manager" then
            if session("shortdayshifts") = "" then session("shortdayshifts") = "-1," & getidlist("select id from shifts where instr(title, '��� �����') > 0")
            if hasmatch(session("shortdayshifts"), shiftid) and cdate(d) > date then du = errorend("_back�� ���� ������ ����� ��� ����� �������� �������")
          end if

          'if specs = "rafael" then 
          '  if d > date and hasmatch(shiftid, "1,2,17,18,") then
          '    du = errorend("_back�� ���� ������ ����� ����� ���� �����")
          '  end if
          'end if

          '---check double booking for the same time
          'if instr(",A,B,AB,", "," & ways & ",") > 0 and hour1 = "00:00" and hour1b = "00:00" then du = errorend("_back�� ����� ����� �����")
          e = checkshiftoverlap(wid, d, hour1, hour1b, ways, getfield("addition"))
          if e = 1 then exit do

          '-------------------------------------
          if specs = "techjet" then
            s = "select shiftid,hour1,hour1b from workershifts where workerid = " & wid & " and date1 = " & sqldate(d)
            set rs1 = conn.execute(sqlfilter(s))
            if not rs1.eof then
              old = rs1("shiftid")
              if request_ways = "X" and getfield("shiftid") = "0" then hour1 = rs1("hour1") : hour1b = rs1("hour1b")
              s = "select data1 from shifts where id = " & old
              set rs1 = conn.execute(sqlfilter(s))
              if not rs1.eof then
                t = getval(rs1("data1"), "shiftids") & shiftid & ","
                if request_ways <> "X" and instr("," & t, "," & shiftid & ",") = 0 then
                  du = errorend("_back�� ���� ������ �� ������ '" & gettitle("shifts", old) & "' ������ '" & gettitle("shifts", shiftid) & "'")
                end if
              end if
            end if
          end if
          '-------------------------------------
          if specs = "iec" then
            tt = getonefield("select courseids from shifts where id = 0" & getfield("shiftid"))
            if tt <> "" and NOT hasmatch(tt, getfield("courseid")) then du = errorend("_back��� ��� ����� ������ ������ ������")
            if tt = "" and getfield("courseid") <> "0" then du = errorend("_back�� ���� ������ ����� ������ ������")
          end if

          '---- shufersal limitations - appears also in _shufersal_workershifts.asp
          if hasmatch(specs, "shufersal,rail,") and instr("X-", ways) = 0 then
            sq = "select id,data1 from workers where id = " & wid
            set rs1 = conn.execute(sqlfilter(sq))
            if not rs1.eof then
              h1 = hour1 : h2 = hour1b
              workerdata1 = rs1("data1")
              w = getval(workerdata1, "allowways") : if w = "" then w = "AB"
              tw = ""
              if w = "A" then tw = "����� ����"
              if w = "B" then tw = "����� ����"
              if instr(ways, "A") > 0 and instr(w, "A") = 0 then du = showerror("������ ����� �� ����� " & getworkername(wid) & " - " & tw) : exit do
              if instr(ways, "B") > 0 and instr(w, "B") = 0 then du = showerror("������ ����� �� ����� " & getworkername(wid) & " - " & tw) : exit do
              t = getval(workerdata1, "disablehourids")
              sq = "select days,hour1,hour2 from disablehours where id in(" & t & "-1)"
              set rs1 = conn.execute(sqlfilter(sq))
              do until rs1.eof
                t1 = rs1("hour1") : t2 = rs1("hour2") : dd = rs1("days")
                e1 = d & " " & h1 : e2 = d & " " & h2 : if cdate(e2) < cdate(e1) then e2 = dateadd("d", 1, e2)
                if instr(ways, "A") > 0 and (h1 >= t1 and h1 <= t2) and hasmatch(dd, getweekday(e1)) then
                  t = "�� ���� ����� ����� ����� " & getworkername(wid) & " - " & t1 & " �� " & t2
                  'if specs = "rail" then du = errorend(t) else du = showerror(t) : exit do
                  du = errorend(t)
                end if
                if instr(ways, "B") > 0 and (h2 >= t1 and h2 <= t2) and hasmatch(dd, getweekday(e2)) then
                  t = "�� ���� ����� ����� ����� " & getworkername(wid) & " - " & t1 & " �� " & t2
                  'if specs = "rail" then du = errorend(t) else du = showerror(t) : exit do
                  du = errorend(t)
                end if
                rs1.movenext
              loop
            end if
          end if

          if ways = "-" or (getfield("addition") = "" and not hasmatch("-A,-B,/A,/B,", ways)) then
            s = "delete * from workershifts where workerid = " & wid & " and date1 = " & sqldate(d)
            set rs = conn.execute(sqlfilter(s))
          'else
          '  s = "delete * from workershifts where workerid = " & wid & " and date1 = " & sqldate(d) & " and ways = 'X'"
          '  set rs = conn.execute(sqlfilter(s))
          end if

          if ways = "-A" or ways = "-B" then
            s = "select id,ways,data1 from workershifts where workerid = " & wid & " and date1 = " & sqldate(d)
            if rs.state = 1 then rs.close
            rs.open sqlfilter(s), conn, 0, 1, 1 'read only
            if rs.eof and shownerrorcannothalfdel = "" then shownerrorcannothalfdel = "on" : du = showerror("�� ���� ����� ����� ������ ������. ���� ������ ����� ������")
            do until rs.eof
              t = rs("ways") : t = replace(t, replace(ways,"-",""), "")
              data1 = ctxt(rs("data1")) : data1 = setval(data1, "manually", "on")
              if t = "" then
                s = "delete * from workershifts where id = " & rs("id")
                set rs1 = conn.execute(sqlfilter(s))
              else
                s = "update workershifts set ways = '" & t & "', data1 = '" & data1 & "', date0 = " & sqldate(now) & " where id = " & rs("id")
                set rs1 = conn.execute(sqlfilter(s))
              end if
              rs.movenext
            loop
          elseif ways = "/A" or ways = "/B" then
            s = "select id,ways,hour1,hour1b,data1 from workershifts where workerid = " & wid & " and date1 = " & sqldate(d)
            if ways = "/A" then s = s & " and instr(ways,'A') > 0"
            if ways = "/B" then s = s & " and instr(ways,'B') > 0"
            if rs.state = 1 then rs.close
            rs.open sqlfilter(s), conn, 0, 1, 1 'read only
            if rs.eof then
              du = showerror("�� ���� ����� ����� ������ ������. ���� ������ ����� ������")
            else
              data1 = ctxt(rs("data1") & "") : data1 = setval(data1, "manually", "on")
              h1 = rs("hour1") : h2 = rs("hour1b")
              s = "update workershifts set"
              if ways = "/A" then h1 = hour1 : s = s & " hour1 = '" & hour1 & "'"
              if ways = "/B" then h2 = hour1b : s = s & " hour1b = '" & hour1b & "'"
              if getfield("foodsupplierid") <> "" and getfield("foodsupplierid") <> "0" then s = s & ",foodsupplierid = " & getfield("foodsupplierid")
              if specs = "hatama" then
                data1 = setval(data1, "altaddress", getfield("altaddress"))
                s = s & ",from1 = 0" & request_from1
                s = s & ",to1 = 0" & request_to1
                s = s & ",courseid = 0" & getfield("courseid")
              end if
              wa = rs("ways")
              if ways = "/A" and instr(wa, "A") = 0 then wa = "A" & wa
              if ways = "/B" and instr(wa, "B") = 0 then wa = wa & "B"
              s = s & ",ways = '" & wa & "'"
              if getfield("shiftid") <> "" and getfield("shiftid") <> "0" then
                s = s & ",shiftid = " & getfield("shiftid")
              else
                t = getval(session("shifthours"), h1 & "-" & h2)
                if t <> "" then s = s & ",shiftid = " & t
              end if
              s = s & ",date0 = " & sqldate(now)
              s = s & ",data1 = '" & data1 & "'"
              s = s & ",userid0 = '" & session("userid") & "'"
              s = s & " where id = " & rs("id")
              set rs1 = conn.execute(sqlfilter(s))
            end if
          elseif getfield(k) = "on" and ways <> "-" then
            from1 = request_from1 : if from1 = "" then from1 = "0"
            to1 = request_to1 : if to1 = "" then to1 = "0"
            courseid = 0

            if from1 = "0" and to1 = "0" or (specs = "iai" and from1 = "0") then
              sq = "select courseid,from1,to1,courseid2,from2,to2 from workercourses where workerid = " & wid
              sq = sq & " and instr(cstr(',' & shiftids), '," & shiftid & ",') > 0"
              sq = sq & " and instr(',' & days, '," & getweekday(d) & ",') > 0"
              set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then
                courseid = rs1("courseid") : from1 = rs1("from1") : to1 = rs1("to1")
                courseid2 = rs1("courseid2") : from2 = rs1("from2") : to2 = rs1("to2")
                if cstr(courseid2) <> "0" and ways = "B" then
                  from1 = from2 : to1 = to2 : courseid = courseid2
                elseif cstr(courseid2) <> "0" and ways = "AB" then
                  from1v = from1 : to1v = to1
                  from2v = from2 : to2v = to2
                  courseid1v = courseid
                  courseid2v = courseid2
                  ways = "#ways" : courseid = "#courseid" : from1 = "#from1" : to1 = "#to1"
                end if
              end if
            end if

            if specs = "iec" and cstr(to1) = "0" and cstr(shiftid) <> "" and cstr(shiftid) <> "0" then
              t = getonefield("select siteid from shifts where id = " & shiftid)
              to1 = getval(session("mainstations"), t)
            end if
            if specs = "iec" and cstr(from1) = "0" and getfield("courseid") <> "" and getfield("courseid") <> "0" then
              du = errorend("������ ����� �� ����� �� ���� �����.")
            end if

            '----haifachem 24h+ shift
            twentyfourplushourshift = ""
            if specs = "haifachem" and ways = "AB" and getfield("shiftid") = 49 then '������ 24
              ways = "#ways" : twentyfourplushourshift = "on"
            end if

            '--choose similar station from other site
            if from1 = "0" and crosssite = "on" and session("setshifts_siteid2") <> "0" then
              f1 = getonefield("select from1 from workers where id = " & wid)
              ti = trim(gettitle("stations", f1))
              if ti <> "" then
                f1 = getonefield("select id from stations where title = '" & ti & "' and siteid = " & session("setshifts_siteid2"))
                if f1 <> "" then from1 = f1
              end if
              if specs = "rafael" and instr(ways, "-") = 0 then
                t = "" & getonefield("select siteid from stations where id = " & from1)
                if t <> session("setshifts_siteid2") then du = errorend("_back����� " & getworkername(wid) & " �� ������ ���� ��� ������ ���� �����. ��� ���� ���� ��� ����� �����, ��� ����� ������.")
              end if
            end if

            if specs = "rafael" and cstr(shiftid) <> "" and cstr(shiftid) <> "0" then
              t = getonefield("select data1 from shifts where id = 0" & shiftid)
              t = getval(t, "to1")
              if t <> "" and t <> "0" then to1 = t
            end if

            '-----------------checks
            if hasmatch(specs, "tikshoov,egged,iec,royalbeach,shufersal,assuta,") and hasmatch(ways, "AB,A,B,/A,/B,") then
              e1 = d & " " & hour1 : e2 = d & " " & hour1b : if cdate(e2) < cdate(e1) then e2 = dateadd("d", 1, e2)
              t = ""
              if instr(ways, "A") > 0 then t = t & "A," & e1 & ",|"
              if instr(ways, "B") > 0 then t = t & "B," & e2 & ",|"
              t = shiftgaps(wid, d, t)
              if t <> "" then
                t = "���� " & getworkername(wid) & " - " & t
                du = errorend(t)
              end if
            end if
            '---

            if getfield("courseid") <> "" and getfield("courseid") <> "0" then courseid = getfield("courseid")
            'if getfield("splittingfrom1") <> "" then from1 = getfield("splittingfrom1")
            if getfield("foodsupplierid") <> "" and getfield("foodsupplierid") <> "0" then foodsupplierid = getfield("foodsupplierid")
            if getfield("workerfood" & wid) <> "" then
              foodsupplierid = getfield("workerfood" & wid)
              'sq = "select id,data1 from workers where id = " & wid
              'set rs1 = conn.execute(sqlfilter(sq))
              'if not rs1.eof then
              '  t = rs1("data1") : t = setval(t, "lastfoodsupplierid", foodsupplierid)
              '  sq = "update workers set data1 = '" & t & "' where id = " & wid
              '  set rs1 = conn.execute(sqlfilter(sq))
              'end if
            end if
            data1 = "manually=on"
            t = getfield("altaddress") : if t <> "" then data1 = setval(data1, "altaddress", t)
            t = getfield("remarks") : if t <> "" then data1 = setval(data1, "remarks", t)
            t = getfield("workerunit" & wid) : if t <> "" then data1 = setval(data1, "unitid", t)
            t = getfield("wbs") : if t <> "" then data1 = setval(data1, "wbs", t)
            t = getfield("internal") : if t <> "" then data1 = setval(data1, "internal", t)
            t = getfield("costcenter") : if t <> "" then data1 = setval(data1, "costcenter", t)
            't = getfield("project") : if t <> "" then data1 = setval(data1, "project", t)
            data1 = setval(data1, "xtype", xtype)
            if specs = "isp" then
              'if instr(ways, "B") > 0 and hasmatch(hour1b, "18:00,19:00,19:30,") then data1 = setval(data1, "draft", "on")
              t = getonefield("select siteid from shifts where id = 0" & shiftid)
              if cstr(t) = "7" and (getweekday(d) = 6 or getweekday(d) = 9) then data1 = setval(data1, "draft", "on")
            elseif getparam("ManagerInsertDraftShifts") = "y" and session("usergroup") = "manager" then
              data1 = setval(data1, "draft", "on")
            end if
            f = "" : v = ""
            f = f & "date1," : v = v & sqldate(d) & ","
            f = f & "workerid," : v = v & wid & ","
            f = f & "ways," : v = v & "'" & ways & "',"
            f = f & "hour1," : v = v & "'" & hour1 & "',"
            f = f & "hour1b," : v = v & "'" & hour1b & "',"
            f = f & "foodsupplierid," : v = v & "0" & foodsupplierid & ","
            f = f & "from1," : v = v & from1 & ","
            f = f & "to1," : v = v & to1 & ","
            f = f & "courseid," : v = v & courseid & ","
            f = f & "shiftid," : v = v & "0" & shiftid & ","
            f = f & "date0," : v = v & sqldate(now) & ","
            f = f & "data1," : v = v & "'" & data1 & "',"
            f = f & "userid0," : v = v & session("userid") & ","
            f = left(f, len(f) - 1): v = left(v, len(v) - 1)
            s = "insert into workershifts(" & f & ") values(" & v & ")"

            '----split to two seperate shifts
            if specs = "kamada" and ways = "AB" then
              sq = replace(s, "'AB'", "'A'",1,1)
              closers(rs) : set rs = conn.execute(sqlfilter(sq))
              sq = replace(s, "'AB'", "'B'",1,1)
              'if hour1 > hour1b then sq = replace(sq, sqldate(d), sqldate(dateadd("d", 1, d)))
              closers(rs) : set rs = conn.execute(sqlfilter(sq))

            elseif instr(ways, "#") = 0 then
              closers(rs) : set rs = conn.execute(sqlfilter(s))

              ''-----------------vishay checks
              'if specs = "vishay" and ways = "AB" then
              '  t1 = dateadd("d", 1 - weekday(d), d) : t2 = dateadd("d", 6, t1)
              '  errors = "" : longs = 0 : shorts = 0 : nights = 0
              '  lastjobend = dateadd("d", -1, t1) & " 00:00:00" : nicebreak = ""
              '  lastlongday = "1/1/2000" : sequencelongs = 0
              '  s = "select date1,hour1,hour1b from workershifts where workerid = " & wid
              '  s = s & " and ways = 'AB'"
              '  s = s & " and date1 >= " & sqldate(dateadd("d", -5, t1))
              '  s = s & " and date1 <= " & sqldate(t2)
              '  s = s & " order by date1, hour1"
              '  set rs = conn.execute(sqlfilter(s))
              '  do until rs.eof
              '    e1 = rs("date1") & " " & rs("hour1") : e2 = rs("date1") & " " & rs("hour1b") : if cdate(e1) > cdate(e2) then e2 = dateadd("d", 1, e2)
              '    h1 = mid(e1,instr(e1," ")+1) : h2 = mid(e2,instr(e2," ")+1)
              '    t = datediff("h", lastjobend, e1) : if t >= 36 then nicebreak = "on"
              '    x = datediff("n", h1, h2)
              '    'rr e1 & " - " & e2 & " - " & x & "<br>"
              '    if rs("date1") >= cdate(t1) and x >= 600 then longs = longs + 1
              '    if rs("date1") >= cdate(t1) and x <  600 then shorts = shorts + 1
              '    if rs("date1") >= cdate(t1) and h1 >= "18:00" then nights = nights + 1
              '    if x >= 600 then
              '      if lastlongday <> "" then
              '        if cdate(lastlongday) = dateadd("d", -1, rs("date1")) then
              '          sequencelongs = sequencelongs + 1
              '          if sequencelongs = 4 then errors = errors & " �� ���� ������ 5 ������ ������ ����."
              '        else
              '          sequencelongs = 0
              '        end if
              '      end if
              '      lastlongday = rs("date1")                     
              '    end if
              '    lastjobend = e2
              '    rs.movenext
              '  loop
              '  'rr longs & "<br>" & shorts & "<br>" & nights & "<hr>"
              '  if nicebreak = "" and cdate(lastjobend) > dateadd("h", -12, t2) then errors = errors & " ��� ����� �� 36 ���� ����� ���."
              '  if nights >= 5 then errors = errors & " �� ���� ������ 5 ������ ���� ����� ���."
              '  if errors <> "" then
              '    errors = "���� " & getworkername(wid) & " ����� " & d & " - " & errors & " �� ���� ������ �����."
              '    du = showerror(errors)
              '    s = "select max(id) as id from workershifts where workerid = " & wid
              '    closers(rs) : set rs = conn.execute(sqlfilter(s))
              '    lastid = rs("id")
              '    s = "delete * from workershifts where id = " & lastid
              '    closers(rs) : set rs = conn.execute(sqlfilter(s))
              '  end if
              'end if

              '-----------------brom checks
              if instr(",brom,haifachem,", "," & specs & ",") > 0 and ways = "AB" then
                e = dateadd("d", -12, d) : errors = "" : daysinarow = 0 : nights = "000000000000"
                t1 = dateadd("d", 1 - weekday(d), d) : t2 = dateadd("d", 6, t1) : inweek = 0
                do until e >= dateadd("d", +12, d)
                  s = "select date1,hour1,hour1b from workershifts where workerid = " & wid
                  s = s & " and ways = 'AB'"
                  s = s & " and date1 = " & sqldate(e)
                  set rs = conn.execute(sqlfilter(s))
                  if rs.eof then
                    daysinarow = 0 : nights = nights & "0"
                  else
                    daysinarow = daysinarow + 1 : if daysinarow = 13 then errors = errors & " �� ���� ������ ��� 12 ���� �� ����� �������."
                    h1 = rs("hour1") : h2 = rs("hour1b") : e1 = rs("date1") & " " & h1 : e2 = rs("date1") & " " & h2 : if cdate(e1) > cdate(e2) then e2 = dateadd("d", 1, e2)
                    if d = rs("date1") and cdate(e1) < cdate(rs("date1") & " 12:00:00") and right(nights,1) = "1" then errors = errors & " ���� ������ ����� ���� ���� ����� ����."
                    if cdate(e2) > cdate(rs("date1") & " 22:00:00") then nights = nights & "1" else nights = nights & "0"
                    if e >= t1 and e <= t2 then inweek = inweek + datediff("n", e1, e2) : if inweek > 58 * 60 and instr(errors, "���� �����") = 0 then errors = errors & " ���� ������ ��� 58 ���� �����."
                    if (lastjobweekday = 6 and weekday(e) = 7) or (lastjobweekday = 7 and weekday(e) = 1) then
                      if datediff("n", lastjobend, e1) < lastjobdur and instr(errors, "����� ������ �����") = 0 then errors = errors & " ���� ����� ���� ����� ����� ����� ����� ������ �����."
                    end if
                  end if
                  nights = mid(nights,2) : if countstring(nights,"1") > 7 and instr(errors, "����� ��� �����") = 0 then errors = errors & " ���� ��� ��� �� 12 ���� ���� ����� ����� 7 ����� ��� �����."
                  lastjobend = e2 : lastjobdur = datediff("n", e1, e2) : lastjobweekday = weekday(e)
                  e = dateadd("d", 1, e)
                loop
                if errors <> "" then
                  errors = "���� " & getworkername(wid) & " ����� " & d & " - " & errors & " �� ���� ������ �����."
                  du = showerror(errors)
                  '---force disable
                  's = "select max(id) as id from workershifts where workerid = " & wid
                  'closers(rs) : set rs = conn.execute(sqlfilter(s))
                  'lastid = rs("id")
                  's = "delete * from workershifts where id = " & lastid
                  'closers(rs) : set rs = conn.execute(sqlfilter(s))
                end if
              end if

              '-----------------isp checks
              if instr(",isp,", "," & specs & ",") > 0 and ways = "AB" then
                errors = ""
                for j = 1 to 2
                  if j = 1 then
                    e1 = dateadd("d", 1 - weekday(d), d) : e2 = dateadd("d", 6, e1)
                    e = e1 : overtotal = 0 : weekdur = 0
                  else
                    'e1 = cdate("1/" & month(d) & "/" & year(d)) : e2 = cdate(lastday(month(d) & "/" & year(d)) &  "/" & month(d) & "/" & year(d))
                    if day(d) >= 26 then
                      e1 = "26/" & month(d) & "/" & year(d) : t = dateadd("m", 1, d) : e2 = "25/" & month(t) & "/" & year(t)
                    else
                      t = dateadd("m", -1, d) : e1 = "26/" & month(t) & "/" & year(t) : e2 = "25/" & month(d) & "/" & year(d)
                    end if
                    e1 = cdate(e1) : e2 = cdate(e2) : e = e1 : overtotal = 0
                  end if
                  do until e > e2
                    dur = 0
                    '---yesterday's night shifts
                    s = "select date1,hour1,hour1b from workershifts where workerid = " & wid
                    s = s & " and ways = 'AB'"
                    s = s & " and date1 = " & sqldate(dateadd("d", -1, e))
                    s = s & " and hour1 > hour1b"
                    set rs = conn.execute(sqlfilter(s))
                    do until rs.eof
                      t1 = dateadd("d", 1, rs("date1")) & " 00:00:00"
                      t2 = dateadd("d", 1, rs("date1")) & " " & rs("hour1b")
                      dur = dur + datediff("n", t1, t2)
                      rs.movenext
                    loop
                    '---today until midnight
                    s = "select date1,hour1,hour1b from workershifts where workerid = " & wid
                    s = s & " and ways = 'AB'"
                    s = s & " and date1 = " & sqldate(e)
                    set rs = conn.execute(sqlfilter(s))
                    do until rs.eof
                      t1 = rs("date1") & " " & rs("hour1") : t2 = rs("date1") & " " & rs("hour1b")
                      if rs("hour1b") < rs("hour1") then t2 = dateadd("d", 1, rs("date1")) & " 00:00:00"
                      dur = dur + datediff("n", t1, t2)
                      rs.movenext
                    loop
                    over = dur - 540 : if weekday(e) = 3 then over = dur - 570
                    if weekday(e) >= 6 then over = dur
                    if over < 0 then over = 0
                    if j = 1 then
                      if dur > 720 then errors = errors & e & " ���� ����� ���� � 12 ���� �����. "
                      'weekdur = weekdur + dur : if weekdur > 3600 then errors = errors & e & " ���� ����� ���� � 60 ���� �����. "
                      if over > 0 then overtotal = overtotal + over : if overtotal > 900 then errors = errors & e & " ���� ����� ���� � 15 ���� ������ �����. "
                    else
                      if over > 0 then overtotal = overtotal + over : if overtotal > 3600 then errors = errors & e & " ���� ����� ���� � 60 ���� ������ �����. "
                    end if
                    e = dateadd("d", 1, e)
                  loop
                next
                if errors <> "" then
                  errors = "���� " & getworkername(wid) & " - " & errors
                  du = showerror(errors)
                end if
              end if
              '---

            else
              sq = s
              sq = replace(sq, "#ways", "A")
              sq = replace(sq, "#courseid", courseid1v)
              sq = replace(sq, "#from1", from1v)
              sq = replace(sq, "#to1", to1v)
              closers(rs) : set rs = conn.execute(sqlfilter(sq))
              sq = s
              sq = replace(sq, "#ways", "B")
              sq = replace(sq, "#courseid", courseid2v)
              sq = replace(sq, "#from1", from2v)
              sq = replace(sq, "#to1", to2v)
              if twentyfourplushourshift = "on" then sq = replacefromto(sq, "", "values(", "),", "values(" & sqldate(dateadd("d",1,d)) & ",")
              closers(rs) : set rs = conn.execute(sqlfilter(sq))
              ways = request_ways
            end if

            '--------learn worker
            if getparam("LearnWorkerCourses") = "y" then
              s = "select id,courseid1,from1,to1 from workers where id = " & wid
              set rs = conn.execute(sqlfilter(s))
              j = 0
              if cstr("" & rs("courseid1")) <> "0" then j = 1
              if trim(gettitle("stations", rs("from1"))) <> "" then j = 1
              if trim(gettitle("stations", rs("to1"))) <> "" then j = 1
              if j = 0 then
                s = "update workers set "
                s = s & "courseid1 = 0" & courseid & ","
                s = s & "from1 = 0" & from1 & ","
                s = s & "to1 = 0" & to1 & ","
                s = left(s, len(s) - 1) & " "
                s = s & "where id = " & wid
                s = replace(s, "#from1", from1v)
                s = replace(s, "#to1", to1v)
                set rs = conn.execute(sqlfilter(s))
              end if
            end if

            '--------------------learn workercourse
            if getparam("LearnWorkerCourses") = "y" and shiftid <> "0" and cstr(courseid) <> "" and cstr(courseid) <> "0" then
              s = "select id from workercourses where workerid = " & wid & " and cstr('' & shiftids) = '" & shiftid & ",'"
              set rs = conn.execute(sqlfilter(s))
              if rs.eof then
                f = "" : v = ""
                f = f & "shiftids," : v = v & "'" & shiftid & ",',"
                f = f & "courseid," : v = v & courseid & ","
                f = f & "days," : v = v & "'1,2,3,4,5,6,7," & getparam("ExtraWeekDays") & "',"
                f = f & "from1," : v = v & from1 & ","
                f = f & "to1," : v = v & to1 & ","
                f = f & "workerid," : v = v & wid & ","
                f = f & "courseid2," : v = v & 0 & ","
                f = f & "from2," : v = v & 0 & ","
                f = f & "to2," : v = v & 0 & ","
                f = left(f, len(f) - 1): v = left(v, len(v) - 1)
                s = "insert into workercourses(" & f & ") values(" & v & ")"
                s = replace(s, "#from1", from1v)
                s = replace(s, "#to1", to1v)
                closers(rs) : set rs = conn.execute(sqlfilter(s))
              end if
            end if

          end if

          '-----------------rotem checks
          if specs = "rotem" and hasmatch(ways, "AB,A,B,/A,/B,") then
            errors = ""
            if cstr(getonefield("select siteid from workers where id = " & wid)) = getval(session("siteidbynames"), "������") then
              x = 0
              '---yesterday
              s = "select hour1b from workershifts where workerid = " & wid
              s = s & " and date1 = " & sqldate(dateadd("d", -1, d))
              s = s & " and hour1b < hour1"
              set rs = conn.execute(sqlfilter(s))
              do until rs.eof
                t = rs("hour1b") : if ishour(t) then : x = x + cdbl(left(t,2)) + cdbl(right(t,2)) / 60
                rs.movenext
              loop
              '---today
              s = "select id,hour1,hour1b from workershifts where workerid = " & wid & " and ways = 'AB'"
              s = s & " and date1 = " & sqldate(d)
              set rs = conn.execute(sqlfilter(s))
              lastid = 0
              do until rs.eof
                lastid = rs("id")
                t1 = rs("hour1") : t2 = rs("hour1b") : if t2 < t1 then t2 = "24:00"
                t1 = cdbl(left(t1,2)) + cdbl(right(t1,2)) / 60
                t2 = cdbl(left(t2,2)) + cdbl(right(t2,2)) / 60
                x = x + t2 - t1
                rs.movenext
              loop
              if x > 12 then errors = errors & " ��� 12 ���� ����� (" & fornum(x) & " ����) "
            end if
            if errors <> "" then
              errors = "���� " & getworkername(wid) & " ����� " & d & " - " & errors & " �� ���� ������ �����."
              du = showerror(errors)
              s = "delete * from workershifts where id = " & lastid
              closers(rs) : set rs = conn.execute(sqlfilter(s))
            end if
          end if

          cc = cc + 1
        end if
      end if
      d = dateadd("d",1,d)
    loop
  loop
  if cctry = 0 then du = showerror("�� ����� ������ ������")
  if msg = "" then msg = cc & " ~Shifts were updated~"
  request_action = "groupshifts"
  if getfield("action2") = "delall" then rr "<script language=vbscript> document.location = ""?" & replace(querystring , "&action2=delall", "") & """</script>"
end if

'==================================================================================
  weekplans = "" : lastn = "" : d = d1
  if session("weekplans_" & d1 & "_" & d2) = "" then
    set st = Server.CreateObject("ADODB.Stream") : st.open
    do until d > d2
      s = "select day1,ways,hour1,hour1b,shiftid,workerid,date1,date2 from weekplan"
      s = s & " where workerid in(select id from workers where 0=0" & okreadsql("workers") & ")"
      s = s & " and day1 = '" & weekday(d) & "'"
      s = s & " and (year(date1) = 1900 or date1 <= " & sqldate(d) & ")"
      s = s & " and (year(date2) = 1900 or date2 >= " & sqldate(d) & ")"
      s = s & " order by workerid, ways"
      set rs1 = conn.execute(sqlfilter(s))
      lastn = ""
      do until rs1.eof
        v = ""
        t = gettitle("shifts", rs1("shiftid")) : if t <> "" then v = v & " " & tocsv(t)
        if instr("" & rs1("ways"),"A") > 0 then v = v & translate(" ~Arrival~ ") & rs1("hour1")
        if instr("" & rs1("ways"),"B") > 0 then v = v & translate(" ~Distribute~ ") & rs1("hour1b")
        if instr("" & rs1("ways"),"X") > 0 then v = v & translate(" ~No Ride~ ")
        v = translate(" (~Fixed~) ") & v & "^"
        'v = translate(" (~Fixed~) ") & getval(session("shiftnames"), rs1("shiftid")) & v & "^"
        n = "u" & rs1("workerid") & "d" & d
        if n <> lastn then st.writetext("|" & n & "=") : lastn = n
        st.writetext(v)
        rs1.movenext
      loop
      d = dateadd("d",1,d)
    loop
    st.writetext("|")
    st.position = 0 : session("weekplans_" & d1 & "_" & d2) = st.readtext(st.size) : st.close
  end if
  weekplans = session("weekplans_" & d1 & "_" & d2)

  '--start-----------------------------------------------------------------------------------
  filterstyle = "background:#fcfcfc; color:aaaaaa; border1:1px solid #cccccc; padding:4; height:32; margin-top:4; white-space:nowrap;"
  ShowTemplatesButton = "" : if getparam("UseWorkerTemplates") = "y" and oksee("template") and specs <> "techjet" then ShowTemplatesButton = "on"
  rr "<iframe id=frpopstations frameborder=0 style='width:0; height:0; position:absolute; visibility:hidden; top:-1000;'></iframe>"
  rr "<iframe id=frpopcourses frameborder=0 style='width:0; height:0; position:absolute; visibility:hidden; top:-1000;'></iframe>"
  rr "<table cellspacing=0 cellpadding=7 bgcolor1=dddddd align=center>"
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=shiftsubmit><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<tr><td class=list_name id=tr0_form_title colspan=99><img src='navclock.png' ~iconsize~> ~Set Shifts~")
  rr(" <font id=msgdiv style='color:#ffffff; font-weight:bold; background:#00aa00; padding:3px;'>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  '-----------tr1
  rr "<tr><td bgcolor1=#fcfcfc colspan=99 style='padding:10;'><nobr>"
  rr " <input type=button class=groovybutton value='~Show~' style='width:110; margin-bottom:2; margin-~langside2~:2;' onclick='vbscript: f1.action.value = ""search"" : f1.submit'>"

  if oksee("string") then rr " <span style='" & filterstyle & "'> ~Find~ <input type=text name=string style='width:120;' value='" & session("setshifts_string") & "' onkeypress='vbscript: if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'></span>"

  if oksee("d1") then
    rr " <span style='" & filterstyle & "'>"
    rr "~From~ " & selectdate("*f1.d1",d1)
    rr " ~To~ " & selectdate("*f1.d2",d2)
    rr "</span>"
  else
    rr "<input type=hidden name=d1 value='" & d1 & "'>"
    rr "<input type=hidden name=d2 value='" & d2 & "'>"
  end if

  '---scroll weeks
  'rr " <input type=button alt='~Previous Week~' style='font-weight:bold; width:20; height:20; border:1px solid #aaaaaa; background:#F2F2F2; color:444444; ' value='<' onclick='vbscript: document.location = ""?action=groupshifts&d1=" & dateadd("d",-7,d1) & "&d2=" & dateadd("d",-1,d1) & """'>"
  'rr " <input type=button alt='~Next Week~'     style='font-weight:bold; width:20; height:20; border:1px solid #aaaaaa; background:#F2F2F2; color:444444; ' value='>' onclick='vbscript: document.location = ""?action=groupshifts&d1=" & dateadd("d",+7,d1) & "&d2=" & dateadd("d",+13,d1) & """'>"
  'rr " <input type=button                       style='font-weight:bold; width1:20; height:20; border:1px solid #aaaaaa; background:#F2F2F2; color:444444; ' value='~Today~' onclick='vbscript: document.location = ""?action=groupshifts&d1=" & dateadd("d", 1 - weekday(date), date) & "&d2=" & dateadd("d", 7 - weekday(date), date) & """'>"
  't = formatdatetime(d1, 1) : t = replace(t, year(d1), "") : rr "&nbsp;&nbsp;" & t & " - " & formatdatetime(d2, 1)
  if session("setshifts_span") = "day" then
    back1 = dateadd("d",-1,d1)
    back2 = dateadd("d",-1,d1)
    forw1 = dateadd("d",+1,d1)
    forw2 = dateadd("d",+1,d1)
  elseif session("setshifts_span") = "week" then
    back1 = dateadd("d",-7,d1)
    back2 = dateadd("d",-1,d1)
    forw1 = dateadd("d",+7,d1)
    forw2 = dateadd("d",+13,d1)
  elseif session("setshifts_span") = "month" then
    back1 = dateadd("m",-1,d1)
    back2 = dateadd("m",0,d1) : back2 = dateadd("d",-1,back2)
    forw1 = dateadd("m",+1,d1)
    forw2 = dateadd("m",+2,d1) : forw2 = dateadd("d",-1,forw2)
  end if
  rr "&nbsp;&nbsp;<img src=button_~langside~.png style='vertical-align:-3;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&d1=" & back1 & "&d2=" & back2 & """'"
  rr " style='overflow-x:hidden; width:30; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#444444;'>"
  rr "<b><</b></a>"
  rr "<img src=button_sep.png style='vertical-align:-3;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&d1=" & forw1 & "&d2=" & forw2 & """'"
  rr " style='overflow-x:hidden; width:30; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#444444;'>"
  rr "<b>></b></a>"
  'rr "<img src=button_~langside2~.png style='vertical-align:-3;'>"
  'rr "&nbsp;&nbsp;<img src=button_~langside~.png style='vertical-align:-3;'>"
  'rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&d1=" & dateadd("d", 1 - weekday(date), date) & "&d2=" & dateadd("d", 7 - weekday(date), date) & """'"
  'rr " style='overflow-x:hidden; width:40; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#444444;'>"
  'rr "<b>~Today~</b></a>"
  rr "<img src=button_~langside2~.png style='vertical-align:-3;'>"

  '---controls
  if oksee("workertypeids") then
    rr " <span style='" & filterstyle & "'>"
    x = getonefield("select count(id) as x from workertypes") : x = cdbl("0" & x)
    if x <= 200 then
      rr "<input type=hidden name=workertypefilter value=''>"
      sq = "select id,title from workertypes where 0=0"
      if specs = "shufersal" and session("usersiteid") <> "0" then sq = sq & " and siteid = " & session("usersiteid")
      sq = sq & " order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      a = ""
      do until rs1.eof
        a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
        rs1.movenext
      loop
      rr " " & multiselect("workertypeids", "~workers_workertypeid~", a, session("setshifts_workertypeids"), 150)
    else
      'rr "<select name=workertypeids dir=~langdir~>"
      'rr("<option value=0 style='color:bbbbfe;'>~workers_workertypeid~")
      'sq = "select id,title from workertypes where 0=0"
      'if session("usersiteid") <> "0" then sq = sq & " and siteid = " & session("usersiteid")
      'sq = sq & " order by title"
      'closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      'do until rs1.eof
      '  rr("<option value='" & rs1("id") & ",'")
      '  if cstr(session("setshifts_workertypeids")) = cstr(rs1("id") & ",") then rr(" selected")
      '  rr(">")
      '  rr(rs1("title") & " ")
      '  rs1.movenext
      'loop
      'rr("</select>")
      rr "<input type=hidden name=workertypeids value=''>"
      rr " ~workers_workertypeid~ "
      rr "<input type=text name=workertypefilter value='" & session("setshifts_workertypefilter") & "'"
      rr " onkeypress='vbscript: if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'"
      rr " style='width:100;'>"
    end if
    rr "</span>"
  end if

  if oksee("active") then
    rr " <span style='" & filterstyle & "'>"
    rr "<select name=active dir=~langdir~>"
    rr "<option value='' style='color:bbbbfe;'>~workers_active~"
    rr "<option value=on" : if lcase(session(tablename & "_active")) = "on" then rr " selected"
    rr "> ~workers_active~: ~Yes~"
    rr "<option value=no" : if lcase(session(tablename & "_active")) = "no" then rr " selected"
    rr "> ~workers_active~: ~No~"
    rr "</select></span>"
  end if

  if oksee("eligible") then
    rr " <span style='" & filterstyle & "'>"
    rr " <select name=eligible dir=~langdir~>"
    rr "<option value='' style='color:bbbbfe;'>~workers_eligible~"
    rr "<option value=on" : if lcase(session(tablename & "_eligible")) = "on" then rr " selected"
    rr "> ~workers_eligible~: ~Yes~"
    rr "<option value=no" : if lcase(session(tablename & "_eligible")) = "no" then rr " selected"
    rr "> ~workers_eligible~: ~No~"
    rr "</select></span>"
  end if

  if specs = "rafael" then
    rr " <span style='" & filterstyle & "'>"
    rr " <select name=person_visible dir=~langdir~>"
    rr "<option value='' style='color:bbbbfe;'>����� ����"
    rr "<option value=on" : if lcase(session(tablename & "_person_visible")) = "on" then rr " selected"
    rr "> ����� ����: ~Yes~"
    rr "<option value=no" : if lcase(session(tablename & "_person_visible")) = "no" then rr " selected"
    rr "> ����� ����: ~No~"
    rr "</select></span>"
    rr " <span style='" & filterstyle & "'>"
    rr " <select name=short_day_eligibility dir=~langdir~>"
    rr "<option value='' style='color:bbbbfe;'>��� �����"
    rr "<option value=on" : if lcase(session(tablename & "_short_day_eligibility")) = "on" then rr " selected"
    rr "> ��� �����: ~Yes~"
    rr "<option value=no" : if lcase(session(tablename & "_short_day_eligibility")) = "no" then rr " selected"
    rr "> ��� �����: ~No~"
    rr "</select></span>"
    rr " <span style='" & filterstyle & "'>"
    rr " <select name=mom dir=~langdir~>"
    rr "<option value='' style='color:bbbbfe;'>�����"
    rr "<option value=on" : if lcase(session(tablename & "_mom")) = "on" then rr " selected"
    rr "> �����: ~Yes~"
    rr "<option value=no" : if lcase(session(tablename & "_mom")) = "no" then rr " selected"
    rr "> �����: ~No~"
    rr "</select></span>"
  end if

  if showdistzone = "on" then
    rr " <span style='" & filterstyle & "'> ���� ����� "
    'rr "<input type=text name=distzone style='width:70;' value='" & session("setshifts_distzone") & "' onkeypress='vbscript: if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'>"
    rr " <select name=distzone><option value=''>"
    a = distzones
    if specs = "imi" and siteidfilter <> "0" then a = getval(distzonesparts, siteidfilter)
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      rr "<option value='" & b & "'" : if b = session(tablename & "_distzone") then rr " selected"
      rr ">" & b
    loop
    rr "</select>"
    rr "</span>"
  end if

  if specs = "imi" then
    rr " <span style='" & filterstyle & "'> ���� "
    rr "<input type=text name=factory style='width:70;' value='" & session("setshifts_factory") & "' onkeypress='vbscript: if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'>"
    rr "</span>"
  end if

  if oksee("unitid") then
    'rr " ~workers_unitid~ "
    s = "select count(id) as x from units where 0=0"
    if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
    if session("usergroup") <> "admin" then s = s & " and id in(" & session("userunitids") & "-1)"
    x = getonefield(s) : x = cdbl("0" & x)

    if specs = "kmc" then
      rr " <span style='" & filterstyle & "'>����� "
      sq = "select id,title from units where 0=0"
      if session("usersiteid") <> "0" then sq = sq & " and siteid in(" & session("usersiteid") & ",)"
      sq = sq & " order by title"
      rr "<div style='position:absolute; display:inline;'>"
      selectsearchwidth = 180
      rr selectsearch("!f1.unitids", session(tablename & "_unitids"), "title,", "units", sq)
      rr "</div>"
      rr "<img src=blank.gif width=180 height=1>"
      rr "</span>"

    elseif x <= 200 or hasmatch(specs, "rafael,isp,") then
      rr " <span style='" & filterstyle & "'>"
      rr "<input type=hidden name=unitid value=0>"
      s = "select units.id,units.title,sites.title as sitetitle from units left join sites on units.siteid = sites.id where 0=0"
      if siteidfilter <> "0" then s = s & " and units.siteid in(0," & siteidfilter & ")"
      if session("usergroup") <> "admin" and specs <> "isp" then s = s & " and units.id in(" & session("userunitids") & "-1)"
      s = s & " order by sites.title, units.title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
      a = ""
      do until rs1.eof
        a = a & rs1("id") & ":"
        if session("usersiteid") = "0" then a = a & rs1("sitetitle") & " - " 
        a = a & replace(rs1("title"),",",";")
        a = a & ","
        rs1.movenext
      loop
      allunits = a
      if specs <> "rafael" then rr " " & multiselect("unitids", "~workers_unitid~", allunits, session("setshifts_unitids"), 200)
      rr "</span>"
    else
      rr " <span style='" & filterstyle & "'>"
      rr "<input type=hidden name=unitids value=''>"
      rr "<select name=unitid dir=~langdir~>"
      rr("<option value=0 style='color:bbbbfe;'>~workers_unitid~")

      sq = "select id,title from units"
      if session("usersiteid") <> "0" then sq = sq & " and siteid = " & session("usersiteid")
      if session("usergroup") <> "admin" then sq = sq & " and id in(" & session("userunitids") & "-1)"
      sq = replace(sq," and "," where ",1,1,1)
      sq = sq & " order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        rr("<option value=""" & rs1("id") & """")
        if cstr(session("setshifts_unitid")) = cstr(rs1("id")) then rr(" selected")
        rr(">")
        rr(rs1("title") & " ")
        rs1.movenext
      loop
      rr("</select>")
      rr "</span>"
    end if

    if specs = "elbit" then
      rr " <span style='" & filterstyle & "'> ��� ����� "
      rr "<input type=text name=unitfilter style='width:80;' value='" & session("setshifts_unitfilter") & "' onkeypress='vbscript: if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'>"
      rr "</span>"
    end if
  end if

  rr "<br><input type=button class=groovybutton value='~Clear Search~' style='width:110; margin-bottom:2; margin-~langside2~:2;' onclick='vbscript: f1.action.value = ""clear"" : f1.submit'>"

  if oksee("template") and getparam("UseWorkerTemplates") = "y" then
    rr " <span style='" & filterstyle & "'>"
    'rr " ~workers_template~ "
    rr "<select name=template dir=~langdir~>"
    rr("<option value='' style='color:bbbbfe;'>~workers_template~")
    sq = "select firstname from workers where lastname = '_TEMPLATE_'"
    if session("usersiteid") <> "0" then sq = sq & " and (siteid = " & session("usersiteid") & ")"
    sq = sq & " order by firstname"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("firstname") & """")
      if lcase(cstr(session(tablename & "_template"))) = lcase(cstr(rs1("firstname"))) then rr(" selected")
      rr ">" & rs1("firstname")
      rs1.movenext
    loop
    rr("</select></span>")
  end if

  if oksee("citystationid") then
    rr " <span style='" & filterstyle & "'>"
    rr " ~workers_citystationid~ "
    rr "<input type=text name=citystationidfilter style='width:80;' value='" & session("setshifts_citystationidfilter") & "' onkeypress='vbscript: if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'>"
    rr("</span>")
  end if

  if oksee("from1filter") then
    rr " <span style='" & filterstyle & "'>"
    rr " ~workers_from1~ "
    rr "<input type=text name=from1filter style='width:80;' value='" & session("setshifts_from1filter") & "' onkeypress='vbscript: if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'>"
    rr("</span>")
  end if

  if oksee("to1filter") then
    rr " <span style='" & filterstyle & "'>"
    rr " ~workers_to1~ "
    rr "<input type=text name=to1filter style='width:80;' value='" & session("setshifts_to1filter") & "' onkeypress='vbscript: if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'>"
    rr("</span>")
  end if

  if oksee("siteid") and (session("usersiteid") = "0" or crosssite = "on" or session("usersiteids") <> "") then
    rr " <span style='" & filterstyle & "'>"
    'rr " ~workers_siteid~ "
    rr "<select name=siteid dir=~langdir~>"
    rr("<option value=0 style='color:bbbbfe;'>~workers_siteid~")
    sq = "select id,title from sites where 0=0"
    if specs = "shufersal" then sq = sq & " and active = 'on'"
    if session("usersiteid") <> "0" and session("usersiteids") <> "" and not crosssite = "on" then sq = sq & " and id in(" & session("usersiteids") & "-1)"
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session("setshifts_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></span>")
  end if

  if oksee("color") and instr(",laufer,sodastream,perrigo,tama,imi,", "," & specs & ",") > 0 then
    rr " <span style='" & filterstyle & "'>"
    rr(" <select style='width:150;' name=colorfilter dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workers_color~")
    if session("workercolors") = "" then
      sq = "select distinct color from workers where color <> ''"
      if session("usersiteid") <> "0" then sq = sq & " and (siteid = " & session("usersiteid") & ")"
      sq = sq & " order by color"
      session("workercolors") = getfieldlist(sq)
    end if
    a = session("workercolors")
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      rr("<option style='color:808080; background:#" & b & "' value=""" & b & """")
      if ucase(session(tablename & "_colorfilter")) = ucase(b) then rr(" selected")
      rr ">" & ucase(b)
    loop
    rr("</select></span>")
  end if

  if oksee("showshiftids") and specs <> "egged" then
    rr " <span style='" & filterstyle & " color:#000000;'>"
    sq = "select id,title from shifts where active='on'"
    if siteidfilter <> "0" then sq = sq & " and siteid = " & siteidfilter
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    a = ""
    do until rs1.eof
      a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
      rs1.movenext
    loop
    rr " " & multiselect("showshiftids", "~Specific shifts set~", a, session("setshifts_showshiftids"), 220)
    rr "</span>"
  end if

  if oksee("byshift") then
    rr " <span style='" & filterstyle & "'>"
    'rr " ~By Shift~ "
    rr "<select name=byshift>"
    rr "<option value='' style='color:#bbbbff;'>~Shifts Already Set~"
    if session("setshifts_orderby") <> "shifthour" then
      t = "~Only selected shift, by collect AND distribute hours~"  : t2 = "selected"      : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
      t = "~Only selected shift, by collect OR distribute hours~"   : t2 = "selected2"   : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
      t = "~Already set any shift~"                                 : t2 = "all"        : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
      t = "~Already set a shift with a ride~"                       : t2 = "all2"       : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
      t = "~Shift I personally submitted~"                          : t2 = "mine"       : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
      t = "~Not set yet~"                                           : t2 = "none"       : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
      t = "~Draft shifts~"                                          : t2 = "draft"      : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
      t = "~Shifts with alternative address~"                       : t2 = "altaddress" : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
      if specs = "rafael" then t = "����� ������ ��������"          : t2 = "all3"       : rr "<option value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
    end if
    t = "~Shift with collect only~"                                 : t2 = "collect"    : rr "<option style='color:#ff0000;' value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
    t = "~Shift with distribute only~"                              : t2 = "distribute" : rr "<option style='color:#ff0000;' value=" & t2 : if session("setshifts_byshift") = t2 then rr " selected>" & t else rr ">" & t
    rr "</select></span>"
  end if

  if oksee("courseid") then
    rr " <span style='" & filterstyle & "'>"
    rr(" ~Course~ ")
    if specs = "tnuva" then
      sq = "select id,code,title from courses order by code1,code,title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      a = ""
      a = a & "n:(~None~),"
      a = a & "a:(~Any~),"
      do until rs1.eof
        t = trim(rs1("code") & " " & rs1("title"))
        if t <> "" then a = a & rs1("id") & ":" & tocsv(t) & ","
        rs1.movenext
      loop
      rr multiselect("courseids", "~orders_courseid~", a, session(tablename & "_courseids"), 180)
    else
      'rr " <img src=icons/i_help.gif style='cursor:hand; filter:alpha(opacity=50); margin-top:8;' alt='~orders_course_search_help~'> "
      rr "<input type=text name=coursefilter style='width:80;' value=""" & session(tablename & "_coursefilter") & """>"
    end if
    rr "</span>"
  end if

  if oksee("lines") then
    rr " <span style='" & filterstyle & "'>"
    rr " <input type=text name=lines value='" & session("setshifts_lines") & "' style='width:30;'> ~Lines~"
    rr "</span>"
  else
    rr "<input type=hidden name=lines value='" & session("setshifts_lines") & "'>"
  end if

  x = 0
  'if specs = "rafael" then x = 1
  if instr(",iai,elbit,", "," & specs & ",") > 0 and session("usergroup") = "admin" then x = 1
  if x = 1 and getfield("action") = "" and getfield("page") = "" then rr "</table><br><br><center><b>��� ��� �������� ����� ���� �� `���`</b>" : rend

  if specs = "rafael" then
    rr "<br><img src=blank.gif width=115 height=30>"
    rr " &nbsp; <span style='" & filterstyle & "'><span style='width:60; overflow:hidden;'>�����</span>"
    rr " " & multiselect("unitids", "~workers_unitid~", allunits, session("setshifts_unitids"), 200) & " &nbsp;"
    rr "</span>"
    bb = "shetach,hativa,"'contractor_company,
    do until bb = ""
      b = left(bb,instr(bb,",")-1) : bb = mid(bb,instr(bb,",")+1)
      'if hasmatch(session(tablename & "_filters"), b) then
        rr " &nbsp;<span style='" & filterstyle & "'><span style='width:60; overflow:hidden;'>~" & b & "~</span>"
        a = ""
        s = "select title from misc where type1 = '" & b & "' order by title"
        set rs1 = conn.execute(sqlfilter(s))
        do until rs1.eof
          a = a & rs1("title") & ":" & rs1("title") & ","
          rs1.movenext
        loop
        rr multiselect(b, "~" & b & "~", a, session(tablename & "_" & b), 180)
        rr "</span>"
      'end if
    loop
    help = "<img src=icons/i_help.gif border=0 alt='n = ���, y = �����, ���� �� ���� ���� ����� �� ������.'>"
    'rr "<span style='" & filterstyle & "'><span style='width:60; overflow:hidden;'>��� " & help & "</span>"
    'rr "<input type=text name=shetach value='" & session(tablename & "_shetach") & "' style='width:180;'></span>"
    'rr "<span style='" & filterstyle & "'><span style='width:60; overflow:hidden;'>����� " & help & "</span>"
    'rr "<input type=text name=hativa value='" & session(tablename & "_hativa") & "' style='width:180;'></span>"
    rr " &nbsp; <span style='" & filterstyle & "'><span style='width:60; overflow:hidden;'>���� " & help & "</span>"
    rr "<input type=text name=contractor_company value='" & session(tablename & "_contractor_company") & "' style='width:180;'></span>"


  end if

  '-----------tr2
  rr "<tr><td colspan=99 bgcolor=#ececec style='padding:10;'><nobr>"
  rr "<center>"

  if oksee("span") then
    rr "<img src=button_~langside~.png style='vertical-align:-3;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&span=day&d1=" & date & "&d2=" & date & """'"
    t = "444444" : if session("setshifts_span") = "day" then t = "aa0000"
    rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~Today~</b></a>"
    rr "<img src=button_sep.png style='vertical-align:-3;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&span=tomorrow&d1=" & dateadd("d",1,date) & "&d2=" & dateadd("d",1,date) & """'"
    t = "444444" : if session("setshifts_span") = "tomorrow" then t = "aa0000"
    rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~Tomorrow~</b></a>"
    rr "<img src=button_sep.png style='vertical-align:-3;'>"
    t1 = dateadd("d", 1 - weekday(date), date)
    t2 = dateadd("d", 7 - weekday(date), date)
    if specs = "osem" and session("usersiteid") = "5" then '�����
      t1 = dateadd("d", -1 - weekday(date), date)
      t2 = dateadd("d", 5 - weekday(date), date)
    end if
    rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&span=week&d1=" & t1 & "&d2=" & t2 & """'"
    t = "444444" : if session("setshifts_span") = "week" or session("setshifts_span") = "" then t = "aa0000"
    rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~This Week~</b></a>"
    if specs = "brom" then
      rr "<img src=button_sep.png style='vertical-align:-3;'>"
      rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&span=month&d1=" & "1/" & month(date) & "/" & year(date) & "&d2=" & lastday(month(date) & "/" & year(date)) & "/" & month(date) & "/" & year(date) & """'"
      t = "444444" : if session("setshifts_span") = "month" then t = "aa0000"
      rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
      rr "<b>~This Month~</b></a>"
    end if
    rr "<img src=button_~langside2~.png style='vertical-align:-3;'>"
  end if

  '---showshift
  if oksee("showshift") then
    rr "<img src=blank.gif width=50 height=1>"
    rr "<img src=button_~langside~.png style='vertical-align:-3;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?showshift=hours&page=0""'"
    t = "444444" : if session("setshifts_showshift") = "hours" then t = "aa0000"
    rr " style='overflow-x:hidden; width:80; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~Show~ ~hours~</b></a>"
    rr "<img src=button_sep.png style='vertical-align:-3;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?showshift=shifts&page=0""'"
    t = "444444" : if session("setshifts_showshift") = "shifts" then t = "aa0000"
    rr " style='overflow-x:hidden; width:80; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~Show~ ~shifts~</b></a>"
    rr "<img src=button_~langside2~.png style='vertical-align:-3;'>"
  end if

  '---orderby
  if oksee("orderby") then
    rr "<img src=blank.gif width=50 height=1>"
    rr "<img src=button_~langside~.png style='vertical-align:-3;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?orderby=abc&page=0""'"
    t = "444444" : if session("setshifts_orderby") = "abc" then t = "aa0000"
    rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~ABC~</b></a>"
    rr "<img src=button_sep.png style='vertical-align:-3;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?orderby=unit&page=0""'"
    t = "444444" : if session("setshifts_orderby") = "unit" then t = "aa0000"
    rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~Unit~</b></a>"
    rr "<img src=button_sep.png style='vertical-align:-3;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?orderby=template&page=0""'"
    t = "444444" : if session("setshifts_orderby") = "template" then t = "aa0000"
    rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~Template~</b></a>"
    rr "<img src=button_sep.png style='vertical-align:-3;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?orderby=shifthour&page=0""'"
    t = "444444" : if session("setshifts_orderby") = "shifthour" then t = "aa0000"
    rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~Hour~</b></a>"
    rr "<img src=button_~langside2~.png style='vertical-align:-3;'>"
  end if

  if specs = "laufer" then rr " <a target=_new href=_get_ezshift.asp>��� ������ ��������</a>"
  if specs = "metropolin" then rr " <a target=_new href=admin_workers.asp?action=metropolin_load_shifts>��� ������ ����� ������ ���� ������</a>"
  if specs = "mh" then rr " <a target=_new href=_ftp_merkazh.asp?action=loadafterftp>����� ������ ���� ������</a>"

  '-----------tr3
  rr "<tr><td bgcolor=#d8d8d8 colspan=99 style='padding:10;'><nobr>"
  rr " <input type=button class=groovybutton value='~Submit Shifts~' style='width:110; margin-bottom:2; margin-~langside2~:2;'"
  if specs = "iec" then
    rr " onclick='vbscript: if f1.shiftid.value = ""0"" then msgbox ""��� ��� ����� �����"" else f1.submit'"
  elseif getparam("SetShiftsChooseIndividualShifts") = "y" then
    rr " onclick='vbscript: f1.submit'"
  else
    t = "" : if ShowTemplatesButton = "on" then t = " and f1.templateid.value = ""0"" "
    rr " onclick='vbscript: if instr(f1.ways.value,""-"") = 0 " & t & " and ((f1.hour1.value = """" and instr(f1.ways.value,""A"")) or (f1.hour1b.value = """" and instr(f1.ways.value,""B""))) then msgbox ""~Choose Hours~"" else f1.submit'"
  end if
  rr ">"

  '--------choose site
  if crosssite = "on" then
    if session("setshifts_siteid2") = "" then session("setshifts_siteid2") = session("usersiteid")
    if session("setshifts_siteid2") <> "0" then t = getval(session("mainstations"), session("setshifts_siteid2")) : t = iif(t = "", "0", t) : session("setshifts_to1") = t
    rr " <span style='" & filterstyle & "'>"
    'rr " ~workers_siteid~ "
    rr "<select name=siteid2 dir=~langdir~ onchange='vbscript: f1.action.value = ""search"" : f1.submit'>"
    rr("<option value=0 style='color:bbbbfe;'>~workers_siteid~")
    sq = "select id,title from sites where active = 'on' order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session("setshifts_siteid2")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></span>")
  end if

  '---set shifts
  if session("usergroup") <> "admin" and session("managershiftids") = "" then
    s = "select id, unitids from shifts"
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      uu = rs1("unitids") : if isnull(uu) then uu = ""
      if uu = "" or hasmatch(session("userunitids"), "" & uu) then session("managershiftids") = session("managershiftids") & rs1("id") & ","
      rs1.movenext
    loop
  end if
  if oksee("shiftid") then
    sq = "select id,title,hour1,hour1b,ways,days,data1 from shifts where active='on'"
    if siteidfilter <> "0" then sq = sq & " and siteid = " & siteidfilter
    if session("managershiftids") <> "" then sq = sq & " and id in(" & session("managershiftids") & "-1)"
    'if specs = "iai" then sq = sq & " and id in(" & getidlist("select id from shifts where cstr(courseids) <> ''") & "-1)"
    if specs = "iai" then sq = sq & " and cstr(courseids) <> ''"
    if session("setshifts_span") = "day" then wd = getweekday(session("setshifts_d1")) : sq = sq & " and instr(',' & days, '," & wd & ",') > 0"
    if specs = "misgav" then tt = getval(session("userdata1"), "productionshiftids") : if tt <> "" then sq = sq & " and id in(" & tt & "-1)"
    sq = sq & " order by title,hour1,hour1b"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    rr " <span style='" & filterstyle & "'>"
    rr "<select name=shiftid dir=~langdir~ onchange='vbscript:sethours'>"
    'rr " ~Shift~ "
    rr "<option value=0 style='color:bbbbfe;'>~Shift~"
    do until rs1.eof
      thedata1 = rs1("data1")
      rr "<option value=""" & rs1("id") & """"
      if session("setshifts_shiftid") = cstr(rs1("id")) then rr " selected"
      rr ">"
      rr rs1("title") & " "
      if NOT hasmatch(specs, "brom,iai,rafael,kla,alliance,sheleg,ap,waldorfastoria,") and instr(siteurl, "/tnuvamaadanot/") = 0 then
        rr rs1("hour1") & "-" & rs1("hour1b")
        if specs <> "hpaper" then
          t = rs1("days")
          if t <> "1,2,3,4,5,6,7," & getparam("ExtraWeekDays") then
            for i = 1 to 20 : t = replace(t, i & ",", "~weekday" & i & "~,") : next
            if right(t,1) = "," then t = left(t,len(t)-1)
            rr " (" & t & ")"
          end if
        end if
      end if
      sethourscode = sethourscode & " if x = """ & rs1("id") & """ then" & vbcrlf
      sethourscode = sethourscode & "   f1.hour1.value = """ & rs1("hour1") & """ : f1.hour1b.value = """ & rs1("hour1b") & """" & vbcrlf
      sethourscode = sethourscode & "   f1.ways.value = """ & rs1("ways") & """" & vbcrlf

      if oksee("foodsupplierid") and getparam("ShiftFood") = "y" then
        if didpopfood = "" then
          didpopfood = "on"
          rr "<script language=vbscript>" & vbcrlf
          rr "function popfood(byval ff, byval defaultfood)" & vbcrlf
          rr "  document.f1.foodsupplierid.options.length = 0" & vbcrlf
          rr "  set o = document.CreateElement(""OPTION"")" & vbcrlf
          rr "  o.Value = ""0""" & vbcrlf
          rr "  o.Text = ""(~Without Food~)""" & vbcrlf
          rr "  document.f1.foodsupplierid.add o" & vbcrlf
          s = "select id,title from foodsuppliers order by title"
          set rs2 = conn.execute(s)
          do until rs2.eof
            rr "  i = " & rs2("id") & vbcrlf
            rr "  if ff = """" or instr("","" & ff, "","" & i & "","") > 0 then" & vbcrlf
            rr "    set o = document.CreateElement(""OPTION"")" & vbcrlf
            rr "    o.Value = """ & rs2("id") & """" & vbcrlf
            rr "    o.Text = """ & tocsv(ctxt(rs2("title"))) & """" & vbcrlf
            rr "    if cstr(defaultfood) = cstr(i) then o.selected = true" & vbcrlf
            rr "    document.f1.foodsupplierid.add o" & vbcrlf
            rr "  end if" & vbcrlf
            rs2.movenext
          loop
          rr "end function" & vbcrlf
          rr "</script>" & vbcrlf
        end if
        ff = getval(thedata1, "foodsupplierids")
        defaultfood = getval(thedata1, "defaultfood")
        'if defaultfood = "" then defaultfood = getlistitem(ff,1)
        if defaultfood = "" then defaultfood = "0"
        sethourscode = sethourscode & "  du = popfood(""" & ff & """, """ & defaultfood & """)" & vbcrlf
      end if

      sethourscode = sethourscode & " end if" & vbcrlf
      rs1.movenext
    loop

    sethours2code = sethours2code & " if instr(f1.ways.value,""A"") > 0 then spancoll.style.visibility = ""visible"" : else spancoll.style.visibility = ""hidden""" & vbcrlf
    sethours2code = sethours2code & " if instr(f1.ways.value,""B"") > 0 then spandist.style.visibility = ""visible"" : else spandist.style.visibility = ""hidden""" & vbcrlf
    rr "</select></span>"
  else
    rr "<input type=hidden name=shiftid value=0>"
  end if

  if oksee("addition") and getparam("MultiDailyShifts") = "y" then
    rr " <span style='" & filterstyle & "'>"
    t = "" : if specs = "rafael" then t = " checked"
    rr "<input type=checkbox name=addition " & t & ">~As Additional Shifts~"
    rr "</span>"
  end if
  '---------ways-------------------------------------------
  if oksee("ways") then
    rr " <span style='" & filterstyle & "'>"
    a = "AB,A,B,/A,/B,X,-,-A,-B,"
    'if specs = "shufersal" and session("usergroup") <> "admin" then a = "AB,A,B,X,"
    rr(" <select name=ways dir=~langdir~ onchange='vbscript: sethours2'>")
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      co = "000000" : if b = "-" then co = "aa0000"
      sel = "" : if specs <> "brom" and session(tablename & "_ways") = b then sel = " selected"
      dis = b : bg = "ffffff"
      if b = "AB" then dis = "~Collect and Distribute~" : bg = "ccffcc"
      if b = "A" then dis = "~Collect~" : bg = "ccffcc"
      if b = "B" then dis = "~Distribute~" : bg = "ccffcc"
      if b = "/A" then dis = "~Update collect hour~" : bg = "ccccff"
      if b = "/B" then dis = "~Update distribute hour~" : bg = "ccccff"
      if b = "X" then dis = "~Mark without ride~" : bg = "cccccc"
      if b = "-" then dis = "~Delete Shifts~" : bg = "ffcccc"
      if b = "-A" then dis = "~Delete~ ~collect~" : bg = "ffcccc"
      if b = "-B" then dis = "~Delete~ ~Distribute~" : bg = "ffcccc"
      'if specs <> "metropolin" then bg = "ffffff"
      rr "<option value=""" & b & """ style='color:#" & co & "; background:#" & bg & ";' " & sel & ">" & dis
    loop
    if specs = "techjet" then
      rr "<option style='color:#0000aa;' value='X:�������'>�������"
      rr "<option style='color:#0000aa;' value='X:����'>����"
      rr "<option style='color:#0000aa;' value='X:����'>����"
      rr "<option style='color:#0000aa;' value='X:���� ������'>���� ������"
    elseif specs = "brom" then
      rr "<option style='color:#0000aa;' value='X:���� ����'>���� ����"
      rr "<option style='color:#0000aa;' value='X:���� �����'>���� �����"
    elseif specs = "lageen" then
      rr "<option style='color:#0000aa;' value='X:����'>����"
    end if
    rr("</select></span>")'form:ways
  else
    rr "<input type=hidden name=ways value=AB>"
  end if

  a = lcase(getparam("SetShiftsCollectDistributeHoursList"))
  t = getval(session("usersitedata1"), "SetShiftsLockHours") : if t = "" then t = getparam("SetShiftsLockHours")
  setshiftslockhours = t

  if not oksee("hours") then
    rr(" <span id=spancoll><input type=hidden name=hour1 value=""" & session("setshifts_hour1") & """ ></span>")
    rr(" <span id=spandist><input type=hidden name=hour1b value=""" & session("setshifts_hour1b") & """ ></span>")
  elseif a = "" or lcase(setshiftslockhours) = "y" then
    rr " <span style='" & filterstyle & "'>"
    t = "" : if lcase(setshiftslockhours) = "y" then t = " onfocus='vbscript:me.blur' style='background:eeeeee; border:1px solid #cccccc;'"
    rr(" ~Arrival~ <span id=spancoll><input type=text maxlength=5 name=hour1 onblur='vbscript:checkhour1' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:50;' value=""" & session("setshifts_hour1") & """ dir=ltr" & t & "></span>")
    rr(" ~Distribute~ <span id=spandist><input type=text maxlength=5 name=hour1b onblur='vbscript:checkhour1b' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:50;' value=""" & session("setshifts_hour1b") & """ dir=ltr" & t & "></span>")
    rr "</span>"
  else
    '--------------select collect/distribute hours separately-----------------
    e = "~Invalid settings parameter SetShiftsCollectDistributeHoursList, ask the administrator to type a valid value~"
    if instr(a,"interval") > 0 then
      a = trim(replace(a, "interval", "")) : if not isnumeric("-" & a) then x = errorend(e)
      if cdbl(a) < 1 or cdbl(a) > (24 * 60) then x = errorend(e)
      interval = a : a = "" : d = "20/1/2000 00:00:00"
      do until cdate(d) >= cdate("21/1/2000")
        h = mid(d, instr(d," ")+1)
        h = left(h,instrrev(h,":")-1)
        h = replace(h,":","")
        a = a & h & ","
        d = dateadd("n", interval, d) : if instr(d,":") = 0 then d = d & " 00:00:00"
      loop
      a = a & "/" & a
    end if
    if countstring(a,"/") <> 1 then x = errorend(e)
    a = replace(a," ","")
    'if specs = "rail" then a = "0000,0015,0030,0045,0100,0115,0130,0145,0200,0215,0230,0245,0300,0315,0330,0345,0400,0415,0430,0445,0500,0515,0530,0545,0600,2000,2015,2030,2045,2100,2115,2130,2145,2200,2215,2230,2245,2300,2315,2330,2345," : a = a & "/" & a
    cc = left(a,instr(a,"/")-1) & "," : dd = mid(a,instr(a,"/")+1) & ","
    for j = 1 to 2
      rr " <span style='" & filterstyle & "'>"
      if j = 1 then rr " ~Arrival~ <span id=spancoll><select name=hour1><option value='' style='color:bbbbfe;'>~Arrival~" : a = cc
      if j = 2 then rr " ~Distribute~ <span id=spandist><select name=hour1b><option value='' style='color:bbbbfe;'>~Distribute~" : a = dd
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        if b <> "" then
          if b <> left(strfilter(b,"0123456789"),4) then x = errorend(e)
          b = left(b,2) & ":" & right(b,2)
          rr "<option value='" & b & "'>" & b
        end if
      loop
      rr "</select></span></span>"
    next
  end if

  showfoodtitle = "" : if specs = "imi" then showfoodtitle = "on"
  if oksee("foodsupplierid") and getparam("ShiftFood") = "y" then
    rr " <span style='" & filterstyle & "'>"
    rr " <select name=foodsupplierid dir=~langdir~>"
    rr "<option value=0 style='color:bbbbfe;'>~Without Food~"
    sq = "select id,title from foodsuppliers"
    'if session("usersiteid") <> "0" then sq = sq & " where siteid = " & session("usersiteid")
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr "<option value=""" & rs1("id") & """"
      'if getfield("foodsupplierid") = cstr(rs1("id")) then rr " selected"
      rr ">"
      rr rs1("title")
      if showfoodtitle <> "on" then rr " (~Code~ ~FoodAbbreviation~" & rs1("id") & ")"
      rs1.movenext
    loop
    rr "</select>"
    rr " <input type=button value='~Submit Food~' onclick='vbscript: if f1.foodsupplierid.value = ""0"" then msgbox ""~Please select food~"" else if msgbox(""~Add food to all selected shifts~"",vbyesno) = 6 then f1.action2.value = ""onlyfood"" : f1.submit'>"
    rr "</span>"
  else
    rr "<input type=hidden name=foodsupplierid value=0>"
  end if

  'if specs = "isp" then
  '  rr " <span style='" & filterstyle & "'>"
  '  rr " ������ <input type=text name=project style='width:100;'>"
  '  rr "</span>"
  'end if

  if oksee("courseid") then
    rr " <span style='" & filterstyle & "'>"
    if specs = "iec" then
      did = false
      rr " <select name=courseid dir=~langdir~ onchange='vbscript: f1.action.value = ""search"" : f1.submit'>"
      rr "<option value=0>"
      s = "select id,code,title from courses where active = 'on'"
      if siteidfilter <> "0" then s = s & " and siteid = " & siteidfilter
      s = s & " order by code, title"
      set rs1 = conn.execute(sqlfilter(s))
      do until rs1.eof
        rr "<option value=" & rs1("id")
        if session("setshifts_courseid") = cstr(rs1("id")) then rr " selected" : did = true
        rr ">" & rs1("code") & " " & rs1("title")
        rs1.movenext
      loop
      rr "</select>"
      if not did then session("setshifts_courseid") = ""
    else
      'rr "~Course~"
      rr " <select name=courseid dir=~langdir~"
      rr " onchange='vbscript: frpopstations.document.location = ""?act=popstations&courseid="" & f1.courseid.value & ""&from1="" & f1.from1.value & ""&to1="" & f1.to1.value'"
      rr "></select>"
    end if
    rr "</span>" 'form:from1
  else
    rr "<input type=hidden name=courseid value=0>"
  end if

  if oksee("from1") then
    
    if specs = "iec" then
      rr " <span style='" & filterstyle & " width:210; position:relative; top:3;'>"
      rr "<div style='position:absolute; display:inline;'>"
      selectsearchwidth = 200
      t = "select top 30 id,title from stations"
      if session("setshifts_courseid") <> "" and session("setshifts_courseid") <> "0" then
        tt = getidlist("select stationid from coursestations where courseid = 0" & session("setshifts_courseid"))
        t = t & " and id in(" & tt & "0)"
      end if
      rr selectsearch("!f1.from1", "", "title,", "stations", t)
      rr "</div></span>"
    else
      rr " <span style='" & filterstyle & "'>"
      rr " <select name=from1 dir=~langdir~ onfocus='vbscript: if document.f1.from1.options.length <= 1 then frpopstations.document.location = ""?act=popstations&shiftid="" & f1.shiftid.value & ""&courseid="" & f1.courseid.value & ""&from1="" & f1.from1.value & ""&to1="" & f1.to1.value'>"
      rr("<option value=0 style='color:bbbbfe;'>~Worker`s station~")
      'rr "<option value=" & session("setshifts_from1") & " selected>" & gettitle("stations", session("setshifts_from1"))
      rr("</select></span>")'form:from1
    end if

    e = 0
    if instr(",laufer,rafael,metropolin,imi,tm,hatama,ap,", "," & specs & ",") > 0 then e = 1
    if instr(siteurl, "/tnuvamilk/") > 0 and session("usersiteid") = "7" then e = 1'������
    if e = 1 then
      rr " <span style='" & filterstyle & "'>����� ������ <input type=text name=altaddress></span>"
    end if
    if specs = "iai" then
      t = getval(session("mainstations"), session("usersiteid")) : if t = "" then t = "0"
      rr "<input type=hidden name=to1 value=" & t & ">"
    'elseif specs = "rafael" then
    '  if session("setshifts_to1") = "" or session("setshifts_to1") = "0" then session("setshifts_to1") = getval(session("mainstations"), session("setshifts_siteid2"))
    '  rr " <span style='" & filterstyle & "'>���� ������ "
    '  s = "select id,title from stations where 0=0"
    '  s = s & " and id in(" & getval(session("mainstations"), "a") & "-1)"
    '  s = s & " order by title"
    '  set rs1 = conn.execute(sqlfilter(s))
    '  rr " <select name=to1><option value=0 style='color:#bbbbfe;'>~Work Location~"
    '  do until rs1.eof
    '    rr "<option value=" & rs1("id")
    '    if session("setshifts_to1") = cstr(rs1("id")) then rr " selected"
    '    rr ">" & rs1("title")
    '    rs1.movenext
    '  loop
    '  rr "</select>"
    '  rr "</span>"
    elseif crosssite = "on" and NOT hasmatch(specs, "imi,") then
      t = session("setshifts_to1")
      if specs = "dexcel" then t = "0" : if session("setshifts_siteid2") <> "0" then t = getval(session("mainstations"), session("setshifts_siteid2"))
      if t = "" or not isnumeric(t) then t = "0"
      rr "<input type=hidden name=to1 value=" & t & ">"

    elseif specs = "egged" then
      rr " <span style='" & filterstyle & "'>"
      s = "select top 30 id,title from stations"
      t = "select distinct to1 from workers where 0=0" : if session("setshifts_siteid") <> "" and session("setshifts_siteid") <> "0" then t = t & " and siteid in(0" & session("setshifts_siteid") & ",0)"
      s = s & " where id in(" & getidlist(t) & "0)"
      s = s & " order by title"
      set rs1 = conn.execute(sqlfilter(s))
      rr " <select name=to1><option value=0 style='color:#bbbbfe;'>~Work Location~"
      do until rs1.eof
        rr "<option value=" & rs1("id")
        if session("setshifts_to1") = cstr(rs1("id")) then rr " selected"
        rr ">" & rs1("title")
        rs1.movenext
      loop
      rr "</select>"
      rr "</span>"
    else
      if getfield("box") = "to1" then theto1 = getfield("boxid")
      rr " <span style='" & filterstyle & "'>"
      rr " <select name=to1 dir=~langdir~ onfocus='vbscript: if document.f1.to1.options.length <= 1 then frpopstations.document.location = ""?act=popstations&shiftid="" & f1.shiftid.value & ""&courseid="" & f1.courseid.value & ""&from1="" & f1.from1.value & ""&to1="" & f1.to1.value'>"
      rr "<option value=0 style='color:bbbbfe;'>~Work Location~"
      rr "</select>"
      if specs = "metropolin" then rr " <input type=button value='��� ����' onclick='vbscript: if f1.to1.value = ""0"" then msgbox ""��� ��� ����"" else if msgbox(""���� ���� ���� ����� ������� �������"",vbyesno) = 6 then f1.action2.value = ""onlyto1"" : f1.submit'>"
      rr "</span>"
    end if
    'rr "<input type=text name=splittingfrom1>"
  else
    rr "<input type=hidden name=from1 value=0>"
    rr "<input type=hidden name=to1 value=0>"
  end if
  if instr(",brom,imi,isp,cf,", "," & specs & ",") > 0 then rr " <span style='" & filterstyle & "'>����� ����� <input type=text name=remarks></span>"

  '-----------templates
  if oksee("template") and getparam("UseWorkerTemplates") = "y" then
    rr " <span style='" & filterstyle & "'>"
    'rr " ~Template~"
    rr " <select name=templateid>"
    rr "<option value=0 style='color:#bbbbff;'>~Template~"
    s = "select id,firstname from workers where lastname = '_TEMPLATE_'"
    if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
    s = s & " order by firstname"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      rr "<option value=" & rs("id")
      if getfield("templateid") = cstr(rs("id")) then rr " selected"
      rr ">" & rs("firstname")
      rs.movenext
    loop
    rr "</select></span>"
  end if

  if specs = "iec" then
    rr "<br><img src=blank.gif width=110 height=30>"
    aa = "wbs,internal,costcenter,"
    do until aa = ""
      a = left(aa,instr(aa,",")-1) : aa = mid(aa,instr(aa,",")+1)
      rr " ~" & a & "~ <select name=" & a & "><option value=''>"
      s = "select title from misc where type1 = '" & a & "'"
      s = s & " and instr(cstr('' & contents), '|siteid=" & session("setshifts_siteid2") & "|') > 0"
      s = s & " order by title"
      set rs1 = conn.execute(sqlfilter(s))
      do until rs1.eof
        v = rs1("title") : v = tocsv(v)
        rr "<option value='" & v & "'>" & v
        rs1.movenext
      loop
      rr "</select>"
    loop
  end if

  '--------------tr4
  if d1 > d2 then x = errorend("_back~End date is smaller than start date~")
  if getfield("csv") = "on" then
    server.scripttimeout = 100000 'seconds
    set csv = Server.CreateObject("ADODB.Stream") : csv.open
    csv.writetext(translate("~Worker~,"))
    csv.writetext(translate("~Code~,"))
    if specs <> "osem" then csv.writetext(translate("~Unit~,"))
    if specs <> "osem" then csv.writetext(translate("~workers_eligible~,"))
    if specs = "tikshoov" then csv.writetext(translate("~workers_from1~,"))
    if specs = "techjet" then csv.writetext(translate("~workers_workertypeid~,"))
    if specs = "brom" then csv.writetext(translate("~workers_citystationid~,"))
    if hasmatch(specs, "imi,philips,") then csv.writetext(translate("~workers_phone1~,"))
    if specs = "osem" then csv.writetext(translate("~workers_citystationid~,"))
  end if

  '-----------scripts-------------
  rr "<script language=vbscript>" & vbcrlf
  if oksee("courseid") and not oksee("shiftid") then rr " frpopcourses.document.location = ""?act=popcourses&shiftid=0&courseid=" & session("setshifts_courseid") & """" & vbcrlf

  rr "function sethours" & vbcrlf
  if oksee("courseid") then rr " frpopcourses.document.location = ""?act=popcourses&shiftid="" & f1.shiftid.value & ""&courseid="" & f1.courseid.value" & vbcrlf
  rr "  f1.hour1.value = """" : f1.hour1b.value = """"" & vbcrlf
  rr "  x = f1.shiftid.value" & vbcrlf
  rr sethourscode
  rr "  x = sethours2" & vbcrlf
  rr "end function" & vbcrlf
  rr "function sethours2" & vbcrlf
  rr sethours2code
  rr "end function" & vbcrlf
  rr "function checkhour1" & vbcrlf
  rr "    if f1.hour1.value = """" then exit function" & vbcrlf
  rr("    if len(f1.hour1.value) = 4 then f1.hour1.value = ""0"" & f1.hour1.value" & vbcrlf)
  rr("    if len(f1.hour1.value) = 5 and mid(f1.hour1.value,3,1) = "":"" and isnumeric(left(f1.hour1.value,2)) and isnumeric(right(f1.hour1.value,2)) and instr(f1.hour1.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) <= 59 then ok = 1 else msgbox ""~Invalid Hour~"" : f1.hour1.value = """"" & vbcrlf)
  rr "end function" & vbcrlf
  rr "function checkhour1b" & vbcrlf
  rr "    if f1.hour1b.value = """" then exit function" & vbcrlf
  rr("    if len(f1.hour1b.value) = 4 then f1.hour1b.value = ""0"" & f1.hour1b.value" & vbcrlf)
  rr("    if len(f1.hour1b.value) = 5 and mid(f1.hour1b.value,3,1) = "":"" and isnumeric(left(f1.hour1b.value,2)) and isnumeric(right(f1.hour1b.value,2)) and instr(f1.hour1b.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) <= 59 then ok = 1 else msgbox ""~Invalid Hour~"" : f1.hour1b.value = """"" & vbcrlf)
  rr "end function" & vbcrlf
  if sethourscode <> "" then
    rr "sethours" & vbcrlf
    if instr(siteurl, "/axisis.net/flextronics/") > 0 then rr " f1.ways.value = ""B""" & vbcrlf
  end if
  rr "</script>"

  if session("scrollwithin") = "" then session("scrollwithin") = "no"
  if getfield("scrollwithin") <> "" then session("scrollwithin") = getfield("scrollwithin")
  if session("scrollwithin") = "on" then
    rr "<tr><td colspan=99 style='padding:0;'><div id=columntitles style='white-space:nowrap; background:#dddddd;'></div>"
    rr "<tr><td colspan=99 style='padding:0;'><div id=scrollwithin style='height:200; overflow-y:scroll; direction:~langdir2~;'><table cellspacing=1 cellpadding=4 dir=~langdir~>"
  end if

  '----workers--------------------------------
  if session("setshifts_byshift") <> "" or session("setshifts_showshiftids") <> "" then
    ww = "" : ww1 = "" : ww2 = ""

    if instr(",mine,none,draft,altaddress,all3,", "," & session("setshifts_byshift") & ",") = 0 then
      s = "select distinct workerid from weekplan where 0=0"
      t = d1 : tt = "" : do until cdate(t) > cdate(d2) : tt = tt & "'" & getweekday(t) & "'," : t = dateadd("d", 1, t) : loop
      s = s & " and day1 in(" & tt & "'/')"
      if session("setshifts_byshift") = "selected" then
        s = s & " and (hour1 = '" & session("setshifts_hour1") & "' or hour1b = '" & session("setshifts_hour1b") & "')"
      elseif session("setshifts_showshiftids") <> "" then
        s = s & " and shiftid in(" & session("setshifts_showshiftids") & "-1)"
      end if
      set rs1 = conn.execute(sqlfilter(s))
      do until rs1.eof
        ww1 = ww1 & rs1("workerid") & ","
        rs1.movenext
      loop
    end if

    s = "select distinct workerid from workershifts where 0=0"
    if session("setshifts_showshiftids") <> "" then
      'ww1 = ""
      s = s & " and workershifts.shiftid in(" & session("setshifts_showshiftids") & "-999)"
    end if
    if session("setshifts_byshift") = "selected" then s = s & " and (hour1 = '" & session("setshifts_hour1") & "' and hour1b = '" & session("setshifts_hour1b") & "')"
    if session("setshifts_byshift") = "selected2" then s = s & " and (hour1 = '" & session("setshifts_hour1") & "' or hour1b = '" & session("setshifts_hour1b") & "')"
    'if session("setshifts_byshift") = "all2" then s = s & " and (hour1 <> '00:00' or hour1b <> '00:00')"
    if session("setshifts_byshift") = "all2" then s = s & " and ways <> 'X'"
    if session("setshifts_byshift") = "all3" then s = s & " and instr(cstr(data1), '|source=mob') > 0"
    if session("setshifts_byshift") = "draft" then s = s & " and instr(cstr(data1), 'draft=on') > 0"
    if session("setshifts_byshift") = "mine" then s = s & " and workershifts.userid0 = " & session("userid")
    if session("setshifts_byshift") = "altaddress" then s = s & " and instr(cstr(data1), '|altaddress=') > 0"
    if session("setshifts_byshift") = "collect" then s = s & " and instr(ways, 'A') > 0"
    if session("setshifts_byshift") = "distribute" then s = s & " and instr(ways, 'B') > 0"
    s = s & " and date1 >= " & sqldate(d1)
    s = s & " and date1 <= " & sqldate(d2)
    ww2 = getidlist(s)
    ww = ww1 & ww2
    if session("setshifts_byshift") = "draft" then ww = ww2
    ww = formatidlistunique(ww)
  end if

  a = session(tablename & "_coursefilter") : ww3 = ""
  if a <> "" then
    s = "select distinct workerid from workershifts where 0=0"
    s = s & " and date1 >= " & sqldate(d1)
    s = s & " and date1 <= " & sqldate(d2)
    a = a & "," : add = ""
    do until a = ""
      w = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
      if lcase(w) = "a" then
        add = add & " or workershifts.courseid > 0"
      elseif lcase(w) = "n" then
        add = add & " or workershifts.courseid = 0"
      elseif instr(w, "!") > 0 then
        w = replace(w, "!", "")
        ii = getidlist("select top 50 id from courses where code = '" & w & "'")
        add = add & " or workershifts.courseid in(" & ii & "-1)"
      elseif w <> "" then
        ii = getidlist("select top 50 id from courses where instr(code & ' ' & title, '" & w & "') > 0")
        add = add & " or workershifts.courseid in(" & ii & "-1)"
      end if
    loop
    if add = "" then s = s & " and 0=1" else s = s & " and(" & mid(add,5) & ")"
    ww3 = ww3 & getidlist(s)
  end if
  a = session(tablename & "_courseids") : add = ""
  if a = "n," then
    add = add & " and workershifts.courseid = 0"
  elseif a = "a," then
    add = add & " and workershifts.courseid > 0"
  elseif a <> "" then
    a = nomatch(a, "a,n,")
    add = add & " and workershifts.courseid in(" & a & "-1)"
  end if
  if add <> "" then
    s = "select distinct workerid from workershifts where 0=0"
    s = s & " and date1 >= " & sqldate(d1)
    s = s & " and date1 <= " & sqldate(d2)
    s = s & add
    ww3 = ww3 & getidlist(s)
  end if

  s = "select workers.id,workers.code,workers.firstname,workers.lastname,workers.unitid,workers.template,workers.siteid,workers.courseid1,workers.workertypeid,workers.data1,workers.eligible,workers.citystationid,workers.from1,workers.phone1,"

  't = "" : if session("setshifts_byshift") = "distribute" then t = "b"
  'if session("setshifts_orderby") = "shifthour" then s = s & "workershifts.hour1" & b & " as shifthour,"

  s = s & "units.title as unittitle"
  s = s & " from (workers left join units on workers.unitid = units.id)"

  if session("setshifts_orderby") = "shifthour" then
    s = s & " left join workershifts on (workershifts.workerid = workers.id and workershifts.date1 = " & sqldate(session("setshifts_d1"))
    if session("setshifts_byshift") = "collect" then s = s & " and instr(ways, 'A') > 0"
    if session("setshifts_byshift") = "distribute" then s = s & " and instr(ways, 'B') > 0"
    s = s & ")"
  end if

  s = s & " where workers.firstname <> '' and workers.lastname <> '_TEMPLATE_'"
  if session("setshifts_eligible") = "on" then s = s & " and workers.eligible = 'on'"
  if session("setshifts_eligible") = "no" then s = s & " and workers.eligible <> 'on'"
  if session("setshifts_active") = "on" then s = s & " and workers.active = 'on'"
  if session("setshifts_active") = "no" then s = s & " and workers.active <> 'on'"
  if specs = "rafael" then
    if session(tablename & "_person_visible") = "on" then s = s & " and instr('' & cstr('' & workers.data1), '|PERSON_VISIBLE=on|') > 0"
    if session(tablename & "_person_visible") = "no" then s = s & " and instr('' & cstr('' & workers.data1), '|PERSON_VISIBLE=on|') = 0"
    if session(tablename & "_short_day_eligibility") = "on" then s = s & " and instr('' & cstr('' & workers.data1), '|SHORT_DAY_ELIGIBILITY=on|') > 0"
    if session(tablename & "_short_day_eligibility") = "no" then s = s & " and instr('' & cstr('' & workers.data1), '|SHORT_DAY_ELIGIBILITY=on|') = 0"
    if session(tablename & "_mom") = "on" then s = s & " and instr('' & cstr('' & workers.data1), '|MOM=on|') > 0"
    if session(tablename & "_mom") = "no" then s = s & " and instr('' & cstr('' & workers.data1), '|MOM=on|') = 0"
  end if
  t = session(tablename & "_distzone") : if t = "���" then s = s & " and instr('' & cstr('' & workers.data1), '|distzone=') = 0" else if t = "���" then s = s & " and instr('' & cstr('' & workers.data1), '|distzone=') > 0" else if t <> "" then s = s & " and instr('' & cstr('' & workers.data1), '|distzone=" & t & "|') > 0"
  t = session(tablename & "_factory") : if t = "���" then s = s & " and instr('' & cstr('' & workers.data1), '|factory=') = 0" else if t = "���" then s = s & " and instr('' & cstr('' & workers.data1), '|factory=') > 0" else if t <> "" then s = s & " and instr('' & cstr('' & workers.data1), '|factory=" & t & "|') > 0"
  if session("setshifts_colorfilter") <> "" then s = s & " and lcase(workers.color) = '" & lcase(session("setshifts_colorfilter")) & "'"
  if specs = "dexcel" and session("usergroup") = "manager" then
    myworkers = getidlist("select id from workers where 0=0" & okreadsql("workers")) & "-1,"
  elseif hasmatch(specs, "rafael,imi,") and session("userunitids") <> "" then
    myworkers = getidlist("select id from workers where unitid in (" & session("userunitids") & "-1)")
    if specs = "imi" then s = s & " and workers.id in(" & myworkers & "-1)"
  elseif instr("," & session("userperm"), ",addglobalworkers,") > 0 then
    'nothing - all workers...
    if specs = "iec" and session("usergroup") = "manager" then s = s & " and unitid in(" & session("userunitids") & "-1)"
  elseif hasmatch(specs, "isp,") and session("usergroup") = "manager" then
    if session("usersiteid") <> "0" then s = s & " and workers.siteid = " & session("usersiteid")
  else
    s = s & okreadsql("workers")
  end if
  if (session("setshifts_byshift") <> "" or session("setshifts_showshiftids") <> "") and session("setshifts_byshift") <> "none" then s = s & " and workers.id in(" & ww & "-1)"

  if instr(",none,", "," & session("setshifts_byshift") & ",") > 0 then s = s & " and workers.id not in(" & ww & "-1)"
  if session(tablename & "_coursefilter") <> "" or session(tablename & "_courseids") <> "" then s = s & " and workers.id in(" & ww3 & "-1)"

  '--- string - space is and, comma is or ---------
  if session("setshifts_string") <> "" then
    a = session("setshifts_string") & "," : a1 = "" : a2 = ""
    do until a = ""
      b = left(a,instr(a,",")-1) & " " : a = mid(a,instr(a,",")+1)
      if trim(b) <> "" then
        a1 = a1 & " or (" : a2 = ""
        do until b = ""
          w = trim(left(b,instr(b," ")-1)) : b = mid(b,instr(b," ")+1)
          if w <> "" then
            a2 = a2 & " and (workers.firstname like '%" & w & "%' or workers.lastname like '%" & w & "%' or workers.code like '%" & w & "%' or workers.address like '%" & w & "%')"
          end if
        loop
        a1 = a1 & mid(a2,6) & ")"
      end if
    loop
    s = s & " and (" & mid(a1,5) & ")"
  end if

  if session("setshifts_workertypeid") <> "" and session("setshifts_workertypeid") <> "0" then s = s & " and workers.workertypeid = " & session("setshifts_workertypeid")
  if session("setshifts_workertypeids") <> "" then s = s & " and workers.workertypeid in(" & session("setshifts_workertypeids") & "-1)"
  if session("setshifts_workertypefilter") <> "" then s = s & " and workers.workertypeid in(" & getidlist("select top 100 id from workertypes where title like '%" & session("setshifts_workertypefilter") & "%'") & "-1)"
  if session("setshifts_unitid") <> "" and session("setshifts_unitid") <> "0" then s = s & " and workers.unitid = " & session("setshifts_unitid")
  if session("setshifts_unitids") <> "" then s = s & " and workers.unitid in (" & session("setshifts_unitids") & "-1)"
  if session("setshifts_unitfilter") <> "" then s = s & " and workers.unitid in(" & getidlist("select top 100 id from units where title like '%" & session("setshifts_unitfilter") & "%'") & "-1)"
  if session("setshifts_template") <> "" then s = s & " and lcase(workers.template) = '" & lcase(session("setshifts_template")) & "'"
  if session("setshifts_citystationidfilter") <> "" then 
    a = session("setshifts_citystationidfilter") : a = "," & uniquelist(a) : a = replace(a, ",", "','") : a = mid(a,3) : a = left(a, len(a)-2)
    cities = getidlist("select top 50 id from stations where title in(" & a & ")")
    s = s & " and workers.citystationid in(" & cities & "-1)"
  end if
  if session("setshifts_from1filter") <> "" then s = s & " and workers.from1 in(" & getidlist("select top 20 id from stations where title = '" & session("setshifts_from1filter") & "'") & "-1)"
  if session("setshifts_to1filter") <> "" then s = s & " and workers.to1 in(" & getidlist("select top 20 id from stations where title = '" & session("setshifts_to1filter") & "'") & "-1)"
  if session("setshifts_siteid") <> "" and session("setshifts_siteid") <> "0" then s = s & " and workers.siteid = " & session("setshifts_siteid")

  bb = "shetach,hativa,"'contractor_company,
  do until bb = ""
    b = left(bb,instr(bb,",")-1) : bb = mid(bb,instr(bb,",")+1)
    tt = session(tablename & "_" & b)
    a = ""
    do until tt = ""
      t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
      a = a & " + instr(workers.data1, '" & b & "=" & t & "')"
    loop
    if a <> "" then s = s & " and " & mid(a,4) & " > 0"
  loop

  ff = "contractor_company,"
  do until ff = ""
    f = left(ff,instr(ff,",")-1) : ff = mid(ff,instr(ff,",")+1)
    t = session(tablename & "_" & f)
    if t = "n" then
      s = s & " and instr(workers.data1, '|" & ucase(f) & "=') = 0"
    elseif t = "y" then
      s = s & " and instr(workers.data1, '|" & ucase(f) & "=') > 0"
    elseif t <> "" then
      'tt = t & "," : a = ""
      'do until tt = ""
      '  t = trim(left(tt,instr(tt,",")-1)) : tt = mid(tt,instr(tt,",")+1)
      '  if t <> "" then a = a & " + instr(workers.data1, '|" & ucase(f) & "=" & t & "')"
      'loop
      'if a <> "" then s = s & " and " & mid(a,4) & " > 0"

      tt = t & "," : a = ""
      do until tt = ""
        t = trim(left(tt,instr(tt,",")-1)) : tt = mid(tt,instr(tt,",")+1)
        ii = getfieldlist("select title from misc where type1 = 'contractor_company' and title like '%" & t & "%'")
        if countstring(ii, ",") > 40 then ii = "" : du = showerror("���� '" & t & "' - ��� ��� ����� ���� ������")
        do until ii = ""
          i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
          if i <> "" then a = a & " + instr(workers.data1, '" & ucase(f) & "=" & i & "')"
        loop
      loop
      if a = "" then s = s & " and 0=1" else s = s & " and " & mid(a,4) & " > 0"

    end if
  loop


  coresort = "workers.firstname, workers.lastname" : if specs = "iec" and not itslocal then coresort = "workers.lastname, workers.firstname"
  if session("setshifts_orderby") = "unit" then
    s = s & " order by units.title, " & coresort
  elseif session("setshifts_orderby") = "template" then
    s = s & " order by workers.template, " & coresort
  elseif session("setshifts_orderby") = "shifthour" then
    t = "workershifts.hour1, workershifts.hour1b," : if session("setshifts_byshift") = "distribute" then t = "workershifts.hour1b, workershifts.hour1,"
    s = s & " order by " & t & " workershifts.shiftid, " & coresort
  else
    s = s & " order by " & coresort
  end if
  sw = s
  session("setshifts_lastlistsql") = s

  '---alreadyset
  if showfoodtitle = "on" then
    foods = ""
    s = "select id,title from foodsuppliers"
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      foods = setval(foods, rs1("id"), rs1("title"))
      rs1.movenext
    loop
  end if

  s = "select id,workerid,date1,ways,hour1,hour1b,foodsupplierid,from1,to1,courseid,data1,shiftid from workershifts"
  s = s & " where date1 >= " & sqldate(d1) & " and date1 <= " & sqldate(d2)
  if session("setshifts_byshift") = "collect" then s = s & " and instr(ways,'A') > 0"
  if session("setshifts_byshift") = "distribute" then s = s & " and instr(ways,'B') > 0"
  if getfield("csv") = "on" and NOT hasmatch(specs, "isp,babcom,") then
    s = s & " and instr(cstr('' & data1), 'draft=on') = 0"
  elseif session("setshifts_orderby") = "shifthour" or session("setshifts_orderby") = "unit" then 'for a little better performance...
    t = "select top " & session(tablename & "_lines") * (session(tablename & "_page") + 1) & mid(sw,7)
    set rs1 = conn.execute(sqlfilter(t))
    a = ""
    do until rs1.eof
      a = a & rs1("id") & ","
      rs1.movenext
    loop
    s = s & " and workerid in(" & a & "0)"
  else 'for even better performance...
    t = "select top " & session(tablename & "_lines") * (session(tablename & "_page") + 1)
    t = t & " workers.id from workers " & mid(sw, instr(sw,"where"))
    s = s & " and workerid in(" & getidlist(t) & "0)"
  end if

  s = s & " order by workerid, date1, ways, hour1, hour1b"
  set rs1 = conn.execute(sqlfilter(s))
  lastk = "" : v = ""
  set kk = Server.CreateObject("ADODB.Stream") : kk.open
  do until rs1.eof
    k = "u" & rs1("workerid") & "d" & replace(cdate(rs1("date1")),"/","_")
    if k = lastk then v = v & "; " else kk.writetext(lastk & "=" & v & "|") : v = ""
    ways = rs1("ways")
    data1 = rs1("data1")
    if instr("" & ways,"A") > 0 then v = v & translate(" ~Arrival~ ") & rs1("hour1")
    if instr("" & ways, "B") > 0 then v = v & translate(" ~Distribute~ ") & rs1("hour1b") : if rs1("hour1") > rs1("hour1b") then v = v & " ~Tomorrow~"
    if instr("" & ways, "X") > 0 or ways = "" then
      v = v & translate(" ~No Ride~ ")
      if specs = "tnuva" then v = v & " - " & gettitle("shifts", rs1("shiftid")) & " "
      if instr(",brom,packer,", "," & specs & ",") > 0 then v = v & " - " & rs1("hour1") & "-" & rs1("hour1b") & " "
      if specs <> "carlsberg" then
        if instr(",,��� ����,���� ������,", "," & getval(data1, "xtype") & ",") > 0 then t = rs1("hour1") & "-" & rs1("hour1b") : if t <> "00:00-00:00" then v = v & " - " & t & " "
      end if
    end if
    foodsupplierid = rs1("foodsupplierid") : if isnull(foodsupplierid) then foodsupplierid = 0
    if ways <> "X" then
      if ways = "B" then
        if cstr("" & rs1("to1")) <> "0" then v = v & translate(" ~From~") & tocsv(gettitle("stations",rs1("to1")))
        if cstr("" & rs1("from1")) <> "0" then v = v & translate(" ~To ~") & tocsv(gettitle("stations",rs1("from1")))
      else
        if cstr("" & rs1("from1")) <> "0" then v = v & translate(" ~From~") & tocsv(gettitle("stations",rs1("from1")))
        if cstr("" & rs1("to1")) <> "0" then v = v & translate(" ~To ~") & tocsv(gettitle("stations",rs1("to1")))
      end if
      if cstr("" & rs1("courseid")) <> "0" then v = v & translate(" ~Course~ ") & tocsv(getcode("courses",rs1("courseid")))
    end if

    if specs = "iai" and getfield("csv") = "on" then
      t = rs1("courseid") : if isnull(t) then t = ""
      if cstr(t) = "0" then t = getidlist("select courseid1 from workers where id = " & rs1("workerid")) : t = replace(t,",","") : if t = "" then t = "0"
      if cstr(t) <> "0" then v = v & translate(" ~Course~ ") & getcode("courses",t)
    end if

    if cdbl(foodsupplierid) > 0 then
      if showfoodtitle = "on" then
        v = v & " (" & getval(foods,foodsupplierid) & ")"
      else
        v = v & " ~FoodAbbreviation~" & foodsupplierid
      end if
    end if
    if hasmatch(specs, "isp,babcom,") and getval(data1, "draft") = "on" then v = v & " (�����)"

    t = ""
    t = t & "|id=" & rs1("id")
    t = t & "|shiftid=" & rs1("shiftid")
    t = t & "|hour1=" & rs1("hour1")
    t = t & "|hour1b=" & rs1("hour1b")
    t = t & "|from1=" & rs1("from1")
    t = t & "|to1=" & rs1("to1")
    t = t & "|courseid=" & rs1("courseid")
    t = t & "|" & ctxt("" & data1) : t = replace(t,"|","\")
    v = v & "�" & t & "�"
    rs1.movenext
    lastk = k : if rs1.eof then kk.writetext k & "=" & v & "|"
  loop
  kk.position = 0 : alreadyset = kk.readtext(kk.size) : kk.close

  'if rs.state = 1 then rs.close
  'rs.open sqlfilter(sw), conn, 3, 1, 1 : rsrc = rs.recordcount : rc = 0
  s = sw
  s = "select count(workers.id) as x " & mid(s,instr(s, " from ")) : if instr(s, "order by ") > 0 then s = left(s, instr(s, "order by ") - 1)
  set rs = conn.execute(sqlfilter(s))
  rc = 0 : rsrc = rs("x") : if isnull(rsrc) then rsrc = 0

  if specs = "iec" then 
    du = rrpagesbar(session("setshifts_lines"), session("setshifts_page"), rsrc)
    rr "<span style='padding-left:30px;'><a href=iec_report1.asp style='font-size:16px; color:green;'>��� �������</a></span>"
  end if

  '--------toprow----------------------------
  t = "<tr bgcolor=#bbbbbb id=trcoltitles><td id=cell_0_0>~Worker~"
  rr t : coltitles = t : cellx = 0
  d = d1
  do until d > d2
    cellx = cellx + 1
    t = "bbbbbb" : wd = getweekday(d)
    if wd = 6 then t = "999999"
    if wd = 7 then t = "999998"
    if wd = 8 then t = "999998"
    if wd = 9 then t = "999999"
    if wd > 9 then t = "999998"
    t = "<td style='background:#" & t & ";' id=cell_0_" & cellx & "><nobr>"
    rr t : coltitles = coltitles & t
    s = "select id from workershifts where date1 = " & sqldate(d) & " and workerid = -" & session("userid")
    set rs1 = conn.execute(sqlfilter(s))
    rr "<table align=~langside2~><TR><TD>"
    if rs1.eof then
      rr "<a href=#aa onclick='vbscript: if msgbox(""" & d & " - ~Mark day as done~?"",vbyesno) = 6 then document.location = ""?action=setdone&token=" & session("token") & "&d=" & d & """'><img src=vbutton0.png border=0 width=16 height=16 alt='~Mark day as done~'></a>"
    else
      rr "<img src=vbutton1.png width=16 height=16 alt='~Day Marked as Done~'>"
    end if
    rr "</table>"
    rr "<input type=checkbox onclick='vbscript: x = checkalldate(""" & replace(d,"/","_") & """, me.checked)'>"

    t = "~weekday" & weekday(d) & "~ " & day(d) & "/" & month(d) & "/" & year(d)
    rr t : coltitles = coltitles & t
    if getfield("csv") = "on" then csv.writetext(d & " " & translate("~weekday" & weekday(d) & "~ ") & ",")

    t = "<br><img src=blank.gif width=50 height=1>"
    rr t : coltitles = coltitles & t

    if specs = "techjet" then
      sq = "select type1, title from holidays where date1 = " & sqldate(d)
      set rs1 = conn.execute(sqlfilter(sq))
      if not rs1.eof then
        rr "<br><img src=blank.gif width=1 height=5><br><div style='position:absolute;'>" & translate("~weekday" & rs1("type1") & "~") & " - " & rs1("title") & "</div><br>"
      end if
    end if

    d = dateadd("d",1,d)
  loop
  if getfield("csv") = "on" then csv.writetext(vbcrlf)
  '------------------

  if getfield("csv") = "on" then
    s = sw
  else
    s = "select top " & cdbl(session(tablename & "_lines")) * (session(tablename & "_page") + 1) & " " & mid(sw,7)
  end if

  set rs = conn.execute(sqlfilter(s))
  ids = "" : c = 0 : cc = 0 : checkboxusers = "" : checkboxdates = "" : lastgroup = "" : celly = 0

  if specs = "sanmina" then
    if session("shiftovertimes") = "" then
      s = "select id,data1 from shifts"
      set rs1 = conn.execute(sqlfilter(s))
      do until rs1.eof : session("shiftovertimes") = setval(session("shiftovertimes"), rs1("id"), getval(rs1("data1"), "overtime")) : rs1.movenext : loop
    end if
    if day(d1) <= 23 then
      sanmina_e1 = 24 & "/" & month(dateadd("m", -1, d1)) & "/" & year(dateadd("m", -1, d1))
      sanmina_e2 = "23/" & month(d1) & "/" & year(d1)
    else
      sanmina_e1 = 24 & "/" & month(d1) & "/" & year(d1)
      sanmina_e2 = "23/" & month(dateadd("m", +1, d1)) & "/" & year(dateadd("m", +1, d1))
    end if
  end if

  '---list
  do until rs.eof
    rc = rc + 1 : showthis = false : if rc > (session("setshifts_page") * session(tablename & "_lines")) and rc <= (session("setshifts_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    data1 = rs("data1")
    if getfield("csv") = "on" then showthis = true
    if showthis then
      celly = celly + 1 : cellx = 0
      cc = cc + 1 : if cc > 10 and session("scrollwithin") <> "on" then rr coltitles : cc = 1
      i = rs("id") : ids = ids & i & ","
      display = rs("firstname") & " " & rs("lastname") & " " & rs("code")
      if session("setshifts_orderby") = "unit" then t = iif("" & rs("unittitle") = "", "��� ���� ������", rs("unittitle"))  : if lastgroup <> t then rr "<tr><td colspan=99 style='background:#ffffff; border-bottom:1px solid #aa0000; color:#aa0000; padding-~langside~:23;'><b>" & t : lastgroup = t : if getfield("action2") = "doc" then csv.writetext(t & vbcrlf)
      if session("setshifts_orderby") = "template" then t = iif("" & rs("template") = "", "��� �����", rs("template")) : if lastgroup <> t then rr "<tr><td colspan=99 style='background:#ffffff; border-bottom:1px solid #aa0000; color:#aa0000; padding-~langside~:23;'><b>" & t : lastgroup = t : if getfield("action2") = "doc" then csv.writetext(t & vbcrlf)
      if specs = "imi" then display = display & " - " & gettitle("stations", rs("citystationid"))
      if specs = "tikshoov" then display = display & " - " & gettitle("stations", rs("from1"))

      if getfield("csv") = "on" then
        csv.writetext(tocsv(ctxt(rs("firstname")) & " " & ctxt(rs("lastname"))) & ",")
        csv.writetext(tocsv(ctxt(rs("code"))) & ",")
        if specs <> "osem" then csv.writetext(tocsv(ctxt(rs("unittitle"))) & ",")
        if specs <> "osem" then if rs("eligible") = "on" then csv.writetext("v,") else csv.writetext(",")
        if specs = "tikshoov" then csv.writetext(tocsv(gettitle("stations", rs("from1"))) & ",")
        if specs = "techjet" then csv.writetext(tocsv(gettitle("workertypes", rs("workertypeid"))) & ",")
        if specs = "brom" then csv.writetext(tocsv(gettitle("stations", rs("citystationid"))) & ",")
        if hasmatch(specs, "imi,philips,") then csv.writetext(tocsv(rs("phone1")) & ",")
        if specs = "osem" then csv.writetext(tocsv(gettitle("stations", rs("citystationid"))) & ",")
      end if

      'rr "<tr valign=top style='background:#ffffff;' id=tr" & i
      'if i > 0 then rr " onmouseover='vbscript: tr" & i & ".style.background=""ffffbb""' onmouseout='vbscript: tr" & i & ".style.background=""ffffff""'"
      'rr ">"

      t = "f8f8f8" : if rc / 6 = fix(rc/6) or (rc+1) / 6 = fix((rc+1)/6) or (rc+2) / 6 = fix((rc+2)/6) then t = "ffffff"
      rr "<tr valign=top style='background:#" & t & ";' id=tr" & i
      rr " onmouseover='vbscript: tr" & i & ".style.background=""f0f0f0""' onmouseout='vbscript: tr" & i & ".style.background=""" & t & """'"
      rr ">"

      t = "" : if celly = 1 then t = " id=cell_1_0"
      e = 1 : if myworkers <> "" and instr("," & myworkers, "," & i & ",") = 0 then e = 0
      rr "<td" & t & " class=list_item width=1><nobr><input type=checkbox onclick='vbscript: x = checkalluser(""" & i & """, me.checked)'"
      if e = 0 then rr " disabled"
      rr ">"
      if instr(",flextronics,plasan,", "," & specs & ",") > 0 then
        if selectfoodbox = "" then
          selectfoodbox = "<select name=workerfood#WORKERID#><option value='' style='color:#cccccc;'>~workershifts_foodsupplierid~"
          sq = "select id,title from foodsuppliers order by title"
          set rs1 = conn.execute(sqlfilter(sq))
          do until rs1.eof
            selectfoodbox = selectfoodbox & "<option value=" & rs1("id") & ">" & rs1("title")
            rs1.movenext
          loop
          selectfoodbox = selectfoodbox & "</select>"
        end if
        a = selectfoodbox : a = replace(a, "#WORKERID#", rs("id"))
        't = getval(data1, "lastfoodsupplierid") : if t <> "" then a = replace(a, "<option value=" & t & ">", "<option value=" & t & " selected>")
        rr a & " "
      end if

      'if instr(",osem,", "," & specs & ",") > 0 then
      '  if selectunitbox= "" then
      '    selectunitbox = "<select name=workerunit#WORKERID#><option value='' style='color:#cccccc;'>~workers_unitid~"
      '    sq = "select id,title from units"
      '    if session("usersiteid") <> "0" then sq = sq & " where siteid = " & session("usersiteid")
      '    if session("usergroup") <> "admin" then sq = sq & " and id in(" & session("userunitids") & "-1)"
      '    sq = sq & " order by title"
      '    set rs1 = conn.execute(sqlfilter(sq))
      '    do until rs1.eof
      '      a = ""
      '      a = a & "<option value=" & rs1("id")
      '      '--this is done on the cached string--' if cstr(rs1("id")) = cstr(rs("unitid")) then a = a & " selected" 
      '      a = a & ">" & rs1("title")
      '      selectunitbox = selectunitbox & a
      '      rs1.movenext
      '    loop
      '    selectunitbox = selectunitbox & "</select>"
      '  end if
      '  a = selectunitbox : a = replace(a, "#WORKERID#", rs("id"))
      '  t = rs("unitid") : if t <> "" then a = replace(a, "<option value=" & t & ">", "<option value=" & t & " selected>")
      '  rr a & " "
      'end if

      rr "<a href=admin_workers.asp?workersview=-&action=form&id=" & i & "&backto=setshifts.asp?action=list>" & display & "</a>"
      if specs = "sanmina" then
        t = "select shiftid from workershifts where workerid = " & i
        t = t & " and date1 >= " & sqldate(sanmina_e1)
        t = t & " and date1 <= " & sqldate(sanmina_e2)
        a = getfieldlist(t) : x = 0
        do until a = ""
          t = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
          x = x + cdbl("0" & strfilter(getval(session("shiftovertimes"), t), "0123456789."))
        loop
        if x > 60 then rr " <img src=icons/i_warning.gif style='cursor:hand;' alt='����� �� " & x & " ���� ������ ������ " & sanmina_e1 & " �� " & sanmina_e2 & "'>"
      end if
      if specs = "kmc" then
        rr " - " & rs("unittitle")
      end if
      if specs = "techjet" then
        rr " - " & rs("unittitle") & " - " & rs("template")
        rr " - " & gettitle("workertypes", rs("workertypeid"))
      end if
      if rs("eligible") <> "on" then rr " (�� ����)"
      d = d1
      checkboxusers = checkboxusers & i & ","

      '----cells
      do until d > d2
        cellx = cellx + 1
        if cc = 1 then checkboxdates = checkboxdates & replace(d,"/","_") & ","
        k = "u" & i & "d" & replace(d,"/","_")
        t = "" : if celly = 1 then t = " id=cell_1_" & cellx
        rr "<td" & t & " class=list_item><img src=blank.gif width=120 height=1><br>"

        e = 1
        if session("setshifts_shiftid") <> "" and date > d then e = 0
        h = getval(session("sitelock"), rs("siteid")) : if not ishour(h) then h = "17:00"
        if weekday(d) >= 6 then h = getval(session("sitelock"), rs("siteid") & "weekend") : if not ishour(h) then h = "17:00"
        if specs = "leumi" and session("usergroup") = "admin" then h = "23:59"
        if cdate(d) < date or (cdate(d) = date and cdate("1/1/2000 " & time) >= cdate("1/1/2000 " & h)) then e = 0
        if specs = "teva" and cstr(rs("siteid")) = "6" and (cdate(d) <= date or (cdate(d) = dateadd("d",1,date) and cdate("1/1/2000 " & time) >= cdate("1/1/2000 " & h))) then e = 0 'teva shoham liel guetta request
        if getparam("LockSetShiftsOnWeekends") = "y" and dateadd("d",4,date) >= cdate(d) then
          if weekday(date) >= 6 and weekday(d) >= 6 then e = 0
          if weekday(date) = 5 and weekday(d) >= 6 and cdate("1/1/2000 " & time) >= cdate("1/1/2000 " & h) then e = 0
        end if

        if session("lockedholidays") = "" then
          session("lockedholidays") = "1/1/1900," & getfieldlist("select date1 from holidays where date1 >= " & sqldate(date) & " and title like '%����%'")
        end if
        if instr("," & session("lockedholidays"), "," & cdate(d) & ",") > 0 then e = 0

        'if specs = "flextronics" and cstr(rs("siteid")) = getval(session("siteidbynames"), "���� ����") then
        '  'e = 0 : if d = date then e = 1
        '  if d > date then
        '    e = 0 : t = date
        '    do
        '      t = dateadd("d", 1, t)
        '      if getweekday(t) < 6 then exit do
        '    loop
        '    if getweekday(d) >= 6 and d < t then e = 1
        '  end if
        'end if

        j = getval(alreadyset,k)

        shiftdata1 = "" : vv = ""
        do until instr(j, "�") = 0
          v = left(j,instr(j,"�")-1) : j = mid(j,instr(j,"�")+1)
          t = left(j,instr(j,"�")-1) : j = mid(j,instr(j,"�")+1)
          t = replace(t, "\", "|")
          if shiftdata1 = "" then shiftdata1 = t
          if getval(t,"source") = "mob" then v = v & " <img src=icons/phone.png style='cursor:hand;' alt='��� �������'>"
          't = getval(t,"project") : if t <> "" then v = v & " (" & t & ")"
          vv = vv & v
        loop
        j = vv

        if specs = "techjet" and j = "" then e = 0
        g = 0 : if j = "" then j = getval(weekplans, "u" & i & "d" & d) : if j <> "" then g = 1
        j = ctxt(j)

        if enableretro = "on" then e = 1
        if myworkers <> "" and instr("," & myworkers, "," & i & ",") = 0 then e = 0

        rr "<nobr><input type=checkbox name=" & k : if e = 0 then rr " disabled"
        if specs = "iai" and j <> "" then
          'if e = 1 and (instr(j, "  �") > 0 and instr(j, "  �") > 0 and instr(j, "  �����") > 0) or g = 1 then
          if g = 1 or cstr(rs("siteid")) = getval(session("siteidbynames"), "����") or e = 1 and getval(shiftdata1, "from1") <> "0" and getval(shiftdata1, "to1") <> "0" and getval(shiftdata1, "courseid") <> "0" then
            rr " style='padding:4; background:#bbffbb;'"
          elseif e = 1 and j <> "" then
            rr " style='padding:4; background:#D63F2E;'"
          end if
        else
          if e = 1 and j <> "" then rr " style='padding:4; background:bbffbb;'"
        end if
        rr ">"

        if getparam("SetShiftsChooseIndividualShifts") = "y" then
          if selectshiftbox = "" then
            a = ""
            a = a & "<select name=workershift#K# style='font-size:9; width:100;' onchange='vbscript: if me.value = """" then #K#.checked = false else #K#.checked = true'>"
            a = a & "<option value='' style='color:#cccccc;'>~shift~"
            sq = "select id,title from shifts where active='on'"
            if session("managershiftids") <> "" then sq = sq & " and id in(" & session("managershiftids") & "-1)"
            if siteidfilter <> "0" then sq = sq & " and siteid = " & siteidfilter
            sq = sq & " order by title"
            set rs1 = conn.execute(sqlfilter(sq))
            do until rs1.eof
              a = a & "<option value=" & rs1("id") & ">" & rs1("title")
              rs1.movenext
            loop
            a = a & "</select>"
            selectshiftbox = a
          end if
          a = selectshiftbox
          a = replace(a, "#K#", k)
          rr a & " "
        end if

        if hasmatch(specs, "techjet,metropolin,") and getval(shiftdata1, "manually") = "on" then
          rr " <img src=flag1.png width=11 height=11 style='cursor:hand;' alt='~orders_manually~'>"
        end if

        t = getval(shiftdata1, "altaddress")
        if t <> "" then
          t = replace(t,"_"," ")
          rr " <img src=marker.png width=9 height=13 style='cursor:hand;' alt='����� ������: " & tocsv(t) & "'>"
        end if

        if getval(shiftdata1, "draft") = "on" then
          if getparam("ManagerInsertDraftShifts") = "y" and session("usergroup") = "manager" or hasmatch(specs, "kvish6,") then
            rr " <span style='color:#808080;'>(~Draft~) "
          else
            wsid = getval(shiftdata1, "id")
            rr " <span id=draftactions" & wsid & " style='background:#eeeeee; border:1px solid #808080; padding:2; color:#808080;'>"
            rr "   <a target=fr1 style='color:#808080;' href=?action=draftreal&id=" & wsid & "&approveways=AB>���</a>"
            rr " | <a target=fr1 style='color:#808080;' href=?action=draftreal&id=" & wsid & "&approveways=A>�����</a>"
            rr " | <a target=fr1 style='color:#808080;' href=?action=draftreal&id=" & wsid & "&approveways=B>�����</a>"
            if getval(shiftdata1, "rejected") = "on" then
              'nothing
            elseif specs = "sanmina" then
              rr " | <a target=fr1 style='color:#808080;' href=?action=draftreal&id=" & wsid & "&approveways=r>���</a>"
            else
              rr " | <a target=fr1 style='color:#808080;' href=?action=draftreal&id=" & wsid & "&approveways=->���</a>"
            end if
          end if
          if getval(shiftdata1, "rejected") = "on" then rr " <span style='color:#cc0000;'>(~Rejected~)</span> "
          rr "</span>"
        end if

        t = "" : if getval(shiftdata1, "draft") = "on" then t = "color:#aaaaaa;"
        rr "<span id=ws" & getval(shiftdata1, "id") & " style='font-size:10px;" & t & "'> "
        disp = replace(j, "^", "<br><img src=blank.gif width=20 height=1>") : dispcsv = ""

        '----
        if session("setshifts_showshift") = "shifts" then
          regdisp = disp : disp = "" : dispcsv = ""
          a = getval(alreadyset,k) & "�" : lastsd = ""
          do until a = ""
            lastsd = sd : sd = left(a, instr(a,"�") - 1) : a = mid(a,instr(a, "�") + 1) : sd = replace(sd, "\", "|") : sd = ctxt(sd)
            if instr(sd, "shiftid=") > 0 then
              t = getval(sd, "shiftid") : t = getval(session("shiftnames"), t)
              if t = "" then
                h1 = getval(sd, "hour1")
                h2 = getval(sd, "hour1b")
                z = h1 & "-" & h2
                t = getval(session("shifthours"), h1 & "-" & h2)
                t = getval(session("shiftnames"), t)
                if h1 = "00:00" and h2 = "00:00" then t = "(��� ����)"
                if hasmatch(specs, "btl,") then t = z
              end if
              if instr(j, translate("~(����)~")) > 0 then
                h1 = getfromto(replace(j,"^","") & " ", "", translate(" ~Arrival~ "), " ", false, false)
                h2 = getfromto(replace(j,"^","") & " ", "", translate(" ~Distribute~ "), " ", false, false)
                t = getval(session("shifthours"), h1 & "-" & h2)
                if t <> "" then t = translate("(~Fixed~) ") & getval(session("shiftnames"), t)
              end if
              if t = "" then
                t = h1 & "-" & h2
                disp = disp & "<span style='padding:2; background:#cccccc;'>" & t & "</span>"
                dispcsv = dispcsv & tocsv(t) & ";"
              else
                zz = "FF8080,FF80FF,ff5555,FFFF00,40FF40,00FFFF,0080C0,8080C0,FF00FF,00BB00,804040,FF8040,00FF00,008080,8080FF,ff0077,FF0080,008040,0000A0,800080,8000FF,808000,808040,808080,40aaaa,C0C0C0,"
                if instr("�" & colorshifts, "�" & t & "�") = 0 then
                  colorshifts = colorshifts & t & "�"
                  colorshiftsi = colorshiftsi + 1
                  z = colorshiftsi
                else
                  z = left(colorshifts, instr("�" & colorshifts, "�" & t & "�"))
                  z = countstring(z, "�") + 1
                end if
                co = mid(zz, z * 7 + 1, 6)
                if getfield("action2") = "doc" then
                  dispcsv = dispcsv & "<span style='padding:2; background:#" & co & ";'>" & tocsv(t) & "</span>"
                else
                  dispcsv = dispcsv & tocsv(t) & ";"
                end if
                disp = disp & "<span style='padding:2; background:#" & co & ";'>" & t
                if specs = "sheba" then
                  if instr(lastsd, "����") > 0 then disp = disp & " ����"
                  if instr(lastsd, "�����") > 0 then disp = disp & " �����"
                end if
                disp = disp & "</span> "
              end if
            end if
          loop
        end if

        rr disp
        t = getval(shiftdata1, "xtype") : if t <> "" then rr " <span style='color:#0000aa;'>" & t & "</span>"
        t = getval(shiftdata1, "remarks") : if t <> "" then rr " <span style='color:#aaaaaa;'>" & t & "</span>"

        rr "</span>"
        if getfield("csv") = "on" then
          j = replace(j,"����","�") : j = replace(j,"�����","��")
          j = replace(j,"Arrival","From") : j = replace(j,"Distribute","To")
          j = replace(j, "^", " ")
          j = replacefromto(j, "", "<img", ">", "")
          if specs = "iai" and j <> "" and instr(j, translate("~Course~")) = 0 then
            e = rs("courseid1") : if cstr(e) <> "0" then j = j & translate(" ~Course~ ") & getcode("courses",e)
          end if
          j = translate(j)
          if dispcsv <> "" then j = dispcsv
          t = getval(shiftdata1, "xtype") : if t <> "" then j = j & " " & tocsv(t)
          t = getval(shiftdata1, "remarks") : if t <> "" then j = j & " " & tocsv(t)
          csv.writetext(j & ",")
        end if
        d = dateadd("d",1,d)
      loop
      if getfield("csv") = "on" then csv.writetext(vbcrlf)
    end if
    rs.movenext
  loop

  if session("scrollwithin") = "on" then
    rr "</table></div>"
    rr "<script language=vbscript>" & vbcrlf
    rr ":h = screen.height - 500 : h = iif(h < 200, 200, h) : scrollwithin.style.height = h" & vbcrlf
    rr "sub setcoltitlewidth" & vbcrlf
    rr "columntitles.innerhtml = columntitles.innerhtml & ""<span style='background:#dddddd; border-left:1px solid #bbbbbb; height:27; width:20;'><img src=blank.gif width=1 height=25></span>""" & vbcrlf
    for i = 0 to cellx
      t = "" : e = "" : if i = 0 then t = "position:relative; top:-2;" : e = "<img src=blank.gif width=1 height=20>"
      rr "columntitles.innerhtml = columntitles.innerhtml & ""<span style='" & t & "border-left:1px solid #bbbbbb; padding:2px; height:25; width:"" & cell_0_" & i & ".offsetWidth & ""; background:"" & cell_0_" & i & ".style.background & ""; '>"" & cell_0_" & i & ".innerhtml & """ & e & "</span>""" & vbcrlf
    next
    rr "trcoltitles.style.visibility = ""hidden"" : trcoltitles.style.position = ""absolute"" : trcoltitles.style.top = -2000" & vbcrlf
    rr "end sub" & vbcrlf
    rr "x = setTimeout(""setcoltitlewidth"",250,""vbscript"")" & vbcrlf
    rr "</script>" & vbcrlf
  end if

  rr "<iframe name=fr1 width=0 height=0></iframe>"
  du = rrpagesbar(session("setshifts_lines"), session("setshifts_page"), rsrc)

  if getfield("csv") = "on" and getfield("action2") = "doc" then
    s = "select firstname,lastname,username from buusers where id = " & session("userid")
    closers(rs) : set rs = conn.execute(sqlfilter(s))
    u = "" : if specs = "techjet" then u = "�� ����: " & rs("firstname") & " " & rs("lastname") : if u = " " then u = rs("username")
    csv.position = 0 : r = csv.readtext(csv.size) : csv.close
    r = replace(r, ",���,", ",���� ����,",1,1,1)
    if specs = "osem" then r = sortcsv(r, 1, "text") : r = sortcsv(r, 3, "text")
    top = left(r, instr(r,vbcrlf)-1) : r = mid(r, instr(r,vbcrlf)+2)
    if right(top,1) = "," then top = left(top, len(top)-1)
    top = replace(top, ",", "<td><nobr>")
    top = u & "<br><table width=100% style='direction:rtl;' bgcolor=#aaaaaa cellpadding=4 cellspacing=1><tr bgcolor=#dddddd><td><nobr>" & top
    rclear
    rr "<body style='direction:rtl;'><br>"
    c = 0 : cc = 0
    do until r = ""
      b = left(r, instr(r,vbcrlf)-1) : r = mid(r, instr(r,vbcrlf)+2)
      c = c + 1 : cc = cc + 1 : if c = 1 then rr top
      rr "<tr bgcolor=#ffffff><td bgcolor=#eeeeee><nobr>"
      if right(b,1) = "," then b = left(b, len(b)-1)
      b = replace(b, ",", "<td><nobr>")
      rr b
      if c = 30 then
        c = 0
        rr "</table><div style='page-break-before:always;'>&nbsp;</div>"
      end if
    loop
    rend
  end if

  if getfield("csv") = "on" then
    csv.position = 0 : r = csv.readtext(csv.size) : csv.close
    response.clear
    response.ContentType = "application/csv"
    response.AddHeader "Content-Disposition", "filename=1.csv"
    response.write r
    rend
  end if

  rr "<input type=hidden name=ids value=" & ids & ">"
  rr "<tr><td bgcolor=eeeeee colspan=99 >"
  if getparam("ClearAllShiftsButton") = "y" and session("usergroup") = "admin" and session("usersiteid") = "0" then rr "<table align=~langside2~><tr><td><nobr><input type=button class=groovybutton style='width:150;' value='~Clear All Shifts~' onclick='vbscript: if msgbox(""~Clear All Shown Shifts~?"",vbyesno) = 6 then document.location = ""?action=shiftsubmit&action2=delall&token=" & session("token") & "&ids=" & ids & """'></table>"
  if getparam("ShiftFood") = "y" then rr "<table align=~langside2~><tr><td><input type=button class=groovybutton style='width:150;' value='~Clear food selection~' onclick='vbscript: if msgbox(""~Clear food selection~ ~of all selected items~?"",vbyesno) = 6 then f1.action2.value = ""delfood"" : f1.submit'></table>"
  if getparam("UseGoogleMaps") = "y" then rr "<table align=~langside2~><tr><td><input type=button class=groovybutton style='width:150;' value='~Show workers on map~' onclick='vbscript: if msgbox(""~Show workers on map~?"",vbyesno) = 6 then document.location = ""?action=mapworkers""'></table>"
  if getparam("ManagerInsertDraftShifts") = "y" and session("usergroup") = "admin" then
    rr "<input type=hidden name=approveways value=AB>"
    rr "<table align=~langside2~><tr><td><input type=button class=groovybutton style='width:150;' value='��� ������' onclick='vbscript: if msgbox(""��� ������ ~of all selected items~?"",vbyesno) = 6 then f1.target = ""fr1"" : f1.action2.value = ""draftreal"" : f1.submit'></table>"
  end if
  'if session("usergroup") = "admin" and getparam("showordertaxi") = "y" then
  '  rr "<table align=~langside2~><tr><td><input type=button class=groovybutton style='width:150; direction:ltr;' value='~Moove~!' onclick='vbscript: if msgbox(""~Order Taxi~ ~of all selected items~?"",vbyesno) = 6 then f1.target = """" : f1.action2.value = ""ordertaxi"" : f1.submit'></table>"
  'end if
  rr "<script language=vbscript>" & vbcrlf
  rr "function checkalluser(u,v)" & vbcrlf
  rr "  a = """ & checkboxdates & """" & vbcrlf
  rr "  do until a = """"" & vbcrlf
  rr "    d = left(a,instr(a,"","")-1) : a = mid(a,instr(a,"","")+1)" & vbcrlf
  rr "    if not document.getelementbyid(""u"" & u & ""d"" & d).disabled then document.getelementbyid(""u"" & u & ""d"" & d).checked = v" & vbcrlf
  rr "  loop" & vbcrlf
  rr "end function" & vbcrlf
  rr "function checkalldate(d,v)" & vbcrlf
  rr "a = """ & checkboxusers & """" & vbcrlf
  rr "  do until a = """"" & vbcrlf
  rr "    u = left(a,instr(a,"","")-1) : a = mid(a,instr(a,"","")+1)" & vbcrlf
  rr "    if not document.getelementbyid(""u"" & u & ""d"" & d).disabled then document.getelementbyid(""u"" & u & ""d"" & d).checked = v" & vbcrlf
  rr "  loop" & vbcrlf
  rr "end function" & vbcrlf
  rr "</script>" & vbcrlf
  rr "</form>" 'f1

  '----loadcsv - form-----------------
  rr "<tr bgcolor1=#ffffff><form name=flist method=post><input type=hidden name=token value=" & session("token") & "><td colspan=99>"
  rr "<a id=reportslink href=#aa onclick='javascript: reportslink.style.visibility = ""hidden""; reportsdiv.style.height = """";'><img border=0 src=icons/settings.png> <b>~Reports~..</b></a>"
  rr "<div id=reportsdiv style='height:1; overflow:hidden;'>"

  if specs = "techjet" then
    rr " <input type=button class=groovybutton id=buttonshiftcounterreport style='width:150;' onclick='vbscript: window.open(""?action=shiftcounterreport"")' value='��� ������ - ����'>"
    rr " <input type=button class=groovybutton id=buttonshiftcounterreport style='width:150;' onclick='vbscript: window.open(""?action=shiftcounterreport&action2=doc"")' value='��� ������ - ����'>"
    rr " <input type=button class=groovybutton id=buttonmedreport style='width:170;' onclick='vbscript: window.open(""?action=medreport"")' value='��� ������ - ����'>"
    rr " <input type=button class=groovybutton id=buttonmedreport style='width:170;' onclick='vbscript: window.open(""?action=medreport&action2=doc"")' value='��� ������ - ����'>"
    rr " <input type=button class=groovybutton id=buttoncreatecsv style='width:170;' onclick='vbscript: window.open(""?action=groupshifts&csv=on"")' value='��� ����� ����� - ����'>"
    rr " <input type=button class=groovybutton id=buttoncreatecsv style='width:170;' onclick='vbscript: window.open(""?action=groupshifts&csv=on&action2=doc"")' value='��� ����� ����� - ����'>"
  else
    rr " <input type=button class=groovybutton id=buttoncreatecsv style='width:170;' onclick='vbscript: window.open(""?action=groupshifts&csv=on"")' value='~Shifts report~ - ~Excel~'>"
    rr " <input type=button class=groovybutton id=buttoncreatecsv style='width:170;' onclick='vbscript: window.open(""?action=groupshifts&csv=on&action2=doc"")' value='~Shifts report~ - ~Print~'>"
  end if

  if session("usergroup") = "admin" then
    rr " <input type=button class=groovybutton style='width:170;' onclick='vbscript: window.open(""?action=readiness"")' value='~Referent Readiness~'>"
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.submit' value='~Load CSV~'>")
    if getparam("MultiDailyShifts") = "y" then rr " <input type=checkbox name=addition>~As Additional Shifts~"
  end if
  x = 0
  if session("usergroup") = "admin" then x = 1
  if specs = "adama" and session("username") <> "kiki" then x = 0
  if x = 1 then rr " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=_load_shifts.asp>~Load shifts from CSV~</a>"

  rr "<br><br>"
  rr " <input type=button class=groovybutton id=buttonshiftcounterreport style='width:170;' onclick='vbscript: a = ""?action=shiftcounterreport"" : if doemail.checked then window.open(a & ""&shiftcounterreportemail="" & shiftcounterreportemail.value) else window.open(a)' value='~Count shift workers~ - ~Excel~'>"
  rr " <input type=button class=groovybutton id=buttonshiftcounterreport style='width:170;' onclick='vbscript: a = ""?action=shiftcounterreport&action2=doc"" : if doemail.checked then window.open(a & ""&shiftcounterreportemail="" & shiftcounterreportemail.value) else window.open(a)' value='~Count shift workers~ - ~Print~'>"
  rr " <input type=button class=groovybutton id=buttonshiftworkersreport style='width:170;' onclick='vbscript: a = ""?action=shiftworkersreport"" : if doemail.checked then window.open(a & ""&shiftcounterreportemail="" & shiftcounterreportemail.value) else window.open(a)' value='~List shift workers~ - ~Excel~'>"
  rr " <input type=button class=groovybutton id=buttonshiftworkersreport style='width:170;' onclick='vbscript: a = ""?action=shiftworkersreport&action2=doc"" : if doemail.checked then window.open(a & ""&shiftcounterreportemail="" & shiftcounterreportemail.value) else window.open(a)' value='~List shift workers~ - ~Print~'>"
  if specs = "rafael" then
    rr " <input type=hidden id=doemail>"
    rr " <input type=hidden id=shiftcounterreportemail>"
  else
    rr " <input type=checkbox id=doemail>~Send by Email to~"
    rr " <input type=text style='width:200; direction:ltr;' id=shiftcounterreportemail value=""" & getparam("shiftcounterreportemail") & """>"
  end if

  if getparam("ShiftFood") = "y" then
    rr "<br><br>"
    rr "~From~ " & selectdate("flist.reportd1*", "1/" & month(date) & "/" & year(date))
    rr "~To~ " & selectdate("flist.reportd2*", lastday(month(date) & "/" & year(date)) & "/" & month(date) & "/" & year(date))
    ac = "foodreport" : t = "" : if specs = "imi" then ac = "foodreportimi" : t = " & ""&distzone="" & f1.distzone.value"
    rr " <input type=button class=groovybutton style='width:150;' onclick='vbscript: window.open(""?action=" & ac & "&reportd1="" & flist.reportd1.value & ""&reportd2="" & flist.reportd2.value & ""&foodreportstyle="" & flist.foodreportstyle.value & ""&output=screen""" & t & ")' value='~Food report~ - ~print~'>"
    rr " <input type=button class=groovybutton style='width:150;' onclick='vbscript: window.open(""?action=foodreport&reportd1="" & flist.reportd1.value & ""&reportd2="" & flist.reportd2.value & ""&foodreportstyle="" & flist.foodreportstyle.value " & t & ")' value='~Food report~'>"
    if specs = "alliance" then
      rr " <select name=foodreportstyle><option value=alliance>����� ������<option value=''>����� ����</select>"
    else
      rr "<input type=hidden name=foodreportstyle>"
    end if
    rr " <input type=button class=groovybutton style='width:150;' onclick='vbscript: window.open(""?action=countworkerfoods&reportd1="" & flist.reportd1.value & ""&reportd2="" & flist.reportd2.value)' value='~Count meals~'>"
    if showdistzone = "on" and session("usergroup") = "admin" then
      rr " <input type=button class=groovybutton style='width:150;' onclick='vbscript: window.open(""?action=distzonereport&reportd1="" & flist.reportd1.value & ""&reportd2="" & flist.reportd2.value)' value='������ ������ �����'>"
    end if
    if specs = "tama" then
      rr " <input type=button class=groovybutton style='width:150;' onclick='vbscript: window.open(""?action=reporttama&reportd1="" & flist.reportd1.value & ""&reportd2="" & flist.reportd2.value & ""&output=screen"")' value='��� ������ ���'>"
    end if
    if session("usergroup") = "admin" then
      rr "<br><br><input type=button class=groovybutton id=buttonfoodreport2 style='width:120;' onclick='vbscript: submitfoodreport2' value='~Food by unit~'> "
      if session("usergroup") = "admin" then
        rr " <input type=checkbox id=foodreport2doemail>~Send by Email to~"
        rr " <input type=text style='width:200; direction:ltr;' id=foodreport2email value=""" & getparam("foodreport2email") & """>"
      else
        rr "<input type=hidden name=foodreport2doemail>"
      end if
      rr "<script language=vbscript>" & vbcrlf
      rr "sub submitfoodreport2" & vbcrlf
      rr "  t = """" : if document.getelementbyid(""foodreport2doemail"").checked then t = ""&foodreport2email="" & document.getelementbyid(""foodreport2email"").value" & vbcrlf
      rr "  window.open(""?action=foodreport2&reportd1="" & flist.reportd1.value & ""&reportd2="" & flist.reportd2.value & ""&action2=doc"" & t)" & vbcrlf
      rr "end sub" & vbcrlf
      rr "</script>" & vbcrlf
    end if
  end if

  rr "</div>" 'divreports

  rr "</tr></form></table>"

rrbottom()
rend()
%>
