<!--#include file="include.asp"-->
<%admin_login = true : admin_top = true : if getfield("act") = "tdata" then emptytop = "on"%>
<!--#include file="top.asp"-->
<%
session("debug") = ""

act = getfield("act")
if getfield("wizardid") <> "" then session("wizardid") = getfield("wizardid")
'------------------------------------------------------------------
if act = "build" then
  request_id = session("wizardid")
  x = build(request_id)
end if

'------------------------------------------------------------------
if act = "modulesubmit" then

  if getfield("fromexcel") <> "" then
    session("showfromexcel") = "on"
    rr "<script language=vbscript> msgbox ""Excel Loaded Successfuly !"" : document.location = ""admin.asp"" </script>"
    rend
  end if

  display = trim(getfield("display"))
  if display = "" then errorend("_back~Please choose a name for the new module~")
  t = strfilter(lcase(display),"abcdefghijklmnopqrstuvwxyz") : c = 0
  if t = "" then t = "a"
  c = 0
  do
    c = c + 1 : title = t : if c > 1 then title = title & c
    set rs = conn.execute("select id from bumodules where title = '" & title & "'")
    if rs.eof and InStr(LCase(SqlReservedKeywords), "," & title & ",") = 0 then exit do
  loop

  u = "admin_bumodules.asp?action=insert"
  u = u & "&title=" & title
  u = u & "&display=" & curl(display)
  u = u & "&icon=" & getfield("icon")
  u = u & "&addnav=" & getfield("addnav")
  w = 0 : if session("debug") = "on" then w = 300
  rr "<iframe width=" & w & " height=" & w & " src=" & u & "></iframe>"

  act = "fieldform"
end if

'------------------------------------------------------------------
if act = "moduleform" then

  rr("<table width=100% cellspacing=5>")
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=act value=modulesubmit>")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/archives.png' ~iconsize~> ~Module Wizard~")

  rr("<tr valign=top id=display_tr0><td class=form_item1 id=display_tr1><span id=display_caption>~Name~</span><td class=form_item2 id=display_tr2>")
  rr("<input type=text maxlength=20 name=display onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedisplay & """ dir=~langdir~>")

  rr("<tr valign=top id=addnav_tr0><td class=form_item1 id=addnav_tr1><span id=addnav_caption>Location</span><td class=form_item2 id=addnav_tr2>")
    a = "y,s,n,"
    rr("<select name=addnav dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Theaddnav = b then rr(" selected")
      rr(">")
      if b = "y" then rr("Add as new tab")
      if b = "s" then rr("Under settings tab")
      if b = "n" then rr("No Clickable Access")
    loop
    rr("</select>")'form:addnav


  theicon = "icons/star.png"
  rr("<tr valign=top><td class=form_item1>~bumodules_icon~<td class=form_item2>")
  rr(" &nbsp;<span id=dicon onclick='vbscript:dicons.style.visibility = ""visible""' style='cursor:hand;'><img src=""" & theicon & """></span><input type=hidden name=icon value=""" & theicon & """>")
  rr("<table id=dicons style='position:absolute; visibility:hidden; top:200; left:135; border:1px solid #808080; background:eeeeee; width:270; height:100; cursor:hand;'><tr valign=top><td style='padding:5px;' onclick='vbscript: dicons.style.visibility = ""hidden""'>")
  Set fo = fs.getfolder(appfolder & "\icons")
  For Each x In fo.Files
    f = LCase(x.Name)
    If Right(LCase(f), 4) = ".gif" or Right(LCase(f), 4) = ".png" Then
      rr("<a href=#aaa onclick='vbscript:dicon.innerhtml = ""<img src=""""icons/" & f & """"">"" : f1.icon.value=""icons/" & f & """ : dicons.style.visibility = ""hidden""'>")
      rr("<img border=0 src='icons/" & f & "'></a> ")
    End If
  Next
  rr("<center><br><br><a href=#aaa>~Cancel~</a>")
  rr("</table>")


  rr("<tr valign=top id=fromexcel_tr0><td class=form_item1 id=fromexcel_tr1><span id=fromexcel_caption>From Excel <img src=excel.gif> </span><td class=form_item2 id=fromexcel_tr2>")
  rr("<nobr><table align=~langside~><tr><td><iframe id=fromexcel_upload frameborder=0 marginwidth=0 marginheight=0 width=70 height=22 scrolling=no src=upload.asp?box=fromexcel&previewdiv=fromexcel_preview></iframe></table>")
  rr("<input type=text maxlength=100 id=fromexcel name=fromexcel style='width:200;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' value=""" & Thefromexcel & """ dir=ltr><br>")


  rr("<tr class=update_bar valign=top id=icon_tr0><td colspan=2>")
  'rr("<input type=button style='width:100;' value=""~Cancel~"" onclick='vbscript:document.location = ""default.asp""'>")
  rr("<input type=button style='width:100;' value=""~Next~"" onclick='vbscript:f1.submit'>")
  rr "</table>"
  rr("<br><br><br><br>")

end if

'------------------------------------------------------------------
if act = "fieldsubmit" then

  thetdata = getfield("tdata")
  thetdisplay = trim(getfield("display"))

  if instr("number,selectlist,selectlookup,selectlookupmultiauto,childpage,", "," & getfield("tcontrol") & ",") > 0 and thetdata = "" then x = errorend("_back~Please specify required parameters~")

  '----------create lookup table
  if left(thetdata,18) = "createlookuptable," then
    a = thetdata
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    icon = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    display = trim(getfield("display"))
    if display = "" then errorend("_back~Please choose a name for the new module~")
    t = strfilter(lcase(display),"abcdefghijklmnopqrstuvwxyz") : c = 0
    if t = "" then t = "a"
    c = 0
    do
      c = c + 1 : title = t : if c > 1 then title = title & c
      set rs = conn.execute("select id from bumodules where title = '" & title & "'")
      if rs.eof and InStr(LCase(SqlReservedKeywords), "," & title & ",") = 0 then exit do
    loop

    u = "admin_bumodules.asp?action=insert"
    u = u & "&title=" & title
    u = u & "&display=" & curl(display)
    u = u & "&icon=" & icon
    u = u & "&addnav=s"
    u = u & "&action2=createaslookup"
    w = 0 : if session("debug") = "on" then w = 300
    rr "<iframe width=" & w & " height=" & w & " src=" & u & "></iframe>"
    t = "++" : if getfield("tcontrol") = "selectlookupmultiauto" then t = "+"
    thetdata = title & ",title," & t
  end if

  '----------create childpage
  if left(thetdata,16) = "createchildpage," then
    display = trim(getfield("display"))
    if display = "" then errorend("_back~Please choose a name for the new module~")
    t = strfilter(lcase(display),"abcdefghijklmnopqrstuvwxyz") : c = 0
    if t = "" then t = "a"
    c = 0
    do
      c = c + 1 : title = t : if c > 1 then title = title & c
      set rs = conn.execute("select id from bumodules where title = '" & title & "'")
      if rs.eof and InStr(LCase(SqlReservedKeywords), "," & title & ",") = 0 then exit do
    loop

    s = "select title from bumodules where id = " & session("wizardid")
    set rs1 = conn.execute(sqlfilter(s))
    fathertitle = rs1("title")

    '---create new childpage
    if instr(thedata,",") = 0 then
      a = thetdata
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      icon = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      u = "admin_bumodules.asp?action=insert"
      u = u & "&title=" & title
      u = u & "&display=" & curl(display)
      u = u & "&icon=" & icon
      u = u & "&addnav=i"
      u = u & "&action2=createaslookup"
      u = u & "&action3=createaschildpage"
      u = u & "&fathertitle=" & fathertitle
      w = 0 : if session("debug") = "on" then w = 300
      rr "<iframe width=" & w & " height=" & w & " src=" & u & "></iframe>"
    end if

    thetname = title
    thetdisplay = title
    thetdata = fathertitle & "id,title,"
  end if

  '----------------------------
  f = "" : v = ""

  if thetdisplay = "" then errorend("_back~Please choose a name for the this field~")
  t = strfilter(lcase(thetdisplay),"abcdefghijklmnopqrstuvwxyz") : if t = "" then t = "a" : c = 0
  c = 0
  do
    c = c + 1 : thetname = t : if c > 1 then thetname = thetname & c
    set rs = conn.execute("select id from bumodulefields where tid = " & session("wizardid") & " and tname = '" & thetname & "'")
    if rs.eof and InStr(LCase(SqlReservedKeywords), "," & thetname & ",") = 0 and len(thetname) > 1 then exit do
  loop

  thettype = "text"
  if instr(",textareaexpand,htmlarea,selectlookupmultiauto,", "," & getfield("tcontrol") & ",") > 0 then thettype = "memo"
  if instr(",number,selectlookup,", "," & getfield("tcontrol") & ",") > 0 then thettype = "number"
  if instr(",time,", "," & getfield("tcontrol") & ",") > 0 then thettype = "datetime"

  thetlength = "50" : if getfield("tlength") <> "" then thetlength = getfield("tlength")
  tdir = "d" : if instr(",number,time,", "," & getfield("tcontrol") & ",") > 0 then tdir = "e"

  if getfield("enablemonthview") = "on" then
    s = "update bumodules set monthviewfield = '" & thetname & "' where id = " & session("wizardid")
    set rs1 = conn.execute(sqlfilter(s))
  end if

  Session("bumodulefieldscontainerid") = session("wizardid")
  u = "admin_bumodulefields.asp?action=insert"
  u = u & "&containerfield=tid"
  u = u & "&containerid=" & session("wizardid")
  u = u & "&createfield=" & getfield("createfield")
  u = u & "&tid=" & session("wizardid")
  u = u & "&tname=" & curl(thetname)
  u = u & "&tdisplay=" & curl(thetdisplay)
  u = u & "&tcontrol=" & curl(getfield("tcontrol"))
  u = u & "&ttype=" & curl(thettype)
  u = u & "&tlength=" & curl(getfield("tlength"))
  u = u & "&tdata=" & curl(thetdata)
  u = u & "&tdefault=" & curl(getfield("tdefault"))
  u = u & "&tmust=" & curl(getfield("tmust"))
  u = u & "&tdir=" & curl(getfield("tdir"))
  u = u & "&tfilter=" & curl(getfield("tfilter"))
  w = 0 : if session("debug") = "on" then w = 300
  rr "<iframe width=" & w & " height=" & w & " src=" & u & "></iframe>"

  act = "fieldform"
  msg = "~Field Added~"

end if

'------------------------------------------------------------------
if act = "fieldform" then
  rr("<table width=100% cellspacing=5>")
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=act value=fieldsubmit>")
  rr("<input type=hidden name=act2>")
  rr("<input type=hidden name=createfield value=" & getfield("createfield") & ">")

  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/archives.png' ~iconsize~> ~Module Wizard~ - <font color=000000>~Add New Field~</font>")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")

  rr("<tr valign=top id=display_tr0><td class=form_item1 id=display_tr1><span id=display_caption>~Field Name~</span><td class=form_item2 id=display_tr2>")
  rr("<input type=text maxlength=20 name=display onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:220;' value=""" & Thedisplay & """ dir=~langdir~>")

  rr("<tr valign=top><td class=form_item1>~Field Type~<td class=form_item2>")
  rr "<table align=left>"
  s = "f1.tdata.value = """" : f1.tlength.value = ""50"" : f1.tmust.value = """" : f1.tdefault.value = """" : f1.tfilter.value = """" : "
  rr "<tr><td><input type=radio name=tcontrol value=textline checked onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = ""?act=tdata&tcontrol=textline""'> Text line"
  rr "<tr><td><input type=radio name=tcontrol value=textareaexpand   onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = """"'> Long text"
  rr "<tr><td><input type=radio name=tcontrol value=htmlarea         onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = """"'> Rich text"
  rr "<tr><td><input type=radio name=tcontrol value=number           onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = ""?act=tdata&tcontrol=number""'> Number"
  rr "<tr><td><input type=radio name=tcontrol value=time             onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = ""?act=tdata&tcontrol=time""'> Date"
  rr "<tr><td><input type=radio name=tcontrol value=email            onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = """"'> Email"
  rr "<tr><td><input type=radio name=tcontrol value=phone            onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = ""?act=tdata&tcontrol=phone""'> Phone"
  rr "<tr><td><input type=radio name=tcontrol value=site             onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = """"'> Website"
  rr "<tr><td><input type=radio name=tcontrol value=video            onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = """"'> YouTube Video"
  rr "<tr><td><input type=radio name=tcontrol value=upload           onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = """" : f1.tlength.value = 100'> File attachment"
  rr "<tr><td><input type=radio name=tcontrol value=checkbox         onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = """"'> Checkbox"
  rr "<tr><td><input type=radio name=tcontrol value=selectlist       onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = ""?act=tdata&tcontrol=selectlist""'> Select from list of options"
  rr "<tr><td><input type=radio name=tcontrol value=selectlookup     onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = ""?act=tdata&tcontrol=selectlookup""'> Select from another table"
  rr "<tr><td><input type=radio name=tcontrol value=selectlookupmultiauto onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = ""?act=tdata&tcontrol=selectlookup&multi=on""'> Select many from another table"
  rr "<tr><td><input type=radio name=tcontrol value=childpage onclick='vbscript: " & s & " document.getelementbyid(""fr1"").src = ""?act=tdata&tcontrol=selectlookup&childpage=on""'> Child Table"
  rr "</table>"
  rr "<table><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<iframe id=fr1 width=400 height=220 frameborder=0 src=?act=tdata&tcontrol=textline></iframe></table>"

  rr "<tr><td>"

  if session("debug") = "on" then
    rr "<input type=text name=tdata><br>"
    rr "<input type=text name=tlength value=50><br>"
    rr "<input type=text name=tmust><br>"
    rr "<input type=text name=tdefault><br>"
    rr "<input type=text name=tfilter><br>"
    rr "<input type=text name=enablemonthview><br>"
  else
    rr("<input type=hidden name=tdata>")
    rr("<input type=hidden name=tlength value=50>")
    rr("<input type=hidden name=tmust>")
    rr("<input type=hidden name=tdefault>")
    rr("<input type=hidden name=tfilter>")
    rr("<input type=hidden name=enablemonthview>")
  end if

  rr("<tr class=update_bar valign=top><td colspan=2>")
  'rr("&nbsp;&nbsp;&nbsp;<input type=button style='width:100;' value=""~Cancel~"" onclick='vbscript:document.location = ""default.asp""'>")
  rr("&nbsp;&nbsp;&nbsp;<input type=button style='width:100;' value=""~Add Field~"" onclick='vbscript: f1.submit'>")
  rr("&nbsp;&nbsp;&nbsp;<input type=button style='width:100;' value=""~Create Now~"" onclick='vbscript: finish'>")

  rr "<" & "script language=vbscript>"
  rr "sub finish" & vbcrlf
  rr "  x = 6 : if trim(f1.display.value) <> """" then x = msgbox(""~Discard Current Field Definitions~?"",vbyesno)" & vbcrlf
  rr "  if x = 6 then document.location = ""?act=build&comefrom=wizard""" & vbcrlf
  rr "end sub" & vbcrlf
  rr "<" & "/script>"

  rr "</table>"

end if

'------------------------------------------------------------------
if act = "tdata" then

  if getfield("tcontrol") = "textline" then
    rr "<form name=f1>"
    rr "<b>Number of characters</b><br><input type=text name=n value=50 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' onchange='vbscript: sync'>"
    rr "<br><input type=checkbox name=m onchange='vbscript: sync'> Mandatory<br>"
    rr "<br><b>Default Value</b><br><input type=text name=d onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' onchange='vbscript: parent.f1.tdefault.value = me.value'>"
    rr "</form>"
    rr "<" & "script language=vbscript>"
    rr "sub sync" & vbcrlf
    rr "  if not isnumeric(""-"" & f1.n.value) then msgbox ""Invalid Value"" : f1.n.value = 50 " & vbcrlf
    rr "  if cdbl(f1.n.value) > 255 then f1.n.value = 255" & vbcrlf
    rr "  parent.f1.tlength.value = f1.n.value" & vbcrlf
    rr "  if f1.m.checked then parent.f1.tmust.value = ""on"" else parent.f1.tmust.value = """"" & vbcrlf
    rr "end sub" & vbcrlf
    rr "<" & "/script>"

  elseif getfield("tcontrol") = "number" then
    rr "<form name=f1>"
    rr "<b>Number of digits after the point</b><br>"
    rr "<input type=text name=n value=0 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' onchange='vbscript: sync'>"
    rr "<br><input type=checkbox name=s onchange='vbscript: sync'> Sum column in list<br>"
    rr "</form>"
    rr "<" & "script language=vbscript>"
    rr "sub sync" & vbcrlf
    rr "  if not isnumeric(""-"" & f1.n.value) then msgbox ""Invalid Value"" : f1.n.value = 0 " & vbcrlf
    rr "  if cdbl(f1.n.value) > 10 then f1.n.value = 10" & vbcrlf
    rr "  parent.f1.tdata.value = f1.n.value" & vbcrlf
    rr "  if f1.s.checked then parent.f1.tdata.value = parent.f1.tdata.value & ""sum""" & vbcrlf
    rr "end sub" & vbcrlf
    rr "parent.f1.tdefault.value = 0" & vbcrlf
    rr "<" & "/script>"

  elseif getfield("tcontrol") = "time" then
    rr "    <input type=checkbox name=hour onclick='vbscript: if me.checked then parent.f1.tdata.value = ""hour"" else parent.f1.tdata.value = """"'> Include Hour"
    rr "<br><input type=checkbox name=mandatory onclick='vbscript: if me.checked then parent.f1.tmust.value = ""on"" else parent.f1.tmust.value = """"'> Mandatory"
    rr "<br><input type=checkbox name=tdefault onclick='vbscript: if me.checked then parent.f1.tdefault.value = ""now"" else parent.f1.tdefault.value = """"'> Default to Now"
    rr "<br><input type=checkbox name=enablemonthview onclick='vbscript: if me.checked then parent.f1.enablemonthview.value = ""on"" else parent.f1.enablemonthview.value = """"'> Enable calendar view by this field"
    rr "<" & "script language=vbscript>parent.f1.tfilter.value = ""on"" <" & "/script>"

  elseif getfield("tcontrol") = "phone" then
    rr "<form name=f1>"
    rr "<b>Available Prefixes</b><br><input type=text name=p style='width:300;' value='050,051,052,053,054,055,056,057,058,059,02,03,04,07,08,09,' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' onchange='vbscript: sync'>"
    rr "</form>"
    rr "<" & "script language=vbscript>"
    rr "sub sync" & vbcrlf
    rr "  if f1.p.value <> """" and right(f1.p.value,1) <> "","" then f1.p.value = f1.p.value & "",""" & vbcrlf
    rr "  parent.f1.tdata.value = f1.p.value" & vbcrlf
    rr "end sub" & vbcrlf
    rr "<" & "/script>"

  elseif getfield("tcontrol") = "selectlist" then
    rr "<b>List of values, seperated by commas:</b><br>"
    rr "<input type=text style='width:300;' onchange='vbscript: parent.f1.tdata.value = me.value : if me.value <> """" and right(me.value,1) <> "","" then parent.f1.tdata.value = me.value & "",""'>"
    rr "<br><br><b>Default Value</b><br><input type=text name=d onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' onchange='vbscript: parent.f1.tdefault.value = me.value'>"
    rr "<" & "script language=vbscript>parent.f1.tfilter.value = ""on"" <" & "/script>"

  elseif getfield("tcontrol") = "selectlookup" then
    t = """""" : if getfield("childpage") = "on" then t = """createchildpage"" & me.value"
    rr "<b>Choose Table</b><br><select name=t onchange='vbscript: parent.f1.tdata.value = " & t & " : document.getelementbyid(""fr2"").src = ""?act=tdata&tcontrol=selectlookup2&childpage=" & getfield("childpage") & "&modid="" & me.value'>"
    rr "<option value=''>"
    s = "select * from bumodules where left(title,5) <> 'bumod' order by display"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      if getfield("childpage") = "" then rr "<option value='" & rs("id") & "'>" & rs("display")
      rs.movenext
    loop
    rr "<option value='0' style='color:ff0000;'>~Create New Table~"
    rr "</select>"
    rr "<iframe id=fr2 width=100% height=180 frameborder=0></iframe>"
    rr "<" & "script language=vbscript>"
    rr "parent.f1.tfilter.value = ""on"""
    if getfield("multi") <> "on" then rr ": parent.f1.tdefault.value = 0"
    rr "<" & "/script>"

  elseif getfield("tcontrol") = "selectlookup2" and getfield("modid") <> "" then
    if getfield("modid") = "0" then
      rr "<form name=f1>"
      theicon = "icons/star.png"
      rr("<br><br><b>~bumodules_icon~</b><br>")
      rr(" &nbsp;<span id=dicon onclick='vbscript:dicons.style.visibility = ""visible""' style='cursor:hand;'><img src=""" & theicon & """></span><input type=hidden name=icon value=""" & theicon & """ onchange='vbscript: sync'>")
      rr("<div id=dicons style='position:absolute; visibility:hidden; top:70; left:35; border:1px solid #808080; background:eeeeee; width:275; height:60; overflow:auto; cursor:hand; padding:5px;' onclick='vbscript: dicons.style.visibility = ""hidden""'>")
      Set fo = fs.getfolder(appfolder & "\icons")
      For Each x In fo.Files
        f = LCase(x.Name)
        If Right(LCase(f), 4) = ".gif" or Right(LCase(f), 4) = ".png" Then
          rr("<a href=#aaa onclick='vbscript:dicon.innerhtml = ""<img src=""""icons/" & f & """"">"" : f1.icon.value=""icons/" & f & """ : dicons.style.visibility = ""hidden"" : sync'>")
          rr("<img border=0 src='icons/" & f & "'></a> ")
        End If
      Next
      rr("<center><a href=#aaa>~Cancel~</a>")
      rr("</div>")
      rr "</form>"
      t = "createlookuptable" : if getfield("childpage") = "on" then t = "createchildpage"
      rr "<" & "script language=vbscript>"
      rr "sync" & vbcrlf
      rr "sub sync" & vbcrlf
      rr "  parent.parent.f1.tdata.value = """ & t & ","" & f1.icon.value & "",""" & vbcrlf
      rr "end sub" & vbcrlf
      rr "<" & "/script>"

    else
      s = "select title from bumodules where id = " & getfield("modid")
      set rs = conn.execute(sqlfilter(s))
      m = rs("title")
      rr "<form name=f1>"
      if getfield("childpage") = "" then
        t = "Choose fields to show in the select box"
        rr "<br><input type=checkbox name=a onclick='vbscript: sync'> 'Add new value' Button<br>"
        rr "<br><b>~" & t & "~</b><br>"
        s = "select * from bumodulefields where tid = " & getfield("modid") & " and (ttype = 'text' or tcontrol = 'number' or tcontrol = 'time') order by tpriority"
        set rs = conn.execute(sqlfilter(s))
        c = 0 : a = ""
        do until rs.eof
          c = c + 1 : t = "" : if c = 1 then t = " checked" : first = rs("tname")
          rr "<input type=checkbox name=" & rs("tname") & t & " onclick='vbscript: sync'> " & rs("tdisplay") & "<br>"
          a = a & rs("tname") & ","
          rs.movenext
        loop
      end if
      rr "</form>"

      rr "<" & "script language=vbscript>"
      rr "sync" & vbcrlf
      rr "sub sync" & vbcrlf
      rr "  parent.parent.f1.tdata.value = """ & m & ",""" & vbcrlf
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        rr "  if f1." & b & ".checked then did = true : parent.parent.f1.tdata.value = parent.parent.f1.tdata.value & """ & b & ",""" & vbcrlf
      loop
      rr "  if f1.a.checked then parent.parent.f1.tdata.value = parent.parent.f1.tdata.value & ""+""" & vbcrlf
      rr "  if not did then msgbox ""~Must choose at lease one field to show~"" : f1." & first & ".checked = true : sync" & vbcrlf
      rr "end sub" & vbcrlf
      rr "<" & "/script>"
    end if

  end if
  rend
end if

'------------------------------------------------------------------
rrbottom()
rend()
%>


