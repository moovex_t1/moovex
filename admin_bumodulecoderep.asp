<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 21/09/2010 15:08:14

function bumodulecoderepdelitem(i)
  bumodulecoderepdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id=" & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function bumodulecoderepinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "-1" : if getfield("times") <> "" then t = getfield("times")
  f = f & "times," : v = v & t & ","

  t = "s" : if getfield("status") <> "" then t = getfield("status")
  f = f & "status," : v = v & "'" & t & "',"

  t = "" : if getfield("applytoall") <> "" then t = getfield("applytoall")
  f = f & "applytoall," : v = v & "'" & t & "',"

  t = "" : if getfield("text1") <> "" then t = getfield("text1")
  f = f & "text1," : v = v & "'" & t & "',"

  t = "" : if getfield("text2") <> "" then t = getfield("text2")
  f = f & "text2," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "tid" then t = session(tablename & "containerid")
  if getfield("tid") <> "" then t = getfield("tid")
  f = f & "tid," : v = v & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into bumodulecoderep(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from bumodulecoderep"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  bumodulecoderepinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table bumodulecoderep (id autoincrement primary key,title text(50),times number,status text(1),applytoall text(2),text1 memo,text2 memo,tid number)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "bumodulecoderep" : session("tablename") = "bumodulecoderep"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "simple" then ulev = 0
if session("usergroup") = "admin" then ulev = 0
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = bumodulecoderepinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("times") then s = s & "times=" & getfield("times") & ","
  if okedit("status") then s = s & "status='" & getfield("status") & "',"
  if okedit("applytoall") then s = s & "applytoall='" & getfield("applytoall") & "',"
'[  if okedit("text1") then s = s & "text1='" & getfield("text1") & "',"
'[  if okedit("text2") then s = s & "text2='" & getfield("text2") & "',"
  if okedit("text1") then s = s & "text1='" & Curl(Request("text1")) & "',"
  if okedit("text2") then s = s & "text2='" & Curl(Request("text2")) & "',"
']
  if okedit("tid") then s = s & "tid=" & getfield("tid") & ","
  s = left(s, len(s) - 1) & " "
  s = s & "where id=" & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(url)

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(url)

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(title,times,status,applytoall,text1,text2,tid)"
  s = s & " SELECT title,times,status,applytoall,text1,text2,tid"
  s = s & " FROM " & tablename & " where id=" & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  request_action = "form"
  request_id = theid
  msg = msg & " ~Record duplicated~"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = bumodulecoderepdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = bumodulecoderepdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id=" & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if

'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = getfield("backto")
  s = "select * from " & tablename & " where id=" & request_id
  if session("usingsql") = "1" then s = replace(s,"*","bumodulecoderep.id,bumodulecoderep.title,bumodulecoderep.times,bumodulecoderep.status,bumodulecoderep.applytoall,bumodulecoderep.text1,bumodulecoderep.text2,bumodulecoderep.tid",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thetimes = rs("times") : if isnull(Thetimes) then Thetimes = 0
  Thetimes = replace(thetimes,",",".")
  Thestatus = rs("status") : if isnull(Thestatus) then Thestatus = ""
  Theapplytoall = rs("applytoall") : if isnull(Theapplytoall) then Theapplytoall = ""
  Thetext1 = rs("text1") : if isnull(Thetext1) then Thetext1 = ""
  Thetext1 = replace(thetext1,"<br>",vbcrlf)
  Thetext2 = rs("text2") : if isnull(Thetext2) then Thetext2 = ""
'[  Thetext2 = replace(thetext2,"<br>",vbcrlf)
  Thetext2 = replace(thetext2,"<br>",vbcrlf)
  thetext1 = chtm(CEng(thetext1))
  thetext2 = chtm(CEng(thetext2))
']
  Thetid = rs("tid") : if isnull(Thetid) then Thetid = 0
  Thetid = replace(thetid,",",".")
  formcontrols = "title,times,status,applytoall,text1,text2,tid,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  rr("<center><table class=form_table cellspacing=5>" & formtopbar)


  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/finished-work.png' ~iconsize~> ~bumodulecoderep_bumodulecoderep~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~bumodulecoderep_title~<img src=must.gif></span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  end if'form:title
  if oksee("title") then rr(" <img src=icons/i_help.gif style='cursor:hand;' alt='~bumodulecoderep_title_help~'>")

  if oksee("times") then'form:times
    rr(" <span id=times_caption class=form_item3>~bumodulecoderep_times~<img src=must.gif></span> ")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=times value=""" & Thetimes & """>")
  end if'form:times

  if oksee("status") then'form:status
    rr(" <span id=status_caption class=form_item3>~bumodulecoderep_status~</span> ")
'[    rr("<input type=text maxlength=1 name=status onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thestatus & """ dir=~langdir~>")
    rr("<select name=status>")
    rr("<option value=c"): If thestatus = "c" Then rr(" selected")
    rr(">Code Tags - Complete Lines")
    rr("<option value=s"): If thestatus = "s" Then rr(" selected")
    rr(">Simple")
    rr("</select>")
']
  end if'form:status

  if oksee("applytoall") then'form:applytoall
    rr(" <span id=applytoall_caption class=form_item3>~bumodulecoderep_applytoall~</span> ")
    rr("<input type=checkbox name=applytoall")
    if theapplytoall = "on" then rr(" checked")
    rr(">")
  end if'form:applytoall

  if oksee("text1") then'form:text1
    rr("<tr valign=top id=text1_tr0><td class=form_item1 id=text1_tr1><span id=text1_caption>~bumodulecoderep_text1~<img src=must.gif></span><td class=form_item2 id=text1_tr2>")
'[    rr("<textarea name=text1 style='width:301; height:100;' dir=~langdir~>" & thetext1 & "</textarea>")
    rr("<textarea id=qtextarea1 name=text1 dir=ltr wrap=off style='width:777; height:200; font-family:courier new;'>")
    Response.write (thetext1 & "</textarea>" & vbCrLf)
']
  end if'form:text1

  if oksee("text2") then'form:text2
    rr("<tr valign=top id=text2_tr0><td class=form_item1 id=text2_tr1><span id=text2_caption>~bumodulecoderep_text2~</span><td class=form_item2 id=text2_tr2>")
'[    rr("<textarea name=text2 style='width:301; height:100;' dir=~langdir~>" & thetext2 & "</textarea>")
    rr("<textarea id=qtextarea2 name=text2 dir=ltr wrap=off style='width:777; height:200; font-family:courier new;'>")
    Response.write (thetext2 & "</textarea>" & vbCrLf)
']
  end if'form:text2

  if oksee("tid") and session(tablename & "containerfield") <> "tid" then
    rr("<tr valign=top id=tid_tr0><td class=form_item1 id=tid_tr1><span id=tid_caption>~bumodulecoderep_tid~</span><td class=form_item2 id=tid_tr2>")
    if getfield("box") = "tid" then thetid = getfield("boxid")
    rr("<select name=tid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~bumodulecoderep_tid~")
    sq = "select * from bumodules order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thetid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:tid
  elseif okedit("tid") then
    rr("<input type=hidden name=tid value=" & thetid & ">")
  end if'form:tid

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button id=buttonsavereturn style='width:80;' value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button id=buttonsave style='width:80;' value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button id=buttonduplicate style='width:80;' value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button id=buttondelete style='width:80;' value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button id=buttonback style='width:80;' value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("title") then rr("    if f1.title.value <> """" then validfield(""title"") else invalidfield(""title"") : ok = false" & vbcrlf)
  if okedit("times") and session(tablename & "containerfield") <> "times" then
    rr("    if isnumeric(f1.times.value) then" & vbcrlf)
    rr("      if cdbl(f1.times.value) >= -1 and cdbl(f1.times.value) <= 1000000 then" & vbcrlf)
    rr("        validfield(""times"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""times"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""times"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("text1") then rr("    if f1.text1.value <> """" then validfield(""text1"") else invalidfield(""text1"") : ok = false" & vbcrlf)
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    a = ""
    For Each sessionitem in Session.Contents
      t = tablename & "_" : if left(sessionitem,len(t)) = t then a = a & sessionitem & ","
    Next
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      session(b) = ""
    loop
    session(tablename & "_firstcome") = "y"
    session(tablename & "_order") = tablename & ".title"
    session(tablename & "_hidelistcolumns") = "text1,text2,tid,"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_groupseperator") = getfield("groupseperator")
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/finished-work.png' ~iconsize~> ~bumodulecoderep_bumodulecoderep~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button id=buttonadd style='width:80;' value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif> <input type=text style='width:101;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  rr("<select name=stringoption>")
  rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  rr(">~All words~")
  rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  rr(">~Any word~")
  rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  rr(">~Exactly~")
  rr("</select>")

  rr("<input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = "title,times,status,applytoall,text1,text2,tid,"
  t = "," & t : a = hidecontrols : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")
  rr(" <input type=button id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit id=buttonsearch value="" ~Show~ ""> ")

else
  rr(" <a href=?advanced=y>~Advanced Search~</a>")
end if
  rr("</tr></form>")'search

  '-------------sql------------------------------
  session(tablename & "_lines") = 1000000
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & bumodules_1.title) as tidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join bumodules bumodules_1 on " & tablename & ".tid = bumodules_1.id)"

  '-sqladd
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","bumodulecoderep.id,bumodulecoderep.title,bumodulecoderep.times,bumodulecoderep.status,bumodulecoderep.applytoall,bumodulecoderep.text1,bumodulecoderep.text2,bumodulecoderep.tid",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = left(ww,instr(ww," ")-1) : ww = mid(ww,instr(ww," ")+1)
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".times = " & w & " or"
        s = s & " " & tablename & ".status like '%" & w & "%' or"
        s = s & " " & tablename & ".text1 like '%" & w & "%' or"
        s = s & " " & tablename & ".text2 like '%" & w & "%' or"
        s = s & " cstr('' & bumodules_1.title) & ' ' like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("title") then f = "bumodulecoderep.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulecoderep_title~</a> " & a2)
  if oksee("times") then f = "bumodulecoderep.times" : f2 = "times" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("times") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulecoderep_times~</a> " & a2)
  if oksee("status") then f = "bumodulecoderep.status" : f2 = "status" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("status") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulecoderep_status~</a> " & a2)
  if oksee("applytoall") then f = "bumodulecoderep.applytoall" : f2 = "applytoall" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("applytoall") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulecoderep_applytoall~</a> " & a2)
  if oksee("text1") then rr("<td class=list_title><nobr>~bumodulecoderep_text1~")
  if oksee("text2") then rr("<td class=list_title><nobr>~bumodulecoderep_text2~")
  if oksee("tid") and session(tablename & "containerfield") <> "tid" then
    f = "bumodules_1.title" : f2 = "tidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulecoderep_tid~</a> " & a2)
  end if

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Thetimes = rs("times") : if isnull(Thetimes) then Thetimes = 0
      Thetimes = replace(thetimes,",",".")
      Thestatus = rs("status") : if isnull(Thestatus) then Thestatus = ""
      Theapplytoall = rs("applytoall") : if isnull(Theapplytoall) then Theapplytoall = ""
      Thetext1 = rs("text1") : if isnull(Thetext1) then Thetext1 = ""
      Thetext2 = rs("text2") : if isnull(Thetext2) then Thetext2 = ""
      Thetid = rs("tid") : if isnull(Thetid) then Thetid = 0
      Thetid = replace(thetid,",",".")
      Thetidlookup = rs("tidlookup") : if isnull(Thetidlookup) then Thetidlookup = ""

      okwritehere = okwrite(tablename,theid)
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/finished-work.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("times") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & thetimes)
      if oksee("status") then rr("<td class=list_item valign=top align=><nobr>" & thestatus)
      if oksee("applytoall") then rr("<td class=list_item valign=top align=><nobr>") : if theapplytoall = "on" then rr("v")
      thetext1 = thetext1 : if isnull(thetext1) then thetext1 = ""
      thetext1 = replace(thetext1,"<br>"," ") : thetext1 = replace(thetext1,"<","[") : thetext1 = replace(thetext1,">","]") : if len(thetext1) > 30 then thetext1 = left(thetext1,30) & "..."
      if oksee("text1") then rr("<td class=list_item valign=top align=><nobr>" & thetext1)
      thetext2 = thetext2 : if isnull(thetext2) then thetext2 = ""
      thetext2 = replace(thetext2,"<br>"," ") : thetext2 = replace(thetext2,"<","[") : thetext2 = replace(thetext2,">","]") : if len(thetext2) > 30 then thetext2 = left(thetext2,30) & "..."
      if oksee("text2") then rr("<td class=list_item valign=top align=><nobr>" & thetext2)
      if oksee("tid") and session(tablename & "containerfield") <> "tid" then rr("<td class=list_item valign=top align=><nobr>" & thetidlookup)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("times") and session(tablename & "containerfield") <> "times" then rr("<td class=list_total1>") 'times
  if oksee("status") and session(tablename & "containerfield") <> "status" then rr("<td class=list_total1>") 'status
  if oksee("applytoall") and session(tablename & "containerfield") <> "applytoall" then rr("<td class=list_total1>") 'applytoall
  if oksee("text1") and session(tablename & "containerfield") <> "text1" then rr("<td class=list_total1>") 'text1
  if oksee("text2") and session(tablename & "containerfield") <> "text2" then rr("<td class=list_total1>") 'text2
  if oksee("tid") and session(tablename & "containerfield") <> "tid" then rr("<td class=list_total1>") 'tid

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then rr(" <input type=button id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete All Checked~'>")
  if ulev > 1 then

    rr(" <input type=button id=buttonmultiupdate value='~Update~' style='width:80;' onclick='vbscript:if msgbox(""~Update All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if

  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth + 20 : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
