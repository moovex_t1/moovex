<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 08/12/2017 11:44:30

function tbl_iecvisadelitem(i)
  tbl_iecvisadelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function tbl_iecvisainsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "mvxsiteid" then t = session(tablename & "containerid")
  if getfield("mvxsiteid") <> "" then t = getfield("mvxsiteid")
  f = f & "mvxsiteid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "mvxstationid" then t = session(tablename & "containerid")
  if getfield("mvxstationid") <> "" then t = getfield("mvxstationid")
  f = f & "mvxstationid," : v = v & t & ","

  t = "0" : if getfield("sapsiteid") <> "" then t = getfield("sapsiteid")
  f = f & "sapsiteid," : v = v & t & ","

  t = "" : if getfield("sapsitedesc") <> "" then t = getfield("sapsitedesc")
  f = f & "sapsitedesc," : v = v & "'" & t & "',"

  t = "" : if getfield("saprakazgrp") <> "" then t = getfield("saprakazgrp")
  f = f & "saprakazgrp," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into tbl_iecvisa(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from tbl_iecvisa"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  tbl_iecvisainsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table tbl_iecvisa (id autoincrement primary key,mvxsiteid number,mvxstationid number,sapsiteid number,sapsitedesc text(50),saprakazgrp text(10))"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "tbl_iecvisa" : session("tablename") = "tbl_iecvisa"
tablefields = "id,mvxsiteid,mvxstationid,sapsiteid,sapsitedesc,saprakazgrp,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = tbl_iecvisainsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("mvxsiteid") then s = s & "mvxsiteid=0" & getfield("mvxsiteid") & ","
  if okedit("mvxstationid") then s = s & "mvxstationid=0" & getfield("mvxstationid") & ","
  if okedit("sapsiteid") then s = s & "sapsiteid=0" & getfield("sapsiteid") & ","
  if okedit("sapsitedesc") then s = s & "sapsitedesc='" & getfield("sapsitedesc") & "',"
  if okedit("saprakazgrp") then s = s & "saprakazgrp='" & getfield("saprakazgrp") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = tbl_iecvisadelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = tbl_iecvisadelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("mvxsiteid") then csv(0) = csv(0) & "~tbl_iecvisa_mvxsiteid~^"
  if oksee("mvxstationid") then csv(0) = csv(0) & "~tbl_iecvisa_mvxstationid~^"
  if oksee("sapsiteid") then csv(0) = csv(0) & "~tbl_iecvisa_sapsiteid~^"
  if oksee("sapsitedesc") then csv(0) = csv(0) & "~tbl_iecvisa_sapsitedesc~^"
  if oksee("saprakazgrp") then csv(0) = csv(0) & "~tbl_iecvisa_saprakazgrp~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Themvxsiteid = rs("mvxsiteid") : if isnull(Themvxsiteid) then Themvxsiteid = 0
  Themvxsiteid = replace(themvxsiteid,",",".")
  Themvxstationid = rs("mvxstationid") : if isnull(Themvxstationid) then Themvxstationid = 0
  Themvxstationid = replace(themvxstationid,",",".")
  Thesapsiteid = rs("sapsiteid") : if isnull(Thesapsiteid) then Thesapsiteid = 0
  Thesapsiteid = replace(thesapsiteid,",",".")
  Thesapsitedesc = rs("sapsitedesc") : if isnull(Thesapsitedesc) then Thesapsitedesc = ""
  Thesaprakazgrp = rs("saprakazgrp") : if isnull(Thesaprakazgrp) then Thesaprakazgrp = ""
  Themvxsiteidlookup = rs("mvxsiteidlookup") : if isnull(Themvxsiteidlookup) then Themvxsiteidlookup = ""
  Themvxstationidlookup = rs("mvxstationidlookup") : if isnull(Themvxstationidlookup) then Themvxstationidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("mvxsiteid") then csv(rc) = csv(rc) & themvxsiteidlookup & "^"
    if oksee("mvxstationid") then csv(rc) = csv(rc) & themvxstationidlookup & "^"
    if oksee("sapsiteid") then csv(rc) = csv(rc) & replace(thesapsiteid,vbcrlf," ") & "^"
    if oksee("sapsitedesc") then csv(rc) = csv(rc) & replace(thesapsitedesc,vbcrlf," ") & "^"
    if oksee("saprakazgrp") then csv(rc) = csv(rc) & replace(thesaprakazgrp,vbcrlf," ") & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
      s = "" : thisid = "0"
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "mvxsiteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("mvxsiteid") then s = s & "mvxsiteid=" & v & ","
        elseif csvff(i) = "mvxstationid" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("mvxstationid") then s = s & "mvxstationid=" & v & ","
        elseif csvff(i) = "sapsiteid" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("sapsiteid") then s = s & "sapsiteid=" & b & ","
        elseif csvff(i) = "sapsitedesc" then
          if len(b) > 50 then b = left(b,50)
          if okedit("sapsitedesc") then s = s & "sapsitedesc='" & b & "',"
        elseif csvff(i) = "saprakazgrp" then
          if len(b) > 10 then b = left(b,10)
          if okedit("saprakazgrp") then s = s & "saprakazgrp='" & b & "',"
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = tbl_iecvisainsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","tbl_iecvisa.id,tbl_iecvisa.mvxsiteid,tbl_iecvisa.mvxstationid,tbl_iecvisa.sapsiteid,tbl_iecvisa.sapsitedesc,tbl_iecvisa.saprakazgrp",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Themvxsiteid = rs("mvxsiteid") : if isnull(Themvxsiteid) then Themvxsiteid = 0
  Themvxsiteid = replace(themvxsiteid,",",".")
  Themvxstationid = rs("mvxstationid") : if isnull(Themvxstationid) then Themvxstationid = 0
  Themvxstationid = replace(themvxstationid,",",".")
  Thesapsiteid = rs("sapsiteid") : if isnull(Thesapsiteid) then Thesapsiteid = 0
  Thesapsiteid = replace(thesapsiteid,",",".")
  Thesapsitedesc = rs("sapsitedesc") : if isnull(Thesapsitedesc) then Thesapsitedesc = ""
  Thesaprakazgrp = rs("saprakazgrp") : if isnull(Thesaprakazgrp) then Thesaprakazgrp = ""
  formcontrols = "mvxsiteid,mvxstationid,sapsiteid,sapsitedesc,saprakazgrp,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><!img src='icons/cost.png' ~iconsize~> ~tbl_iecvisa_tbl_iecvisa~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("mvxsiteid") and session(tablename & "containerfield") <> "mvxsiteid" then
    rr("<tr valign=top id=mvxsiteid_tr0><td class=form_item1 id=mvxsiteid_tr1><span id=mvxsiteid_caption>~tbl_iecvisa_mvxsiteid~</span><td class=form_item2 id=mvxsiteid_tr2>")
    if getfield("box") = "mvxsiteid" then themvxsiteid = getfield("boxid")
    rr("<select name=mvxsiteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~tbl_iecvisa_mvxsiteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Themvxsiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:mvxsiteid
  elseif okedit("mvxsiteid") then
    rr("<input type=hidden name=mvxsiteid value=" & themvxsiteid & ">")
  end if'form:mvxsiteid

  if oksee("mvxstationid") and session(tablename & "containerfield") <> "mvxstationid" then
    rr("<tr valign=top id=mvxstationid_tr0><td class=form_item1 id=mvxstationid_tr1><span id=mvxstationid_caption>~tbl_iecvisa_mvxstationid~</span><td class=form_item2 id=mvxstationid_tr2>")
    if getfield("box") = "mvxstationid" then themvxstationid = getfield("boxid")
    rr("<select name=mvxstationid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~tbl_iecvisa_mvxstationid~")
    sq = "select * from stations order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Themvxstationid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:mvxstationid
  elseif okedit("mvxstationid") then
    rr("<input type=hidden name=mvxstationid value=" & themvxstationid & ">")
  end if'form:mvxstationid

  if oksee("sapsiteid") then'form:sapsiteid
    rr("<tr valign=top id=sapsiteid_tr0><td class=form_item1 id=sapsiteid_tr1><span id=sapsiteid_caption>~tbl_iecvisa_sapsiteid~</span><td class=form_item2 id=sapsiteid_tr2>")
    rr("<input type=text maxlength=0 name=sapsiteid onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thesapsiteid & """ dir=~langdir~>")
  end if'form:sapsiteid

  if oksee("sapsitedesc") then'form:sapsitedesc
    rr("<tr valign=top id=sapsitedesc_tr0><td class=form_item1 id=sapsitedesc_tr1><span id=sapsitedesc_caption>~tbl_iecvisa_sapsitedesc~</span><td class=form_item2 id=sapsitedesc_tr2>")
    rr("<input type=text maxlength=50 name=sapsitedesc onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thesapsitedesc & """ dir=~langdir~>")
  end if'form:sapsitedesc

  if oksee("saprakazgrp") then'form:saprakazgrp
    rr("<tr valign=top id=saprakazgrp_tr0><td class=form_item1 id=saprakazgrp_tr1><span id=saprakazgrp_caption>~tbl_iecvisa_saprakazgrp~</span><td class=form_item2 id=saprakazgrp_tr2>")
    rr("<input type=text maxlength=10 name=saprakazgrp onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thesaprakazgrp & """ dir=~langdir~>")
  end if'form:saprakazgrp

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  rr(checkformadd)

  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  rr("<script language=vbscript>on error resume next : f1.sapsiteid.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : du = rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".id" : session(tablename & "_hidelistcolumns") = ""
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".id"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><!img src='icons/cost.png' ~iconsize~> ~tbl_iecvisa_tbl_iecvisa~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  if session(tablename & "containerfield") <> "mvxsiteid" then
    rr(" <nobr><img id=mvxsiteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=mvxsiteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~tbl_iecvisa_mvxsiteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_mvxsiteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:mvxsiteid
  end if'search:mvxsiteid
  if session(tablename & "containerfield") <> "mvxstationid" then
    rr(" <nobr><img id=mvxstationid_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=mvxstationid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~tbl_iecvisa_mvxstationid~")
    sq = "select * from stations order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_mvxstationid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:mvxstationid
  end if'search:mvxstationid
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as mvxsiteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".mvxsiteid = sites_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_1.title) as mvxstationidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_1 on " & tablename & ".mvxstationid = stations_1.id)"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","tbl_iecvisa.id,tbl_iecvisa.mvxsiteid,tbl_iecvisa.mvxstationid,tbl_iecvisa.sapsiteid,tbl_iecvisa.sapsitedesc,tbl_iecvisa.saprakazgrp",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_mvxsiteid") <> "" and session(tablename & "containerfield") <> "mvxsiteid" then s = s & " and " & tablename & ".mvxsiteid = " & session(tablename & "_mvxsiteid")
  if session(tablename & "_mvxstationid") <> "" and session(tablename & "containerfield") <> "mvxstationid" then s = s & " and " & tablename & ".mvxstationid = " & session(tablename & "_mvxstationid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_1.title) & ' ' like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".sapsiteid = " & w & " or"
        s = s & " " & tablename & ".sapsitedesc like '%" & w & "%' or"
        s = s & " " & tablename & ".saprakazgrp like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  f = "sites_1.title" : f2 = "mvxsiteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~tbl_iecvisa_mvxsiteid~</a> " & a2)
  if oksee("mvxstationid") and session(tablename & "containerfield") <> "mvxstationid" then
    f = "stations_1.title" : f2 = "mvxstationidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~tbl_iecvisa_mvxstationid~</a> " & a2)
  end if
  if oksee("sapsiteid") then f = "tbl_iecvisa.sapsiteid" : f2 = "sapsiteid" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("sapsiteid") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~tbl_iecvisa_sapsiteid~</a> " & a2)
  if oksee("sapsitedesc") then f = "tbl_iecvisa.sapsitedesc" : f2 = "sapsitedesc" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("sapsitedesc") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~tbl_iecvisa_sapsitedesc~</a> " & a2)
  if oksee("saprakazgrp") then f = "tbl_iecvisa.saprakazgrp" : f2 = "saprakazgrp" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("saprakazgrp") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~tbl_iecvisa_saprakazgrp~</a> " & a2)

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Themvxsiteid = rs("mvxsiteid") : if isnull(Themvxsiteid) then Themvxsiteid = 0
      Themvxsiteid = replace(themvxsiteid,",",".")
      Themvxstationid = rs("mvxstationid") : if isnull(Themvxstationid) then Themvxstationid = 0
      Themvxstationid = replace(themvxstationid,",",".")
      Thesapsiteid = rs("sapsiteid") : if isnull(Thesapsiteid) then Thesapsiteid = 0
      Thesapsiteid = replace(thesapsiteid,",",".")
      Thesapsitedesc = rs("sapsitedesc") : if isnull(Thesapsitedesc) then Thesapsitedesc = ""
      Thesaprakazgrp = rs("saprakazgrp") : if isnull(Thesaprakazgrp) then Thesaprakazgrp = ""
      Themvxsiteidlookup = rs("mvxsiteidlookup") : if isnull(Themvxsiteidlookup) then Themvxsiteidlookup = ""
      Themvxstationidlookup = rs("mvxstationidlookup") : if isnull(Themvxstationidlookup) then Themvxstationidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = themvxsiteidlookup
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/cost.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("mvxstationid") and session(tablename & "containerfield") <> "mvxstationid" then rr("<td class=list_item valign=top align=><nobr>" & themvxstationidlookup)
      if oksee("sapsiteid") then rr("<td class=list_item valign=top align=><nobr>" & thesapsiteid)
      if oksee("sapsitedesc") then rr("<td class=list_item valign=top align=><nobr>" & thesapsitedesc)
      if oksee("saprakazgrp") then rr("<td class=list_item valign=top align=><nobr>" & thesaprakazgrp)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("mvxstationid") and session(tablename & "containerfield") <> "mvxstationid" then rr("<td class=list_total1>") 'mvxstationid
  if oksee("sapsiteid") and session(tablename & "containerfield") <> "sapsiteid" then rr("<td class=list_total1>") 'sapsiteid
  if oksee("sapsitedesc") and session(tablename & "containerfield") <> "sapsitedesc" then rr("<td class=list_total1>") 'sapsitedesc
  if oksee("saprakazgrp") and session(tablename & "containerfield") <> "saprakazgrp" then rr("<td class=list_total1>") 'saprakazgrp

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
