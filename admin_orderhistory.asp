<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 29/05/2013 23:07:15

function orderhistorydelitem(i)
  orderhistorydelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function orderhistoryinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = now : if getfield("date0") <> "" then t = getfield("date0")
  f = f & "date0," : v = v & sqldate(t) & ","

  t = now : if getfield("date1") <> "" then t = getfield("date1")
  f = f & "date1," : v = v & sqldate(t) & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "userid" then t = session(tablename & "containerid")
  if getfield("userid") <> "" then t = getfield("userid")
  f = f & "userid," : v = v & t & ","

  t = "" : if getfield("status") <> "" then t = getfield("status")
  f = f & "status," : v = v & "'" & t & "',"

  t = "" : if getfield("pids") <> "" then t = getfield("pids")
  f = f & "pids," : v = v & "'" & t & "',"

  t = "" : if getfield("course") <> "" then t = getfield("course")
  f = f & "course," : v = v & "'" & t & "',"

  t = "0" : if getfield("price") <> "" then t = getfield("price")
  f = f & "price," : v = v & t & ","

'[  t = "0" : if getfield("orderid") <> "" then t = getfield("orderid")
  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "orderid" then t = session(tablename & "containerid")
']
  f = f & "orderid," : v = v & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into orderhistory(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from orderhistory"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  orderhistoryinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table orderhistory (id autoincrement primary key,date0 datetime,date1 datetime,userid number,status text(20),pids memo,course memo,price number,orderid number)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "orderhistory" : session("tablename") = "orderhistory"
tablefields = "id,date0,date1,userid,status,pids,course,price,orderid,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 1
if session("usergroup") = "supplieradmin" then ulev = 1
if session("usergroup") = "manager" then ulev = 1
if session("usergroup") = "teamleader" then ulev = 1
if session("usergroup") = "approver" then ulev = 1
if session("usergroup") = "approvedone" then ulev = 1
if session("usergroup") = "adminassistant" then ulev = 1
if session("usergroup") = "admin" then ulev = 1
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
if session(tablename & "containerfield") = "orderid" then hidecontrols = hidecontrols & "orderid,"
if instr(",supplier,supplieradmin,", "," & session("usergroup") & ",") > 0 then hidecontrols = hidecontrols & "price,"
if instr("," & session("userperm"), ",prices,") = 0 then hidecontrols = hidecontrols & "price,"
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = orderhistoryinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("date0") then s = s & "date0=" & sqldate(getfield("date0")) & ","
  if okedit("date1") then s = s & "date1=" & sqldate(getfield("date1")) & ","
  if okedit("userid") then s = s & "userid=0" & getfield("userid") & ","
  if okedit("status") then s = s & "status='" & getfield("status") & "',"
  if okedit("pids") then s = s & "pids='" & getfield("pids") & "',"
  if okedit("course") then s = s & "course='" & getfield("course") & "',"
  if okedit("price") then s = s & "price=0" & getfield("price") & ","
  if okedit("orderid") then s = s & "orderid=0" & getfield("orderid") & ","
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = orderhistorydelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = orderhistorydelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if

'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","orderhistory.id,orderhistory.date0,orderhistory.date1,orderhistory.userid,orderhistory.status,orderhistory.pids,orderhistory.course,orderhistory.price,orderhistory.orderid",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thedate0 = rs("date0") : if isnull(Thedate0) then Thedate0 = "1/1/1900"
  Thedate0 = replace(thedate0,".","/")
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Theuserid = rs("userid") : if isnull(Theuserid) then Theuserid = 0
  Theuserid = replace(theuserid,",",".")
  Thestatus = rs("status") : if isnull(Thestatus) then Thestatus = ""
  Thepids = rs("pids") : Thepids = formatidlist(Thepids)
  Thecourse = rs("course") : if isnull(Thecourse) then Thecourse = ""
  Theprice = rs("price") : if isnull(Theprice) then Theprice = 0
  Theprice = replace(theprice,",",".") : Theprice = formatnumber(theprice,2,true,false,false)
  Theorderid = rs("orderid") : if isnull(Theorderid) then Theorderid = 0
  Theorderid = replace(theorderid,",",".") : Theorderid = formatnumber(theorderid,0,true,false,false)
  formcontrols = "date0,date0_day,date0_month,date0_year,date0_hour,date0_insertdate,date0_removedate,date1,date1_day,date1_month,date1_year,date1_hour,date1_insertdate,date1_removedate,userid,status,pids,pids_select,pids_trigger,course,price,orderid,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_clock.gif' ~iconsize~> ~orderhistory_orderhistory~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("date0") then'form:date0
    rr("<tr valign=top id=date0_tr0><td class=form_item1 id=date0_tr1><span id=date0_caption>~orderhistory_date0~</span><td class=form_item2 id=date0_tr2>")
'[    rr(SelectDate("f1.date0:", thedate0))
    rr "<input type=hidden name=date0 value=""" & thedate0 & """>"
    rr thedate0
']
  end if'form:date0

  if oksee("date1") then'form:date1
    rr("<tr valign=top id=date1_tr0><td class=form_item1 id=date1_tr1><span id=date1_caption>~orderhistory_date1~</span><td class=form_item2 id=date1_tr2>")
'[    rr(SelectDate("f1.date1:", thedate1))
    rr "<input type=hidden name=date1 value=""" & thedate1 & """>"
    if instr(thedate1, "/1900") = 0 then rr thedate1
']
  end if'form:date1

  if oksee("userid") and session(tablename & "containerfield") <> "userid" then
    rr("<tr valign=top id=userid_tr0><td class=form_item1 id=userid_tr1><span id=userid_caption>~orderhistory_userid~</span><td class=form_item2 id=userid_tr2>")
'[    if getfield("box") = "userid" then theuserid = getfield("boxid")
'[    rr("<select name=userid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~orderhistory_userid~")
'[    sq = "select * from buusers order by username"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Theuserid) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("username") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:userid
    rr("<input type=hidden name=userid value=" & theuserid & ">")
    rr getuserorworker(theuserid)
']
  elseif okedit("userid") then
    rr("<input type=hidden name=userid value=" & theuserid & ">")
  end if'form:userid

  if oksee("status") then'form:status
    rr("<tr valign=top id=status_tr0><td class=form_item1 id=status_tr1><span id=status_caption>~orderhistory_status~</span><td class=form_item2 id=status_tr2>")
'[    rr("<input type=text maxlength=20 name=status onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thestatus & """ dir=~langdir~>")
    rr("<input type=hidden name=status value=""" & Thestatus & """>")
    rr "~" & thestatus & "~"
']
  end if'form:status

  if oksee("pids") then'form:pids
    rr("<tr valign=top id=pids_tr0><td class=form_item1 id=pids_tr1><span id=pids_caption>~orderhistory_pids~</span><td class=form_item2 id=pids_tr2>")
'[    rr(selectlookupmultiauto("f1.pids",thepids, "icons/suppliers.png", "workers,firstname,lastname,code,", 300, okwritehere))
    disablecontrols = disablecontrols & "pids,"
    t = ":select id,firstname,lastname,code from workers where id in(" & thepids & "0) order by firstname"
    rr(selectlookupmultiauto("f1.pids",thepids, "icons/suppliers.png", "workers,firstname,lastname,code," & t, 400, okwritehere))
']
  end if'form:pids

  if oksee("course") then'form:course
    rr("<tr valign=top id=course_tr0><td class=form_item1 id=course_tr1><span id=course_caption>~orderhistory_course~</span><td class=form_item2 id=course_tr2>")
'[    rr("<input type=text maxlength=50 name=course onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thecourse & """ dir=~langdir~>")
    rr("<input type=hidden value=""" & Thecourse & """>")
    rr thecourse
']
  end if'form:course

  if oksee("price") then'form:price
    rr("<tr valign=top id=price_tr0><td class=form_item1 id=price_tr1><span id=price_caption>~orderhistory_price~<img src=must.gif></span><td class=form_item2 id=price_tr2>")
'[    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=price value=""" & Theprice & """>")
    rr("<input type=hidden value=""" & Theprice & """>")
    rr theprice
']
  end if'form:price

  if oksee("orderid") then'form:orderid
    rr("<tr valign=top id=orderid_tr0><td class=form_item1 id=orderid_tr1><span id=orderid_caption>~orderhistory_orderid~<img src=must.gif></span><td class=form_item2 id=orderid_tr2>")
'[    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=orderid value=""" & Theorderid & """>")
    rr("<input type=hidden name=orderid value=""" & Theorderid & """>")
    rr theorderid
']
  end if'form:orderid

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("price") and session(tablename & "containerfield") <> "price" then
    rr("    if isnumeric(f1.price.value) then" & vbcrlf)
    rr("      if cdbl(f1.price.value) >= -4000000000 and cdbl(f1.price.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""price"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""price"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""price"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("orderid") and session(tablename & "containerfield") <> "orderid" then
    rr("    if isnumeric(f1.orderid.value) then" & vbcrlf)
    rr("      if cdbl(f1.orderid.value) >= -4000000000 and cdbl(f1.orderid.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""orderid"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""orderid"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""orderid"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.status.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date0" : session(tablename & "_hidelistcolumns") = ""
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".date0"
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/i_clock.gif' ~iconsize~> ~orderhistory_orderhistory~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
'[  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  if session("userlevel") = "5" and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
']
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & buusers_1.username) as useridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join buusers buusers_1 on " & tablename & ".userid = buusers_1.id)"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","orderhistory.id,orderhistory.date0,orderhistory.date1,orderhistory.userid,orderhistory.status,orderhistory.pids,orderhistory.course,orderhistory.price,orderhistory.orderid",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
'[  s = s & " order by " & session(tablename & "_order")
  t = session(tablename & "_order")
  if t = "orderhistory.date0" then t = "orderhistory.id"
  s = s & " order by " & t
']
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("date0") then f = "orderhistory.date0" : f2 = "date0" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("date0") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~orderhistory_date0~</a> " & a2)
  if oksee("date1") then f = "orderhistory.date1" : f2 = "date1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("date1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~orderhistory_date1~</a> " & a2)
  if oksee("userid") and session(tablename & "containerfield") <> "userid" then
    f = "buusers_1.username" : f2 = "useridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~orderhistory_userid~</a> " & a2)
  end if
  if oksee("status") then f = "orderhistory.status" : f2 = "status" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("status") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~orderhistory_status~</a> " & a2)
  if oksee("pids") then rr("<td class=list_title><nobr>~orderhistory_pids~")
  if oksee("course") then rr("<td class=list_title><nobr>~orderhistory_course~")
  if oksee("price") then f = "orderhistory.price" : f2 = "price" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("price") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~orderhistory_price~</a> " & a2)
  if oksee("orderid") then f = "orderhistory.orderid" : f2 = "orderid" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("orderid") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~orderhistory_orderid~</a> " & a2)

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
'[  '-------------rows--------------------------
  lastsupplierupdate = ""
']
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thedate0 = rs("date0") : if isnull(Thedate0) then Thedate0 = "1/1/1900"
      Thedate0 = replace(thedate0,".","/")
      Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
      Thedate1 = replace(thedate1,".","/")
      Theuserid = rs("userid") : if isnull(Theuserid) then Theuserid = 0
      Theuserid = replace(theuserid,",",".")
      Thestatus = rs("status") : if isnull(Thestatus) then Thestatus = ""
      Thepids = rs("pids") : Thepids = formatidlist(Thepids)
      Thecourse = rs("course") : if isnull(Thecourse) then Thecourse = ""
      Theprice = rs("price") : if isnull(Theprice) then Theprice = 0
      Theprice = replace(theprice,",",".") : Theprice = formatnumber(theprice,2,true,false,false)
      Theorderid = rs("orderid") : if isnull(Theorderid) then Theorderid = 0
      Theorderid = replace(theorderid,",",".") : Theorderid = formatnumber(theorderid,0,true,false,false)
      Theuseridlookup = rs("useridlookup") : if isnull(Theuseridlookup) then Theuseridlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
'[      rr("<tr bgcolor=ffffff id=tr" & rc)
      'if session(tablename & "_order") = "orderhistory.date0" then
      '  if thedate1 = lastdate1 then thedate1 = "1/1/1900"
      '  if thestatus = laststatus then thestatus = ""
      '  if thepids = lastpids then thepids = ""
      '  if thecourse = lastcourse then thecourse = ""
      '  lastdate1 = Thedate1
      '  laststatus = Thestatus
      '  lastpids = Thepids
      '  lastcourse = Thecourse
      'end if
      rr("<tr bgcolor=ffffff id=tr" & rc)
']
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thedate0
      if isnull(firstcoltitle) then firstcoltitle = "1/1/1900"
      if year(firstcoltitle) = 1900 then firstcoltitle = ""
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/i_clock.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("date1") then rr("<td class=list_item valign=top align=><nobr>") : if year(thedate1) <> 1900 then rr(thedate1)
'[      if oksee("userid") and session(tablename & "containerfield") <> "userid" then rr("<td class=list_item valign=top align=><nobr>" & theuseridlookup)
      if oksee("userid") then
        t = getuserorworker(theuserid)
        rr "<td class=list_item valign=top align=><nobr>" & t
        if session(tablename & "containerid") <> "0" and instr(t, "����� �����") > 0 then
          if lastsupplierupdate = "" then
            lastsupplierupdate = getonefield("select id from orderhistory where orderid = " & session(tablename & "containerid") & " and userid = -9999997 order by date1 desc")
            if lastsupplierupdate = "" then lastsupplierupdate = "0"
          end if
          if cstr(theid) = cstr(lastsupplierupdate) then
            t = getonefield("select history from orders where id = " & theorderid)
            t = getval(t, "supsysuser")
            if t <> "" then rr " " & t
          end if
        end if
      end if
']
'[      if oksee("status") then rr("<td class=list_item valign=top align=><nobr>" & thestatus)
      't = thestatus : if instr(t, ".") > 0 then t = ".~" & replace(t,".","") & "~" else t = "~" & t & "~"
      t = "~" & thestatus & "~"
      if oksee("status") then rr("<td class=list_item valign=top align=><nobr>" & t)
']
'[      t = "" : d = formatidlist(thepids)
'[      sq = "select * from workers where id in(" & d & "0)"
'[      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[      do until rs1.eof
'[        t = t & ","
'[        t = t & " " & rs1("firstname")
'[        t = t & " " & rs1("lastname")
'[        t = t & " " & rs1("code")
'[        rs1.movenext
'[      loop
'[      if t <> "" then t = mid(t,2)
'[      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      t = countstring(thepids, ",") : if cstr(t) = "0" then t = ""
']
      if oksee("pids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      thecourse = thecourse : if isnull(thecourse) then thecourse = ""
      thecourse = replace(thecourse,"<br>"," ") : thecourse = replace(thecourse,"<","[") : thecourse = replace(thecourse,">","]") : if len(thecourse) > 30 then thecourse = left(thecourse,30) & "..."
      if oksee("course") then rr("<td class=list_item valign=top align=><nobr>" & thecourse)
      if oksee("price") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(theprice,2,true,false,true))
'[      if oksee("orderid") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(theorderid,0,true,false,true))
      if oksee("orderid") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(theorderid,0,true,false,false))
']
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("date1") and session(tablename & "containerfield") <> "date1" then rr("<td class=list_total1>") 'date1
  if oksee("userid") and session(tablename & "containerfield") <> "userid" then rr("<td class=list_total1>") 'userid
  if oksee("status") and session(tablename & "containerfield") <> "status" then rr("<td class=list_total1>") 'status
  if oksee("pids") and session(tablename & "containerfield") <> "pids" then rr("<td class=list_total1>") 'pids
  if oksee("course") and session(tablename & "containerfield") <> "course" then rr("<td class=list_total1>") 'course
  if oksee("price") and session(tablename & "containerfield") <> "price" then rr("<td class=list_total1>") 'price
  if oksee("orderid") and session(tablename & "containerfield") <> "orderid" then rr("<td class=list_total1>") 'orderid

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if

  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
