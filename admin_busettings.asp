<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 15/06/2014 12:31:04

function busettingsdelitem(i)
  busettingsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function busettingsinsertrecord
'[  if ulev < 2 then errorend("~Access Denied~")
  if ulev < 5 then errorend("~Access Denied~")
']
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "" : if getfield("description") <> "" then t = getfield("description")
  f = f & "description," : v = v & "'" & t & "',"

  t = "" : if getfield("explain1") <> "" then t = getfield("explain1")
  f = f & "explain1," : v = v & "'" & t & "',"

  t = "0"
  sq = "select max(priority) as t from " & tablename
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  if not rs1.eof then t = rs1("t") : if isnull(t) then t = "0"
  t = cdbl(t) + 10
  if getfield("priority") <> "" then t = getfield("priority")
  f = f & "priority," : v = v & t & ","

  t = "" : if getfield("group1") <> "" then t = getfield("group1")
  f = f & "group1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into busettings(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from busettings"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  busettingsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'['-------------declarations-------------------------------
a = ""
For Each sessionitem in Session.Contents
  t = "getparam_" : if left(sessionitem,len(t)) = t then a = a & sessionitem & ","
Next
do until a = ""
  b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  session(b) = ""
loop
']
's = "create table busettings (id autoincrement primary key,title text(50),description memo,explain1 memo,priority number,group1 text(50))"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "busettings" : session("tablename") = "busettings"
tablefields = "id,title,description,explain1,priority,group1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
if ulev < 5 then
  hidecontrols = hidecontrols & "group1,buttondelete,buttonduplicate,"
  locked = getidlist("select id from busettings where priority < 1800")
  if instr("," & session("userperm"), ",editsettings,") = 0 then
    ulev = 1
    session("userpermissions") = setval(session("userpermissions"), "busettings", ulev)
  end if
end if
if getfield("e") <> "" then
  s = "select id from busettings where lcase(title) = '" & lcase(getfield("e")) & "'"
  set rs = conn.execute(sqlfilter(s))
  if not rs.eof then request_action = "form" : request_id = cstr(rs("id"))
end if

']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = busettingsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
'[  if okedit("title") then s = s & "title='" & getfield("title") & "',"
'[  if okedit("description") then s = s & "description='" & getfield("description") & "',"
'[  if okedit("explain1") then s = s & "explain1='" & getfield("explain1") & "',"
'[  if okedit("priority") then s = s & "priority=0" & getfield("priority") & ","
  if ulev = 5 then s = s & "title='" & getfield("title") & "',"
  t = getfield("description") : if getfield("title") = "Theme" then if t <> "" and right(t,1) <> "/" then t = t & "/"
  s = s & "description='" & t & "',"
  if ulev = 5 then s = s & "explain1='" & getfield("explain1") & "',"
  if ulev = 5 then s = s & "priority=0" & getfield("priority") & ","
']
  if okedit("group1") then s = s & "group1='" & getfield("group1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'['-------------afterupdate-------------

'if request_action = "update" and getfield("refreshed") = "" then
'  x = getparam(getfield("title")) 'refresh cache
'  r = "" : For Each Item in Request.Form : r = r & "&" & item & "=" & Request.Form(Item) : next
'  rred(request.servervariables("script_name") & "?" & request.servervariables("query_string") & "&refreshed=on" & r)
'end if
if request_action = "update" then x = getparam(getfield("title")) 'refresh cache
if request_action = "update" and request_action2 = "stay" then rred("?action=form&id=" & request_id)
if request_action = "update" and request_action2 = "" then rred("?id=" & request_id)

'----------hardcode upload parameters for .net application where upload script still in regular asp and not connected to DB
f = getfield("title") : v = getfield("description")
if instr(1,scriptname,".aspx",1) and instr(1,",UploadAllowedExtentions,UploadAllowedSize,", "," & f & ",",1) > 0 then
  if fs.fileexists(Server.mappath("upload.asp")) then
    Set thefile = fs.opentextfile(Server.mappath("upload.asp"))
    b = ""
    Do Until thefile.atendofstream
      s = thefile.readline
      if instr(1,s,f & " = ",1) > 0 and instr(1,s,"'hardcode",1) > 0 then s = "  " & f & " = """ & v & """ 'hardcode"
      b = b & s & vbCrLf
    Loop
    thefile.Close : set thefile = nothing
    Set thefile = fs.createTextFile(Server.mappath("upload.asp"), True)
    thefile.writeline(b)
    thefile.Close : set thefile = nothing
  end if
end if
']
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(title,description,explain1,priority,group1)"
  s = s & " SELECT title,description,explain1,priority,group1"
  s = s & " FROM " & tablename & " where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  request_action = "form"
  request_id = theid
  msg = msg & " ~Record duplicated~"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = busettingsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if


'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~busettings_title~^"
  if oksee("description") then csv(0) = csv(0) & "~busettings_description~^"
  if oksee("explain1") then csv(0) = csv(0) & "~busettings_explain1~^"
  if oksee("priority") then csv(0) = csv(0) & "~busettings_priority~^"
  if oksee("group1") then csv(0) = csv(0) & "~busettings_group1~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
  Theexplain1 = rs("explain1") : if isnull(Theexplain1) then Theexplain1 = ""
  Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
  Thepriority = replace(thepriority,",",".")
  Thegroup1 = rs("group1") : if isnull(Thegroup1) then Thegroup1 = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("description") then csv(rc) = csv(rc) & replace(thedescription,vbcrlf," ") & "^"
    if oksee("explain1") then csv(rc) = csv(rc) & replace(theexplain1,vbcrlf," ") & "^"
    if oksee("priority") then csv(rc) = csv(rc) & replace(thepriority,vbcrlf," ") & "^"
    if oksee("group1") then csv(rc) = csv(rc) & replace(thegroup1,vbcrlf," ") & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
      s = "" : thisid = "0"
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
'[        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
'[          thisid = b
'[        elseif csvff(i) = "title" then
'[          if len(b) > 50 then b = left(b,50)
'[          if okedit("title") then s = s & "title='" & b & "',"
        if csvff(i) = "title" then
          if len(b) > 50 then b = left(b,50)
          if okedit("title") then s = s & "title='" & b & "',"
          x = getonefield("select id from " & tablename & " where lcase(title) = '" & lcase(b) & "'")
          if x <> "" then thisid = x
']
        elseif csvff(i) = "description" then
          if okedit("description") then s = s & "description='" & b & "',"
        elseif csvff(i) = "explain1" then
          if okedit("explain1") then s = s & "explain1='" & b & "',"
        elseif csvff(i) = "priority" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("priority") then s = s & "priority=" & b & ","
        elseif csvff(i) = "group1" then
          if len(b) > 50 then b = left(b,50)
          if okedit("group1") then s = s & "group1='" & b & "',"
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = busettingsinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'['-------------more actions-------------------------------------
rr "<iframe name=fronoff id=fronoff width=0 height=0 style='position:absolute;'></iframe>"
if request_action = "onoff" then
  if not okwrite(tablename,getfield("id")) then errorend("~Access Denied~")
  s = "select title,description from busettings where id = " & getfield("id")
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rend
  t = rs("description") : if t = "y" then t = "n" else t = "y"
  application.lock : application(appname & "_getparam_" & rs("title")) = t : application.unlock
  s = "update busettings set description = '" & t & "' where id = " & getfield("id")
  set rs1 = conn.execute(sqlfilter(s))
  response.clear
  if t = "y" then t = "onoff1.png" else t = "onoff0.png"
  rr "<script language=vbscript> parent.document.getelementbyid(""onoff" & getfield("id") & """).src = """ & t & """</script>"
  rend
end if

if request_action = "descupdate" then
  response.clear
  if not okwrite(tablename,getfield("id")) then errorend("~Access Denied~")
  s = "select title,description from busettings where id = " & getfield("id")
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rend
  application.lock : application(appname & "_getparam_" & rs("title")) = getfield("description") : application.unlock
  s = "update busettings set description = '" & getfield("description") & "' where id = " & getfield("id")
  set rs = conn.execute(sqlfilter(s))
  rr "<script language=vbscript>"
  rr "parent.document.getelementbyid(""descupdate" & getfield("id") & """).disabled = true" & vbcrlf
  rr "parent.document.getelementbyid(""description" & getfield("id") & """).style.background = ""#ccffcc""" & vbcrlf
  rr "x = setTimeout(""doit"",1000,""vbscript"")" & vbcrlf
  rr "sub doit" & vbcrlf
  rr "parent.document.getelementbyid(""description" & getfield("id") & """).style.background = ""#f5f5f5""" & vbcrlf
  rr "end sub" & vbcrlf
  rr "</script>"
  rend
end if
']
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","busettings.id,busettings.title,busettings.description,busettings.explain1,busettings.priority,busettings.group1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
  Thedescription = replace(thedescription,"<br>",vbcrlf)
  Theexplain1 = rs("explain1") : if isnull(Theexplain1) then Theexplain1 = ""
  Theexplain1 = replace(theexplain1,"<br>",vbcrlf)
  Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
  Thepriority = replace(thepriority,",",".")
  Thegroup1 = rs("group1") : if isnull(Thegroup1) then Thegroup1 = ""
  formcontrols = "title,description,explain1,priority,group1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/settings.png' ~iconsize~> ~busettings_busettings~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~busettings_title~</span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  end if'form:title

  if oksee("description") then'form:description
    rr("<tr valign=top id=description_tr0><td class=form_item1 id=description_tr1><span id=description_caption>~busettings_description~</span><td class=form_item2 id=description_tr2>")
'[    rr("<textarea name=description style='width:301; height:100;' dir=~langdir~>" & thedescription & "</textarea>")
  if hasmatch(lcase(thetitle), "eligible0letter,eligible1letter,") then
    rr("<textarea name=description style='width:301; height:100;' dir=~langdir~>" & thedescription & "</textarea>")
  elseif left(lcase(thetitle),6) = "image_" then
    rr("<table border=0 cellpadding=0 cellspacing=0 align=right><tr><td><div id=previewdescription_>")
    if lcase(right(thedescription,4)) = ".jpg" or lcase(right(thedescription,4)) = ".gif" then
      rr("<a href='" & thedescription & "' target=_blank><img src='" & thedescription & "' width=100 border=0></a>")
    else
     rr("<a href='" & thedescription & "' target=_blank><font style='font-size:18;'><b>" & ucase(mid(thedescription,instrrev(thedescription,".")+1)) & "</a>")
    end if
    rr("</div></table>")
    if okwritehere then rr("<nobr><table align=left><tr><td><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=22 scrolling=no src=upload.asp?box=description&previewdiv=previewdescription_></iframe></table>")
    rr("<input type=text maxlength=200 id=description name=description style='width:200;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' value=""" & Thedescription & """ dir=ltr><br>")
  else
    rr("<input type=text name=description onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedescription & """ dir=ltr>")
  end if
']
  end if'form:description

  if oksee("explain1") then'form:explain1
    rr("<tr valign=top id=explain1_tr0><td class=form_item1 id=explain1_tr1><span id=explain1_caption>~busettings_explain1~</span><td class=form_item2 id=explain1_tr2>")
    rr("<textarea name=explain1 style='width:301; height:100;' dir=~langdir~>" & theexplain1 & "</textarea>")
'[  end if'form:explain1
  end if'form:explain1
  hidecontrols = hidecontrols & "explain1,"
']

  if oksee("priority") then'form:priority
    rr("<tr valign=top id=priority_tr0><td class=form_item1 id=priority_tr1><span id=priority_caption>~busettings_priority~<img src=must.gif></span><td class=form_item2 id=priority_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=priority value=""" & Thepriority & """>")
  end if'form:priority

  if oksee("group1") then'form:group1
    rr("<tr valign=top id=group1_tr0><td class=form_item1 id=group1_tr1><span id=group1_caption>~busettings_group1~</span><td class=form_item2 id=group1_tr2>")
    rr("<input type=text maxlength=50 name=group1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thegroup1 & """ dir=~langdir~>")
  end if'form:group1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
'[    rr(" <input type=button class=groovybutton id=buttonduplicate value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if ulev >= 5 then rr(" <input type=button class=groovybutton id=buttonduplicate value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
']
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("priority") and session(tablename & "containerfield") <> "priority" then
    rr("    if isnumeric(f1.priority.value) then" & vbcrlf)
    rr("      if cdbl(f1.priority.value) >= -4000000000 and cdbl(f1.priority.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""priority"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""priority"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""priority"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
'[  if request_action = "priorityshift" then
  if request_action = "priorityshift" and ulev > 4 then
']
    ff = getfield("field") : i1 = request_id
    s = "select * from " & tablename & " where id = " & i1
    closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
    t = "" : if session(tablename & "containerid") <> "0" then t = " and " & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
    p1 = rs1(ff) : t1 = " < " : t2 = " desc" : if request_action2 = "down" then t1 = " > " : t2 = ""
    s = "select * from " & tablename & " where id <> " & i1 & " and " & ff & t1 & p1 & t & " order by " & ff & t2
    closers(rs2) : set rs2 = conn.execute(sqlfilter(s))
    if not rs2.eof then
      i2 = rs2("id") : p2 = rs2(ff)
      s = "update " & tablename & " set " & ff & " = " & p2 & " where id = " & i1
      closers(rs) : set rs = conn.execute(sqlfilter(s))
      s = "update " & tablename & " set " & ff & " = " & p1 & " where id = " & i2
      closers(rs) : set rs = conn.execute(sqlfilter(s))
    end if
  end if
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".id" : session(tablename & "_hidelistcolumns") = "priority,group1,"
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".id"
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
'[  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/settings.png' ~iconsize~> ~busettings_busettings~ " & t2)
'[  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
'[  if childpage and request_action3 = "minimized" then rend
'[
'[  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
'[  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr "<img src=blank.gif height=5><br>"
']
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"

  '-------------sql------------------------------
  session(tablename & "_lines") = 1000000
  s = "select " & tablename & ".* from (" & tablename & ")"

'[  '-sqladd
  s = s & " and priority >= 0"
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","busettings.id,busettings.title,busettings.description,busettings.explain1,busettings.priority,busettings.group1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
'[  s = s & " order by " & session(tablename & "_order")
  s = s & " order by priority, id"
']
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

'[  '-------------toprow-----------------------------
'[  rr("<tr class=list_title>")
'[  if oksee("title") then f = "busettings.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~busettings_title~</a> " & a2)
'[  if oksee("description") then rr("<td class=list_title><nobr>~busettings_description~")
'[  if oksee("explain1") then rr("<td class=list_title><nobr>~busettings_explain1~")
'[  if oksee("priority") then f = "busettings.priority" : f2 = "priority" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("priority") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~busettings_priority~</a> " & a2)
'[  if oksee("group1") then f = "busettings.group1" : f2 = "group1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("group1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~busettings_group1~</a> " & a2)
    rr "<tr><td colspan=9 height=20>"
    rr "<tr><td colspan=9 height=30 bgcolor=eeeeee id=tabsmenu>"
    rr "<tr><td colspan=9><table cellspacing=0 width=100% >"
    tabsmenu = ""
    tabsmenu = tabsmenu & "<table align=~langside2~ border=0 cellpadding=0 cellspacing=0><tr><td>"
    if instr(siteurl, "/localhost/") + instr(siteurl, "/127.0.0.1/") > 0 and session("settingsview") = "" then session("settingsview") = "all"
    if getfield("settingsview") <> "" then session("settingsview") = getfield("settingsview")
    if session("settingsview") = "all" then
      tabsmenu = tabsmenu & "<a style='color:#aaaaaa;' href=?settingsview=div>~Sections~</a></table>"
    else
      tabsmenu = tabsmenu & "<a style='color:#aaaaaa;' href=?settingsview=all>~All~</a></table>"
    end if
']

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
      Theexplain1 = rs("explain1") : if isnull(Theexplain1) then Theexplain1 = ""
      Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
      Thepriority = replace(thepriority,",",".")
      Thegroup1 = rs("group1") : if isnull(Thegroup1) then Thegroup1 = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
'[      rr("<tr bgcolor=ffffff id=tr" & rc)

      if trim(thegroup1) <> "" and session("settingsview") = "all" then
        rr "<tr><td colspan=99 style='background:#cccccc; color:#D54937; font-size:20; font-weight:bold; padding-~langside~:20; border-bottom:1px solid #000000;'>~" & thegroup1 & "~"
      elseif trim(thegroup1) <> "" and session("settingsview") <> "all" then
        t = "visibility:hidden; top:-4000;" : if rc = 1 then t = ""
        rr "</table><table id=tabdiv" & rc & " cellspacing=0 cellpadding=4 border=0 style='width:100%; position:absolute; " & t & "'>"
        rr "<tr><td colspan=99 height=30 style='background:#cccccc; color:#D54937; font-size:20; padding-~langside~:20; border-bottom:1px solid #000000;'><b>~" & thegroup1 & "~"
        tabshide = tabshide & " tabdiv" & rc & ".style.visibility = ""hidden"" : tabdiv" & rc & ".style.top = ""-4000"" : document.getelementbyid(""tabslink" & rc & """).style.fontweight = """"" & vbcrlf
        t = "" : if rc = 1 then t = "font-weight:bold;"
        tabsmenu = tabsmenu & " &nbsp;&nbsp;&nbsp;&nbsp;<a href=#aa id=tabslink" & rc & " style='text-decoration:none; " & t & "' onclick='vbscript: tabshide : tabdiv" & rc & ".style.visibility = ""visible"" : tabdiv" & rc & ".style.top = """" : me.style.fontweight = ""bold""'>"
        tabsmenu = tabsmenu & " <img src=arrow_small_~langside2~.gif border=0> ~" & thegroup1 & "~</a>"
      end if
      rr("<tr bgcolor=ffffff id=tr" & rc)

']
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : t = "" : if instr("," & request_ids, "," & i & ",") > 0 then t = " checked"
      if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & t & ">")
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"

'[      if session(tablename & "_order") = tablename & ".priority" then
      if session("busettings_order") = "busettings.priority" and ulev > 4 then
']
        rr("<a href=?action=priorityshift&action2=up&field=priority&id=" & theid & "><img src=up.gif border=0></a>")
        rr("<a href=?action=priorityshift&action2=down&field=priority&id=" & theid & "><img src=down.gif border=0></a> ")
      end if

      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/settings.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
'[      thedescription = thedescription : if isnull(thedescription) then thedescription = ""
'[      thedescription = replace(thedescription,"<br>"," ") : thedescription = replace(thedescription,"<","[") : thedescription = replace(thedescription,">","]") : if len(thedescription) > 30 then thedescription = left(thedescription,30) & "..."
'[      if oksee("description") then rr("<td class=list_item valign=top align=><nobr>" & thedescription)
      if oksee("description") then
        rr("<td class=list_item valign=top align=><nobr>")
        if thedescription <> "y" and thedescription <> "n" and okwrite(tablename,theid) and instr("," & locked, "," & theid & ",") = 0 then
          'rr "<a href=?action=form&id=" & theid & "><span style='overflow:hidden; cursor:hand; width:200; padding:4;  border:1px solid #dddddd; background:#fafafa; font-family:courier new;'>" & thedescription & "</span></a>"
          rr "<input type=text name=description" & theid & " style='width:300; height:25; direction:ltr; padding:4;  border:1px solid #dddddd; background:#fafafa; font-family:courier new;' onkeydown='vbscript: descupdate" & theid & ".disabled = false' value=""" & thedescription & """>"
          rr "<input type=button id=descupdate" & theid & " value='~Save~' style='height:25;' onclick='vbscript: fronoff.document.location = ""?action=descupdate&token=" & session("token") & "&id=" & theid & "&description="" & curl(description" & theid & ".value)' disabled>"
        elseif thedescription <> "y" and thedescription <> "n" then
          rr "<span style='overflow:hidden; width:200; padding:4;  border:1px solid #dddddd; background:#fafafa; font-family:courier new;'>" & thedescription & "</span>"
        elseif okwrite(tablename,theid) and instr("," & locked, "," & theid & ",") = 0 then
          t = "0" : if thedescription = "y" then t = "1"
          rr "<img src=onoff" & t & ".png id=onoff" & theid & " style='cursor:hand;' onclick='vbscript: fronoff.document.location = ""?action=onoff&token=" & session("token") & "&id=" & theid & """'>"
        else
          if thedescription = "y" then rr "<img src=onoff1.png>" else rr "<img src=onoff0.png>"
        end if
      end if

']
'[      theexplain1 = theexplain1 : if isnull(theexplain1) then theexplain1 = ""
'[      theexplain1 = replace(theexplain1,"<br>"," ") : theexplain1 = replace(theexplain1,"<","[") : theexplain1 = replace(theexplain1,">","]") : if len(theexplain1) > 30 then theexplain1 = left(theexplain1,30) & "..."
'[      if oksee("explain1") then rr("<td class=list_item valign=top align=><nobr>" & theexplain1)
      t = "~param_" & thetitle & "~" : expl = translate(t) : if expl = replace(t,"~","") then expl = ""
      if oksee("explain1") then rr("<td class=list_item valign=top width=100% style='color:#808080;'>" & expl)
']
      if oksee("priority") then rr("<td class=list_item valign=top align=><nobr>" & thepriority)
      if oksee("group1") then rr("<td class=list_item valign=top align=><nobr>" & thegroup1)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

'[  '-------------bottom-----------------------
    rr "<div id=dummydiv style='position:absolute; visibility:hidden;'>" & tabsmenu & "</div>"
    rr "<script language=vbscript>" & vbcrlf
    rr "tabsmenu.innerhtml = dummydiv.innerhtml" & vbcrlf
    rr "function tabshide" & vbcrlf
    rr tabshide & vbcrlf
    rr "end function" & vbcrlf
    rr "</script>" & vbcrlf

']
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("description") and session(tablename & "containerfield") <> "description" then rr("<td class=list_total1>") 'description
  if oksee("explain1") and session(tablename & "containerfield") <> "explain1" then rr("<td class=list_total1>") 'explain1
  if oksee("priority") and session(tablename & "containerfield") <> "priority" then rr("<td class=list_total1>") 'priority
  if oksee("group1") and session(tablename & "containerfield") <> "group1" then rr("<td class=list_total1>") 'group1

'[  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
'[  rr("~All Results~ (" & rsrc & ")")
']
  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
'[  if ulev > 1 and okedit("buttonloadcsv") then
  if session("userlevel") = "5" then
']
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
'[rrbottom()
']
rend()
%>
