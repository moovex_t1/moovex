<!--#Include file="include.Asp"-->
<%

if getfield("action") = "sessioncsv" then
  response.clear
  response.ContentType = "application/csv"
  response.AddHeader "Content-Disposition", "filename=1.csv"
  response.write session("csv")
  rend
end if

admin_login = true
if getfield("action") = "reportshowchart" then
  rr "<html><meta http-equiv=""X-UA-Compatible"" content=""IE=11"" /><body>"
else
  admin_top = true
end if
%>
<!--#include file="top.asp"-->
<%
averagetitle = "All Average" : if specs = "teva" then averagetitle = "Region"
if instr(",admin,finance,", "," & session("usergroup") & ",") = 0 then rr "Access Denied" : rend
server.scripttimeout = 100000 'seconds

'-------------------------------------------form-------------------------------------------------------
if getfield("action") = "" then
  rr("<br><center><table cellspacing=10 cellpadding=5>")
  rr "<form name=f1 id=f1><input type=hidden name=action value=''><input type=hidden name=action2 value=''><input type=hidden name=action3 value=''>"
  rr("<tr><td style='background1:#000080; color1:#ffffff; font-size:20;' colspan=99>~Chart Builder~")
  t = " style='width:25%; background:#dddddd; border-bottom:1px solid #808080;'"
  rr "<tr><td" & t &">~Time spans~<td" & t &">~Sites~<td" & t &">~Aditional division~<td" & t &">~Report type~"

  '---------spans------------------
  rr "<tr valign=top><td><nobr>"
  yyy = getfield("yyy") : if yyy = "" then yyy = year(date) & ",;,"
  if getfield("action3") = "yyyadd" then yyy = yyy & year(date) & ",;,"
  a = "yyydel"
  if left(getfield("action3"),len(a)) = a then
    j = strfilter(getfield("action3"), "0123456789")
    r = "" : a = yyy : i = 0
    do until a = ""
      i = i + 1
      b1 = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      b2 = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      if cstr(i) <> cstr(j) then r = r & b1 & "," & b2 & ","
    loop
    yyy = r
  end if
  aa = yyy
  rr "<input type=hidden name=yyy value='" & aa & "'>"
  i = 0 : ss = ""
  divstyle = "<div style='background1:#dddddd; border1:1px solid #dddddd; margin-bottom:5; height:25;'>"
  do until aa = ""
    i = i + 1
    yyy = left(aa,instr(aa,",")-1) : aa = mid(aa,instr(aa,",")+1)
    mmm = left(aa,instr(aa,",")-1) : aa = mid(aa,instr(aa,",")+1)
    rr divstyle
    rr "<a href=#aa onclick='vbscript: f1.action.value = """" : f1.action3.value = ""yyydel" & i & """ : f1.submit'><img src=minus.gif border=0></a> "

    rr "&nbsp;&nbsp;&nbsp;"
    rr " <select name=yyy" & i & " onchange='vbscript:updateyyy'>"
    for j = year(date) - 10 to year(date) + 2 : rr "<option value=" & j & iif(yyy = cstr(j), " selected", "") & ">" & j : next
    rr "</select>"

    rr " <select name=mmm" & i & " onchange='vbscript:updateyyy'>"
    for j = 1 to 12 : rr "<option value=" & j & ";" & iif(mmm = j & ";", " selected", "") & ">" & j : next
    for j = 1 to 4 : v = "Q" & j & ";" : rr "<option value='" & v & "'" & iif(mmm = v, " selected", "") & ">Q" & j : next
    for j = 1 to 2 : v = "H" & j & ";" : rr "<option value='" & v & "'" & iif(mmm = v, " selected", "") & ">H" & j : next
    v = ";"                            : rr "<option value='" & v & "'" & iif(mmm = v, " selected", "") & ">All Year"
    v = "H1;H2;"                       : rr "<option value='" & v & "'" & iif(mmm = v, " selected", "") & ">2 Halves"
    v = "Q1;Q2;Q3;Q4;"                 : rr "<option value='" & v & "'" & iif(mmm = v, " selected", "") & ">4 Quarters"
    v = "1;2;3;4;5;6;7;8;9;10;11;12;"  : rr "<option value='" & v & "'" & iif(mmm = v, " selected", "") & ">12 Months"
    rr "</select>"

    rr "</div>"
    ss = ss & " t = t & f1.yyy" & i & ".value & "","" & f1.mmm" & i & ".value & "",""" & vbcrlf
  loop
  rr divstyle
  rr "<a href=#aa onclick='vbscript: f1.action.value = """" : f1.action3.value = ""yyyadd"" : submit' style='color:#0000aa;'><img src=plus.gif border=0>&nbsp;&nbsp;&nbsp; ~Add~</a>"
  rr "</div>"
  rr "<script language=vbscript>" & vbcrlf
  rr "  sub updateyyy" & vbcrlf
  rr "    t = """"" & vbcrlf
  rr ss & vbcrlf
  rr "    f1.yyy.value = t" & vbcrlf
  rr "  end sub" & vbcrlf
  rr "</script>" & vbcrlf

  '---------sites------------------
  rr "<td><nobr>"
  sss = getfield("sss") : if sss = "" then sss = "0,"

  c = 0 : s = "" : a = "0,99999," & getidlist("select id from sites where active = 'on' order by title")
  cc = countstring(a,",")
  do until a = ""
    c = c + 1
    b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
    rr("<input type=checkbox name=sss_c" & c)
    if instr("," & sss & ",","," & b & ",") > 0 then rr(" checked")
    rr " onclick='vbscript:if me.checked then f1.sss.value = formatidlistunique(f1.sss.value & """ & b & ","") else t = f1.sss.value : t = replace("","" & t, ""," & b & ","", "","") : t = mid(t,2) : f1.sss.value = t'>"
    n = gettitle("sites",b) : if specs = "shufersal" then t = getonefield("select data1 from sites where id = 0" & b) : n = n & " " & getval(t, "branchname")
    if b = "0" then n = "<b>~All sites~</b>"
    if b = "99999" then n = "<b>~All sites~ (~Average per site~)</b>"
    rr n & "<br>"
    s = s & "if f1.sss_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
  loop
  rr "<input type=hidden name=sss value=" & sss & ">"

  '---------divs------------------
  rr "<td>"
  ooo = getfield("ooo")
  rr "<select name=ooo>"
  rr "<option value=''" & iif(ooo = "", " selected", "") & ">~None~"
  rr "<option value='ordertypes'" & iif(ooo = "ordertypes", " selected", "") & ">~ordertypes_ordertypes~"
  rr "<option value='reasons'" & iif(ooo = "reasons", " selected", "") & ">~reasons_reasons~"
  rr "<option value='statuses'" & iif(ooo = "statuses", " selected", "") & ">~Status~"
  rr "</select>"

  '---------type------------------
  rr "<td>"
  rr "<select name=reporttype>"
  rr "<option value=sum" & iif(getfield("reporttype") = "sum", " selected", "") & ">~Order Cost~"
  rr "<option value=count" & iif(getfield("reporttype") = "count", " selected", "") & ">~Order Count~"
  rr "<option value=avg" & iif(getfield("reporttype") = "avg", " selected", "") & ">~Worker average per ride~"
  rr "<option value=avgeff" & iif(getfield("reporttype") = "avgeff", " selected", "") & ">~Worker average per ride~ + ���� ������"
  rr "</select>"

  '--------
  rr "<tr><td colspan=99 align=~langside2~ style='background:#dddddd;'>"
  rr "<a href=#aa onclick='vbscript: f1.action.value = ""report"" : f1.submit' style='width:116; height:31; background-image:url(bluebutton.png); text-align:center; padding-top:8; font-weight:bold; color:#ffffff; font-decoration:none;'>~Produce Report~</a>"

  rr "</table></form>"
  rend
end if

'-------------------------------------------report-------------------------------------------------------
if getfield("action") = "report" then
  numberofsites = ""
  rr "<br><div style='font-size:18; font-weight:bold; text-align:center;'>"
  if getfield("reporttype") = "sum" then rr "���� �����"
  if getfield("reporttype") = "count" then rr "���� ������"
  if getfield("reporttype") = "avg" then rr "����� ����� ������"
  if getfield("reporttype") = "avgeff" then rr "����� ����� ������ ��� ���� ������"
  rr "</div>"

  request_sss = getfield("sss")
  if left(getfield("reporttype"),3) = "avg" then request_sss = replace("," & request_sss, ",99999,", ",") : request_sss = mid(request_sss,2)

  dim rrr(200,100)
  rr "<script language=vbscript> progress.style.top = 200 </script>" : response.flush : rsc = 0
  rsrc = countstring(getfield("yyy"), ";") * countstring(request_sss, ",")
  yyy = getfield("yyy") : i = 0
  ooo = getfield("ooo")

  do until yyy = ""
    yy = left(yyy,instr(yyy,",")-1) : yyy = mid(yyy,instr(yyy,",")+1)
    mm = left(yyy,instr(yyy,",")-1) : yyy = mid(yyy,instr(yyy,",")+1)
    do until mm = ""
      m = left(mm,instr(mm,";")-1) : mm = mid(mm,instr(mm,";")+1)
      sss = request_sss : sss = replace("," & sss, "99999,", "0.1,") : sss = mid(sss,2) : sss = sortlist(sss,"number") : sss = replace("," & sss, "0.1,", "99999,") : sss = mid(sss,2)
      t = m
      if t = "1" then t = "JAN"
      if t = "2" then t = "FEB"
      if t = "3" then t = "MAR"
      if t = "4" then t = "APR"
      if t = "5" then t = "MAY"
      if t = "6" then t = "JUN"
      if t = "7" then t = "JUL"
      if t = "8" then t = "AUG"
      if t = "9" then t = "SEP"
      if t = "10" then t = "OCT"
      if t = "11" then t = "NOV"
      if t = "12" then t = "DEC"
      i = i + 1 : rrr(i,0) = yy & " " & t
      j = 0
      do until sss = ""
        ss = left(sss,instr(sss,",")-1) : sss = mid(sss,instr(sss,",")+1)
        j = j + 1
        if rrr(0,j) = "" then
          if ss = "0" then rrr(0,j) = "All"
          if ss = "99999" then rrr(0,j) = averagetitle : if numberofsites = "" then numberofsites = getonefield("select count(id) as x from sites where active = 'on'")
          if rrr(0,j) = "" then rrr(0,j) = gettitle("sites", ss) : if specs = "shufersal" then t = getonefield("select data1 from sites where id = 0" & ss) : rrr(0,j) = rrr(0,j) & " " & getval(t, "branchname")
        end if
        t = "sum(price)"
        if getfield("reporttype") = "count" then t = "count(id)"
        if left(getfield("reporttype"),3) = "avg" then t = "avg(price/pcount)"
        s = "select " & t & " as x from orders where orders.status in('Done','Done Approved')"
        s = s & " and year(orders.date1) <> 1900"
        if ss <> "0" and ss <> "99999" then s = s & " and orders.siteid = " & ss
        s = s & " and year(orders.date1) = " & yy
        if left(m,1) = "Q" then
          t = cdbl(mid(m,2)) : t = t * 3 - 2
          s = s & " and month(orders.date1) >= " & t
          s = s & " and month(orders.date1) <= " & t + 2
        elseif left(m,1) = "H" then
          t = cdbl(mid(m,2)) : t = t * 6 - 5
          s = s & " and month(orders.date1) >= " & t
          s = s & " and month(orders.date1) <= " & t + 5
        elseif m <> "" then
          s = s & " and month(orders.date1) = " & m
        end if
        if left(getfield("reporttype"),3) = "avg" then s = s & " and orders.pcount > 0 and orders.price > 0"
        origs = s
        set rs = conn.execute(sqlfilter(s))
        x = rs("x") : if isnull(x) then x = 0
        if ss = "99999" and left(getfield("reporttype"),3) <> "avg" then x = x / numberofsites
        rrr(i,j) = x

        '----efficiency
        if getfield("reporttype") = "avgeff" then
          j = j + 1
          if rrr(0,j) = "" then
            if ss = "0" then rrr(0,j) = "All Efficiency"
            if rrr(0,j) = "" then rrr(0,j) = gettitle("sites", ss) & " Efficiency"
          end if
          '''select avg(price/pcount) as x from orders where status in('Done','Done Approved') and year(date1) <> 1900 and siteid = 3 and year(date1) = 2014 and month(date1) = 1 and pcount > 0 and price > 0
          s = "select sum(pcount) / sum(cars.passengers) as x from orders left join cars on orders.carid = cars.id"
          s = s & " where orders.carid > 0 and cars.passengers > 0"
          s = s & " and " & mid(origs,instr(origs, " where ") + 7)
          set rs = conn.execute(sqlfilter(s))
          x = rs("x") : if isnull(x) then x = 0
          rrr(i,j) = x * 100
        end if

        rsc = rsc + 1 : rr "<script language=vbscript> progresstext.innerhtml = ""<b>~Processing~..</b> " & rsc & "/" & rsrc & """ : progressbar.style.width = " & fix(400 * rsc / rsrc) & " </script>" : response.flush
      loop
    loop
  loop
  rr "<script language=vbscript> progress.style.top = -1000 </script>" : response.flush

  '-----------------------------table
  dim vsum(1000) : tsum = 0 : afterdec = 0 : csv = "" : if left(getfield("reporttype"),3) = "avg" then afterdec = 2
  rr "<br><center><table border=0 cellpadding=5>"
  for x = 0 to i
    rr "<tr>" : hsum = 0
    for y = 0 to j
      special = false : if rrr(0,y) = "All" or rrr(0,y) = averagetitle then special = true
      v = rrr(x,y)
      if x = 0 or y = 0 then
        rr "<td style='background:#dddddd;'" & t & "><b>" & v
        csv = csv & tocsv(v) & ","
      else
        vsum(y) = vsum(y) + v : if not special then tsum = tsum + v : hsum = hsum + v
        rr "<td style='background:#" & iif(special, "aaaaaa", "dddddd") & ";'" & t & ">" & formatnumber(v,afterdec,true,true,true)
        csv = csv & fix(v) & ","
      end if
    next

    '--last column is average only of real columns
    if left(getfield("reporttype"),3) = "avg" then
      realj = j
      if instr("," & request_sss, ",0,") > 0 then realj = realj - 1
      if instr("," & request_sss, ",99999,") > 0 then realj = realj - 1
      if realj > 0 then hsum = hsum / realj
    end if
    if j > 1 then
      if getfield("reporttype") = "avgeff" then
        'no horizontal sum
      elseif x = 0 then
        rr "<td style='background:#dddddd;'><b>~Total~"
        csv = csv & "Total,"
      else
        hsum = formatnumber(hsum,afterdec,true,true,true)
        rr "<td style='background:#dddddd; color:#ff0000;'>" & hsum
        csv = csv & fix(hsum) & ","
      end if
    end if
    csv = csv & vbcrlf
  next
  rr "<tr><td style='background:#dddddd;'><b>~Total~"
  csv = csv & "Total,"
  for x = 1 to j
    if left(getfield("reporttype"),3) = "avg" and i > 0 then vsum(x) = vsum(x) / i
    special = false : if rrr(0,x) = "All" or rrr(0,x) = averagetitle then special = true
    t = formatnumber(vsum(x),afterdec,true,true,true)
    rr "<td style='background:#" & iif(special, "aaaaaa", "dddddd") & "; color:#ff0000;'>" & t
    csv = csv & fix(t) & ","
  next
  if left(getfield("reporttype"),3) = "avg" and i > 0 and realj > 0 then tsum = tsum / i / realj

  if getfield("reporttype") = "avgeff" then
   'no horizontal sum
  elseif j > 1 then
    t = formatnumber(tsum,afterdec,true,true,true)
    rr "<td style='background:#dddddd; color:#ff0000;'><b>" & t & " ~SystemCurrency~"
    csv = csv & fix(t) & ","
  end if
  rr "</table>"
  session("csv") = csv

  ''----simple csv
  'csv = ""
  'for x = 0 to i
  '  for y = 0 to j
  '    v = rrr(x,y) : if x > 0 and y > 0 then v = fix(v)
  '    csv = csv & tocsv(v) & ","
  '  next
  '  csv = csv & vbcrlf
  'next
  'session("csv") = csv

  '----chart
  titles = "" : for x = 1 to i: titles = titles & tocsv(heb2uhtm(rrr(x,0))) & "," : next
  for y = 1 to j
    values = values & tocsv(heb2uhtm(rrr(0,y))) & ":"
    for x = 1 to i
      t = rrr(x,y) : if isnumeric(t) then t = formatnumber(t, afterdec, true, false, false)
      values = values & t & ","
    next
    values = values & rrr(x,y) & "|"
  next

  'amchartwidth = "1200" : amchartheight = "700" : amchartstacktype = "none" 'regular
  'rr amchart("", titles, values, "bar")
  rr "<br><br>"
  rr "<form name=fchart3 action=? target=_new method=post>"
  rr "<input type=hidden name=action value=reportshowchart>"
  rr "<input type=hidden name=titles value='" & titles & "'>"
  rr "<input type=hidden name=values value='" & values & "'>"
  rr "<input type=hidden name=design value=bar>"
  rr "</form>"
  rr "<a href=#aa style='font-size:20; color:#0000ff; margin-~langside~:50;' onclick='vbscript: fchart3.action.value = ""reportshowchart"" : fchart3.design.value = ""bar""  : fchart3.target = ""chart1"" : fchart3.submit'>Show Bar Chart</a>"
  rr "<a href=#aa style='font-size:20; color:#0000ff; margin-~langside~:50;'  onclick='vbscript: fchart3.action.value = ""reportshowchart"" : fchart3.design.value = ""line"" : fchart3.target = ""chart2"" : fchart3.submit'>Show Line Chart</a>"
  rr "<a href=#aa style='font-size:20; color:#0000ff; margin-~langside~:50;'  onclick='vbscript: fchart3.action.value = ""sessioncsv""      : fchart3.design.value = """"     : fchart3.target = ""csv""    : fchart3.submit'>Export To Excel</a>"

  rr "<br><br><br><br>"

end if

if getfield("action") = "reportshowchart" then
  amchartwidth = "1200" : amchartheight = "700" : amchartstacktype = "none" 'regular
  rr amchart("", getfield("titles"), getfield("values"), getfield("design"))
end if


%>
