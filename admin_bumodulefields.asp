<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 21/09/2010 15:08:12

function bumodulefieldsdelitem(i)
  bumodulefieldsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id=" & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function bumodulefieldsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("tname") <> "" then t = getfield("tname")
  f = f & "tname," : v = v & "'" & t & "',"

  t = "" : if getfield("tdisplay") <> "" then t = getfield("tdisplay")
  f = f & "tdisplay," : v = v & "'" & t & "',"

  t = "" : if getfield("tcontrol") <> "" then t = getfield("tcontrol")
  f = f & "tcontrol," : v = v & "'" & t & "',"

  t = "on" : if getfield("tlist") <> "" then t = getfield("tlist")
  f = f & "tlist," : v = v & "'" & t & "',"

  t = "0"
  sq = "select max(tpriority) as t from " & tablename
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  if not rs1.eof then t = rs1("t") : if isnull(t) then t = "0"
  t = cdbl(t) + 10
  if getfield("tpriority") <> "" then t = getfield("tpriority")
  f = f & "tpriority," : v = v & t & ","

  t = "" : if getfield("tnotr") <> "" then t = getfield("tnotr")
  f = f & "tnotr," : v = v & "'" & t & "',"

  t = "" : if getfield("ttype") <> "" then t = getfield("ttype")
  f = f & "ttype," : v = v & "'" & t & "',"

  t = "50" : if getfield("tlength") <> "" then t = getfield("tlength")
  f = f & "tlength," : v = v & t & ","

  t = "d" : if getfield("tdir") <> "" then t = getfield("tdir")
  f = f & "tdir," : v = v & "'" & t & "',"

  t = "" : if getfield("tdefault") <> "" then t = getfield("tdefault")
  f = f & "tdefault," : v = v & "'" & t & "',"

  t = "" : if getfield("tdata") <> "" then t = getfield("tdata")
  f = f & "tdata," : v = v & "'" & t & "',"

  t = "" : if getfield("thelp") <> "" then t = getfield("thelp")
  f = f & "thelp," : v = v & "'" & t & "',"

  t = "" : if getfield("tmultiupdate") <> "" then t = getfield("tmultiupdate")
  f = f & "tmultiupdate," : v = v & "'" & t & "',"

  t = "" : if getfield("tlistupdate") <> "" then t = getfield("tlistupdate")
  f = f & "tlistupdate," : v = v & "'" & t & "',"

  t = "" : if getfield("tfilter") <> "" then t = getfield("tfilter")
  f = f & "tfilter," : v = v & "'" & t & "',"

  t = "" : if getfield("tmust") <> "" then t = getfield("tmust")
  f = f & "tmust," : v = v & "'" & t & "',"

  t = "-4000000000" : if getfield("tmin") <> "" then t = getfield("tmin")
  f = f & "tmin," : v = v & t & ","

  t = "4000000000" : if getfield("tmax") <> "" then t = getfield("tmax")
  f = f & "tmax," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "tid" then t = session(tablename & "containerid")
  if getfield("tid") <> "" then t = getfield("tid")
  f = f & "tid," : v = v & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into bumodulefields(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from bumodulefields"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  bumodulefieldsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table bumodulefields (id autoincrement primary key,tname text(100),tdisplay text(100),tcontrol text(30),tlist text(2),tpriority number,tnotr text(1),ttype text(20),tlength number,tdir text(1),tdefault memo,tdata memo,thelp memo,tmultiupdate text(2),tlistupdate text(2),tfilter text(2),tmust text(2),tmin number,tmax number,tid number)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "bumodulefields" : session("tablename") = "bumodulefields"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "simple" then ulev = 0
if session("usergroup") = "admin" then ulev = 0
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = bumodulefieldsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  msg = ""
  Thetname = Trim(lcase(GetField("tname"))) : thetcontrol = Trim(GetField("tcontrol"))
  If thetcontrol = "touchuser" Then Thetname = "updateduserid"
  If thetcontrol = "touchtime" Then Thetname = "updateddate"
  If Len(Thetname) < 2 Then
    msg = "~Invalid Field Name~"
  ElseIf IsNumeric(Left(Thetname, 1)) Then
    msg = "~Invalid Field Name~"
  ElseIf thetcontrol <> "touchuser" And thetcontrol <> "touchtime" And InStr(LCase(SqlReservedKeywords), "," & LCase(Thetname) & ",") > 0 Then
    msg = "~Invalid Field Name~!!"
  ElseIf Len(Trim(GetField("tdisplay"))) < 1 And GetField("tcontrol") <> "childpage" Then
    msg = "~Invalid display name~"
  ElseIf GetField("ttype") = "number" And Not IsNumeric(GetField("tdefault")) Then
    msg = "~Default Value is not numeric~"
  Else
    closers(rs) : set rs = conn.execute(SQLfilter("select * from bumodulefields where tname = '" & Thetname & "' and tid=" & Session("bumodulefieldscontainerid") & " and id <> " & request_id))
    If Not rs.EOF Then msg = "~Field Name in use~"
    For i = 1 To Len(Thetname)
      If InStr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_", Mid(Thetname, i, 1)) = 0 Then msg = "~Invalid Field Name~": Exit For
    Next
  End If
  If msg <> "" Then errorend("_back" & msg)
  msg = msg & " ~Item Updated~"
']
  s = "update " & tablename & " set "
'[  if okedit("tname") then s = s & "tname='" & getfield("tname") & "',"
  if okedit("tname") then s = s & "tname='" & thetname & "',"
']
  if okedit("tdisplay") then s = s & "tdisplay='" & getfield("tdisplay") & "',"
  if okedit("tcontrol") then s = s & "tcontrol='" & getfield("tcontrol") & "',"
  if okedit("tlist") then s = s & "tlist='" & getfield("tlist") & "',"
  if okedit("tpriority") then s = s & "tpriority=" & getfield("tpriority") & ","
  if okedit("tnotr") then s = s & "tnotr='" & getfield("tnotr") & "',"
  if okedit("ttype") then s = s & "ttype='" & getfield("ttype") & "',"
  if okedit("tlength") then s = s & "tlength=" & getfield("tlength") & ","
  if okedit("tdir") then s = s & "tdir='" & getfield("tdir") & "',"
  if okedit("tdefault") then s = s & "tdefault='" & getfield("tdefault") & "',"
  if okedit("tdata") then s = s & "tdata='" & getfield("tdata") & "',"
  if okedit("thelp") then s = s & "thelp='" & getfield("thelp") & "',"
  if okedit("tmultiupdate") then s = s & "tmultiupdate='" & getfield("tmultiupdate") & "',"
  if okedit("tlistupdate") then s = s & "tlistupdate='" & getfield("tlistupdate") & "',"
  if okedit("tfilter") then s = s & "tfilter='" & getfield("tfilter") & "',"
  if okedit("tmust") then s = s & "tmust='" & getfield("tmust") & "',"
  if okedit("tmin") then s = s & "tmin=" & getfield("tmin") & ","
  if okedit("tmax") then s = s & "tmax=" & getfield("tmax") & ","
  if okedit("tid") then s = s & "tid=" & getfield("tid") & ","
  s = left(s, len(s) - 1) & " "
  s = s & "where id=" & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'['-------------afterupdate-------------
  If GetField("createfield") = "on" and getfield("tcontrol") <> "childpage" Then
    if Thetname = "" then Thetname = getfield("tname")
    Add = vbCrLf & "'" & Now & vbCrLf
    closers(rs1) : set rs1 = conn.execute(SQLfilter("select * from bumodules where id = " & Session("bumodulefieldscontainerid")))
    tn = rs1("title")

    conn.close() : conn.open(connstring) 'because i had "couldn't unlock db" error message
    On Error Resume Next
    s = "alter table " & tn & " drop column " & Thetname
    closers(rs1) : set rs1 = conn.execute(SQLfilter(s))
    On Error GoTo 0
    Add = Add & "'on error resume next : set rs1 = conn.exe" & "cute(sqlfilter(""" & s & """)) : on error goto 0" & vbCrLf

    s = "alter table " & tn & " add column " & Thetname & " " & GetField("ttype")
    If GetField("ttype") = "text" Then s = s & "(" & GetField("tlength") & ")"
    closers(rs1) : set rs1 = conn.execute(SQLfilter(s))
    Add = Add & "closers(rs1) : set rs1 = conn.execute(sqlfilter(""" & s & """))" & vbCrLf

    if instr(",selectlookup,selectlookuppro,selectlookupauto,touchuser,", "," & getfield("tcontrol") & ",") > 0 then
      s = "create index " & thetname & "_index on " & tn & "(" & thetname & ")"
      closers(rs1) : set rs1 = conn.execute(SQLfilter(s))
      Add = Add & "closers(rs1) : set rs1 = conn.execute(sqlfilter(""" & s & """))" & vbCrLf
    end if

    s = "update " & tn & " set " & Thetname & " = "
    If GetField("ttype") = "text" Or GetField("ttype") = "memo" Then s = s & "'" & GetField("tdefault") & "'"
    If GetField("ttype") = "number" Then s = s & GetField("tdefault")
    If GetField("ttype") = "datetime" Then
      t = GetField("tdefault")
      If t = "" Then If GetField("tmust") = "on" Then t = "now" Else t = "1/1/1900"
      If IsDate(t) Then t = SQLDate(t) Else t = ctext(t)
      If t = "date" Then t = "date()"
      If t = "now" Then t = "now()"
      s = s & t
    End If

    closers(rs1) : set rs1 = conn.execute(SQLfilter(s))
    Add = Add & "closers(rs1) : set rs1 = conn.execute(sqlfilter(""" & s & """))" & vbCrLf

    x = addtranslation(tn & "_" & Thetname, GetField("tdisplay"), 0)

    Add = Add & "x = addtranslation(""" & tn & "_" & Thetname & """,""" & GetField("tdisplay") & """, 0)" & vbCrLf
    If GetField("thelp") <> "" Then
      x = addtranslation(tn & "_" & Thetname & "_help", GetField("thelp"), 0)
      Add = Add & "x = addtranslation(""" & tn & "_" & Thetname & "_help"",""" & GetField("thelp") & """, 0)" & vbCrLf
    End If

    s = "insert into bumodulefields(tname,tid) values('" & thetname & "'," & getfield("tid") & ")"
    Add = Add & "closers(rs1) : set rs1 = conn.execute(sqlfilter(""" & s & """))" & vbCrLf

    x = addlog(Server.mappath("_upgrade.asp"), Add)
  End If
']
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(url)

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(url)

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(tname,tdisplay,tcontrol,tlist,tpriority,tnotr,ttype,tlength,tdir,tdefault,tdata,thelp,tmultiupdate,tlistupdate,tfilter,tmust,tmin,tmax,tid)"
  s = s & " SELECT tname,tdisplay,tcontrol,tlist,tpriority,tnotr,ttype,tlength,tdir,tdefault,tdata,thelp,tmultiupdate,tlistupdate,tfilter,tmust,tmin,tmax,tid"
  s = s & " FROM " & tablename & " where id=" & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  request_action = "form"
  request_id = theid
'[  msg = msg & " ~Record duplicated~"
  msg = msg & " ~Record duplicated~"
  s = "update bumodulefields set tpriority = tpriority + 1 where id = " & theid
  closers(rs) : set rs = conn.execute(sqlfilter(s))
']
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = bumodulefieldsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = bumodulefieldsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      t = getfield("listupdate_tdisplay_" & b) : if t = "" then t = getfield("multiupdate_tdisplay")
      if okedit("tdisplay") then s = s & ",tdisplay = '" & t & "'"
      if okedit("tlist") then s = s & ",tlist = '" & getfield("listupdate_tlist_" & b) & "'"
      t = getfield("listupdate_tpriority_" & b) : if t = "" then t = getfield("multiupdate_tpriority")
      if okedit("tpriority") then s = s & ",tpriority = " & t
      t = getfield("listupdate_tnotr_" & b) : if t = "" then t = getfield("multiupdate_tnotr")
      if okedit("tnotr") then s = s & ",tnotr = '" & t & "'"
      t = getfield("listupdate_ttype_" & b) : if t = "" then t = getfield("multiupdate_ttype")
      if okedit("ttype") then s = s & ",ttype = '" & t & "'"
      t = getfield("listupdate_tdir_" & b) : if t = "" then t = getfield("multiupdate_tdir")
      if okedit("tdir") then s = s & ",tdir = '" & t & "'"
      t = getfield("listupdate_tdefault_" & b) : if t = "" then t = getfield("multiupdate_tdefault")
      if okedit("tdefault") then s = s & ",tdefault = '" & t & "'"
      t = getfield("listupdate_tdata_" & b) : if t = "" then t = getfield("multiupdate_tdata")
      if okedit("tdata") then s = s & ",tdata = '" & t & "'"
      if okedit("tmultiupdate") then s = s & ",tmultiupdate = '" & getfield("listupdate_tmultiupdate_" & b) & "'"
      if okedit("tlistupdate") then s = s & ",tlistupdate = '" & getfield("listupdate_tlistupdate_" & b) & "'"
      if okedit("tfilter") then s = s & ",tfilter = '" & getfield("listupdate_tfilter_" & b) & "'"
      if okedit("tmust") then s = s & ",tmust = '" & getfield("listupdate_tmust_" & b) & "'"
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id=" & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if

'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = getfield("backto")
  s = "select * from " & tablename & " where id=" & request_id
  if session("usingsql") = "1" then s = replace(s,"*","bumodulefields.id,bumodulefields.tname,bumodulefields.tdisplay,bumodulefields.tcontrol,bumodulefields.tlist,bumodulefields.tpriority,bumodulefields.tnotr,bumodulefields.ttype,bumodulefields.tlength,bumodulefields.tdir,bumodulefields.tdefault,bumodulefields.tdata,bumodulefields.thelp,bumodulefields.tmultiupdate,bumodulefields.tlistupdate,bumodulefields.tfilter,bumodulefields.tmust,bumodulefields.tmin,bumodulefields.tmax,bumodulefields.tid",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id)
  Theid = rs("id")
  Thetname = rs("tname") : if isnull(Thetname) then Thetname = ""
  Thetdisplay = rs("tdisplay") : if isnull(Thetdisplay) then Thetdisplay = ""
  Thetcontrol = rs("tcontrol") : if isnull(Thetcontrol) then Thetcontrol = ""
  Thetlist = rs("tlist") : if isnull(Thetlist) then Thetlist = ""
  Thetpriority = rs("tpriority") : if isnull(Thetpriority) then Thetpriority = 0
  Thetpriority = replace(thetpriority,",",".")
  Thetnotr = rs("tnotr") : if isnull(Thetnotr) then Thetnotr = ""
  Thettype = rs("ttype") : if isnull(Thettype) then Thettype = ""
  Thetlength = rs("tlength") : if isnull(Thetlength) then Thetlength = 0
  Thetlength = replace(thetlength,",",".") : Thetlength = formatnumber(thetlength,0,true,false,false)
  Thetdir = rs("tdir") : if isnull(Thetdir) then Thetdir = ""
  Thetdefault = rs("tdefault") : if isnull(Thetdefault) then Thetdefault = ""
  Thetdefault = replace(thetdefault,"<br>",vbcrlf)
  Thetdata = rs("tdata") : if isnull(Thetdata) then Thetdata = ""
  Thethelp = rs("thelp") : if isnull(Thethelp) then Thethelp = ""
  Thethelp = replace(thethelp,"<br>",vbcrlf)
  Thetmultiupdate = rs("tmultiupdate") : if isnull(Thetmultiupdate) then Thetmultiupdate = ""
  Thetlistupdate = rs("tlistupdate") : if isnull(Thetlistupdate) then Thetlistupdate = ""
  Thetfilter = rs("tfilter") : if isnull(Thetfilter) then Thetfilter = ""
  Thetmust = rs("tmust") : if isnull(Thetmust) then Thetmust = ""
  Thetmin = rs("tmin") : if isnull(Thetmin) then Thetmin = 0
  Thetmin = replace(thetmin,",",".") : Thetmin = formatnumber(thetmin,2,true,false,false)
  Thetmax = rs("tmax") : if isnull(Thetmax) then Thetmax = 0
  Thetmax = replace(thetmax,",",".") : Thetmax = formatnumber(thetmax,2,true,false,false)
  Thetid = rs("tid") : if isnull(Thetid) then Thetid = 0
  Thetid = replace(thetid,",",".")
  formcontrols = "tname,tdisplay,tcontrol,tlist,tpriority,tnotr,ttype,tlength,tdir,tdefault,tdata,thelp,tmultiupdate,tlistupdate,tfilter,tmust,tmin,tmax,tid,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  rr("<center><table class=form_table cellspacing=5>" & formtopbar)

  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/product.png' ~iconsize~> ~bumodulefields_bumodulefields~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("tname") then'form:tname
    rr("<tr valign=top id=tname_tr0><td class=form_item1 id=tname_tr1><span id=tname_caption>~bumodulefields_tname~<img src=must.gif></span><td class=form_item2 id=tname_tr2>")
'[    rr("<input type=text maxlength=100 name=tname onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetname & """ dir=~langdir~>")
    rr("<input type=text maxlength=100 name=tname onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:200;' value=""" & Thetname & """ dir=~langdir~")
    rr(" onblur='vbscript: if f1.tdisplay.value = """" then f1.tdisplay.value = ucase(left(me.value,1)) & lcase(mid(me.value,2))'>")
    closers(rs1) : set rs1 = conn.execute(sqlfilter("select * from bumodules where id = " & session("bumodulefieldscontainerid")))
    if not rs1.eof Then
      if rs1("created") = "on" Then
        t = "" : if request_action2 = "newitem" then t = " checked"
        rr(" <input type=checkbox name=createfield " & t & ">~Create Field~")
      end if
    end if

']
  end if'form:tname

  if oksee("tdisplay") then'form:tdisplay
    rr("<tr valign=top id=tdisplay_tr0><td class=form_item1 id=tdisplay_tr1><span id=tdisplay_caption>~bumodulefields_tdisplay~<img src=must.gif></span><td class=form_item2 id=tdisplay_tr2>")
    rr("<input type=text maxlength=100 name=tdisplay onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetdisplay & """ dir=~langdir~>")
  end if'form:tdisplay

  if oksee("tcontrol") then'form:tcontrol
    rr("<tr valign=top id=tcontrol_tr0><td class=form_item1 id=tcontrol_tr1><span id=tcontrol_caption>~bumodulefields_tcontrol~</span><td class=form_item2 id=tcontrol_tr2>")
'[    rr("<input type=text maxlength=30 name=tcontrol onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetcontrol & """ dir=~langdir~>")
  '==================================================
  a = ""
  a = a & "textline,title,text*,50,Title,,d,,,*,,,-4000000000*,4000000000*,,on,,?"
  a = a & "textarea,description,memo*,0*,Description,,d,*,*,*,,,-4000000000*,4000000000*,*,*,,?"
  a = a & "textareaexpand,description,memo*,0*,Description,,d,*,*,*,,,-4000000000*,4000000000*,*,*,,?"
  a = a & "htmlarea,contents,memo*,0*,Contents,,d,*,*,*,,,-4000000000*,4000000000*,*,*,,?"
  a = a & "time,date1,datetime*,0*,Date,,e*,,,hour (date and time),*,,-4000000000*,4000000000*,,on,,?"
  a = a & "number,clicks,number*,0*,Clicks,0,e*,,,2sum,,on,-4000000000,4000000000,,on,,?"
  a = a & "phone,phone1,text*,20*,Phone,,e*,,,050;051;052;053;054;055;056;057;058;059;02;03;04;07;08;09;,,,-4000000000*,4000000000*,,on,,?"
  a = a & "email,email,text*,70*,Email,,e*,,,,,,-4000000000*,4000000000*,,on,,?"
  a = a & "site,site,text*,100*,Site,,e*,,,,,,-4000000000*,4000000000*,,on,,?"
  a = a & "video,video,text*,20*,Video,,e*,,,,,,-4000000000*,4000000000*,,on,,?"
  a = a & "priority,priority,number*,0*,Priority,0,e*,,,on*,,on,-4000000000,4000000000,,on,,?"
  a = a & "upload,file1,text*,100*,File,,e*,*,*,*,,,-4000000000*,4000000000*,*,on,,?"
  a = a & "uploadindex,index1,memo*,50,*,*,e*,*,*,file1,*,*,-4000000000*,4000000000*,*,,,?"
  a = a & "formdata,data1,memo*,0*,Description,- Title Line=|<br>T Text Field=|<br>A Textarea=|<br>D Date=|<br>N Number=|<br>C Checkbox=|<br>S Choose option;option1;option2;=|<br>F upload file=|<br>U user=|<br>W users=|<br>R selectlistfree=|<br>,d,*,*,+-,,,-4000000000*,4000000000*,*,*,,?"
  a = a & "checkbox,available,text*,2*,Available,,d*,,,*,*,,-4000000000*,4000000000*,,on,,?"
  a = a & "checkboxbit,yesno,bit*,2*,YesNo,,d*,,,*,*,,-4000000000*,4000000000*,,False,,?"
  a = a & "selectstatus,status,text*,20,Status,,d,on,,,,,-4000000000*,4000000000*,,on,,?"
  a = a & "selectlist,hobbies,text*,50,Hobbies,,d,on,,eating;drinking;sleeping;,,,-4000000000*,4000000000*,,on,,?"
  a = a & "selectlistfree,hobbies2,text*,50,Hobbies free,,d,on,,eating;drinking;sleeping;,,,-4000000000*,4000000000*,,on,,?"
  a = a & "selectlistfreeauto,hobbies2,text*,50,Hobbies free,,d,on,,eating;drinking;sleeping;,,,-4000000000*,4000000000*,,on,,?"
  a = a & "selectlookup,userid1,number*,0*,User,0*,d,on,,buusers;firstname;lastname;++,*,,-4000000000*,4000000000*,,on,,?"
  a = a & "selectlookuppro,userid2,number*,0*,User,0*,d,on,,buusers;firstname;lastname;+,*,,-4000000000*,4000000000*,,on,,?"
  a = a & "selectlookupauto,userid,number*,0*,User,0*,d,on,,buusers;firstname;lastname;+,*,,-4000000000*,4000000000*,,on,,?"
  a = a & "selectlookupmulti,users,memo*,0*,Users,*,d,*,*,buusers;firstname;lastname;+,*,,-4000000000*,4000000000*,*,on,,?"
  a = a & "selectlookupmultipro,users2,memo*,0*,Users,*,d,*,*,buusers;firstname;lastname;+,*,,-4000000000*,4000000000*,*,on,,?"
  a = a & "selectlookupmultiauto,users3,memo*,0*,Users,*,d,,*,buusers;firstname;lastname;+,*,,-4000000000*,4000000000*,*,on,,?"
  a = a & "selecttree,treeid,number*,0*,Category from tree,0*,d,,,cats;parentid;title;,*,,-4000000000*,4000000000*,,on,,?"
  a = a & "selecttreepro,catid,number*,0*,Category from tree,0*,d,,,cats;parentid;title;,*,,-4000000000*,4000000000*,,on,,?"
  a = a & "selecttreemultipro,catids,memo*,0*,Categories from tree,*,d,,,cats;parentid;title;,*,,-4000000000*,4000000000*,,,,?"
  a = a & "options,payaccept,memo*,0*,Payment Method,,d,*,*,cash;credit;cheques;,*,,-4000000000*,4000000000*,*,on,,?"
  a = a & "touchuser,updateduserid*,number*,0*,Updated By,0*,e*,,*,buusers;username;*,*,*,-4000000000*,4000000000*,*,on,,?"
  a = a & "touchtime,updateddate*,datetime*,0*,Date,now*,e*,,,*,*,,-4000000000*,4000000000*,,on,on,?"
  a = a & "childpage,ChildTableName,text*,20*,Childpage*,*,e*,*,*,-containeridfield;searchfield;searchfield;DUP,*,*,-4000000000*,4000000000*,*,,,?"
  a = a & "hidden,data1,text,50,*,*,e*,*,*,,*,*,-4000000000*,4000000000*,*,,,?"

  rr("<select name=tcontrol id=tcontrol dir=ltr onchange='vbscript:popfields(true)' style='width:150;'>")
  sc = ""
  Do Until a = ""
    b = Left(a, InStr(a, "?") - 1): a = Mid(a, InStr(a, "?") + 1)
    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    ff = e
    sc = sc & "if f1.tcontrol.value = """ & Replace(e, "*", "") & """ then " & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.tname.style.backgroundcolor = ""#ffffff"" else f1.tname.style.backgroundcolor = ""#dddddd""" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & f2 & """,""*"") = 0 then f1.ttype.style.backgroundcolor = ""#ffffff"" else f1.ttype.style.backgroundcolor = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.ttype.value = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.tlength.style.backgroundcolor = ""#ffffff"" else f1.tlength.style.backgroundcolor = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tlength.value = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.tdisplay.style.backgroundcolor = ""#ffffff"" else f1.tdisplay.style.backgroundcolor = ""#dddddd""" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    If ff = "formdata" Then
      e = Replace(e, ";", ",")
    End If
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.tdefault.style.backgroundcolor = ""#ffffff"" else f1.tdefault.style.backgroundcolor = ""#dddddd""" & vbCrLf
    e = Replace(e, "*", "")
    e = Replace(e, "<br>", """ & vbcrlf & """)
    sc = sc & "  if doset then f1.tdefault.value = """ & e & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.tdir.style.backgroundcolor = ""#ffffff"" else f1.tdir.style.backgroundcolor = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tdir.value = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then tfilter_caption.style.color = ""#000000"" else tfilter_caption.style.color = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tfilter.checked = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then tmultiupdate_caption.style.color = ""#000000"" else tmultiupdate_caption.style.color = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tlistupdate.checked = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    e = Replace(e, ";", ",")
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.tdata.style.backgroundcolor = ""#ffffff"" else f1.tdata.style.backgroundcolor = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tdata.value = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.thelp.style.backgroundcolor = ""#ffffff"" else f1.thelp.style.backgroundcolor = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.thelp.value = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then tmust_caption.style.color = ""#000000"" else tmust_caption.style.color = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tmust.checked = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.tmin.style.backgroundcolor = ""#ffffff"" else f1.tmin.style.backgroundcolor = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tmin.value = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then f1.tmax.style.backgroundcolor = ""#ffffff"" else f1.tmax.style.backgroundcolor = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tmax.value = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then tlistupdate_caption.style.color = ""#000000"" else tlistupdate_caption.style.color = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tmultiupdate.checked = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then tlist_caption.style.color = ""#000000"" else tlist_caption.style.color = ""#dddddd""" & vbCrLf
    sc = sc & "  if doset then f1.tlist.checked = """ & Replace(e, "*", "") & """" & vbCrLf

    e = Left(b, InStr(b, ",") - 1): b = Mid(b, InStr(b, ",") + 1)
    sc = sc & "  if instr(""" & e & """,""*"") = 0 then tnotr_caption.style.color = ""#000000"" else tnotr_caption.style.color = ""#dddddd""" & vbCrLf
   'sc = sc & "  if doset then f1.tnotr.value = """ & Replace(e, "*", "") & """" & vbCrLf

    sc = sc & "end if" & vbCrLf
    sc = sc & vbCrLf

    rr("<option value=" & ff)
    If thetcontrol = ff Then rr(" selected")
    rr(">" & ff)

  Loop
  rr("</select>")

  rr("<script language=vbscript>" & vbCrLf)
  rr("  sub popfields(doset)" & vbCrLf)
  rr(sc & vbCrLf)
  rr("  end sub" & vbCrLf)
  rr("</script>" & vbCrLf)
  '==================================================
']
  end if'form:tcontrol

  if oksee("tlist") then'form:tlist
    rr("<tr valign=top id=tlist_tr0><td class=form_item1 id=tlist_tr1><span id=tlist_caption>~bumodulefields_tlist~</span><td class=form_item2 id=tlist_tr2>")
    rr("<input type=checkbox name=tlist")
    if thetlist = "on" then rr(" checked")
    rr(">")
  end if'form:tlist

  if oksee("tpriority") then'form:tpriority
    rr("<tr valign=top id=tpriority_tr0><td class=form_item1 id=tpriority_tr1><span id=tpriority_caption>~bumodulefields_tpriority~<img src=must.gif></span><td class=form_item2 id=tpriority_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=tpriority value=""" & Thetpriority & """>")
  end if'form:tpriority

  if oksee("tnotr") then'form:tnotr
    rr("<tr valign=top id=tnotr_tr0><td class=form_item1 id=tnotr_tr1><span id=tnotr_caption>~bumodulefields_tnotr~</span><td class=form_item2 id=tnotr_tr2>")
    a = "R,C,F,"
    rr("<select name=tnotr dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
'[      if Thetnotr = b then rr(" selected")
      if lcase(Thetnotr) = lcase(b) then rr(" selected")
']
      rr(">" & b)
    loop
    rr("</select>")'form:tnotr
  end if'form:tnotr

  if oksee("ttype") then'form:ttype
    rr("<tr valign=top id=ttype_tr0><td class=form_item1 id=ttype_tr1><span id=ttype_caption>~bumodulefields_ttype~<img src=must.gif></span><td class=form_item2 id=ttype_tr2>")
    a = "text,memo,number,datetime,bit,"
    rr("<select name=ttype dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Thettype = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:ttype
  end if'form:ttype

  if oksee("tlength") then'form:tlength
    rr("<tr valign=top id=tlength_tr0><td class=form_item1 id=tlength_tr1><span id=tlength_caption>~bumodulefields_tlength~<img src=must.gif></span><td class=form_item2 id=tlength_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=tlength value=""" & Thetlength & """>")
  end if'form:tlength

  if oksee("tdir") then'form:tdir
    rr("<tr valign=top id=tdir_tr0><td class=form_item1 id=tdir_tr1><span id=tdir_caption>~bumodulefields_tdir~</span><td class=form_item2 id=tdir_tr2>")
    a = "d,e,h,"
    rr("<select name=tdir dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Thetdir = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:tdir
  end if'form:tdir

  if oksee("tdefault") then'form:tdefault
    rr("<tr valign=top id=tdefault_tr0><td class=form_item1 id=tdefault_tr1><span id=tdefault_caption>~bumodulefields_tdefault~</span><td class=form_item2 id=tdefault_tr2>")
    rr("<textarea id=qtextarea1 name=tdefault style='width:301; height:20;' onkeyup='vbscript:resizeq1'>")
    rr(thetdefault)
    rr("</textarea>" & vbcrlf)
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub resizeq1" & vbcrlf)
    rr("    dim d,h,i" & vbcrlf)
    rr("    d = document.getelementbyid(""qtextarea1"").value" & vbcrlf)
    rr("    h = 22" & vbcrlf)
    rr("    for i = 1 to len(d)" & vbcrlf)
    rr("      if mid(d,i,2) = vbcrlf then h = h + 15" & vbcrlf)
    rr("    next" & vbcrlf)
    rr("    if clng(h) < clng(len(d) / 2) then h = 16 + cint(len(d)/3)" & vbcrlf)
    rr("    if h > 150 then h = 150" & vbcrlf)
    rr("    document.getelementbyid(""qtextarea1"").style.height = h" & vbcrlf)
    if childpage then rr("    parent.document.getelementbyid(""fr" & tablename & """).height = document.body.scrollheight + 20" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("  resizeq1" & vbcrlf)
    rr("</script>" & vbcrlf)
  end if'form:tdefault

  if oksee("tdata") then'form:tdata
    rr("<tr valign=top id=tdata_tr0><td class=form_item1 id=tdata_tr1><span id=tdata_caption>~bumodulefields_tdata~</span><td class=form_item2 id=tdata_tr2>")
    rr("<input type=text maxlength=250 name=tdata onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetdata & """ dir=~langdir~>")
  end if'form:tdata

  if oksee("thelp") then'form:thelp
    rr("<tr valign=top id=thelp_tr0><td class=form_item1 id=thelp_tr1><span id=thelp_caption>~bumodulefields_thelp~</span><td class=form_item2 id=thelp_tr2>")
    rr("<textarea id=qtextarea2 name=thelp style='width:301; height:20;' onkeyup='vbscript:resizeq2'>")
    rr(thethelp)
    rr("</textarea>" & vbcrlf)
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub resizeq2" & vbcrlf)
    rr("    dim d,h,i" & vbcrlf)
    rr("    d = document.getelementbyid(""qtextarea2"").value" & vbcrlf)
    rr("    h = 22" & vbcrlf)
    rr("    for i = 1 to len(d)" & vbcrlf)
    rr("      if mid(d,i,2) = vbcrlf then h = h + 15" & vbcrlf)
    rr("    next" & vbcrlf)
    rr("    if clng(h) < clng(len(d) / 2) then h = 16 + cint(len(d)/3)" & vbcrlf)
    rr("    if h > 150 then h = 150" & vbcrlf)
    rr("    document.getelementbyid(""qtextarea2"").style.height = h" & vbcrlf)
    if childpage then rr("    parent.document.getelementbyid(""fr" & tablename & """).height = document.body.scrollheight + 20" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("  resizeq2" & vbcrlf)
    rr("</script>" & vbcrlf)
  end if'form:thelp

  if oksee("tmultiupdate") then'form:tmultiupdate
    rr("<tr valign=top id=tmultiupdate_tr0><td class=form_item1 id=tmultiupdate_tr1><span id=tmultiupdate_caption>~bumodulefields_tmultiupdate~</span><td class=form_item2 id=tmultiupdate_tr2>")
    rr("<input type=checkbox name=tmultiupdate")
    if thetmultiupdate = "on" then rr(" checked")
    rr(">")
  end if'form:tmultiupdate

  if oksee("tlistupdate") then'form:tlistupdate
    rr("<tr valign=top id=tlistupdate_tr0><td class=form_item1 id=tlistupdate_tr1><span id=tlistupdate_caption>~bumodulefields_tlistupdate~</span><td class=form_item2 id=tlistupdate_tr2>")
    rr("<input type=checkbox name=tlistupdate")
    if thetlistupdate = "on" then rr(" checked")
    rr(">")
  end if'form:tlistupdate

  if oksee("tfilter") then'form:tfilter
    rr("<tr valign=top id=tfilter_tr0><td class=form_item1 id=tfilter_tr1><span id=tfilter_caption>~bumodulefields_tfilter~</span><td class=form_item2 id=tfilter_tr2>")
    rr("<input type=checkbox name=tfilter")
    if thetfilter = "on" then rr(" checked")
    rr(">")
  end if'form:tfilter

  if oksee("tmust") then'form:tmust
    rr("<tr valign=top id=tmust_tr0><td class=form_item1 id=tmust_tr1><span id=tmust_caption>~bumodulefields_tmust~</span><td class=form_item2 id=tmust_tr2>")
    rr("<input type=checkbox name=tmust")
    if thetmust = "on" then rr(" checked")
    rr(">")
  end if'form:tmust

  if oksee("tmin") then'form:tmin
    rr("<tr valign=top id=tmin_tr0><td class=form_item1 id=tmin_tr1><span id=tmin_caption>~bumodulefields_tmin~<img src=must.gif></span><td class=form_item2 id=tmin_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=tmin value=""" & Thetmin & """>")
  end if'form:tmin

  if oksee("tmax") then'form:tmax
    rr("<tr valign=top id=tmax_tr0><td class=form_item1 id=tmax_tr1><span id=tmax_caption>~bumodulefields_tmax~<img src=must.gif></span><td class=form_item2 id=tmax_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=tmax value=""" & Thetmax & """>")
  end if'form:tmax

  if oksee("tid") and session(tablename & "containerfield") <> "tid" then
    rr("<tr valign=top id=tid_tr0><td class=form_item1 id=tid_tr1><span id=tid_caption>~bumodulefields_tid~</span><td class=form_item2 id=tid_tr2>")
    if getfield("box") = "tid" then thetid = getfield("boxid")
    rr("<select name=tid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~bumodulefields_tid~")
    sq = "select * from bumodules order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thetid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:tid
  elseif okedit("tid") then
    rr("<input type=hidden name=tid value=" & thetid & ">")
  end if'form:tid

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button id=buttonsavereturn style='width:80;' value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button id=buttonsave style='width:80;' value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button id=buttonduplicate style='width:80;' value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button id=buttondelete style='width:80;' value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button id=buttonback style='width:80;' value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

'[  '-------------checkform-------------
    rr("<script language=vbscript>popfields(false)</script>")
']
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("tname") then rr("    if f1.tname.value <> """" then validfield(""tname"") else invalidfield(""tname"") : ok = false" & vbcrlf)
  if okedit("tdisplay") then rr("    if f1.tdisplay.value <> """" then validfield(""tdisplay"") else invalidfield(""tdisplay"") : ok = false" & vbcrlf)
  if okedit("tpriority") and session(tablename & "containerfield") <> "tpriority" then
    rr("    if isnumeric(f1.tpriority.value) then" & vbcrlf)
    rr("      if cdbl(f1.tpriority.value) >= -4000000000 and cdbl(f1.tpriority.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""tpriority"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""tpriority"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""tpriority"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("ttype") then rr("    if f1.ttype.value <> """" then validfield(""ttype"") else invalidfield(""ttype"") : ok = false" & vbcrlf)
  if okedit("tlength") and session(tablename & "containerfield") <> "tlength" then
    rr("    if isnumeric(f1.tlength.value) then" & vbcrlf)
    rr("      if cdbl(f1.tlength.value) >= 0 and cdbl(f1.tlength.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""tlength"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""tlength"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""tlength"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("tmin") and session(tablename & "containerfield") <> "tmin" then
    rr("    if isnumeric(f1.tmin.value) then" & vbcrlf)
    rr("      if cdbl(f1.tmin.value) >= -4000000000 and cdbl(f1.tmin.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""tmin"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""tmin"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""tmin"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("tmax") and session(tablename & "containerfield") <> "tmax" then
    rr("    if isnumeric(f1.tmax.value) then" & vbcrlf)
    rr("      if cdbl(f1.tmax.value) >= -4000000000 and cdbl(f1.tmax.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""tmax"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""tmax"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""tmax"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.tname.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if request_action = "priorityshift" then
    ff = getfield("field") : i1 = request_id
    s = "select * from " & tablename & " where id = " & i1
    closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
    t = "" : if session(tablename & "containerid") <> "0" then t = " and " & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
    p1 = rs1(ff) : t1 = " < " : t2 = " desc" : if request_action2 = "down" then t1 = " > " : t2 = ""
    s = "select * from " & tablename & " where id <> " & i1 & " and " & ff & t1 & p1 & t & " order by " & ff & t2
    closers(rs2) : set rs2 = conn.execute(sqlfilter(s))
    if not rs2.eof then
      i2 = rs2("id") : p2 = rs2(ff)
      s = "update " & tablename & " set " & ff & " = " & p2 & " where id = " & i1
      closers(rs) : set rs = conn.execute(sqlfilter(s))
      s = "update " & tablename & " set " & ff & " = " & p1 & " where id = " & i2
      closers(rs) : set rs = conn.execute(sqlfilter(s))
    end if
  end if
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
    session(tablename & "_hidelistcolumns") = "thelp,tmin,tmax,"
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".tpriority"
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 align=center>" & listtopbar)

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/product.png' ~iconsize~> ~bumodulefields_bumodulefields~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
'[  if ulev > 1 and okedit("buttonadd") then rr("<input type=button id=buttonadd style='width:80;' value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button id=buttonadd style='width:80;' value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  closers(rs1) : set rs1 = conn.execute(sqlfilter("select * from bumodules where id = " & session("bumodulefieldscontainerid")))
  t = "" : if not rs1.eof Then if rs1("created") = "on" Then t = "&createfield=on"
  rr(" <input type=button onclick='vbscript: window.top.document.location = ""wizard.asp?wizardid=" & session(tablename & "containerid") & "&act=fieldform" & t & """' value=""~New Field Wizard~"">")
']

  t = "," & t : a = hidecontrols : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)

  '-------------sql------------------------------
  session(tablename & "_lines") = 1000000
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & bumodules_1.title) as tidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join bumodules bumodules_1 on " & tablename & ".tid = bumodules_1.id)"

  '-sqladd
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","bumodulefields.id,bumodulefields.tname,bumodulefields.tdisplay,bumodulefields.tcontrol,bumodulefields.tlist,bumodulefields.tpriority,bumodulefields.tnotr,bumodulefields.ttype,bumodulefields.tlength,bumodulefields.tdir,bumodulefields.tdefault,bumodulefields.tdata,bumodulefields.thelp,bumodulefields.tmultiupdate,bumodulefields.tlistupdate,bumodulefields.tfilter,bumodulefields.tmust,bumodulefields.tmin,bumodulefields.tmax,bumodulefields.tid",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title2>")
  if oksee("tname") then f = "bumodulefields.tname" : f2 = "tname" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tname") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tname~</a> " & a2)
  if oksee("tdisplay") then f = "bumodulefields.tdisplay" : f2 = "tdisplay" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tdisplay") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tdisplay~</a> " & a2)
  if oksee("tcontrol") then f = "bumodulefields.tcontrol" : f2 = "tcontrol" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tcontrol") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tcontrol~</a> " & a2)
  if oksee("tlist") then f = "bumodulefields.tlist" : f2 = "tlist" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tlist") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tlist~</a> " & a2)
  if oksee("tpriority") then f = "bumodulefields.tpriority" : f2 = "tpriority" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tpriority") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tpriority~</a> " & a2)
  if oksee("tnotr") then f = "bumodulefields.tnotr" : f2 = "tnotr" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tnotr") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tnotr~</a> " & a2)
  if oksee("ttype") then f = "bumodulefields.ttype" : f2 = "ttype" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("ttype") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_ttype~</a> " & a2)
  if oksee("tlength") then f = "bumodulefields.tlength" : f2 = "tlength" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tlength") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tlength~</a> " & a2)
  if oksee("tdir") then f = "bumodulefields.tdir" : f2 = "tdir" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tdir") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tdir~</a> " & a2)
  if oksee("tdefault") then rr("<td class=list_title2><nobr>~bumodulefields_tdefault~")
  if oksee("tdata") then rr("<td class=list_title2><nobr>~bumodulefields_tdata~")
  if oksee("thelp") then rr("<td class=list_title2><nobr>~bumodulefields_thelp~")
  if oksee("tmultiupdate") then f = "bumodulefields.tmultiupdate" : f2 = "tmultiupdate" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tmultiupdate") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tmultiupdate~</a> " & a2)
  if oksee("tlistupdate") then f = "bumodulefields.tlistupdate" : f2 = "tlistupdate" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tlistupdate") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tlistupdate~</a> " & a2)
  if oksee("tfilter") then f = "bumodulefields.tfilter" : f2 = "tfilter" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tfilter") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tfilter~</a> " & a2)
  if oksee("tmust") then f = "bumodulefields.tmust" : f2 = "tmust" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tmust") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tmust~</a> " & a2)
  if oksee("tmin") then f = "bumodulefields.tmin" : f2 = "tmin" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tmin") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tmin~</a> " & a2)
  if oksee("tmax") then f = "bumodulefields.tmax" : f2 = "tmax" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("tmax") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tmax~</a> " & a2)
  if oksee("tid") and session(tablename & "containerfield") <> "tid" then
    f = "bumodules_1.title" : f2 = "tidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodulefields_tid~</a> " & a2)
  end if

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetname = rs("tname") : if isnull(Thetname) then Thetname = ""
      Thetdisplay = rs("tdisplay") : if isnull(Thetdisplay) then Thetdisplay = ""
      Thetcontrol = rs("tcontrol") : if isnull(Thetcontrol) then Thetcontrol = ""
      Thetlist = rs("tlist") : if isnull(Thetlist) then Thetlist = ""
      Thetpriority = rs("tpriority") : if isnull(Thetpriority) then Thetpriority = 0
      Thetpriority = replace(thetpriority,",",".")
      Thetnotr = rs("tnotr") : if isnull(Thetnotr) then Thetnotr = ""
      Thettype = rs("ttype") : if isnull(Thettype) then Thettype = ""
      Thetlength = rs("tlength") : if isnull(Thetlength) then Thetlength = 0
      Thetlength = replace(thetlength,",",".") : Thetlength = formatnumber(thetlength,0,true,false,false)
      Thetdir = rs("tdir") : if isnull(Thetdir) then Thetdir = ""
      Thetdefault = rs("tdefault") : if isnull(Thetdefault) then Thetdefault = ""
      Thetdata = rs("tdata") : if isnull(Thetdata) then Thetdata = ""
      Thethelp = rs("thelp") : if isnull(Thethelp) then Thethelp = ""
      Thetmultiupdate = rs("tmultiupdate") : if isnull(Thetmultiupdate) then Thetmultiupdate = ""
      Thetlistupdate = rs("tlistupdate") : if isnull(Thetlistupdate) then Thetlistupdate = ""
      Thetfilter = rs("tfilter") : if isnull(Thetfilter) then Thetfilter = ""
      Thetmust = rs("tmust") : if isnull(Thetmust) then Thetmust = ""
      Thetmin = rs("tmin") : if isnull(Thetmin) then Thetmin = 0
      Thetmin = replace(thetmin,",",".") : Thetmin = formatnumber(thetmin,2,true,false,false)
      Thetmax = rs("tmax") : if isnull(Thetmax) then Thetmax = 0
      Thetmax = replace(thetmax,",",".") : Thetmax = formatnumber(thetmax,2,true,false,false)
      Thetid = rs("tid") : if isnull(Thetid) then Thetid = 0
      Thetid = replace(thetid,",",".")
      Thetidlookup = rs("tidlookup") : if isnull(Thetidlookup) then Thetidlookup = ""

      okwritehere = okwrite(tablename,theid)
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetname
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"

      if session(tablename & "_order") = tablename & ".tpriority" then
        rr("<a href=?action=priorityshift&action2=up&field=tpriority&id=" & theid & "><img src=up.gif border=0></a>")
        rr("<a href=?action=priorityshift&action2=down&field=tpriority&id=" & theid & "><img src=down.gif border=0></a> ")
      end if

      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/product.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("tdisplay") and session(tablename & "containerfield") <> "tdisplay" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tdisplay") then
      elseif session(tablename & "containerfield") = "tdisplay" then
        rr("<input type=hidden name=listupdate_tdisplay_" & theid & " value=" & thetdisplay & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tdisplay" then
        rr("<input type=text maxlength=100 name=listupdate_tdisplay_" & theid & " onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & thetdisplay & """ dir=~langdir~>")
      else
        rr(thetdisplay)
      end if
      if oksee("tcontrol") then rr("<td class=list_item valign=top align=><nobr>" & thetcontrol)
      if oksee("tlist") and session(tablename & "containerfield") <> "tlist" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tlist") then
      elseif session(tablename & "containerfield") = "tlist" then
        rr("<input type=hidden name=listupdate_tlist_" & theid & " value=" & thetlist & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tlist" then
        rr("<input type=checkbox name=listupdate_tlist_" & theid & "")
        if thetlist = "on" then rr(" checked")
        rr(">")
      else
        if thetlist = "on" then rr("v")
      end if
      if oksee("tpriority") and session(tablename & "containerfield") <> "tpriority" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tpriority") then
      elseif session(tablename & "containerfield") = "tpriority" then
        rr("<input type=hidden name=listupdate_tpriority_" & theid & " value=" & thetpriority & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tpriority" then
        rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=listupdate_tpriority_" & theid & " value=""" & thetpriority & """>")
      else
        rr(thetpriority)
      end if
      if oksee("tnotr") and session(tablename & "containerfield") <> "tnotr" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tnotr") then
      elseif session(tablename & "containerfield") = "tnotr" then
        rr("<input type=hidden name=listupdate_tnotr_" & theid & " value=" & thetnotr & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tnotr" then
        a = "R,C,F,"
        rr("<select name=listupdate_tnotr_" & theid & " dir=~langdir~>")
        do until a = ""
          b = left(a,instr(a,",")-1)
          a = mid(a,instr(a,",")+1)
          rr("<option value=""" & b & """")
'[          if thetnotr = b then rr(" selected")
          if lcase(Thetnotr) = lcase(b) then rr(" selected")
']
          rr(">" & b)
        loop
        rr("</select>")'form:listupdate_tnotr
      else
        rr(thetnotr)
      end if
      if oksee("ttype") and session(tablename & "containerfield") <> "ttype" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("ttype") then
      elseif session(tablename & "containerfield") = "ttype" then
        rr("<input type=hidden name=listupdate_ttype_" & theid & " value=" & thettype & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "ttype" then
        a = "text,memo,number,datetime,bit,"
        rr("<select name=listupdate_ttype_" & theid & " dir=~langdir~>")
        do until a = ""
          b = left(a,instr(a,",")-1)
          a = mid(a,instr(a,",")+1)
          rr("<option value=""" & b & """")
          if thettype = b then rr(" selected")
          rr(">" & b)
        loop
        rr("</select>")'form:listupdate_ttype
      else
        rr(thettype)
      end if
      if oksee("tlength") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(thetlength,0,true,false,true))
      if oksee("tdir") and session(tablename & "containerfield") <> "tdir" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tdir") then
      elseif session(tablename & "containerfield") = "tdir" then
        rr("<input type=hidden name=listupdate_tdir_" & theid & " value=" & thetdir & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tdir" then
        a = "d,e,h,"
        rr("<select name=listupdate_tdir_" & theid & " dir=~langdir~>")
        do until a = ""
          b = left(a,instr(a,",")-1)
          a = mid(a,instr(a,",")+1)
          rr("<option value=""" & b & """")
          if thetdir = b then rr(" selected")
          rr(">" & b)
        loop
        rr("</select>")'form:listupdate_tdir
      else
        rr(thetdir)
      end if
      if oksee("tdefault") and session(tablename & "containerfield") <> "tdefault" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tdefault") then
      elseif session(tablename & "containerfield") = "tdefault" then
        rr("<input type=hidden name=listupdate_tdefault_" & theid & " value=" & thetdefault & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tdefault" then
        rr("<textarea id=qtextarea1 name=listupdate_tdefault_" & theid & " style='width:100; height:20;' onkeyup='vbscript:resizeq1'>")
        rr(thetdefault)
        rr("</textarea>" & vbcrlf)
        rr("<script language=vbscript>" & vbcrlf)
        rr("  sub resizeq1" & vbcrlf)
        rr("    dim d,h,i" & vbcrlf)
        rr("    d = document.getelementbyid(""qtextarea1"").value" & vbcrlf)
        rr("    h = 22" & vbcrlf)
        rr("    for i = 1 to len(d)" & vbcrlf)
        rr("      if mid(d,i,2) = vbcrlf then h = h + 15" & vbcrlf)
        rr("    next" & vbcrlf)
        rr("    if clng(h) < clng(len(d) / 2) then h = 16 + cint(len(d)/3)" & vbcrlf)
        rr("    if h > 150 then h = 150" & vbcrlf)
        rr("    document.getelementbyid(""qtextarea1"").style.height = h" & vbcrlf)
        if childpage then rr("    parent.document.getelementbyid(""fr" & tablename & """).height = document.body.scrollheight + 20" & vbcrlf)
        rr("  end sub" & vbcrlf)
        rr("  resizeq1" & vbcrlf)
        rr("</script>" & vbcrlf)
      else
        rr(thetdefault)
      end if
      if oksee("tdata") and session(tablename & "containerfield") <> "tdata" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tdata") then
      elseif session(tablename & "containerfield") = "tdata" then
        rr("<input type=hidden name=listupdate_tdata_" & theid & " value=" & thetdata & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tdata" then
        rr("<input type=text maxlength=250 name=listupdate_tdata_" & theid & " onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & thetdata & """ dir=~langdir~>")
      else
        rr(thetdata)
      end if
      thethelp = thethelp : if isnull(thethelp) then thethelp = ""
      thethelp = replace(thethelp,"<br>"," ") : thethelp = replace(thethelp,"<","[") : thethelp = replace(thethelp,">","]") : if len(thethelp) > 30 then thethelp = left(thethelp,30) & "..."
      if oksee("thelp") then rr("<td class=list_item valign=top align=><nobr>" & thethelp)
      if oksee("tmultiupdate") and session(tablename & "containerfield") <> "tmultiupdate" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tmultiupdate") then
      elseif session(tablename & "containerfield") = "tmultiupdate" then
        rr("<input type=hidden name=listupdate_tmultiupdate_" & theid & " value=" & thetmultiupdate & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tmultiupdate" then
        rr("<input type=checkbox name=listupdate_tmultiupdate_" & theid & "")
        if thetmultiupdate = "on" then rr(" checked")
        rr(">")
      else
        if thetmultiupdate = "on" then rr("v")
      end if
      if oksee("tlistupdate") and session(tablename & "containerfield") <> "tlistupdate" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tlistupdate") then
      elseif session(tablename & "containerfield") = "tlistupdate" then
        rr("<input type=hidden name=listupdate_tlistupdate_" & theid & " value=" & thetlistupdate & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tlistupdate" then
        rr("<input type=checkbox name=listupdate_tlistupdate_" & theid & "")
        if thetlistupdate = "on" then rr(" checked")
        rr(">")
      else
        if thetlistupdate = "on" then rr("v")
      end if
      if oksee("tfilter") and session(tablename & "containerfield") <> "tfilter" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tfilter") then
      elseif session(tablename & "containerfield") = "tfilter" then
        rr("<input type=hidden name=listupdate_tfilter_" & theid & " value=" & thetfilter & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tfilter" then
        rr("<input type=checkbox name=listupdate_tfilter_" & theid & "")
        if thetfilter = "on" then rr(" checked")
        rr(">")
      else
        if thetfilter = "on" then rr("v")
      end if
      if oksee("tmust") and session(tablename & "containerfield") <> "tmust" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("tmust") then
      elseif session(tablename & "containerfield") = "tmust" then
        rr("<input type=hidden name=listupdate_tmust_" & theid & " value=" & thetmust & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "tmust" then
        rr("<input type=checkbox name=listupdate_tmust_" & theid & "")
        if thetmust = "on" then rr(" checked")
        rr(">")
      else
        if thetmust = "on" then rr("v")
      end if
      if oksee("tmin") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(thetmin,2,true,false,true))
      if oksee("tmax") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(thetmax,2,true,false,true))
      if oksee("tid") and session(tablename & "containerfield") <> "tid" then rr("<td class=list_item valign=top align=><nobr>" & thetidlookup)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("tdisplay") and session(tablename & "containerfield") <> "tdisplay" then rr("<td class=list_total1>") 'tdisplay
  if oksee("tcontrol") and session(tablename & "containerfield") <> "tcontrol" then rr("<td class=list_total1>") 'tcontrol
  if oksee("tlist") and session(tablename & "containerfield") <> "tlist" then rr("<td class=list_total1>") 'tlist
  if oksee("tpriority") and session(tablename & "containerfield") <> "tpriority" then rr("<td class=list_total1>") 'tpriority
  if oksee("tnotr") and session(tablename & "containerfield") <> "tnotr" then rr("<td class=list_total1>") 'tnotr
  if oksee("ttype") and session(tablename & "containerfield") <> "ttype" then rr("<td class=list_total1>") 'ttype
  if oksee("tlength") and session(tablename & "containerfield") <> "tlength" then rr("<td class=list_total1>") 'tlength
  if oksee("tdir") and session(tablename & "containerfield") <> "tdir" then rr("<td class=list_total1>") 'tdir
  if oksee("tdefault") and session(tablename & "containerfield") <> "tdefault" then rr("<td class=list_total1>") 'tdefault
  if oksee("tdata") and session(tablename & "containerfield") <> "tdata" then rr("<td class=list_total1>") 'tdata
  if oksee("thelp") and session(tablename & "containerfield") <> "thelp" then rr("<td class=list_total1>") 'thelp
  if oksee("tmultiupdate") and session(tablename & "containerfield") <> "tmultiupdate" then rr("<td class=list_total1>") 'tmultiupdate
  if oksee("tlistupdate") and session(tablename & "containerfield") <> "tlistupdate" then rr("<td class=list_total1>") 'tlistupdate
  if oksee("tfilter") and session(tablename & "containerfield") <> "tfilter" then rr("<td class=list_total1>") 'tfilter
  if oksee("tmust") and session(tablename & "containerfield") <> "tmust" then rr("<td class=list_total1>") 'tmust
  if oksee("tmin") and session(tablename & "containerfield") <> "tmin" then rr("<td class=list_total1>") 'tmin
  if oksee("tmax") and session(tablename & "containerfield") <> "tmax" then rr("<td class=list_total1>") 'tmax
  if oksee("tid") and session(tablename & "containerfield") <> "tid" then rr("<td class=list_total1>") 'tid

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then rr(" <input type=button id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete All Checked~'>")
  if ulev > 1 then

    rr(" <input type=button id=buttonmultiupdate value='~Update~' style='width:80;' onclick='vbscript:if msgbox(""~Update All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if

  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth + 20 : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
