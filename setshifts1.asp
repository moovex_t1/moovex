<!--#include file="include.asp"-->
<%
request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
admin_top = true : admin_login = true
%><!--#include file="top.asp"--><%
tablename = "setshifts"
if instr("," & session("userperm"), ",setshifts,") = 0 then x = errorend("��� ����� ����� ������")
request_ways = getfield("ways") : x = instr(request_ways, ":") : if x > 0 then xtype = mid(request_ways,x+1) : request_ways = left(request_ways, x-1)

'---session
if getfield("mode") <> "" then session("setshifts_mode") = getfield("mode") : rend
if session("setshifts_mode") = "" then session("setshifts_mode") = "show"
if getfield("span") <> "" then session("setshifts_span") = getfield("span")
if session("setshifts_span") = "" then session("setshifts_span") = "week"
if getfield("orderby") <> "" then session("setshifts_orderby") = getfield("orderby")
if session("setshifts_orderby") = "" then session("setshifts_orderby") = "abc"
if getfield("showshift") <> "" then session("setshifts_showshift") = getfield("showshift")
if session("setshifts_showshift") = "" then session("setshifts_showshift") = "hours"

if session("setshifts_showshift") = "shifts" and session("shiftnames") = "" then
  s = "select id,title,hour1,hour1b from shifts where 0=0"
  if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
  set rs2 = conn.execute(sqlfilter(s))
  do until rs2.eof
    session("shiftnames") = setval(session("shiftnames"), rs2("id"), rs2("title"))
    session("shifthours") = setval(session("shifthours"), rs2("hour1") & "-" & rs2("hour1b"), rs2("id"))
    rs2.movenext
  loop
end if
if getfield("action2") = "delall" and session("usergroup") <> "admin" then rr "~Access Denied~" : rend

'----------------------------
if request_action = "draftreal" then
  rclear
  i = getfield("id") : ways = request_ways
  if ways = "-" then
    s = "delete * from workershifts where id = " & i
    set rs = conn.execute(sqlfilter(s))
  else
    s = "select data1 from workershifts where id = " & i
    set rs = conn.execute(sqlfilter(s))
    data1 = "" : if not rs.eof then data1 = ctxt("" & rs("data1"))
    data1 = setval(data1, "draft", "")
    s = "update workershifts set data1 = '" & data1 & "', ways = '" & ways & "' where id = " & i
    set rs = conn.execute(sqlfilter(s))
  end if
  rr "<script language=vbscript>" & vbcrlf
  rr "  parent.ws" & i & ".style.color = ""#000000""" & vbcrlf
  rr "  a = parent.ws" & i & ".innerhtml" & vbcrlf
  if ways = "B" then rr "  a = mid(a, instr(a,""�����""))" & vbcrlf
  if ways = "A" then rr "  a = left(a, instr(a,""�����"")-1)" & vbcrlf
  if ways = "-" then rr "  a = """"" & vbcrlf
  rr "  parent.ws" & i & ".innerhtml = a" & vbcrlf
  rr "</script>" & vbcrlf
  rend
end if

'----------------------------
if left(request_action,10) = "workerstep" then
  'http://localhost/trans/setshifts.asp?username=1&pass=1&action=workerstepin&workercode=1111
  'http://localhost/trans/setshifts.asp?username=1&pass=1&action=workerstepout&workercode=1111

  rclear
  s = "select id,data1 from workers where code = '" & getfield("workercode") & "'"
  s = s & okreadsql("workers")
  set rs = conn.execute(sqlfilter(s))
  if getfield("workercode") = "" then
    rr "Invalid WorkerCode"
  elseif rs.eof then
    rr "Worker Not Found"
  else
    a = ""
    if request_action = "workerstepin" then a = "in"
    if request_action = "workerstepout" then a = "out"
    d = rs("data1") & "" : d = setval(d, "step", a) : d = setval(d, "steptime", now)
    s = "update workers set data1 = '" & d & "' where id = " & rs("id")
    set rs1 = conn.execute(sqlfilter(s))
    rr "OK-" & rs("id") & "-" & a
  end if
  rend
end if

'----------------------------
if getfield("act") = "popstations" then
  response.clear
  rr("<script language=vbscript>" & vbcrlf)
  for j = 1 to 2
    if j = 1 then box = "from1"
    if j = 2 then box = "to1"
    if specs = "iai" and j = 1 then j = 2 '...disabled to1 field
    rr("  parent.document.f1." & box & ".options.length = 0" & vbcrlf)
    rr("  set o = document.CreateElement(""OPTION"")" & vbcrlf)
    rr("  o.Value = ""0""" & vbcrlf)
    rr("  o.Text = ""          """ & vbcrlf)
    'rr("  o.selected = true" & vbcrlf)
    rr("  o.style.color = ""#bbbbfe""" & vbcrlf)
    rr("  parent.document.f1." & box & ".add o" & vbcrlf)
    if session("splittingcourses") = "" then session("splittingcourses") = "-1," & getidlist("select id from courses where type1 = 'Splitting'")
    sp = 0 : if instr("," & session("splittingcourses"), "," & getfield("courseid") & ",") > 0 then sp = 1
    if sp = 0 and getparam("ShiftAlternativeCourses") = "y" and getfield("courseid") <> "" and getfield("courseid") <> "0" then
      s = "select coursestations.stationid as id, stations.title as title"
      s = s & " from coursestations left join stations on coursestations.stationid = stations.id"
      s = s & " where courseid = " & getfield("courseid")
      s = s & " order by minutes"
    elseif sp = 0 and getfield("shiftid") <> "" then
      s = "select id,title from stations where 0=0"
      if session("usersiteid") <> "0" then s = s & " and (siteid = 0 or siteid = " & session("usersiteid") & ")"
      ii = getidlist("select courseids from shifts where id = " & getfield("shiftid"))
      ii = getidlist("select stationid from coursestations where courseid in(" & ii & "-1)")
      if ii <> "" then s = s & " and id in(" & ii & "-1)"
      s = s & " order by title"
    else
      s = "select id,title from stations where special = 'on'"
      if session("usersiteid") <> "0" then s = s & " and (siteid = 0 or siteid = " & session("usersiteid") & ")"
      s = s & " order by title"
    end if
    set rs = conn.execute(s)
    do until rs.eof
      rr("  set o = document.CreateElement(""OPTION"")" & vbcrlf)
      rr("  o.Value = """ & rs("id") & """" & vbcrlf)
      t = rs("title") : if isnull(t) then t = ""
      t = replace(t,"""","``") : t = ctxt(t) : t = replace(t,"'","`") : t = replace(t,"""","``")
      rr("  o.Text = """ & t & """" & vbcrlf)
      if getfield(box) = cstr(rs("id")) then rr("  o.selected = true" & vbcrlf)
      rs.movenext
      if rs.eof and getfield("courseid") <> "" and getfield("courseid") <> "0" and box = "to1" then rr("  o.selected = true" & vbcrlf)
      rr("  parent.document.f1." & box & ".add o" & vbcrlf)
    loop
  next
  rr("</script>" & vbcrlf)
  rend
end if

if getfield("act") = "popcourses" then
  response.clear
  rr("<script language=vbscript>" & vbcrlf)
  box = "courseid"
  rr("  parent.document.f1." & box & ".options.length = 0" & vbcrlf)
  rr("  set o = document.CreateElement(""OPTION"")" & vbcrlf)
  rr("  o.Value = ""0""" & vbcrlf)
  rr("  o.Text = ""          """ & vbcrlf)
  rr("  o.selected = true" & vbcrlf)
  rr("  o.style.color = ""#bbbbfe""" & vbcrlf)
  rr("  parent.document.f1." & box & ".add o" & vbcrlf)
  ii = getidlist("select courseids from shifts where id = " & getfield("shiftid"))
  if ii = "" then
    s = "select id,code,title from courses where active = 'on'"
    if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
    s = s & " order by code, title"
  else
    s = "select id,code,title from courses where active = 'on' and id in(" & ii & "-1)"
  end if
  set rs = conn.execute(s)
  do until rs.eof
    rr("  set o = document.CreateElement(""OPTION"")" & vbcrlf)
    rr("  o.Value = """ & rs("id") & """" & vbcrlf)
    t = rs("code") & " " & rs("title") : t = replace(t,"""","``") : t = ctxt(t) : t = replace(t,"'","`") : t = replace(t,"""","``")
    rr("  o.Text = """ & t & """" & vbcrlf)
    if getfield(box) = cstr(rs("id")) then rr("  o.selected = true" & vbcrlf)
    rr("  parent.document.f1." & box & ".add o" & vbcrlf)
    rs.movenext
  loop
  rr("</script>" & vbcrlf)
  rend
end if

'----------------------------
if getfield("action") = "search" or getfield("action") = "shiftsubmit" then
  For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
  For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
  if getfield("action") = "search" then session(tablename & "_page") = 0
end if
if getfield("action") = "clear" then
  For Each sessionitem in Session.Contents
    if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
  Next
end if

if getfield("d1") <> "" then session("setshifts_d1") = getfield("d1")
if getfield("d2") <> "" then session("setshifts_d2") = getfield("d2")
d1 = session("setshifts_d1") : if d1 = "" then d1 = date : if specs = "brom" then do until weekday(d1) = 6 : d1 = dateadd("d", -1, d1) : loop
d2 = session("setshifts_d2") : if d2 = "" then d2 = dateadd("d",6,d1)
if session("setshifts_orderby") = "shifthour" then
  if getfield("span") = "week" then
    session("setshifts_orderby") = "abc"
  else
    session("setshifts_span") = "day"
    session("setshifts_d1") = session("setshifts_d2")
    d1 = d2
  end if
end if
d1 = cdate(d1) : d2 = cdate(d2)
if datediff("d",d1,d2) > 31 then x = errorend("_back~Cannot set range for more than a month~")
If getfield("page") <> "" Then session("setshifts_page") = getfield("page")
if isnumeric("-" & session("setshifts_page")) then session("setshifts_page") = cdbl(session("setshifts_page")) else session("setshifts_page") = 0
if not isnumeric("-" & session(tablename & "_lines")) then session(tablename & "_lines") = "30" : if getparam("DefaultSetshiftsLines") <> "" then session(tablename & "_lines") = getparam("DefaultSetshiftsLines")
if cdbl(session(tablename & "_lines")) > 300 then session(tablename & "_lines") = "300"
if cdbl(session(tablename & "_lines")) < 10 then session(tablename & "_lines") = "10"

'---------------------foodreport
if getfield("action") = "foodreport" or getfield("action") = "medreport" then
  rclear
  if getfield("foodreportemail") <> "" then x = putparam("-1000|foodreportemail", getfield("foodreportemail"))
  set csv = Server.CreateObject("ADODB.Stream") : csv.open
  set doc = Server.CreateObject("ADODB.Stream") : doc.open
  doc.writetext("<body style='direction:rtl;'>")
  if getfield("action") = "medreport" then
    oo = getidlist("select id from workertypes where instr(title, '����') > 0")
    oo = getidlist("select id from workers where workertypeid in(" & oo & "-1)")
    title = "��� ������"
  else
    title = "��� ������"
  end if
  s = "select id,title,hour1,hour1b from shifts" & okreadsql("shifts") & " order by title"
  s = replace(s," and "," where ",1,1,1)
  set rs = conn.execute(sqlfilter(s))
  c = 0 : cc = 0
  do until rs.eof
    c = c + 1 : cc = cc + 1
    if c = 1 then
      t = "" : if cc <> 1 then t = "page-break-before:always;"
      doc.writetext("<table width=100% style='direction:rtl; " & t & "' cellpadding=4 cellspacing=1 bgcolor=#aaaaaa><tr bgcolor=dddddd><td><nobr><b>" & title)
      csv.writetext(tocsv(title) & ",")
      d = d1 : do until d > d2 : t = translate("~weekday" & weekday(d) & "~ " & d) : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",") : d = dateadd("d", 1, d) : loop
    end if
    t = rs("title") & " " & rs("hour1") & "-" & rs("hour1b") : doc.writetext("<tr bgcolor=ffffff><td bgcolor=#eeeeee><nobr><b>" & t) : csv.writetext(vbcrlf & tocsv(t) & ",")
    d = d1

    do until d > d2
      if getfield("action") = "medreport" then
        s = "select workerid from workershifts"
        if instr(",techjet,", "," & specs & ",") > 0 then
          s = s & " where shiftid = " & rs("id")
        else
          s = s & " where (hour1 = '" & rs("hour1") & "' and hour1b = '" & rs("hour1b") & "')"
        end if
        s = s & " and date1 = " & sqldate(d)
        s = s & " and workerid in(" & oo & "-1)"
        ii = getidlist(s)
        s = "select firstname,lastname,phone1 from workers where id in(" & ii & "-1) order by firstname, lastname"
        set rs1 = conn.execute(sqlfilter(s))
        doc.writetext("<td><nobr>")
        csv.writetext("""")
        do until rs1.eof
          t = rs1("firstname") & " " & rs1("lastname") & " " & strfilter(rs1("phone1"), "0123456789")
          doc.writetext(t & "<br>") : csv.writetext(tocsv(t))
          rs1.movenext
          if not rs1.eof then csv.writetext(chr(10))
        loop
        csv.writetext(""",")
      else
        s = "select count(id) as x from workershifts"
        if instr(",techjet,", "," & specs & ",") > 0 then
          s = s & " where shiftid = " & rs("id")
        else
          s = s & " where (hour1 = '" & rs("hour1") & "' and hour1b = '" & rs("hour1b") & "')"
        end if
        s = s & " and date1 = " & sqldate(d)
        if instr(",techjet,", "," & specs & ",") = 0 then s = s & " and foodsupplierid <> 0"
        set rs1 = conn.execute(sqlfilter(s))
        t = rs1("x") : doc.writetext("<td><nobr>" & t) : csv.writetext(tocsv(t) & ",")
      end if
      d = dateadd("d", 1, d)
    loop

    rs.movenext
    if c = 30 and getfield("action2") = "doc" then doc.writetext("</table><br>") : c = 0
  loop
  doc.writetext("</table>")

  doc.position = 0 : doc1 = doc.readtext(doc.size) : doc.close
  csv.position = 0 : csv1 = csv.readtext(csv.size) : csv.close
  rr doc1

  if getfield("foodreportemail") <> "" then
    'f = "upload/foodreportemail_" & createpassword(6) & ".csv"
    'x = addlog(server.mappath(f), csv1)
    'subject = getparam("applicationdisplay") & "- ��� ������"
    'body = "<a href=" & siteurl & f & ">��� ������</a>"
    'x = sendemail(getparam("siteemail") & "/", getfield("foodreportemail"), "", "", subject, body)
    subject = getparam("applicationdisplay") & "- ��� ������"
    x = sendemail(getparam("siteemail") & "/", getfield("foodreportemail"), "", "", subject, doc1)
  end if

  if getfield("action2") <> "doc" then
    response.clear
    response.ContentType = "application/csv"
    response.AddHeader "Content-Disposition", "filename=1.csv"
    response.write csv1
  end if

  rend
end if

'---------------------foodreport2
if getfield("action") = "foodreport2" then
  rclear
  if getfield("foodreportemail") <> "" then x = putparam("-1000|foodreportemail", getfield("foodreportemail"))
  set csv = Server.CreateObject("ADODB.Stream") : csv.open
  set doc = Server.CreateObject("ADODB.Stream") : doc.open
  doc.writetext("<body style='direction:rtl;'>")
  title = "��� ������ - " & getfield("d")
  s = "select id,title from units" & okreadsql("units") & " order by title"
  s = replace(s," and "," where ",1,1,1)
  set rs = conn.execute(sqlfilter(s))
  c = 0 : cc = 0 : fff = "" : totals = ""
  do until rs.eof
    c = c + 1 : cc = cc + 1
    if c = 1 then
      t = "" : if cc <> 1 then t = "page-break-before:always;"
      doc.writetext("<table width=100% style='direction:rtl; " & t & "' cellpadding=4 cellspacing=1 bgcolor=#aaaaaa><tr bgcolor=#dddddd><td width=300><nobr><b>" & title)
      csv.writetext(tocsv(title) & ",")
      set rs1 = conn.execute(sqlfilter("select id,title from foodsuppliers order by title"))
      do until rs1.eof
        if cc = 1 then fff = fff & rs1("id") & ","
        t = rs1("title") : doc.writetext("<td><nobr><b>" & t) : csv.writetext(tocsv(t) & ",")
        rs1.movenext
      loop
      doc.writetext("<td><nobr><b>~Total~") : csv.writetext(tocsv(translate("~Total~")) & ",")
    end if

    t = rs("title") : add1  = "<tr bgcolor=#ffffff><td bgcolor=#dddddd><nobr><b>" & t : add2 = vbcrlf & tocsv(t) & ","

    ww = getidlist("select id from workers where unitid = " & rs("id"))
    ff = fff : tot = 0
    do until ff = ""
      f = left(ff,instr(ff,",")-1) : ff = mid(ff, instr(ff,",")+1)
      s = "select count(id) as x from workershifts"
      s = s & " where foodsupplierid=" & f
      s = s & " and date1 = " & sqldate(getfield("d"))
      s = s & " and workerid in(" & ww & "-1)"
      set rs1 = conn.execute(sqlfilter(s))
      t = rs1("x") : tot = tot + t : add1 = add1 & "<td><nobr>" & t : add2 = add2 & tocsv(t) & ","
      totals = setval(totals, f, cdbl("0" & getval(totals, f)) + t)
    loop
    add1 = add1 & "<td bgcolor=#dddddd><nobr>" & tot : add2 = add2 & tocsv(tot) & ","
    if tot > 0 then doc.writetext(add1) : csv.writetext(add2)
    rs.movenext
    if c = 30 and getfield("action2") = "doc" then doc.writetext("</table><br>") : c = 0
  loop
  '---totals

  doc.writetext("<tr bgcolor=#dddddd><td>~Total~") : csv.writetext(vbcrlf & tocsv(translate("~Total~")) & ",")
  ff = fff : tot = 0 : add1 = "" : add2 = ""
  do until ff = ""
    f = left(ff,instr(ff,",")-1) : ff = mid(ff, instr(ff,",")+1)
    t = getval(totals, f) : add1 = add1 & "<td><nobr>" & t : add2 = add2 & tocsv(t) & ","
    tot = tot + t
  loop
  add1 = add1 & "<td><nobr>" & tot : add2 = add2 & tocsv(tot) & ","
  doc.writetext(add1) : csv.writetext(add2)

  doc.writetext("</table>")

  doc.position = 0 : doc1 = doc.readtext(doc.size) : doc.close
  csv.position = 0 : csv1 = csv.readtext(csv.size) : csv.close
  rr doc1

  if getfield("foodreportemail") <> "" then
    'f = "upload/foodreportemail_" & createpassword(6) & ".csv"
    'x = addlog(server.mappath(f), csv1)
    'subject = getparam("applicationdisplay") & "- ��� ������"
    'body = "<a href=" & siteurl & f & ">��� ������</a>"
    'x = sendemail(getparam("siteemail") & "/", getfield("foodreportemail"), "", "", subject, body)
    subject = getparam("applicationdisplay") & "- ��� ������"
    x = sendemail(getparam("siteemail") & "/", getfield("foodreportemail"), "", "", subject, doc1)
  end if

  if getfield("action2") <> "doc" then
    response.clear
    response.ContentType = "application/csv"
    response.AddHeader "Content-Disposition", "filename=1.csv"
    response.write csv1
  end if

  rend
end if

'------------------countworkerfoods
if getfield("action") = "countworkerfoods" then
  s = "select workerid, foodsupplierid from workershifts where foodsupplierid > 0"
  s = s & " and date1 >= " & sqldate(getfield("countworkerfoodsd1"))
  s = s & " and date1 <= " & sqldate(getfield("countworkerfoodsd2"))
  set rs = conn.execute(sqlfilter(s))
  Set wfdict = Server.CreateObject("Scripting.Dictionary")
  do until rs.eof
    wfdict(cstr(rs("workerid"))) = cdbl("0" & wfdict(cstr(rs("workerid")))) + 1
    rs.movenext
  loop
  rclear
  response.ContentType = "application/csv"
  response.AddHeader "Content-Disposition", "filename=1.csv"
  rr "�� ����,�� �����,���� ����,���� ������," & vbcrlf
  s = "select id,firstname,lastname,code from workers where lastname <> '_TEMPLATE_'" & okreadsql("workers") & " order by firstname, lastname, code"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    rr tocsv(rs("firstname")) & ","
    rr tocsv(rs("lastname")) & ","
    rr tocsv(rs("code")) & ","
    rr wfdict(cstr(rs("id"))) & ","
    rr vbcrlf
    rs.movenext
  loop
  rend
end if

'--------------------loadcsv
if session("usergroup") = "admin" and trim(getfield("loadcsv")) <> "" then

  server.scripttimeout = 100000 'seconds
  set thefile = fs.opentextfile(server.mappath(getfield("loadcsv")))
  csvdd = thefile.readall & vbcrlf
  thefile.close

  '-------get worker codes
  wc = ""
  set st = Server.CreateObject("ADODB.Stream") : st.open
  sq = "select id,code from workers where firstname <> ''"
  sq = sq & okreadsql("workers")
  set rs1 = conn.execute(sqlfilter(sq))
  do until rs1.eof
    st.writetext(rs1("code") & "=" & rs1("id") & "|")
    rs1.movenext
  loop
  st.position = 0 : wc = "|" & st.readtext(st.size) : st.close

  '-------first column
  c = 0 : datecols = "" : codecol = ""
  a = left(csvdd,instr(csvdd,vbcrlf)-1) & "," : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
  do until a = ""
    c = c + 1
    b = trim(lcase(left(a,instr(a,",")-1))) : a = mid(a,instr(a,",")+1)
    if b = lcase(translate("~code~")) or instr(",���,��� ����,��� �����,code,worker code,workers code,worker`s code,", "," & b & ",") > 0 then codecol = c
    b = strfilter(b,"0123456789/.")
    if isdate(b) then datecols = setval(datecols, c, b)
  loop

  c = 0
  do until csvdd = ""
    a = left(csvdd,instr(csvdd,vbcrlf)-1) & ",,,," : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    col = 0 : date1 = "" : workerid = "" : hour1 = "" : hour1b = ""
    for i = 1 to countstring(a,",")
      col = col + 1
      if instr(a,",") = 0 then exit for
      if left(a,1) = """" and instr(2,a,"""") >= 2 then
        x = 1
        do until x >= len(a)
          x = x + 1
          if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
          if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
        loop
        b = left(a,x) : a = mid(a,x+2)
        b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
      else
        b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
      end if
      b = trim(chtm(b))
      '---------
      if cstr(col) = cstr(codecol) then
        if b <> "" then workerid = getval(wc, b)
        if workerid = "" and b <> "" then du = showerror("~Worker Not Found~ " & b)
      end if
      if getval(datecols, col) <> "" and workerid <> "" then
        date1 = getval(datecols, col)
        if instr(lcase(b),lcase(translate("~Fixed~"))) + instr(lcase(b),"fixed") + instr(lcase(b),"����") > 0 then b = ""
        if session("setshifts_addition") <> "on" then
          s = "delete * from workershifts where workerid = " & workerid & " and date1 = " & sqldate(date1)
          closers(rs) : set rs = conn.execute(sqlfilter(s))
        end if
        if b <> "" then
          ways = ""
          b = replace(b,"�����","��")
          hour1 = getfromto(" " & b & " ", "", translate("~From~ "), " ", false, false) : if ishour(hour1) then ways = ways & "A" else hour1 = "00:00"
          hour1b = getfromto(" " & b & " ", "", translate("~To~ "), " ", false, false) : if ishour(hour1b) then ways = ways & "B" else hour1b = "00:00"
          if instr(b,"��� ����") + instr(b,"����") + instr(b,"No Ride") > 0 then ways = "X"
          if ways <> "" then
            s = "insert into workershifts(date0,date1,workerid,ways,hour1,hour1b,foodsupplierid,from1,to1,courseid,shiftid,data1,userid0) values("
            s = s & sqldate(now) & "," & sqldate(date1) & "," & workerid & ",'" & ways & "','" & hour1 & "','" & hour1b & "',0,0,0,0,0,''" & getfield("shiftid") & "," & session("userid") & ")"
            closers(rs) : set rs = conn.execute(sqlfilter(s))
            c = c + 1
          end if
        end if
      end if
    next
  loop
  if msg = "" then msg = c & " ~Shifts were updated~"
end if

'----------------setdone---------------------------------------------------
if request_action = "setdone" then
  s = "insert into workershifts(date0,date1,workerid,data1,userid0) values("
  s = s & sqldate(now) & "," & sqldate(getfield("d")) & ", -" & session("userid") & ",''," & session("userid") & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if

'----------------submit---------------------------------------------------
if request_action = "shiftsubmit" then

  'if specs = "flextronics" then
  '  if getfield("foodsupplierid") <> "" and getfield("foodsupplierid") <> "0" and getfield("hour1b") <> "19:15" then
  '    du = errorend("_back���� ����� ��� ���� �� ������� ��������� � 19:15")
  '  end if
  'end if

  a = getfield("ids") : cc = 0 : reporteddays = ""
  ways = request_ways : if ways = "" then ways = "AB"

  days = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
  if session("setshifts_shiftid") <> "" and session("setshifts_shiftid") <> "0" then
    s = "select id,title,hour1,hour1b,days from shifts"
    s = s & " where id = " & session("setshifts_shiftid")
    set rs = conn.execute(sqlfilter(s))
    if not rs.eof then days = rs("days")
  end if
  'hour1 = "00:00" : hour1b = "00:00" : title = "" : if not rs.eof then hour1 = rs("hour1") : hour1b = rs("hour1b") : title = rs("title")

  hour1 = "00:00" : if getfield("hour1") <> "" then hour1 = getfield("hour1")
  hour1b = "00:00" : if getfield("hour1b") <> "" then hour1b = getfield("hour1b")
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    d = d1
    do until d > d2
      fromtemplate = "" : if session("setshifts_templateid") <> "" and session("setshifts_templateid") <> "0" then fromtemplate = "on"
      k = "u" & b & "d" & replace(d,"/","_")
      if getfield("action2") = "delall" or getfield(k) = "on" then

        if fromtemplate = "on" then
          session("setshifts_shiftid") = "0"
          session("setshifts_ways") = "AB"
          session("setshifts_addition") = ""
          x = dupshifts(b & ",", session("setshifts_templateid"), "", "", d, d)
          cc = cc + 1

        elseif instr(days,getweekday(d)) = 0 and instr("X-", ways) = 0 then
          if instr(reporteddays,getweekday(d)) = 0 then
            reporteddays = reporteddays & weekday(d)
            du = showerror("~Cannot set the selected shift to~ ~weekday" & weekday(d) & "~")
          end if

        else

          if instr(",X,-,-A,-B,", "," & ways & ",") > 0 then
            s = "select hour1,hour1b,ways from workershifts where workerid = " & b & " and date1 = " & sqldate(d) & " order by hour1"
            set rs1 = conn.execute(sqlfilter(s))
            h1 = "23:59" : h2 = h1
            if not rs1.eof then h1 = rs1("hour1") : h2 = rs1("hour1b") : if rs1("ways") = "B" then h1 = h2
            h1 = cdate(d & " " & h1) : h2 = cdate(d & " " & h2) : if h2 <= h1 then h2 = dateadd("d", 1, h2)
            if ways = "-A" then h2 = h1
            if ways = "-B" then h1 = h2
          else
            h1 = hour1  : if instr(h1,":") = 0 or instr(ways, "A") = 0 then h1 = "23:59"
            h2 = hour1b : if instr(h2,":") = 0 or instr(ways, "B") = 0 then h2 = "23:59"
            h1 = cdate(d & " " & h1) : h2 = cdate(d & " " & h2) : if h2 <= h1 then h2 = dateadd("d", 1, h2)
          end if

          ddd = getproduceduntil
          if (h1 < ddd and h2 >= ddd) then du = errorend("_back~Cannot set shifts to a date~ " & day(ddd) & "/" & month(ddd) & "/" & year(ddd) & " ~to an hour before~ " & hour(ddd) & ":" & minute(ddd) & ". ~the COLLECT hour has passed. Please select DISTRIBUTE ONLY~")
          if (h1 < ddd and h2 < ddd) then du = errorend("_back~Cannot set shifts to a date before~ " & ddd)
          if getparam("LockWorkerShifts") = "y" and session("setshifts_shiftid") <> "" and instr("X-", ways) = 0 then
            t = getidlist("select shiftids from workers where id = " & b)
            if t <> "" and instr("," & t, "," & session("setshifts_shiftid") & ",") = 0 then
              du = showerror("~Cannot set the selected shift for worker~ " & getworkername(b))
              exit do
            end if
          end if

          '-------------------------------------
          if specs = "techjet" then
            s = "select shiftid,hour1,hour1b from workershifts where workerid = " & b & " and date1 = " & sqldate(d)
            set rs1 = conn.execute(sqlfilter(s))
            if not rs1.eof then
              old = rs1("shiftid")
              if request_ways = "X" then hour1 = rs1("hour1") : hour1b = rs1("hour1b")
              s = "select data1 from shifts where id = " & old
              set rs1 = conn.execute(sqlfilter(s))
              if not rs1.eof then
                t = getval(rs1("data1"), "shiftids") & getfield("shiftid") & ","
                if request_ways <> "X" and instr("," & t, "," & getfield("shiftid") & ",") = 0 then
                  du = errorend("_back�� ���� ������ �� ������ '" & gettitle("shifts", old) & "' ������ '" & gettitle("shifts", getfield("shiftid")) & "'")
                end if
              end if
            end if
          end if

          '---- shufersal limitations - appears also, in a different context in _shufersal_workershifts.asp
          if specs = "shufersal" and instr("X-", ways) = 0 then
            sq = "select id,data1 from workers where id = " & b
            set rs1 = conn.execute(sqlfilter(sq))
            if not rs1.eof then
              h1 = getfield("hour1") : h2 = getfield("hour1b")
              workerdata1 = rs1("data1")
              w = getval(workerdata1, "allowways") : if w = "" then w = "AB"
              tw = ""
              if w = "A" then tw = "����� ����"
              if w = "B" then tw = "����� ����"
              if instr(ways, "A") > 0 and instr(w, "A") = 0 then du = showerror("������ ����� �� ����� " & getworkername(b) & " - " & tw) : exit do
              if instr(ways, "B") > 0 and instr(w, "B") = 0 then du = showerror("������ ����� �� ����� " & getworkername(b) & " - " & tw) : exit do
              t = "0" & getval(workerdata1, "disablehourid")
              sq = "select hour1,hour2 from disablehours where id = " & t
              set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then
                t1 = rs1("hour1") : t2 = rs1("hour2")
                t = d & " " & h1
                if getweekday(t) < 6 or ((getweekday(t) = 6 or getweekday(t) = 9) and h1 < "14:00") then
	                if instr(ways, "A") > 0 and (h1 >= t1 and h1 <= t2) then
	                  du = showerror("�� ���� ����� ����� ����� " & getworkername(b) & " - " & t1 & " �� " & t2) : exit do
	                end if
                end if
                t = d & " " & h2 : if cdate(t) < cdate(d & " " & h1) then t = dateadd("d", 1, t)
                if getweekday(t) < 6 or ((getweekday(t) = 6 or getweekday(t) = 9) and h2 < "14:00") then
	                if instr(ways, "B") > 0 and (h2 >= t1 and h2 <= t2) then
	                  du = showerror("�� ���� ����� ����� ����� " & getworkername(b) & " - " & t1 & " �� " & t2) : exit do
	                end if
                end if

              end if
            end if
          end if

          if (getfield("addition") = "" and ways <> "-A" and ways <> "-B") or ways = "X" or ways = "-" then
            s = "delete * from workershifts where workerid = " & b & " and date1 = " & sqldate(d)
            set rs = conn.execute(sqlfilter(s))
          else
            s = "delete * from workershifts where workerid = " & b & " and date1 = " & sqldate(d) & " and ways = 'X'"
            set rs = conn.execute(sqlfilter(s))
          end if
          if ways = "-A" or ways = "-B" then
            s = "select id,ways,data1 from workershifts where workerid = " & b & " and date1 = " & sqldate(d)
            if rs.state = 1 then rs.close
            rs.open sqlfilter(s), conn, 0, 1, 1 'read only
            if rs.eof then du = showerror("�� ���� ����� ����� ������ ������. ���� ������ ����� ������")
            do until rs.eof
              t = rs("ways") : t = replace(t, replace(ways,"-",""), "")
              data1 = ctxt(rs("data1")) : data1 = setval(data1, "manually", "on")
              if t = "" then
                s = "delete * from workershifts where id = " & rs("id")
                set rs1 = conn.execute(sqlfilter(s))
              else
                s = "update workershifts set ways = '" & t & "', data1 = '" & data1 & "' where id = " & rs("id")
                set rs1 = conn.execute(sqlfilter(s))
              end if
              rs.movenext
            loop
          elseif getfield(k) = "on" and ways <> "-" then
            from1 = getfield("from1")
            to1 = getfield("to1")
            courseid = 0
            if from1 = "0" and to1 = "0" or (specs = "iai" and from1 = "0" and to1 = session("defaultstation")) then
              sq = "select courseid,from1,to1,courseid2,from2,to2 from workercourses where workerid = " & b
              sq = sq & " and instr(cstr(',' & shiftids), '," & session("setshifts_shiftid") & ",') > 0"
              sq = sq & " and instr(',' & days, '," & getweekday(d) & ",') > 0"
              set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then
                courseid = rs1("courseid") : from1 = rs1("from1") : to1 = rs1("to1")
                courseid2 = rs1("courseid2") : from2 = rs1("from2") : to2 = rs1("to2")
                if cstr(courseid2) <> "0" and ways = "B" then
                  from1 = from2 : to1 = to2
                elseif cstr(courseid2) <> "0" and ways = "AB" then
                  from1v = from1 : to1v = to1
                  from2v = from2 : to2v = to2
                  ways = "#ways" : from1 = "#from1" : to1 = "#to1"
                end if
              end if
            end if
            if getfield("courseid") <> "" and getfield("courseid") <> "0" then courseid = getfield("courseid")
            'if getfield("splittingfrom1") <> "" then from1 = getfield("splittingfrom1")

            foodsupplierid = getfield("foodsupplierid")
            if getfield("workerfood" & b) <> "" then
              foodsupplierid = getfield("workerfood" & b)
              'sq = "select id,data1 from workers where id = " & b
              'set rs1 = conn.execute(sqlfilter(sq))
              'if not rs1.eof then
              '  t = rs1("data1") : t = setval(t, "lastfoodsupplierid", foodsupplierid)
              '  sq = "update workers set data1 = '" & t & "' where id = " & b
              '  set rs1 = conn.execute(sqlfilter(sq))
              'end if
            end if
            data1 = "manually=on"
            t = getfield("altaddress") : if t <> "" then data1 = setval(data1, "altaddress", t)
            t = getfield("remarks") : if t <> "" then data1 = setval(data1, "remarks", t)
            data1 = setval(data1, "xtype", xtype)

            f = "" : v = ""
            f = f & "date1," : v = v & sqldate(d) & ","
            f = f & "workerid," : v = v & b & ","
            f = f & "ways," : v = v & "'" & ways & "',"
            f = f & "hour1," : v = v & "'" & hour1 & "',"
            f = f & "hour1b," : v = v & "'" & hour1b & "',"
            f = f & "foodsupplierid," : v = v & foodsupplierid & ","
            f = f & "from1," : v = v & from1 & ","
            f = f & "to1," : v = v & to1 & ","
            f = f & "courseid," : v = v & courseid & ","
            f = f & "shiftid," : v = v & "0" & getfield("shiftid") & ","
            f = f & "date0," : v = v & sqldate(now) & ","
            f = f & "data1," : v = v & "'" & data1 & "',"
            f = f & "userid0," : v = v & session("userid") & ","
            f = left(f, len(f) - 1): v = left(v, len(v) - 1)
            s = "insert into workershifts(" & f & ") values(" & v & ")"
            if instr(ways, "#") = 0 then
              closers(rs) : set rs = conn.execute(sqlfilter(s))

              '-----------------vishay checks
              if specs = "vishay" and ways = "AB" then
                t1 = dateadd("d", 1 - weekday(d), d) : t2 = dateadd("d", 6, t1)
                errors = "" : longs = 0 : shorts = 0 : nights = 0
                lastjobend = dateadd("d", -1, t1) & " 00:00:00" : nicebreak = ""
                lastlongday = "1/1/2000" : sequencelongs = 0
                s = "select date1,hour1,hour1b from workershifts where workerid = " & b
                s = s & " and ways = 'AB'"
                s = s & " and date1 >= " & sqldate(dateadd("d", -5, t1))
                s = s & " and date1 <= " & sqldate(t2)
                s = s & " order by date1, hour1"
                set rs = conn.execute(sqlfilter(s))
                do until rs.eof
                  e1 = rs("date1") & " " & rs("hour1") : e2 = rs("date1") & " " & rs("hour1b") : if cdate(e1) > cdate(e2) then e2 = dateadd("d", 1, e2)
                  h1 = mid(e1,instr(e1," ")+1) : h2 = mid(e2,instr(e2," ")+1)
                  t = datediff("h", lastjobend, e1) : if t >= 36 then nicebreak = "on"
                  x = datediff("n", h1, h2)
                  'rr e1 & " - " & e2 & " - " & x & "<br>"
                  if rs("date1") >= cdate(t1) and x >= 600 then longs = longs + 1
                  if rs("date1") >= cdate(t1) and x <  600 then shorts = shorts + 1
                  if rs("date1") >= cdate(t1) and h1 >= "18:00" then nights = nights + 1
                  if x >= 600 then
                    if lastlongday <> "" then
                      if cdate(lastlongday) = dateadd("d", -1, rs("date1")) then
                        sequencelongs = sequencelongs + 1
                        if sequencelongs = 4 then errors = errors & " �� ���� ������ 5 ������ ������ ����."
                      else
                        sequencelongs = 0
                      end if
                    end if
                    lastlongday = rs("date1")                     
                  end if
                  lastjobend = e2
                  rs.movenext
                loop
                'rr longs & "<br>" & shorts & "<br>" & nights & "<hr>"
                if nicebreak = "" and cdate(lastjobend) > dateadd("h", -12, t2) then errors = errors & " ��� ����� �� 36 ���� ����� ���."
                if nights >= 5 then errors = errors & " �� ���� ������ 5 ������ ���� ����� ���."
                if errors <> "" then
                  errors = "���� " & getworkername(b) & " ����� " & d & " - " & errors & " �� ���� ������ �����."
                  du = showerror(errors)
                  s = "select max(id) as id from workershifts where workerid = " & b
                  closers(rs) : set rs = conn.execute(sqlfilter(s))
                  lastid = rs("id")
                  s = "delete * from workershifts where id = " & lastid
                  closers(rs) : set rs = conn.execute(sqlfilter(s))
                end if
              end if

              '-----------------brom checks
              if instr(",brom,haifachem,osem,", "," & specs & ",") > 0 and ways = "AB" then
                e = dateadd("d", -12, d) : errors = "" : daysinarow = 0 : nights = "000000000000"
                t1 = dateadd("d", 1 - weekday(d), d) : t2 = dateadd("d", 6, t1) : inweek = 0
                do until e >= dateadd("d", +12, d)
                  s = "select date1,hour1,hour1b from workershifts where workerid = " & b
                  s = s & " and ways = 'AB'"
                  s = s & " and date1 = " & sqldate(e)
                  set rs = conn.execute(sqlfilter(s))
                  if rs.eof then
                    daysinarow = 0 : nights = nights & "0"
                  else
                    daysinarow = daysinarow + 1 : if daysinarow = 13 then errors = errors & " �� ���� ������ ��� 12 ���� �� ����� �������."
                    h1 = rs("hour1") : h2 = rs("hour1b") : e1 = rs("date1") & " " & h1 : e2 = rs("date1") & " " & h2 : if cdate(e1) > cdate(e2) then e2 = dateadd("d", 1, e2)
                    if d = rs("date1") and cdate(e1) < cdate(rs("date1") & " 12:00:00") and right(nights,1) = "1" then errors = errors & " ���� ������ ����� ���� ���� ����� ����."
                    if cdate(e2) > cdate(rs("date1") & " 22:00:00") then nights = nights & "1" else nights = nights & "0"
                    if e >= t1 and e <= t2 then inweek = inweek + datediff("n", e1, e2) : if inweek > 58 * 60 and instr(errors, "���� �����") = 0 then errors = errors & " ���� ������ ��� 58 ���� �����."
                    if (lastjobweekday = 6 and weekday(e) = 7) or (lastjobweekday = 7 and weekday(e) = 1) then
                      if datediff("n", lastjobend, e1) < lastjobdur and instr(errors, "����� ������ �����") = 0 then errors = errors & " ���� ����� ���� ����� ����� ����� ����� ������ �����."
                    end if
                  end if
                  nights = mid(nights,2) : if countstring(nights,"1") > 7 and instr(errors, "����� ��� �����") = 0 then errors = errors & " ���� ��� ��� �� 12 ���� ���� ����� ����� 7 ����� ��� �����."
                  lastjobend = e2 : lastjobdur = datediff("n", e1, e2) : lastjobweekday = weekday(e)
                  e = dateadd("d", 1, e)
                loop
                if errors <> "" then
                  errors = "���� " & getworkername(b) & " ����� " & d & " - " & errors & " �� ���� ������ �����."
                  du = showerror(errors)
                  '---force disable
                  's = "select max(id) as id from workershifts where workerid = " & b
                  'closers(rs) : set rs = conn.execute(sqlfilter(s))
                  'lastid = rs("id")
                  's = "delete * from workershifts where id = " & lastid
                  'closers(rs) : set rs = conn.execute(sqlfilter(s))
                end if
              end if
              '---

            else
              sq = s
              sq = replace(sq, "#ways", "A")
              sq = replace(sq, "#from1", from1v)
              sq = replace(sq, "#to1", to1v)
              closers(rs) : set rs = conn.execute(sqlfilter(sq))
              sq = s
              sq = replace(sq, "#ways", "B")
              sq = replace(sq, "#from1", from2v)
              sq = replace(sq, "#to1", to2v)
              closers(rs) : set rs = conn.execute(sqlfilter(sq))
              ways = request_ways
            end if

            '--------learn worker
            if getparam("LearnWorkerCourses") = "y" then
              s = "select courseid1,from1,to1 from workers where id = " & b
              set rs = conn.execute(sqlfilter(s))
              j = 0
              if cstr("" & rs("courseid1")) <> "0" then j = 1
              if trim(gettitle("stations", rs("from1"))) <> "" then j = 1
              if trim(gettitle("stations", rs("to1"))) <> "" then j = 1
              if j = 0 then
                s = "update workers set "
                s = s & "courseid1 = 0" & courseid & ","
                s = s & "from1 = 0" & from1 & ","
                s = s & "to1 = 0" & to1 & ","
                s = left(s, len(s) - 1) & " "
                s = s & "where id = " & b
                s = replace(s, "#from1", from1v)
                s = replace(s, "#to1", to1v)
                set rs = conn.execute(sqlfilter(s))
              end if
            end if

            '--------------------learn workercourse
            if getparam("LearnWorkerCourses") = "y" and session("setshifts_shiftid") <> "0" and cstr(courseid) <> "" and cstr(courseid) <> "0" then
              s = "select id from workercourses where workerid = " & b & " and shiftid = " & session("setshifts_shiftid")
              set rs = conn.execute(sqlfilter(s))
              if rs.eof then
                f = "" : v = ""
                f = f & "shiftids," : v = v & "'" & session("setshifts_shiftid") & ",',"
                f = f & "courseid," : v = v & courseid & ","
                f = f & "days," : v = v & "'1,2,3,4,5,6,7," & getparam("ExtraWeekDays") & "',"
                f = f & "from1," : v = v & from1 & ","
                f = f & "to1," : v = v & to1 & ","
                f = f & "workerid," : v = v & b & ","
                f = f & "courseid2," : v = v & 0 & ","
                f = f & "from2," : v = v & 0 & ","
                f = f & "to2," : v = v & 0 & ","
                f = left(f, len(f) - 1): v = left(v, len(v) - 1)
                s = "insert into workercourses(" & f & ") values(" & v & ")"
                s = replace(s, "#from1", from1v)
                s = replace(s, "#to1", to1v)
                closers(rs) : set rs = conn.execute(sqlfilter(s))
              end if
            end if

          end if
          cc = cc + 1
        end if
      end if
      d = dateadd("d",1,d)
    loop
  loop
  if msg = "" then msg = cc & " ~Shifts were updated~"
  request_action = "groupshifts"
  if getfield("action2") = "delall" then rr "<script language=vbscript> document.location = ""?" & replace(querystring , "&action2=delall", "") & """</script>"
end if
'==================================================================================
sitelock = ""
sq = "select id,data1 from sites"
set rs1 = conn.execute(sqlfilter(sq))
do until rs1.eof
  d = rs1("data1")
  sitelock = setval(sitelock, rs1("id"), getval(d, "t setshiftsuntil"))
  sitelock = setval(sitelock, rs1("id") & "weekend", getval(d, "t weekendsetshiftsuntil"))
  rs1.movenext
loop

'----------------
  weekplans = "" : lastn = "" : d = d1
  if session("weekplans_" & d1 & "_" & d2) = "" then
	  set st = Server.CreateObject("ADODB.Stream") : st.open
	  do until d > d2
	    s = "select day1,ways,hour1,hour1b,workerid,date1,date2 from weekplan"
	    s = s & " where workerid in(select id from workers where 0=0" & okreadsql("workers") & ")"
	    s = s & " and day1 = '" & weekday(d) & "'"
	    s = s & " and (year(date1) = 1900 or date1 <= " & sqldate(d) & ")"
	    s = s & " and (year(date2) = 1900 or date2 >= " & sqldate(d) & ")"
	    s = s & " order by workerid, ways"
	    set rs1 = conn.execute(sqlfilter(s))
	    lastn = ""
	    do until rs1.eof
	      v = ""
	      if instr("" & rs1("ways"),"A") > 0 then v = v & translate(" ~Arrival~ ") & rs1("hour1")
	      if instr("" & rs1("ways"),"B") > 0 then v = v & translate(" ~Distribute~ ") & rs1("hour1b")
	      if instr("" & rs1("ways"),"X") > 0 then v = v & translate(" ~No Ride~ ")
	      v = translate(" (~Fixed~) ") & v & "^"
	      n = "u" & rs1("workerid") & "d" & d
	      if n <> lastn then st.writetext("|" & n & "=") : lastn = n
	      st.writetext(v)
	      rs1.movenext
	    loop
	    d = dateadd("d",1,d)
	  loop
	  st.writetext("|")
	  st.position = 0 : session("weekplans_" & d1 & "_" & d2) = st.readtext(st.size) : st.close
  end if
  weekplans = session("weekplans_" & d1 & "_" & d2)

  '--start-----------------------------------------------------------------------------------
  ShowTemplatesButton = "" : if getparam("UseWorkerTemplates") = "y" and specs <> "techjet" then ShowTemplatesButton = "on"
  rr "<iframe id=frpopstations frameborder=0 style='width:0; height:0; position:absolute; visibility:hidden; top:-1000;'></iframe>"
  rr "<iframe id=frpopcourses frameborder=0 style='width:0; height:0;' position:absolute; visibility:hidden; top:-1000;'></iframe>"

  rr "<table cellspacing=1 cellpadding=4>"
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=shiftsubmit><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<tr><td class=list_name id=tr0_form_title colspan=99><img src='icons/i_clock.gif' ~iconsize~> ~Set Shifts~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")

  rr "<tr><td bgcolor=#dddddd colspan=99 style='padding:10;'><nobr>"
  rr "<img src=button_~langside~.png style='vertical-align:top;'>"

  'rr "<img src=button_sep.png style='vertical-align:top;'>"
  rr "<a id=buttonmode0 href=#aa onclick='vbscript"
  rr ":buttonmode0.style.color = ""#aa0000"""
  rr ":buttonmode1.style.color = ""#444444"""
  if ShowTemplatesButton = "on" then rr ":buttonmode2.style.color = ""#444444"""
  rr ":buttonsetshifts.style.visibility = ""hidden"""
  rr ":tr01.style.visibility = ""hidden""  : tr01.style.position = ""absolute"" : tr01.style.top = -1000"
  if ShowTemplatesButton = "on" then rr ":tr02.style.visibility = ""hidden""  : tr02.style.position = ""absolute"" : tr02.style.top = -1000"
  rr ":tr00.style.visibility = ""visible"" : tr00.style.position = ""relative"" : tr00.style.top = 0"
  rr ":fr1.document.location = ""?mode=show"""
  rr "'"
  t = "444444" : if session("setshifts_mode") = "show" then t = "aa0000"
  rr " style='overflow-x:hidden; width:90; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>����� �����</b></a>"

  rr "<img src=button_sep.png style='vertical-align:top;'>"
  rr "<a id=buttonmode1 href=#aa onclick='vbscript"
  rr ":buttonmode1.style.color = ""#aa0000"""
  if ShowTemplatesButton = "on" then rr ":buttonmode2.style.color = ""#444444"" : f1.templateid.value = ""0"""
  rr ":buttonmode0.style.color = ""#444444"""
  rr ":buttonsetshifts.style.visibility = ""visible"""
  rr ":tr01.style.visibility = ""visible"" : tr01.style.position = ""relative"" : tr01.style.top = 0"
  if ShowTemplatesButton = "on" then rr ":tr02.style.visibility = ""hidden""  : tr02.style.position = ""absolute"" : tr02.style.top = -1000"
  rr ":tr00.style.visibility = ""hidden""  : tr00.style.position = ""absolute"" : tr00.style.top = -1000"
  rr ":fr1.document.location = ""?mode=set"""
  rr "'"
  t = "444444" : if session("setshifts_mode") = "set" then t = "aa0000"
  rr " style='overflow-x:hidden; width:90; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>���� ������</b></a>"

  if ShowTemplatesButton = "on" then
    rr "<img src=button_sep.png style='vertical-align:top;'>"
    rr "<a id=buttonmode2 href=#aa onclick='vbscript"
    rr ":buttonmode0.style.color = ""#444444"""
    rr ":buttonmode1.style.color = ""#444444"""
    rr ":buttonmode2.style.color = ""#aa0000"""
    rr ":buttonsetshifts.style.visibility = ""visible"""
    rr ":tr01.style.visibility = ""hidden""  : tr01.style.position = ""absolute"" : tr01.style.top = -1000"
    rr ":tr02.style.visibility = ""visible""  : tr02.style.position = ""relative"" : tr02.style.top = 0"
    rr ":tr00.style.visibility = ""hidden"" : tr00.style.position = ""absolute"" : tr00.style.top = -1000"
    rr ":fr1.document.location = ""?mode=template"""
    rr "'"
    t = "444444" : if session("setshifts_mode") = "template" then t = "aa0000"
    rr " style='overflow-x:hidden; width:90; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>���� ������</b></a>"
  end if

  rr "<img src=button_~langside2~.png style='vertical-align:top;'>"

  '---scroll weeks
  'rr " <input type=button alt='~Previous Week~' style='font-weight:bold; width:20; height:20; border:1px solid #aaaaaa; background:#F2F2F2; color:444444; ' value='<' onclick='vbscript: document.location = ""?action=groupshifts&d1=" & dateadd("d",-7,d1) & "&d2=" & dateadd("d",-1,d1) & """'>"
  'rr " <input type=button alt='~Next Week~'     style='font-weight:bold; width:20; height:20; border:1px solid #aaaaaa; background:#F2F2F2; color:444444; ' value='>' onclick='vbscript: document.location = ""?action=groupshifts&d1=" & dateadd("d",+7,d1) & "&d2=" & dateadd("d",+13,d1) & """'>"
  'rr " <input type=button                       style='font-weight:bold; width1:20; height:20; border:1px solid #aaaaaa; background:#F2F2F2; color:444444; ' value='~Today~' onclick='vbscript: document.location = ""?action=groupshifts&d1=" & dateadd("d", 1 - weekday(date), date) & "&d2=" & dateadd("d", 7 - weekday(date), date) & """'>"
  't = formatdatetime(d1, 1) : t = replace(t, year(d1), "") : rr "&nbsp;&nbsp;" & t & " - " & formatdatetime(d2, 1)
  if session("setshifts_span") = "day" then
    back1 = dateadd("d",-1,d1)
    back2 = dateadd("d",-1,d1)
    forw1 = dateadd("d",+1,d1)
    forw2 = dateadd("d",+1,d1)
  elseif session("setshifts_span") = "week" then
    back1 = dateadd("d",-7,d1)
    back2 = dateadd("d",-1,d1)
    forw1 = dateadd("d",+7,d1)
    forw2 = dateadd("d",+13,d1)
  elseif session("setshifts_span") = "month" then
    back1 = dateadd("m",-1,d1)
    back2 = dateadd("m",0,d1) : back2 = dateadd("d",-1,back2)
    forw1 = dateadd("m",+1,d1)
    forw2 = dateadd("m",+2,d1) : forw2 = dateadd("d",-1,forw2)
  end if
  rr "&nbsp;&nbsp;<img src=button_~langside~.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&d1=" & back1 & "&d2=" & back2 & """'"
  rr " style='overflow-x:hidden; width:30; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#444444;'>"
  rr "<b><</b></a>"
  rr "<img src=button_sep.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&d1=" & forw1 & "&d2=" & forw2 & """'"
  rr " style='overflow-x:hidden; width:30; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#444444;'>"
  rr "<b>></b></a>"
  'rr "<img src=button_~langside2~.png style='vertical-align:top;'>"
  'rr "&nbsp;&nbsp;<img src=button_~langside~.png style='vertical-align:top;'>"
  'rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&d1=" & dateadd("d", 1 - weekday(date), date) & "&d2=" & dateadd("d", 7 - weekday(date), date) & """'"
  'rr " style='overflow-x:hidden; width:40; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#444444;'>"
  'rr "<b>~Today~</b></a>"
  rr "<img src=button_~langside2~.png style='vertical-align:top;'>"

  rr "&nbsp;&nbsp;<img src=button_~langside~.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&span=day&d1=" & date & "&d2=" & date & """'"
  t = "444444" : if session("setshifts_span") = "day" or session("setshifts_span") = "" then t = "aa0000"
  rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>~Today~</b></a>"
  rr "<img src=button_sep.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&span=week&d1=" & dateadd("d", 1 - weekday(date), date) & "&d2=" & dateadd("d", 7 - weekday(date), date) & """'"
  t = "444444" : if session("setshifts_span") = "week" then t = "aa0000"
  rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>~This Week~</b></a>"
  if specs = "brom" then
    rr "<img src=button_sep.png style='vertical-align:top;'>"
    rr "<a href=#aa onclick='vbscript: document.location = ""?action=groupshifts&span=month&d1=" & "1/" & month(date) & "/" & year(date) & "&d2=" & lastday(month(date) & "/" & year(date)) & "/" & month(date) & "/" & year(date) & """'"
    t = "444444" : if session("setshifts_span") = "month" then t = "aa0000"
    rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
    rr "<b>~This Month~</b></a>"
  end if
  rr "<img src=button_~langside2~.png style='vertical-align:top;'>"

  '---showshift
  rr "&nbsp;&nbsp;<img src=button_~langside~.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?showshift=hours&page=0""'"
  t = "444444" : if session("setshifts_showshift") = "hours" then t = "aa0000"
  rr " style='overflow-x:hidden; width:80; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>��� ����</b></a>"
  rr "<img src=button_sep.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?showshift=shifts&page=0""'"
  t = "444444" : if session("setshifts_showshift") = "shifts" then t = "aa0000"
  rr " style='overflow-x:hidden; width:80; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>��� ������</b></a>"
  rr "<img src=button_~langside2~.png style='vertical-align:top;'>"

  '---orderby
  rr "&nbsp;&nbsp;<img src=button_~langside~.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?orderby=abc&page=0""'"
  t = "444444" : if session("setshifts_orderby") = "abc" then t = "aa0000"
  rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>�'-�'</b></a>"
  rr "<img src=button_sep.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?orderby=unit&page=0""'"
  t = "444444" : if session("setshifts_orderby") = "unit" then t = "aa0000"
  rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>~Unit~</b></a>"
  rr "<img src=button_sep.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?orderby=template&page=0""'"
  t = "444444" : if session("setshifts_orderby") = "template" then t = "aa0000"
  rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>~Template~</b></a>"
  rr "<img src=button_sep.png style='vertical-align:top;'>"
  rr "<a href=#aa onclick='vbscript: document.location = ""?orderby=shifthour&page=0""'"
  t = "444444" : if session("setshifts_orderby") = "shifthour" then t = "aa0000"
  rr " style='overflow-x:hidden; width:60; height:31; background-image:url(button_middle.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#" & t & ";'>"
  rr "<b>���</b></a>"
  rr "<img src=button_~langside2~.png style='vertical-align:top;'>"

  '---
  rr " &nbsp;&nbsp;&nbsp; <a id=buttonsetshifts href=#aa "
  t = "" : if ShowTemplatesButton = "on" then t = " and f1.templateid.value = ""0"" "
  rr " onclick='vbscript: if instr(f1.ways.value,""-"") = 0 " & t & " and ((f1.hour1.value = """" and instr(f1.ways.value,""A"")) or (f1.hour1b.value = """" and instr(f1.ways.value,""B""))) then msgbox ""~Choose Hours~"" else f1.submit'"
  t = "visibility:hidden;" : if session("setshifts_mode") = "set" or session("setshifts_mode") = "template" then t = ""
  rr " style='" & t & " overflow-x:hidden; width:116; height:31; background-image:url(bluebutton.png); text-align:center; padding-top:8; font-weight:bold; font-decoration:none; color:#ffffff;'>"
  rr " ��� ������ </a>"

  if specs = "laufer" then rr "<a target=_new href=_get_ezshift.asp>��� ������ ��������</a>"

  '---controls
  t = "visibility:hidden; position:absolute; top:-1000;" : if session("setshifts_mode") = "show" then t = ""
  rr "<tr id=tr00 style='" & t & "'><td bgcolor1=#dddddd colspan=99 style='padding:15;'><nobr>"

  rr " ~Find~ <input type=text name=string style='width:150;' value='" & session("setshifts_string") & "' onchange='vbscript: f1.string2.value = me.value' onkeypress='vbscript: f1.string2.value = me.value : if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'>"

  rr " ~From~ " & selectdate("*f1.d1",d1)
  rr " ~To~ " & selectdate("*f1.d2",d2)
  'rr "<input type=hidden name=d1 value=" & d1 & ">"
  'rr "<input type=hidden name=d2 value=" & d2 & ">"

  'rr(" ~workers_workertypeid~ <select name=workertypeid dir=~langdir~ style='width:150;'>")
  'rr("<option value=0 style='color:bbbbfe;'>~workers_workertypeid~")
  'sq = "select id,title from workertypes order by title"
  'closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof
  '  rr("<option value=""" & rs1("id") & """")
  '  if cstr(session("setshifts_workertypeid")) = cstr(rs1("id")) then rr(" selected")
  '  rr(">")
  '  rr(rs1("title") & " ")
  '  rs1.movenext
  'loop
  'rr("</select>")
  rr(" ~workers_workertypeid~")
  sq = "select * from workertypes order by title"
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  a = ""
  do until rs1.eof
    a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
    rs1.movenext
  loop
  rr " " & multiselect("workertypeids", "~workers_workertypeid~", a, session("setshifts_workertypeids"), 150)

  rr(" ~workers_unitid~ <select name=unitid dir=~langdir~ style='width:300;'>")
  rr("<option value=0 style='color:bbbbfe;'>~workers_unitid~")
  sq = "select id,title from units"
  if session("usersiteid") <> "0" then sq = sq & " and siteid = " & session("usersiteid")
  if session("usergroup") <> "admin" then sq = sq & " and id in(" & session("userunitids") & "-1)"
  sq = replace(sq," and "," where ",1,1,1)
  sq = sq & " order by title"
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  do until rs1.eof
    rr("<option value=""" & rs1("id") & """")
    if cstr(session("setshifts_unitid")) = cstr(rs1("id")) then rr(" selected")
    rr(">")
    rr(rs1("title") & " ")
    rs1.movenext
  loop
  rr("</select>")

  rr "<BR><BR>"

  if getparam("UseWorkerTemplates") = "y" then
    rr(" ~workers_template~ <select style='width:150;' name=template dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workers_template~")
    sq = "select firstname from workers where lastname = '_TEMPLATE_'"
    if session("usersiteid") <> "0" then sq = sq & " and (siteid = " & session("usersiteid") & ")"
    sq = sq & " order by firstname"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("firstname") & """")
      if lcase(cstr(session(tablename & "_template"))) = lcase(cstr(rs1("firstname"))) then rr(" selected")
      rr ">" & rs1("firstname")
      rs1.movenext
    loop
    rr("</select>")
  end if

  if session("usersiteid") = "0" then 
    rr(" ~workers_siteid~ <select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~workers_siteid~")
    sq = "select id,title from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session("setshifts_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")
  end if

  rr " ~By Shift~ <select name=byshift>"
  rr "<option value=''>"
  rr "<option value=selected" : if session("setshifts_byshift") = "selected" then rr " selected>~Only the selected shift~" else rr ">~Only the selected shift~"
  rr "<option value=all" : if session("setshifts_byshift") = "all" then rr " selected>~Already set any shift~" else rr ">~Already set any shift~"
  rr "<option value=none" : if session("setshifts_byshift") = "none" then rr " selected>~Not set yet~" else rr ">~Not set yet~"
  rr "<option value=draft" : if session("setshifts_byshift") = "draft" then rr " selected>~Draft~" else rr ">~Draft~"
  rr "</select>"

  if specs = "techjet" then
    rr(" ~Course~ ")
    rr " <img src=icons/i_help.gif style='cursor:hand; filter:alpha(opacity=50); margin-top:8;' alt='~orders_course_search_help~'> "
    rr "<input type=text name=coursefilter style='width:80;' value=""" & session(tablename & "_coursefilter") & """>"
  end if

  rr " <input type=text name=lines value='" & session("setshifts_lines") & "' style='width:30;'> ~Lines~"
  rr " <input type=button value='~Clear Search~' onclick='vbscript: f1.action.value = ""clear"" : f1.submit'>"
  rr " <input type=button value='     ~Show~     ' onclick='vbscript: f1.action.value = ""search"" : f1.submit'>"

  t = "visibility:hidden; position:absolute; top:-1000;" : if session("setshifts_mode") = "set" then t = ""
  rr "<tr id=tr01 style='" & t & "'><td colspan=99 style='padding:15;'><nobr>"

  '-----------------set shifts--------------
  rr " ~Find~ <input type=text name=string2 style='width:150;' value='" & session("setshifts_string") & "' onchange='vbscript: f1.string.value = me.value' onkeypress='vbscript:f1.string.value = me.value : if window.event.keycode = 13 then f1.action.value = ""search"" : f1.submit'>"

  if session("usergroup") <> "admin" and session("managershiftids") = "" then
    s = "select id, unitids from shifts"
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      uu = rs1("unitids") : if isnull(uu) then uu = ""
      if uu = "" or hasmatch(session("userunitids"), "" & uu) then session("managershiftids") = session("managershiftids") & rs1("id") & ","
      rs1.movenext
    loop
  end if

  rr " ~Shift~ <select name=shiftid dir=~langdir~ onchange='vbscript:sethours'>"
  rr "<option value=0 style='color:bbbbfe;'>~Shift~"
  sq = "select id,title,hour1,hour1b,ways,days from shifts"
  if session("usersiteid") <> "0" then sq = sq & " and siteid = " & session("usersiteid")
  if session("managershiftids") <> "" then sq = sq & " and id in(" & session("managershiftids") & "-1)"
  if specs = "iai" then sq = sq & " and id in(" & getidlist("select id from shifts where courseids <> ''") & "-1)"
  if session("setshifts_span") = "day" then wd = getweekday(session("setshifts_d1")) : sq = sq & " and instr(',' & days, '," & wd & ",') > 0"
  sq = sq & " order by title,hour1,hour1b"
  sq = replace(sq," and "," where ",1,1,1)
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  do until rs1.eof
    rr "<option value=""" & rs1("id") & """"
    if session("setshifts_shiftid") = cstr(rs1("id")) then rr " selected"
    rr ">"
    rr rs1("title") & " "
    if instr(",brom,iai,", "," & specs & ",") = 0 then
      rr rs1("hour1") & "-" & rs1("hour1b")
      if specs <> "hpaper" then
        t = rs1("days") : for i = 1 to 20 : t = replace(t, i & ",", "~weekday" & i & "~,") : next
        if right(t,1) = "," then t = left(t,len(t)-1)
        rr " (" & t & ")"
      end if
    end if
    sethourscode = sethourscode & " if x = """ & rs1("id") & """ then" & vbcrlf
    sethourscode = sethourscode & "   f1.hour1.value = """ & rs1("hour1") & """ : f1.hour1b.value = """ & rs1("hour1b") & """" & vbcrlf
    sethourscode = sethourscode & "   f1.ways.value = """ & rs1("ways") & """" & vbcrlf
    sethourscode = sethourscode & " end if" & vbcrlf
    rs1.movenext
  loop

  sethours2code = sethours2code & " if instr(f1.ways.value,""A"") > 0 then spancoll.style.visibility = ""visible"" : else spancoll.style.visibility = ""hidden""" & vbcrlf
  sethours2code = sethours2code & " if instr(f1.ways.value,""B"") > 0 then spandist.style.visibility = ""visible"" : else spandist.style.visibility = ""hidden""" & vbcrlf

  rr "</select>"

  if getparam("MultiDailyShifts") = "y" then rr " <input type=checkbox name=addition>~As Additional Shifts~"
  '---------ways-------------------------------------------
  a = "AB,A,B,X,-,-A,-B,"
  if specs = "shufersal" and session("usergroup") <> "admin" then a = "AB,A,B,X,"
  rr(" <select name=ways dir=~langdir~ onchange='vbscript: sethours2'>")
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if specs <> "brom" and session(tablename & "_ways") = b then rr(" selected")
    if b = "-" then b = "~Delete Shifts~"
    if b = "-A" then b = "��� �����"
    if b = "-B" then b = "��� �����"
    if b = "X" then b = "~Mark without ride~"
    rr(">" & b)
  loop
  if specs = "techjet" then
    rr "<option style='color:#0000aa;' value='X:�������'>�������"
    rr "<option style='color:#0000aa;' value='X:����'>����"
    rr "<option style='color:#0000aa;' value='X:����'>����"
    rr "<option style='color:#0000aa;' value='X:���� ������'>���� ������"
  end if
  rr("</select>")'form:ways

  a = lcase(getparam("SetShiftsCollectDistributeHoursList"))
  if a = "" or lcase(getparam("SetShiftsLockHours")) = "y" then
    t = "" : if lcase(getparam("SetShiftsLockHours")) = "y" then t = " onfocus='vbscript:me.blur' style='background:eeeeee; border:1px solid #cccccc;'"
    rr(" <span id=spancoll>~Arrival~ <input type=text maxlength=5 name=hour1 onblur='vbscript:checkhour1' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:50;' value=""" & session("setshifts_hour1") & """ dir=ltr" & t & "></span>")
    rr(" <span id=spandist>~Distribute~ <input type=text maxlength=5 name=hour1b onblur='vbscript:checkhour1b' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:50;' value=""" & session("setshifts_hour1b") & """ dir=ltr" & t & "></span>")
  else
    '--------------select collect/distribute hours separately-----------------
    e = "~Invalid settings parameter SetShiftsCollectDistributeHoursList, ask the administrator to type a valid value~"
    if instr(a,"interval") > 0 then
      a = trim(replace(a, "interval", "")) : if not isnumeric("-" & a) then x = errorend(e)
      if cdbl(a) < 1 or cdbl(a) > (24 * 60) then x = errorend(e)
      interval = a : a = "" : d = "20/1/2000 00:00:00"
      do until cdate(d) >= cdate("21/1/2000")
        h = mid(d, instr(d," ")+1)
        h = left(h,instrrev(h,":")-1)
        h = replace(h,":","")
        a = a & h & ","
        d = dateadd("n", interval, d) : if instr(d,":") = 0 then d = d & " 00:00:00"
      loop
      a = a & "/" & a
    end if
    if countstring(a,"/") <> 1 then x = errorend(e)
    a = replace(a," ","")
    'if specs = "rail" then a = "0000,0015,0030,0045,0100,0115,0130,0145,0200,0215,0230,0245,0300,0315,0330,0345,0400,0415,0430,0445,0500,0515,0530,0545,0600,2000,2015,2030,2045,2100,2115,2130,2145,2200,2215,2230,2245,2300,2315,2330,2345," : a = a & "/" & a
    cc = left(a,instr(a,"/")-1) & "," : dd = mid(a,instr(a,"/")+1) & ","
    for j = 1 to 2
      if j = 1 then rr " <span id=spancoll>~Arrival~ <select name=hour1><option value='' style='color:bbbbfe;'>~Arrival~" : a = cc
      if j = 2 then rr " <span id=spandist>~Distribute~ <select name=hour1b><option value='' style='color:bbbbfe;'>~Distribute~" : a = dd
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        if b <> "" then
          if b <> left(strfilter(b,"0123456789"),4) then x = errorend(e)
          b = left(b,2) & ":" & right(b,2)
          rr "<option value='" & b & "'>" & b
        end if
      loop
      rr "</select></span>"
    next
  end if
  if instr(",iai,elbit,", "," & specs & ",") > 0 and session("usergroup") = "admin" and getfield("action") = "" and getfield("page") = "" then rr "<br><br><br><br><center><b>��� ��� �������� ����� ���� �� `���`</b>" : rend
  if getparam("ShiftFood") = "y" then
    rr " <select name=foodsupplierid dir=~langdir~ onchange='vbscript:sethours'>"
    rr "<option value=0 style='color:bbbbfe;'>~Without Food~"
    sq = "select id,title from foodsuppliers"
    'if session("usersiteid") <> "0" then sq = sq & " where siteid = " & session("usersiteid")
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr "<option value=""" & rs1("id") & """"
      'if getfield("foodsupplierid") = cstr(rs1("id")) then rr " selected"
      rr ">"
      rr rs1("title") & " (~Code~ ~FoodAbbreviation~" & rs1("id") & ")"
      rs1.movenext
    loop
    rr "</select>"
  else
    rr "<input type=hidden name=foodsupplierid value=0>"
  end if

  rr "<BR><img src=blank.gif width=1 height=30>"
  if getparam("ShiftAlternativeCourses") = "y" then
    rr(" ~Course~ ")
    rr("<select name=courseid style='width:150;' dir=~langdir~")
    if getparam("ShiftAlternativeStations") = "y" then rr " onchange='vbscript: frpopstations.document.location = ""?act=popstations&courseid="" & f1.courseid.value & ""&from1="" & f1.from1.value & ""&to1="" & f1.to1.value'"
    rr("></select>")'form:from1
  else
    rr "<input type=hidden name=courseid value=0>"
  end if

  if getparam("ShiftAlternativeStations") = "y" then
    rr(" ~Worker`s station~ ")
    if getfield("box") = "from1" then thefrom1 = getfield("boxid")
    rr "<input type=hidden name=from1populated>"
    rr "<select name=from1 style='width:150;' dir=~langdir~ onfocus='vbscript: if f1.from1populated.value = """" then f1.from1populated.value = ""1"" : frpopstations.document.location = ""?act=popstations&shiftid="" & f1.shiftid.value & ""&courseid="" & f1.courseid.value & ""&from1="" & f1.from1.value & ""&to1="" & f1.to1.value'>"
    rr("<option value=0 style='color:bbbbfe;'>~Alternative From~")
    rr("</select>")'form:from1
    if specs = "laufer" then rr " ����� ������ <input type=text name=altaddress>"

    if specs = "iai" then
      rr "<input type=hidden name=to1 value=" & session("defaultstation") & ">"
    else
      rr(" ~Work location~ ")
      if getfield("box") = "to1" then theto1 = getfield("boxid")
      rr "<select name=to1 style='width:150;' dir=~langdir~ onfocus='vbscript: if f1.from1populated.value = """" then f1.from1populated.value = ""1"" : frpopstations.document.location = ""?act=popstations&shiftid="" & f1.shiftid.value & ""&courseid="" & f1.courseid.value & ""&from1="" & f1.from1.value & ""&to1="" & f1.to1.value'>"
      rr("<option value=0 style='color:bbbbfe;'>~Alternative To~")
      rr("</select>")'form:to1
    end if
    'rr "<input type=text name=splittingfrom1>"

  else
    rr "<input type=hidden name=from1 value=0>"
    rr "<input type=hidden name=to1 value=0>"
  end if

  if specs = "brom" then rr " ����� ����� <input type=text name=remarks>"

  if d1 > d2 then x = errorend("_back~End date is smaller than start date~")

  if getfield("csv") = "on" then
    server.scripttimeout = 100000 'seconds
    set csv = Server.CreateObject("ADODB.Stream") : csv.open
    csv.writetext(translate("~Worker~,~Code~,~Unit~,"))
    if specs = "techjet" then csv.writetext(translate("~workers_workertypeid~,"))
    if instr(",techjet,brom,packer,", "," & specs & ",") > 0 then csv.writetext(translate("~workers_eligible~,"))
    if specs = "brom" then csv.writetext(translate("~workers_citystationid~,"))
  end if

  '-----------templates
  if getparam("UseWorkerTemplates") = "y" then
    t = "visibility:hidden; position:absolute; top:-1000;" : if session("setshifts_mode") = "template" then t = ""
    rr "<tr id=tr02 style='" & t & "'><td colspan=99 style='padding:15;'><nobr>"
    rr " ~Template~ "
    rr "<select name=templateid style='width:300;'>"
    rr "<option value=0>"
    s = "select id,firstname from workers where lastname = '_TEMPLATE_'"
    if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
    s = s & " order by firstname"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      rr "<option value=" & rs("id")
      if getfield("templateid") = cstr(rs("id")) then rr " selected"
      rr ">" & rs("firstname")
      rs.movenext
    loop
    rr "</select>"
  end if


  '-----------scripts-------------
  rr "<script language=vbscript>" & vbcrlf
  rr "function sethours" & vbcrlf
  rr "  f1.hour1.value = """" : f1.hour1b.value = """"" & vbcrlf
  rr "  x = f1.shiftid.value" & vbcrlf
  rr sethourscode
  rr "  x = sethours2" & vbcrlf
  if getparam("ShiftAlternativeCourses") = "y" then rr " frpopcourses.document.location = ""?act=popcourses&shiftid=""  & f1.shiftid.value & ""&courseid="" & f1.courseid.value" & vbcrlf
  'if getparam("ShiftAlternativeStations") = "y" then rr " frpopstations.document.location = ""?act=popstations&shiftid="" & f1.shiftid.value & ""&courseid="" & f1.courseid.value" & vbcrlf
  rr "end function" & vbcrlf
  rr "function sethours2" & vbcrlf
  rr sethours2code
  rr "end function" & vbcrlf
  rr "function checkhour1" & vbcrlf
  rr "    if f1.hour1.value = """" then exit function" & vbcrlf
  rr("    if len(f1.hour1.value) = 4 then f1.hour1.value = ""0"" & f1.hour1.value" & vbcrlf)
  rr("    if len(f1.hour1.value) = 5 and mid(f1.hour1.value,3,1) = "":"" and isnumeric(left(f1.hour1.value,2)) and isnumeric(right(f1.hour1.value,2)) and instr(f1.hour1.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) <= 59 then ok = 1 else msgbox ""~Invalid Hour~"" : f1.hour1.value = """"" & vbcrlf)
  rr "end function" & vbcrlf
  rr "function checkhour1b" & vbcrlf
  rr "    if f1.hour1b.value = """" then exit function" & vbcrlf
  rr("    if len(f1.hour1b.value) = 4 then f1.hour1b.value = ""0"" & f1.hour1b.value" & vbcrlf)
  rr("    if len(f1.hour1b.value) = 5 and mid(f1.hour1b.value,3,1) = "":"" and isnumeric(left(f1.hour1b.value,2)) and isnumeric(right(f1.hour1b.value,2)) and instr(f1.hour1b.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) <= 59 then ok = 1 else msgbox ""~Invalid Hour~"" : f1.hour1b.value = """"" & vbcrlf)
  rr "end function" & vbcrlf
  if sethourscode <> "" then
    rr "sethours" & vbcrlf
    if specs = "flextronics" then rr " f1.ways.value = ""B""" & vbcrlf
  end if
  rr "</script>"

  if session("scrollwithin") = "" then session("scrollwithin") = "on"
  if getfield("scrollwithin") <> "" then session("scrollwithin") = getfield("scrollwithin")
  if session("scrollwithin") = "on" then
    rr "<tr><td colspan=99 style='padding:0;'><div id=columntitles style='white-space:nowrap; background:#dddddd;'></div>"
    rr "<tr><td colspan=99 style='padding:0;'><div id=scrollwithin style='height:200; overflow-y:scroll; direction:~langdir2~;'><table cellspacing=1 cellpadding=4 dir=~langdir~>"
  end if

  '--------toprow----------------------------
  t = "<tr bgcolor=#dddddd id=trcoltitles><td id=cell_0_0>~Worker~"
  rr t : coltitles = t : cellx = 0
  d = d1
  do until d > d2
    cellx = cellx + 1
    t = "dddddd" : wd = getweekday(d)
    if wd = 6 then t = "cccc99"
    if wd = 7 then t = "cccc66"
    if wd = 8 then t = "cccc66"
    if wd = 9 then t = "cccc99"
    if wd > 9 then t = "cccc66"
    t = "<td style='background:#" & t & ";' id=cell_0_" & cellx & "><nobr>"
    rr t : coltitles = coltitles & t
    s = "select id from workershifts where date1 = " & sqldate(d) & " and workerid = -" & session("userid")
    set rs1 = conn.execute(sqlfilter(s))
    rr "<table align=~langside2~><TR><TD>"
    if rs1.eof then
      rr "<a href=#aa onclick='vbscript: if msgbox(""" & d & " - ~Mark day as done~?"",vbyesno) = 6 then document.location = ""?action=setdone&token=" & session("token") & "&d=" & d & """'><img src=vbutton0.png border=0 width=16 height=16 alt='~Mark day as done~'></a>"
    else
      rr "<img src=vbutton1.png width=16 height=16 alt='~Day Marked as Done~'>"
    end if
    rr "</table>"
    rr "<input type=checkbox onclick='vbscript: x = checkalldate(""" & replace(d,"/","_") & """, me.checked)'>"

    t = "~weekday" & weekday(d) & "~ " & day(d) & "/" & month(d) & "/" & year(d)
    rr t : coltitles = coltitles & t
    if getfield("csv") = "on" then csv.writetext(d & " " & translate("~weekday" & weekday(d) & "~ ") & ",")

    t = "<br><img src=blank.gif width=50 height=1>"
    rr t : coltitles = coltitles & t

    if specs = "techjet" then
      sq = "select type1, title from holidays where date1 = " & sqldate(d)
      set rs1 = conn.execute(sqlfilter(sq))
      if not rs1.eof then
        rr "<br><img src=blank.gif width=1 height=5><br><div style='position:absolute;'>" & translate("~weekday" & rs1("type1") & "~") & " - " & rs1("title") & "</div><br>"
      end if
    end if

    d = dateadd("d",1,d)
  loop

  '----workers--------------------------------
  if getfield("csv") = "on" then csv.writetext(vbcrlf)
  if session("setshifts_byshift") <> "" then
    ww = "" : ww1 = "" : ww2 = ""
    s = "select distinct workerid from weekplan"
    if session("setshifts_byshift") = "selected" then s = s & " where (hour1 = '" & session("setshifts_hour1") & "' or hour1b = '" & session("setshifts_hour1b") & "')"
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      ww1 = ww1 & rs1("workerid") & ","
      rs1.movenext
    loop
    s = "select distinct workerid from workershifts where 0=0"
    if session("setshifts_byshift") = "selected" then
      if instr(",sodastream,brom,", "," & specs & ",") > 0 then
        ww1 = ""
        s = s & " and workershifts.shiftid = 0" & session("setshifts_shiftid")
      else
        s = s & " and (hour1 = '" & session("setshifts_hour1") & "' or hour1b = '" & session("setshifts_hour1b") & "')"
      end if
    end if
    if session("setshifts_byshift") = "draft" then s = s & " and instr(cstr(data1), 'draft=on') > 0"
    s = s & " and date1 >= " & sqldate(d1)
    s = s & " and date1 <= " & sqldate(d2)
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      ww2 = ww2 & rs1("workerid") & ","
      rs1.movenext
    loop
    ww = ww1 & ww2
    if session("setshifts_byshift") = "draft" then ww = ww2
    ww = formatidlistunique(ww)
  end if

  w = session(tablename & "_coursefilter") : ww3 = ""
  if w <> "" then
    s = "select distinct workerid from workershifts where 0=0"
    s = s & " and date1 >= " & sqldate(d1)
    s = s & " and date1 <= " & sqldate(d2)
    if lcase(w) = "a" then
      s = s & " and workershifts.courseid > 0"
    elseif lcase(w) = "n" then
      s = s & " and workershifts.courseid = 0"
    elseif instr(w, "!") > 0 then
      w = replace(w, "!", "")
      ii = getidlist("select top 50 id from courses where code = '" & w & "'")
      s = s & " and workershifts.courseid in(" & ii & "-1)"
    elseif w <> "" then
      ii = getidlist("select top 50 id from courses where instr(code & ' ' & title, '" & w & "') > 0")
      s = s & " and workershifts.courseid in(" & ii & "-1)"
    end if
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      ww3 = ww3 & rs1("workerid") & ","
      rs1.movenext
    loop
  end if

  s = "select workers.id,workers.code,workers.firstname,workers.lastname,workers.unitid,workers.template,workers.siteid,workers.courseid1,workers.workertypeid,workers.data1,workers.eligible,workers.citystationid,"
  if session("setshifts_orderby") = "shifthour" then s = s & "workershifts.hour1 as shifthour,"
  s = s & "units.title as unittitle"
  s = s & " from (workers left join units on workers.unitid = units.id)"
  if session("setshifts_orderby") = "shifthour" then s = s & " right join workershifts on (workershifts.workerid = workers.id and workershifts.date1 = " & sqldate(session("setshifts_d1")) & ")"
  s = s & " where workers.firstname <> ''"
  if instr(",techjet,brom,packer,", "," & specs & ",") > 0 then
    s = s & " and workers.active = 'on' and workers.lastname <> '_TEMPLATE_'"
  else
    s = s & " and workers.eligible = 'on'"
  end if
  s = s & okreadsql("workers")
  if instr(",selected,all,draft,", "," & session("setshifts_byshift") & ",") > 0 then s = s & " and workers.id in(" & ww & "-1)"
  if instr(",none,", "," & session("setshifts_byshift") & ",") > 0 then s = s & " and workers.id not in(" & ww & "-1)"
  if session(tablename & "_coursefilter") <> "" then s = s & " and workers.id in(" & ww3 & "-1)"
  if session("setshifts_string") <> "" then
    a = session("setshifts_string") & " "
    do until a = ""
      b = left(a,instr(a," ")-1) : a = mid(a,instr(a," ")+1)
      if b <> "" then s = s & " and (workers.firstname like '%" & b & "%' or workers.lastname like '%" & b & "%' or workers.code like '%" & b & "%' or workers.address like '%" & b & "%')"
    loop
  end if

  if session("setshifts_workertypeid") <> "" and session("setshifts_workertypeid") <> "0" then s = s & " and workers.workertypeid = " & session("setshifts_workertypeid")
  if session("setshifts_workertypeids") <> "" then s = s & " and workers.workertypeid in(" & session("setshifts_workertypeids") & "-1)"
  if session("setshifts_unitid") <> "" and session("setshifts_unitid") <> "0" then s = s & " and workers.unitid = " & session("setshifts_unitid")
  if session("setshifts_template") <> "" then s = s & " and lcase(workers.template) = '" & lcase(session("setshifts_template")) & "'"
  if session("setshifts_siteid") <> "" and session("setshifts_siteid") <> "0" then s = s & " and workers.siteid = " & session("setshifts_siteid")

  if session("setshifts_orderby") = "unit" then
    s = s & " order by units.title, workers.firstname, workers.lastname"
  elseif session("setshifts_orderby") = "template" then
    s = s & " order by workers.template, workers.firstname, workers.lastname"
  elseif session("setshifts_orderby") = "shifthour" then
    s = s & " order by workershifts.hour1, workers.firstname, workers.lastname"
  else
    s = s & " order by workers.firstname, workers.lastname"
  end if
  sw = s

  '---alreadyset
  s = "select id,workerid,date1,ways,hour1,hour1b,foodsupplierid,from1,to1,courseid,data1,shiftid from workershifts"
  s = s & " where date1 >= " & sqldate(d1) & " and date1 <= " & sqldate(d2)
  if getfield("csv") = "on" then
    s = s & " and instr(cstr(data1), 'draft=on') = 0"
  elseif session("setshifts_orderby") = "shifthour" or session("setshifts_orderby") = "unit" then 'for a little better performance...
    t = "select top " & session(tablename & "_lines") * (session(tablename & "_page") + 1) & mid(sw,7)
    set rs1 = conn.execute(sqlfilter(t))
    a = ""
    do until rs1.eof
      a = a & rs1("id") & ","
      rs1.movenext
    loop
    s = s & " and workerid in(" & a & "0)"
  else 'for even better performance...
    t = "select top " & session(tablename & "_lines") * (session(tablename & "_page") + 1)
    t = t & " workers.id from workers " & mid(sw, instr(sw,"where"))
    s = s & " and workerid in(" & getidlist(t) & "0)"
  end if

  s = s & " order by workerid, date1, ways, hour1, hour1b"
  set rs1 = conn.execute(sqlfilter(s))
  lastk = "" : v = ""
  set kk = Server.CreateObject("ADODB.Stream") : kk.open
  do until rs1.eof
    k = "u" & rs1("workerid") & "d" & replace(cdate(rs1("date1")),"/","_")
    if k = lastk then v = v & "; " else kk.writetext(lastk & "=" & v & "|") : v = ""
    ways = rs1("ways")
    data1 = rs1("data1")
    if instr("" & ways,"A") > 0 then v = v & translate(" ~Arrival~ ") & rs1("hour1")
    if instr("" & ways, "B") > 0 then v = v & translate(" ~Distribute~ ") & rs1("hour1b") : if rs1("hour1") > rs1("hour1b") then v = v & " �����"
    if instr("" & ways, "X") > 0 then
      v = v & translate(" ~No Ride~ ")
      if specs = "tnuva" then v = v & " - " & gettitle("shifts", rs1("shiftid")) & " "
      if instr(",brom,packer,", "," & specs & ",") > 0 then v = v & " - " & rs1("hour1") & "-" & rs1("hour1b") & " "
      if instr(",techjet,", "," & specs & ",") > 0 then
        if instr(",,��� ����,���� ������,", "," & getval(data1, "xtype") & ",") > 0 then v = v & " - " & rs1("hour1") & "-" & rs1("hour1b") & " "
      end if
    end if
    foodsupplierid = rs1("foodsupplierid") : if isnull(foodsupplierid) then foodsupplierid = 0
    if getfield("csv") <> "on" and ways <> "X" then
      if ways = "B" then
        if cstr("" & rs1("to1")) <> "0" then v = v & translate(", ~From~") & gettitle("stations",rs1("to1"))
        if cstr("" & rs1("from1")) <> "0" then v = v & translate(", ~To ~") & gettitle("stations",rs1("from1"))
      else
        if cstr("" & rs1("from1")) <> "0" then v = v & translate(", ~From~") & gettitle("stations",rs1("from1"))
        if cstr("" & rs1("to1")) <> "0" then v = v & translate(", ~To ~") & gettitle("stations",rs1("to1"))
      end if
      if cstr("" & rs1("courseid")) <> "0" then v = v & translate(", ~Course~ ") & getcode("courses",rs1("courseid"))
    end if
    if specs = "iai" and getfield("csv") = "on" then
      t = rs1("courseid") : if isnull(t) then t = ""
      if cstr(t) = "0" then t = getidlist("select courseid1 from workers where id = " & rs1("workerid")) : t = replace(t,",","") : if t = "" then t = "0"
      if cstr(t) <> "0" then v = v & translate(" ~Course~ ") & getcode("courses",t)
    end if
    if cdbl(foodsupplierid) > 0 then v = v & " ~FoodAbbreviation~" & foodsupplierid
    t = ""
    t = t & "|id=" & rs1("id")
    t = t & "|shiftid=" & rs1("shiftid")
    t = t & "|hour1=" & rs1("hour1")
    t = t & "|hour1b=" & rs1("hour1b")
    t = t & "|" & ctxt("" & data1) : t = replace(t,"|","\")
    v = v & "�" & t & "�"
    rs1.movenext
    lastk = k : if rs1.eof then kk.writetext k & "=" & v & "|"
  loop
  kk.position = 0 : alreadyset = kk.readtext(kk.size) : kk.close

  'if rs.state = 1 then rs.close
  'rs.open sqlfilter(sw), conn, 3, 1, 1 : rsrc = rs.recordcount : rc = 0
  s = sw
  s = "select count(workers.id) as x " & mid(s,instr(s, " from ")) : if instr(s, "order by ") > 0 then s = left(s, instr(s, "order by ") - 1)
  set rs = conn.execute(sqlfilter(s))
  rc = 0 : rsrc = rs("x") : if isnull(rsrc) then rsrc = 0
  if getfield("csv") = "on" then
    s = sw
  else
    s = "select top " & cdbl(session(tablename & "_lines")) * (session(tablename & "_page") + 1) & " " & mid(sw,7)
  end if
  set rs = conn.execute(sqlfilter(s))
  ids = "" : c = 0 : cc = 0 : checkboxusers = "" : checkboxdates = "" : lastgroup = "" : celly = 0

  do until rs.eof
    rc = rc + 1 : showthis = false : if rc > (session("setshifts_page") * session(tablename & "_lines")) and rc <= (session("setshifts_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    data1 = rs("data1")
    if getfield("csv") = "on" then showthis = true
    if showthis then
      celly = celly + 1 : cellx = 0
      cc = cc + 1 : if cc > 10 and session("scrollwithin") <> "on" then rr coltitles : cc = 1
      i = rs("id") : ids = ids & i & ","
      display = rs("firstname") & " " & rs("lastname") & " " & rs("code")
      if session("setshifts_orderby") = "unit" then t = iif("" & rs("unittitle") = "", "��� ���� ������", rs("unittitle"))  : if lastgroup <> t then rr "<tr><td colspan=99 style='border-bottom:1px solid #aa0000; color:#aa0000; padding-~langside~:23;'><b>" & t : lastgroup = t : if getfield("action2") = "doc" then csv.writetext(t & vbcrlf)
      if session("setshifts_orderby") = "template" then t = iif("" & rs("template") = "", "��� �����", rs("template")) : if lastgroup <> t then rr "<tr><td colspan=99 style='border-bottom:1px solid #aa0000; color:#aa0000; padding-~langside~:23;'><b>" & t : lastgroup = t : if getfield("action2") = "doc" then csv.writetext(t & vbcrlf)

      if getfield("csv") = "on" then
        csv.writetext(replace(ctxt(rs("firstname")) & " " & ctxt(rs("lastname")), ",", ";") & ",")
        csv.writetext(replace(ctxt(rs("code")), ",", ";") & ",")
        csv.writetext(replace(ctxt(rs("unittitle")), ",", ";") & ",")
        if specs = "techjet" then csv.writetext(gettitle("workertypes", rs("workertypeid"))) & ","
        if instr(",techjet,brom,packer,", "," & specs & ",") > 0 then if rs("eligible") = "on" then csv.writetext(" (����),") else csv.writetext(" (�� ����),")
        if specs = "brom" then csv.writetext(tocsv(gettitle("stations", rs("citystationid"))) & ",")
      end if

      'rr "<tr valign=top style='background:#ffffff;' id=tr" & i
      'if i > 0 then rr " onmouseover='vbscript: tr" & i & ".style.background=""ffffbb""' onmouseout='vbscript: tr" & i & ".style.background=""ffffff""'"
      'rr ">"
      t = "ffffdd" : if rc / 6 = fix(rc/6) or (rc+1) / 6 = fix((rc+1)/6) or (rc+2) / 6 = fix((rc+2)/6) then t = "ffffff"
      rr "<tr valign=top style='background:#" & t & ";' id=tr" & i & ">"

      t = "" : if celly = 1 then t = " id=cell_1_0"
      rr "<td" & t & " width=1% ><nobr><input type=checkbox onclick='vbscript: x = checkalluser(""" & i & """, me.checked)'>"
      if instr(",flextronics,plasan,", "," & specs & ",") > 0 then
        if selectfoodbox = "" then
          selectfoodbox = "<select name=workerfood#WORKERID#><option value='' style='color:#cccccc;'>~workershifts_foodsupplierid~"
          sq = "select id,title from foodsuppliers"
          set rs1 = conn.execute(sqlfilter(sq))
          do until rs1.eof
            selectfoodbox = selectfoodbox & "<option value=" & rs1("id") & ">" & rs1("title")
            rs1.movenext
          loop
          selectfoodbox = selectfoodbox & "</select>"
        end if
        a = selectfoodbox : a = replace(a, "#WORKERID#", rs("id"))
        't = getval(data1, "lastfoodsupplierid") : if t <> "" then a = replace(a, "<option value=" & t & ">", "<option value=" & t & " selected>")
        rr a & " "
      end if

      rr "<a href=admin_workers.asp?workersview=-&action=form&id=" & i & "&backto=setshifts.asp?action=list>" & display & "</a>"
      if specs = "techjet" then
        rr " - " & rs("unittitle") & " - " & rs("template")
        rr " - " & gettitle("workertypes", rs("workertypeid"))
        if rs("eligible") = "on" then rr " (����)" else rr " (�� ����)"
      end if
      if instr(",brom,packer,", "," & specs & ",") > 0 then if rs("eligible") <> "on" then rr " (�� ����)"
      d = d1
      checkboxusers = checkboxusers & i & ","

      '----rows
      do until d > d2
        cellx = cellx + 1
        if cc = 1 then checkboxdates = checkboxdates & replace(d,"/","_") & ","
        k = "u" & i & "d" & replace(d,"/","_")
        t = "" : if celly = 1 then t = " id=cell_1_" & cellx
        rr "<td" & t & "><img src=blank.gif width=120 height=1><br>"

        e = 1
        if session("setshifts_shiftid") <> "" and date > d then e = 0
        h = getval(sitelock, rs("siteid")) : if not ishour(h) then h = "17:00"
        if weekday(d) >= 6 then h = getval(sitelock, rs("siteid") & "weekend") : if not ishour(h) then h = "17:00"
        if cdate(d) < date or (cdate(d) = date and cdate("1/1/2000 " & time) >= cdate("1/1/2000 " & h)) then e = 0
        if specs = "teva" and cstr(rs("siteid")) = "6" and (cdate(d) <= date or (cdate(d) = dateadd("d",1,date) and cdate("1/1/2000 " & time) >= cdate("1/1/2000 " & h))) then e = 0 'teva shoham liel guetta request
        if getparam("LockSetShiftsOnWeekends") = "y" and dateadd("d",4,date) >= cdate(d) then
          if weekday(date) >= 6 and weekday(d) >= 6 then e = 0
          if weekday(date) = 5 and weekday(d) >= 6 and cdate("1/1/2000 " & time) >= cdate("1/1/2000 " & h) then e = 0
        end if
        if specs = "flextronics" then
          'e = 0 : if d = date then e = 1
          if d > date then
            e = 0 : t = date
            do
              t = dateadd("d", 1, t)
              if getweekday(t) < 6 then exit do
            loop
            if getweekday(d) >= 6 and d < t then e = 1
          end if
        end if
        j = getval(alreadyset,k)

        shiftdata1 = ""
        if instr(j, "�") > 0 then
          shiftdata1 = getfromto(j, "", "�", "�", false, false) : shiftdata1 = replace(shiftdata1, "\", "|")
          do until instr(j, "�") = 0 : j = replacefromto(j, "", "�", "�", "") : loop
        end if
        if specs = "techjet" and j = "" then e = 0
        g = 0 : if j = "" then j = getval(weekplans, "u" & i & "d" & d) : if j <> "" then g = 1
        j = ctxt(j)
        rr "<nobr><input type=checkbox name=" & k : if e = 0 then rr " disabled"
        if specs = "iai" then
          if e = 1 and (instr(j, ", �") > 0 and instr(j, ", �") > 0 and instr(j, ", �����") > 0) or g = 1 then
             rr " style='padding:4; background:#bbffbb;'"
          elseif e = 1 and j <> "" then
             rr " style='padding:4; background:#D63F2E;'"
          end if
        else
          if e = 1 and j <> "" then rr " style='padding:4; background:bbffbb;'"
        end if
        rr ">"


        if specs = "techjet" and getval(shiftdata1, "manually") = "on" then
          rr " <img src=flag1.png width=11 height=11 style='cursor:hand;' alt='~orders_manually~'>"
        end if

        if getval(shiftdata1, "draft") = "on" then
          wsid = getval(shiftdata1, "id")
          rr " <span id=draftactions" & wsid & " style='background:#eeeeee; border:1px solid #808080; padding:2; color:#808080;'>"
          rr "   <a target=fr1 style='color:#808080;' onclick='vbscript: document.getelementbyid(""draftactions" & wsid & """).style.visibility = ""hidden""' href=?action=draftreal&id=" & wsid & "&ways=AB>���</a>"
          rr " | <a target=fr1 style='color:#808080;' onclick='vbscript: document.getelementbyid(""draftactions" & wsid & """).style.visibility = ""hidden""' href=?action=draftreal&id=" & wsid & "&ways=A>�����</a>"
          rr " | <a target=fr1 style='color:#808080;' onclick='vbscript: document.getelementbyid(""draftactions" & wsid & """).style.visibility = ""hidden""' href=?action=draftreal&id=" & wsid & "&ways=B>�����</a>"
          rr " | <a target=fr1 style='color:#808080;' onclick='vbscript: document.getelementbyid(""draftactions" & wsid & """).style.visibility = ""hidden""' href=?action=draftreal&id=" & wsid & "&ways=->���</a>"
          rr "</span>"
        end if

        t = "" : if getval(shiftdata1, "draft") = "on" then t = "color:#aaaaaa;"
        rr "<span id=ws" & getval(shiftdata1, "id") & " style='font-size:10px;" & t & "'> "
        disp = replace(j, "^", "<br><img src=blank.gif width=20 height=1>") : dispcsv = ""

        if session("setshifts_showshift") = "shifts" then
          t = getval(shiftdata1, "shiftid") : t = getval(session("shiftnames"), t)
          if t = "" then
            h1 = getval(shiftdata1, "hour1")
            h2 = getval(shiftdata1, "hour1b")
            t = getval(session("shifthours"), h1 & "-" & h2)
            t = getval(session("shiftnames"), t)
          end if
          if instr(j, translate("~(����)~")) > 0 then
            h1 = getfromto(replace(j,"^","") & " ", "", translate(" ~Arrival~ "), " ", false, false)
            h2 = getfromto(replace(j,"^","") & " ", "", translate(" ~Distribute~ "), " ", false, false)
            t = getval(session("shifthours"), h1 & "-" & h2)
            if t <> "" then t = translate("(~Fixed~) ") & getval(session("shiftnames"), t)
          end if          
          if t <> "" then disp = t : dispcsv = t
          if t <> "" then
            zz = "FF8080,FF80FF,ff5555,FFFF00,80FF00,00FF40,00FFFF,0080C0,8080C0,FF00FF,804040,FF8040,00FF00,008080,8080FF,ff0077,FF0080,008040,0000A0,800080,8000FF,808000,808040,808080,40aaaa,C0C0C0,"
            'x = 0 : for m = 1 to len(t) : x = x + asc(mid(t,m,1)) : next : z = fix(((x / 25) - fix(x / 25)) * 25)
            if instr("�" & colorshifts, "�" & t & "�") = 0 then
              colorshifts = colorshifts & t & "�"
              colorshiftsi = colorshiftsi + 1
              z = colorshiftsi
            else
              z = left(colorshifts, instr(colorshifts, t))
              z = countstring(z, "�") + 1
            end if
            co = mid(zz, z * 7 + 1, 6)
            disp = "<span style='padding:2; background:#" & co & ";'>" & disp & "</span>"
          end if
        end if
        t = getval(shiftdata1, "xtype") : if t <> "" then rr "<span style='color:#0000aa;'>" & t & "</span>"
        t = getval(shiftdata1, "remarks") : if t <> "" then rr "<BR><span style='color:#aaaaaa;'>" & t & "</span>"
        rr disp

        rr "</span>"
        if getfield("csv") = "on" then
          j = replace(j,"����","�") : j = replace(j,"�����","��")
          j = replace(j,"Arrival","From") : j = replace(j,"Distribute","To")
          j = replace(j, "^", " ")
          if specs = "iai" and j <> "" and instr(j, translate("~Course~")) = 0 then
            e = rs("courseid1") : if cstr(e) <> "0" then j = j & translate(" ~Course~ ") & getcode("courses",e)
          end if
          j = translate(j)
          if dispcsv <> "" then j = dispcsv
          t = getval(shiftdata1, "xtype") : if t <> "" then j = j & " " & tocsv(t)
          t = getval(shiftdata1, "remarks") : if t <> "" then j = j & " " & tocsv(t)
          csv.writetext(j & ",")
        end if
        d = dateadd("d",1,d)
      loop
      if getfield("csv") = "on" then csv.writetext(vbcrlf)
    end if
    rs.movenext
  loop

  if session("scrollwithin") = "on" then
    rr "</table></div>"
    rr "<script language=vbscript>" & vbcrlf
    rr ":h = screen.height - 500 : h = iif(h < 200, 200, h) : scrollwithin.style.height = h" & vbcrlf
    rr "sub setcoltitlewidth" & vbcrlf
    rr "columntitles.innerhtml = columntitles.innerhtml & ""<span style='background:#dddddd; border-left:1px solid #bbbbbb; height:27; width:20;'><img src=blank.gif width=1 height=25></span>""" & vbcrlf
    for i = 0 to cellx
      t = "" : e = "" : if i = 0 then t = "position:relative; top:-2;" : e = "<img src=blank.gif width=1 height=20>"
      rr "columntitles.innerhtml = columntitles.innerhtml & ""<span style='" & t & "border-left:1px solid #bbbbbb; padding:2px; height:25; width:"" & cell_0_" & i & ".offsetWidth & ""; background:"" & cell_0_" & i & ".style.background & ""; '>"" & cell_0_" & i & ".innerhtml & """ & e & "</span>""" & vbcrlf
    next
    rr "trcoltitles.style.visibility = ""hidden"" : trcoltitles.style.position = ""absolute"" : trcoltitles.style.top = -2000" & vbcrlf
    rr "end sub" & vbcrlf
    rr "x = setTimeout(""setcoltitlewidth"",250,""vbscript"")" & vbcrlf
    rr "</script>" & vbcrlf
  end if

  rr "<iframe name=fr1 width=0 height=0></iframe>"
  x = rrpagesbar(session("setshifts_lines"), session("setshifts_page"), rsrc)
  if getfield("csv") = "on" and getfield("action2") = "doc" then
    s = "select firstname,lastname,username from buusers where id = " & session("userid")
    closers(rs) : set rs = conn.execute(sqlfilter(s))
    u = ""
    if specs = "techjet" then
      u = "�� ����: " & rs("firstname") & " " & rs("lastname") : if u = " " then u = rs("username")
    end if
    csv.position = 0 : r = csv.readtext(csv.size) : csv.close
    r = replace(r, ",���,", ",���� ����,",1,1,1)
    top = left(r, instr(r,vbcrlf)-1) : r = mid(r, instr(r,vbcrlf)+2)
    if right(top,1) = "," then top = left(top, len(top)-1)
    top = replace(top, ",", "<td><nobr>")
    top = u & "<br><table width=100% style='direction:rtl;' bgcolor=#aaaaaa cellpadding=4 cellspacing=1><tr bgcolor=#dddddd><td><nobr>" & top
    rclear
    rr "<body style='direction:rtl;'><br>"
    c = 0 : cc = 0
    do until r = ""
      b = left(r, instr(r,vbcrlf)-1) : r = mid(r, instr(r,vbcrlf)+2)
      c = c + 1 : cc = cc + 1 : if c = 1 then rr top
      rr "<tr bgcolor=#ffffff><td bgcolor=#eeeeee><nobr>"
      if right(b,1) = "," then b = left(b, len(b)-1)
      b = replace(b, ",", "<td><nobr>")
      rr b
      if c = 30 then
        c = 0
        rr "</table><div style='page-break-before:always;'>&nbsp;</div>"
      end if
    loop
    rend
  end if

  if getfield("csv") = "on" then
    csv.position = 0 : r = csv.readtext(csv.size) : csv.close
    response.clear
    response.ContentType = "application/csv"
    response.AddHeader "Content-Disposition", "filename=1.csv"
    response.write r
    rend
  end if

  rr "<input type=hidden name=ids value=" & ids & ">"
  rr "<tr><td bgcolor=eeeeee colspan=99 >"
  if getparam("ClearAllShiftsButton") = "y" and session("usergroup") = "admin" and session("usersiteid") = "0" then rr "<table align=left><tr><td><input type=button style='color:aa0000;' value='~Clear All Shifts~' onclick='vbscript: if msgbox(""~Clear All Shown Shifts~?"",vbyesno) = 6 then document.location = ""?action=shiftsubmit&action2=delall&token=" & session("token") & "&ids=" & ids & """'></table>"

  rr "<script language=vbscript>" & vbcrlf
  rr "function checkalluser(u,v)" & vbcrlf
  rr "  a = """ & checkboxdates & """" & vbcrlf
  rr "  do until a = """"" & vbcrlf
  rr "    d = left(a,instr(a,"","")-1) : a = mid(a,instr(a,"","")+1)" & vbcrlf
  rr "    if not document.getelementbyid(""u"" & u & ""d"" & d).disabled then document.getelementbyid(""u"" & u & ""d"" & d).checked = v" & vbcrlf
  rr "  loop" & vbcrlf
  rr "end function" & vbcrlf
  rr "function checkalldate(d,v)" & vbcrlf
  rr "a = """ & checkboxusers & """" & vbcrlf
  rr "  do until a = """"" & vbcrlf
  rr "    u = left(a,instr(a,"","")-1) : a = mid(a,instr(a,"","")+1)" & vbcrlf
  rr "    if not document.getelementbyid(""u"" & u & ""d"" & d).disabled then document.getelementbyid(""u"" & u & ""d"" & d).checked = v" & vbcrlf
  rr "  loop" & vbcrlf
  rr "end function" & vbcrlf
  rr "</script>" & vbcrlf
  rr "</form>" 'f1


  '----loadcsv - form-----------------
  rr "<tr><form name=flist method=post><input type=hidden name=token value=" & session("token") & "><td colspan=99>"

  if specs = "techjet" then
    rr " <input type=button class=groovybutton id=buttonfoodreport style='width:150; color:#008000;' onclick='vbscript: window.open(""?action=foodreport"")' value='��� ������ - ����'>"
    rr " <input type=button class=groovybutton id=buttonfoodreport style='width:150; color:#000080;' onclick='vbscript: window.open(""?action=foodreport&action2=doc"")' value='��� ������ - ����'>"
    rr " <input type=button class=groovybutton id=buttonmedreport style='width:150; color:#008000;' onclick='vbscript: window.open(""?action=medreport"")' value='��� ������ - ����'>"
    rr " <input type=button class=groovybutton id=buttonmedreport style='width:150; color:#000080;' onclick='vbscript: window.open(""?action=medreport&action2=doc"")' value='��� ������ - ����'>"
    rr " <input type=button class=groovybutton id=buttoncreatecsv style='width:150; color:#008000;' onclick='vbscript: window.open(""?action=groupshifts&csv=on"")' value='��� ����� ����� - ����'>"
    rr " <input type=button class=groovybutton id=buttoncreatecsv style='width:150; color:#000080;' onclick='vbscript: window.open(""?action=groupshifts&csv=on&action2=doc"")' value='��� ����� ����� - ����'>"
  else
    rr " <input type=button class=groovybutton id=buttoncreatecsv style='width:150; color:#008000;' onclick='vbscript: window.open(""?action=groupshifts&csv=on"")' value='��� ����� ����� - ����'>"
    rr " <input type=button class=groovybutton id=buttoncreatecsv style='width:150; color:#000080;' onclick='vbscript: window.open(""?action=groupshifts&csv=on&action2=doc"")' value='��� ����� ����� - ����'>"
  end if

  if session("usergroup") = "admin" then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.submit' style='color:#008000;' value='~Load CSV~'>")
    if getparam("MultiDailyShifts") = "y" then rr " <input type=checkbox name=addition>~As Additional Shifts~"
  end if

  if instr(",flextronics,plasan,", "," & specs & ",") > 0 then
    rr "<br><br>"
    rr " <input type=button class=groovybutton id=buttonfoodreport style='width:100; color:#008000;' onclick='vbscript: submitfoodreport' value='��� ������'>"
    rr selectdate("flist.foodreportdate*", d1)
    if session("usergroup") = "admin" then
      rr " <input type=checkbox id=doemail>��� ����� �"
      rr " <input type=text style='width:200; direction:ltr;' id=foodreportemail value=""" & getparam("foodreportemail") & """>"
    end if
    rr "<script language=vbscript>" & vbcrlf
    rr "sub submitfoodreport" & vbcrlf
    rr "  t = """" : if document.getelementbyid(""doemail"").checked then t = ""&foodreportemail="" & document.getelementbyid(""foodreportemail"").value" & vbcrlf
    rr "  window.open(""?action=foodreport2&d="" & document.getelementbyid(""foodreportdate"").value & ""&action2=doc"" & t)" & vbcrlf
    rr "end sub" & vbcrlf
    rr "</script>" & vbcrlf

    rr "<br><br>"
    rr " <input type=button class=groovybutton style='width:100; color:#008000;' onclick='vbscript: window.open(""?action=countworkerfoods&countworkerfoodsd1="" & flist.countworkerfoodsd1.value & ""&countworkerfoodsd2="" & flist.countworkerfoodsd2.value)' value='���� ������ ����'> "
    rr "~From~ " & selectdate("flist.countworkerfoodsd1*", "1/" & month(date) & "/" & year(date))
    rr "~To~ " & selectdate("flist.countworkerfoodsd2*", lastday(month(date) & "/" & year(date)) & "/" & month(date) & "/" & year(date))

  elseif getparam("ShiftFood") = "y" then
    rr "<br><br>"
    rr " <input type=button class=groovybutton id=buttonfoodreport style='width:100; color:#008000;' onclick='vbscript: if doemail.checked then window.open(""?action=foodreport&action2=doc&foodreportemail="" & foodreportemail.value) else window.open(""?action=foodreport&action2=doc"")' value='��� ������'>"
    rr " <input type=checkbox id=doemail>��� ����� �"
    rr " <input type=text style='width:200; direction:ltr;' id=foodreportemail value=""" & getparam("foodreportemail") & """>"
  end if

  rr "</tr></form></table>"

rrbottom()
rend()
%>
