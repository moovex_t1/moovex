<!--#include file="include.asp"-->
<%
'GET - http://localhost/trans/sup.asp?supplierid=9&pass=1&act=get
'GET - http://localhost/trans/sup.asp?supplierid=1&pass=1234567&act=get&d1=21/9/2011+00:00:00&d2=21/9/2011+23:59:59&u1=-60&u2=1/1/2025
'GET - http://127.0.0.1/trans/sup.asp?supplierid=1&pass=2222222&act=get&d1=31/01/2013+00:00:00&d2=31/01/2013+23:59:59&u1=1/2/2012+00:00:00
'SET - http://localhost/trans/sup.asp?supplierid=9&pass=1&act=set&orderid=4419&status=Done&driver=����+����&car=12233434&remarks=123
'http://localhost/trans/sup.asp?act=get&ids=7171,7172&supplierid=9&pass=1&output=screen
'=======================================================================================
server.scripttimeout = 600 'seconds

if getfield("act") = "get" and getfield("ids") <> "" then
  'nothing
else
  if getfield("supplierid") <> "" then
    supplierid = getfield("supplierid")
  elseif getfield("supplier") <> "" then
    i = getidlist("select id from suppliers where title = '" & getfield("supplier") & "'")
    if i <> "" then i = left(i,instr(i,",")-1) else rr "Supplier not found" : rend
    supplierid = i
  else
    rr "Please choose supplier" : rend
  end if
  s = "select id from buusers where instr(cstr(',' & supplierids), '," & supplierid & ",') > 0 and pass = '" & encrypt(getfield("pass"),"1") & "'"
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "Wrong Password" : rend
end if

'=======================================================================================
if getfield("act") = "get" then
  response.clear
  set rrr = Server.CreateObject("ADODB.Stream") : rrr.open
  set rrr1 = Server.CreateObject("ADODB.Stream") : rrr1.open
  set rrr2 = Server.CreateObject("ADODB.Stream") : rrr2.open
  nnn = "" 'no_station_workers buffer
  replacements = "" 'for laufer
  '---dictionaries for fast access
  Set csdict = Server.CreateObject("Scripting.Dictionary")
  a = session("cs")
  do until a = ""
    b = left(a,instr(a,"|")-1) : a = mid(a,instr(a,"|")+1)
    if instr(b,"=") > 0 then
      csdict.item(left(b,instr(b,"=")-1)) = mid(b,instr(b,"=")+1)
    end if
  loop
  Set csmdict = Server.CreateObject("Scripting.Dictionary")
  a = session("csm")
  do until a = ""
    b = left(a,instr(a,"|")-1) : a = mid(a,instr(a,"|")+1)
    if instr(b,"=") > 0 then
      csmdict.item(left(b,instr(b,"=")-1)) = mid(b,instr(b,"=")+1)
    end if
  loop

  tablename = "orders"
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_1.title) as from1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_1 on " & tablename & ".from1 = stations_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_2.title) as to1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_2 on " & tablename & ".to1 = stations_2.id)"
  s = replace(s, " from ", ", cstr('' & reasons_1.title) as reasonidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join reasons reasons_1 on " & tablename & ".reasonid = reasons_1.id)"
  s = replace(s, " from ", ", cstr('' & suppliers_1.title) as supplieridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join suppliers suppliers_1 on " & tablename & ".supplierid = suppliers_1.id)"
  s = replace(s, " from ", ", cstr('' & cars_1.title) & ' ' & cstr('' & cars_1.code) as caridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join cars cars_1 on " & tablename & ".carid = cars_1.id)"
  s = replace(s, " from ", ", cstr('' & drivers_1.title) & ' ' & cstr('' & drivers_1.phone) as driveridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join drivers drivers_1 on " & tablename & ".driverid = drivers_1.id)"
  s = replace(s, " from ", ", cstr('' & ordertypes_1.title) as ordertypeidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join ordertypes ordertypes_1 on " & tablename & ".ordertypeid = ordertypes_1.id)"
  s = replace(s, " from ", ", cstr('' & buusers_1.username) as useridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join buusers buusers_1 on " & tablename & ".userid = buusers_1.id)"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","orders.id,orders.date1,orders.date2,orders.date1b,orders.status,orders.manually,orders.courseid,orders.from1,orders.to1,orders.direction,orders.reasonid,orders.supplierid,orders.carid,orders.driverid,orders.ordertypeid,orders.production,orders.price,orders.addother,orders.fineother,orders.pids,orders.userid,orders.remarks,orders.siteid,orders.altstations,orders.updateddate,orders.via1,orders.history,orders.data1,orders.additionids",1,1,1)
  if getfield("ids") <> "" then
    s = s & " and " & tablename & ".id in(" & getfield("ids") & "0)"
  else
    d1 = now : if getfield("d1") <> "" then d1 = getfield("d1")
    s = s & " and " & tablename & ".date1 >= " & sqldate(d1)
    d2 = "1/1/2030" : if getfield("d2") <> "" then d2 = getfield("d2")
    if d2 <> "" then s = s & " and " & tablename & ".date1 <= " & sqldate(d2)
    u1 = getfield("u1") : if left(u1,1) = "-" then u1 = dateadd("n", u1, now)
    if u1 <> "" then s = s & " and " & tablename & ".updateddate >= " & sqldate(u1)
    if getfield("u2") <> "" then s = s & " and " & tablename & ".updateddate <= " & sqldate(getfield("u2"))
    s = s & " and orders.supplierid = " & supplierid
    if getfield("siteid") <> "" then s = s & " and orders.siteid = " & getfield("siteid")
    if lcase(getfield("status")) = "" then 
      s = s & " and orders.status in('Sent','Received','Canceled')"
    elseif lcase(getfield("status")) = "done" then
      s = s & " and orders.status in('Done','Done Approved')"
    elseif lcase(getfield("status")) = "allbutnew" then
      s = s & " and orders.status not in('Draft','New','Approved','Rejected')"
    elseif lcase(getfield("status")) = "all" then
      'nothing
    elseif lcase(getfield("status")) <> "" then
      s = s & " and orders.status = '" & getfield("status") & "'"
    end if
  end if
  if getfield("output") = "jaime" then s = s & " and instr(cstr(',' & orders.history), 'limoid=') = 0"
  s = s & " order by orders.date1"
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  set rs = conn.execute(sqlfilter(s))
  c = 0 : totalids = ""
  do until rs.eof
    c = c + 1
    theid = rs("id") : totalids = totalids & theid & ","
    Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
    Thedate1 = replace(thedate1,".","/")
    Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
    Thedirection = rs("direction") : if isnull(Thedirection) then Thedirection = ""
    Thepids = rs("pids") : Thepids = formatidlist(Thepids)
    Thealtstations = rs("altstations") : if isnull(Thealtstations) then Thealtstations = ""
    thehistory = rs("history") & ""
    theordertypeid = rs("ordertypeid")
    thedata1 = rs("data1") & "" : ddd = thedata1 : ddd = replace(ddd,"*","") : ddd = replace(ddd,";","")
    t = ""
    t = t & "Order,"
    t = t & "Date,"
    t = t & "Status,"
    t = t & "Course,"
    t = t & "CourseID,"
    t = t & "From,"
    t = t & "To,"
    t = t & "Direction,"
    t = t & "Car,"
    t = t & "Driver,"
    t = t & "Price,"
    t = t & "Remarks,"
    t = t & "Updated,"
    if specs = "imi" then t = t & "OrderType,"
    if instr(siteurl, "/tnuva/") > 0 and cstr(theordertypeid) = "20" then'������
      't = t & "Arrive,"
      t = t & "Purpose,"
      t = t & "Contact Person,"
      t = t & "Course Description,"
    end if
    if instr(siteurl, "/tnuva/") > 0 then
      t = t & "Waiting,"
    end if
    if specs = "elbit" then t = t & "Decomp,"
    if specs = "brom" then t = t & "Budget,"
    if specs = "rail" and cstr(theordertypeid) = session("bus_otid") then t = t & "Until," 'date1b
    if specs = "rail" then t = t & "Reason,"
    t = t & "Customer,"

    if specs = "iec" then
      t = t & "Rate Code,"
      t = t & "Deal Number,"
      t = t & "Deal Item,"
      t = t & "KM,"
      t = t & "Manual KM,"
      t = t & "Decomp,"
      t = t & "Site,"
      t = t & "Site ID,"
    end if

    t = t & vbcrlf
    rrr.writetext(t) : if c = 1 then rrr1.writetext(t)
    a = "id,date1,status,courseidlookup,courseid,from1lookup,to1lookup,direction,caridlookup,driveridlookup,price,remarks,updateddate,"
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      v = rs(b)
      if b = "date1" and instr(v, " ") = 0 then v = v & " 00:00:00"
      if specs = "laufer" and cstr(thecourseid) <> "0" and b = "from1lookup" then v = "#" & theid & "FROM#" 'replacements
      if specs = "laufer" and cstr(thecourseid) <> "0" and b = "to1lookup" then v = "#" & theid & "TO#" 'replacements
      'if instr(",tnuva,rail,ysteel,mizrahi,teva_new,shufersal," ,"," & specs & ",") > 0 and b = "to1lookup" then
      if b = "to1lookup" then
        t = trim("" & rs("via1"))
        if t <> "" and t <> "0" then v = v & " ��� " & t
      end if
      if b = "courseid" and v = "0" then v = rs("from1") & "-" & rs("to1")
      if b = "direction" and v = "A" then v = translate("~Collect~")
      if b = "direction" and v = "B" then v = translate("~Distribute~")
      if specs = "rail" and b = "remarks" and cstr(theordertypeid) = session("bus_otid") then
        t = getval(ddd, "n ����_���������_�����") : if t <> "" then v = v & " **���� ��������� ����� - " & t & "**"
      end if
      rrr.writetext(tocsv(v) & ",") : rrr1.writetext(tocsv(v) & ",")
    loop
    if specs = "imi" then
      v = gettitle("ordertypes", theordertypeid)
      rrr.writetext(tocsv(v) & ",") : rrr1.writetext(tocsv(v) & ",")
    end if

    if instr(siteurl, "/tnuva/") > 0 and cstr(theordertypeid) = "20" then'������
      'v = rs("date1b")
      'rrr.writetext(tocsv(v) & ",") : rrr1.writetext(tocsv(v) & ",")
      v = getval(ddd, "t ����_������")
      rrr.writetext(tocsv(v) & ",") : rrr1.writetext(tocsv(v) & ",")
      v = "" : t = getval(ddd, "w ���_���") : t = getlistitem(t, 1)
      if isnumeric(t) then
        sq = "select firstname,lastname,phone1 from workers where id = " & t
        set rs1 = conn.execute(sqlfilter(sq))
        if not rs1.eof then v = rs1("firstname") & " " & rs1("lastname") & " " & rs1("phone1")
      end if
      rrr.writetext(tocsv(v) & ",") : rrr1.writetext(tocsv(v) & ",")
      v = getval(ddd, "a �����")
      rrr.writetext(tocsv(v) & ",") : rrr1.writetext(tocsv(v) & ",")
    end if
    if instr(siteurl, "/tnuva/") > 0 then'waiting
      t = getfieldlist("select title from additions where id in(" & rs("additionids") & "0)")
      v = "0" : if instr(t, "�����") > 0 then v = "1"
      rrr.writetext(tocsv(v) & ",") : rrr1.writetext(tocsv(v) & ",")
    end if
    if specs = "elbit" then
      v = tocsv(getval(thehistory, "decomp"))
      rrr.writetext(tocsv(v) & ",") : rrr1.writetext(tocsv(v) & ",")
    end if
    if specs = "brom" then t = tocsv(getval(thehistory, "budgettype")) : rrr.writetext(t & ",") : rrr1.writetext(t & ",")
    if specs = "rail" and cstr(theordertypeid) = session("bus_otid") then t = tocsv(rs("date1b")) : rrr.writetext(t & ",") : rrr1.writetext(t & ",")
    if specs = "rail" then t = tocsv(rs("reasonidlookup")) : rrr.writetext(t & ",") : rrr1.writetext(t & ",")
    t = specs
    rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")

    if specs = "iec" then
      courseratedata1 = getonefield("select data1 from courserates where id = " & getval(thehistory, "courserateid"))
      t = getval(courseratedata1, "ratecode")
      rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")
      t = getval(courseratedata1, "dealnumber")
      rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")
      t = getval(courseratedata1, "dealitem")
      rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")

      t = getval(thehistory, "legdisa") : t = cdbl("0" & strfilter(t, "0123456789")) : t = fix(t/1000)
      rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")
      t = getval(thehistory, "manualkm")
      rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")

      t = getval(thehistory, "decomp")
      rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")

      t = "" & gettitle("sites",rs("siteid"))
      rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")
      t = "" & rs("siteid")
      rrr.writetext(tocsv(t) & ",") : rrr1.writetext(tocsv(t) & ",")
    end if

    if getfield("output") = "jaime" then
	    if t = "laufer" then t = "2112"
	    if t = "teva" then t = "2386"
	    if t = "iai" then t = "9165"
	    if t = "elbit" then t = "11560"
    end if
    rrr.writetext(vbcrlf) : rrr1.writetext(vbcrlf)

    '----------workers---------------------
    r2 = ""
    t = "Passengers,Firstname,Lastname,Address,CellPhone,HomePhone,Email,Unit,Station,StationID," & vbcrlf
    rrr.writetext(t) : if c = 1 then rrr2.writetext("orderid," & t)
    s = "select id,code,firstname,lastname,from1,to1,address,phone1,phone2,email,unitid from workers where id in(" & rs("pids") & "0) order by firstname,lastname"
    set rs1 = conn.execute(sqlfilter(s))
    workerbuffer = "" : cc = 0
    do until rs1.eof
      cc = cc + 1
      workerbuffer = setval(workerbuffer, rs1("id") & "firstname", tocsv(rs1("firstname")))
      t = rs1("lastname") : if getfield("output") = "file" then t = left(t,1)
      workerbuffer = setval(workerbuffer, rs1("id") & "lastname", tocsv(t))
      t = rs1("code") : if not isnumeric(lcase(trim(t))) then t = t & "-ID" & rs1("id")
      if getfield("output") = "file" then t = "M" & rs1("id")
      workerbuffer = setval(workerbuffer, rs1("id") & "code", tocsv(t))
      workerbuffer = setval(workerbuffer, rs1("id") & "from1", tocsv(rs1("from1")))
      workerbuffer = setval(workerbuffer, rs1("id") & "to1", tocsv(rs1("to1")))
      sid = getval(thealtstations, rs1("id") & "from1") : if sid = "" then sid = rs1("from1")
      r2 = r2 & "#SID" & sid & "#,"
      r2 = r2 & rs("id") & ","

      a = "code,firstname,lastname,address,phone1,phone2,email,"
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        t = rs1(b)
        if b = "code" then t = getval(workerbuffer, rs1("id") & "code")
        if b = "lastname" then t = getval(workerbuffer, rs1("id") & "lastname")
        if b = "address" then t = tocsv(getval(thealtstations, rs1("id") & "altaddress")) : if t = "" then t = tocsv(rs1("address"))
        t = tocsv(t)
        rrr.writetext(t & ",") : r2 = r2 & t & ","
      loop

      t = tocsv(gettitle("units", rs1("unitid"))) & "," : if specs = "rafael" and getfield("output") = "file" then t = ""
      rrr.writetext(t) : r2 = r2 & t
      t = tocsv(gettitle("stations", sid)) & "," & sid & "," : rrr.writetext(t) : r2 = r2 & t
      t = vbcrlf : rrr.writetext(t) : r2 = r2 & t
      rs1.movenext
    loop

    '-----stations------------------
    nnn = nnn & theid & "|"

    aa = getval(thehistory, "stationcourse")
    if aa <> "" then
      rrr.writetext("Stations,Hour,Get On,Get Off," & vbcrlf)
      c = 0 : dur = 0
      do until aa = ""
        stationid = left(aa,instr(aa,",")-1) : aa = mid(aa,instr(aa,",")+1)
        c = c + 1
        rrr.writetext(tocsv(gettitle("stations",stationid)) & ",")
    	  if c > 1 then t = getval(thehistory, "legdur" & c-1) : if isnumeric(t) then dur = dur + t
    	  t = dateadd("s", dur, thedate1) : t = getdatehourstring(t)
        rrr.writetext(t & ",")
				pp = thepids : t1 = "" : t2 = ""
				do until pp = ""
				  p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
				  if thedirection = "A" and getval(thealtstations, p & "from1") = stationid then t1 = t1 & getval(workerbuffer, p & "code") & ";"
				  if thedirection = "A" and getval(thealtstations, p & "to1")   = stationid then t2 = t2 & getval(workerbuffer, p & "code") & ";"
				  if thedirection = "B" and getval(thealtstations, p & "from1") = stationid then t2 = t2 & getval(workerbuffer, p & "code") & ";"
				  if thedirection = "B" and getval(thealtstations, p & "to1")   = stationid then t1 = t1 & getval(workerbuffer, p & "code") & ";"
				loop
        rrr.writetext(t1 & ",")
        rrr.writetext(t2 & ",")
        rrr.writetext(vbcrlf)
      loop

    elseif cstr(thecourseid) <> "0" then
      ti = getarrive(thecourseid, thedate1, thepids, thealtstations)
      workerstations = getarriveworkerstations : min = getarrivemin : max = getarrivemax
      du = courseplan(thecourseid, thedate1, thepids, thealtstations, thedirection, thehistory, "") 'only for courseplanstationtimes
      rrr.writetext("Stations,Hour,Get On,Get Off," & vbcrlf)
      ss = csdict("" & thecourseid) : ss = bothmatch(ss, workerstations)
      if specs = "tnuva" and cstr(theordertypeid) = session("regular_otid") then
        ss = csdict("" & thecourseid) : min = 0 : max = getonefield("select max(minutes) as x from coursestations where courseid = " & thecourseid) : if not isnumeric(max) then max = 0
      end if
      if thedirection = "B" then ss = reverselist(ss)
      if ss = "" then
        ss = csdict("" & thecourseid)
        if thedirection = "B" then ss = reverselist(ss)
        if ss <> "" then ss = left(ss,instr(ss,","))
      end if
      cc = 0 : plotted1 = "" : plotted2 = "" : setworkertime = ""
      sor = formatidlistunique(getval(thehistory, "sortworkers") & thepids) : sor = bothmatch(sor, thepids)
      pp = sor
      do until ss = ""
        cc = cc + 1
        stationid = left(ss,instr(ss,",")-1) : ss = mid(ss,instr(ss,",")+1)
        stationminutes = csmdict(thecourseid & "-" & stationid) : if stationminutes = "" then stationminutes = "0"
        if specs = "laufer" and cc = 1 then replacements = replacements & "#" & theid & "FROM#," & stationid & ","
        r2 = replace(r2, "#SID" & stationid & "#", cc)
        rrr.writetext(tocsv(gettitle("stations",stationid)) & ",")
        if thedirection = "B" then
          ti = dateadd("n", max - cdbl(stationminutes), thedate1)
        else
          ti = dateadd("n", stationminutes, thedate1)
          ti = dateadd("n", -min , ti)
        end if
        ti = getdatehourstring(ti)
        x = getval(courseplanstationtimes, stationid) : if x <> "" then ti = x
        stationtime = ti : if setworkertime <> "" then stationtime = setworkertime : setworkertime = ""
        rrr.writetext(stationtime & ",")
        for i = 1 to 2
          if i = 1 then t = "from1" : if thedirection = "B" then t = "to1"
          if i = 2 then t = "to1" : if thedirection = "B" then t = "from1"
          a = pp : ww = "" : lastworker = ""
          do until a = ""
            b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
            if instr("|" & thealtstations, "|" & b & t & "=" & stationid & "|") > 0 or (instr("|" & thealtstations, "|" & b & t & "=") = 0 and getval(workerbuffer, b & t) = cstr(stationid)) then
              workertime = getval(courseplanworkertimes, b)
              if specs = "metropolin" and (thedirection = "A" and ss <> "" or thedirection = "B" and cc > 1) and workertime > stationtime and lastworker <> "" then
                 ss = stationid & "," & ss : pp = nomatch(pp, lastworker) : setworkertime = workertime
                 exit do
              end if
              rrr.writetext(tocsv(getval(workerbuffer, b & "code")) & ";")
              lastworker = b
              if i = 1 then plotted1 = plotted1 & b & ","
              if i = 2 then plotted2 = plotted2 & b & ","
            end if
          loop
          if cc = 1 and i = 1 then rrr.writetext("#" & theid & "nnn1#")
          if ss = "" and i = 2 then rrr.writetext("#" & theid & "nnn2#")
          rrr.writetext(",")
          if i = 2 then rrr.writetext(vbcrlf)
        next
      loop
      if specs = "laufer" then replacements = replacements & "#" & theid & "TO#," & stationid & ","
    end if

    for i = 1 to 2
      if i = 1 then a = nomatch(thepids, plotted1)
      if i = 2 then a = nomatch(thepids, plotted2)
      if a <> "" then
        sq = "select id,code from workers where id in(" & a & "-1) order by code"
        set rs1 = conn.execute(sqlfilter(sq))
        do until rs1.eof
          't = rs1("code") : if not isnumeric(lcase(trim(t))) then t = t & "-ID" & rs1("id")
          t = getval(workerbuffer,rs1("id") & "code")
          nnn = nnn & t & ";"
          rs1.movenext
        loop
      end if
      nnn = nnn & "|"
    next

    '----sort by station time and discard the first column that was meant for sorting only..
    r2 = replace(r2, ",", "�") : r2 = replace(r2, vbcrlf, ",")
    r2 = sortlist(r2, "text")
    r2 = replace(r2, ",", vbcrlf) : r2 = replace(r2, "�", ",")
    a = r2 : r2 = ""
    do until a = ""
      b = left(a,instr(a,vbcrlf)-1) : a = mid(a,instr(a,vbcrlf)+2)
      b = mid(b,instr(b,",")+1)
      r2 = r2 & b & vbcrlf
    loop
    rrr2.writetext(r2)
    '---

    rrr.writetext(vbcrlf)
    rs.movenext
  loop
  rrr.position = 0 : r = rrr.readtext(rrr.size) : rrr.close

  do until nnn = ""
    i = left(nnn,instr(nnn,"|")-1) : nnn = mid(nnn,instr(nnn,"|")+1)
    n1 = left(nnn,instr(nnn,"|")-1) : nnn = mid(nnn,instr(nnn,"|")+1)
    n2 = left(nnn,instr(nnn,"|")-1) : nnn = mid(nnn,instr(nnn,"|")+1)
    r = replace(r, "#" & i & "nnn1#", n1)
    r = replace(r, "#" & i & "nnn2#", n2)
  loop

  if getfield("output") = "screen" then
    rr csv2table(r)
    rend

  elseif instr(getfield("output"), "csv") > 0 then
    response.ContentType = "application/csv"
    t = "1.csv" : if right(lcase(getfield("output")), 4) = ".csv" then t = strfilter(getfield("output"), "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-���������������������������")
    response.AddHeader "Content-Disposition", "filename=" & t
    rr r

  elseif getfield("output") = "jaime" then
    rrr1.position = 0 : r1 = rrr1.readtext(rrr1.size) : rrr1.close
    rrr2.position = 0 : r2 = rrr2.readtext(rrr2.size) : rrr2.close
    a = replacements
    do until a = ""
      f = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      v = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      r1 = replace(r1,f,v)
    loop
    rr r1 & "<hr>" & r2 & "<hr>"
    'u = "http://212.143.135.5:8098/mamanlauferghiwebs/TalWSWeb.asmx/CallTalWSProxy"
    'u = "http://212.143.135.4:8098/mamanlauferghiwebs/TalWSWeb.asmx/CallTalWSProxy"
    'u = "http://www.tal-limousine.com/mamanlauferghiwebs/TalWSWeb.asmx/CallTalWSProxy"
    'u = "http://www.tal-limo.co.il/mamanlauferghiwebs/TalWSWeb.asmx/CallTalWSProxy"
    u = "https://www.tal-limo.co.il/mamanlauferghiwebs/TalWSWeb.asmx/CallTalWSProxy"
    if instr(siteurl, "/localhost/") + instr(siteurl, "/127.0.0.1/") > 0 then u = "http://localhost/trans/sup.asp?act=nothing"
    req = "ParentData=" & replace(r1, "&", "*") & "&ChildData=" & replace(r2, "&", "*")
    if usingsuppliersystem = "on" then
      a = u & "?" & req
      f = "upload/tallimo_" & datediff("s", "1/1/2016 00:00:00", now) & "_" & createpassword(5) & ".txt"
	    du = addlog(f, a)
    else
	    set o = CreateObject("MSXML2.ServerXMLHTTP") 
	    o.open "POST", u, false
	    o.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
	    o.send req
	    res = o.responsetext : res = utf2ansi(res)
	    set o = nothing
	    logfile = server.mappath("upload/limolog" & year(date) & "-" & month(date) & ".txt")
	    t = ""
	    t = t & "|time=" & now
	    t = t & "|req=" & replace(req, vbcrlf, " ") & vbcrlf
	    t = t & "|res=" & replace(res, vbcrlf, " ") & vbcrlf
	    du = addlog(logfile, t)
	    '---limoid
	    a = res : a = replace(a,"""","") : a = striphtml(a) : a = replace(a, chr(10), "�") : a = replace(a, chr(13), "�") : a = a & "�"
	    do until a = ""
	      b = left(a,instr(a,"�")-1) : a = mid(a,instr(a,"�")+1)
	      if instr(b, "Success") > 0 then
	        rr b & "!<br>"
	        id = left(b,instr(b,",")-1)
	        limoid = mid(b,instrrev(b,",")+1)
	        s = "select history from orders where id = " & id
	        set rs = conn.execute(sqlfilter(s))
	        if not rs.eof then
	          history = rs("history") : if isnull(history) then history = ""
	          history = ctxt(history)
	          history = setval(history, "limoid", limoid)
	          s = "update orders set"
	          s = s & " history='" & chtm(history) & "'"
	          s = s & " where id = " & id
	          set rs1 = conn.execute(sqlfilter(s))
	        end if
	      end if
	    loop
    end if

  elseif getfield("output") = "post" then
    rr r
    set o = CreateObject("MSXML2.ServerXMLHTTP") 
    o.open "POST", "http://SOME_URL/get_moovex.asp", false 
    o.setRequestHeader "Content-Type", "application/x-www-form-urlencoded" 
    o.send "csv=" & curl(replace(r, "&", "*"))
    set o = nothing

  elseif getfield("output") = "file" then
    f = getparam("CSVPath")
    if f <> "" then
      if instr(f, ":") + instr(f, "\") = 0 then f = server.mappath(f)
      t = "" : if countstring(getfield("ids"), ",") = 1 then t = replace(getfield("ids"), ",", "_")
      f = left(f,instrrev(f,"\")) & "yit_" & getfield("supplierid") & "_" & t & datediff("s", "1/1/2012", now) & "_" & createpassword(8) & ".txt"
      x = addlog(f, r)
      du = zipfile(f)
    end if

  else
    rr r
    'if now < cdate("17/06/2014 15:00") and instr(siteurl, "127.0.0.1") > 0 then rr csv2table(r) else rr r

  end if

  logfile = server.mappath("upload/suplog" & year(date) & "-" & month(date) & ".txt")
  t = ""
  t = t & "|time=" & now
  t = t & "|output=" & getfield("output")
  t = t & "|ids=" & totalids
  du = addlog(logfile, t)
  rend
end if

'=======================================================================================
if getfield("act") = "set" then
  drivernames = ""
  s = "select id,title from drivers where supplierid = " & supplierid
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    t = rs("title") : t = trim(t) : t = ctxt(t) : t = replace(t,"""","") : t = replace(t,"'","")
    drivernames = setval(drivernames, t, rs("id"))
    rs.movenext
  loop
  t = getfield("driver") : t = trim(t) : t = ctxt(t) : t = replace(t,"""","") : t = replace(t,"'","")
  driverid = getval(drivernames, t) : if driverid = "" then driverid = "0"
  if getfield("driver") <> "" and driverid = "0" then rr "Driver Not Found<br>"

  s = "select id from cars where supplierid = " & supplierid & " and trim(code) = '" & getfield("car") & "'"
  set rs = conn.execute(sqlfilter(s))
  carid = 0 : if not rs.eof then carid = rs("id")
  if getfield("car") <> "" and cstr(carid) = "0" then rr "Car Not Found<br>"

  s = ""
  if getfield("driver") <> "" then s = s & ",driverid = " & driverid
  if getfield("car") <> "" then s = s & ",carid = " & carid
  if getfield("remarks") <> "" then s = s & ",remarks = '" & getfield("remarks") & "'"
  if getfield("status") <> "" then
    if instr(",Done,Received,", "," & getfield("status") & ",") > 0 then
      s = s & ",status = '" & getfield("status") & "'"
    else
      rr "Invalid Status<br>"
    end if
  end if

  if getfield("orderid") <> "" and s <> "" then
    sq = "select id from orders where id = " & getfield("orderid") & " and supplierid = " & supplierid
    set rs = conn.execute(sqlfilter(sq))
    if rs.eof then rr "Order Not Found" : rend
    s = "update orders set " & mid(s,2)
    s = s & ",updateduserid = " & session("userid") & ",updateddate = " & sqldate(now)
    s = s & " where id = " & getfield("orderid") & " and supplierid = " & supplierid
    set rs = conn.execute(sqlfilter(s))
    rr "OK " & getfield("orderid")
  end if
  rend
end if

%>
