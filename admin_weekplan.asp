<!--#include file="include.asp"-->
<%'serverfunctions
'['-------------functions---------------------------------
For Each item in Session.Contents
  if instr(item,"weekplans_") > 0 then session(item) = ""
Next
']
'BU generated 09/10/2016 12:25:46

function weekplandelitem(i)
  weekplandelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function weekplaninsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "0" : if getfield("day1") <> "" then t = getfield("day1")
  f = f & "day1," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "workerid" then t = session(tablename & "containerid")
  if getfield("workerid") <> "" then t = getfield("workerid")
  f = f & "workerid," : v = v & t & ","

  t = "AB" : if getfield("ways") <> "" then t = getfield("ways")
  f = f & "ways," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "shiftid" then t = session(tablename & "containerid")
  if getfield("shiftid") <> "" then t = getfield("shiftid")
  f = f & "shiftid," : v = v & t & ","

  t = "00:00" : if getfield("hour1") <> "" then t = getfield("hour1")
  f = f & "hour1," : v = v & "'" & t & "',"

  t = "00:00" : if getfield("hour1b") <> "" then t = getfield("hour1b")
  f = f & "hour1b," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "courseid" then t = session(tablename & "containerid")
  if getfield("courseid") <> "" then t = getfield("courseid")
  f = f & "courseid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "from1" then t = session(tablename & "containerid")
  if getfield("from1") <> "" then t = getfield("from1")
  f = f & "from1," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "to1" then t = session(tablename & "containerid")
  if getfield("to1") <> "" then t = getfield("to1")
  f = f & "to1," : v = v & t & ","

  t = "1/1/1900" : if getfield("date1") <> "" then t = getfield("date1")
  f = f & "date1," : v = v & sqldate(t) & ","

  t = "1/1/1900" : if getfield("date2") <> "" then t = getfield("date2")
  f = f & "date2," : v = v & sqldate(t) & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into weekplan(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from weekplan"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  weekplaninsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table weekplan (id autoincrement primary key,day1 text(1),workerid number,ways text(2),shiftid number,hour1 text(5),hour1b text(5),courseid number,from1 number,to1 number,date1 datetime,date2 datetime)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "weekplan" : session("tablename") = "weekplan"
tablefields = "id,day1,workerid,ways,shiftid,hour1,hour1b,courseid,from1,to1,date1,date2,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 4
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = weekplaninsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  msg = msg & " ~Item Updated~"
  's = "delete * from weekplan where id <> " & request_id & " and workerid = " & getfield("workerid") & " and day1 = '" & getfield("day1") & "'"
  'set rs1 = conn.execute(sqlfilter(s))
']
  s = "update " & tablename & " set "
  if okedit("day1") then s = s & "day1='" & getfield("day1") & "',"
  if okedit("workerid") then s = s & "workerid=0" & getfield("workerid") & ","
  if okedit("ways") then s = s & "ways='" & getfield("ways") & "',"
  if okedit("shiftid") then s = s & "shiftid=0" & getfield("shiftid") & ","
'[  if okedit("hour1") then s = s & "hour1='" & getfield("hour1") & "',"
'[  if okedit("hour1b") then s = s & "hour1b='" & getfield("hour1b") & "',"
  hour1 = getfield("hour1")
  hour1b = getfield("hour1b")
  if getfield("action3") = "sethours" then
    t = getonefield("select hour1 from shifts where id = 0" & getfield("shiftid")) : if ishour(t) then hour1 = t
    t = getonefield("select hour1b from shifts where id = 0" & getfield("shiftid")) : if ishour(t) then hour1b = t
  end if
  if okedit("hour1") then s = s & "hour1='" & hour1 & "',"
  if okedit("hour1b") then s = s & "hour1b='" & hour1b & "',"
']
'[  if okedit("courseid") then s = s & "courseid=0" & getfield("courseid") & ","
  if hasmatch(specs, "jerusalem,muni,") and getfield("courseid") = "0" then du = errorend("_back�� ����� �����")
  if okedit("courseid") then s = s & "courseid=0" & getfield("courseid") & ","

']
  if okedit("from1") then s = s & "from1=0" & getfield("from1") & ","
  if okedit("to1") then s = s & "to1=0" & getfield("to1") & ","
  if okedit("date1") then s = s & "date1=" & sqldate(getfield("date1")) & ","
  if okedit("date2") then s = s & "date2=" & sqldate(getfield("date2")) & ","
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(day1,workerid,ways,shiftid,hour1,hour1b,courseid,from1,to1,date1,date2)"
  s = s & " SELECT day1,workerid,ways,shiftid,hour1,hour1b,courseid,from1,to1,date1,date2"
  s = s & " FROM " & tablename & " where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  request_action = "form"
  request_id = theid
  msg = msg & " ~Record duplicated~"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = weekplandelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = weekplandelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      t = getfield("listupdate_day1_" & b) : if t = "" then t = getfield("multiupdate_day1")
      if okedit("day1") then s = s & ",day1 = '" & t & "'"
      t = getfield("listupdate_courseid_" & b) : if t = "" then t = getfield("multiupdate_courseid")
      if okedit("courseid") and t <> "" then s = s & ",courseid = " & t
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("day1") then csv(0) = csv(0) & "~weekplan_day1~^"
'[  if oksee("workerid") then csv(0) = csv(0) & "~weekplan_workerid~^"
  if oksee("workerid") then csv(0) = csv(0) & "~weekplan_workerid~^"
  if hasmatch(specs, "misgav,brener,") then
    csv(0) = csv(0) & "~workers_unitid~^"
    csv(0) = csv(0) & "����^"'grade
    csv(0) = csv(0) & "���^"'citystationid
  end if
']
  if oksee("ways") then csv(0) = csv(0) & "~weekplan_ways~^"
  if oksee("shiftid") then csv(0) = csv(0) & "~weekplan_shiftid~^"
  if oksee("hour1") then csv(0) = csv(0) & "~weekplan_hour1~^"
  if oksee("hour1b") then csv(0) = csv(0) & "~weekplan_hour1b~^"
  if oksee("courseid") then csv(0) = csv(0) & "~weekplan_courseid~^"
  if oksee("from1") then csv(0) = csv(0) & "~weekplan_from1~^"
  if oksee("to1") then csv(0) = csv(0) & "~weekplan_to1~^"
  if oksee("date1") then csv(0) = csv(0) & "~weekplan_date1~^"
  if oksee("date2") then csv(0) = csv(0) & "~weekplan_date2~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Theday1 = rs("day1") : if isnull(Theday1) then Theday1 = ""
  Theworkerid = rs("workerid") : if isnull(Theworkerid) then Theworkerid = 0
  Theworkerid = replace(theworkerid,",",".")
  Theways = rs("ways") : if isnull(Theways) then Theways = ""
  Theshiftid = rs("shiftid") : if isnull(Theshiftid) then Theshiftid = 0
  Theshiftid = replace(theshiftid,",",".")
  Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
  Thehour1b = rs("hour1b") : if isnull(Thehour1b) then Thehour1b = ""
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
  Thefrom1 = replace(thefrom1,",",".")
  Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
  Theto1 = replace(theto1,",",".")
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Thedate2 = rs("date2") : if isnull(Thedate2) then Thedate2 = "1/1/1900"
  Thedate2 = replace(thedate2,".","/")
  Theworkeridlookup = rs("workeridlookup") : if isnull(Theworkeridlookup) then Theworkeridlookup = ""
  Theshiftidlookup = rs("shiftidlookup") : if isnull(Theshiftidlookup) then Theshiftidlookup = ""
  Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
  Thefrom1lookup = rs("from1lookup") : if isnull(Thefrom1lookup) then Thefrom1lookup = ""
  Theto1lookup = rs("to1lookup") : if isnull(Theto1lookup) then Theto1lookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("day1") then csv(rc) = csv(rc) & replace(theday1,vbcrlf," ") & "^"
'[    if oksee("workerid") then csv(rc) = csv(rc) & theworkeridlookup & "^"
    if oksee("workerid") then csv(rc) = csv(rc) & theworkeridlookup & "^"
    if hasmatch(specs, "misgav,brener,") then
      sq = "select unitid,citystationid,data1 from workers where id = 0" & theworkerid
      set rs1 = conn.execute(sqlfilter(sq)) : workerunitid = 0 : workercitystationid = 0 : workerdata1 = "" : if not rs1.eof then workerunitid = rs1("unitid") : workercitystationid = rs1("citystationid") : workerdata1 = rs1("data1")
      csv(rc) = csv(rc) & gettitle("units", workerunitid) & "^"
      csv(rc) = csv(rc) & getval(workerdata1, "grade") & "^"
      csv(rc) = csv(rc) & gettitle("stations", workercitystationid) & "^"
    end if
']

    if oksee("ways") then csv(rc) = csv(rc) & replace(theways,vbcrlf," ") & "^"
    if oksee("shiftid") then csv(rc) = csv(rc) & theshiftidlookup & "^"
    if oksee("hour1") then csv(rc) = csv(rc) & replace(thehour1,vbcrlf," ") & "^"
    if oksee("hour1b") then csv(rc) = csv(rc) & replace(thehour1b,vbcrlf," ") & "^"
    if oksee("courseid") then csv(rc) = csv(rc) & thecourseidlookup & "^"
    if oksee("from1") then csv(rc) = csv(rc) & thefrom1lookup & "^"
    if oksee("to1") then csv(rc) = csv(rc) & theto1lookup & "^"
    t = "" : if isdate(thedate1) then if year(thedate1) <> 1900 then t = thedate1
    if oksee("date1") then csv(rc) = csv(rc) & t & "^"
    t = "" : if isdate(thedate2) then if year(thedate2) <> 1900 then t = thedate2
    if oksee("date2") then csv(rc) = csv(rc) & t & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
'[  loadcsvprepare()
  loadcsvprepare()
  courseids = ""
']
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0" : siteid = "0" : workerid = ""
      day1 = ""
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "day1" then
'[          if len(b) > 1 then b = left(b,1)
'[          if okedit("day1") then s = s & "day1='" & b & "',"
          b = replace(b, ";", ",") : b = formatidlist(b)
          day1 = b
          if okedit("day1") then s = s & "day1='#day1#',"
']
'[        elseif csvff(i) = "workerid" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from workers where lcase(cstr('' & firstname)) & ' ' & lcase(cstr('' & lastname)) & ' ' & lcase(cstr('' & code)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("workerid") then s = s & "workerid=" & v & ","
        elseif csvff(i) = "workerid" then
          if b = "" then
            v = 0
          else
            sq = "select id,siteid from workers"
            sq = sq & " where lcase(cstr('' & firstname)) & ' ' & lcase(cstr('' & lastname)) & ' ' & lcase(cstr('' & code)) = '" & lcase(b) & "'"
            sq = sq & " or lcase(cstr('' & code)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id") : siteid = rs1("siteid") : if isnull(siteid) then siteid = 0
          end if
          if v = 0 then rr "~Not found~: " & b & "<br>"
          workerid = v
          if v <> 0 and instr("," & updatedworkers, "," & v & ",") = 0 then updatedworkers = updatedworkers & v & ","
          if okedit("workerid") then s = s & "workerid=" & v & ","
']
        elseif csvff(i) = "ways" then
          if len(b) > 2 then b = left(b,2)
          if okedit("ways") then s = s & "ways='" & b & "',"
        elseif csvff(i) = "shiftid" then
          if b = "" then
            v = 0
          else
            sq = "select * from shifts where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("shiftid") then s = s & "shiftid=" & v & ","
        elseif csvff(i) = "hour1" then
          if len(b) > 5 then b = left(b,5)
          if okedit("hour1") then s = s & "hour1='" & b & "',"
        elseif csvff(i) = "hour1b" then
          if len(b) > 5 then b = left(b,5)
          if okedit("hour1b") then s = s & "hour1b='" & b & "',"
        elseif csvff(i) = "courseid" then
          if b = "" then
            v = 0
          else
'[            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "' or lcase(cstr('' & code)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            if instr("," & courseids, "," & v & ",") = 0 then courseids = courseids & v & ","
']
          end if
          if okedit("courseid") then s = s & "courseid=" & v & ","
'[        elseif csvff(i) = "from1" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("from1") then s = s & "from1=" & v & ","
        elseif csvff(i) = "from1" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "' and (siteid = 0 or siteid = " & siteid & ")"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("from1") then s = s & "from1=" & v & ","
']
'[        elseif csvff(i) = "to1" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("to1") then s = s & "to1=" & v & ","
        elseif csvff(i) = "to1" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "' and (siteid = 0 or siteid = " & siteid & ")"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("to1") then s = s & "to1=" & v & ","
']
        elseif csvff(i) = "date1" then
          if not isdate(b) then b = "1/1/1900"
          if okedit("date1") then s = s & "date1=" & sqldate(b) & ","
        elseif csvff(i) = "date2" then
          if not isdate(b) then b = "1/1/1900"
          if okedit("date2") then s = s & "date2=" & sqldate(b) & ","
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
       if s <> "" then
'[        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = weekplaninsertrecord
'[        s = "update " & tablename & " set " & s
'[        s = left(s, len(s) - 1) & " "
'[        s = s & "where id = " & theid
'[        if okwrite(tablename,theid) then
'[          closers(rs) : set rs = conn.execute(sqlfilter(s))
'[          c = c + 1
'[        end if
        if workerid = "" then rr "Line " & line & " - WORKER NOT FOUND<br>"
        a = day1 : orig = s
        do until a = ""
          b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
          s = replace(orig, "#day1#", b)
	        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = weekplaninsertrecord
	        s = "update " & tablename & " set " & s
	        s = left(s, len(s) - 1) & " "
	        s = s & "where id = " & theid
	        if okwrite(tablename,theid) then
	          closers(rs) : set rs = conn.execute(sqlfilter(s))
	          c = c + 1
	        end if
        loop
']
      end if
    end if
  loop
'[  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
  if hasmatch(specs, "jerusalem,muni,brener,") then
    ii = courseids
    do until ii = ""
      i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
      du = rebuildcoursefromweekplan(i)
      du = updatecourseunitids(i)
      du = setcoursetitle(i)
    loop
    ii = updatedworkers
    do until ii = ""
      i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
      du = updatestatusbamaarach(i)
    loop
  end if
']
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","weekplan.id,weekplan.day1,weekplan.workerid,weekplan.ways,weekplan.shiftid,weekplan.hour1,weekplan.hour1b,weekplan.courseid,weekplan.from1,weekplan.to1,weekplan.date1,weekplan.date2",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Theday1 = rs("day1") : if isnull(Theday1) then Theday1 = ""
  Theworkerid = rs("workerid") : if isnull(Theworkerid) then Theworkerid = 0
  Theworkerid = replace(theworkerid,",",".")
  Theways = rs("ways") : if isnull(Theways) then Theways = ""
  Theshiftid = rs("shiftid") : if isnull(Theshiftid) then Theshiftid = 0
  Theshiftid = replace(theshiftid,",",".")
  Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
  Thehour1b = rs("hour1b") : if isnull(Thehour1b) then Thehour1b = ""
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
  Thefrom1 = replace(thefrom1,",",".")
  Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
  Theto1 = replace(theto1,",",".")
  Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
  Thedate1 = replace(thedate1,".","/")
  Thedate2 = rs("date2") : if isnull(Thedate2) then Thedate2 = "1/1/1900"
  Thedate2 = replace(thedate2,".","/")
  formcontrols = "day1,workerid,ways,shiftid,hour1,hour1b,courseid,from1,to1,date1,date1_day,date1_month,date1_year,date1_hour,date1_insertdate,date1_removedate,date2,date2_day,date2_month,date2_year,date2_hour,date2_insertdate,date2_removedate,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><!img src='icons/calendar.png' ~iconsize~> ~weekplan_weekplan~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("day1") then'form:day1
    rr("<tr valign=top id=day1_tr0><td class=form_item1 id=day1_tr1><span id=day1_caption>~weekplan_day1~</span><td class=form_item2 id=day1_tr2>")
    a = "0,1,2,3,4,5,6,7,"
    rr("<select name=day1 dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
'[      if Theday1 = b then rr(" selected")
'[      rr(">" & b)
      if Theday1 = b then rr(" selected")
      rr(">~weekday" & b & "~")
']
    loop
    rr("</select>")'form:day1
  end if'form:day1

  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then
    rr("<tr valign=top id=workerid_tr0><td class=form_item1 id=workerid_tr1><span id=workerid_caption>~weekplan_workerid~</span><td class=form_item2 id=workerid_tr2>")
    if getfield("box") = "workerid" then theworkerid = getfield("boxid")
    rr("<select name=workerid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~weekplan_workerid~")
    sq = "select * from workers order by firstname,lastname,code"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theworkerid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("firstname") & " ")
      rr(rs1("lastname") & " ")
      rr(rs1("code") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:workerid
  elseif okedit("workerid") then
    rr("<input type=hidden name=workerid value=" & theworkerid & ">")
  end if'form:workerid

  if oksee("ways") then'form:ways
    rr("<tr valign=top id=ways_tr0><td class=form_item1 id=ways_tr1><span id=ways_caption>~weekplan_ways~</span><td class=form_item2 id=ways_tr2>")
    a = "AB,A,B,X,"
    rr("<select name=ways dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Theways = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:ways
  end if'form:ways

  if oksee("shiftid") and session(tablename & "containerfield") <> "shiftid" then
    rr("<tr valign=top id=shiftid_tr0><td class=form_item1 id=shiftid_tr1><span id=shiftid_caption>~weekplan_shiftid~</span><td class=form_item2 id=shiftid_tr2>")
    if getfield("box") = "shiftid" then theshiftid = getfield("boxid")
'[    rr("<select name=shiftid dir=~langdir~>")
    rr("<select name=shiftid dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : f1.action3.value = ""sethours"" : checkform'>")
']
    rr("<option value=0 style='color:bbbbfe;'>~weekplan_shiftid~")
    sq = "select * from shifts order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theshiftid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:shiftid
  elseif okedit("shiftid") then
    rr("<input type=hidden name=shiftid value=" & theshiftid & ">")
  end if'form:shiftid

  if oksee("hour1") then'form:hour1
    rr("<tr valign=top id=hour1_tr0><td class=form_item1 id=hour1_tr1><span id=hour1_caption>~weekplan_hour1~<img src=must.gif></span><td class=form_item2 id=hour1_tr2>")
'[    rr("<input type=text maxlength=5 name=hour1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thehour1 & """ dir=ltr>")
    rr("<input type=text maxlength=5 name=hour1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & Thehour1 & """ dir=ltr>")

    's = "select courseid1 from workers where id = " & theworkerid
    'set rs1 = conn.execute(sqlfilter(s))
    'courseid = 0 : if not rs1.eof then courseid = rs1("courseid1")
    'if courseid = 0 then
    '  rr "<input type=hidden name=hour1><font color=ff0000>~Worker does not have Course~"
    'else
    '  rr "<select name=hour1>"
    '  s = "select hour1 from coursehours where courseid = " & courseid & " and direction = 'A' order by hour1"
    '  set rs1 = conn.execute(sqlfilter(s))
    '  existed = ""
    '  do until rs1.eof
    '    rr "<option value='" & rs1("hour1") & "'" : if thehour1 = rs1("hour1") then rr " selected" : existed = "on"
    '    rr ">" & rs1("hour1")
    '    rs1.movenext
    '  loop
    '  rr "</select>"
    '  if existed = "" and request_action2 <> "newitem" then rr " * ~Hour for this day was~ " & thehour1 & " ~and is already not valid for the worker`s course~"
    'end if
']
  end if'form:hour1

  if oksee("hour1b") then'form:hour1b
    rr("<tr valign=top id=hour1b_tr0><td class=form_item1 id=hour1b_tr1><span id=hour1b_caption>~weekplan_hour1b~<img src=must.gif></span><td class=form_item2 id=hour1b_tr2>")
'[    rr("<input type=text maxlength=5 name=hour1b onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thehour1b & """ dir=ltr>")
    rr("<input type=text maxlength=5 name=hour1b onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & Thehour1b & """ dir=ltr>")

    'if courseid = 0 then
    '  rr "<input type=hidden name=hour1b>"
    'else
    '  rr "<select name=hour1b>"
    '  s = "select hour1 from coursehours where courseid = " & courseid & " and direction = 'B' order by hour1"
    '  set rs1 = conn.execute(sqlfilter(s))
    '  existed = ""
    '  do until rs1.eof
    '    rr "<option value='" & rs1("hour1") & "'" : if thehour1b = rs1("hour1") then rr " selected" : existed = "on"
    '    rr ">" & rs1("hour1")
    '    rs1.movenext
    '  loop
    '  rr "</select>"
    '  if existed = "" and request_action2 <> "newitem" then rr " * ~Hour for this day was~ " & thehour1b & " ~and is already not valid for the worker`s course~"
    'end if
']
  end if'form:hour1b

  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    rr("<tr valign=top id=courseid_tr0><td class=form_item1 id=courseid_tr1><span id=courseid_caption>~weekplan_courseid~</span><td class=form_item2 id=courseid_tr2>")
    if getfield("box") = "courseid" then thecourseid = getfield("boxid")
    rr("<select name=courseid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~weekplan_courseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thecourseid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:courseid
  elseif okedit("courseid") then
    rr("<input type=hidden name=courseid value=" & thecourseid & ">")
  end if'form:courseid

  if oksee("from1") and session(tablename & "containerfield") <> "from1" then
    rr("<tr valign=top id=from1_tr0><td class=form_item1 id=from1_tr1><span id=from1_caption>~weekplan_from1~</span><td class=form_item2 id=from1_tr2>")
    if getfield("box") = "from1" then thefrom1 = getfield("boxid")
'[    rr("<select name=from1 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~weekplan_from1~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Thefrom1) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:from1
    if hasmatch(specs, "jerusalem,muni,brener,") and session(tablename & "containerid") <> "0" then
      ii = ""
      if specs = "brener" then
        ii = getidlist("select id from stations")
      else
        s = "select from1,from2,to1,to2 from workers where id = 0" & session(tablename & "containerid")
        set rs = conn.execute(sqlfilter(s))
        if not rs.eof then
          'ii = rs("from1") & "," & rs("from2") & "," & rs("to1") & "," & rs("to2") & "," : ii = formatidlistunique(ii)
          ii = rs("from1") & "," & rs("from2") & "," : ii = formatidlistunique(ii)
        end if
      end if
      rr("<select name=from1 dir=~langdir~>")
      rr("<option value=0 style='color:bbbbfe;'>~weekplan_from1~")
      sq = "select * from stations where id in(" & ii & thefrom1 & ") order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        rr("<option value=""" & rs1("id") & """")
        if cstr(Thefrom1) = cstr(rs1("id")) then rr(" selected")
        rr(">")
        rr(rs1("title") & " ")
        rs1.movenext
      loop
      rr("</select>")
    else
      rr selectsearch("!f1.from1", thefrom1 & ",", "title,", "stations", "select id,title from stations")
    end if
']
  elseif okedit("from1") then
    rr("<input type=hidden name=from1 value=" & thefrom1 & ">")
  end if'form:from1

  if oksee("to1") and session(tablename & "containerfield") <> "to1" then
    rr("<tr valign=top id=to1_tr0><td class=form_item1 id=to1_tr1><span id=to1_caption>~weekplan_to1~</span><td class=form_item2 id=to1_tr2>")
    if getfield("box") = "to1" then theto1 = getfield("boxid")
'[    rr("<select name=to1 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~weekplan_to1~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Theto1) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:to1
    if hasmatch(specs, "jerusalem,muni,brener,") and session(tablename & "containerid") <> "0" then
      ii = ""
      if specs = "brener" then
        ii = getidlist("select id from stations")
      else
        s = "select from1,from2,to1,to2 from workers where id = 0" & session(tablename & "containerid")
        set rs = conn.execute(sqlfilter(s))
        if not rs.eof then
          'ii = rs("from1") & "," & rs("from2") & "," & rs("to1") & "," & rs("to2") & "," : ii = formatidlistunique(ii)
          ii = rs("to1") & "," & rs("to2") & "," : ii = formatidlistunique(ii)
        end if
      end if
      rr("<select name=to1 dir=~langdir~>")
      rr("<option value=0 style='color:bbbbfe;'>~weekplan_to1~")
      sq = "select * from stations where id in(" & ii & theto1 & ") order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        rr("<option value=""" & rs1("id") & """")
        if cstr(Theto1) = cstr(rs1("id")) then rr(" selected")
        rr(">")
        rr(rs1("title") & " ")
        rs1.movenext
      loop
      rr("</select>")

    else
      rr selectsearch("!f1.to1", theto1 & ",", "title,", "stations", "select id,title from stations")
    end if
']
  elseif okedit("to1") then
    rr("<input type=hidden name=to1 value=" & theto1 & ">")
  end if'form:to1

  if oksee("date1") then'form:date1
    rr("<tr valign=top id=date1_tr0><td class=form_item1 id=date1_tr1><span id=date1_caption>~weekplan_date1~</span><td class=form_item2 id=date1_tr2>")
    rr(SelectDate("f1.date1", thedate1))
  end if'form:date1

  if oksee("date2") then'form:date2
    rr("<tr valign=top id=date2_tr0><td class=form_item1 id=date2_tr1><span id=date2_caption>~weekplan_date2~</span><td class=form_item2 id=date2_tr2>")
    rr(SelectDate("f1.date2", thedate2))
  end if'form:date2

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonduplicate value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  rr(checkformadd)
'[  if okedit("hour1") then rr("    if f1.hour1.value <> """" then validfield(""hour1"") else invalidfield(""hour1"") : ok = false" & vbcrlf)
'[  if okedit("hour1b") then rr("    if f1.hour1b.value <> """" then validfield(""hour1b"") else invalidfield(""hour1b"") : ok = false" & vbcrlf)
  if okedit("hour1") then rr("    if len(f1.hour1.value) = 4 then f1.hour1.value = ""0"" & f1.hour1.value" & vbcrlf)
  if okedit("hour1") then rr("    if len(f1.hour1.value) = 5 and mid(f1.hour1.value,3,1) = "":"" and isnumeric(left(f1.hour1.value,2)) and isnumeric(right(f1.hour1.value,2)) and instr(f1.hour1.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) <= 59 then validfield(""hour1"") else invalidfield(""hour1"") : ok = false" & vbcrlf)
  if okedit("hour1b") then rr("    if len(f1.hour1b.value) = 4 then f1.hour1b.value = ""0"" & f1.hour1b.value" & vbcrlf)
  if okedit("hour1b") then rr("    if len(f1.hour1b.value) = 5 and mid(f1.hour1b.value,3,1) = "":"" and isnumeric(left(f1.hour1b.value,2)) and isnumeric(right(f1.hour1b.value,2)) and instr(f1.hour1b.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) <= 59 then validfield(""hour1b"") else invalidfield(""hour1b"") : ok = false" & vbcrlf)
  if okedit("from1") then rr("if instr(f1.from1.value, "","") > 0 then f1.from1.value = replace(f1.from1.value, "","", """")" & vbcrlf)
  if okedit("to1") then rr("if instr(f1.to1.value, "","") > 0 then f1.to1.value = replace(f1.to1.value, "","", """")" & vbcrlf)
']
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
 : rr("<script language=vbscript>on error resume next : f1.hour1.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".day1" : session(tablename & "_hidelistcolumns") = ""
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".day1"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><!img src='icons/calendar.png' ~iconsize~> ~weekplan_weekplan~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  rr(" <nobr><img id=day1_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=day1 dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~weekplan_day1~")
  aa = "0,1,2,3,4,5,6,7," : a = aa
  'sq = "select distinct day1 from weekplan order by day1"
  'set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof : a = a & rs1("day1") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if session(tablename & "_day1") = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select></nobr>")'search:day1
'[  if session(tablename & "containerfield") <> "workerid" then
'[    rr(" <nobr><img id=workerid_filtericon src=icons/suppliers.png style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=workerid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~weekplan_workerid~")
'[    sq = "select * from workers order by firstname,lastname,code"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_workerid")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("firstname") & " ")
'[      rr(rs1("lastname") & " ")
'[      rr(rs1("code") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:workerid
'[  end if'search:workerid

  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workers_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title"))
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
  end if'search:siteid

  if hasmatch(specs, "jerusalem,muni,brener,") then
    rr " ���� <input type=text name=unit style='width:150;' value=""" & session("weekplan_unit") & """>"
    rr " ���� <input type=text name=grade style='width:150;' value=""" & session("weekplan_grade") & """>"
  end if

']
  rr(" <nobr><img id=ways_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=ways dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~weekplan_ways~")
  aa = "AB,A,B,X," : a = aa
  'sq = "select distinct ways from weekplan order by ways"
  'set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof : a = a & rs1("ways") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if session(tablename & "_ways") = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select></nobr>")'search:ways
  if session(tablename & "containerfield") <> "shiftid" then
    rr(" <nobr><img id=shiftid_filtericon src=icons/refresh.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=shiftid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~weekplan_shiftid~")
    sq = "select * from shifts order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_shiftid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:shiftid
  end if'search:shiftid
  if session(tablename & "containerfield") <> "courseid" then
    rr(" <nobr><img id=courseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=courseid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~weekplan_courseid~")
'[    sq = "select * from courses order by code,title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_courseid")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("code") & " ")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:courseid
    rr "~weekplan_courseid~ <input type=text style='width:150;' name=coursefilter value='" & session(tablename & "_coursefilter") & "'>"
']
  end if'search:courseid
  if session(tablename & "containerfield") <> "from1" then
    rr(" <nobr><img id=from1_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=from1 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~weekplan_from1~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_from1")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:from1
    rr "~weekplan_from1~ <input type=text style='width:150;' name=from1filter value='" & session(tablename & "_from1filter") & "'>"
']
  end if'search:from1
  if session(tablename & "containerfield") <> "to1" then
    rr(" <nobr><img id=to1_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=to1 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~weekplan_to1~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_to1")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:to1
    rr "~weekplan_to1~ <input type=text style='width:150;' name=to1filter value='" & session(tablename & "_to1filter") & "'>"
']
  end if'search:to1
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & workers_1.firstname) & ' ' & cstr('' & workers_1.lastname) & ' ' & cstr('' & workers_1.code) as workeridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join workers workers_1 on " & tablename & ".workerid = workers_1.id)"
  s = replace(s, " from ", ", cstr('' & shifts_1.title) as shiftidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join shifts shifts_1 on " & tablename & ".shiftid = shifts_1.id)"
  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_1.title) as from1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_1 on " & tablename & ".from1 = stations_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_2.title) as to1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_2 on " & tablename & ".to1 = stations_2.id)"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","weekplan.id,weekplan.day1,weekplan.workerid,weekplan.ways,weekplan.shiftid,weekplan.hour1,weekplan.hour1b,weekplan.courseid,weekplan.from1,weekplan.to1,weekplan.date1,weekplan.date2",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_day1") <> "" then s = s & " and lcase(" & tablename & ".day1) = '" & lcase(session(tablename & "_day1")) & "'"
'[  if session(tablename & "_workerid") <> "" and session(tablename & "containerfield") <> "workerid" then s = s & " and " & tablename & ".workerid = " & session(tablename & "_workerid")
  if session(tablename & "_workerid") <> "" and session(tablename & "containerfield") <> "workerid" then s = s & " and " & tablename & ".workerid = " & session(tablename & "_workerid")
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".workerid in(select id from workers where siteid = " & session(tablename & "_siteid") & ")"
  if session("usersiteid") <> "0" and session(tablename & "_workerid") = "" then s = s & " and " & tablename & ".workerid in(select id from workers where siteid = " & session("usersiteid") & ")"

  if session(tablename & "_unit") <> "" then
    a = getidlist("select id from units where title like '%" & session(tablename & "_unit") & "%'")
    a = getidlist("select id from workers where unitid in(" & a & "-1)")
    s = s & " and " & tablename & ".workerid in(" & a & "-1)"
  end if

  if session(tablename & "_grade") <> "" then
    a = getidlist("select id from workers where instr(cstr('' & workers.data1), '|grade=" & session(tablename & "_grade") & "') > 0")
    s = s & " and " & tablename & ".workerid in(" & a & "-1)"
  end if
']
  if session(tablename & "_ways") <> "" then s = s & " and lcase(" & tablename & ".ways) = '" & lcase(session(tablename & "_ways")) & "'"
  if session(tablename & "_shiftid") <> "" and session(tablename & "containerfield") <> "shiftid" then s = s & " and " & tablename & ".shiftid = " & session(tablename & "_shiftid")
'[  if session(tablename & "_courseid") <> "" and session(tablename & "containerfield") <> "courseid" then s = s & " and " & tablename & ".courseid = " & session(tablename & "_courseid")
  v = session(tablename & "_coursefilter")
  if v <> "" then ii = getidlist("select id from courses where code = '" & v & "'") : s = s & " and " & tablename & ".courseid in(" & ii & "-1)"
']
'[  if session(tablename & "_from1") <> "" and session(tablename & "containerfield") <> "from1" then s = s & " and " & tablename & ".from1 = " & session(tablename & "_from1")
  v = session(tablename & "_from1filter")
  if v <> "" then ii = getidlist("select id from stations where title like '%" & v & "%'") : s = s & " and " & tablename & ".from1 in(" & ii & "-1)"
']
'[  if session(tablename & "_to1") <> "" and session(tablename & "containerfield") <> "to1" then s = s & " and " & tablename & ".to1 = " & session(tablename & "_to1")
  v = session(tablename & "_to1filter")
  if v <> "" then ii = getidlist("select id from stations where title like '%" & v & "%'") : s = s & " and " & tablename & ".to1 in(" & ii & "-1)"
']
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".day1 like '%" & w & "%' or"
        s = s & " cstr('' & workers_1.firstname) & ' ' & cstr('' & workers_1.lastname) & ' ' & cstr('' & workers_1.code) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".ways like '%" & w & "%' or"
        s = s & " cstr('' & shifts_1.title) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".hour1 like '%" & w & "%' or"
        s = s & " " & tablename & ".hour1b like '%" & w & "%' or"
        s = s & " cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_2.title) & ' ' like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("day1") then f = "weekplan.day1" : f2 = "day1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("day1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_day1~</a> " & a2)
  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then
    f = "workers_1.firstname" : f2 = "workeridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_workerid~</a> " & a2)
  end if
  if oksee("ways") then f = "weekplan.ways" : f2 = "ways" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("ways") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_ways~</a> " & a2)
  if oksee("shiftid") and session(tablename & "containerfield") <> "shiftid" then
    f = "shifts_1.title" : f2 = "shiftidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_shiftid~</a> " & a2)
  end if
  if oksee("hour1") then f = "weekplan.hour1" : f2 = "hour1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("hour1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_hour1~</a> " & a2)
  if oksee("hour1b") then f = "weekplan.hour1b" : f2 = "hour1b" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("hour1b") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_hour1b~</a> " & a2)
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    f = "courses_1.code" : f2 = "courseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_courseid~</a> " & a2)
  end if
  if oksee("from1") and session(tablename & "containerfield") <> "from1" then
    f = "stations_1.title" : f2 = "from1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_from1~</a> " & a2)
  end if
  if oksee("to1") and session(tablename & "containerfield") <> "to1" then
    f = "stations_2.title" : f2 = "to1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_to1~</a> " & a2)
  end if
  if oksee("date1") then f = "weekplan.date1" : f2 = "date1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("date1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_date1~</a> " & a2)
  if oksee("date2") then f = "weekplan.date2" : f2 = "date2" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("date2") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~weekplan_date2~</a> " & a2)

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Theday1 = rs("day1") : if isnull(Theday1) then Theday1 = ""
      Theworkerid = rs("workerid") : if isnull(Theworkerid) then Theworkerid = 0
      Theworkerid = replace(theworkerid,",",".")
      Theways = rs("ways") : if isnull(Theways) then Theways = ""
      Theshiftid = rs("shiftid") : if isnull(Theshiftid) then Theshiftid = 0
      Theshiftid = replace(theshiftid,",",".")
      Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
      Thehour1b = rs("hour1b") : if isnull(Thehour1b) then Thehour1b = ""
      Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
      Thecourseid = replace(thecourseid,",",".")
      Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
      Thefrom1 = replace(thefrom1,",",".")
      Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
      Theto1 = replace(theto1,",",".")
      Thedate1 = rs("date1") : if isnull(Thedate1) then Thedate1 = "1/1/1900"
      Thedate1 = replace(thedate1,".","/")
      Thedate2 = rs("date2") : if isnull(Thedate2) then Thedate2 = "1/1/1900"
      Thedate2 = replace(thedate2,".","/")
      Theworkeridlookup = rs("workeridlookup") : if isnull(Theworkeridlookup) then Theworkeridlookup = ""
      Theshiftidlookup = rs("shiftidlookup") : if isnull(Theshiftidlookup) then Theshiftidlookup = ""
      Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
      Thefrom1lookup = rs("from1lookup") : if isnull(Thefrom1lookup) then Thefrom1lookup = ""
      Theto1lookup = rs("to1lookup") : if isnull(Theto1lookup) then Theto1lookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
'[      firstcoltitle = theday1
      firstcoltitle = "~weekday" & theday1 & "~"
']
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/calendar.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then rr("<td class=list_item valign=top align=><nobr>" & theworkeridlookup)
      if oksee("ways") then rr("<td class=list_item valign=top align=><nobr>" & theways)
      if oksee("shiftid") and session(tablename & "containerfield") <> "shiftid" then rr("<td class=list_item valign=top align=><nobr>" & theshiftidlookup)
      if oksee("hour1") then rr("<td class=list_item valign=top align=><nobr>" & thehour1)
      if oksee("hour1b") then rr("<td class=list_item valign=top align=><nobr>" & thehour1b)
      if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_item valign=top align=><nobr>" & thecourseidlookup)
      if oksee("from1") and session(tablename & "containerfield") <> "from1" then rr("<td class=list_item valign=top align=><nobr>" & thefrom1lookup)
      if oksee("to1") and session(tablename & "containerfield") <> "to1" then rr("<td class=list_item valign=top align=><nobr>" & theto1lookup)
      if oksee("date1") then rr("<td class=list_item valign=top align=><nobr>") : if year(thedate1) <> 1900 then rr(thedate1)
      if oksee("date2") then rr("<td class=list_item valign=top align=><nobr>") : if year(thedate2) <> 1900 then rr(thedate2)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then rr("<td class=list_total1>") 'workerid
  if oksee("ways") and session(tablename & "containerfield") <> "ways" then rr("<td class=list_total1>") 'ways
  if oksee("shiftid") and session(tablename & "containerfield") <> "shiftid" then rr("<td class=list_total1>") 'shiftid
  if oksee("hour1") and session(tablename & "containerfield") <> "hour1" then rr("<td class=list_total1>") 'hour1
  if oksee("hour1b") and session(tablename & "containerfield") <> "hour1b" then rr("<td class=list_total1>") 'hour1b
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_total1>") 'courseid
  if oksee("from1") and session(tablename & "containerfield") <> "from1" then rr("<td class=list_total1>") 'from1
  if oksee("to1") and session(tablename & "containerfield") <> "to1" then rr("<td class=list_total1>") 'to1
  if oksee("date1") and session(tablename & "containerfield") <> "date1" then rr("<td class=list_total1>") 'date1
  if oksee("date2") and session(tablename & "containerfield") <> "date2" then rr("<td class=list_total1>") 'date2

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then
    rr(" ~weekplan_day1~ ")
    a = "0,1,2,3,4,5,6,7,"
    rr("<select name=multiupdate_day1 dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
'[      if Themultiupdate_day1 = b then rr(" selected")
'[      rr(">" & b)
      if Themultiupdate_day1 = b then rr(" selected")
      rr(">~weekday" & b & "~")
']
    loop
    rr("</select>")'form:multiupdate_day1
    rr(" ~weekplan_courseid~ ")
    rr("<select name=multiupdate_courseid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~weekplan_courseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Themultiupdate_courseid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:multiupdate_courseid
    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
