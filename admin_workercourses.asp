<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 13/01/2014 15:35:28

function workercoursesdelitem(i)
  workercoursesdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function workercoursesinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("shiftids") <> "" then t = getfield("shiftids")
  f = f & "shiftids," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "courseid" then t = session(tablename & "containerid")
  if getfield("courseid") <> "" then t = getfield("courseid")
  f = f & "courseid," : v = v & t & ","

'[  t = "1,2,3,4,5,6,7,8,9," : if getfield("days") <> "" then t = getfield("days")
  t = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
  if hasmatch(specs, "yoav,jerusalem,muni,brener,") then t = ""
  if getfield("days") <> "" then t = getfield("days")
']
  f = f & "days," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "from1" then t = session(tablename & "containerid")
  if getfield("from1") <> "" then t = getfield("from1")
  f = f & "from1," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "to1" then t = session(tablename & "containerid")
  if getfield("to1") <> "" then t = getfield("to1")
  f = f & "to1," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "workerid" then t = session(tablename & "containerid")
  if getfield("workerid") <> "" then t = getfield("workerid")
  f = f & "workerid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "courseid2" then t = session(tablename & "containerid")
  if getfield("courseid2") <> "" then t = getfield("courseid2")
  f = f & "courseid2," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "from2" then t = session(tablename & "containerid")
  if getfield("from2") <> "" then t = getfield("from2")
  f = f & "from2," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "to2" then t = session(tablename & "containerid")
  if getfield("to2") <> "" then t = getfield("to2")
  f = f & "to2," : v = v & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into workercourses(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from workercourses"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  workercoursesinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table workercourses (id autoincrement primary key,shiftids memo,courseid number,days text(50),from1 number,to1 number,workerid number,courseid2 number,from2 number,to2 number)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "workercourses" : session("tablename") = "workercourses"
tablefields = "id,shiftids,courseid,days,from1,to1,workerid,courseid2,from2,to2,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 4
if session("usergroup") = "teamleader" then ulev = 4
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = workercoursesinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("shiftids") then s = s & "shiftids='" & getfield("shiftids") & "',"
  if okedit("courseid") then s = s & "courseid=0" & getfield("courseid") & ","
  if okedit("days") then s = s & "days='" & getfield("days") & "',"
  if okedit("from1") then s = s & "from1=0" & getfield("from1") & ","
  if okedit("to1") then s = s & "to1=0" & getfield("to1") & ","
  if okedit("workerid") then s = s & "workerid=0" & getfield("workerid") & ","
  if okedit("courseid2") then s = s & "courseid2=0" & getfield("courseid2") & ","
  if okedit("from2") then s = s & "from2=0" & getfield("from2") & ","
  if okedit("to2") then s = s & "to2=0" & getfield("to2") & ","
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = workercoursesdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = workercoursesdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("shiftids") then csv(0) = csv(0) & "~workercourses_shiftids~^"
  if oksee("courseid") then csv(0) = csv(0) & "~workercourses_courseid~^"
  if oksee("days") then csv(0) = csv(0) & "~workercourses_days~^"
  if oksee("from1") then csv(0) = csv(0) & "~workercourses_from1~^"
  if oksee("to1") then csv(0) = csv(0) & "~workercourses_to1~^"
  if oksee("workerid") then csv(0) = csv(0) & "~workercourses_workerid~^"
  if oksee("courseid2") then csv(0) = csv(0) & "~workercourses_courseid2~^"
  if oksee("from2") then csv(0) = csv(0) & "~workercourses_from2~^"
  if oksee("to2") then csv(0) = csv(0) & "~workercourses_to2~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Theshiftids = rs("shiftids") : Theshiftids = formatidlist(Theshiftids)
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
  Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
  Thefrom1 = replace(thefrom1,",",".")
  Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
  Theto1 = replace(theto1,",",".")
  Theworkerid = rs("workerid") : if isnull(Theworkerid) then Theworkerid = 0
  Theworkerid = replace(theworkerid,",",".")
  Thecourseid2 = rs("courseid2") : if isnull(Thecourseid2) then Thecourseid2 = 0
  Thecourseid2 = replace(thecourseid2,",",".")
  Thefrom2 = rs("from2") : if isnull(Thefrom2) then Thefrom2 = 0
  Thefrom2 = replace(thefrom2,",",".")
  Theto2 = rs("to2") : if isnull(Theto2) then Theto2 = 0
  Theto2 = replace(theto2,",",".")
  Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
  Thefrom1lookup = rs("from1lookup") : if isnull(Thefrom1lookup) then Thefrom1lookup = ""
  Theto1lookup = rs("to1lookup") : if isnull(Theto1lookup) then Theto1lookup = ""
  Theworkeridlookup = rs("workeridlookup") : if isnull(Theworkeridlookup) then Theworkeridlookup = ""
  Thecourseid2lookup = rs("courseid2lookup") : if isnull(Thecourseid2lookup) then Thecourseid2lookup = ""
  Thefrom2lookup = rs("from2lookup") : if isnull(Thefrom2lookup) then Thefrom2lookup = ""
  Theto2lookup = rs("to2lookup") : if isnull(Theto2lookup) then Theto2lookup = ""

    csv(rc) = csv(rc) & theid & "^"
      t = "" : d = formatidlist(theshiftids)
      sq = "select * from shifts where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    if oksee("shiftids") then csv(rc) = csv(rc) & t & "^"
    if oksee("courseid") then csv(rc) = csv(rc) & thecourseidlookup & "^"
    if oksee("days") then csv(rc) = csv(rc) & replace(thedays,vbcrlf," ") & "^"
    if oksee("from1") then csv(rc) = csv(rc) & thefrom1lookup & "^"
    if oksee("to1") then csv(rc) = csv(rc) & theto1lookup & "^"
    if oksee("workerid") then csv(rc) = csv(rc) & theworkeridlookup & "^"
    if oksee("courseid2") then csv(rc) = csv(rc) & thecourseid2lookup & "^"
    if oksee("from2") then csv(rc) = csv(rc) & thefrom2lookup & "^"
    if oksee("to2") then csv(rc) = csv(rc) & theto2lookup & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
      s = "" : thisid = "0"
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "shiftids" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from shifts where lcase(cstr('' & title)) = '" & lcase(t) & "'"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("shiftids") then s = s & "shiftids='" & v & "',"
'[        elseif csvff(i) = "courseid" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "'"
        elseif csvff(i) = "courseid" then
          if b = "" then
            v = 0
          else
            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "' or lcase(cstr('' & code)) = '" & lcase(b) & "'"
']
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("courseid") then s = s & "courseid=" & v & ","
        elseif csvff(i) = "days" then
          if len(b) > 50 then b = left(b,50)
          if okedit("days") then s = s & "days='" & b & "',"
        elseif csvff(i) = "from1" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("from1") then s = s & "from1=" & v & ","
        elseif csvff(i) = "to1" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("to1") then s = s & "to1=" & v & ","
        elseif csvff(i) = "workerid" then
          if b = "" then
            v = 0
          else
'[            sq = "select * from workers where lcase(cstr('' & firstname)) & ' ' & lcase(cstr('' & lastname)) & ' ' & lcase(cstr('' & code)) = '" & lcase(b) & "'"
            sq = "select * from workers where lcase(cstr('' & firstname)) & ' ' & lcase(cstr('' & lastname)) & ' ' & lcase(cstr('' & code)) = '" & lcase(b) & "' or lcase(cstr('' & code)) = '" & lcase(b) & "'"
']
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("workerid") then s = s & "workerid=" & v & ","
'[        elseif csvff(i) = "courseid2" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "'"
        elseif csvff(i) = "courseid2" then
          if b = "" then
            v = 0
          else
            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "' or lcase(cstr('' & code)) = '" & lcase(b) & "'"
']
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("courseid2") then s = s & "courseid2=" & v & ","
        elseif csvff(i) = "from2" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("from2") then s = s & "from2=" & v & ","
        elseif csvff(i) = "to2" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("to2") then s = s & "to2=" & v & ","
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = workercoursesinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","workercourses.id,workercourses.shiftids,workercourses.courseid,workercourses.days,workercourses.from1,workercourses.to1,workercourses.workerid,workercourses.courseid2,workercourses.from2,workercourses.to2",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Theshiftids = rs("shiftids") : Theshiftids = formatidlist(Theshiftids)
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
  Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
  Thefrom1 = replace(thefrom1,",",".")
  Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
  Theto1 = replace(theto1,",",".")
  Theworkerid = rs("workerid") : if isnull(Theworkerid) then Theworkerid = 0
  Theworkerid = replace(theworkerid,",",".")
  Thecourseid2 = rs("courseid2") : if isnull(Thecourseid2) then Thecourseid2 = 0
  Thecourseid2 = replace(thecourseid2,",",".")
  Thefrom2 = rs("from2") : if isnull(Thefrom2) then Thefrom2 = 0
  Thefrom2 = replace(thefrom2,",",".")
  Theto2 = rs("to2") : if isnull(Theto2) then Theto2 = 0
  Theto2 = replace(theto2,",",".")
  formcontrols = "shiftids,shiftids_select,shiftids_trigger,courseid,days,from1,to1,workerid,courseid2,from2,to2,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/upcoming-work.png' ~iconsize~> ~workercourses_workercourses~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("shiftids") then'form:shiftids
    rr("<tr valign=top id=shiftids_tr0><td class=form_item1 id=shiftids_tr1><span id=shiftids_caption>~workercourses_shiftids~</span><td class=form_item2 id=shiftids_tr2>")
'[    rr(selectlookupmultiauto("f1.shiftids",theshiftids, "icons/refresh.png", "shifts,title,", 300, okwritehere))
    sid = getonefield("select siteid from workers where id = " & theworkerid) : if sid = "" then sid = "0"
    t = "select * from shifts where id in(" & theshiftids & "-1) or active = 'on' and (siteid = " & sid & " or siteid = 0) order by title,hour1,hour1b"
    rr(selectlookupmultiauto("f1.shiftids",theshiftids, "icons/refresh.png", "shifts,title,:" & t, 300, okwritehere))
    rr "<br><font style='font-size:10;'>* ���� ����� ������ �� ���� ""����"" �� ��� ����� �� �������� ������� ������� ������."
']
  end if'form:shiftids

  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    rr("<tr valign=top id=courseid_tr0><td class=form_item1 id=courseid_tr1><span id=courseid_caption>~workercourses_courseid~</span><td class=form_item2 id=courseid_tr2>")
    if getfield("box") = "courseid" then thecourseid = getfield("boxid")
'[    rr("<select name=courseid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workercourses_courseid~")
'[    sq = "select * from courses order by code,title"
    sq = "select courseids from shifts where id in(" & theshiftids & "-1)"
    set rs = conn.execute(sqlfilter(sq))
    courseids = "" : if not rs.eof then courseids = rs("courseids")
    rr("<select name=courseid dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : checkform'>")
    rr("<option value=0 style='color:bbbbfe;'>~workercourses_courseid~")
    sq = "select * from courses where id = " & thecourseid & " or (id in(" & courseids & "0) and active = 'on') order by code,title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thecourseid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:courseid
  elseif okedit("courseid") then
    rr("<input type=hidden name=courseid value=" & thecourseid & ">")
  end if'form:courseid

  if oksee("days") then'form:days
    rr("<tr valign=top id=days_tr0><td class=form_item1 id=days_tr1><span id=days_caption>~workercourses_days~</span><td class=form_item2 id=days_tr2>")
'[    c = 0 : s = "" : a = "1,2,3,4,5,6,7,8,9,"
    c = 0 : s = "" : a = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
']
    cc = countstring(a,",")
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
      rr("<input type=checkbox name=days_c" & c)
      if instr("," & thedays & ",","," & b & ",") > 0 then rr(" checked")
      rr(" onclick='vbscript:populatedays'>")
'[      rr(b & " ")
      rr("~weekday" & b & "~&nbsp;&nbsp;&nbsp;")
']
      s = s & "if f1.days_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
    loop
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub populatedays" & vbcrlf)
    rr("    t = """"" & vbcrlf)
    rr(s & vbcrlf)
    rr("    f1.days.value = t" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("</script>" & vbcrlf)
    rr("<input type=hidden name=days value=""" & Thedays & """>")
  end if'form:days

  if oksee("from1") and session(tablename & "containerfield") <> "from1" then
    rr("<tr valign=top id=from1_tr0><td class=form_item1 id=from1_tr1><span id=from1_caption>~workercourses_from1~</span><td class=form_item2 id=from1_tr2>")
    if getfield("box") = "from1" then thefrom1 = getfield("boxid")
    rr("<select name=from1 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workercourses_from1~")
'[    sq = "select * from stations order by title"
    ii = ""
    sq = "select stationid from coursestations where courseid = " & thecourseid
    set rs = conn.execute(sqlfilter(sq))
    do until rs.eof
      ii = ii & rs("stationid") & ","
      rs.movenext
    loop
    rr("<option value=0 style='color:bbbbfe;'>~workercourses_from1~")
    sq = "select * from stations where id in(" & ii & "0) order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thefrom1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:from1
  elseif okedit("from1") then
    rr("<input type=hidden name=from1 value=" & thefrom1 & ">")
  end if'form:from1

  if oksee("to1") and session(tablename & "containerfield") <> "to1" then
    rr("<tr valign=top id=to1_tr0><td class=form_item1 id=to1_tr1><span id=to1_caption>~workercourses_to1~</span><td class=form_item2 id=to1_tr2>")
    if getfield("box") = "to1" then theto1 = getfield("boxid")
    rr("<select name=to1 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workercourses_to1~")
'[    sq = "select * from stations order by title"
    rr("<option value=0 style='color:bbbbfe;'>~workercourses_to1~")
    sq = "select * from stations where id in(" & ii & "0) order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theto1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:to1
  elseif okedit("to1") then
    rr("<input type=hidden name=to1 value=" & theto1 & ">")
  end if'form:to1

  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then
    rr("<tr valign=top id=workerid_tr0><td class=form_item1 id=workerid_tr1><span id=workerid_caption>~workercourses_workerid~</span><td class=form_item2 id=workerid_tr2>")
    if getfield("box") = "workerid" then theworkerid = getfield("boxid")
    rr("<select name=workerid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~workercourses_workerid~")
    sq = "select * from workers order by firstname,lastname,code"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theworkerid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("firstname") & " ")
      rr(rs1("lastname") & " ")
      rr(rs1("code") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:workerid
  elseif okedit("workerid") then
    rr("<input type=hidden name=workerid value=" & theworkerid & ">")
  end if'form:workerid

  if oksee("courseid2") and session(tablename & "containerfield") <> "courseid2" then
'[    rr("<tr valign=top id=courseid2_tr0><td class=form_item1 id=courseid2_tr1><span id=courseid2_caption>~workercourses_courseid2~</span><td class=form_item2 id=courseid2_tr2>")
    rr "<tr><td><td style='font-size:10;'>~Fill in the following details only if distribution course is different from collection course~"
    rr("<tr valign=top id=courseid2_tr0><td class=form_item1 id=courseid2_tr1><span id=courseid2_caption>~workercourses_courseid2~</span><td class=form_item2 id=courseid2_tr2>")
']
    if getfield("box") = "courseid2" then thecourseid2 = getfield("boxid")
'[    rr("<select name=courseid2 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workercourses_courseid2~")
'[    sq = "select * from courses order by code,title"
    rr("<select name=courseid2 dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : checkform'>")
    rr("<option value=0 style='color:bbbbfe;'>~workercourses_courseid2~")
    sq = "select * from courses where id = " & thecourseid2 & " or (id in(" & courseids & "0) and active = 'on') order by code,title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thecourseid2) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:courseid2
  elseif okedit("courseid2") then
    rr("<input type=hidden name=courseid2 value=" & thecourseid2 & ">")
  end if'form:courseid2

  if oksee("from2") and session(tablename & "containerfield") <> "from2" then
    rr("<tr valign=top id=from2_tr0><td class=form_item1 id=from2_tr1><span id=from2_caption>~workercourses_from2~</span><td class=form_item2 id=from2_tr2>")
    if getfield("box") = "from2" then thefrom2 = getfield("boxid")
    rr("<select name=from2 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workercourses_from2~")
'[    sq = "select * from stations order by title"
    ii = ""
    sq = "select stationid from coursestations where courseid = " & thecourseid2
    set rs = conn.execute(sqlfilter(sq))
    do until rs.eof
      ii = ii & rs("stationid") & ","
      rs.movenext
    loop
    rr("<option value=0 style='color:bbbbfe;'>~workercourses_from2~")
    sq = "select * from stations where id in(" & ii & "0) order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thefrom2) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:from2
  elseif okedit("from2") then
    rr("<input type=hidden name=from2 value=" & thefrom2 & ">")
  end if'form:from2

  if oksee("to2") and session(tablename & "containerfield") <> "to2" then
    rr("<tr valign=top id=to2_tr0><td class=form_item1 id=to2_tr1><span id=to2_caption>~workercourses_to2~</span><td class=form_item2 id=to2_tr2>")
    if getfield("box") = "to2" then theto2 = getfield("boxid")
    rr("<select name=to2 dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~workercourses_to2~")
'[    sq = "select * from stations order by title"
    rr("<option value=0 style='color:bbbbfe;'>~workercourses_to2~")
    sq = "select * from stations where id in(" & ii & "0) order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Theto2) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:to2
  elseif okedit("to2") then
    rr("<input type=hidden name=to2 value=" & theto2 & ">")
  end if'form:to2

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)

  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)


'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".id" : session(tablename & "_hidelistcolumns") = ""
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".id"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/upcoming-work.png' ~iconsize~> ~workercourses_workercourses~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
'[  if session(tablename & "containerfield") <> "courseid" then
'[    rr(" <nobr><img id=courseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=courseid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~workercourses_courseid~")
'[    sq = "select * from courses order by code,title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_courseid")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("code") & " ")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:courseid
'[  end if'search:courseid
  rr "<nobr> ~workercourses_courseid~ <input type=text name=courseidfilter style='width:80;' value='" & session(tablename & "_courseidfilter") & "'></nobr>"
']
'[  if session(tablename & "containerfield") <> "from1" then
'[    rr(" <nobr><img id=from1_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=from1 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~workercourses_from1~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_from1")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:from1
'[  end if'search:from1
  rr "<nobr> ~workercourses_from1~ <input type=text name=from1filter style='width:80;' value='" & session(tablename & "_from1filter") & "'></nobr>"
']

'[  if session(tablename & "containerfield") <> "to1" then
'[    rr(" <nobr><img id=to1_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=to1 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~workercourses_to1~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_to1")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:to1
'[  end if'search:to1
  rr "<nobr> ~workercourses_to1~ <input type=text name=to1filter style='width:80;' value='" & session(tablename & "_to1filter") & "'></nobr>"
']
'[  if session(tablename & "containerfield") <> "workerid" then
'[    rr(" <nobr><img id=workerid_filtericon src=icons/suppliers.png style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=workerid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~workercourses_workerid~")
'[    sq = "select * from workers order by firstname,lastname,code"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_workerid")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("firstname") & " ")
'[      rr(rs1("lastname") & " ")
'[      rr(rs1("code") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:workerid
'[  end if'search:workerid
  rr "<nobr> ~workercourses_workerid~ <input type=text name=workeridfilter style='width:80;' value='" & session(tablename & "_workeridfilter") & "'></nobr>"
']
'[  if session(tablename & "containerfield") <> "courseid2" then
'[    rr(" <nobr><img id=courseid2_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=courseid2 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~workercourses_courseid2~")
'[    sq = "select * from courses order by code,title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_courseid2")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("code") & " ")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:courseid2
'[  end if'search:courseid2
  rr "<nobr> ~workercourses_courseid2~ <input type=text name=courseid2filter style='width:80;' value='" & session(tablename & "_courseid2filter") & "'></nobr>"
']
'[  if session(tablename & "containerfield") <> "from2" then
'[    rr(" <nobr><img id=from2_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=from2 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~workercourses_from2~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_from2")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:from2
'[  end if'search:from2
  rr "<nobr> ~workercourses_from2~ <input type=text name=from2filter style='width:80;' value='" & session(tablename & "_from2filter") & "'></nobr>"
']
'[  if session(tablename & "containerfield") <> "to2" then
'[    rr(" <nobr><img id=to2_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'>")
'[    rr(" <select style='width:150;' name=to2 dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~workercourses_to2~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(session(tablename & "_to2")) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select></nobr>")'search:to2
'[  end if'search:to2
  rr "<nobr> ~workercourses_to2~ <input type=text name=to2filter style='width:80;' value='" & session(tablename & "_to2filter") & "'></nobr>"
']
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_1.title) as from1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_1 on " & tablename & ".from1 = stations_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_2.title) as to1lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_2 on " & tablename & ".to1 = stations_2.id)"
  s = replace(s, " from ", ", cstr('' & workers_1.firstname) & ' ' & cstr('' & workers_1.lastname) & ' ' & cstr('' & workers_1.code) as workeridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join workers workers_1 on " & tablename & ".workerid = workers_1.id)"
  s = replace(s, " from ", ", cstr('' & courses_2.code) & ' ' & cstr('' & courses_2.title) as courseid2lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_2 on " & tablename & ".courseid2 = courses_2.id)"
  s = replace(s, " from ", ", cstr('' & stations_3.title) as from2lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_3 on " & tablename & ".from2 = stations_3.id)"
  s = replace(s, " from ", ", cstr('' & stations_4.title) as to2lookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_4 on " & tablename & ".to2 = stations_4.id)"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","workercourses.id,workercourses.shiftids,workercourses.courseid,workercourses.days,workercourses.from1,workercourses.to1,workercourses.workerid,workercourses.courseid2,workercourses.from2,workercourses.to2",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

'[  if session(tablename & "_courseid") <> "" and session(tablename & "containerfield") <> "courseid" then s = s & " and " & tablename & ".courseid = " & session(tablename & "_courseid")
  t = session(tablename & "_courseidfilter") : if t <> "" then s = s & " and " & tablename & ".courseid in(" & getidlist("select id from courses where code = '" & t & "'") & "-1)"
']
'[  if session(tablename & "_from1") <> "" and session(tablename & "containerfield") <> "from1" then s = s & " and " & tablename & ".from1 = " & session(tablename & "_from1")
  t = session(tablename & "_from1filter") : if t <> "" then s = s & " and " & tablename & ".from1 in(" & getidlist("select id from stations where title like '%" & t & "%'") & "-1)"
']
'[  if session(tablename & "_to1") <> "" and session(tablename & "containerfield") <> "to1" then s = s & " and " & tablename & ".to1 = " & session(tablename & "_to1")
  t = session(tablename & "_to1filter") : if t <> "" then s = s & " and " & tablename & ".to1 in(" & getidlist("select id from stations where title like '%" & t & "%'") & "-1)"
']
'[  if session(tablename & "_workerid") <> "" and session(tablename & "containerfield") <> "workerid" then s = s & " and " & tablename & ".workerid = " & session(tablename & "_workerid")
  t = session(tablename & "_workeridfilter") : if t <> "" then s = s & " and " & tablename & ".workerid in(" & getidlist("select id from workers where instr(firstname & ' ' & lastname & ' ' & code, '" & t & "') > 0") & "-1)"  
']
'[  if session(tablename & "_courseid2") <> "" and session(tablename & "containerfield") <> "courseid2" then s = s & " and " & tablename & ".courseid2 = " & session(tablename & "_courseid2")
  t = session(tablename & "_courseid2filter") : if t <> "" then s = s & " and " & tablename & ".courseid2 in(" & getidlist("select id from courses where code = '" & t & "'") & "-1)"
']
'[  if session(tablename & "_from2") <> "" and session(tablename & "containerfield") <> "from2" then s = s & " and " & tablename & ".from2 = " & session(tablename & "_from2")
  t = session(tablename & "_from2filter") : if t <> "" then s = s & " and " & tablename & ".from2 in(" & getidlist("select id from stations where title like '%" & t & "%'") & "-1)"
']
'[  if session(tablename & "_to2") <> "" and session(tablename & "containerfield") <> "to2" then s = s & " and " & tablename & ".to2 = " & session(tablename & "_to2")
  t = session(tablename & "_to2filter") : if t <> "" then s = s & " and " & tablename & ".to2 in(" & getidlist("select id from stations where title like '%" & t & "%'") & "-1)"
']

  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        t = "select top 50 id from shifts where (cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".shiftids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        s = s & " cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".days like '%" & w & "%' or"
        s = s & " cstr('' & stations_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_2.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & workers_1.firstname) & ' ' & cstr('' & workers_1.lastname) & ' ' & cstr('' & workers_1.code) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & courses_2.code) & ' ' & cstr('' & courses_2.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_3.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_4.title) & ' ' like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
'[  s = s & " order by " & session(tablename & "_order")
  if instr(session(tablename & "_order"), "shiftids") = 0 then session(tablename & "_order") = "workercourses.courseid"
  s = s & " order by " & session(tablename & "_order")
']
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("shiftids") then rr("<td class=list_title><nobr>~workercourses_shiftids~")
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    f = "courses_1.code" : f2 = "courseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workercourses_courseid~</a> " & a2)
  end if
  if oksee("days") then f = "workercourses.days" : f2 = "days" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("days") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workercourses_days~</a> " & a2)
  if oksee("from1") and session(tablename & "containerfield") <> "from1" then
    f = "stations_1.title" : f2 = "from1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workercourses_from1~</a> " & a2)
  end if
  if oksee("to1") and session(tablename & "containerfield") <> "to1" then
    f = "stations_2.title" : f2 = "to1lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workercourses_to1~</a> " & a2)
  end if
  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then
    f = "workers_1.firstname" : f2 = "workeridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workercourses_workerid~</a> " & a2)
  end if
  if oksee("courseid2") and session(tablename & "containerfield") <> "courseid2" then
    f = "courses_2.code" : f2 = "courseid2lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workercourses_courseid2~</a> " & a2)
  end if
  if oksee("from2") and session(tablename & "containerfield") <> "from2" then
    f = "stations_3.title" : f2 = "from2lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workercourses_from2~</a> " & a2)
  end if
  if oksee("to2") and session(tablename & "containerfield") <> "to2" then
    f = "stations_4.title" : f2 = "to2lookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~workercourses_to2~</a> " & a2)
  end if

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Theshiftids = rs("shiftids") : Theshiftids = formatidlist(Theshiftids)
      Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
      Thecourseid = replace(thecourseid,",",".")
      Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
      Thefrom1 = rs("from1") : if isnull(Thefrom1) then Thefrom1 = 0
      Thefrom1 = replace(thefrom1,",",".")
      Theto1 = rs("to1") : if isnull(Theto1) then Theto1 = 0
      Theto1 = replace(theto1,",",".")
      Theworkerid = rs("workerid") : if isnull(Theworkerid) then Theworkerid = 0
      Theworkerid = replace(theworkerid,",",".")
      Thecourseid2 = rs("courseid2") : if isnull(Thecourseid2) then Thecourseid2 = 0
      Thecourseid2 = replace(thecourseid2,",",".")
      Thefrom2 = rs("from2") : if isnull(Thefrom2) then Thefrom2 = 0
      Thefrom2 = replace(thefrom2,",",".")
      Theto2 = rs("to2") : if isnull(Theto2) then Theto2 = 0
      Theto2 = replace(theto2,",",".")
      Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
      Thefrom1lookup = rs("from1lookup") : if isnull(Thefrom1lookup) then Thefrom1lookup = ""
      Theto1lookup = rs("to1lookup") : if isnull(Theto1lookup) then Theto1lookup = ""
      Theworkeridlookup = rs("workeridlookup") : if isnull(Theworkeridlookup) then Theworkeridlookup = ""
      Thecourseid2lookup = rs("courseid2lookup") : if isnull(Thecourseid2lookup) then Thecourseid2lookup = ""
      Thefrom2lookup = rs("from2lookup") : if isnull(Thefrom2lookup) then Thefrom2lookup = ""
      Theto2lookup = rs("to2lookup") : if isnull(Theto2lookup) then Theto2lookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
'[      firstcoltitle = theshiftids
      t = ""
      sq = "select title from shifts where id in(" & theshiftids & "-1) order by title"
      set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & rs1("title") & ", "
        rs1.movenext
      loop
      if t <> "" then t = left(t, len(t)-2)
      if len(t) > 50 then t = left(t,50) & ".."
      firstcoltitle = t
']
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/upcoming-work.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_item valign=top align=><nobr>" & thecourseidlookup)
'[      if oksee("days") then rr("<td class=list_item valign=top align=><nobr>" & thedays)
      t = thedays : if t <> "" then t = "," & t : t = left(t, len(t)-1) : t = replace(t, ",", "~, ~weekday") & "~" : t = mid(t,4)
      if oksee("days") then rr("<td class=list_item valign=top align=><nobr>" & t)
']
      if oksee("from1") and session(tablename & "containerfield") <> "from1" then rr("<td class=list_item valign=top align=><nobr>" & thefrom1lookup)
      if oksee("to1") and session(tablename & "containerfield") <> "to1" then rr("<td class=list_item valign=top align=><nobr>" & theto1lookup)
      if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then rr("<td class=list_item valign=top align=><nobr>" & theworkeridlookup)
      if oksee("courseid2") and session(tablename & "containerfield") <> "courseid2" then rr("<td class=list_item valign=top align=><nobr>" & thecourseid2lookup)
      if oksee("from2") and session(tablename & "containerfield") <> "from2" then rr("<td class=list_item valign=top align=><nobr>" & thefrom2lookup)
      if oksee("to2") and session(tablename & "containerfield") <> "to2" then rr("<td class=list_item valign=top align=><nobr>" & theto2lookup)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_total1>") 'courseid
  if oksee("days") and session(tablename & "containerfield") <> "days" then rr("<td class=list_total1>") 'days
  if oksee("from1") and session(tablename & "containerfield") <> "from1" then rr("<td class=list_total1>") 'from1
  if oksee("to1") and session(tablename & "containerfield") <> "to1" then rr("<td class=list_total1>") 'to1
  if oksee("workerid") and session(tablename & "containerfield") <> "workerid" then rr("<td class=list_total1>") 'workerid
  if oksee("courseid2") and session(tablename & "containerfield") <> "courseid2" then rr("<td class=list_total1>") 'courseid2
  if oksee("from2") and session(tablename & "containerfield") <> "from2" then rr("<td class=list_total1>") 'from2
  if oksee("to2") and session(tablename & "containerfield") <> "to2" then rr("<td class=list_total1>") 'to2

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
