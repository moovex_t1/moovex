<%
  v = session(tablename & "_period")
  if v = "day" then session(tablename & "_date1from") = date & " 00:00:00" : session(tablename & "_date1to") = date & " 23:59:59"
  if v = "week" then d = dateadd("d", - weekday(date) + 1, date) : session(tablename & "_date1from") = d & " 00:00:00" : session(tablename & "_date1to") = dateadd("d", 6, d) & " 23:59:59"
  if v = "month" then d = dateadd("d", - day(date) + 1, date) : session(tablename & "_date1from") = d & " 00:00:00" : session(tablename & "_date1to") = lastday(month(d) & "/" & year(d)) & "/" & month(d) & "/" & year(d) & " 23:59:59"
  if v = "year" then session(tablename & "_date1from") = "1/1/" & year(date) & " 00:00:00" : session(tablename & "_date1to") = "31/12/" & year(date) & " 23:59:59"
  if v = "bill" then
    d = dateadd("m", -1, "1/" & month(date) & "/" & year(date))
    session(tablename & "_date1from") = d & " 00:00:00"
    session(tablename & "_date1to") = lastday(month(d) & "/" & year(d)) & "/" & month(d) & "/" & year(d) & " 23:59:59"
    session(tablename & "_statuses") = "Done,Done Approved,"
  end if
  if v = "bill2" then
    d = dateadd("m", -1, "21/" & month(date) & "/" & year(date))
    session(tablename & "_date1from") = d & " 00:00:00"
    d = "20/" & month(date) & "/" & year(date)
    session(tablename & "_date1to") = d & " 23:59:59"
    session(tablename & "_statuses") = "Done,Done Approved,"
  end if
  if left(v,7) = "weekday" then
    i = mid(v,8)
    d = dateadd("d", - weekday(date) + i, date)
    session(tablename & "_date1from") = d & " 00:00:00" : session(tablename & "_date1to") = d & " 23:59:59"
  end if
  v = session(tablename & "_relhour1")
  if v <> "" then
    t = session(tablename & "_date1from")
    t = left(t & " ", instr(t & " ", " ") - 1) & " 00:00:00"
    t = dateadd("d", left(v,1) - 1, t)
    t = dateadd("h", mid(v,3), t)
    session(tablename & "_date1from") = t
  end if
  v = session(tablename & "_relhour2")
  if v <> "" then
    t = session(tablename & "_date1from")
    t = left(t & " ", instr(t & " ", " ") - 1) & " 00:59:59"
    t = dateadd("d", left(v,1) - 1 , t)
    t = dateadd("h", mid(v,3), t)
    session(tablename & "_date1to") = t
  end if
  rr("<table class=list_table width=100% cellspacing=1 cellpadding=5 align=~langside~>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/payment-card.png' ~iconsize~> ~orders_orders~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  if specs = "leumi" and hasmatch("manager,worker,", session("usergroup")) then
    'dont show shift tabs
  elseif getparam("ShowShiftFilterTabs") <> "n" then
    sty  = "color:#000000; background:#eeeeee; border:1px solid #cccccc; padding:4; padding-top:7; height:32; white-space:nowrap; font-weight:bold;"
    sty2 = "color:#800000; background:#dddddd; border:1px solid #cccccc; padding:4; padding-top:7; height:38; white-space:nowrap; font-weight:bold; margin-bottom:-9; position:absolute; border-bottom:#dddddd 1px solid;"
    rr "<tr><td colspan=99 bgcolor1=#ff0000 style='padding:2;'>"
    title = "�� �������"
    t = sty : if instr(scriptname,"admin_orders.asp") > 0 and session("ordersview") = "all" then t = sty2
    rr "<a href=admin_orders.asp?ordersview=all style='" & t & "'><span style='width1:110; overflow-x:hidden; cursor:hand;'><nobr>" & title & "</nobr></span></a>"
    if t = sty2 then rr "<span style='" & sty & "'>" & title & "</span>" 'just to fill the space
    rr "&nbsp;&nbsp;"
    v = getparam("ShowShiftFilterTabs") : v = formatidlist(v)
    if v <> "" then
      ii = v
    else
      s = "select id,title from shifts where 0=0"
      if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
      s = s & " order by hour1b, hour1, title"
      ii = getidlist(s)
    end if
    do until ii = ""
      i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
      title = gettitle("shifts", i)
      t = sty : if instr(scriptname,"admin_orders.asp") > 0 and session("ordersview") = "shiftid" & i then t = sty2
      rr "<a href=admin_orders.asp?ordersview=shiftid" & i & " style='" & t & "'><span style='width1:110; overflow-x:hidden; cursor:hand;'><nobr>" & title & "</nobr></span></a>"
      if t = sty2 then rr "<span style='" & sty & "'>" & title & "</span>" 'just to fill the space
      rr "&nbsp;&nbsp;"
    loop
  end if

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  rr("<input type=hidden name=action value=search>")
  rr("<input type=hidden name=action2>")
  rr("<input type=hidden name=action3>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")

  rr(" <nobr><img id=status_filtericon src=icons/calendar.png style='filter:alpha(opacity=50); margin-top:8;'> ")
  rr "<select name=period>"
  rr "<option value='' style='color:bbbbfe;'>~Period~"
  rr "<option value=day" : if session("orders_period") = "day" then rr " selected"
  rr ">~Today~"
  rr "<option value=week" : if session("orders_period") = "week" then rr " selected"
  rr ">~This Week~"
  rr "<option value=month" : if session("orders_period") = "month" then rr " selected"
  rr ">~This Month~"
  rr "<option value=year" : if session("orders_period") = "year" then rr " selected"
  rr ">~This Year~"
  for i = 1 to 7
    rr "<option value=weekday" & i : if session("orders_period") = "weekday" & i then rr " selected"
    rr ">~weekday" & i & "~ ~This Week~"
  next
  rr "<option value=bill" : if session("orders_period") = "bill" then rr " selected"
  rr ">���� �����"
  if specs = "iscar" then rr "<option value=bill2" : rr iif(session("orders_period") = "bill2", " selected", "") : rr ">���� �������"
  rr "</select></nobr>"

  if session(tablename & "_advanced") = "y" and getparam("SearchOrderDatesWithHours") = "y" then
    rr(" <nobr><img id=status_filtericon src=icons/i_clock.gif style='filter:alpha(opacity=50); margin-top:8;'> ")
    rr "<select name=relhour1>"
    rr "<option value='' style='color:bbbbfe;'>~Hour~"
    for j = 1 to 2
      if j = 1 then d = "~Today~"
      if j = 2 then d = "~Tomorrow~"
      for i = 0 to 23
        rr "<option value=" & j & "." & i : if session("orders_relhour1") = j & "." & i then rr " selected"
        rr ">" & i & ":00 " & d
      next
    next
    rr "</select>"
    rr " - <select name=relhour2>"
    rr "<option value='' style='color:bbbbfe;'>~Hour~"
    for j = 1 to 2
      if j = 1 then d = "~Today~"
      if j = 2 then d = "~Tomorrow~"
      for i = 0 to 23
        rr "<option value=" & j & "." & i : if session("orders_relhour2") = j & "." & i then rr " selected"
        rr ">" & i & ":59 " & d
      next
    next
    rr "</select>"
  end if


  t = "" : if getparam("SearchOrderDatesWithHours") = "y" then t = ":"
  rr("<nobr> <span style='width:20;'> ~From~ </span>")
  rr(SelectDate("f2.date1from" & t, session(tablename & "_date1from")))
  rr("<span style='width:20;'> ~To~ </span>")
  rr(SelectDate("f2.date1to" & t, session(tablename & "_date1to")))
  rr "</nobr>"

  '-----shift and date filters-----------
  if session(tablename & "_advanced") = "y" then
    if specs = "iai" then
      rr "��� ����� �����<input type=hidden name=bysentdate1 value=on><input type=checkbox name=bysentdate" : if session(tablename & "_bysentdate") = "on" then rr " checked>" else rr ">"
    end if

    rr "<nobr> ~Week Day~ <select name=weekdayfilter>"
    rr("<option value='' style='color:bbbbfe;'>~Day~")
    for i = 1 to 7
      rr "<option value=" & i
      if session(tablename & "_weekdayfilter") = cstr(i) then rr " selected"
      rr ">~weekday" & i & "~"
    next
    rr "</select></nobr>"

    if specs = "iscar" then
      rr "<nobr>"
      for i = 1 to 2
        sc = ""
        if i = 1 then f = "hour1" : n = "date2collect" : display = "~Collect~"
        if i = 2 then f = "hour1b" : n = "date2distribute" : display = "~Distribute~"
        sq = "select id,title," & f & " from shifts"
        if session("usersiteid") <> "0" then sq = sq & " where siteid = " & session("usersiteid")
        sq = sq & " order by title"
        set rs1 = conn.execute(sqlfilter(sq))
        rr "<input type=hidden name=" & n & " value='" & session(tablename & "_" & n) & "'>"
        rr " " & display & " <select name=" & n & "shiftid onclick='vbscript: " & n & "set'>"
        rr("<option value='' style='color:bbbbfe;'>" & display)
        do until rs1.eof
          rr "<option value=" & rs1("id")
          if session(tablename & "_" & n & "shiftid") = cstr(rs1("id")) then rr " selected"
          rr ">" & rs1("title")
          sc = sc & "if f2." & n & "shiftid.value = " & rs1("id") & " then f2." & n & ".value = """ & rs1(f) & """" & vbcrlf
          rs1.movenext
        loop
        rr "</select>"
        rr "<script language=vbscript>"
        rr "sub " & n & "set" & vbcrlf
        rr sc
        rr "end sub" & vbcrlf
        rr "</script>"
      next
      rr "</nobr>"
    else
      rr "<nobr>"
      for i = 1 to 2
        if i = 1 then f = "hour1" : n = "date2collect" : display = "~Collect~"
        if i = 2 then f = "hour1b" : n = "date2distribute" : display = "~Distribute~"
        sq = "select " & f & " from shifts"
        if session("usersiteid") <> "0" then sq = sq & " where siteid = " & session("usersiteid")
        sq = sq & " order by " & f
        set rs1 = conn.execute(sqlfilter(sq))
        rr " " & display & " <select name=" & n & ">"
        rr("<option value='' style='color:bbbbfe;'>" & display)
        already = ""
        do until rs1.eof
          h = rs1(f)
          if instr("," & already, "," & h & ",") = 0 then
            already = alreay & h & ","
            rr "<option value=" & h
            if session(tablename & "_" & n) = cstr(h) then rr " selected"
            rr ">" & h
          end if
          rs1.movenext
        loop
        rr "</select>"
      next
      rr "</nobr>"
    end if
  end if
  '--------

  rr("</nobr>")

  'rr(" <nobr><img id=status_filtericon src=icons/plus.png style='filter:alpha(opacity=50); margin-top:8;'>")
  'rr(" <select name=status dir=~langdir~ style='width:150;'>")
  'rr("<option value='' style='color:bbbbfe;'>~orders_status~")
  'b = "Pending"
  'rr("<option value='" & b & "' style='color:ff0000;'")
  'if session(tablename & "_status") = b then rr(" selected")
  'rr(">" & b)
  'if donestatus = "Done Approved" then
  '  b = "Done/Done Approved"
  '  rr("<option value='" & b & "' style='color:ff0000;'")
  '  if session(tablename & "_status") = b then rr(" selected")
  '  rr(">~" & b & "~")
  'end if
  'aa = "New,Approved,Rejected,Sent,Received,Done,Done Approved,Canceled," : a = aa
  'if session("usergroup") = "supplier" then a = getparam("SupplierViewOrderStatuses")
  'if donestatus = "Done" then a = replace(a, ",Done Approved,", ",")
  'do until a = ""
  '  b = left(a,instr(a,",")-1)
  '  a = mid(a,instr(a,",")+1)
  '  rr("<option value=""" & b & """")
  '  if session(tablename & "_status") = b then rr(" selected")
  '  rr(">" & b)
  'loop
  'rr("</select></nobr>")'search:status

  rr(" <nobr><img id=status_filtericon src=icons/plus.png style='filter:alpha(opacity=50); margin-top:8;'> ")
  a = "Pending,New,Approved,Rejected,Sent,Received,Done,Done Approved,Canceled,"
  if session("usergroup") = "supplier" then a = getparam("SupplierViewOrderStatuses")
  if donestatus = "Done" then a = replace(a, ",Done Approved,", ",")
  if usingsuppliersystem = "on" then a = replace(a, ",Sent,", ",Sent,Sent_No_Signal,") : a = replace(a, ",Canceled,", ",Canceled,Canceled_No_Signal,")
  t = session(tablename & "_statuses") : t = bothmatch(t, a)
  rr multiselect("statuses", "~orders_status~", a, t, 150)
  rr "</nobr>"

if session(tablename & "_advanced") = "y" then

  'if specs = "iai" then
  '  rr(" <nobr><img id=suppliersystemstatus_filtericon src=icons/publish.png style='filter:alpha(opacity=50); margin-top:8;'>")
  '  rr(" <select name=suppliersystemstatus dir=~langdir~ style='width:150;'>")
  '  rr("<option value='' style='color:bbbbfe;'>~orders_suppliersystemstatus~")
  '  a = "None,Sent,Received,Done,Canceled,"
  '  do until a = ""
  '    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  '    rr("<option value=""" & b & """")
  '    if session(tablename & "_suppliersystemstatus") = b then rr(" selected")
  '    rr(">~" & b & "~")
  '  loop
  '  rr("</select></nobr>")'search:status
  'end if

  rr("&nbsp;<nobr><img id=manually_filtericon src=flag1.png style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=manually dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~orders_manually~")
  rr("<option value=on") : if lcase(session(tablename & "_manually")) = "on" then rr(" selected")
  rr("> ~orders_manually~: ~Yes~")
  rr("<option value=no") : if lcase(session(tablename & "_manually")) = "no" then rr(" selected")
  rr("> ~orders_manually~: ~No~")
  rr("</select></nobr>")'search:manually
  rr(" <nobr><img id=starred_filtericon src=st1.png style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=starred dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~orders_starred~")
  rr("<option value=on") : if lcase(session(tablename & "_starred")) = "on" then rr(" selected")
  rr("> ~orders_starred~: ~Yes~")
  rr("<option value=no") : if lcase(session(tablename & "_starred")) = "no" then rr(" selected")
  rr("> ~orders_starred~: ~No~")
  rr("</select></nobr>")'search:starred

  rr(" <nobr><img id=ordertypeids_filtericon src=icons/order-1.png style='filter:alpha(opacity=50); margin-top:8;'>")
  sq = "select * from ordertypes order by title"
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  a = "-1:(~None~),"
  do until rs1.eof
    a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
    rs1.movenext
  loop
  rr " " & multiselect("ordertypeids", "~orders_ordertypeid~", a, session(tablename & "_ordertypeids"), 150)
  rr "</nobr>"

  '---disabled - too slow
  'if specs = "rail" then
  '  rr(" <nobr><img id=workertypeids_filtericon src=icons/finished-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
  '  sq = "select * from workertypes order by title"
  '  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  '  a = ""
  '  do until rs1.eof
  '    a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
  '    rs1.movenext
  '  loop
  '  rr " " & multiselect("workertypeids", "~workers_workertypeid~", a, session(tablename & "_workertypeids"), 150)
  'end if

  session(tablename & "_reasonids") = formatidlistunique(session(tablename & "_reasonids"))
  rr(" <nobr><img src=icons/bookmark.png style='filter:alpha(opacity=50);'> ")
  sq = "select * from reasons order by title"
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  a = "-1:(~None~),"
  do until rs1.eof
    a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
    rs1.movenext
  loop
  rr multiselect("reasonids", "~orders_reasonid~", a, session(tablename & "_reasonids"), 150)
  rr "</nobr>"
  rr(" <nobr><img id=direction_filtericon src=icons/i_redo.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=direction dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~orders_direction~")
  aa = "A,B,E," : a = aa
  'sq = "select distinct direction from orders order by direction"
  'set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof : a = a & rs1("direction") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if session(tablename & "_direction") = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select></nobr>")'search:direction

  'rr(" <nobr><img id=courseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
  'sq = "select * from courses where (code <> '' or title <> '')"
  'if session("usersiteid") <> "0" then sq = sq & " and (siteid = " & session("usersiteid") & " or siteid = 0)"
  'sq = sq & " order by code1,title"
  'rr(" <select name=courseid dir=~langdir~ onfocus='vbscript: if f2.courseidpopulated.value = """" then f2.courseidpopulated.value = ""1"" : document.getelementbyid(""frpopcourseid"").src=""?action=popselect&box=f2.courseid&display=" & replace(translate("~orders_courseid~")," ","+") & "&fields=code,title,&table=courses&courseid=" & session(tablename & "_courseid") & "&append=on&specialquery=" & encrypt(sq,"3") & """'>")
  'rr("<option value='' style='color:bbbbfe;'>~orders_courseid~")
  'rr "<option value='0' style='color:#0000ff;'"
  'if cstr(session(tablename & "_courseid")) = "0" then rr(" selected")
  'rr ">~Special ride with no course~"
  'rr "<option value='-77' style='color:0000bb;'"
  'if cstr(session(tablename & "_courseid")) = "-77" then rr(" selected")
  'rr ">~Course ride - all courses~"
  't = cstr(session(tablename & "_courseid")) : if t <> "" and t <> "0" and t <> "-77" then rr "<option value=" & t & " selected>" & getcode("courses", t) & " " & gettitle("courses", t)
  'rr("</select></nobr>")'search:courseid
  'rr "<iframe id=frpopcourseid width=0 height=0 frameborder=0></iframe>"
  'rr "<input type=hidden name=courseidpopulated>"

  rr(" <nobr><img id=courseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'> ~orders_courseid~ ")
  rr " <img src=icons/i_help.gif style='filter:alpha(opacity=50); margin-top:8;' alt='~orders_course_search_help~'> "
  rr "<input type=text name=course style='width:80; direction:ltr;' value=""" & session(tablename & "_course") & """>"

  if specs = "shufersal" then
    rr(" <nobr><img id=fromtofilter_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'> ����\��� ")
    rr "<input type=text name=fromtofilter style='width:80;' value=""" & session(tablename & "_fromtofilter") & """>"
  end if

  rr(" <nobr><img id=from1filter_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'> ~orders_from1~ ")
  rr "<input type=text name=from1filter style='width:80;' value=""" & session(tablename & "_from1filter") & """>"

  rr(" <nobr><img id=to1filter_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'> ~orders_to1~ ")
  rr "<input type=text name=to1filter style='width:80;' value=""" & session(tablename & "_to1filter") & """>"

  rr(" <nobr><img id=via1filter_filtericon src=icons/i_home.gif style='filter:alpha(opacity=50); margin-top:8;'> ~orders_via1~ ")
  rr "<input type=text name=via1filter style='width:80;' value=""" & session(tablename & "_via1filter") & """>"

  rr(" <nobr><img id=courserateidfilter_filtericon src=icons/cost.png style='filter:alpha(opacity=50); margin-top:8;'> ~Course Rate~ ")
  rr " <img src=icons/i_help.gif style='filter:alpha(opacity=50); margin-top:8;' alt='n = ��� ������'> "
  rr "<input type=text name=courserateidfilter style='width:80; direction:ltr;' value=""" & session(tablename & "_courserateidfilter") & """>"
  if session(tablename & "containerfield") <> "supplierid" then
    session(tablename & "_supplierid") = formatidlistunique(session(tablename & "_supplierid"))
    rr(" <nobr><img id=supplierid_filtericon src=icons/i_key.gif style='filter:alpha(opacity=50);'> ")
    sq = "select * from suppliers"
    if session("usersiteid") <> "0" then sq = sq & " where (siteid = " & session("usersiteid") & " or siteid = 0)"
    sq = sq & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    a = "0:(~None~),"
    do until rs1.eof
      a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
      rs1.movenext
    loop
    rr multiselect("supplierid", "~orders_supplierid~", a, session(tablename & "_supplierid"), 150)
    rr "</nobr>"
  end if

  if session(tablename & "containerfield") <> "carid" then
    rr(" <nobr><img id=carid_filtericon src=icons/i_car.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    'rr(" <select style='width:150;' name=carid dir=~langdir~>")
    'rr("<option value='' style='color:bbbbfe;'>~orders_carid~")
    'sq = "select * from cars"
    'if session("usersiteid") <> "0" then sq = sq & " and (siteid = " & session("usersiteid") & " or siteid = 0)"
    'if instr(",supplier,supplieradmin,", "," & session("usergroup") & ",") > 0 then sq = sq & " and supplierid in(" & session("usersupplierids") & "-1)"
    'sq = replace(sq," and "," where ",1,1,1)
    'sq = sq & " order by title,code"
    'closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    'do until rs1.eof
    '  rr("<option value=""" & rs1("id") & """")
    '  if cstr(session(tablename & "_carid")) = cstr(rs1("id")) then rr(" selected")
    '  rr(">")
    '  rr(rs1("title") & " ")
    '  rr(rs1("code") & " ")
    '  rs1.movenext
    'loop
    'rr("</select></nobr>")'search:carid

    rr " ~orders_carid~ <input type=text name=caridfilter style='width:80;' value=""" & session(tablename & "_caridfilter") & """>"
  end if'search:carid
  if session(tablename & "containerfield") <> "driverid" then
    rr(" <nobr><img id=driverid_filtericon src=icons/special-offer.png style='filter:alpha(opacity=50); margin-top:8;'>")

    'sq = "select * from drivers"
    'if session("usersiteid") <> "0" then sq = sq & " and (siteid = " & session("usersiteid") & " or siteid = 0)"
    'if instr(",supplier,supplieradmin,", "," & session("usergroup") & ",") > 0 then sq = sq & " and supplierid in(" & session("usersupplierids") & "-1)"
    'sq = replace(sq," and "," where ",1,1,1)
    'sq = sq & " order by title"
    'rr(" <select name=driverid dir=~langdir~ onfocus='vbscript: if f2.driveridpopulated.value = """" then f2.driveridpopulated.value = ""1"" : document.getelementbyid(""frpopdriverid"").src=""?action=popselect&box=f2.driverid&display=" & replace(translate("~orders_driverid~")," ","+") & "&fields=title,&table=drivers&driverid=" & session(tablename & "_driverid") & "&append=on&specialquery=" & encrypt(sq,"3") & """'>")
    'rr("<option value='' style='color:bbbbfe;'>~orders_driverid~")
    't = cstr(session(tablename & "_driverid")) : if t <> "" and t <> "0" then rr "<option value=" & t & " selected>" & gettitle("drivers", t)
    'rr("</select></nobr>")'search:driverid
    'rr "<iframe id=frpopdriverid width=0 height=0 frameborder=0></iframe>"
    'rr "<input type=hidden name=driveridpopulated>"
    if instr(",sheleg,ap,", "," & specs & ",") > 0 then
      rr(" <select style='width:150;' name=driverid dir=~langdir~>")
      rr("<option value='' style='color:bbbbfe;'>~orders_driverid~")
      rr "<option value=0 style='color:800000;'" : if session(tablename & "_driverid") = "0" then rr " selected"
      rr ">��� ���"
      sq = "select * from drivers"
      if session("usersiteid") <> "0" then sq = sq & " and (siteid = " & session("usersiteid") & " or siteid = 0)"
      if instr(",supplier,supplieradmin,", "," & session("usergroup") & ",") > 0 then sq = sq & " and supplierid in(" & session("usersupplierids") & "-1)"
      sq = replace(sq," and "," where ",1,1,1)
      sq = sq & " order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        rr("<option value=""" & rs1("id") & """")
        if cstr(session(tablename & "_driverid")) = cstr(rs1("id")) then rr(" selected")
        rr ">" & rs1("title")
        rs1.movenext
      loop
      rr("</select></nobr>")
    else
      rr " ~orders_driverid~ <input type=text name=driveridfilter style='width:80;' value=""" & session(tablename & "_driveridfilter") & """>"
    end if
  end if'search:driverid
  rr(" <nobr><img id=additionids_filtericon src=icons/plus.png style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=additionidsfilter dir=~langdir~>")

  rr("<option value='' style='color:bbbbfe;'>~orders_additionids~")
  rr "<option value='a'" : if cstr(session(tablename & "_siteid")) = "a" then rr " selected"
  rr ">(~Any~)"
  rr "<option value='0'" : if cstr(session(tablename & "_siteid")) = "0" then rr " selected"
  rr ">(~None~)"

  sq = "select * from additions order by title"
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  do until rs1.eof
    rr("<option value=""" & rs1("id") & """")
    if cstr(session(tablename & "_additionidsfilter")) = cstr(rs1("id")) then rr(" selected")
    rr(">")
    rr(rs1("title") & " ")
    rr(rs1("addition") & " ")
    rs1.movenext
  loop
  rr("</select></nobr>")'search:additionidsfilter
  if session(tablename & "containerfield") <> "userid" then
    'rr(" <select style='width:150;' name=userid dir=~langdir~>")
    'rr("<option value='' style='color:bbbbfe;'>~orders_userid~")
    'rr "<option value='-1' style='color:ff0000;'"
    'if session(tablename & "_userid") = "-1" then rr " selected"
    'rr ">~Worker Request~"
    'sq = "select * from buusers"
    'if session("usersiteid") <> "0" then sq = sq & " where (siteid = " & session("usersiteid") & " or siteid = 0)"
    'sq = sq & " order by username"
    'closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    'do until rs1.eof
    '  rr("<option value=""" & rs1("id") & """")
    '  if cstr(session(tablename & "_userid")) = cstr(rs1("id")) then rr(" selected")
    '  rr(">")
    '  rr(rs1("username") & " ")
    '  rs1.movenext
    'loop
    'rr("</select></nobr>")'search:userid

    'sq = "select * from buusers"
    'if session("usersiteid") <> "0" then sq = sq & " where (siteid = " & session("usersiteid") & " or siteid = 0)"
    'sq = sq & " order by username"
    'rr(" <select name=userid dir=~langdir~ onfocus='vbscript: if f2.useridpopulated.value = """" then f2.useridpopulated.value = ""1"" : document.getelementbyid(""frpopuserid"").src=""?action=popselect&box=f2.userid&display=" & replace(translate("~orders_userid~")," ","+") & "&fields=username,&table=buusers&userid=" & session(tablename & "_userid") & "&append=on&specialquery=" & encrypt(sq,"3") & """'>")
    'rr("<option value='' style='color:bbbbfe;'>~orders_userid~")
    'rr "<option value='-1' style='color:ff0000;'"
    'if session(tablename & "_userid") = "-1" then rr " selected"
    'rr ">~Worker Request~"
    't = cstr(session(tablename & "_userid")) : if t <> "" and t <> "-1" then rr "<option value=" & t & " selected>" & getuser(t)
    'rr("</select></nobr>")'search:userid
    'rr "<iframe id=frpopuserid width=0 height=0 frameborder=0></iframe>"
    'rr "<input type=hidden name=useridpopulated>"

    rr " <nobr><img id=userid_filtericon src=icons/hire-me.png style='filter:alpha(opacity=50); margin-top:8;'> ~orders_userid~"
    rr " <img src=icons/i_help.gif style='filter:alpha(opacity=50); margin-top:8;' alt='~orders_userid_search_help~'> "
    rr " <input type=text name=userid style='width:100;' value=""" & session(tablename & "_userid") & """></nobr>"


  end if'search:userid

  'if session("usersiteid") = "0" and session(tablename & "containerfield") <> "siteid" then
  '  rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
  '  rr(" <select style='width:150;' name=siteid dir=~langdir~>")
  '  rr("<option value='' style='color:bbbbfe;'>~orders_siteid~")
  '  rr "<option value='0'" : if cstr(session(tablename & "_siteid")) = "0" then rr " selected"
  '  rr ">(~None~)"
  '  sq = "select * from sites order by title"
  '  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  '  do until rs1.eof
  '    rr("<option value=""" & rs1("id") & """")
  '    if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
  '    rr(">")
  '    rr(rs1("title") & " ")
  '    rs1.movenext
  '  loop
  '  rr("</select></nobr>")'search:siteid
  'end if'search:siteid

  if session("usersiteid") = "0" then
    rr(" <nobr><img src=icons/i_earth.gif style='filter:alpha(opacity=50);'> ")
    sq = "select id,title from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    a = "0:(~None~),"
    do until rs1.eof
      a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
      rs1.movenext
    loop
    rr multiselect("siteids", "~orders_siteid~", a, session(tablename & "_siteids"), 150)
  end if

  if specs = "teva" then

    if session("sitecountries") = "" then
      s = "select id,data1 from sites"
      set rs = conn.execute(sqlfilter(s))
      do until rs.eof
        d = rs("data1")
        t = tocsv(getval(d, "country")) : if instr("," & session("sitecountries"), "," & t & ",") = 0 then session("sitecountries") = session("sitecountries") & t & ","
        t = tocsv(getval(d, "region")) : if instr("," & session("siteregions"), "," & t & ",") = 0 then session("siteregions") = session("siteregions") & t & ","
        rs.movenext
       loop
    end if

    rr "<select name=sitecountry>"
    rr "<option value='' style='color:#bbbbff;'>~Country~"
    tt = session("sitecountries")
    do until tt = ""
      t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
      rr "<option value='" & t & "'" : if session("orders_sitecountry") = t then rr " selected"
      rr ">" & t
    loop
    rr "</select>"

    rr "&nbsp;<select name=siteregion>"
    rr "<option value='' style='color:#bbbbff;'>~Region~"
    tt = session("siteregions")
    do until tt = ""
      t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
      rr "<option value='" & t & "'" : if session("orders_siteregion") = t then rr " selected"
      rr ">" & t
    loop
    rr "</select>"
  end if
 
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"

  if instr(",shufersal,dexcel,brom,", "," & specs & ",") > 0 then
    rr(" <nobr><img id=unitid_filtericon src=icons/i_shield.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=unitid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~workers_unitid~")
    sq = "select * from units order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_unitid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")
  end if

  if oksee("production") then
    rr " <nobr><img src=icons/bookmark.png style='filter:alpha(opacity=50); margin-top:8;'>~Production~ "
    rr "<input type=text name=production style='width:60; direction:ltr;' value=""" & session(tablename & "_production") & """></nobr>"
  end if

  rr " <nobr><img src=icons/suppliers.png style='filter:alpha(opacity=50); margin-top:8;'> ~orders_pids~ "
  rr "<input type=text name=pidsfilter style='width:60;' value=""" & session(tablename & "_pidsfilter") & """></nobr>"
  rr " <nobr> ~orders_pcount~ "
  rr "<input type=text name=pcountfilter style='width:35;' value=""" & session(tablename & "_pcountfilter") & """></nobr>"
  rr " <img src=icons/i_help.gif style='filter:alpha(opacity=50); margin-top:8;' alt='3&nbsp;,3+,3-'> "

  rr " <nobr><img src=note.png style='filter:alpha(opacity=50); margin-top:8;'> ~orders_remarks~ "
  rr "<input type=text name=remarksfilter style='width:60;' value=""" & session(tablename & "_remarksfilter") & """></nobr>"
  rr " <img src=icons/i_help.gif style='filter:alpha(opacity=50); margin-top:8;' alt='a = any'> "

  if specs = "rail" then
    rr "<nobr> ������� "
    rr "<input type=text name=invoicefilter style='width:80;' value=""" & session(tablename & "_invoicefilter") & """></nobr>"
  end if

  if specs = "rafael" then
    v = session(tablename & "_allvalidfilter")
    rr " <nobr><img id=status_filtericon src=icons/i_notice.gif style='filter:alpha(opacity=50); margin-top:8;'> "
    rr "<select name=allvalidfilter><option value='' style='color:#bbbbff;'>�������"
    rr "<option value=on" : if v = "on" then rr " selected"
    rr ">�� �������"
    rr "<option value=no" : if v = "no" then rr " selected"
    rr ">��� �������"
    rr "</select></nobr>"
    v = session(tablename & "_reportedfilter")
    rr " <nobr><img id=status_filtericon src=icons/i_notice.gif style='filter:alpha(opacity=50); margin-top:8;'> "
    rr "<select name=reportedfilter><option value='' style='color:#bbbbff;'>��� ������"
    rr "<option value=on" : if v = "on" then rr " selected"
    rr ">���� ���"
    rr "<option value=no" : if v = "no" then rr " selected"
    rr ">��� ����"
    rr "</select></nobr>"
  end if

  if specs = "rafael" and session("ordersview") = "otid" & session("natbag_otid") then
    rr(" <nobr>���� ����� ")
    rr "<input type=text name=flightcompanyfilter style='width:50;' value=""" & session(tablename & "_flightcompanyfilter") & """>"
    rr(" <nobr>���� ���� ")
    rr "<input type=text name=flightcodefilter style='width:50;' value=""" & session(tablename & "_flightcodefilter") & """>"
    rr(" <nobr>��� �����/����� ")
    rr "<input type=text name=flighthourfilter style='width:50;' value=""" & session(tablename & "_flighthourfilter") & """>"
  end if

  if specs = "iai" then
    rr " <nobr><img src=icons/product-1.png style='filter:alpha(opacity=50); margin-top:8;'> ���� ����� "
    rr "<input type=text name=packagenumber style='width:80;' value=""" & session(tablename & "_packagenumber") & """></nobr>"
  end if

  if session("userlevel") = "5" then
    rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  else
    rr " <nobr>~Lines~ <select name=lines>"
    rr "<option value=20" : if session(tablename & "_lines") = "20" then rr " selected"
    rr ">20"
    rr "<option value=50" : if session(tablename & "_lines") = "50" then rr " selected"
    rr ">50"
    rr "<option value=100" : if session(tablename & "_lines") = "100" then rr " selected"
    rr ">100"
    rr "</select></nobr>"
  end if
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  rr "<input type=checkbox name=showdecomp" : if session(tablename & "_showdecomp") = "on" then rr " checked"
  rr(">��� ����� ����")
else'advanced
  rr "<input type=hidden name=ordertypeids value=" & session("orders_ordertypeids") & ">" 'for reportsocial
end if
  a = getparam("DefaultOrderDateFilter") : if countstring(a,",") <> 1 or strfilter(a, "-0123456789,") <> a or instr(a,",") < 2 or instr(a,",") = len(a) then a = "-7,31"
  d1 = dateadd("d", left(a,instr(a,",")-1) , date) & " 00:00:00" : d2 = dateadd("d", mid(a,instr(a,",")+1) , date) & " 23:59:59"
  if specs = "leumi" then d1 = now

  '---columns
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  t = replace(t, ",workerunits,", ",")
  t = replace(t, ",altstations,", ",")
  t = replace(t, ",history,", ",")
  't = replace(t, ",data1,", ",")
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")
  '---

  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear&date1from=" & d1 & "&date1to=" & d2 & """'> ")
  if instr(request_action2, "report") then
    rr(" <input type=button class=groovybutton id=buttonsearch value=""    ~Show~    "" onclick='vbscript: f2.action2.value = """ & request_action2 & """ : f2.action3.value = """ & request_action3 & """ : f2.submit'> ")
  else
    rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  end if
  if session(tablename & "_advanced") = "y" then rr(" <a href=?advanced=n><nobr>~Simple search~</nobr></a>")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  if session("buildchartid") <> "" then
    'rr "<input type=hidden name=action2>"
    rr("<tr><td colspan=99 bgcolor=cccccc><img src=icons/i_query.gif>")
    rr(" <input type=text name=querytitle style='width:120;' value=""" & session(tablename & "_querytitle") & """>")
    rr(" <input type=button class=groovybutton value='~Add Chart Query~' onclick=""javascript: if(f2.querytitle.value == '') {alert('~Please choose a name for this query~');} else {f2.action2.value = 'chartaddquery'; f2.submit();}"">")
    rr(" <input type=button class=groovybutton value='~Cancel~' onclick='javascript: document.location = ""admin_charts.asp?action=form&action2=cancelbuild&id=" & session("buildchartid") & """;'>")
  end if

  rr "<nobr><input type=hidden name=thumbs value='" & session(tablename & "_thumbs") & "'>"
  t1 = "cccccc" : t2 = "cccccc" : if session(tablename & "_thumbs") = "on" then t2 = "4444cc" else t1 = "4444cc"
  rr "&nbsp;&nbsp;&nbsp;<a href=#aa onclick='vbscript:f2.thumbs.value = ""no"" : f2.submit'><img src=viewlist.png alt='~List View~'style='border:1px solid #" & t1 & "; vertical-align:bottom; margin-top:3;'></a>"
  rr "&nbsp;&nbsp;&nbsp;<a href=#aa onclick='vbscript:f2.thumbs.value = ""on"" : f2.submit'><img src=viewthumbs.png alt='~Thumbnail View~' style='border:1px solid #" & t2 & "; vertical-align:bottom; margin-top:3;'></a>"
  'rr "&nbsp;&nbsp;&nbsp;<img src=viewform.png  style='border:1px solid #aaaaaa; vertical-align:top; margin-top:3;'>"
  rr "</nobr>"
  rr("</tr></form>")'search

  if instr(",admin,finance,", "," & session("usergroup") & ",") > 0 and request_action = "reportbuttons" then

    '----------
    aprefix = "<a style='border:1px solid #cccccc; width:100; height:150; padding:10; margin:5; text-align:center; overflow:hidden;' "
    rr "<tr><td colspan=99 valign=top align=center>"
    '----------
    rr "<div style='width:330; height1:170; vertical-align:top; display:inline; overflow:hidden; margin:30;'>"
    rr "<div style='height:30; background:#3973DC; color:#ffffff; padding:8; text-align:center; font-weight:bold;'>����� ������ ����� ���� �������</div>"
    rr "<div style='width:330; height1:130; overflow-y1:auto; overflow-x:hidden; text-align:~langside~; padding1:20; border1:1px solid #dddddd;'>"
    if specs = "brom" then
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportbrom"" : f2.action3.value = """" : f2.submit'><img src=bigicons/paste.png border=0><br><font color=#800000><b> ��� ��� ���� ���� �� �����</b></font></a>"
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportbrom2"" : f2.action3.value = """" : f2.submit'><img src=bigicons/paste.png border=0><br><font color=#800000><b> ��� ��� ���� ������ �� ������ ����</b></font></a>"
    end if
    if specs = "leumi" then
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportsocial"" : f2.submit'><img src=bigicons/paste.png border=0><br><font color=#800000><b> ��� ��������</b></font></a>"
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportsocial"" : f2.ordertypeids.value = """ & session("mechanic_otid") & ","" : f2.submit'><img src=bigicons/paste.png border=0><br><font color=#800000><b> ��� ������ ��������</b></font></a>"
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportsocial"" : f2.ordertypeids.value = """ & session("shuttle_otid") & ","" : f2.submit'><img src=bigicons/paste.png border=0><br><font color=#800000><b> ��� ������ ��������</b></font></a>"
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""report2"" : f2.action3.value = ""output2db"" : f2.submit'><img src=bigicons/paste.png border=0><br><font color=#800000><b> ~Worker Cost Report~ - ���� ���� ���� ������</b></font></a>"
    end if
    if specs = "elbit" then
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""report2"" : f2.action3.value = ""output2db"" : f2.submit'><img src=bigicons/paste.png border=0><br><font color=#800000><b> ~Worker Cost Report~ - ���� ���� ���� ������</b></font></a>"
    end if
    if specs = "modiin" then
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""report2"" : f2.action3.value = ""escort"" : f2.submit'><img src=bigicons/paste.png border=0><br><font color=#800000><b> ��� ������ ������</b></font></a>"
    end if

    rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""report1"" : f2.action3.value = """" : f2.submit'><img src=bigicons/paste.png border=0 alt='��� ������ ������ - ����� ���� ������� ������ ������ ������� �� ������� �� �� �����'><br> ~Unit Cost Report~</a>"
    rr "<div style='position:absolute;'><div style='position:relative; width:100; top:103; ~langside~:10; text-align:~langside~; font-size:9;'>"
    rr "<nobr><input type=text id=paretopercent value=20 style='width:30; font-size:9;'> % PARETO</nobr><br>"
    rr "</div></div>"
    rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""report2"" : f2.action3.value = ""|paretopercent="" & paretopercent.value & ""|"" : f2.submit'><img src=bigicons/paste.png border=0 alt='����� ������� ������� ����� ����� �� �� ���� ������� ����� ����� ������� ��� ���  �����'><br> ~Worker Cost Report~</a>"
    rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""report2"" : f2.action3.value = ""inactive"" : f2.submit'><img src=bigicons/paste.png border=0 alt='����� ������� ������� ����� ����� �� �� ���� ������� ����� ����� ������� ��� ���  ����� - �� ������� �� �����'><br> ~Worker Cost Report~ - ~Inactive Workers~</a>"
    rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""report2"" : f2.action3.value = ""details"" : f2.submit'><img src=bigicons/paste.png border=0 alt='����� ������� ������� ����� ����� �� �� ���� ������� ����� ����� ������� ��� ���  �����, ���� ����� �� �� ������� ��� ���� ����� (���� ��� �� ����� ���� ���)'><br> ~Detailed Worker Cost Report~</a>"
    rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportupdatedpassengers"" : f2.submit'><img src=bigicons/paste.png border=0 alt='���� ����� �� ������ ������ �� ����� ������ ���� ��� �����, ���� ����� �� ���� ������� �������� �� �� ����'><br> ~Retrospect Passengers Changes Report~</a>"
    rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportcourses"" : f2.submit'><img src=bigicons/paste.png border=0 alt='��� ������ ��� ������� ������ �����'><br> ~Courses Report~</a>"
    rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportshifts"" : f2.submit'><img src=bigicons/paste.png border=0 alt='��� ������ ��� ������ ������ �����'><br> ~Shifts Report~</a>"
   
    if specs = "rafael" then
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportrafael"" : f2.action3.value = """" : f2.submit'><img src=bigicons/checkmark.png border=0 alt=''><br> ��� ����� ������</a>"
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportrafael"" : f2.action3.value = ""companies"" : f2.submit'><img src=bigicons/checkmark.png border=0 alt=''><br> ��� ����� ������ + �����</a>"
      rr aprefix & "href=#aa onclick='vbscript: f2.method = ""get"" :f2.action2.value = ""reportrafael2"" : f2.action3.value = """" : f2.submit'><img src=bigicons/checkmark.png border=0 alt=''><br> ��� ������ ���</a>"
    end if

    if specs = "laufer" and session("usergroup") = "admin" then
      rr aprefix & "href=#aa onclick='vbscript: document.location=""_limo_report.asp""'><img src=bigicons/checkmark.png border=0 alt=''><br>��� ������ ��� �������</a>"
    end if
    
    rr "</div></div>"
    '-------------
    rr "<div style='width:330; height1:170; vertical-align:top; display:inline; overflow:hidden; margin:30; '>"
    rr "<div style='height:30; background:#3973DC; color:#ffffff; padding:5; font-weight:bold; overflow:hidden;'>"
    rr "<span style='position:relative; top:-3;'>����� ��������</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
    rr " <select id=yyy>" : for i = year(date) to year(date)-10 step - 1 : rr "<option value=" & i & ">" & i : next : rr "</select>"
    rr " <select id=mmm><option value=''>~all~"
    for i = 1 to 12 : rr "<option value=" & i & ">" & i : next
    for i = 1 to 4 : rr "<option value=Q" & i & ">Q" & i : next
    rr "</select>"
    rr "</div>"
    rr "<div style='width:330; height1:130; overflow-y1:auto; overflow-x:hidden; text-align:~langside~; padding1:20; border1:1px solid #dddddd;'>"

    x = 0 : if session("elang") = "2" then x = 226
    rr "<select name=sss style='position:absolute; left:" & x & "; top:75; width:98;'>"
    rr "<option value=0 style='color:#bbbbff;'>~Supplier~"
    s = "select id,title from suppliers"
    if session("usersiteid") <> "0" then s = s & " where siteid in(" & session("usersiteid") & ",0)"
    s = s & " order by title"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      rr "<option value=" & rs("id") & ">" & rs("title")
      rs.movenext
    loop
    rr "</select>"
    rr aprefix & "href=#aa onclick='vbscript: if sss.value = ""0"" then msgbox ""��� ��� ���"" else document.location = ""?action=reportsuppliers&yyy="" & yyy.value & ""&mmm="" & mmm.value & ""&sss="" & sss.value'>"
    rr "<img src=bigicons/paste.png border=0><br><br> ~Monthly supplier rides report~ </font>"
    rr "</a>"

    rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportunits&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0><br> ~Yearly Unit Report~</a>"
    rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportbunits&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0><br> ~Yearly Business Unit Report~</a>"
    rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportbunits&action2=suppliers&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0><br> ~Yearly Business Unit Report~ - ~Suppliers~</a>"
    rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportbunits&action2=ordertypes&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0><br> ~Yearly Business Unit Report~ - ~ordertypes_ordertypes~</a>"
    rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportseatuse&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0><br> ~Seat Use Per Unit~</a>"
    rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportsuppliersummary&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0><br> ~Concise supplier report~</a>"
    rr "</div></div>"
    '----------
    if session("usersiteid") = "0" then
      rr "<div style='width:330; height1:170; vertical-align:top; display:inline; overflow:hidden; margin:30;'>"
      rr "<div style='height:30; background:#3973DC; color:#ffffff; padding:8; text-align:center; font-weight:bold;'>����� ���� �� ����</div>"
      rr "<div style='width:330; height1:130; overflow-y1:auto; overflow-x:hidden; text-align:~langside~; padding1:20; border1:1px solid #dddddd;'>"

      rr "<select name=reportsiteid style='position:absolute; left:" & x & "; top:75; width:98;'>"
      rr "<option value='' style='color:#bbbbff;'>~Site~"
      s = "select id,title from sites order by title"
      set rs = conn.execute(sqlfilter(s))
      do until rs.eof
        rr "<option value=" & rs("id") & ">" & rs("title")
        rs.movenext
      loop
      rr "</select> "
      rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportsiteordertype&action2=onesitemonths&yyy="" & yyy.value & ""&mmm="" & mmm.value & ""&siteid="" & reportsiteid.value'><img src=bigicons/paste.png border=0><br><br> ~Site cost by order type~ - ~by month~</a>"
      rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportsiteordertype&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0><br> ~Site cost by order type~</a>"
      rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportsiteordertype&action2=workereaverage&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0 alt='����� ���� ����� ��� ��� ��� ���� ������'><br> ~Worker average~</a>"
      rr aprefix & "href=#aa onclick='vbscript: document.location = ""?action=reportsiteseat&yyy="" & yyy.value & ""&mmm="" & mmm.value'><img src=bigicons/paste.png border=0 alt='����� ������ ����� ��� ��� ��� ���� �����'><br> ~Seat Usage Percentage Report~</a>"
      rr aprefix & "href=report.asp><img src=bigicons/stats.png border=0> ~Chart builder~ </a>"
      rr "</div></div>"
    end if

    '----------
    rr "</tr>"
    '----------

    rr "</table>"
    rend
  end if
%>