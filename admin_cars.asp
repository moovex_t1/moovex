<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 16/06/2015 22:37:09

function carsdelitem(i)
  carsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function carsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "" : if getfield("description") <> "" then t = getfield("description")
  f = f & "description," : v = v & "'" & t & "',"

  t = "" : if getfield("code") <> "" then t = getfield("code")
  f = f & "code," : v = v & "'" & t & "',"

  t = "on" : if getfield("active") <> "" then t = getfield("active")
  f = f & "active," : v = v & "'" & t & "',"

  t = "on" : if getfield("board") <> "" then t = getfield("board")
  f = f & "board," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "supplierid" then t = session(tablename & "containerid")
  if getfield("supplierid") <> "" then t = getfield("supplierid")
  f = f & "supplierid," : v = v & t & ","

  t = "0" : if getfield("passengers") <> "" then t = getfield("passengers")
  f = f & "passengers," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  t = session("usersiteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
']
  f = f & "siteid," : v = v & t & ","

  t = "0" : if getfield("rate0") <> "" then t = getfield("rate0")
  f = f & "rate0," : v = v & t & ","

  t = "0" : if getfield("ratekm") <> "" then t = getfield("ratekm")
  f = f & "ratekm," : v = v & t & ","

  t = "0" : if getfield("rate1") <> "" then t = getfield("rate1")
  f = f & "rate1," : v = v & t & ","

  t = "0" : if getfield("rent1") <> "" then t = getfield("rent1")
  f = f & "rent1," : v = v & t & ","

  t = "0" : if getfield("rent2") <> "" then t = getfield("rent2")
  f = f & "rent2," : v = v & t & ","

  t = "0" : if getfield("rent3") <> "" then t = getfield("rent3")
  f = f & "rent3," : v = v & t & ","

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into cars(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from cars"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  carsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table cars (id autoincrement primary key,title text(50),description text(50),code text(20),active text(2),board text(2),supplierid number,passengers number,siteid number,rate0 number,ratekm number,rate1 number,rent1 number,rent2 number,rent3 number,data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "cars" : session("tablename") = "cars"
tablefields = "id,title,description,code,active,board,supplierid,passengers,siteid,rate0,ratekm,rate1,rent1,rent2,rent3,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 1
if session("usergroup") = "supplieradmin" then ulev = 1
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
session("carpassengers") = ""
if session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"
'if getparam("UseDistanceRates") <> "y" then hidecontrols = hidecontrols & "rate0,ratekm,rate1,"
if specs = "iai" then trans = vbcrlf & "~cars_title~��\��� ����" & vbcrlf & "~cars_description~�����\����" & vbcrlf & trans
if specs = "iai" and session("username") <> "kiki" and session("username") <> "mamandom\dnoach" then hidecontrols = hidecontrols & "buttondelete,buttonmultidelete,"
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = carsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("description") then s = s & "description='" & getfield("description") & "',"
  if okedit("code") then s = s & "code='" & getfield("code") & "',"
  if okedit("active") then s = s & "active='" & getfield("active") & "',"
  if okedit("board") then s = s & "board='" & getfield("board") & "',"
  if okedit("supplierid") then s = s & "supplierid=0" & getfield("supplierid") & ","
  if okedit("passengers") then s = s & "passengers=0" & getfield("passengers") & ","
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  if okedit("rate0") then s = s & "rate0=0" & getfield("rate0") & ","
  if okedit("ratekm") then s = s & "ratekm=0" & getfield("ratekm") & ","
  if okedit("rate1") then s = s & "rate1=0" & getfield("rate1") & ","
  if okedit("rent1") then s = s & "rent1=0" & getfield("rent1") & ","
  if okedit("rent2") then s = s & "rent2=0" & getfield("rent2") & ","
  if okedit("rent3") then s = s & "rent3=0" & getfield("rent3") & ","
  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = carsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = carsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      if okedit("active") then s = s & ",active = '" & getfield("listupdate_active_" & b) & "'"
      if okedit("board") then s = s & ",board = '" & getfield("listupdate_board_" & b) & "'"
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~cars_title~^"
  if oksee("description") then csv(0) = csv(0) & "~cars_description~^"
  if oksee("code") then csv(0) = csv(0) & "~cars_code~^"
  if oksee("active") then csv(0) = csv(0) & "~cars_active~^"
  if oksee("board") then csv(0) = csv(0) & "~cars_board~^"
  if oksee("supplierid") then csv(0) = csv(0) & "~cars_supplierid~^"
  if oksee("passengers") then csv(0) = csv(0) & "~cars_passengers~^"
  if oksee("siteid") then csv(0) = csv(0) & "~cars_siteid~^"
  if oksee("rate0") then csv(0) = csv(0) & "~cars_rate0~^"
  if oksee("ratekm") then csv(0) = csv(0) & "~cars_ratekm~^"
  if oksee("rate1") then csv(0) = csv(0) & "~cars_rate1~^"
  if oksee("rent1") then csv(0) = csv(0) & "~cars_rent1~^"
  if oksee("rent2") then csv(0) = csv(0) & "~cars_rent2~^"
  if oksee("rent3") then csv(0) = csv(0) & "~cars_rent3~^"
'[  if oksee("data1") then csv(0) = csv(0) & "~cars_data1~^"
  if showcarfleet = "on" then
    csv(0) = csv(0) & "�� ��� ����^"
    csv(0) = csv(0) & "����� ��� ����^"
    csv(0) = csv(0) & "����� ���� ����^"
    csv(0) = csv(0) & "����� ���� ����� ���^"
    csv(0) = csv(0) & "����� ����� �����^"
    csv(0) = csv(0) & "���� �����^"
    csv(0) = csv(0) & "��������^"
  end if
  if oksee("data1") then csv(0) = csv(0) & "~cars_data1~^"
']
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
  Thecode = rs("code") : if isnull(Thecode) then Thecode = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Theboard = rs("board") : if isnull(Theboard) then Theboard = ""
  Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
  Thesupplierid = replace(thesupplierid,",",".")
  Thepassengers = rs("passengers") : if isnull(Thepassengers) then Thepassengers = 0
  Thepassengers = replace(thepassengers,",",".") : Thepassengers = formatnumber(thepassengers,0,true,false,false)
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Therate0 = rs("rate0") : if isnull(Therate0) then Therate0 = 0
  Therate0 = replace(therate0,",",".") : Therate0 = formatnumber(therate0,2,true,false,false)
  Theratekm = rs("ratekm") : if isnull(Theratekm) then Theratekm = 0
  Theratekm = replace(theratekm,",",".") : Theratekm = formatnumber(theratekm,0,true,false,false)
  Therate1 = rs("rate1") : if isnull(Therate1) then Therate1 = 0
  Therate1 = replace(therate1,",",".") : Therate1 = formatnumber(therate1,2,true,false,false)
  Therent1 = rs("rent1") : if isnull(Therent1) then Therent1 = 0
  Therent1 = replace(therent1,",",".") : Therent1 = formatnumber(therent1,2,true,false,false)
  Therent2 = rs("rent2") : if isnull(Therent2) then Therent2 = 0
  Therent2 = replace(therent2,",",".") : Therent2 = formatnumber(therent2,2,true,false,false)
  Therent3 = rs("rent3") : if isnull(Therent3) then Therent3 = 0
  Therent3 = replace(therent3,",",".") : Therent3 = formatnumber(therent3,2,true,false,false)
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thesupplieridlookup = rs("supplieridlookup") : if isnull(Thesupplieridlookup) then Thesupplieridlookup = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("description") then csv(rc) = csv(rc) & replace(thedescription,vbcrlf," ") & "^"
    if oksee("code") then csv(rc) = csv(rc) & replace(thecode,vbcrlf," ") & "^"
    if oksee("active") then csv(rc) = csv(rc) & replace(theactive,vbcrlf," ") & "^"
    if oksee("board") then csv(rc) = csv(rc) & replace(theboard,vbcrlf," ") & "^"
    if oksee("supplierid") then csv(rc) = csv(rc) & thesupplieridlookup & "^"
    if oksee("passengers") then csv(rc) = csv(rc) & replace(thepassengers,vbcrlf," ") & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
    if oksee("rate0") then csv(rc) = csv(rc) & replace(therate0,vbcrlf," ") & "^"
    if oksee("ratekm") then csv(rc) = csv(rc) & replace(theratekm,vbcrlf," ") & "^"
    if oksee("rate1") then csv(rc) = csv(rc) & replace(therate1,vbcrlf," ") & "^"
    if oksee("rent1") then csv(rc) = csv(rc) & replace(therent1,vbcrlf," ") & "^"
    if oksee("rent2") then csv(rc) = csv(rc) & replace(therent2,vbcrlf," ") & "^"
    if oksee("rent3") then csv(rc) = csv(rc) & replace(therent3,vbcrlf," ") & "^"
'[    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
		if showcarfleet = "on" then
		  csv(rc) = csv(rc) & tocsv(getval(thedata1, "carowner")) & "^"
		  csv(rc) = csv(rc) & tocsv(getval(thedata1, "carownerjob")) & "^"
		  csv(rc) = csv(rc) & tocsv(getval(thedata1, "datereceived")) & "^"
		  csv(rc) = csv(rc) & tocsv(getval(thedata1, "testexpires")) & "^"
		  csv(rc) = csv(rc) & tocsv(getval(thedata1, "insuranceend")) & "^"
		  csv(rc) = csv(rc) & tocsv(getval(thedata1, "insuranceagent")) & "^"
		  csv(rc) = csv(rc) & tocsv(getval(thedata1, "kilometer")) & "^"
		end if
    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
']
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      siteid = session("usersiteid")
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "title" then
          if len(b) > 50 then b = left(b,50)
          if okedit("title") then s = s & "title='" & b & "',"
        elseif csvff(i) = "description" then
          if len(b) > 50 then b = left(b,50)
          if okedit("description") then s = s & "description='" & b & "',"
        elseif csvff(i) = "code" then
          if len(b) > 20 then b = left(b,20)
          if okedit("code") then s = s & "code='" & b & "',"
        elseif csvff(i) = "active" then
          if len(b) > 2 then b = left(b,2)
          if okedit("active") then s = s & "active='" & b & "',"
        elseif csvff(i) = "board" then
          if len(b) > 2 then b = left(b,2)
          if okedit("board") then s = s & "board='" & b & "',"
        elseif csvff(i) = "supplierid" then
          if b = "" then
            v = 0
          else
'[            sq = "select * from suppliers where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            sq = "select * from suppliers where lcase(cstr('' & title)) = '" & lcase(b) & "' and siteid = " & siteid
']
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("supplierid") then s = s & "supplierid=" & v & ","
        elseif csvff(i) = "passengers" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("passengers") then s = s & "passengers=" & b & ","
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("siteid") then s = s & "siteid=" & v & ","
          if okedit("siteid") then s = s & "siteid=" & v & ","
          if session("usersiteid") = "0" then siteid = v
']
        elseif csvff(i) = "rate0" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("rate0") then s = s & "rate0=" & b & ","
        elseif csvff(i) = "ratekm" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("ratekm") then s = s & "ratekm=" & b & ","
        elseif csvff(i) = "rate1" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("rate1") then s = s & "rate1=" & b & ","
        elseif csvff(i) = "rent1" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("rent1") then s = s & "rent1=" & b & ","
        elseif csvff(i) = "rent2" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("rent2") then s = s & "rent2=" & b & ","
        elseif csvff(i) = "rent3" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("rent3") then s = s & "rent3=" & b & ","
        elseif csvff(i) = "data1" then
          if okedit("data1") then s = s & "data1='" & b & "',"
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = carsinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","cars.id,cars.title,cars.description,cars.code,cars.active,cars.board,cars.supplierid,cars.passengers,cars.siteid,cars.rate0,cars.ratekm,cars.rate1,cars.rent1,cars.rent2,cars.rent3,cars.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
  Thecode = rs("code") : if isnull(Thecode) then Thecode = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Theboard = rs("board") : if isnull(Theboard) then Theboard = ""
  Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
  Thesupplierid = replace(thesupplierid,",",".")
  Thepassengers = rs("passengers") : if isnull(Thepassengers) then Thepassengers = 0
  Thepassengers = replace(thepassengers,",",".") : Thepassengers = formatnumber(thepassengers,0,true,false,false)
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Therate0 = rs("rate0") : if isnull(Therate0) then Therate0 = 0
  Therate0 = replace(therate0,",",".") : Therate0 = formatnumber(therate0,2,true,false,false)
  Theratekm = rs("ratekm") : if isnull(Theratekm) then Theratekm = 0
  Theratekm = replace(theratekm,",",".") : Theratekm = formatnumber(theratekm,0,true,false,false)
  Therate1 = rs("rate1") : if isnull(Therate1) then Therate1 = 0
  Therate1 = replace(therate1,",",".") : Therate1 = formatnumber(therate1,2,true,false,false)
  Therent1 = rs("rent1") : if isnull(Therent1) then Therent1 = 0
  Therent1 = replace(therent1,",",".") : Therent1 = formatnumber(therent1,2,true,false,false)
  Therent2 = rs("rent2") : if isnull(Therent2) then Therent2 = 0
  Therent2 = replace(therent2,",",".") : Therent2 = formatnumber(therent2,2,true,false,false)
  Therent3 = rs("rent3") : if isnull(Therent3) then Therent3 = 0
  Therent3 = replace(therent3,",",".") : Therent3 = formatnumber(therent3,2,true,false,false)
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "title,description,code,active,board,supplierid,passengers,siteid,rate0,ratekm,rate1,rent1,rent2,rent3,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
'[  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  if showcarfleet = "on" and not childpage and getfield("printversion") <> "on" then
    rr("<div class=subtablinks>")

    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?> ~" & tablename & "_" & tablename & "~</a>")
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "> <b> " & thetitle & "</b></a>")

    miscview = "car_fixes" & theid
    miscdisplay = "������� ��������"
    misctrans = curl("title,����,contents,�����,num1,����,")
    mischidefields = "-"
    t = "000000" : if getfield("childpage") = "misc" and getfield("miscview") = miscview then t = "bb0000"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=misc&miscview=" & miscview & "&miscdisplay=" & curl(miscdisplay) & "&misctrans=" & misctrans & "&mischidefields=" & mischidefields & "> <font color=" & t & "> " & miscdisplay & " </font></a>")

    miscview = "car_fines" & theid
    miscdisplay = "�����"
    misctrans = curl("title,���� ������,contents,����� ������,num1,����,")
    mischidefields = "-"
    t = "000000" : if getfield("childpage") = "misc" and getfield("miscview") = miscview then t = "bb0000"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=misc&miscview=" & miscview & "&miscdisplay=" & curl(miscdisplay) & "&misctrans=" & misctrans & "&mischidefields=" & mischidefields & "> <font color=" & t & "> " & miscdisplay & " </font></a>")

    miscview = "car_fuel" & theid
    miscdisplay = "���"
    misctrans = curl("title,�� ���,num1,����,contents,���� �����,")
    mischidefields = ""
    t = "000000" : if getfield("childpage") = "misc" and getfield("miscview") = miscview then t = "bb0000"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=misc&miscview=" & miscview & "&miscdisplay=" & curl(miscdisplay) & "&misctrans=" & misctrans & "&mischidefields=" & mischidefields & "> <font color=" & t & "> " & miscdisplay & " </font></a>")

    miscview = "car_tollroads" & theid
    miscdisplay = "����� ����"
    misctrans = curl("title,����,contents,���� ������,num1,����,")
    mischidefields = "-"
    t = "000000" : if getfield("childpage") = "misc" and getfield("miscview") = miscview then t = "bb0000"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=misc&miscview=" & miscview & "&miscdisplay=" & curl(miscdisplay) & "&misctrans=" & misctrans & "&mischidefields=" & mischidefields & "> <font color=" & t & "> " & miscdisplay & " </font></a>")

    miscview = "car_ituran" & theid
    miscdisplay = "�������"
    misctrans = curl("title,�����,num1,����,")
    mischidefields = "contents,"
    t = "000000" : if getfield("childpage") = "misc" and getfield("miscview") = miscview then t = "bb0000"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=misc&miscview=" & miscview & "&miscdisplay=" & curl(miscdisplay) & "&misctrans=" & misctrans & "&mischidefields=" & mischidefields & "> <font color=" & t & "> " & miscdisplay & " </font></a>")

    miscview = "car_documents" & theid
    miscdisplay = "������"
    misctrans = curl("title,��,contents,�����,file1,����,")
    mischidefields = "num1,"
    t = "000000" : if getfield("childpage") = "misc" and getfield("miscview") = miscview then t = "bb0000"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=misc&miscview=" & miscview & "&miscdisplay=" & curl(miscdisplay) & "&misctrans=" & misctrans & "&mischidefields=" & mischidefields & "> <font color=" & t & "> " & miscdisplay & " </font></a>")

    rr("<span id=childchildren></span></div>")
    if getfield("action2") = "showchildpage" then
      t = "admin_misc.asp?miscview=" & curl(getfield("miscview")) & "&containerid=0&miscdisplay=" & curl(getfield("miscdisplay")) & "&mischidefields=" & curl(getfield("mischidefields")) & "&misctrans=" & curl(getfield("misctrans"))
      rr("<br><iframe id=fr" & getfield("childpage") & " name=fr" & getfield("childpage") & " style='width:100%; height:1000;' frameborder=0 scrolling=no src=" & t & "></iframe>")
      rrbottom : rend
    end if
  end if

']


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_car.gif' ~iconsize~> ~cars_cars~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~cars_title~</span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  end if'form:title

  if oksee("description") then'form:description
    rr("<tr valign=top id=description_tr0><td class=form_item1 id=description_tr1><span id=description_caption>~cars_description~</span><td class=form_item2 id=description_tr2>")
    rr("<input type=text maxlength=50 name=description onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedescription & """ dir=~langdir~>")
  end if'form:description

  if oksee("code") then'form:code
    rr("<tr valign=top id=code_tr0><td class=form_item1 id=code_tr1><span id=code_caption>~cars_code~</span><td class=form_item2 id=code_tr2>")
    rr("<input type=text maxlength=20 name=code onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thecode & """ dir=~langdir~>")
  end if'form:code

  if oksee("active") then'form:active
    rr("<tr valign=top id=active_tr0><td class=form_item1 id=active_tr1><span id=active_caption>~cars_active~</span><td class=form_item2 id=active_tr2>")
    rr("<input type=checkbox name=active")
    if theactive = "on" then rr(" checked")
    rr(">")
  end if'form:active

  if oksee("board") then'form:board
    rr("<tr valign=top id=board_tr0><td class=form_item1 id=board_tr1><span id=board_caption>~cars_board~</span><td class=form_item2 id=board_tr2>")
    rr("<input type=checkbox name=board")
    if theboard = "on" then rr(" checked")
    rr(">")
'[  end if'form:board
  end if'form:board

  'if specs = "modiin" then
  '  rr("<tr valign=top><td class=form_item1><span>�����</span><td class=form_item2>")
  '  rr "  <input type=checkbox name=haslift " : if getval(thedata1, "haslift") = "on" then rr " checked"
  '  rr ">"
  '  checkformadd = checkformadd & "t = """" : if f1.haslift.checked then t = ""on""" & vbcrlf
  '  checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""haslift"", t)" & vbcrlf
  'end if

  if specs = "iai" then
    rr("<tr valign=top><td class=form_item1>���� ��� ����� ������<td class=form_item2>")

    v = getval(thedata1, "unit0") : if v = "" then v = getval(thedata1, "unit1") & "-" & getval(thedata1, "unit2") : if v = "-" then v = ""
    rr "<input type=text name=unit0 style='width:300;' value='" & v & "'>"
    rr "* <span style='font-size:10;'>���� ����� ����� �� ����� �� ������, ���� 40,45,50-57</span>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""unit0"", f1.unit0.value)" & vbcrlf

    rr("<tr valign=top><td class=form_item1>���� �������<td class=form_item2>")
    rr "<input type=text name=carproject style='width:150;' value='" & getval(thedata1, "carproject") & "'>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""carproject"", f1.carproject.value)" & vbcrlf
  end if
']

  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then
    rr("<tr valign=top id=supplierid_tr0><td class=form_item1 id=supplierid_tr1><span id=supplierid_caption>~cars_supplierid~</span><td class=form_item2 id=supplierid_tr2>")
    if getfield("box") = "supplierid" then thesupplierid = getfield("boxid")
    rr("<select name=supplierid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~cars_supplierid~")
'[    sq = "select * from suppliers order by title"
    rr("<option value=0 style='color:bbbbfe;'>~cars_supplierid~")
    sq = "select * from suppliers where id = " & thesupplierid & " or siteid in(" & thesiteid & ",0) order by title"

']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesupplierid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:supplierid
  elseif okedit("supplierid") then
    rr("<input type=hidden name=supplierid value=" & thesupplierid & ">")
  end if'form:supplierid

  if oksee("passengers") then'form:passengers
    rr("<tr valign=top id=passengers_tr0><td class=form_item1 id=passengers_tr1><span id=passengers_caption>~cars_passengers~<img src=must.gif></span><td class=form_item2 id=passengers_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=passengers value=""" & Thepassengers & """>")
'[  end if'form:passengers
  end if'form:passengers

  if instr(",rail,rafael,", "," & specs & ",") > 0 then
    rr("<tr valign=top id=hourrate_tr0><td class=form_item1 id=hourrate_tr1><span id=hourrate_caption>����� ����</span><td class=form_item2 id=hourrate_tr2>")
    rr "  <input type=text name=hourrate style='width:70; direction:ltr;' value='" & getval(thedata1, "hourrate") & "'>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""hourrate"", f1.hourrate.value)" & vbcrlf
  end if
']

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~cars_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~cars_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
'[  end if'form:siteid
  end if'form:siteid

  '---------------------------showcarfleet-------------------
  if showcarfleet = "on" then

    rr "<tr><td colspan=9 class=form_name>����"

    testexpires = getval(thedata1, "testexpires") : if testexpires = "" then testexpires = "1/1/1900"
    rr "<tr valign=top id=type1_tr0><td class=form_item1>����� ���� ����� ���<td class=form_item2>"
    rr SelectDate("f1.testexpires", testexpires)
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""testexpires"", f1.testexpires.value)" & vbcrlf

    rr "<tr valign=top id=type1_tr0><td class=form_item1>�� ��� ����<td class=form_item2>"
    rr "<input type=text name=carowner value='" & getval(thedata1, "carowner") & "' dir=ltr style='width:100;'>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""carowner"", f1.carowner.value)" & vbcrlf

    rr "<tr valign=top id=type1_tr0><td class=form_item1>����� ��� ����<td class=form_item2>"
    rr "<input type=text name=carphone value='" & getval(thedata1, "carphone") & "' dir=ltr style='width:100;'>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""carphone"", f1.carphone.value)" & vbcrlf

    rr "<tr valign=top id=type1_tr0><td class=form_item1>����� ��� ����<td class=form_item2>"
    rr "<input type=text name=carownerjob value='" & getval(thedata1, "carownerjob") & "' dir=ltr style='width:100;'>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""carownerjob"", f1.carownerjob.value)" & vbcrlf

    rr "<tr valign=top id=type1_tr0><td class=form_item1>����� ���<td class=form_item2>"
    rr "<select name=carstatus>"
    rr "<option value=''>"
    rr "<option value='���'" & iif(getval(thedata1, "carstatus") = "���", " selected", "") & ">���"
    rr "<option value='������'" & iif(getval(thedata1, "carstatus") = "������", " selected", "") & ">������"
    rr "<option value='������'" & iif(getval(thedata1, "carstatus") = "������", " selected", "") & ">������"
    rr "</select>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""carstatus"", f1.carstatus.value)" & vbcrlf

    datereceived = getval(thedata1, "datereceived") : if datereceived = "" then datereceived = "1/1/1900"
    rr "<tr valign=top id=type1_tr0><td class=form_item1>���� ����<td class=form_item2>"
    rr SelectDate("f1.datereceived", datereceived)
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""datereceived"", f1.datereceived.value)" & vbcrlf

    rr "<tr><td colspan=9 class=form_name>�����"

    insurancebegin = getval(thedata1, "insurancebegin") : if insurancebegin = "" then insurancebegin = "1/1/1900"
    rr "<tr valign=top id=type1_tr0><td class=form_item1>����� ����� �����<td class=form_item2>"
    rr SelectDate("f1.insurancebegin", insurancebegin)
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""insurancebegin"", f1.insurancebegin.value)" & vbcrlf

    insuranceend = getval(thedata1, "insuranceend") : if insuranceend = "" then insuranceend = "1/1/1900"
    rr "<tr valign=top id=type1_tr0><td class=form_item1>����� ����� �����<td class=form_item2>"
    rr SelectDate("f1.insuranceend", insuranceend)
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""insuranceend"", f1.insuranceend.value)" & vbcrlf

    rr "<tr valign=top id=type1_tr0><td class=form_item1>��� �����<td class=form_item2>"
    rr "<select name=insurancetype>"
    rr "<option value=''>"
    rr "<option value='����'" & iif(getval(thedata1, "insurancetype") = "����", " selected", "") & ">����"
    rr "<option value='���� �����'" & iif(getval(thedata1, "insurancetype") = "���� �����", " selected", "") & ">���� �����"
    rr "<option value='����'" & iif(getval(thedata1, "insurancetype") = "����", " selected", "") & ">����"
    rr "</select>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""insurancetype"", f1.insurancetype.value)" & vbcrlf

    rr "<tr valign=top id=type1_tr0><td class=form_item1>���� �����<td class=form_item2>"
    rr "<input type=text name=insuranceagent value='" & getval(thedata1, "insuranceagent") & "' dir=ltr style='width:70;'>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""insuranceagent"", f1.insuranceagent.value)" & vbcrlf

    rr "<tr><td colspan=9 class=form_name>������� ������ ���"

    rr "<tr valign=top id=type1_tr0><td class=form_item1>���������'<td class=form_item2>"
    rr "<input type=text name=kilometer value='" & getval(thedata1, "kilometer") & "' dir=ltr style='width:70;'>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""kilometer"", f1.kilometer.value)" & vbcrlf

    rr "<tr valign=top id=type1_tr0><td class=form_item1>���������' ������ ������<td class=form_item2>"
    rr "<input type=text name=lasttreatkilometer value='" & getval(thedata1, "lasttreatkilometer") & "' dir=ltr style='width:70;'>"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""lasttreatkilometer"", f1.lasttreatkilometer.value)" & vbcrlf
    if isnumeric(lasttreatkilometer) then rr " <b>����� ��� � " & lasttreatkilometer + 15000 & " �""�</b>"

    treatdate = getval(thedata1, "treatdate") : if treatdate = "" then treatdate = "1/1/1900"
    rr "<tr valign=top id=type1_tr0><td class=form_item1>����� ����� �����<td class=form_item2>"
    rr SelectDate("f1.treatdate", treatdate)
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""treatdate"", f1.treatdate.value)" & vbcrlf

  end if
  '============================================================================
  rr "<tr><td colspan=9 class=form_name>~courses_courserates~"

']

  if oksee("rate0") then'form:rate0
    rr("<tr valign=top id=rate0_tr0><td class=form_item1 id=rate0_tr1><span id=rate0_caption>~cars_rate0~<img src=must.gif></span><td class=form_item2 id=rate0_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=rate0 value=""" & Therate0 & """>")
  end if'form:rate0

  if oksee("ratekm") then'form:ratekm
    rr("<tr valign=top id=ratekm_tr0><td class=form_item1 id=ratekm_tr1><span id=ratekm_caption>~cars_ratekm~<img src=must.gif></span><td class=form_item2 id=ratekm_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=ratekm value=""" & Theratekm & """>")
  end if'form:ratekm

  if oksee("rate1") then'form:rate1
    rr("<tr valign=top id=rate1_tr0><td class=form_item1 id=rate1_tr1><span id=rate1_caption>~cars_rate1~<img src=must.gif></span><td class=form_item2 id=rate1_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=rate1 value=""" & Therate1 & """>")
'[  end if'form:rate1
    if specs = "brom" then
      rr " ���� ��""� ��� 80 <input type=text name=rate80 style='width:50; direction:ltr;' value='" & getval(thedata1, "rate80") & "'>"
      checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""rate80"", f1.rate80.value)" & vbcrlf
    end if
  end if'form:rate1
']

  if oksee("rent1") then'form:rent1
    rr("<tr valign=top id=rent1_tr0><td class=form_item1 id=rent1_tr1><span id=rent1_caption>~cars_rent1~<img src=must.gif></span><td class=form_item2 id=rent1_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=rent1 value=""" & Therent1 & """>")
  end if'form:rent1

  if oksee("rent2") then'form:rent2
    rr("<tr valign=top id=rent2_tr0><td class=form_item1 id=rent2_tr1><span id=rent2_caption>~cars_rent2~<img src=must.gif></span><td class=form_item2 id=rent2_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=rent2 value=""" & Therent2 & """>")
  end if'form:rent2

  if oksee("rent3") then'form:rent3
    rr("<tr valign=top id=rent3_tr0><td class=form_item1 id=rent3_tr1><span id=rent3_caption>~cars_rent3~<img src=must.gif></span><td class=form_item2 id=rent3_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=rent3 value=""" & Therent3 & """>")
  end if'form:rent3

'[  if oksee("data1") then'form:data1

  '-----------drivers
  if request_action3 = "avaadd" then
    s = "select data1 from " & tablename & " where id = " & request_id
    set rs = conn.execute(sqlfilter(s))
    d = rs("data1")
    a = getval(d, "ava")
    a = a & "0;1,2,3,4,5,6,7," & getparam("ExtraWeekDays") & ";00:00;23:59;"
    d = setval(d, "ava", a)
    s = "update " & tablename & " set data1 = '" & d & "' where id = " & request_id
    set rs1 = conn.execute(sqlfilter(s))
    thedata1 = d
  end if
  if left(request_action3,6) = "avadel" then
    j = mid(request_action3, 7)
    s = "select data1 from " & tablename & " where id = " & request_id
    set rs = conn.execute(sqlfilter(s))
    d = rs("data1")
    a = getval(d, "ava") : r = ""
    i = 0
    do until a = ""
      i = i + 1
      b1 = left(a,instr(a,";")-1) : a = mid(a,instr(a,";")+1)
      b2 = left(a,instr(a,";")-1) : a = mid(a,instr(a,";")+1)
      b3 = left(a,instr(a,";")-1) : a = mid(a,instr(a,";")+1)
      b4 = left(a,instr(a,";")-1) : a = mid(a,instr(a,";")+1)
      if cstr(i) <> cstr(j) then r = r & b1 & ";" & b2 & ";" & b3 & ";" & b4 & ";"
    loop
    d = setval(d, "ava", r)
    s = "update " & tablename & " set data1 = '" & d & "' where id = " & request_id
    set rs1 = conn.execute(sqlfilter(s))
    thedata1 = d
  end if
  if instr(",sheleg,ap,", "," & specs & ",") > 0 then
    rr "<tr valign=top id=type1_tr0><td class=form_item1>�����<td class=form_item2>"
    aa = getval(thedata1, "ava")
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""ava"", f1.ava.value)" & vbcrlf
    'rr "<input type=text name=ava value='" & aa & "' dir=ltr style='width:700;'><br>"
    rr "<input type=hidden name=ava value='" & aa & "'>"
    i = 0 : ss = ""
    do until aa = ""
      i = i + 1
      driverid = left(aa,instr(aa,";")-1) : aa = mid(aa,instr(aa,";")+1)
      days = left(aa,instr(aa,";")-1) : aa = mid(aa,instr(aa,";")+1)
      hour1 = left(aa,instr(aa,";")-1) : aa = mid(aa,instr(aa,";")+1)
      hour1b = left(aa,instr(aa,";")-1) : aa = mid(aa,instr(aa,";")+1)
      rr "<div style='background:#eeeeee; padding:6; border:1px solid #dddddd; margin-bottom:5;'>"
      rr "<a href=#aa onclick='vbscript: f1.action2.value = ""stay"" : f1.action3.value = ""avadel" & i & """ : checkform'><img src=minus.gif border=0></a> "

      ss = ss & " t = t & f1.driverid" & i & ".value & "";""" & vbcrlf
      rr "<select name=driverid" & i & " dir=~langdir~ onchange='vbscript: updateava'>"
      rr "<option value=0 style='color:bbbbfe;'>~orders_driverid~"
      sq = "select id,title from drivers where 0=0"
      sq = sq & " and (id = " & driverid & " or active = 'on')"
      sq = sq & " and supplierid = " & thesupplierid
      sq = sq & " order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        rr "<option value=" & rs1("id")
        if cstr(driverid) = cstr(rs1("id")) then rr " selected"
        rr ">" & rs1("title")
        rs1.movenext
      loop
      rr "</select>"
      rr("&nbsp;&nbsp;&nbsp;<input type=text maxlength=5 name=hour1" & i & " onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & hour1 & """ dir=ltr onchange='vbscript:updateava'>")
      rr(" �� <input type=text maxlength=5 name=hour1b" & i & " onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:40;' value=""" & hour1b & """ dir=ltr onchange='vbscript:updateava'>")
      c = 0 : a = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
      do until a = ""
        c = c + 1
        b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
        rr(" <input type=checkbox name=days" & i & "_c" & c)
        if instr("," & days & ",","," & b & ",") > 0 then rr(" checked")
        rr(" onclick='vbscript:updateava'>")
        rr("~weekday" & b & "~ &nbsp;")
        ss = ss & "if f1.days" & i & "_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
      loop
      rr "</div>"
      ss = ss & " t = t & "";"" & f1.hour1" & i & ".value & "";"" & f1.hour1b" & i & ".value & "";""" & vbcrlf
    loop
    rr "&nbsp;&nbsp;<a href=#aa onclick='vbscript: f1.action2.value = ""stay"" : f1.action3.value = ""avaadd"" : checkform' style='color:#0000aa;'><img src=plus.gif border=0>&nbsp;&nbsp;&nbsp; ~Add~ ~driver~</a>"
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub updateava" & vbcrlf)
    rr("    t = """"" & vbcrlf)
    rr(ss & vbcrlf)
    rr("    f1.ava.value = t" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("</script>" & vbcrlf)
  end if

  if hasmatch(specs, "police,royalbeach,sheleg,") then
    rr "<tr valign=top><td class=form_item1 style='color:#0000aa;'>����� ����� �����<td class=form_item2>"
    rr "<input type=text id=add_station style='width:100; direction:ltr;' value='" & getval(thedata1, "add_station") & "' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""add_station"", me.value)'>"
  end if

  if oksee("data1") then'form:data1
']
    rr("<span id=data1_caption></span> ")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
  end if'form:data1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
'[  rr("  sub checkform" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr checkformadd
']
  rr("    ok = true" & vbcrlf)
  if okedit("passengers") and session(tablename & "containerfield") <> "passengers" then
    rr("    if isnumeric(f1.passengers.value) then" & vbcrlf)
    rr("      if cdbl(f1.passengers.value) >= 0 and cdbl(f1.passengers.value) <= 1000 then" & vbcrlf)
    rr("        validfield(""passengers"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""passengers"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""passengers"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("rate0") and session(tablename & "containerfield") <> "rate0" then
    rr("    if isnumeric(f1.rate0.value) then" & vbcrlf)
    rr("      if cdbl(f1.rate0.value) >= -4000000000 and cdbl(f1.rate0.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""rate0"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""rate0"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""rate0"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("ratekm") and session(tablename & "containerfield") <> "ratekm" then
    rr("    if isnumeric(f1.ratekm.value) then" & vbcrlf)
    rr("      if cdbl(f1.ratekm.value) >= -4000000000 and cdbl(f1.ratekm.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""ratekm"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""ratekm"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""ratekm"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("rate1") and session(tablename & "containerfield") <> "rate1" then
    rr("    if isnumeric(f1.rate1.value) then" & vbcrlf)
    rr("      if cdbl(f1.rate1.value) >= -4000000000 and cdbl(f1.rate1.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""rate1"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""rate1"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""rate1"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("rent1") and session(tablename & "containerfield") <> "rent1" then
    rr("    if isnumeric(f1.rent1.value) then" & vbcrlf)
    rr("      if cdbl(f1.rent1.value) >= 0 and cdbl(f1.rent1.value) <= 999999 then" & vbcrlf)
    rr("        validfield(""rent1"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""rent1"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""rent1"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("rent2") and session(tablename & "containerfield") <> "rent2" then
    rr("    if isnumeric(f1.rent2.value) then" & vbcrlf)
    rr("      if cdbl(f1.rent2.value) >= 0 and cdbl(f1.rent2.value) <= 999999 then" & vbcrlf)
    rr("        validfield(""rent2"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""rent2"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""rent2"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("rent3") and session(tablename & "containerfield") <> "rent3" then
    rr("    if isnumeric(f1.rent3.value) then" & vbcrlf)
    rr("      if cdbl(f1.rent3.value) >= 0 and cdbl(f1.rent3.value) <= 999999 then" & vbcrlf)
    rr("        validfield(""rent3"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""rent3"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""rent3"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = "data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/i_car.gif' ~iconsize~> ~cars_cars~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  rr(" <nobr><img id=active_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=active dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~cars_active~")
  rr("<option value=on") : if lcase(session(tablename & "_active")) = "on" then rr(" selected")
  rr("> ~cars_active~: ~Yes~")
  rr("<option value=no") : if lcase(session(tablename & "_active")) = "no" then rr(" selected")
  rr("> ~cars_active~: ~No~")
  rr("</select></nobr>")'search:active
  if session(tablename & "containerfield") <> "supplierid" then
    rr(" <nobr><img id=supplierid_filtericon src=icons/i_key.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=supplierid dir=~langdir~>")
'[    rr("<option value='' style='color:bbbbfe;'>~cars_supplierid~")
'[    sq = "select * from suppliers order by title"
    rr("<option value='' style='color:bbbbfe;'>~cars_supplierid~")
    sq = "select * from suppliers"
    if session("usersiteid") <> "0" then sq = sq & " where (siteid = " & session("usersiteid") & " or siteid = 0)"
    sq = sq & " order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_supplierid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:supplierid
  end if'search:supplierid
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~cars_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
'[  end if'search:siteid
  end if'search:siteid
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"

  if specs = "iai" then
    v = session("cars_filterigumin")
    rr " <nobr><img id=siteid_filtericon src=icons/i_car.gif style='filter:alpha(opacity=50); margin-top:8;'>"
    rr " <select name=filterigumin>"
    rr "<option value='' style='color:cccccc;'>��� �����"
    rr "<option value=y" & iif(v = "y", " selected", "") & ">��� ����� �����"
    rr "<option value=n" & iif(v = "n", " selected", "") & ">��� ����� ����"
    rr "</select>"
  end if
']
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & suppliers_1.title) as supplieridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join suppliers suppliers_1 on " & tablename & ".supplierid = suppliers_1.id)"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

'[  '-sqladd
  if session("cars_filterigumin") = "y" then
    t = "select carid from orders where ordertypeid = " & session("igum_otid") & " and status = 'Received'"
    if session("usersiteid") <> "0" then t = t & " and siteid = " & session("usersiteid")
    ii = getidlist(t)
    sups = getidlist("select id from suppliers where title like '������%'")
    s = s & " and cars.active = 'on' and cars.id not in(" & ii & "-1) and supplierid in(" & sups & "-1)"
  elseif session("cars_filterigumin") = "n" then
    t = "select carid from orders where ordertypeid = " & session("igum_otid") & " and status = 'Received'"
    if session("usersiteid") <> "0" then t = t & " and siteid = " & session("usersiteid")
    ii = getidlist(t)
    s = s & " and cars.id in(" & ii & "-1)"
  end if
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","cars.id,cars.title,cars.description,cars.code,cars.active,cars.board,cars.supplierid,cars.passengers,cars.siteid,cars.rate0,cars.ratekm,cars.rate1,cars.rent1,cars.rent2,cars.rent3,cars.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_active") = "on" then s = s & " and lcase('' & " & tablename & ".active) = 'on'"
  if session(tablename & "_active") = "no" then s = s & " and lcase('' & " & tablename & ".active) <> 'on'"
  if session(tablename & "_supplierid") <> "" and session(tablename & "containerfield") <> "supplierid" then s = s & " and " & tablename & ".supplierid = " & session(tablename & "_supplierid")
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".description like '%" & w & "%' or"
        s = s & " " & tablename & ".code like '%" & w & "%' or"
        s = s & " cstr('' & suppliers_1.title) & ' ' like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".passengers = " & w & " or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".rate0 = " & w & " or"
        if isnumeric(w) then s = s & " " & tablename & ".ratekm = " & w & " or"
        if isnumeric(w) then s = s & " " & tablename & ".rate1 = " & w & " or"
        if isnumeric(w) then s = s & " " & tablename & ".rent1 = " & w & " or"
        if isnumeric(w) then s = s & " " & tablename & ".rent2 = " & w & " or"
        if isnumeric(w) then s = s & " " & tablename & ".rent3 = " & w & " or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("title") then f = "cars.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_title~</a> " & a2)
  if oksee("description") then f = "cars.description" : f2 = "description" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("description") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_description~</a> " & a2)
  if oksee("code") then f = "cars.code" : f2 = "code" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("code") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_code~</a> " & a2)
  if oksee("active") then f = "cars.active" : f2 = "active" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("active") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_active~</a> " & a2)
  if oksee("board") then f = "cars.board" : f2 = "board" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("board") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_board~</a> " & a2)
  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then
    f = "suppliers_1.title" : f2 = "supplieridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_supplierid~</a> " & a2)
  end if
  if oksee("passengers") then f = "cars.passengers" : f2 = "passengers" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("passengers") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_passengers~</a> " & a2)
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_siteid~</a> " & a2)
  end if
  if oksee("rate0") then f = "cars.rate0" : f2 = "rate0" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("rate0") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_rate0~</a> " & a2)
  if oksee("ratekm") then f = "cars.ratekm" : f2 = "ratekm" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("ratekm") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_ratekm~</a> " & a2)
  if oksee("rate1") then f = "cars.rate1" : f2 = "rate1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("rate1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_rate1~</a> " & a2)
  if oksee("rent1") then f = "cars.rent1" : f2 = "rent1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("rent1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_rent1~</a> " & a2)
  if oksee("rent2") then f = "cars.rent2" : f2 = "rent2" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("rent2") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_rent2~</a> " & a2)
  if oksee("rent3") then f = "cars.rent3" : f2 = "rent3" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("rent3") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~cars_rent3~</a> " & a2)
'[  if oksee("data1") then rr("<td class=list_title><nobr>~cars_data1~")
  if showcarfleet = "on" then
    rr "<td class=list_title><nobr>�� ��� ����"
    rr "<td class=list_title><nobr>����� ��� ����"
    rr "<td class=list_title><nobr>����� ���� ����"
    rr "<td class=list_title><nobr>����� ���� ����� ���"
    rr "<td class=list_title><nobr>����� ����� �����"
    rr "<td class=list_title><nobr>���� �����"
    rr "<td class=list_title><nobr>��������'"
  end if
  if oksee("data1") then rr("<td class=list_title><nobr>~cars_data1~")
']
  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Thedescription = rs("description") : if isnull(Thedescription) then Thedescription = ""
      Thecode = rs("code") : if isnull(Thecode) then Thecode = ""
      Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
      Theboard = rs("board") : if isnull(Theboard) then Theboard = ""
      Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
      Thesupplierid = replace(thesupplierid,",",".")
      Thepassengers = rs("passengers") : if isnull(Thepassengers) then Thepassengers = 0
      Thepassengers = replace(thepassengers,",",".") : Thepassengers = formatnumber(thepassengers,0,true,false,false)
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Therate0 = rs("rate0") : if isnull(Therate0) then Therate0 = 0
      Therate0 = replace(therate0,",",".") : Therate0 = formatnumber(therate0,2,true,false,false)
      Theratekm = rs("ratekm") : if isnull(Theratekm) then Theratekm = 0
      Theratekm = replace(theratekm,",",".") : Theratekm = formatnumber(theratekm,0,true,false,false)
      Therate1 = rs("rate1") : if isnull(Therate1) then Therate1 = 0
      Therate1 = replace(therate1,",",".") : Therate1 = formatnumber(therate1,2,true,false,false)
      Therent1 = rs("rent1") : if isnull(Therent1) then Therent1 = 0
      Therent1 = replace(therent1,",",".") : Therent1 = formatnumber(therent1,2,true,false,false)
      Therent2 = rs("rent2") : if isnull(Therent2) then Therent2 = 0
      Therent2 = replace(therent2,",",".") : Therent2 = formatnumber(therent2,2,true,false,false)
      Therent3 = rs("rent3") : if isnull(Therent3) then Therent3 = 0
      Therent3 = replace(therent3,",",".") : Therent3 = formatnumber(therent3,2,true,false,false)
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thesupplieridlookup = rs("supplieridlookup") : if isnull(Thesupplieridlookup) then Thesupplieridlookup = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/i_car.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("description") then rr("<td class=list_item valign=top align=><nobr>" & thedescription)
      if oksee("code") then rr("<td class=list_item valign=top align=><nobr>" & thecode)
      if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("active") then
      elseif session(tablename & "containerfield") = "active" then
        rr("<input type=hidden name=listupdate_active_" & theid & " value=" & theactive & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "active" then
        rr("<input type=checkbox name=listupdate_active_" & theid & "")
        if theactive = "on" then rr(" checked")
        rr(">")
      else
        if theactive = "on" then rr("v")
      end if
      if oksee("board") and session(tablename & "containerfield") <> "board" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("board") then
      elseif session(tablename & "containerfield") = "board" then
        rr("<input type=hidden name=listupdate_board_" & theid & " value=" & theboard & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "board" then
        rr("<input type=checkbox name=listupdate_board_" & theid & "")
        if theboard = "on" then rr(" checked")
        rr(">")
      else
        if theboard = "on" then rr("v")
      end if
      if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then rr("<td class=list_item valign=top align=><nobr>" & thesupplieridlookup)
      if oksee("passengers") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(thepassengers,0,true,false,true))
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
      if oksee("rate0") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(therate0,2,true,false,true))
      if oksee("ratekm") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(theratekm,0,true,false,true))
      if oksee("rate1") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(therate1,2,true,false,true))
      if oksee("rent1") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(therent1,2,true,false,true))
      if oksee("rent2") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(therent2,2,true,false,true))
      if oksee("rent3") then rr("<td class=list_item valign=top align=right dir=ltr><nobr>" & formatnumber(therent3,2,true,false,true))
'[      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
'[      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
'[      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
		  if showcarfleet = "on" then
		    rr "<td class=list_item><nobr>" & getval(thedata1, "carowner")
		    rr "<td class=list_item><nobr>" & getval(thedata1, "carownerjob")
		    rr "<td class=list_item><nobr>" & getval(thedata1, "datereceived")
		    rr "<td class=list_item><nobr>" & getval(thedata1, "testexpires")
		    rr "<td class=list_item><nobr>" & getval(thedata1, "insuranceend")
		    rr "<td class=list_item><nobr>" & getval(thedata1, "insuranceagent")
		    rr "<td class=list_item><nobr>" & getval(thedata1, "kilometer")
		  end if
      t = thedata1 & ""
      t = replace(t,"<br>"," ") : t = replace(t,"<","[") : t = replace(t,">","]") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & t)
']
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("description") and session(tablename & "containerfield") <> "description" then rr("<td class=list_total1>") 'description
  if oksee("code") and session(tablename & "containerfield") <> "code" then rr("<td class=list_total1>") 'code
  if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_total1>") 'active
  if oksee("board") and session(tablename & "containerfield") <> "board" then rr("<td class=list_total1>") 'board
  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then rr("<td class=list_total1>") 'supplierid
  if oksee("passengers") and session(tablename & "containerfield") <> "passengers" then rr("<td class=list_total1>") 'passengers
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid
  if oksee("rate0") and session(tablename & "containerfield") <> "rate0" then rr("<td class=list_total1>") 'rate0
  if oksee("ratekm") and session(tablename & "containerfield") <> "ratekm" then rr("<td class=list_total1>") 'ratekm
  if oksee("rate1") and session(tablename & "containerfield") <> "rate1" then rr("<td class=list_total1>") 'rate1
  if oksee("rent1") and session(tablename & "containerfield") <> "rent1" then rr("<td class=list_total1>") 'rent1
  if oksee("rent2") and session(tablename & "containerfield") <> "rent2" then rr("<td class=list_total1>") 'rent2
  if oksee("rent3") and session(tablename & "containerfield") <> "rent3" then rr("<td class=list_total1>") 'rent3
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
