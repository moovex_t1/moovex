<!--#include file="include.asp"-->
<%
rr("<html dir=~langdir~><head>" & vbcrlf)
rr("~langmeta~")
rr("<meta name=""robots"" content=""noindex"">")
rr("<link rel=""stylesheet"" rel=""text/css"" href=style.css>")
rr "<body><center><table>"

'----------------------
if getfield("workerid") <> "" then session("workerrides_workerid") = getfield("workerid")
if not hasmatch(session("usergroup"), "manager,adminassistant,admin,") then du = errorend("~Access Denied~")
i = getonefield("select id from workers where id = " & session("workerrides_workerid") &  okreadsql("workers"))
if cstr(i) = "" then du = errorend("~Access Denied~")

if getfield("m") <> "" then session("workerrides_monthview_m") = getfield("m")
if getfield("y") <> "" then session("workerrides_monthview_y") = getfield("y")
rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar style='padding:4;'>")
m = session("workerrides_monthview_m") : if m = "" then m = month(date)
y = session("workerrides_monthview_y") : if y = "" then y = year(date)
prevy = y : prevm = m - 1 : if prevm < 1 then prevm = 12 : prevy = prevy - 1
nexty = y : nextm = m + 1 : if nextm > 12 then nextm = 1 : nexty = nexty + 1
rr(" <a href=?action=set&y=" & prevy & "&m=" & prevm & "><img src=~langside~arrow.png ~iconsize~ border=0></a>")
rr(" <a href=?action=set&y=" & nexty & "&m=" & nextm & "><img src=~langside2~arrow.png ~iconsize~ border=0></a>")

rr "<form name=f1><input type=hidden name=action value=set>"
rr(" ~Month~ <SELECT NAME=m>")
for i = 1 to 12
  rr("<option value=" & i)
  if cstr(m) = cstr(i) then rr(" selected")
  rr(">" & i)
next
rr("</SELECT>")

rr("<SELECT NAME=y>")
for i = year(now) - 20 to year(now) + 20
  rr("<option value=" & i)
  if cstr(y) = cstr(i) then rr(" selected")
  rr(">" & i)
next
rr("</SELECT>")

rr(" <input type=submit value='    ~Show~    '>")
rr("<table></form></table>")

'-------calendar-----------------------------------
rr("<tr><td class=monthdaytitle>~Sunday~<td class=monthdaytitle>~Monday~<td class=monthdaytitle>~Tuesday~<td class=monthdaytitle>~Wednesday~<td class=monthdaytitle>~Thursday~<td class=monthdaytitle>~Friday~<td class=monthdaytitle>~Saturday~")
d = weekday(1 & "/" & m & "/" & y)
rr("<tr>") : for i = 1 to d - 1: rr("<td>") : next
chosen = date : if getfield("d") <> "" then chosen = getfield("d")
for i = 1 to cint(lastday(m & "/" & y))
  d = cdate(i & "/" & m & "/" & y)
  wd = weekday(d) : if wd = 1 then rr "<tr>"
  t = "" : tt = getweekday(d)
  if tt = 6 or tt >= 9 then t = "weekend2"
  if tt = 7 or tt = 8 then t = "weekend"
  if cdate(d) = cdate(chosen) then t = t & "chosen"
  rr "<td class=monthday" & t & ">"
  rr "<table width=100% bgcolor=#eeeeee><tr><td width=100% ><nobr><font style='font-size:18; font-weight:bold; color:#808080;'>" & i & "</font><td><nobr></table>"

  s = "select id,status,courseid,direction,date1,pids,carid from orders "
  s = s & " where date1 >= " & sqldate(d & " 00:00:00") & " and date1 <= " & sqldate(d & " 23:59:59")
  s = s & " and status not in('Canceled','Draft','')"
  s = s & " and instr(cstr(',' & pids), '," & session("workerrides_workerid") & ",') > 0"
  s = s & " order by date1"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1
    pids = rs("pids")
    rr "<div id=d" & rs("id") & " style='position:absolute; visibility:hidden;'></div><nobr>"
    rr "<a target=_top href=admin_orders.asp?containerid=0&action=form&id=" & rs("id") & " style='color:#808080;'>"
    rr "<img src=" & statusicon(rs("status")) & " border=0 width=20 style='vertical-align:top; margin-bottom:1;'> "
    rr "<span style='display:inline-block; margin-top:3; cursor:hand;'>"
    rr rs("id")
    t = getcode("courses",rs("courseid")) : if t <> "" then rr " ~Course~ " & t
    if rs("direction") = "A" then rr " ~Collect~" else if rs("direction") = "B" then rr " ~Distribute~"
    rr " " & getdatehourstring(rs("date1"))
    rr "</span></a>"
    rr "<br>"
    rs.movenext
  loop
next
rr("</table>")

%>