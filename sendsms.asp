<!--#include file="include.asp"-->
<html><body leftmargin=0 topmargin=0 rightmargin=0 bottommargin=0 bgcolor=eeeeee>
<%
if instr(siteurl,"localhost") then
  rr("from: " & request("fromphone") & "<BR>")
  rr("to: " & request("tophones") & "<BR>")
  rr("message:<br>" & request("message") & "<BR>")
end if
if ulev < 1 then response.write "Access Denied" : rend

'-----smsbank
'd = request("tophones") : d = replace(d," ","") : if d <> "" and right(d,1) <> "," then d = d & ","
'units = countstring(d,",")
'u = getparam("smsbank") : u = cdbl("0" & u)
'if u - units < 0 then errorend("SMS Bank units left: " & u & ", sms not sent.")
's = "update busettings set description = cstr(cdbl(description) - " & units & ") where lcase(title) = 'smsbank'"
'closers(rs1) : set rs1 = conn.execute(sqlfilter(s))

'-----send
'x = sendsms("054-3433444","050-4433221","kiki kiki")
x = sendsms(request("fromphone"), request("tophones"), request("message"))
x = replace(x,"<","<!")
response.write x

'=================================================================================
function sendSMS(byval fromphone, byval tophones, byval message)
  Dim DataToSend, xmlhttp, p, a, b

  '--------------------------------------------------------------------------
  if getparam("smshost") = "cellact" then
    fromphone = strfilter(fromphone, "0123456789")
    tophones = strfilter(tophones, "0123456789")
    if len(fromphone) <> "10" then response.write "SMS: sender phone must be xxx-xxxxxxx" : response.end
    if len(tophones) <> "10" then response.write "SMS: destination phone must be xxx-xxxxxxx" : response.end
    if len(message) = 0 then response.write "SMS: No message to send" : response.end

  	sms = ""
  	sms = sms & "<PALO>"
  	sms = sms & "<HEAD>"
  	sms = sms & "<FROM>" & getparam("smsfrom") & "</FROM>"
  	sms = sms & "<APP USER='" & getparam("smsusername") & "' PASSWORD='" & getparam("smspassword") & "'>LA</APP>"
  	sms = sms & "<CMD>sendtextmt</CMD>"
  	sms = sms & "</HEAD>"
  	sms = sms & "<BODY>"
  	sms = sms & "<SENDER>" & fromphone & "</SENDER>"
  	sms = sms & "<CONTENT>" & cutf8(message) & "</CONTENT>"
  	sms = sms & "<DEST_LIST><TO>" & tophones & "</TO></DEST_LIST>"
  	sms = sms & "</BODY>"
  	sms = sms & "</PALO>"

    DataToSend = "XMLString=" & Server.URLEncode(sms)
  	rr DataToSend
    set xmlhttp = server.Createobject("MSXML2.ServerXMLHTTP")
    xmlhttp.setOption 0, 65001
  	xmlhttp.Open "POST", "http://la.cellactpro.com/unistart5.asp" , false
  		
    xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    xmlhttp.send(DataToSend)
    sendSMS = xmlhttp.responsetext
    set xmlhttp = nothing

  '--------------------------------------------------------------------------
  elseif getparam("smshost") = "goldman" then
    fromphone = strfilter(fromphone,"0123456789")
    if len(fromphone) <> "10" then response.write "SMS: sender phone must be 10 digits" : response.end
    fromphone = left(fromphone,3) & "-" & mid(fromphone,4)
    'tophones = strfilter(tophones,"0123456789")
    'if len(tophones) <> "10" then response.write "SMS: destination phone must be 10 digits" : response.end
    'tophones = left(tophones,3) & "-" & mid(tophones,4)
    if len(message) = 0 then response.write "SMS: No message to send" : response.end

    a = "<SMS2U>"
    a = a & "<USERNAME>" & getparam("smsusername") & "</USERNAME>"
    a = a & "<PASSWORD>" & getparam("smspassword") & "</PASSWORD>"
    a = a & "<SENDER>" & fromphone & "</SENDER>"
    a = a & "<MSGLNG>HEB</MSGLNG>"
    a = a & "<MSG><![CDATA[" & cutf8(message) & "]]></MSG>"
    a = a & "<NUMLIST>"
    pp = tophones & ","
    do until pp = ""
      p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
      p = strfilter(p,"0123456789")
      if len(p) = 10 then
        p = left(p,3) & "-" & mid(p,4)
        a = a & "<CELLNUMBER>" & p & "</CELLNUMBER>"
      end if
    loop
    a = a & "</NUMLIST></SMS2U>"
    DataToSend = "XMLString=" & Server.URLEncode(a)
    'set xmlhttp = server.Createobject("MSXML2.ServerXMLHTTP.5.0")
    set xmlhttp = server.Createobject("MSXML2.ServerXMLHTTP")
    xmlhttp.setOption 0, 65001
    xmlhttp.Open "POST", "http://goldman.co.il/SendSMSAPI/SMSAPI.asmx/SendXMLSMS", false
    xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    xmlhttp.send(DataToSend)
    sendSMS = xmlhttp.responsetext
    set xmlhttp = nothing
  end if
end function

Function CUTF8(s)
  Dim c, t, n
  t = ""
  For n = 1 To Len(s)
    c = AscW(Mid(s, n, 1))
    If c < 128 Then
      t = t & Mid(s, n, 1)
    ElseIf ((c > 127) And (c < 2048)) Then
      t = t & Chr(((c \ 64) Or 192))              '((c>>6)|192);
      t = t & Chr(((c And 63) Or 128))            '((c&63)|128);}
    Else
      t = t & Chr(((c \ 144) Or 234))             '((c>>12)|224);
      t = t & Chr((((c \ 64) And 63) Or 128))     '(((c>>6)&63)|128);
      t = t & Chr(((c And 63) Or 128))            '((c&63)|128);
    End If
  Next
  CUTF8 = t
End Function
rend
%>
