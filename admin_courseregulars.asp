<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 04/01/2016 09:00:50

function courseregularsdelitem(i)
  courseregularsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function courseregularsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("days") <> "" then t = getfield("days")
  f = f & "days," : v = v & "'" & t & "',"

  t = "" : if getfield("direction") <> "" then t = getfield("direction")
  f = f & "direction," : v = v & "'" & t & "',"

  t = "" : if getfield("hours") <> "" then t = getfield("hours")
  f = f & "hours," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "supplierid" then t = session(tablename & "containerid")
  if getfield("supplierid") <> "" then t = getfield("supplierid")
  f = f & "supplierid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "carid" then t = session(tablename & "containerid")
  if getfield("carid") <> "" then t = getfield("carid")
  f = f & "carid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "courseid" then t = session(tablename & "containerid")
  if getfield("courseid") <> "" then t = getfield("courseid")
  f = f & "courseid," : v = v & t & ","

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into courseregulars(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from courseregulars"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  courseregularsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table courseregulars (id autoincrement primary key,days text(50),direction text(2),hours memo,supplierid number,carid number,courseid number,data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "courseregulars" : session("tablename") = "courseregulars"
tablefields = "id,days,direction,hours,supplierid,carid,courseid,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = courseregularsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("days") then s = s & "days='" & getfield("days") & "',"
  if okedit("direction") then s = s & "direction='" & getfield("direction") & "',"
  if okedit("hours") then s = s & "hours='" & getfield("hours") & "',"
  if okedit("supplierid") then s = s & "supplierid=0" & getfield("supplierid") & ","
  if okedit("carid") then s = s & "carid=0" & getfield("carid") & ","
  if okedit("courseid") then s = s & "courseid=0" & getfield("courseid") & ","
  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(days,direction,hours,supplierid,carid,courseid,data1)"
  s = s & " SELECT days,direction,hours,supplierid,carid,courseid,data1"
  s = s & " FROM " & tablename & " where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  request_action = "form"
  request_id = theid
  msg = msg & " ~Record duplicated~"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = courseregularsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = courseregularsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
'[      s = ""
      s = ""
      if getfield("multisupplierid") <> "" then s = s & ", supplierid = " & getfield("multisupplierid")
']

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("days") then csv(0) = csv(0) & "~courseregulars_days~^"
  if oksee("direction") then csv(0) = csv(0) & "~courseregulars_direction~^"
  if oksee("hours") then csv(0) = csv(0) & "~courseregulars_hours~^"
  if oksee("supplierid") then csv(0) = csv(0) & "~courseregulars_supplierid~^"
  if oksee("carid") then csv(0) = csv(0) & "~courseregulars_carid~^"
  if oksee("courseid") then csv(0) = csv(0) & "~courseregulars_courseid~^"
  if oksee("data1") then csv(0) = csv(0) & "~courseregulars_data1~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
  Thedirection = rs("direction") : if isnull(Thedirection) then Thedirection = ""
  Thehours = rs("hours") : if isnull(Thehours) then Thehours = ""
  Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
  Thesupplierid = replace(thesupplierid,",",".")
  Thecarid = rs("carid") : if isnull(Thecarid) then Thecarid = 0
  Thecarid = replace(thecarid,",",".")
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thesupplieridlookup = rs("supplieridlookup") : if isnull(Thesupplieridlookup) then Thesupplieridlookup = ""
  Thecaridlookup = rs("caridlookup") : if isnull(Thecaridlookup) then Thecaridlookup = ""
  Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("days") then csv(rc) = csv(rc) & replace(thedays,vbcrlf," ") & "^"
    if oksee("direction") then csv(rc) = csv(rc) & replace(thedirection,vbcrlf," ") & "^"
    if oksee("hours") then csv(rc) = csv(rc) & replace(thehours,vbcrlf," ") & "^"
    if oksee("supplierid") then csv(rc) = csv(rc) & thesupplieridlookup & "^"
    if oksee("carid") then csv(rc) = csv(rc) & thecaridlookup & "^"
    if oksee("courseid") then csv(rc) = csv(rc) & thecourseidlookup & "^"
    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      supplierid = "0"
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "days" then
          if len(b) > 50 then b = left(b,50)
          if okedit("days") then s = s & "days='" & b & "',"
        elseif csvff(i) = "direction" then
          if len(b) > 2 then b = left(b,2)
          if okedit("direction") then s = s & "direction='" & b & "',"
        elseif csvff(i) = "hours" then
          if okedit("hours") then s = s & "hours='" & b & "',"
        elseif csvff(i) = "supplierid" then
          if b = "" then
            v = 0
          else
            sq = "select * from suppliers where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("supplierid") then s = s & "supplierid=" & v & ","
          if okedit("supplierid") then s = s & "supplierid=" & v & ","
          supplierid = v
']
        elseif csvff(i) = "carid" then
          if b = "" then
            v = 0
          else
'[            sq = "select * from cars where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            sq = "select * from cars where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            if supplierid <> "0" then sq = sq & " and supplierid = " & supplierid
']
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("carid") then s = s & "carid=" & v & ","
        elseif csvff(i) = "courseid" then
          if b = "" then
            v = 0
          else
'[            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "'"
            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "' or lcase(cstr('' & code))= '" & lcase(b) & "'"
']
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("courseid") then s = s & "courseid=" & v & ","
        elseif csvff(i) = "data1" then
          if okedit("data1") then s = s & "data1='" & b & "',"
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = courseregularsinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","courseregulars.id,courseregulars.days,courseregulars.direction,courseregulars.hours,courseregulars.supplierid,courseregulars.carid,courseregulars.courseid,courseregulars.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
  Thedirection = rs("direction") : if isnull(Thedirection) then Thedirection = ""
  Thehours = rs("hours") : if isnull(Thehours) then Thehours = ""
  Thehours = replace(thehours,"<br>",vbcrlf)
  Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
  Thesupplierid = replace(thesupplierid,",",".")
  Thecarid = rs("carid") : if isnull(Thecarid) then Thecarid = 0
  Thecarid = replace(thecarid,",",".")
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "days,direction,hours,supplierid,carid,courseid,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/full-time.png' ~iconsize~> ~courseregulars_courseregulars~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("days") then'form:days
    rr("<tr valign=top id=days_tr0><td class=form_item1 id=days_tr1><span id=days_caption>~courseregulars_days~</span><td class=form_item2 id=days_tr2>")
'[    c = 0 : s = "" : a = "1,2,3,4,5,6,7,"
    c = 0 : s = "" : a = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
']
    cc = countstring(a,",")
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
      rr("<input type=checkbox name=days_c" & c)
      if instr("," & thedays & ",","," & b & ",") > 0 then rr(" checked")
      rr(" onclick='vbscript:populatedays'>")
'[      rr(b & " ")
      rr("~weekday" & b & "~&nbsp;&nbsp;&nbsp;")
']
      s = s & "if f1.days_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
    loop
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub populatedays" & vbcrlf)
    rr("    t = """"" & vbcrlf)
    rr(s & vbcrlf)
    rr("    f1.days.value = t" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("</script>" & vbcrlf)
    rr("<input type=hidden name=days value=""" & Thedays & """>")
  end if'form:days

  if oksee("direction") then'form:direction
    rr("<tr valign=top id=direction_tr0><td class=form_item1 id=direction_tr1><span id=direction_caption>~courseregulars_direction~</span><td class=form_item2 id=direction_tr2>")
    a = "A,B,"
    rr("<select name=direction dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Thedirection = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:direction
  end if'form:direction

  if oksee("hours") then'form:hours
    rr("<tr valign=top id=hours_tr0><td class=form_item1 id=hours_tr1><span id=hours_caption>~courseregulars_hours~</span><td class=form_item2 id=hours_tr2>")
'[    rr("<textarea name=hours style='width:301; height:100;' dir=ltr>" & thehours & "</textarea>")
    rr " *~Format 00:00, break separated, can type each hour multiple times for multiple rides at the same time~<br>"
    if specs = "brom" then rr "* ���� �� ����� ��� `���` ���� ���� ��� ����<br>"
    rr("<textarea name=hours style='width:301; height:100;' dir=ltr>" & thehours & "</textarea>")
']
  end if'form:hours

  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then
    rr("<tr valign=top id=supplierid_tr0><td class=form_item1 id=supplierid_tr1><span id=supplierid_caption>~courseregulars_supplierid~</span><td class=form_item2 id=supplierid_tr2>")
    if getfield("box") = "supplierid" then thesupplierid = getfield("boxid")
'[    rr("<select name=supplierid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~courseregulars_supplierid~")
'[    sq = "select * from suppliers order by title"
    rr("<select name=supplierid dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : checkform'>")
    rr("<option value=0 style='color:bbbbfe;'>~courseregulars_supplierid~")
    i = getidlist("select siteid from courses where id = " & thecourseid) : if i = "" then i = "0,"
    sq = "select * from suppliers where siteid in(" & i & "0) order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesupplierid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:supplierid
  elseif okedit("supplierid") then
    rr("<input type=hidden name=supplierid value=" & thesupplierid & ">")
  end if'form:supplierid

  if oksee("carid") and session(tablename & "containerfield") <> "carid" then
    rr("<tr valign=top id=carid_tr0><td class=form_item1 id=carid_tr1><span id=carid_caption>~courseregulars_carid~</span><td class=form_item2 id=carid_tr2>")
    if getfield("box") = "carid" then thecarid = getfield("boxid")
    rr("<select name=carid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~courseregulars_carid~")
'[    sq = "select * from cars order by title"
    rr("<option value=0 style='color:bbbbfe;'>~courseregulars_carid~")
    sq = "select * from cars where supplierid = " & thesupplierid & " order by title"
']
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thecarid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:carid
  elseif okedit("carid") then
    rr("<input type=hidden name=carid value=" & thecarid & ">")
  end if'form:carid

  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    rr("<tr valign=top id=courseid_tr0><td class=form_item1 id=courseid_tr1><span id=courseid_caption>~courseregulars_courseid~</span><td class=form_item2 id=courseid_tr2>")
    if getfield("box") = "courseid" then thecourseid = getfield("boxid")
    rr("<select name=courseid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~courseregulars_courseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thecourseid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:courseid
  elseif okedit("courseid") then
    rr("<input type=hidden name=courseid value=" & thecourseid & ">")
  end if'form:courseid

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
'[    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
    rr("<tr valign=top><td class=form_item1>~Type~<td class=form_item2>")
    rr "<input type=checkbox name=asshuttle onclick='vbscript: f1.data1.value = setval(f1.data1.value, ""asshuttle"", iif(me.checked,""on"",""""))'"
    if getval(thedata1, "asshuttle") = "on" then rr " checked"
    rr "> ~Create as shuttle ride~"
']
  end if'form:data1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonduplicate value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
'[  rr("  sub checkform" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr checkformadd
']
  rr("    ok = true" & vbcrlf)
  rr(checkformadd)

  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.hours.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".id" : session(tablename & "_hidelistcolumns") = "data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".id"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/full-time.png' ~iconsize~> ~courseregulars_courseregulars~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  rr(" <nobr><img id=direction_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=direction dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~courseregulars_direction~")
  aa = "A,B," : a = aa
  'sq = "select distinct direction from courseregulars order by direction"
  'set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof : a = a & rs1("direction") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if session(tablename & "_direction") = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select></nobr>")'search:direction
  if session(tablename & "containerfield") <> "supplierid" then
    rr(" <nobr><img id=supplierid_filtericon src=icons/i_key.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=supplierid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~courseregulars_supplierid~")
    sq = "select * from suppliers order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_supplierid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:supplierid
  end if'search:supplierid
  if session(tablename & "containerfield") <> "carid" then
    rr(" <nobr><img id=carid_filtericon src=icons/i_car.gif style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=carid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~courseregulars_carid~")
    sq = "select * from cars order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_carid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:carid
  end if'search:carid
  if session(tablename & "containerfield") <> "courseid" then
    rr(" <nobr><img id=courseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=courseid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~courseregulars_courseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_courseid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:courseid
'[  end if'search:courseid

    if session("usersiteid") = "0" then
	    rr(" <nobr><img id=coursesiteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
	    rr(" <select style='width:150;' name=coursesiteid dir=~langdir~>")
	    rr("<option value='' style='color:bbbbfe;'>~Site~")
	    sq = "select * from sites order by title"
	    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
	    do until rs1.eof
	      rr("<option value=""" & rs1("id") & """")
	      if cstr(session(tablename & "_coursesiteid")) = cstr(rs1("id")) then rr(" selected")
	      rr(">")
	      rr(rs1("title") & " ")
	      rs1.movenext
	    loop
	    rr("</select></nobr>")
    end if

  end if'search:courseid


']
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & suppliers_1.title) as supplieridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join suppliers suppliers_1 on " & tablename & ".supplierid = suppliers_1.id)"
  s = replace(s, " from ", ", cstr('' & cars_1.title) as caridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join cars cars_1 on " & tablename & ".carid = cars_1.id)"
'[  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", courses_1.siteid as coursesiteid from ",1,1,1)
']

'[  '-sqladd
  t = session(tablename & "_coursesiteid")
  if t <> "" then s = s & " and courseid in(select id from courses where siteid = " & t & ")"
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","courseregulars.id,courseregulars.days,courseregulars.direction,courseregulars.hours,courseregulars.supplierid,courseregulars.carid,courseregulars.courseid,courseregulars.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_direction") <> "" then s = s & " and lcase(" & tablename & ".direction) = '" & lcase(session(tablename & "_direction")) & "'"
  if session(tablename & "_supplierid") <> "" and session(tablename & "containerfield") <> "supplierid" then s = s & " and " & tablename & ".supplierid = " & session(tablename & "_supplierid")
  if session(tablename & "_carid") <> "" and session(tablename & "containerfield") <> "carid" then s = s & " and " & tablename & ".carid = " & session(tablename & "_carid")
  if session(tablename & "_courseid") <> "" and session(tablename & "containerfield") <> "courseid" then s = s & " and " & tablename & ".courseid = " & session(tablename & "_courseid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".days like '%" & w & "%' or"
        s = s & " " & tablename & ".direction like '%" & w & "%' or"
        s = s & " " & tablename & ".hours like '%" & w & "%' or"
        s = s & " cstr('' & suppliers_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & cars_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("days") then f = "courseregulars.days" : f2 = "days" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("days") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courseregulars_days~</a> " & a2)
  if oksee("direction") then f = "courseregulars.direction" : f2 = "direction" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("direction") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courseregulars_direction~</a> " & a2)
  if oksee("hours") then rr("<td class=list_title><nobr>~courseregulars_hours~")
  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then
    f = "suppliers_1.title" : f2 = "supplieridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courseregulars_supplierid~</a> " & a2)
  end if
  if oksee("carid") and session(tablename & "containerfield") <> "carid" then
    f = "cars_1.title" : f2 = "caridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courseregulars_carid~</a> " & a2)
  end if
'[  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
'[    f = "courses_1.code" : f2 = "courseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courseregulars_courseid~</a> " & a2)
'[  end if
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    f = "courses_1.code" : f2 = "courseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~courseregulars_courseid~</a> " & a2)
  end if
  f = "courses_1.siteid" : f2 = "coursesiteid" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~Site~</a> " & a2)
']
  if oksee("data1") then rr("<td class=list_title><nobr>~courseregulars_data1~")

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
      Thedirection = rs("direction") : if isnull(Thedirection) then Thedirection = ""
      Thehours = rs("hours") : if isnull(Thehours) then Thehours = ""
      Thesupplierid = rs("supplierid") : if isnull(Thesupplierid) then Thesupplierid = 0
      Thesupplierid = replace(thesupplierid,",",".")
      Thecarid = rs("carid") : if isnull(Thecarid) then Thecarid = 0
      Thecarid = replace(thecarid,",",".")
      Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
      Thecourseid = replace(thecourseid,",",".")
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thesupplieridlookup = rs("supplieridlookup") : if isnull(Thesupplieridlookup) then Thesupplieridlookup = ""
      Thecaridlookup = rs("caridlookup") : if isnull(Thecaridlookup) then Thecaridlookup = ""
      Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
'[      firstcoltitle = thedays
      t = thedays : for i = 20 to 1 step -1 : t = replace(t, i & ",", "~weekday" & i & "~,") : next
      if right(t,1) = "," then t = left(t,len(t)-1)
      firstcoltitle = t
']
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/full-time.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("direction") then rr("<td class=list_item valign=top align=><nobr>" & thedirection)
      thehours = thehours : if isnull(thehours) then thehours = ""
      thehours = replace(thehours,"<br>"," ") : thehours = replace(thehours,"<","[") : thehours = replace(thehours,">","]") : if len(thehours) > 30 then thehours = left(thehours,30) & "..."
      if oksee("hours") then rr("<td class=list_item valign=top align=><nobr>" & thehours)
      if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then rr("<td class=list_item valign=top align=><nobr>" & thesupplieridlookup)
      if oksee("carid") and session(tablename & "containerfield") <> "carid" then rr("<td class=list_item valign=top align=><nobr>" & thecaridlookup)
'[      if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_item valign=top align=><nobr>" & thecourseidlookup)
      if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_item valign=top align=><nobr>" & thecourseidlookup)
      rr "<td class=list_item valign=top align=><nobr>" & gettitle("sites", rs("coursesiteid"))
']
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("direction") and session(tablename & "containerfield") <> "direction" then rr("<td class=list_total1>") 'direction
  if oksee("hours") and session(tablename & "containerfield") <> "hours" then rr("<td class=list_total1>") 'hours
  if oksee("supplierid") and session(tablename & "containerfield") <> "supplierid" then rr("<td class=list_total1>") 'supplierid
  if oksee("carid") and session(tablename & "containerfield") <> "carid" then rr("<td class=list_total1>") 'carid
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_total1>") 'courseid
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
'[  if ulev > 1 then
'[
'[    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  if session("userlevel") = "5" then
	  rr " ~Supplier~ <select name=multisupplierid><option value=''>"
	  s = "select id,title from suppliers order by title"
	  set rs = conn.execute(sqlfilter(s))
	  do until rs.eof
	    rr "<option value=" & rs("id") & ">" & rs("title")
	    rs.movenext
	  loop
	  rr "</select>"
    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
']
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
