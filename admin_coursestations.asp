<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 03/06/2013 13:13:38

function coursestationsdelitem(i)
  coursestationsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function coursestationsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "0" : if getfield("minutes") <> "" then t = getfield("minutes")
  f = f & "minutes," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "courseid" then t = session(tablename & "containerid")
  if getfield("courseid") <> "" then t = getfield("courseid")
  f = f & "courseid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "stationid" then t = session(tablename & "containerid")
  if getfield("stationid") <> "" then t = getfield("stationid")
  f = f & "stationid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
  if getfield("siteid") <> "" then t = getfield("siteid")
  f = f & "siteid," : v = v & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into coursestations(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from coursestations"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  coursestationsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table coursestations (id autoincrement primary key,minutes number,courseid number,stationid number,siteid number)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "coursestations" : session("tablename") = "coursestations"
tablefields = "id,minutes,courseid,stationid,siteid,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
hidecontrols = hidecontrols & "siteid,"
if session(tablename & "containerid") <> "" then if not okwrite("courses", session(tablename & "containerid")) then ulev = 1
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = coursestationsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("minutes") then s = s & "minutes=0" & getfield("minutes") & ","
  if okedit("courseid") then s = s & "courseid=0" & getfield("courseid") & ","
  if okedit("stationid") then s = s & "stationid=0" & getfield("stationid") & ","
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'['-------------afterupdate-------------
if request_action = "update" then
  ii = getidlist("select id from courserates where courseid = " & getfield("courseid") & " and instr(cstr('' & data1), 'relativeprice=on') + instr(cstr('' & data1), 'relativeprice&#61;on') > 0")
  if ii <> "" then du = showerror("������ �� �� �������� ������, ��� ���� �� ������ �� ����� �������� ������ - <a target=_top href=admin_courserates.asp?containerid=0&action=search&action2=clear&showids=" & ii & ">��� ��� ����� ���������</a>")
end if

']
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = coursestationsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = coursestationsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
'[    if okwrite(tablename,b) then

    if session("usergroup") = "admin" and session("usersiteid") = "0" and trim(getfield("stationappend")) <> "" then
      a = trim(getfield("stationappend")) : a = chtm(a)
      sq = "select stationid from coursestations where id = " & b
      set rs1 = conn.execute(sqlfilter(sq))
      if not rs1.eof then
        sq = "update stations set title = left(cstr(title) & ' " & a & "',50) where id = " & rs1("stationid")
        set rs1 = conn.execute(sqlfilter(sq))
      end if
    end if

    if okwrite(tablename,b) then
']
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  csv(0) = csv(0) & "~coursestations_minutes~^"
  csv(0) = csv(0) & "~coursestations_courseid~^"
  csv(0) = csv(0) & "~coursestations_stationid~^"
  csv(0) = csv(0) & "~coursestations_siteid~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Theminutes = rs("minutes") : if isnull(Theminutes) then Theminutes = 0
  Theminutes = replace(theminutes,",",".") : Theminutes = formatnumber(theminutes,0,true,false,false)
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thestationid = rs("stationid") : if isnull(Thestationid) then Thestationid = 0
  Thestationid = replace(thestationid,",",".")
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
  Thestationidlookup = rs("stationidlookup") : if isnull(Thestationidlookup) then Thestationidlookup = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    csv(rc) = csv(rc) & replace(theminutes,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & thecourseidlookup & "^"
    csv(rc) = csv(rc) & thestationidlookup & "^"
    csv(rc) = csv(rc) & thesiteidlookup & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      siteid = session("usersiteid")
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "minutes" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("minutes") then s = s & "minutes=" & b & ","
'[        elseif csvff(i) = "courseid" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("courseid") then s = s & "courseid=" & v & ","
        elseif csvff(i) = "courseid" then
          if b = "" then
            v = 0
          else
            sq = "select * from courses where code = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if v = 0 then rr "~Not found~: " & b & "<br>"
          if okedit("courseid") then s = s & "courseid=" & v & ","
']
'[        elseif csvff(i) = "stationid" then
'[          if b = "" then
'[            v = 0
'[          else
'[            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
'[            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[            if rs1.eof then v = 0 else v = rs1("id")
'[          end if
'[          if okedit("stationid") then s = s & "stationid=" & v & ","
        elseif csvff(i) = "stationid" then
          if b = "" then
            v = 0
          else
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            if v = 0 then
              sq = "insert into stations(title, siteid, special) values('" & left(b,50) & "'," & siteid & ", '')"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from stations"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              v = rs1("id")
            end if
          end if
          if okedit("stationid") then s = s & "stationid=" & v & ","
']
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("siteid") then s = s & "siteid=" & v & ","
          if okedit("siteid") then s = s & "siteid=" & v & ","
          if session("usersiteid") = "0" then siteid = v
']
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = coursestationsinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","coursestations.id,coursestations.minutes,coursestations.courseid,coursestations.stationid,coursestations.siteid",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Theminutes = rs("minutes") : if isnull(Theminutes) then Theminutes = 0
  Theminutes = replace(theminutes,",",".") : Theminutes = formatnumber(theminutes,0,true,false,false)
  Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
  Thecourseid = replace(thecourseid,",",".")
  Thestationid = rs("stationid") : if isnull(Thestationid) then Thestationid = 0
  Thestationid = replace(thestationid,",",".")
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  formcontrols = "minutes,courseid,stationid,siteid,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock
  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_home.gif' ~iconsize~> ~coursestations_coursestations~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

'[  '-------------controls--------------------------
  if ulev <= 1 then okwritehere = false
']
  if oksee("minutes") then'form:minutes
    rr("<tr valign=top id=minutes_tr0><td class=form_item1 id=minutes_tr1><span id=minutes_caption>~coursestations_minutes~<img src=must.gif></span><td class=form_item2 id=minutes_tr2>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=minutes value=""" & Theminutes & """>")
  end if'form:minutes

  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    rr("<tr valign=top id=courseid_tr0><td class=form_item1 id=courseid_tr1><span id=courseid_caption>~coursestations_courseid~</span><td class=form_item2 id=courseid_tr2>")
    if getfield("box") = "courseid" then thecourseid = getfield("boxid")
    rr("<select name=courseid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~coursestations_courseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thecourseid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:courseid
  elseif okedit("courseid") then
    rr("<input type=hidden name=courseid value=" & thecourseid & ">")
  end if'form:courseid

  if oksee("stationid") and session(tablename & "containerfield") <> "stationid" then
    rr("<tr valign=top id=stationid_tr0><td class=form_item1 id=stationid_tr1><span id=stationid_caption>~coursestations_stationid~</span><td class=form_item2 id=stationid_tr2>")
    if getfield("box") = "stationid" then thestationid = getfield("boxid")
    rr("<select name=stationid dir=~langdir~>")
'[    rr("<option value=0 style='color:bbbbfe;'>~coursestations_stationid~")
'[    sq = "select * from stations order by title"
'[    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[    do until rs1.eof
'[      rr("<option value=""" & rs1("id") & """")
'[      if cstr(Thestationid) = cstr(rs1("id")) then rr(" selected")
'[      rr(">")
'[      rr(rs1("title") & " ")
'[      rs1.movenext
'[    loop
'[    rr("</select>")'form:stationid
    s = "select siteid from courses where id = " & thecourseid
    set rs1 = conn.execute(sqlfilter(s))
    siteid = 0 : if not rs1.eof then siteid = rs1("siteid")
    rr("<option value=0 style='color:bbbbfe;'>~coursestations_stationid~")
    'sq = "select id,title,siteid from stations where id = " & thestationid & " or siteid = " & siteid & " or isnull(siteid) order by title"
    sq = "select id,title,siteid from stations where id = " & thestationid & " or siteid = " & siteid & " or siteid = 0 order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thestationid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      if cstr("" & rs1("siteid")) = "0" then rr " *"
      rs1.movenext
    loop
    rr("</select>")'form:stationid
    rr " <a href=#aa onclick='vbscript: parent.document.location = ""admin_stations.asp?containerid=0&action=form&id="" & f1.stationid.value'>~Map~</a>"
']
  elseif okedit("stationid") then
    rr("<input type=hidden name=stationid value=" & thestationid & ">")
  end if'form:stationid

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~coursestations_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~coursestations_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("minutes") and session(tablename & "containerfield") <> "minutes" then
    rr("    if isnumeric(f1.minutes.value) then" & vbcrlf)
    rr("      if cdbl(f1.minutes.value) >= -4000000000 and cdbl(f1.minutes.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""minutes"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""minutes"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""minutes"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.minutes.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".minutes" : session(tablename & "_hidelistcolumns") = ""
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

'[  '-------------listsession-------------
  coursedata1 = "" : if session(tablename & "containerid") <> "" then coursedata1 = getonefield("select data1 from courses where id = " & session(tablename & "containerid"))
  if request_action = "priorityshift" then
    ff = getfield("field") : i1 = request_id
    s = "select * from " & tablename & " where id = " & i1
    closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
    t = "" : if session(tablename & "containerid") <> "0" then t = " and " & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
    p1 = rs1(ff) : t1 = " < " : t2 = " desc" : if request_action2 = "down" then t1 = " > " : t2 = ""
    s = "select * from " & tablename & " where id <> " & i1 & " and " & ff & t1 & p1 & t & " order by " & ff & t2
    closers(rs2) : set rs2 = conn.execute(sqlfilter(s))
    if not rs2.eof then
      i2 = rs2("id") : p2 = rs2(ff)
      s = "update " & tablename & " set " & ff & " = " & p2 & " where id = " & i1
      closers(rs) : set rs = conn.execute(sqlfilter(s))
      s = "update " & tablename & " set " & ff & " = " & p1 & " where id = " & i2
      closers(rs) : set rs = conn.execute(sqlfilter(s))
    end if

    ii = getidlist("select stationid from coursestations where courseid = 0" & session(tablename & "containerid") & " order by minutes")
    rebuildcourseoptimize = 0
    du = createcourse(session(tablename & "containerid"), ii)

  end if
']

  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
'[  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 100
']
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".minutes"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/i_home.gif' ~iconsize~> ~coursestations_coursestations~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  if session(tablename & "containerfield") <> "courseid" then
    rr(" <nobr><img id=courseid_filtericon src=icons/upcoming-work.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=courseid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~coursestations_courseid~")
    sq = "select * from courses order by code,title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_courseid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("code") & " ")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:courseid
  end if'search:courseid
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~coursestations_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
  end if'search:siteid
  rr(" <input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y>~Advanced Search~</a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
'[  rr("</tr></form>")'search
  rr("</tr></form>")'search
  if NOT hasmatch(specs, "jerusalem,muni,brener,") then
    rr "<tr><td colspan=99><center><font color=800000>~Please specify stations in chronological order, including minutes, in the collect direction course~"
  end if
']

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & stations_1.title) as stationidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join stations stations_1 on " & tablename & ".stationid = stations_1.id)"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

'[  '-sqladd
  if session("usersiteid") <> "0" then s = s & " and courseid in(" & getidlist("select id from courses where siteid = " & session("usersiteid")) & "0)"
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","coursestations.id,coursestations.minutes,coursestations.courseid,coursestations.stationid,coursestations.siteid",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_courseid") <> "" and session(tablename & "containerfield") <> "courseid" then s = s & " and " & tablename & ".courseid = " & session(tablename & "_courseid")
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        if isnumeric(w) then s = s & " " & tablename & ".minutes = " & w & " or"
        s = s & " cstr('' & courses_1.code) & ' ' & cstr('' & courses_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & stations_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
'[  s = s & " order by " & session(tablename & "_order")
  if hasmatch(specs, "jerusalem,muni,brener,") then
    session(tablename & "_order") = tablename & ".minutes"
    if getval(coursedata1, "ways") = "B" then session(tablename & "_order") = tablename & ".minutes desc"
  end if
  s = s & " order by " & session(tablename & "_order")
']
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("minutes") then f = "coursestations.minutes" : f2 = "minutes" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("minutes") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~coursestations_minutes~</a> " & a2)
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then
    f = "courses_1.code" : f2 = "courseidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~coursestations_courseid~</a> " & a2)
  end if
  if oksee("stationid") and session(tablename & "containerfield") <> "stationid" then
    f = "stations_1.title" : f2 = "stationidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~coursestations_stationid~</a> " & a2)
  end if
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~coursestations_siteid~</a> " & a2)
  end if

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")

  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Theminutes = rs("minutes") : if isnull(Theminutes) then Theminutes = 0
      Theminutes = replace(theminutes,",",".") : Theminutes = formatnumber(theminutes,0,true,false,false)
      Thecourseid = rs("courseid") : if isnull(Thecourseid) then Thecourseid = 0
      Thecourseid = replace(thecourseid,",",".")
      Thestationid = rs("stationid") : if isnull(Thestationid) then Thestationid = 0
      Thestationid = replace(thestationid,",",".")
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Thecourseidlookup = rs("courseidlookup") : if isnull(Thecourseidlookup) then Thecourseidlookup = ""
      Thestationidlookup = rs("stationidlookup") : if isnull(Thestationidlookup) then Thestationidlookup = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = theminutes
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
'[      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
'[      rr("<img src=icons/i_home.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")

      if hasmatch(specs, "jerusalem,muni,brener,") and session(tablename & "_order") = tablename & ".minutes" and ulev > 1 then
        rr(" <a href=?action=priorityshift&action2=up&field=minutes&id=" & theid & "><img src=up.gif border=0></a> &nbsp; ")
        rr(" <a href=?action=priorityshift&action2=down&field=minutes&id=" & theid & "><img src=down.gif border=0></a> &nbsp; ")
      end if

      rr "<img src=icons/i_home.gif ~iconsize~ border=0 style='vertical-align:top;'> "
      'if hasmatch(specs, "jerusalem,") then
      '  h = getval(coursedata1, "hour1") : if not ishour(h) then h = "00:00"
      '  h = "1/1/2016 " & h
      '  if getval(coursedata1, "ways") = "A" then
      '    max = getonefield("select max(minutes) as x from coursestations where courseid = " & session(tablename & "containerid"))
      '    if max = "" then max = 0
      '    h = dateadd("n", -max, h)
      '  end if
      '  t = dateadd("n", theminutes, h) : t = getdatehourstring(t)
      '  rr "<input type=text name=houratstation" & theid & " style='width:50; direction:ltr;' value='" & t & "' onkeydown='vbscript: flist.c" & theid & ".checked = true'> &nbsp;&nbsp;&nbsp;"
      'end if
      rr "<a class=list_item_link href=?action=form&id=" & theid & ">"
      rr firstcoltitle & "</a>"
      t = getval(coursedata1, "legdis" & rc-1) : if t <> "" then rr " <span style='color:#aaaaaa;'>(" & fix(t/100)/10 & " ~km~)</span>"
']
      if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_item valign=top align=><nobr>" & thecourseidlookup)
'[      if oksee("stationid") and session(tablename & "containerfield") <> "stationid" then rr("<td class=list_item valign=top align=><nobr>" & thestationidlookup)
      if oksee("stationid") and session(tablename & "containerfield") <> "stationid" then
        rr "<td class=list_item valign=top align=><nobr>"
        if session(tablename & "_order") = "coursestations.minutes" then rr "<span style='color:#aaaaaa;'>(" & rc & ")</span> "
        rr "<a target=_top href=admin_stations.asp?containerid=0&action=form&id=" & thestationid & ">" & thestationidlookup & "</a>"
        sq = "select siteid,markerx,markery,inmap from stations where id = " & thestationid
        set rs1 = conn.execute(sqlfilter(sq))
        if not rs1.eof then
          t = rs1("siteid") : if isnull(t) then t = "0"
          if cstr(t) = "0" then rr " *"
          if getparam("UseGoogleMaps") = "y" then if rs1("markerx") & "" = "" then rr " <span style='color:#ff0000;'>*</span>"
        end if
        'if rc <> rsrc then
          if hasmatch(specs, "jerusalem,muni,brener,") then
            ii = getidlist("select workerid from weekplan where courseid = 0" & thecourseid)
	          s = "select id,firstname,lastname,unitid from workers where id in(" & ii & "-1) and eligible = 'on'"
            if hasmatch(specs, "muni,brener,") and getval(coursedata1, "ways") = "B" then
              s = s & " and (to2 = " & thestationid & " or to2 = 0 and from1 = " & thestationid & ")"
            else
              s = s & " and from1 = " & thestationid
            end if
            s = s & " order by firstname,lastname"
            set rs1 = conn.execute(sqlfilter(s))
            do until rs1.eof
	            rr " &nbsp;&nbsp;&nbsp; <span style='color:#aaaaaa;'><nobr><a target=_top style='color:#aaaaaa;' href=admin_workers.asp?workersview=-&action=form&id=" & rs1("id") & ">" & rs1("firstname") & " " & rs1("lastname") & " (" & gettitle("units", rs1("unitid")) & ")</a></nobr></span>"
              rs1.movenext
            loop

          else
	          s = "select id from workers where from1 = " & thestationid & " and eligible = 'on'"
	          if session("usersiteid") <> "0" then s = s & " and siteid = " & session("usersiteid")
	          ii = getidlist(s)
	          rr " <span style='color:#aaaaaa;'>(<a target=_top style='color:#aaaaaa;' href=admin_workers.asp?action=search&action2=clear&showids=" & ii & ">" & countstring(ii,",") & " ~Workers~</a>)</span>"
          end if
        'end if
      end if
']
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("courseid") and session(tablename & "containerfield") <> "courseid" then rr("<td class=list_total1>") 'courseid
  if oksee("stationid") and session(tablename & "containerfield") <> "stationid" then rr("<td class=list_total1>") 'stationid
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")
  if ulev > 1 then

'[    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
    if getpermission("stations") > 0 and session(tablename & "containerfield") = "courseid" and session(tablename & "containerid") <> "" and session(tablename & "containerid") <> "0" then
      stationids = getidlist("select stationid from coursestations where courseid = " & session(tablename & "containerid"))
      rr " <input type=button class=groovybutton value='~Station Management~' onclick='vbscript: parent.document.location = ""admin_stations.asp?containerid=0&action=search&action2=clear&showids=" & stationids & """'>"
    end if
    if session("usergroup") = "admin" and session("usersiteid") = "0" then rr " ~Append to station titles~ <input type=text name=stationappend style='width:100;'>"
    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
    rr " <input type=button class=groovybutton value='~Rebuild Course~' onclick='vbscript:if msgbox(""~Rebuild Course~?"",vbyesno) = 6 then parent.document.location = ""admin_courses.asp?action=rebuildcourse&comefrom=coursestations&ids=" & session(tablename & "containerid") & ",&rebuildcourseoptimize="" & flist.rebuildcourseoptimize.value'>"
    rr "<select name=rebuildcourseoptimize style='font-size:10;'><option value=0>~courseoptimize0~<option value=1>~courseoptimize1~<option value=2 selected>~courseoptimize2~</select>"
']
  end if

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
'[  rr("</tr></form>" & bottombar & "</table>")'list
  rr("</tr></form>" & bottombar & "</table>")'list

  if hasmatch(specs, "jerusalem,muni,brener,") and session(tablename & "containerid") <> "0" then
    rr "<center><a href=#aa onclick=""javascript: window.open('map.asp?courseids=" & session(tablename & "containerid") & ",&stationids=-&workerids=-', '_blank')""><b>��� ���� ���</b></a></center><br>"
    rr "<div style='position:absolute; width:100%; height:1000;'></div>" 'disable map
    rr "<center><iframe width=600 height=400 frameborder=0 style='border:1px solid #000000;' src=map.asp?embed=on&courseids=" & session(tablename & "containerid") & ",&stationids=-&workerids=-></iframe>"
  end if
']
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
