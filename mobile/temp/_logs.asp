  if request("sqllog") <> "" then session("sqllog") = request("sqllog")
  if instr(siteurl, "/localhost/") + instr(siteurl, "/127.0.0.1/") > 0 and session("sqllog") = "on" then
    f = "1.htm"
    if didsqllog = "" then didsqllog = "on" : du = addlog(server.mappath(f), "----------" & scriptname & "?" & querystring & "<br><nobr>")
    'timer01 = timer : closers(rs4) : set rs4 = conn.execute(s) : du = addlog(server.mappath(f), formatnumber(timer - timer01,5,true,false,false) & " - ")
     du = addlog(server.mappath(f), s & "<br>")
  end if

'[
  '----------log all changes----------------------------------------
  if getparam("SqlLogPath") <> "" then
    x = 0
    if session("userid") <> "" and left(lcase(s), 6) <> "select" then x = 1
    x = x + instr(s, "from workers where lcase(adusername) =")
    x = x + instr(s, "from buusers where lcase(username) =")
    x = x + instr(s, "from workers where eligible = 'on' and trim(email) =")
    x = x + instr(s, "from buusers where lcase(username) =")
    if x > 0 then
      f = getparam("SqlLogPath") & "\sqllog-" & year(date) & "-" & month(date) & "-" & day(date) & ".csv"
      a = ""
      a = a & tocsv(now) & ","
      a = a & tocsv(request.servervariables("remote_addr")) & ","
      a = a & tocsv(session("username")) & ","
      a = a & tocsv(scriptname) & ","
      a = a & tocsv(querystring) & ","
      a = a & """" & replace(s, """", """""") & ""","
      du = addlog(f, a)
      if getparam("SQLLogToDB") = "y" then
        's = "create table sqllog (id autoincrement, date1 datetime, ip text(50), username text(50),scriptname text(100),querystring memo,sqlstring memo)"
        f = "" : v = ""
        f = f & "date1," : v = v & sqldate(now) & ","
        f = f & "ip," : v = v & "'" & replace(request.servervariables("remote_addr"), "'", "''") & "',"
        f = f & "username," : v = v & "'" & replace(session("username"), "'", "''") & "',"
        f = f & "scriptname," : v = v & "'" & replace(scriptname, "'", "''") & "',"
        f = f & "querystring," : v = v & "'" & replace(querystring, "'", "''") & "',"
        f = f & "sqlstring," : v = v & "'" & replace(s, "'", "''") & "',"
        f = left(f, len(f) - 1): v = left(v, len(v) - 1)
        sq = "insert into sqllog(" & f & ") values(" & v & ")"
        closers(rs) : set rs = conn.execute(sq)
      end if
    end if
  end if


  '----------order history recorder---------------------------------
  s = lcase(s)
  if session("showorderhistory") = "y" and left(s,14) = "update orders " and instr(s, " where ") > 0 then
    dim date1,status,pids,direction,courseid,from1,to1,course,userid,price
    ii = getidlist("select id from orders " & mid(s, instr(s, " where ")))
    s = replace(s, "update orders set ","update orders set,")
    s = replace(s, "where id",",where id")

    date1     = getfromto(s, "", "date1=",      ",", false, false)
    if instr(date1, "convert") > 0 then date1 = getfromto(s, "", "date1=",      ")", false, true)
    if date1 = "" then date1 = sqldate("1/1/1900")

    s = replace(s, " ", "")
    status    = getfromto(s, "", "status='",    "'", false, false) : status = left(status,20) : status = replace(status, "'", "`")
    pids      = getfromto(s, "", "pids='",      "'", false, false) : pids = formatidlist(pids)
    direction = getfromto(s, "", "direction='", "'", false, false)
    courseid  = getfromto(s, "", "courseid=",   ",", false, false) : courseid = trim(getcode("courses", courseid) & " " & gettitle("courses", courseid))
    from1     = getfromto(s, "", "from1=",      ",", false, false) : from1 = gettitle("stations", from1)
    to1       = getfromto(s, "", "to1=",        ",", false, false) : to1 = gettitle("stations", to1)
    price     = getfromto(s, "", "price=",      ",", false, false) : price = strfilter(price, "0123456789.") : if price = "" then price = "0"

    course = "" : if courseid <> "" and courseid <> "0" then course = courseid & " - " & direction else course = from1 & " - " & to1
    userid = session("userid") : if not isnumeric(userid) then userid = "0"

    if status & pids & replace(course, " - ", "") <> "" then
      do until ii = ""
        i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
        f = "" : v = ""
        f = f & "date0," : v = v & sqldate(now) & ","
        f = f & "date1," : v = v & date1 & ","
        f = f & "userid," : v = v & userid & ","
        f = f & "status," : v = v & "'" & status & "',"
        f = f & "pids," : v = v & "'" & pids & "',"
        f = f & "course," : v = v & "'" & course & "',"
        f = f & "price," : v = v & price & ","
        f = f & "orderid," : v = v & i & ","
        f = left(f, len(f) - 1): v = left(v, len(v) - 1)
        s = "insert into orderhistory(" & f & ") values(" & v & ")"
        closers(rs) : set rs = conn.execute(sqlfilter(s))
      loop
    end if
  end if

  '----------worker history recorder---------------------------------
  s = lcase(s)
  if session("showworkerhistory") = "y" and left(s,15) = "update workers " and instr(s, " where ") > 0 then
    ii = getidlist("select id from workers " & mid(s, instr(s, " where ")))
    s = replace(s, "update workers set ","update workers set,")
    s = replace(s, "where id",",where id")
    s = replace(s, " ", "")
    a = ""
    t = getfromto(s, "", "courseid1=", ",", false, false) : t = trim(getcode("courses", t) & " " & gettitle("courses", t)) : a = a & "|courseid1=" & t
    t = getfromto(s, "", "from1=", ",", false, false) : t = gettitle("stations", t) : a = a & "|from1=" & t
    t = getfromto(s, "", "citystationid=", ",", false, false) : t = gettitle("stations", t) : a = a & "|citystationid=" & t
    t = getfromto(s, "", "eligible='", "'", false, false) : a = a & "|eligible=" & t
    t = getfromto(s, "", "specialrides='", "'", false, false) : a = a & "|specialrides=" & t
    t = getfromto(s, "", "workertypeid=", ",",  false, false) : t = gettitle("workertypes", t) : a = a & "|workertypeid=" & t
    userid = session("userid") : if not isnumeric(userid) then userid = "0"
    do until ii = ""
      i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
      f = "" : v = ""
      f = f & "date0," : v = v & sqldate(now) & ","
      f = f & "data1," : v = v & "'" & a & "',"
      f = f & "userid," : v = v & userid & ","
      f = f & "workerid," : v = v & i & ","
      f = left(f, len(f) - 1): v = left(v, len(v) - 1)
      s = "insert into workerhistory(" & f & ") values(" & v & ")"
      closers(rs) : set rs = conn.execute(sqlfilter(s))
    loop
  end if
']


    'thepath = getparam("SQLLogPath") & "\sqllog-" & year(date) & "-" & month(date) & "-" & day(date) & ".csv"
    'thepath = "D:\HostingSpaces\axisg\axisis.net\wwwroot\paz\mobile\uploads\sqllog-" & year(date) & "-" & month(date) & "-" & day(date) & ".csv"
    thepath = "D:\webAxis\trans\mobile\uploads\sqllog-" & year(date) & "-" & month(date) & "-" & day(date) & ".csv"
     v = ""
     v = v & session("userid") & ","
     v = v & session("username") & ","
     v = v & session("usersiteid") & ","
     'x = addlog(thepath,v)
