<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Media Query Demos</title>
<style type="text/css">
.wrapper {
	border: solid 1px #666;	
	padding: 5px 10px;
	margin: 40px;
}

.viewing-area span {
	color: #666;
	display: none;
}

/* max-width */
@media all (max-width:900px)
 {
	.one {
		background: #F9C;
	}
	span.lt600 {
		display: inline-block;
	}
}

/* min-width */
@media all (min-width:910px) {
	.two {
		background: #F90;
	}
	span.gt900 {
		display: inline-block;
	}
}

/* min-width & max-width 
@media screen and (min-width: 900px)
/* max device width 
@media screen and (max-device-width: 640px) {
	*/
	
</style>
</head>

<body>
	<div class="wrapper one">1</div>
	<div class="wrapper two">2</div>
	<p class="viewing-area"><strong>Your current viewing area is:</strong> <span class="lt600">less than 600px</span> <span class="bt600-900">between 600 - 900px</span> <span class="gt900">greater than 900px</span></p>
	<p>Tutorial by <a href="http://www.webdesignerwall.com">Web Designer Wall</a></p>
<hr>
<p>Click the button to display the total width of your screen, in pixels.</p>

<button onclick="myFunction()">Try it</button>

<p id="demo"></p>

<script>
function myFunction() {
    var x = "Total Width: " + screen.width + "px";
    document.getElementById("demo").innerHTML = x;
}
</script>
  
</body>
</html>
