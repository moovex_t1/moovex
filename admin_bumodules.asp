<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 21/09/2010 15:08:09

function bumodulesdelitem(i)
  bumodulesdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
'[  s = "delete * from " & tablename & " where id=" & i
'[  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select * from bumodules where id=" & i & " and created = 'on'"
  closers(rs) : set rs = conn.execute(SQLfilter(s))
  If Not rs.EOF Then
    errorend("_back~Unable to delete table - must remove from system first~")
    delmodule = -1: Exit Function
  End If
  s = "delete * from bumodules where id=" & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

']
  s = "delete * from bumodulefields where tid=" & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "delete * from bumodulecoderep where tid=" & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function bumodulesinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "" : if getfield("display") <> "" then t = getfield("display")
  f = f & "display," : v = v & "'" & t & "',"

  t = "500"
'[  sq = "select max(priority) as t from " & tablename
  sq = "select max(priority) as t from " & tablename & " where priority > 0"
']
  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  if not rs1.eof then t = rs1("t") : if isnull(t) then t = "500"
  t = cdbl(t) + 10
  if getfield("priority") <> "" then t = getfield("priority")
  f = f & "priority," : v = v & t & ","

  t = "i_cube.gif" : if getfield("icon") <> "" then t = getfield("icon")
  f = f & "icon," : v = v & "'" & t & "',"

  t = "on" : if getfield("createtable") <> "" then t = getfield("createtable")
  f = f & "createtable," : v = v & "'" & t & "',"

  t = "on" : if getfield("addtrans") <> "" then t = getfield("addtrans")
  f = f & "addtrans," : v = v & "'" & t & "',"

  t = "" : if getfield("astree") <> "" then t = getfield("astree")
  f = f & "astree," : v = v & "'" & t & "',"

  t = "on" : if getfield("multilang") <> "" then t = getfield("multilang")
  f = f & "multilang," : v = v & "'" & t & "',"

  t = "i" : if getfield("addnav") <> "" then t = getfield("addnav")
  f = f & "addnav," : v = v & "'" & t & "',"

  t = "" : if getfield("showid") <> "" then t = getfield("showid")
  f = f & "showid," : v = v & "'" & t & "',"

  t = "on" : if getfield("addsearch") <> "" then t = getfield("addsearch")
  f = f & "addsearch," : v = v & "'" & t & "',"

  t = "on" : if getfield("addsearchbar") <> "" then t = getfield("addsearchbar")
  f = f & "addsearchbar," : v = v & "'" & t & "',"

  t = "" : if getfield("addcsv") <> "" then t = getfield("addcsv")
  f = f & "addcsv," : v = v & "'" & t & "',"

  t = "" : if getfield("addloadcsv") <> "" then t = getfield("addloadcsv")
  f = f & "addloadcsv," : v = v & "'" & t & "',"

  t = "" : if getfield("addprint") <> "" then t = getfield("addprint")
  f = f & "addprint," : v = v & "'" & t & "',"

  t = "on" : if getfield("addmultiactions") <> "" then t = getfield("addmultiactions")
  f = f & "addmultiactions," : v = v & "'" & t & "',"

  t = "" : if getfield("addduplicatebutton") <> "" then t = getfield("addduplicatebutton")
  f = f & "addduplicatebutton," : v = v & "'" & t & "',"

  t = "on" : if getfield("addpages") <> "" then t = getfield("addpages")
  f = f & "addpages," : v = v & "'" & t & "',"

  t = "" : if getfield("monthviewfield") <> "" then t = getfield("monthviewfield")
  f = f & "monthviewfield," : v = v & "'" & t & "',"

  t = "" : if getfield("created") <> "" then t = getfield("created")
  f = f & "created," : v = v & "'" & t & "',"

  t = "" : if getfield("defaultorder") <> "" then t = getfield("defaultorder")
  f = f & "defaultorder," : v = v & "'" & t & "',"

  f = f & "updateduserid," : v = v & session("userid") & ","

  f = f & "updateddate," : v = v & sqldate(now) & ","

'[  t = "" : if getfield("permissions") <> "" then t = getfield("permissions")
  t = "|Admin&#61;4|" : if getfield("permissions") <> "" then t = getfield("permissions")
']
  f = f & "permissions," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into bumodules(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from bumodules"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
'[  bumodulesinsertrecord = rs("id")
  bumodulesinsertrecord = rs("id")

  if getfield("action2") = "createaslookup" then
    response.clear
    Session("bumodulefieldscontainerid") = bumodulesinsertrecord
    u = "admin_bumodulefields.asp?action=insert"
    u = u & "&tid=" & bumodulesinsertrecord
    u = u & "&tname=title"
    u = u & "&tdisplay=Title"
    u = u & "&tcontrol=textline"
    u = u & "&ttype=text"
    u = u & "&tlength=50"
    u = u & "&tdata="
    u = u & "&tdefault="
    u = u & "&tmust=on"
    u = u & "&tdir=d"
    u = u & "&tfilter="
    w = 0 : if session("debug") = "on" then w = 300
    rr "<iframe width=" & w & " height=" & w & " src=" & u & "></iframe>"

    if getfield("action3") = "createaschildpage" then
      u = "admin_bumodulefields.asp?action=insert"
      u = u & "&tid=" & bumodulesinsertrecord
      u = u & "&tname=" & getfield("fathertitle") & "id"
      u = u & "&tdisplay=" & getfield("fathertitle") & "id"
      u = u & "&tcontrol=selectlookup"
      u = u & "&ttype=number"
      u = u & "&tdata=" & getfield("fathertitle") & ",id,"
      u = u & "&tdefault=0"
      w = 0 : if session("debug") = "on" then w = 300
      rr "<iframe width=" & w & " height=" & w & " src=" & u & "></iframe>"
    end if
    rr("<iframe width=" & w & " height=" & w & " scrolling=no frameborder=0 src=?action=build&redir=no&id=" & bumodulesinsertrecord & "></iframe>")
    rend
  else
    session("wizardid") = rs("id")
  end if
']
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table bumodules (id autoincrement primary key,title text(100),display text(100),priority number,icon text(30),createtable text(2),addtrans text(2),astree text(2),multilang text(2),addnav text(1),showid text(2),addsearch text(2),addsearchbar text(2),addcsv text(2),addloadcsv text(2),addprint text(2),addmultiactions text(2),addduplicatebutton text(2),addpages text(2),monthviewfield text(100),created text(2),defaultorder text(50),updateduserid number,updateddate datetime,permissions memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "bumodules" : session("tablename") = "bumodules"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
'[admin_top = true : admin_login = true
  admin_top = true : admin_login = true
  if getfield("action") = "editfile" or instr(getfield("action"),"editpermissions") > 0 or getfield("action") = "build" then emptytop = "on"
']
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "simple" then ulev = 0
if session("usergroup") = "admin" then ulev = 0
if session("userlevel") = "5" then ulev = 5

'['-------------start--------------------------------------
locked = "," & getidlist("select * from bumodules where priority < 0")

if getfield("action") = "build" then
  x = build(request_id)
end if
']
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = bumodulesinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  msg = ""
  thetitle = lcase(trim(getfield("title")))
  If Len(thetitle) < 2 Then
    msg = "~Invalid Table Name~"
  ElseIf IsNumeric(Left(thetitle, 1)) Then
    msg = "~Invalid Table Name~"
  ElseIf InStr(LCase(SqlReservedKeywords), "," & thetitle & ",") > 0 Then
    msg = "~Invalid Table Name~"
  ElseIf Len(GetField("display")) < 1 Then
    msg = "~Invalid display name~"
  Else
    For i = 1 To Len(thetitle)
      If InStr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_", Mid(thetitle, i, 1)) = 0 Then msg = "~Invalid Table Name~": Exit For
    Next
    closers(rs) : set rs = conn.execute(SQLfilter("select * from bumodules where title = '" & thetitle & "' and id <> " & request_id))
    If Not rs.EOF Then msg = "~Table name in use~"
  End If
  if msg <> "" Then errorend("_back" & msg)
  msg = "~Item Updated~"

  If GetField("createparentid") = "on" Then
    Add = vbCrLf & "'" & Now & vbCrLf

    On Error Resume Next
    s = "alter table " & thetitle & " drop column parentid"
    closers(rs1) : set rs1 = conn.execute(SQLfilter(s))
    On Error GoTo 0
    Add = Add & "'on error resume next : set rs1 = conn.exe" & "cute(sqlfilter(""" & s & """)) : on error goto 0" & vbCrLf

    s = "alter table " & thetitle & " add column parentid number"
    closers(rs1) : set rs1 = conn.execute(SQLfilter(s))
    Add = Add & "closers(rs1) : set rs1 = conn.execute(sqlfilter(""" & s & """))" & vbCrLf
    
    s = "update " & thetitle & " set parentid = 0"
    closers(rs1) : set rs1 = conn.execute(SQLfilter(s))
    Add = Add & "closers(rs1) : set rs1 = conn.execute(sqlfilter(""" & s & """))" & vbCrLf

    x = addlog(Server.mappath("_upgrade.asp"), Add)

  End If
  

']
  s = "update " & tablename & " set "
'[  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("title") then s = s & "title='" & thetitle & "',"
']
  if okedit("display") then s = s & "display='" & getfield("display") & "',"
  if okedit("priority") then s = s & "priority=" & getfield("priority") & ","
  if okedit("icon") then s = s & "icon='" & getfield("icon") & "',"
  if okedit("createtable") then s = s & "createtable='" & getfield("createtable") & "',"
  if okedit("addtrans") then s = s & "addtrans='" & getfield("addtrans") & "',"
  if okedit("astree") then s = s & "astree='" & getfield("astree") & "',"
  if okedit("multilang") then s = s & "multilang='" & getfield("multilang") & "',"
  if okedit("addnav") then s = s & "addnav='" & getfield("addnav") & "',"
  if okedit("showid") then s = s & "showid='" & getfield("showid") & "',"
  if okedit("addsearch") then s = s & "addsearch='" & getfield("addsearch") & "',"
  if okedit("addsearchbar") then s = s & "addsearchbar='" & getfield("addsearchbar") & "',"
  if okedit("addcsv") then s = s & "addcsv='" & getfield("addcsv") & "',"
  if okedit("addloadcsv") then s = s & "addloadcsv='" & getfield("addloadcsv") & "',"
  if okedit("addprint") then s = s & "addprint='" & getfield("addprint") & "',"
  if okedit("addmultiactions") then s = s & "addmultiactions='" & getfield("addmultiactions") & "',"
  if okedit("addduplicatebutton") then s = s & "addduplicatebutton='" & getfield("addduplicatebutton") & "',"
  if okedit("addpages") then s = s & "addpages='" & getfield("addpages") & "',"
  if okedit("monthviewfield") then s = s & "monthviewfield='" & getfield("monthviewfield") & "',"
  if okedit("created") then s = s & "created='" & getfield("created") & "',"
  if okedit("defaultorder") then s = s & "defaultorder='" & getfield("defaultorder") & "',"
  t = session("userid") : if ulev > 4 then t = getfield("updateduserid")
  if okedit("updateduserid") then s = s & "updateduserid=" & t & ","
  t = now : if ulev > 4 then t = replace(getfield("updateddate"),".","/")
  if okedit("updateddate") then s = s & "updateddate=" & sqldate(t) & ","
'[  if okedit("permissions") then s = s & "permissions='" & getfield("permissions") & "',"
']
  s = left(s, len(s) - 1) & " "
  s = s & "where id=" & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'['-------------afterupdate-------------
If request_action = "update" and request_action2 = "build" Then
  x = build(request_id)
ElseIf request_action = "update" and request_action2 = "clean" Then
  If clng("0" & getfield("priority")) < 0 Then errorend("_back~Can't remove system table~")
  request_action2 = "remove"
  x = build(request_id)
end if

']
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(url)

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(url)

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(title,display,priority,icon,createtable,addtrans,astree,multilang,addnav,showid,addsearch,addsearchbar,addcsv,addloadcsv,addprint,addmultiactions,addduplicatebutton,addpages,monthviewfield,created,defaultorder,updateduserid,updateddate,permissions)"
  s = s & " SELECT title,display,priority,icon,createtable,addtrans,astree,multilang,addnav,showid,addsearch,addsearchbar,addcsv,addloadcsv,addprint,addmultiactions,addduplicatebutton,addpages,monthviewfield,created,defaultorder," & session("userid") & "," & sqldate(now) & ",permissions"
  s = s & " FROM " & tablename & " where id=" & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  s = "insert into bumodulefields(tname,tdisplay,tcontrol,tlist,tpriority,tnotr,ttype,tlength,tdir,tdefault,tdata,thelp,tmultiupdate,tlistupdate,tfilter,tmust,tmin,tmax,tid)"
  s = s & " SELECT tname,tdisplay,tcontrol,tlist,tpriority,tnotr,ttype,tlength,tdir,tdefault,tdata,thelp,tmultiupdate,tlistupdate,tfilter,tmust,tmin,tmax," & theid & ""
  s = s & " FROM bumodulefields where tid=" & request_id
  closers(rs1) : set rs1 = conn.execute(sqlfilter(s))

  s = "insert into bumodulecoderep(title,times,status,applytoall,text1,text2,tid)"
  s = s & " SELECT title,times,status,applytoall,text1,text2," & theid & ""
  s = s & " FROM bumodulecoderep where tid=" & request_id
  closers(rs1) : set rs1 = conn.execute(sqlfilter(s))

  request_action = "form"
  request_id = theid
'[  msg = msg & " ~Record duplicated~"
  t = getfield("title") : x = 0
  do
    x = x + 1
    s = "select * from bumodules where title = '" & t & x & "'"
    set rs1 = conn.execute(sqlfilter(s))
    if rs1.eof then exit do
  loop
  s = "update bumodules set title = '" & t & x & "', created = '', priority = priority + 1, createtable = 'on', addtrans = 'on', updateddate = " & sqldate(now) & " where id = " & theid
  set rs1 = conn.execute(sqlfilter(s))
  msg = msg & " ~Record duplicated~"
']
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = bumodulesdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = bumodulesdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      t = getfield("listupdate_priority_" & b) : if t = "" then t = getfield("multiupdate_priority")
      if okedit("priority") then s = s & ",priority = " & t
      if okedit("createtable") then s = s & ",createtable = '" & getfield("listupdate_createtable_" & b) & "'"
      if okedit("addtrans") then s = s & ",addtrans = '" & getfield("listupdate_addtrans_" & b) & "'"
      if okedit("astree") then s = s & ",astree = '" & getfield("listupdate_astree_" & b) & "'"
      if okedit("multilang") then s = s & ",multilang = '" & getfield("listupdate_multilang_" & b) & "'"
      t = getfield("listupdate_addnav_" & b) : if t = "" then t = getfield("multiupdate_addnav")
      if okedit("addnav") then s = s & ",addnav = '" & t & "'"
      if okedit("showid") then s = s & ",showid = '" & getfield("listupdate_showid_" & b) & "'"
      if okedit("addsearch") then s = s & ",addsearch = '" & getfield("listupdate_addsearch_" & b) & "'"
      if okedit("addsearchbar") then s = s & ",addsearchbar = '" & getfield("listupdate_addsearchbar_" & b) & "'"
      if okedit("addcsv") then s = s & ",addcsv = '" & getfield("listupdate_addcsv_" & b) & "'"
      if okedit("addloadcsv") then s = s & ",addloadcsv = '" & getfield("listupdate_addloadcsv_" & b) & "'"
      if okedit("addprint") then s = s & ",addprint = '" & getfield("listupdate_addprint_" & b) & "'"
      if okedit("addmultiactions") then s = s & ",addmultiactions = '" & getfield("listupdate_addmultiactions_" & b) & "'"
      if okedit("addduplicatebutton") then s = s & ",addduplicatebutton = '" & getfield("listupdate_addduplicatebutton_" & b) & "'"
      if okedit("addpages") then s = s & ",addpages = '" & getfield("listupdate_addpages_" & b) & "'"
      t = getfield("listupdate_defaultorder_" & b) : if t = "" then t = getfield("multiupdate_defaultorder")
      if okedit("defaultorder") then s = s & ",defaultorder = '" & t & "'"
'[      if s <> "" then
'[        s = "update " & tablename & " set " & mid(s,2) & " where id=" & b
'[        closers(rs) : set rs = conn.execute(sqlfilter(s))
'[        c = c + 1
'[      end if
      s = s & ",updateddate = " & sqldate(now)
      if s <> "" then
        s = "update bumodules set " & mid(s,2) & " where id=" & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if

      if request_action2 = "build" then
        rr("<iframe width=800 height=22 scrolling=no frameborder=0 src=?action=build&redir=no&id=" & b & "></iframe><br>")
      elseif request_action2 = "clean" then
        closers(rs) : set rs = conn.execute(SQLfilter("select * from bumodules where id=" & b))
        If clng("0" & rs("priority")) < 0 Then errorend("_back~Can't remove system table~")
        rr("<iframe width=800 height=22 scrolling=no frameborder=0 src=?action=build&action2=remove&redir=no&id=" & b & "></iframe><br>")
      end if
']
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if

'['-------------more actions-------------------------------------
if request_action = "upgradeallpermissions" then
  set rs = conn.execute("select * from bumodules")
  do until rs.eof
    s = "update bumodules set permissions = '" & rs("permissions") & "' where title = '" & rs("title") & "'"
    Add = "closers(rs1) : set rs1 = conn.execute(sqlfilter(""" & s & """))"
    x = addlog(Server.mappath("_upgrade.asp"), Add)
    rs.movenext
  loop
  rr("Upgrade sql has been written to _upgrade.asp")
  rend
end if

if request_action = "editallpermissions" then
  s = "select id from bumodules where instr(title,'bumodule') <> 1 order by priority"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    rr("<iframe id=freditpermissions" & rs("id") & " width=100% height=300 src=?action=editpermissions&id=" & rs("id") & "&action2=editallpermissions frameborder=0></iframe>")
    rs.movenext
  loop
  rend
end if
if request_action = "editpermissionssubmit" then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  s = "update bumodules set permissions = '" & getfield("permissions") & "', updateddate = " & sqldate(now) & " where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "update bumodules set permissions = '" & getfield("permissions") & "' where title = '" & gettitle("bumodules", request_id) & "'"
  Add = "'" & now & vbcrlf & "closers(rs1) : set rs1 = conn.execute(sqlfilter(""" & s & """))" & vbCrLf
  x = addlog(Server.mappath("_upgrade.asp"), Add)
  request_action = "editpermissions"
  msg = msg & " ~Item Updated~"
end if

if request_action = "editpermissions" then

  s = "select title,permissions from bumodules where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  thetitle = rs("title") : if isnull(thetitle) then thetitle = ""
  thepermissions = rs("permissions") : if isnull(thepermissions) then thepermissions = ""
  thepermissions = ctxt(thepermissions)

  rr("<center><table class=form_table cellspacing=5>" & formtopbar)

  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=editpermissionssubmit>")
  rr("<input type=hidden name=id value=" & request_id & ">")

  'rr("<textarea name=permissions style='width:700; height:100;'>" & thepermissions & "</textarea>")
  rr("<input type=hidden name=permissions value=""" & thepermissions & """>")

  rr("<tr><td class=form_name colspan=99><img src='icons/lock.png' ~iconsize~> ~Permissions~ - " & "~" & thetitle & "_" & thetitle & "~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><script language=vbscript> a = """" : x = setTimeout(""msgdiv.innerhtml = a"",3000,""vbscript"") </script>")

  a = getval(thepermissions,"additionalelements")
  a = "," & a : if right(a,1) = "," then a = left(a,len(a)-1)
  a = replace(a, ",", ",ADDITIONAL: ") & ","
  a = mid(a,2)

  elements = screenelements & a
  s = "select tname from bumodulefields where tid = " & request_id & " order by tpriority"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    elements = elements & "FIELD: " & rs("tname") & ","
    rs.movenext
  loop

  rr("<tr><td class=list_title><b>~User Group~</b><td class=list_title><b>User Level</b>")
  a = elements
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    if instr(b,"FIELD: ") > 0 then
      b = mid(b,8)
      rr("<td class=list_title2>" & b)
    elseif instr(b,"ADDITIONAL: ") > 0 then
      b = mid(b,13)
      rr("<td class=list_title2>" & b)
    elseif b <> "" then
      rr("<td class=list_title2>~screen_elements_" & b & "~")
    end if
  loop

  gg = "EVERYBODY," & getparam("usergroups")
  c = 0
  do until gg = ""
    g = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
    rr("<tr><td class=form_item1>" & g)

    rr("<td class=form_item2 style='width:100%;'>")
    if c = 0 then
      rr("<img src=blank.gif width=1 height=20>")
    else
      a = "0,1,2,3,4,5,"
      rr("<select name=" & g & " dir=~langdir~")
      rr(" onchange='vbscript: f1.permissions.value = setval(f1.permissions.value, """ & g & """, me.value)'")
      rr(">")
      do until a = ""
        b = left(a,instr(a,",")-1)
        a = mid(a,instr(a,",")+1)
        rr("<option value=""" & b & """")
        if b = getval(thepermissions,g) then rr(" selected")
        rr(">" & b & " " & getuserlevel(b))
      loop
      rr("</select>")
    end if

    a = elements
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)

      if left(b,7) = "FIELD: " then
        b = mid(b,8)
      elseif left(b,12) = "ADDITIONAL: " then
        b = mid(b,13)
      else
      end if

      rr("<td class=form_item2>")

      f = g & "," & b
      v = getval(thepermissions,f) : if v = "" then v = "S"

      color_s = "aaffaa"
      color_d = "aaaaaa"
      color_h = "ffaaaa"
      if v = "S" then t = color_s
      if v = "D" then t = color_d
      if v = "H" then t = color_h
      rr("<font id=togglecontrol" & c & " style='border:1px solid #808080; padding:1; background:" & t & "; color:000000; cursor:hand;' onclick='vbscript: togglecontrol" & c & "'>" & v & "</font>")

      rr("<" & "script language=vbscript>" & vbcrlf)
      rr("sub togglecontrol" & c & vbcrlf)
      rr("  doneit = false" & vbcrlf)
      rr("  x = document.getelementbyid(""togglecontrol" & c & """).innertext" & vbcrlf)
      rr("  if not doneit and x = ""S"" then doneit = true : x = ""D"" : document.getelementbyid(""togglecontrol" & c & """).style.background = ""#" & color_d & """" & vbcrlf)
      rr("  if not doneit and x = ""D"" then doneit = true : x = ""H"" : document.getelementbyid(""togglecontrol" & c & """).style.background = ""#" & color_h & """" & vbcrlf)
      rr("  if not doneit and x = ""H"" then doneit = true : x = ""S"" : document.getelementbyid(""togglecontrol" & c & """).style.background = ""#" & color_s & """" & vbcrlf)
      rr("  f1.permissions.value = setval(f1.permissions.value, """ & f & """, x)" & vbcrlf)
      rr("  document.getelementbyid(""togglecontrol" & c & """).innertext = x" & vbcrlf)
      rr("end sub" & vbcrlf)
      rr("<" & "/script>" & vbcrlf)
    loop
  loop

  rr("<tr><td colspan=99 class=update_bar>")

  rr("~bumodules_additionalelements~ <input type=text name=additionalelements dir=ltr value=""" & getval(thepermissions,"additionalelements") & """")
  rr(" onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0'")
  rr(" onchange='vbscript: f1.permissions.value = setval(f1.permissions.value, ""additionalelements"", me.value)'>")

  rr(" <input type=button style='width:80;' value=""~Save~"" onclick='vbscript: f1.submit'>")
  rr(" <input type=button style='width:80;' value=""~Clear~"" onclick='vbscript: if msgbox(""~Clear~ ?"",vbyesno) = 6 then f1.permissions.value = """" : f1.submit'>")
  rr(" <input type=button style='width:80;' value=""~Cancel~"" onclick='vbscript: document.location = ""?action=editpermissions&id=" & request_id & """'>")
  rr(" * Show / Disable / Hide")
  rr("</tr></form>" & bottombar & "</table>")

  t = "" : if request_action2 = "editallpermissions" then t = request_id
  rr("<script language=vbscript>parent.document.getelementbyid(""freditpermissions" & t & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""freditpermissions" & t & """).style.width = document.body.scrollwidth + 20 </script>")

  rend

end if



if request_action = "editfile" and okwrite(tablename,"0" & request_id) then
  if getfield("filename") <> "" then
    filename = getfield("filename")
  else
    s = "select * from bumodules where id = " & request_id
    closers(rs) : set rs = conn.execute(sqlfilter(s))
    if rs.eof then rend
    filename = rs("title")
    filename = "admin_" & filename & ".asp"
  end if

  If request_action2 = "savefile" Then
    Set thefile = fs.createTextFile(Server.mappath(filename), True)
    thefile.write (Request("text1"))
    thefile.Close
    msg = "~Record was saved~"
  End If

  If Not fs.fileexists(Server.mappath(filename)) Then rend
  Set thefile = fs.opentextfile(Server.mappath(filename))
  thetext1 = thefile.readall
  thefile.Close
  
  rr("<center><table class=form_table cellspacing=5>" & formtopbar)

  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=editfile>")
  rr("<input type=hidden name=action2 value=savefile>")
  rr("<input type=hidden name=filename value='" & filename & "'>")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name colspan=9><img src='icons/invoice.png' ~iconsize~> ~Source Code~ <font color=808080> " & filename & "</font>")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><script language=vbscript> a = """" : x = setTimeout(""msgdiv.innerhtml = a"",3000,""vbscript"") </script>")

  rr("<tr valign=top><td class=form_item2 colspan=9>")
  rr("<textarea id=qtextarea1 name=text1 dir=ltr wrap=off style='width:100%; height:500; font-family:courier new;'>")
  Response.write (thetext1)
  rr("</textarea>")
  
  rr("<tr><td colspan=9 class=update_bar>")
  rr(" <input type=button style='width:80;' value=""~Save~"" onclick='vbscript: f1.submit'>")
  rr(" <input type=button style='width:80;' value=""~Cancel~"" onclick='vbscript: document.location = ""?action=editfile&id=" & request_id & """'>")
  rr("</tr></form>" & bottombar & "</table>")
  
  rr("<script language=vbscript>parent.document.getelementbyid(""freditfile"").height = document.body.scrollheight + 20</script>")
  rend
end if
']
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = getfield("backto")
  s = "select * from " & tablename & " where id=" & request_id
  if session("usingsql") = "1" then s = replace(s,"*","bumodules.id,bumodules.title,bumodules.display,bumodules.priority,bumodules.icon,bumodules.createtable,bumodules.addtrans,bumodules.astree,bumodules.multilang,bumodules.addnav,bumodules.showid,bumodules.addsearch,bumodules.addsearchbar,bumodules.addcsv,bumodules.addloadcsv,bumodules.addprint,bumodules.addmultiactions,bumodules.addduplicatebutton,bumodules.addpages,bumodules.monthviewfield,bumodules.created,bumodules.defaultorder,bumodules.updateduserid,bumodules.updateddate,bumodules.permissions",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thedisplay = rs("display") : if isnull(Thedisplay) then Thedisplay = ""
  Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
  Thepriority = replace(thepriority,",",".")
  Theicon = rs("icon") : if isnull(Theicon) then Theicon = ""
  Thecreatetable = rs("createtable") : if isnull(Thecreatetable) then Thecreatetable = ""
  Theaddtrans = rs("addtrans") : if isnull(Theaddtrans) then Theaddtrans = ""
  Theastree = rs("astree") : if isnull(Theastree) then Theastree = ""
  Themultilang = rs("multilang") : if isnull(Themultilang) then Themultilang = ""
  Theaddnav = rs("addnav") : if isnull(Theaddnav) then Theaddnav = ""
  Theshowid = rs("showid") : if isnull(Theshowid) then Theshowid = ""
  Theaddsearch = rs("addsearch") : if isnull(Theaddsearch) then Theaddsearch = ""
  Theaddsearchbar = rs("addsearchbar") : if isnull(Theaddsearchbar) then Theaddsearchbar = ""
  Theaddcsv = rs("addcsv") : if isnull(Theaddcsv) then Theaddcsv = ""
  Theaddloadcsv = rs("addloadcsv") : if isnull(Theaddloadcsv) then Theaddloadcsv = ""
  Theaddprint = rs("addprint") : if isnull(Theaddprint) then Theaddprint = ""
  Theaddmultiactions = rs("addmultiactions") : if isnull(Theaddmultiactions) then Theaddmultiactions = ""
  Theaddduplicatebutton = rs("addduplicatebutton") : if isnull(Theaddduplicatebutton) then Theaddduplicatebutton = ""
  Theaddpages = rs("addpages") : if isnull(Theaddpages) then Theaddpages = ""
  Themonthviewfield = rs("monthviewfield") : if isnull(Themonthviewfield) then Themonthviewfield = ""
  Thecreated = rs("created") : if isnull(Thecreated) then Thecreated = ""
  Thedefaultorder = rs("defaultorder") : if isnull(Thedefaultorder) then Thedefaultorder = ""
  Theupdateduserid = rs("updateduserid") : if isnull(Theupdateduserid) then Theupdateduserid = 0
  Theupdateduserid = replace(theupdateduserid,",",".")
  Theupdateddate = rs("updateddate") : if isnull(Theupdateddate) then Theupdateddate = ""
  Thepermissions = rs("permissions") : if isnull(Thepermissions) then Thepermissions = ""
  formcontrols = "title,display,priority,icon,createtable,addtrans,astree,multilang,addnav,showid,addsearch,addsearchbar,addcsv,addloadcsv,addprint,addmultiactions,addduplicatebutton,addpages,monthviewfield,created,defaultorder,updateduserid,updateddate,updateddate_day,updateddate_month,updateddate_year,updateddate_hour,permissions,"
'[  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,buttoncreate,buttonremove,"
']
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  if not childpage and getfield("printversion") <> "on" then

    rr("<div class=subtablinks>")
    t = "000000" : if request_action2 <> "showchildpage" then t = "67b14e"
'[    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "> <font color=" & t & "> ~" & tablename & "_" & tablename & "~ " & request_id & "</font></a>")
    a = "~" & tablename & "_" & tablename & "~ " & request_id : if thetitle <> "" then a = thetitle
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "> <font color=" & t & "> " & a & "</font></a>")
']
'[    t = "ffffff" : if getfield("childpage") = "bumodulefields" then t = "67b14e"
'[    if getpermission("bumodulefields") >= 1 then rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=bumodulefields&containerfield=tid> <font color=" & t & "> ~bumodulefields_bumodulefields~ </font></a>")
    t = "000000" : if getfield("childpage") = "bumodulefields" then t = "67b14e"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=bumodulefields&containerfield=tid> <font color=" & t & "> ~bumodulefields_bumodulefields~ </font></a>")
    t = "000000" : if getfield("action3") = "editpermissions" then t = "67b14e"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=editpermissions&action3=editpermissions> <font color=" & t & "> ~Permissions~ </font></a>")
    t = "000000" : if getfield("action3") = "editfile" then t = "67b14e"
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=editfile&action3=editfile> <font color=" & t & "> ~Source Code~ </font></a>")
']
    t = "000000" : if getfield("childpage") = "bumodulecoderep" then t = "67b14e"
    if getpermission("bumodulecoderep") >= 1 then rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=bumodulecoderep&containerfield=tid> <font color=" & t & "> ~bumodulecoderep_bumodulecoderep~ </font></a>")
'[    rr("<span id=childchildren></span></div>")
    rr " &nbsp;&nbsp; <input type=button style='border:1px; height:15; color:ff0000;' value='~Create in system~' onclick='vbscript: if msgbox(""~Create ?~"",vbyesno) = 6 then document.location = ""?action=build&id=" & theid & """'>"
    rr " &nbsp;&nbsp; <input type=button style='border:1px; height:15; color:000000;' value='~Back~' onclick='vbscript: document.location = ""?""'>"
    rr("<span id=childchildren></span></div>")
']
    if getfield("action2") = "showchildpage" then
'[      t = "admin_" & getfield("childpage") & ".asp?containerfield=" & getfield("containerfield") & "&containerid=" & request_id & "&action=search&action2=clear"
      t = "admin_" & getfield("childpage") & ".asp?containerfield=" & getfield("containerfield") & "&containerid=" & request_id & "&action=search&action2=clear"
      if getfield("action3") = "editpermissions" then t = "?action=editpermissions&id=" & request_id
      if getfield("action3") = "editfile" then t = "?action=editfile&id=" & request_id
']
      rr("<br><iframe id=fr" & getfield("childpage") & " name=fr" & getfield("childpage") & " style='width:100%; height:1000;' frameborder=0 scrolling=no src=" & t & "></iframe>")
      rrbottom : rend
    end if
  end if
  rr("<center><table class=form_table cellspacing=5>" & formtopbar)

  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/archives.png' ~iconsize~> ~bumodules_bumodules~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~bumodules_title~<img src=must.gif></span><td class=form_item2 id=title_tr2>")
'[    rr("<input type=text maxlength=100 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
    rr("<input type=text maxlength=100 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150;' value=""" & Thetitle & """ dir=~langdir~")
    rr(" onblur='vbscript: if f1.display.value = """" then f1.display.value = ucase(left(me.value,1)) & lcase(mid(me.value,2))'>")
    if thecreated = "on" then disablecontrols = disablecontrols & "title," : rr(" <font style='font-size:10;'>(~bumodules_created~)</font>")
']
  end if'form:title

  if oksee("display") then'form:display
    rr("<tr valign=top id=display_tr0><td class=form_item1 id=display_tr1><span id=display_caption>~bumodules_display~<img src=must.gif></span><td class=form_item2 id=display_tr2>")
    rr("<input type=text maxlength=100 name=display onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedisplay & """ dir=~langdir~>")
  end if'form:display

  if oksee("priority") then'form:priority
    rr("<tr valign=top id=priority_tr0><td class=form_item1 id=priority_tr1><span id=priority_caption>~bumodules_priority~<img src=must.gif></span><td class=form_item2 id=priority_tr2>")
'[    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=priority value=""" & Thepriority & """>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=priority value=""" & Thepriority & """>")
    rr( "<img src=blank.gif width=50 height=1>")
']
  end if'form:priority

  if oksee("icon") then'form:icon
    rr("<tr valign=top id=icon_tr0><td class=form_item1 id=icon_tr1><span id=icon_caption>~bumodules_icon~<img src=must.gif></span><td class=form_item2 id=icon_tr2>")
'[    rr("<input type=text maxlength=30 name=icon onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theicon & """ dir=~langdir~>")
    rr(" &nbsp;<span id=dicon onclick='vbscript:dicons.style.visibility = ""visible""' style='cursor:hand;'><img src=""" & theicon & """></span><input type=hidden name=icon value=""" & theicon & """>")
    rr("<table id=dicons style='position:absolute; visibility:hidden; top:240; left:300; border:2px solid #808080; background:ffffff; width:400; height:100; cursor:hand;'><tr valign=top><td style='padding:5px;' onclick='vbscript: dicons.style.visibility = ""hidden""'>")
    Set fo = fs.getfolder(appfolder & "\icons")
    For Each x In fo.Files
      f = LCase(x.Name)
      If Right(LCase(f), 4) = ".gif" or Right(LCase(f), 4) = ".png" Then
        rr("<a href=#aaa onclick='vbscript:dicon.innerhtml = ""<img src=""""icons/" & f & """"">"" : f1.icon.value=""icons/" & f & """ : dicons.style.visibility = ""hidden""'>")
        rr("<img border=0 src='icons/" & f & "'></a> ")
      End If
    Next
    rr("<center><br><br><a href=#aaa>~Cancel~</a>")
    rr("</table>")
']
  end if'form:icon

  if oksee("createtable") then'form:createtable
    rr("<tr valign=top id=createtable_tr0><td class=form_item1 id=createtable_tr1><span id=createtable_caption>~bumodules_createtable~</span><td class=form_item2 id=createtable_tr2>")
    rr("<input type=checkbox name=createtable")
    if thecreatetable = "on" then rr(" checked")
    rr(">")
  end if'form:createtable

  if oksee("addtrans") then'form:addtrans
    rr("<tr valign=top id=addtrans_tr0><td class=form_item1 id=addtrans_tr1><span id=addtrans_caption>~bumodules_addtrans~</span><td class=form_item2 id=addtrans_tr2>")
    rr("<input type=checkbox name=addtrans")
    if theaddtrans = "on" then rr(" checked")
    rr(">")
  end if'form:addtrans

  if oksee("astree") then'form:astree
    rr("<tr valign=top id=astree_tr0><td class=form_item1 id=astree_tr1><span id=astree_caption>~bumodules_astree~</span><td class=form_item2 id=astree_tr2>")
'[    rr("<input type=checkbox name=astree")
'[    if theastree = "on" then rr(" checked")
'[    rr(">")
    rr("<input type=checkbox name=astree")
    if theastree = "on" then rr(" checked")
    rr(">")
    if thecreated = "on" then rr("<input type=checkbox name=createparentid> ~Create Field~<br>")
']
  end if'form:astree

  if oksee("multilang") then'form:multilang
    rr("<tr valign=top id=multilang_tr0><td class=form_item1 id=multilang_tr1><span id=multilang_caption>~bumodules_multilang~</span><td class=form_item2 id=multilang_tr2>")
    rr("<input type=checkbox name=multilang")
    if themultilang = "on" then rr(" checked")
    rr(">")
  end if'form:multilang

  if oksee("addnav") then'form:addnav
    rr("<tr valign=top id=addnav_tr0><td class=form_item1 id=addnav_tr1><span id=addnav_caption>~bumodules_addnav~</span><td class=form_item2 id=addnav_tr2>")
    a = "y,n,s,i,"
    rr("<select name=addnav dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
'[      if Theaddnav = b then rr(" selected")
'[      rr(">" & b)
      if Theaddnav = b then rr(" selected")
      rr(">")
      if b = "y" then rr("Yes")
      if b = "n" then rr("No")
      if b = "s" then rr("Under Settings")
      if b = "i" then rr("Ignore")
']
    loop
    rr("</select>")'form:addnav
  end if'form:addnav

  if oksee("showid") then'form:showid
    rr("<tr valign=top id=showid_tr0><td class=form_item1 id=showid_tr1><span id=showid_caption>~bumodules_showid~</span><td class=form_item2 id=showid_tr2>")
    rr("<input type=checkbox name=showid")
    if theshowid = "on" then rr(" checked")
    rr(">")
  end if'form:showid

  if oksee("addsearch") then'form:addsearch
    rr("<tr valign=top id=addsearch_tr0><td class=form_item1 id=addsearch_tr1><span id=addsearch_caption>~bumodules_addsearch~</span><td class=form_item2 id=addsearch_tr2>")
    rr("<input type=checkbox name=addsearch")
    if theaddsearch = "on" then rr(" checked")
    rr(">")
  end if'form:addsearch

  if oksee("addsearchbar") then'form:addsearchbar
    rr("<tr valign=top id=addsearchbar_tr0><td class=form_item1 id=addsearchbar_tr1><span id=addsearchbar_caption>~bumodules_addsearchbar~</span><td class=form_item2 id=addsearchbar_tr2>")
    rr("<input type=checkbox name=addsearchbar")
    if theaddsearchbar = "on" then rr(" checked")
    rr(">")
  end if'form:addsearchbar

  if oksee("addcsv") then'form:addcsv
    rr("<tr valign=top id=addcsv_tr0><td class=form_item1 id=addcsv_tr1><span id=addcsv_caption>~bumodules_addcsv~</span><td class=form_item2 id=addcsv_tr2>")
    rr("<input type=checkbox name=addcsv")
    if theaddcsv = "on" then rr(" checked")
    rr(">")
  end if'form:addcsv

  if oksee("addloadcsv") then'form:addloadcsv
    rr("<tr valign=top id=addloadcsv_tr0><td class=form_item1 id=addloadcsv_tr1><span id=addloadcsv_caption>~bumodules_addloadcsv~</span><td class=form_item2 id=addloadcsv_tr2>")
    rr("<input type=checkbox name=addloadcsv")
    if theaddloadcsv = "on" then rr(" checked")
    rr(">")
  end if'form:addloadcsv

  if oksee("addprint") then'form:addprint
    rr("<tr valign=top id=addprint_tr0><td class=form_item1 id=addprint_tr1><span id=addprint_caption>~bumodules_addprint~</span><td class=form_item2 id=addprint_tr2>")
    rr("<input type=checkbox name=addprint")
    if theaddprint = "on" then rr(" checked")
    rr(">")
  end if'form:addprint

  if oksee("addmultiactions") then'form:addmultiactions
    rr("<tr valign=top id=addmultiactions_tr0><td class=form_item1 id=addmultiactions_tr1><span id=addmultiactions_caption>~bumodules_addmultiactions~</span><td class=form_item2 id=addmultiactions_tr2>")
    rr("<input type=checkbox name=addmultiactions")
    if theaddmultiactions = "on" then rr(" checked")
    rr(">")
  end if'form:addmultiactions

  if oksee("addduplicatebutton") then'form:addduplicatebutton
    rr("<tr valign=top id=addduplicatebutton_tr0><td class=form_item1 id=addduplicatebutton_tr1><span id=addduplicatebutton_caption>~bumodules_addduplicatebutton~</span><td class=form_item2 id=addduplicatebutton_tr2>")
    rr("<input type=checkbox name=addduplicatebutton")
    if theaddduplicatebutton = "on" then rr(" checked")
    rr(">")
  end if'form:addduplicatebutton

  if oksee("addpages") then'form:addpages
    rr("<tr valign=top id=addpages_tr0><td class=form_item1 id=addpages_tr1><span id=addpages_caption>~bumodules_addpages~</span><td class=form_item2 id=addpages_tr2>")
    rr("<input type=checkbox name=addpages")
    if theaddpages = "on" then rr(" checked")
    rr(">")
  end if'form:addpages

  if oksee("monthviewfield") then'form:monthviewfield
    rr("<tr valign=top id=monthviewfield_tr0><td class=form_item1 id=monthviewfield_tr1><span id=monthviewfield_caption>~bumodules_monthviewfield~</span><td class=form_item2 id=monthviewfield_tr2>")
'[    rr("<input type=text maxlength=100 name=monthviewfield onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Themonthviewfield & """ dir=~langdir~>")
    rr("<select name=monthviewfield style='width:100;'>")
    rr("<option value=''>")
    closers(rs1) : set rs1 = conn.execute(SQLfilter("select * from bumodulefields where tid = " & request_id & " and ttype = 'datetime' order by tpriority"))
    Do Until rs1.EOF
      rr("<option value=""" & rs1("tname") & """")
      If rs1("tname") = themonthviewfield Then rr(" selected")
      rr(">" & rs1("tname"))
      rs1.movenext
    Loop
    rr("</select>")
']
  end if'form:monthviewfield

  if oksee("created") then'form:created
'[    rr("<tr valign=top id=created_tr0><td class=form_item1 id=created_tr1><span id=created_caption>~bumodules_created~</span><td class=form_item2 id=created_tr2>")
'[    rr("<input type=checkbox name=created")
'[    if thecreated = "on" then rr(" checked")
'[    rr(">")
    rr("<input type=hidden name=created value='" & thecreated & "'>")
']
  end if'form:created

  if oksee("defaultorder") then'form:defaultorder
    rr("<tr valign=top id=defaultorder_tr0><td class=form_item1 id=defaultorder_tr1><span id=defaultorder_caption>~bumodules_defaultorder~</span><td class=form_item2 id=defaultorder_tr2>")
'[    rr("<input type=text maxlength=50 name=defaultorder onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thedefaultorder & """ dir=ltr>")
    a = thedefaultorder : b = ""
    if instr(a," ") > 0 then b = "on" : a = left(a,instr(a," ")-1)
    rr("<input type=hidden name=defaultorder value=""" & thedefaultorder & """>")
    rr("<select name=defaultorderfield onchange='vbscript: f1.defaultorder.value = f1.defaultorderfield.value : if f1.defaultorderdesc.checked = true then f1.defaultorder.value = f1.defaultorderfield.value & "" desc""'>")
    rr("<option value=''>")
    rr("<option value=""id""") : if "id" = a then rr(" selected")
    rr(">ID")

    sq = "select tname from bumodulefields where tid = " & request_id & " and ttype <> 'memo' order by tpriority"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("tname") & """") : if lcase(rs1("tname")) = a then rr(" selected")
      rr(">" & rs1("tname"))
      rs1.movenext
    loop
    rr("</select>")
    rr("<input type=checkbox name=defaultorderdesc")
    rr(" onclick='vbscript: f1.defaultorder.value = f1.defaultorderfield.value : if f1.defaultorderdesc.checked = true then f1.defaultorder.value = f1.defaultorderfield.value & "" desc""'")
    if b = "on" then rr(" checked")
    rr("> ~Desc~")
']
  end if'form:defaultorder

  if oksee("updateduserid") then'form:updateduserid
    rr("<tr valign=top id=updateduserid_tr0><td class=form_item1 id=updateduserid_tr1><span id=updateduserid_caption>~bumodules_updateduserid~</span><td class=form_item2 id=updateduserid_tr2>")
    if ulev > 4 then
      rr("<select name=updateduserid dir=~langdir~>")
      sq = "select * from buusers order by username"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      rr("<option value=0>")
      do until rs1.eof
        rr("<option value=""" & rs1("id") & """")
        if cstr(Theupdateduserid) = cstr(rs1("id")) then rr(" selected")
        rr(">" & rs1("username"))
        rs1.movenext
      loop
      rr("</select>")'form:updateduserid
    else
      sq = "select * from buusers where id = " & theupdateduserid
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      if not rs1.eof then rr("<b>" & rs1("username") & "</b>")
    end if'form:updateduserid
  end if'form:updateduserid

  if oksee("updateddate") then'form:updateddate
    rr("<tr valign=top id=updateddate_tr0><td class=form_item1 id=updateddate_tr1><span id=updateddate_caption>~bumodules_updateddate~</span><td class=form_item2 id=updateddate_tr2>")
    if ulev > 4 then
      rr(SelectDate("f1.updateddate*:", theupdateddate))
    else
      rr(theupdateddate)
    end if
  end if'form:updateddate

  if oksee("permissions") then'form:permissions
    rr("<span id=permissions_caption></span> ")
    rr("<input type=hidden name=permissions value=""" & Thepermissions & """>")
  end if'form:permissions

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
'[    rr(" <input type=submit style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=submit style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button id=buttoncreate style='width:150;' value=""~Create in system~"" onclick='vbscript:if msgbox(""~Create ?~"",vbyesno) = 6 then f1.action2.value = ""build"" : checkform'>")
    If InStr(locked, "," & request_id & ",") = 0 And thecreated = "on" Then rr("<input type=button id=buttonremove style='width:150;' value=""~Remove from system~"" onclick='vbscript:if msgbox(""~Remove~?"",vbyesno) = 6 then f1.action2.value = ""clean"" : checkform'>")
']
    rr(" <input type=button id=buttonsavereturn style='width:80;' value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button id=buttonsave style='width:80;' value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button id=buttonduplicate style='width:80;' value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button id=buttondelete style='width:80;' value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button id=buttonback style='width:80;' value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  t = "admin_bumodulefields.asp?containerfield=tid&containerid=" & request_id : if getfield("bumodulefieldschildpageid") <> "" then t = t & "&action=form&id=" & getfield("bumodulefieldschildpageid") else t = t & "&action=search&action2=clear"
  if getpermission("bumodulefields") >= 1 and childpage then rr("<tr><td colspan=99><iframe id=frbumodulefields width=100% height=1 src=" & t & " frameborder=0></iframe>")

'[  t = "admin_bumodulecoderep.asp?containerfield=tid&containerid=" & request_id : if getfield("bumodulecoderepchildpageid") <> "" then t = t & "&action=form&id=" & getfield("bumodulecoderepchildpageid") else t = t & "&action=search&action2=clear"
'[  if getpermission("bumodulecoderep") >= 1 and childpage then rr("<tr><td colspan=99><iframe id=frbumodulecoderep width=100% height=1 src=" & t & " frameborder=0></iframe>")
  'rr("<tr valign=top><td class=form_item2 colspan=99>")
  'rr("<iframe id=freditpermissions width=100% height=0 src=?action=editpermissions&id=" & request_id & " frameborder=0></iframe>")

  t = "admin_bumodulecoderep.asp?containerfield=tid&containerid=" & request_id : if getfield("bumodulecoderepchildpageid") <> "" then t = t & "&action=form&id=" & getfield("bumodulecoderepchildpageid") else t = t & "&action=search&action2=clear"
  if getpermission("bumodulecoderep") >= 1 and childpage then rr("<tr><td colspan=99><iframe id=frbumodulecoderep width=100% height=1 src=" & t & " frameborder=0></iframe>")

  'rr("<tr valign=top><td class=form_item2 colspan=99>")
  'rr("<iframe id=freditfile width=100% height=0 src=?action=editfile&id=" & request_id & " frameborder=0></iframe>")

']
  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("title") then rr("    if f1.title.value <> """" then validfield(""title"") else invalidfield(""title"") : ok = false" & vbcrlf)
  if okedit("display") then rr("    if f1.display.value <> """" then validfield(""display"") else invalidfield(""display"") : ok = false" & vbcrlf)
  if okedit("priority") and session(tablename & "containerfield") <> "priority" then
    rr("    if isnumeric(f1.priority.value) then" & vbcrlf)
    rr("      if cdbl(f1.priority.value) >= -4000000000 and cdbl(f1.priority.value) <= 4000000000 then" & vbcrlf)
    rr("        validfield(""priority"")" & vbcrlf)
    rr("      else" & vbcrlf)
    rr("        invalidfield(""priority"")" & vbcrlf)
    rr("        ok = false" & vbcrlf)
    rr("      end if " & vbcrlf)
    rr("    else" & vbcrlf)
    rr("      invalidfield(""priority"")" & vbcrlf)
    rr("      ok = false" & vbcrlf)
    rr("    end if" & vbcrlf)
  end if
  if okedit("icon") then rr("    if f1.icon.value <> """" then validfield(""icon"") else invalidfield(""icon"") : ok = false" & vbcrlf)
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if request_action = "priorityshift" then
    ff = getfield("field") : i1 = request_id
    s = "select * from " & tablename & " where id = " & i1
    closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
    t = "" : if session(tablename & "containerid") <> "0" then t = " and " & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
    p1 = rs1(ff) : t1 = " < " : t2 = " desc" : if request_action2 = "down" then t1 = " > " : t2 = ""
    s = "select * from " & tablename & " where id <> " & i1 & " and " & ff & t1 & p1 & t & " order by " & ff & t2
    closers(rs2) : set rs2 = conn.execute(sqlfilter(s))
    if not rs2.eof then
      i2 = rs2("id") : p2 = rs2(ff)
      s = "update " & tablename & " set " & ff & " = " & p2 & " where id = " & i1
      closers(rs) : set rs = conn.execute(sqlfilter(s))
      s = "update " & tablename & " set " & ff & " = " & p1 & " where id = " & i2
      closers(rs) : set rs = conn.execute(sqlfilter(s))
    end if
  end if
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    a = ""
    For Each sessionitem in Session.Contents
      t = tablename & "_" : if left(sessionitem,len(t)) = t then a = a & sessionitem & ","
    Next
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      session(b) = ""
    loop
    session(tablename & "_firstcome") = "y"
    session(tablename & "_order") = tablename & ".priority"
    session(tablename & "_hidelistcolumns") = "icon,permissions,"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_groupseperator") = getfield("groupseperator")
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
'[  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 200
']
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".priority"
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/archives.png' ~iconsize~> ~bumodules_bumodules~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button id=buttonadd style='width:80;' value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif> <input type=text style='width:101;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  rr("<select name=stringoption>")
  rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  rr(">~All words~")
  rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  rr(">~Any word~")
  rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  rr(">~Exactly~")
  rr("</select>")

  rr(" <input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~")
  rr("<input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
'[  rr(">~Groups~")
  rr(">~Groups~")
  'rr(" <input type=checkbox name=showsystem")
  'If session("bumodules_showsystem") = "on" Then rr(" checked")
  'rr(">~bumodules_showsystem~")
']
  t = "title,display,priority,icon,createtable,addtrans,astree,multilang,addnav,showid,addsearch,addsearchbar,addcsv,addloadcsv,addprint,addmultiactions,addduplicatebutton,addpages,monthviewfield,created,defaultorder,updateduserid,updateddate,permissions,"
  t = "," & t : a = hidecontrols : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")
  rr(" <input type=button id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit id=buttonsearch value="" ~Show~ ""> ")

else
  rr(" <a href=?advanced=y>~Advanced Search~</a>")
end if
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & buusers_1.username) as updateduseridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join buusers buusers_1 on " & tablename & ".updateduserid = buusers_1.id)"

'[  '-sqladd
  'If session("bumodules_showsystem") <> "on" Then s = s & " and priority >= 0"
']
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","bumodules.id,bumodules.title,bumodules.display,bumodules.priority,bumodules.icon,bumodules.createtable,bumodules.addtrans,bumodules.astree,bumodules.multilang,bumodules.addnav,bumodules.showid,bumodules.addsearch,bumodules.addsearchbar,bumodules.addcsv,bumodules.addloadcsv,bumodules.addprint,bumodules.addmultiactions,bumodules.addduplicatebutton,bumodules.addpages,bumodules.monthviewfield,bumodules.created,bumodules.defaultorder,bumodules.updateduserid,bumodules.updateddate,bumodules.permissions",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 3 then response.clear : errorend("_back~Search string must include at least 3 characters")
    ww = ww & " "
    do until ww = ""
      w = left(ww,instr(ww," ")-1) : ww = mid(ww,instr(ww," ")+1)
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        t = "select distinct tid from bumodulecoderep where (title like '%" & w & "%' or text1 like '%" & w & "%' or text2 like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        ids = "" : do until rs1.eof : ids = ids & rs1("tid") & "," : rs1.movenext :loop : ids = ids & "-1"
        s = s & " " & tablename & ".id in(" & ids & ") or"
        t = "select distinct tid from bumodulefields where (tname like '%" & w & "%' or tdisplay like '%" & w & "%' or tcontrol like '%" & w & "%' or ttype like '%" & w & "%' or tdefault like '%" & w & "%' or tdata like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        ids = "" : do until rs1.eof : ids = ids & rs1("tid") & "," : rs1.movenext :loop : ids = ids & "-1"
        s = s & " " & tablename & ".id in(" & ids & ") or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".display like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".priority = " & w & " or"
        s = s & " " & tablename & ".icon like '%" & w & "%' or"
        s = s & " " & tablename & ".addnav like '%" & w & "%' or"
        s = s & " " & tablename & ".monthviewfield like '%" & w & "%' or"
        s = s & " " & tablename & ".defaultorder like '%" & w & "%' or"
        s = s & " cstr('' & buusers_1.username) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".permissions like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title2>")
  if oksee("title") then f = "bumodules.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_title~</a> " & a2)
  if oksee("display") then f = "bumodules.display" : f2 = "display" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("display") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_display~</a> " & a2)
  if oksee("priority") then f = "bumodules.priority" : f2 = "priority" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("priority") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_priority~</a> " & a2)
  if oksee("priority") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_priority~</a> " & a2)
  rr("<td class=list_title2><nobr>~bumodulecoderep_bumodulecoderep~")
']
  if oksee("icon") then f = "bumodules.icon" : f2 = "icon" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("icon") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_icon~</a> " & a2)
  if oksee("createtable") then f = "bumodules.createtable" : f2 = "createtable" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("createtable") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_createtable~</a> " & a2)
  if oksee("addtrans") then f = "bumodules.addtrans" : f2 = "addtrans" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addtrans") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addtrans~</a> " & a2)
  if oksee("astree") then f = "bumodules.astree" : f2 = "astree" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("astree") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_astree~</a> " & a2)
  if oksee("multilang") then f = "bumodules.multilang" : f2 = "multilang" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("multilang") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_multilang~</a> " & a2)
  if oksee("addnav") then f = "bumodules.addnav" : f2 = "addnav" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addnav") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addnav~</a> " & a2)
  if oksee("showid") then f = "bumodules.showid" : f2 = "showid" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("showid") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_showid~</a> " & a2)
  if oksee("addsearch") then f = "bumodules.addsearch" : f2 = "addsearch" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addsearch") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addsearch~</a> " & a2)
  if oksee("addsearchbar") then f = "bumodules.addsearchbar" : f2 = "addsearchbar" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addsearchbar") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addsearchbar~</a> " & a2)
  if oksee("addcsv") then f = "bumodules.addcsv" : f2 = "addcsv" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addcsv") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addcsv~</a> " & a2)
  if oksee("addloadcsv") then f = "bumodules.addloadcsv" : f2 = "addloadcsv" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addloadcsv") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addloadcsv~</a> " & a2)
  if oksee("addprint") then f = "bumodules.addprint" : f2 = "addprint" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addprint") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addprint~</a> " & a2)
  if oksee("addmultiactions") then f = "bumodules.addmultiactions" : f2 = "addmultiactions" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addmultiactions") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addmultiactions~</a> " & a2)
  if oksee("addduplicatebutton") then f = "bumodules.addduplicatebutton" : f2 = "addduplicatebutton" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addduplicatebutton") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addduplicatebutton~</a> " & a2)
  if oksee("addpages") then f = "bumodules.addpages" : f2 = "addpages" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("addpages") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_addpages~</a> " & a2)
  if oksee("monthviewfield") then f = "bumodules.monthviewfield" : f2 = "monthviewfield" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("monthviewfield") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_monthviewfield~</a> " & a2)
  if oksee("created") then f = "bumodules.created" : f2 = "created" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("created") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_created~</a> " & a2)
  if oksee("defaultorder") then f = "bumodules.defaultorder" : f2 = "defaultorder" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("defaultorder") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_defaultorder~</a> " & a2)
  if oksee("updateduserid") and session(tablename & "containerfield") <> "updateduserid" then
    f = "buusers_1.username" : f2 = "updateduseridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_updateduserid~</a> " & a2)
  end if
  if oksee("updateddate") then f = "bumodules.updateddate" : f2 = "updateddate" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("updateddate") then rr("<td class=list_title2><nobr><a class=list_title2_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~bumodules_updateddate~</a> " & a2)
  if oksee("permissions") then rr("<td class=list_title2><nobr>~bumodules_permissions~")

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Thedisplay = rs("display") : if isnull(Thedisplay) then Thedisplay = ""
      Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
      Thepriority = replace(thepriority,",",".")
      Theicon = rs("icon") : if isnull(Theicon) then Theicon = ""
      Thecreatetable = rs("createtable") : if isnull(Thecreatetable) then Thecreatetable = ""
      Theaddtrans = rs("addtrans") : if isnull(Theaddtrans) then Theaddtrans = ""
      Theastree = rs("astree") : if isnull(Theastree) then Theastree = ""
      Themultilang = rs("multilang") : if isnull(Themultilang) then Themultilang = ""
      Theaddnav = rs("addnav") : if isnull(Theaddnav) then Theaddnav = ""
      Theshowid = rs("showid") : if isnull(Theshowid) then Theshowid = ""
      Theaddsearch = rs("addsearch") : if isnull(Theaddsearch) then Theaddsearch = ""
      Theaddsearchbar = rs("addsearchbar") : if isnull(Theaddsearchbar) then Theaddsearchbar = ""
      Theaddcsv = rs("addcsv") : if isnull(Theaddcsv) then Theaddcsv = ""
      Theaddloadcsv = rs("addloadcsv") : if isnull(Theaddloadcsv) then Theaddloadcsv = ""
      Theaddprint = rs("addprint") : if isnull(Theaddprint) then Theaddprint = ""
      Theaddmultiactions = rs("addmultiactions") : if isnull(Theaddmultiactions) then Theaddmultiactions = ""
      Theaddduplicatebutton = rs("addduplicatebutton") : if isnull(Theaddduplicatebutton) then Theaddduplicatebutton = ""
      Theaddpages = rs("addpages") : if isnull(Theaddpages) then Theaddpages = ""
      Themonthviewfield = rs("monthviewfield") : if isnull(Themonthviewfield) then Themonthviewfield = ""
      Thecreated = rs("created") : if isnull(Thecreated) then Thecreated = ""
      Thedefaultorder = rs("defaultorder") : if isnull(Thedefaultorder) then Thedefaultorder = ""
      Theupdateduserid = rs("updateduserid") : if isnull(Theupdateduserid) then Theupdateduserid = 0
      Theupdateduserid = replace(theupdateduserid,",",".")
      Theupdateddate = rs("updateddate") : if isnull(Theupdateddate) then Theupdateddate = ""
      Thepermissions = rs("permissions") : if isnull(Thepermissions) then Thepermissions = ""
      Theupdateduseridlookup = rs("updateduseridlookup") : if isnull(Theupdateduseridlookup) then Theupdateduseridlookup = ""

      okwritehere = okwrite(tablename,theid)
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"

      if session(tablename & "_order") = tablename & ".priority" then
        rr("<a href=?action=priorityshift&action2=up&field=priority&id=" & theid & "><img src=up.gif border=0></a>")
        rr("<a href=?action=priorityshift&action2=down&field=priority&id=" & theid & "><img src=down.gif border=0></a> ")
      end if

      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
'[      rr("<img src=icons/archives.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      rr("<img src=" & theicon & " ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
']
      if oksee("display") then rr("<td class=list_item valign=top align=><nobr>" & thedisplay)
      if oksee("priority") and session(tablename & "containerfield") <> "priority" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("priority") then
      elseif session(tablename & "containerfield") = "priority" then
        rr("<input type=hidden name=listupdate_priority_" & theid & " value=" & thepriority & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "priority" then
        rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=listupdate_priority_" & theid & " value=""" & thepriority & """>")
      else
'[        rr(thepriority)
'[      end if
        rr(thepriority)
      end if
      closers(rs1) : set rs1 = conn.execute(SQLfilter("select count(id) as c from bumodulecoderep where tid = " & rs("id")))
      t = rs1("c"): If IsNull(t) Then t = 0
      closers(rs1) : set rs1 = conn.execute(SQLfilter("select * from bumodulecoderep where tid = " & rs("id") & " and applytoall = 'on'"))
      If Not rs1.EOF Then t = "<font color=ff0000><b>" & t & "</b></font>"
      rr("<td class=list_item valign=top align=center><nobr>" & t)

']
      if oksee("icon") then rr("<td class=list_item valign=top align=><nobr>" & theicon)
      if oksee("createtable") and session(tablename & "containerfield") <> "createtable" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("createtable") then
      elseif session(tablename & "containerfield") = "createtable" then
        rr("<input type=hidden name=listupdate_createtable_" & theid & " value=" & thecreatetable & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "createtable" then
        rr("<input type=checkbox name=listupdate_createtable_" & theid & "")
        if thecreatetable = "on" then rr(" checked")
        rr(">")
      else
        if thecreatetable = "on" then rr("v")
      end if
      if oksee("addtrans") and session(tablename & "containerfield") <> "addtrans" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addtrans") then
      elseif session(tablename & "containerfield") = "addtrans" then
        rr("<input type=hidden name=listupdate_addtrans_" & theid & " value=" & theaddtrans & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addtrans" then
        rr("<input type=checkbox name=listupdate_addtrans_" & theid & "")
        if theaddtrans = "on" then rr(" checked")
        rr(">")
      else
        if theaddtrans = "on" then rr("v")
      end if
      if oksee("astree") and session(tablename & "containerfield") <> "astree" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("astree") then
      elseif session(tablename & "containerfield") = "astree" then
        rr("<input type=hidden name=listupdate_astree_" & theid & " value=" & theastree & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "astree" then
        rr("<input type=checkbox name=listupdate_astree_" & theid & "")
        if theastree = "on" then rr(" checked")
        rr(">")
      else
        if theastree = "on" then rr("v")
      end if
      if oksee("multilang") and session(tablename & "containerfield") <> "multilang" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("multilang") then
      elseif session(tablename & "containerfield") = "multilang" then
        rr("<input type=hidden name=listupdate_multilang_" & theid & " value=" & themultilang & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "multilang" then
        rr("<input type=checkbox name=listupdate_multilang_" & theid & "")
        if themultilang = "on" then rr(" checked")
        rr(">")
      else
        if themultilang = "on" then rr("v")
      end if
      if oksee("addnav") and session(tablename & "containerfield") <> "addnav" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addnav") then
      elseif session(tablename & "containerfield") = "addnav" then
        rr("<input type=hidden name=listupdate_addnav_" & theid & " value=" & theaddnav & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addnav" then
        a = "y,n,s,i,"
        rr("<select name=listupdate_addnav_" & theid & " dir=~langdir~>")
        do until a = ""
          b = left(a,instr(a,",")-1)
          a = mid(a,instr(a,",")+1)
          rr("<option value=""" & b & """")
          if theaddnav = b then rr(" selected")
          rr(">" & b)
        loop
        rr("</select>")'form:listupdate_addnav
      else
        rr(theaddnav)
      end if
      if oksee("showid") and session(tablename & "containerfield") <> "showid" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("showid") then
      elseif session(tablename & "containerfield") = "showid" then
        rr("<input type=hidden name=listupdate_showid_" & theid & " value=" & theshowid & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "showid" then
        rr("<input type=checkbox name=listupdate_showid_" & theid & "")
        if theshowid = "on" then rr(" checked")
        rr(">")
      else
        if theshowid = "on" then rr("v")
      end if
      if oksee("addsearch") and session(tablename & "containerfield") <> "addsearch" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addsearch") then
      elseif session(tablename & "containerfield") = "addsearch" then
        rr("<input type=hidden name=listupdate_addsearch_" & theid & " value=" & theaddsearch & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addsearch" then
        rr("<input type=checkbox name=listupdate_addsearch_" & theid & "")
        if theaddsearch = "on" then rr(" checked")
        rr(">")
      else
        if theaddsearch = "on" then rr("v")
      end if
      if oksee("addsearchbar") and session(tablename & "containerfield") <> "addsearchbar" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addsearchbar") then
      elseif session(tablename & "containerfield") = "addsearchbar" then
        rr("<input type=hidden name=listupdate_addsearchbar_" & theid & " value=" & theaddsearchbar & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addsearchbar" then
        rr("<input type=checkbox name=listupdate_addsearchbar_" & theid & "")
        if theaddsearchbar = "on" then rr(" checked")
        rr(">")
      else
        if theaddsearchbar = "on" then rr("v")
      end if
      if oksee("addcsv") and session(tablename & "containerfield") <> "addcsv" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addcsv") then
      elseif session(tablename & "containerfield") = "addcsv" then
        rr("<input type=hidden name=listupdate_addcsv_" & theid & " value=" & theaddcsv & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addcsv" then
        rr("<input type=checkbox name=listupdate_addcsv_" & theid & "")
        if theaddcsv = "on" then rr(" checked")
        rr(">")
      else
        if theaddcsv = "on" then rr("v")
      end if
      if oksee("addloadcsv") and session(tablename & "containerfield") <> "addloadcsv" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addloadcsv") then
      elseif session(tablename & "containerfield") = "addloadcsv" then
        rr("<input type=hidden name=listupdate_addloadcsv_" & theid & " value=" & theaddloadcsv & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addloadcsv" then
        rr("<input type=checkbox name=listupdate_addloadcsv_" & theid & "")
        if theaddloadcsv = "on" then rr(" checked")
        rr(">")
      else
        if theaddloadcsv = "on" then rr("v")
      end if
      if oksee("addprint") and session(tablename & "containerfield") <> "addprint" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addprint") then
      elseif session(tablename & "containerfield") = "addprint" then
        rr("<input type=hidden name=listupdate_addprint_" & theid & " value=" & theaddprint & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addprint" then
        rr("<input type=checkbox name=listupdate_addprint_" & theid & "")
        if theaddprint = "on" then rr(" checked")
        rr(">")
      else
        if theaddprint = "on" then rr("v")
      end if
      if oksee("addmultiactions") and session(tablename & "containerfield") <> "addmultiactions" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addmultiactions") then
      elseif session(tablename & "containerfield") = "addmultiactions" then
        rr("<input type=hidden name=listupdate_addmultiactions_" & theid & " value=" & theaddmultiactions & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addmultiactions" then
        rr("<input type=checkbox name=listupdate_addmultiactions_" & theid & "")
        if theaddmultiactions = "on" then rr(" checked")
        rr(">")
      else
        if theaddmultiactions = "on" then rr("v")
      end if
      if oksee("addduplicatebutton") and session(tablename & "containerfield") <> "addduplicatebutton" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addduplicatebutton") then
      elseif session(tablename & "containerfield") = "addduplicatebutton" then
        rr("<input type=hidden name=listupdate_addduplicatebutton_" & theid & " value=" & theaddduplicatebutton & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addduplicatebutton" then
        rr("<input type=checkbox name=listupdate_addduplicatebutton_" & theid & "")
        if theaddduplicatebutton = "on" then rr(" checked")
        rr(">")
      else
        if theaddduplicatebutton = "on" then rr("v")
      end if
      if oksee("addpages") and session(tablename & "containerfield") <> "addpages" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("addpages") then
      elseif session(tablename & "containerfield") = "addpages" then
        rr("<input type=hidden name=listupdate_addpages_" & theid & " value=" & theaddpages & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "addpages" then
        rr("<input type=checkbox name=listupdate_addpages_" & theid & "")
        if theaddpages = "on" then rr(" checked")
        rr(">")
      else
        if theaddpages = "on" then rr("v")
      end if
'[      if oksee("monthviewfield") then rr("<td class=list_item valign=top align=><nobr>" & themonthviewfield)
      if oksee("monthviewfield") then rr("<td class=list_item valign=top align=><nobr>") : if themonthviewfield <> "" then rr("v")
']
      if oksee("created") then rr("<td class=list_item valign=top align=><nobr>") : if thecreated = "on" then rr("v")
      if oksee("defaultorder") and session(tablename & "containerfield") <> "defaultorder" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("defaultorder") then
      elseif session(tablename & "containerfield") = "defaultorder" then
        rr("<input type=hidden name=listupdate_defaultorder_" & theid & " value=" & thedefaultorder & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "defaultorder" then
        rr("<input type=text maxlength=50 name=listupdate_defaultorder_" & theid & " onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & thedefaultorder & """ dir=ltr>")
      else
        rr(thedefaultorder)
      end if
      if oksee("updateduserid") and session(tablename & "containerfield") <> "updateduserid" then rr("<td class=list_item valign=top align=><nobr>" & theupdateduseridlookup)
      if oksee("updateddate") then rr("<td class=list_item valign=top align=><nobr>" & theupdateddate)
      thepermissions = thepermissions : if isnull(thepermissions) then thepermissions = ""
      thepermissions = replace(thepermissions,"<br>"," ") : thepermissions = replace(thepermissions,"<","[") : thepermissions = replace(thepermissions,">","]") : if len(thepermissions) > 30 then thepermissions = left(thepermissions,30) & "..."
      if oksee("permissions") then rr("<td class=list_item valign=top align=><nobr>" & thepermissions)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("display") and session(tablename & "containerfield") <> "display" then rr("<td class=list_total1>") 'display
  if oksee("priority") and session(tablename & "containerfield") <> "priority" then rr("<td class=list_total1>") 'priority
  if oksee("icon") and session(tablename & "containerfield") <> "icon" then rr("<td class=list_total1>") 'icon
  if oksee("createtable") and session(tablename & "containerfield") <> "createtable" then rr("<td class=list_total1>") 'createtable
  if oksee("addtrans") and session(tablename & "containerfield") <> "addtrans" then rr("<td class=list_total1>") 'addtrans
  if oksee("astree") and session(tablename & "containerfield") <> "astree" then rr("<td class=list_total1>") 'astree
  if oksee("multilang") and session(tablename & "containerfield") <> "multilang" then rr("<td class=list_total1>") 'multilang
  if oksee("addnav") and session(tablename & "containerfield") <> "addnav" then rr("<td class=list_total1>") 'addnav
  if oksee("addsearch") and session(tablename & "containerfield") <> "addsearch" then rr("<td class=list_total1>") 'addsearch
  if oksee("addsearchbar") and session(tablename & "containerfield") <> "addsearchbar" then rr("<td class=list_total1>") 'addsearchbar
  if oksee("addcsv") and session(tablename & "containerfield") <> "addcsv" then rr("<td class=list_total1>") 'addcsv
  if oksee("addloadcsv") and session(tablename & "containerfield") <> "addloadcsv" then rr("<td class=list_total1>") 'addloadcsv
  if oksee("addmultiactions") and session(tablename & "containerfield") <> "addmultiactions" then rr("<td class=list_total1>") 'addmultiactions
  if oksee("addduplicatebutton") and session(tablename & "containerfield") <> "addduplicatebutton" then rr("<td class=list_total1>") 'addduplicatebutton
  if oksee("addpages") and session(tablename & "containerfield") <> "addpages" then rr("<td class=list_total1>") 'addpages
  if oksee("created") and session(tablename & "containerfield") <> "created" then rr("<td class=list_total1>") 'created
  if oksee("defaultorder") and session(tablename & "containerfield") <> "defaultorder" then rr("<td class=list_total1>") 'defaultorder
  if oksee("updateduserid") and session(tablename & "containerfield") <> "updateduserid" then rr("<td class=list_total1>") 'updateduserid
  if oksee("updateddate") and session(tablename & "containerfield") <> "updateddate" then rr("<td class=list_total1>") 'updateddate
  if oksee("permissions") and session(tablename & "containerfield") <> "permissions" then rr("<td class=list_total1>") 'permissions

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
'[  if ulev > 1 then rr(" <input type=button id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete All Checked~'>")
  if ulev > 1 then rr(" <input type=button id=buttonmulticreate style='width:150;' onclick='vbscript:if msgbox(""~Create in the system all checked items ?~"",vbyesno) = 6 then flist.action.value = ""multiupdate"" : flist.action2.value = ""build"" : flist.submit' value='~Create in system~'>")
  if ulev > 1 then rr(" <input type=button id=buttonmultiremove style='width:150;' onclick='vbscript:if msgbox(""~Remove from the system all checked items ?~"",vbyesno) = 6 then flist.action.value = ""multiupdate"" : flist.action2.value = ""clean"" : flist.submit' value='~Remove from system~'>")
  if ulev > 1 then rr(" <input type=button id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete All Checked~'>")
']
  if ulev > 1 then

'[    rr(" <input type=button id=buttonmultiupdate value='~Update~' style='width:80;' onclick='vbscript:if msgbox(""~Update All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
    rr(" <input type=button id=buttonmultiupdate value='~Update~' style='width:80;' onclick='vbscript:if msgbox(""~Update All Checked ?~"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
    rr("<tr><td colspan=99><center><br>")
    rr("&nbsp;&nbsp;&nbsp;<a href=?action=editallpermissions><b>~Edit All Permissions~</a>")
    if right(scriptname,4) = ".asp" then
      rr("&nbsp;&nbsp;&nbsp;<a href=_publish.asp><b>~Publish System~</a>")
      rr("&nbsp;&nbsp;&nbsp;<a href=_asp2net.asp><b>~Convet to .net~</a>")
    end if
']
  end if

  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth + 20 : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
'[rend()
session("trans") = ""
rend()
']
%>
