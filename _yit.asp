<!--#include file="include.asp"-->
<%
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
function SendYIT(byval ids)
  'du = addlog("upload/yitresponse.txt", now & " TRY " & ids & "<br>")
  dim a,b,c,d,i,j,jj,p,pp,t,rs,rs1,s,s1,doit,date1,theday1,thedate1,theyear1,themonth1,date2,theday2,thedate2,theyear2,themonth2
  dim orderid,thedirection,thestatus,theordertypeid,thecourseid,thecoursename,thefrom1,theto1,thevia,thepids,thepcount,theprice,theremarks,thehistory,thealtstations,thecarid,thesupplierid,thesiteid,cities
  dim line_description,dur,is_active,visa_number,client_order_code,line_date,start_time,end_time,group_name,department_name,short_remarks,long_remarks,order_car_type,order_car_type_code,line_pass_qty,station_minutes
  dim client_price,course_id,course_direction,create_by,station_city,pass_internal_code,last_name,first_name,pass_phone,pass_department_name,station_street,pass_qty,station_remark,thestation,collect,getstationname
  dim target_account,datatosend,lena,o,u,r,q,yit_response_text,yit_response_code,h,ridedirection,sn,sep

  if session("supplieryitaccounts") = "" then
    s = "select id,data1 from suppliers"
    set rs = conn.execute(sqlfilter(s))
    do until rs.eof
      d = rs("data1") : t = getval(d, "yitaccount")
      session("supplieryitaccounts") = setval(session("supplieryitaccounts"), rs("id"), t)
      rs.movenext
    loop
  end if
  if session("supplieryitaccounts") = "|" then exit function

  jj = ids : q = ""

  do until jj = ""
    j = left(jj,instr(jj,",")-1) : jj = mid(jj,instr(jj,",")+1)
    s = "select * from orders where id = " & j
    closers(rs) : set rs = conn.execute(sqlfilter(s))
    orderid = rs("id")
    date1 = rs("date1") : thedate1 = getdatedatestring(date1) : thetime1 = getdatehourstring(date1) ' ��� �����
    theday1 = datepart("d",thedate1) : themonth1 = datepart("m",thedate1) : theyear1 = datepart("yyyy",thedate1)
    thedate1 = theyear1 &"/" & themonth1 & "/" & theday1

    date2 = rs("date2") : thedate2 = getdatedatestring(date2) : thetime2 = getdatehourstring(date2) ' ��� �����
    theday2 = datepart("d",thedate1) : themonth2 = datepart("m",thedate1) : theyear2 = datepart("yyyy",thedate1)
    thedate2 = theyear2 &"/" & themonth2 & "/" & theday2

    thedirection = rs("direction")
    if thedirection = "A" then codedirection = "1" : ridedirection = "�����"
    if thedirection = "B" then codedirection = "2" : ridedirection = "�����"
    thestatus = rs("status")
    theordertypeid = rs("ordertypeid")
    thecourseid = rs("courseid")
    thecoursename = getcode("courses", thecourseid) & " - " & getonefield("select title from courses where id = "& thecourseid)
    thefrom1 = rs("from1")
    theto1 = rs("to1")
    thevia = rs("via1")
    thepids = rs("pids")
    thepcount = rs("pcount")
    theprice = rs("price")
    theremarks = rs("remarks")
    thehistory = rs("history")
    thedata1 = rs("data1")    
    thealtstations = rs("altstations")
    thecarid = rs("carid")
    thesupplierid = rs("supplierid")
    thesiteid = rs("siteid")
    collect = "" : cities = ""
    
    if specs = "teva" then
      collect = "fromhome"
      if thetime1 => "06:00" and thetime1 =< "18:00" then collect = ""
    end if

    if specs = "xxx" then getstationname = "inmap"

    doit = true
    if NOT hasmatch(thestatus, "Sent,Received,Done,DoneApprove,Canceled,") then doit = false ': du = addlog("upload/yitresponse.txt", now & j & " - other status<br>")
    target_account = getval(session("supplieryitaccounts"), thesupplierid) : if target_account = "" then doit = false 's: du = addlog("upload/yitresponse.txt", now & j & " - other supplier<br>")
    if specs = "egged" and cstr(thesupplierid) = "4" and cstr(theordertypeid) = "5" then ' �� ����� ������ ������ ���� ���
      s = "update orders set status = 'Done' where id = "& orderid
      closers(rs) : set rs = conn.execute(sqlfilter(s))
      doit = false
    end if

    if doit then
      visa_number = orderid
      is_active = "1" : if thestatus = "Canceled" then is_active = "0"
      client_order_code = "0"
      line_date = thedate1
      start_time = thetime1
      dur = getval(thehistory,"duration")
      end_time = getdatehourstring(dateadd("n", dur , rs("date1")))
      if dur = 0 then end_time = thetime2
      pp = thepids : unitlist = ""
      do until pp = ""
        p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
        workerunits = gettitle("units", getonefield("select unitid from workers where id = "& p ))
        unitlist = unitlist & workerunits & ","
      loop
      group_name = gettitle("sites", thesiteid)
      line_type = "" 'gettitle("ordertypes",theordertypeid)
      if hasmatch(specs, "bontour,jr,") then line_type = theordertypeid
      via_stations = thevia
      department_name = uniquelist(unitlist)
      if hasmatch(specs, "jr,teva,") then department_name = ""
      order_car_type = gettitle("cars", thecarid)
      order_car_type_code = thecarid
      line_pass_qty = thepcount 
      client_price = theprice
      course_id = strfilter(getcode("courses", thecourseid),"0123456789")

      ii = getval(thehistory,"stationcourse")
      if ii <> "" then
        do until ii = ""
          i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1) : j = i
          i = gettitle("stations", i)
          if getstationname = "inmap" then i = getonefield("select inmap from stations where id = "& j)

          if instr(i, ";") > 0 then sep = ";"
          if instr(i, ",") > 0 then sep = ","
          if sep <> "" then station_city = mid(i,instr(i,sep)+1)
          cities = cities & station_city : cities = uniquelist(cities)
        loop
          line_description = ridedirection & ": " & cities
          if cities = "" then line_description = ridedirection & ": " & gettitle("stations", thefrom1) & " - " & gettitle("stations", theto1)

      elseif ii = "" and thefrom1 > "0" and theto1 > "0" then
        line_description = ridedirection & ": " & gettitle("stations", thefrom1) & " - " & gettitle("stations", theto1)

      '--special
      elseif thecourseid > "0" and hasmatch(specs, "teva,mizrahi,tnuva,egged,") then
        line_description = ridedirection & ": " & thecoursename
      end if

      course_direction = codedirection
      short_remarks = theremarks
      if short_remarks = "�����" then short_remarks = ""
      if short_remarks <> "" then short_remarks = replace(short_remarks, "<br>" ,"; ")
      create_by = "99995"
      '---

      a = "" : q = ""
      '---preview on local
      q = q & "<table cellpadding=5 cellspacing=5>"
      q = q & "<tr><td bgcolor=#dddddd>���� ����:</td><td>"& visa_number &"</td><td bgcolor=#dddddd>��� ����:</td><td>"& client_order_code &"</td><td bgcolor=#dddddd>�� �����:</td><td>"& department_name &"</td><td bgcolor=#dddddd>�� �����:</td><td>"& group_name &"</td><td bgcolor=#dddddd>��� �����:</td><td>"& line_type &"</td></tr>"
      q = q & "<tr><td bgcolor=#dddddd>�����:</td><td dir=ltr>("& rs("status") &") " & is_active &"</td><td bgcolor=#dddddd>�����:</td><td>"& line_date &"</td><td bgcolor=#dddddd>��� �����:</td><td>"& start_time &"</td><td bgcolor=#dddddd>��� ����:</td><td>"& end_time &"</td><td bgcolor=#dddddd>�����:</td><td>("& ridedirection &") "& course_direction &"</td></tr>"
      q = q & "<tr><td bgcolor=#dddddd>���� ���:</td><td>"& line_description &"</td><td bgcolor=#dddddd>��� �����:</td><td>"& course_id &"</td><td bgcolor=#dddddd>��� ���:</td><td>"& order_car_type &"</td><td bgcolor=#dddddd>��� ���:</td><td>"& order_car_type_code &"</td><td bgcolor=#dddddd>����:</td><td>"& client_price &"</td></tr>"
      q = q & "<tr><td bgcolor=#dddddd>���� �� ���:</td><td>"& create_by &"</td><td bgcolor=#dddddd>����� ���:</td><td>"& short_remarks &"</td><td bgcolor=#dddddd>����� ����:</td><td>"& long_remarks &"</td><td bgcolor=#dddddd>XXX1:</td><td>XXX1</td><td bgcolor=#dddddd>XXX2:</td><td>XXX2</td></tr>"
      q = q & "</table>"

      a = a & "<?xml version=""1.0"" encoding=""windows-1255"" ?>"
      a = a & "<root>"
      a = a & " <line>"
      a = a & "  <visa_number>"& visa_number &"</visa_number> "
      a = a & "  <client_order_code>"& client_order_code &"</client_order_code> "
      a = a & "  <is_active>"& is_active &"</is_active> "
      a = a & "  <line_date>"& line_date &"</line_date> "
      a = a & "  <start_time>"& start_time &"</start_time> "
      a = a & "  <end_time>"& end_time &"</end_time> "
      a = a & "  <line_description>"& line_description &"</line_description> "
      a = a & "  <line_type>"& line_type &"</line_type> "
      a = a & "  <group_name>"& group_name &"</group_name> "
      a = a & "  <department_name>"& department_name &"</department_name> "
      a = a & "  <short_remarks>"& short_remarks &"</short_remarks> "
      a = a & "  <long_remarks>"& long_remarks &"</long_remarks> "
      a = a & "  <order_car_type>"& order_car_type &"</order_car_type> " '!!!
      a = a & "  <order_car_type_code>"& order_car_type_code &"</order_car_type_code> " '!!!
      a = a & "  <line_pass_qty>"& line_pass_qty &"</line_pass_qty> " '!!!
      a = a & "  <client_price>"& client_price &"</client_price> "
      a = a & "  <course_id>"& course_id &"</course_id> "
      a = a & "  <course_direction>"& course_direction &"</course_direction> "
      a = a & "  <course_alternative></course_alternative> "
      a = a & "  <create_by></create_by> "

      '---
      if getval(thehistory, "stationcourse") <> "" then
        du = courseplantaxi(rs("date1"), thehistory, thepids, thedirection, thealtstations)
        pp = getval(courseplantaxibuf, "pp") : c = 0 : sn = 0
        myride = "geo"

      elseif getval(thehistory, "textstations") <> "" then
        'GETT_RIDE (only in TEVA now, like geo but different style - no station ids, only text geo locations...)
        myride = "gett"
      
      elseif thecourseid <> "" and thecourseid <> "0" and thepids <> "" then
        du = courseplan(thecourseid, rs("date1"), thepids, thealtstations, thedirection, thehistory, rs("data1")) 'CLASSIC_COURSE
        'courseplanworkers, courseplanworkertimes, courseplanworkerstations, courseplanallstations, courseplanarrive, courseplanduration, courseplanadditionalworkers, courseplanadditionalstations, courseplanplan, courseplanstationtimes
        pp = courseplanworkers
        myride = "course"

      elseif thefrom1 <> "" and thefrom1 <> "0" and theto1 <> "" and theto1 <> "0" then
        'RIDE FROM TO
        pp = getonefield("select pids from orders where id = "& orderid)
        myride = "fromto"

      elseif thecourseid <> "" and thecourseid <> "0" and thepids = "" then
        pp = getidlist("select stationid from coursestations where courseid = " & thecourseid & " order by minutes")
        if thedirection = "B" then pp = reverselist(pp)
        myride = "nopids"
      else
        'INVALID_RIDE
      end if

      '---
      q = q & "<table cellpadding=5 cellspacing=5 border=1>"
      q = q & "<tr bgcolor=#dddddd>"
      q = q & "<td width=100>���� ����</td><td width=100>��� ����</td><td width=100>���</td><td width=100>�� �����</td><td width=100>�� ����</td><td width=100>�����</td><td width=100>���</td><td width=100>����</td><td width=100>���</td><td width=100>�����</td>"
      q = q & "</tr>"

      do until pp = ""
        c = c + 1 : sn = sn + 1 : pickup = "" : dropoff = "" : sep = "" : station_street = "" : station_city = "" : start_time = ""
        p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
        if myride <> "nopids" then
          pass_internal_code = getcode("workers", p)
          last_name = getonefield("select lastname from workers where id = "& p)
          first_name = getonefield("select firstname from workers where id = "& p)
          pass_phone = getonefield("select phone1 from workers where id = "& p)
        end if

        if myride = "course" then
          station_start_time = getval(courseplanworkertimes, p)
          station_street = gettitle("stations", getval(thealtstations, p & "from1"))
          if station_street = "" then
            cs = getidlist("select stationid from coursestations where courseid = "& thecourseid)
            do until cs = ""
              ss = left(cs,instr(cs,",")-1) : cs = mid(cs,instr(cs,",")+1)
              station_street = gettitle("stations",ss)
            loop
          end if
          station_remark = "����: " & gettitle("stations", getval(thealtstations, p & "to1"))

          if thedirection = "B" then
            station_street = gettitle("stations", getval(thealtstations, p & "from1"))
            station_remark = "����: " & gettitle("stations", getval(thealtstations, p & "to1"))
          end if

        elseif myride = "geo" then
          station_start_time = getdatehourstring(getval(courseplantaxibuf, p & "pick"))
          station_street = gettitle("stations", getval(courseplantaxibuf, p & "on"))
          station_remark = "����: " & gettitle("stations", getval(courseplantaxibuf, p & "off"))

          if thedirection = "B" then
            station_start_time = getdatehourstring(getval(courseplantaxibuf, p & "drop"))
            station_street = gettitle("stations", getval(courseplantaxibuf, p & "off"))
            station_remark = "����: " & gettitle("stations", getval(courseplantaxibuf, p & "on"))
          end if

        elseif myride = "fromto" then
          station_street = gettitle("stations", thefrom1)
          station_remark = "����: " & gettitle("stations", theto1)

          if thedirection = "B" then
            station_street = gettitle("stations", theto1)
            station_remark = "����: " & gettitle("stations", thefrom1)
          end if

        elseif myride = "nopids" then
          station_minutes = getonefield("select minutes from coursestations where courseid = "& thecourseid &" and stationid = "& p)
          start_time = dateadd("n", station_minutes, rs("date1"))
          station_start_time = getdatehourstring(start_time)
          station_street = gettitle("stations", p)
          station_remark = "����: " & gettitle("stations",getonefield("select stationid from coursestations where courseid = "& thecourseid))
        end if
        '---
        if collect = "fromhome" then station_street = getonefield("select address from workers where id = "& p)

        thestation = station_street : sep = ""
        if instr(thestation, ";") > 0 then sep = ";"
        if instr(thestation, ",") > 0 then sep = ","

        if sep <> "" then station_street = left(thestation,instr(thestation,sep)-1)
        if sep <> "" then station_city = mid(thestation,instr(thestation,sep)+1)

        station_number = sn
        station_street = strfilter(station_street, "��������������������������� -,'.")
        station_house = strfilter(thestation, "0123456789")

        '---preview on local
        q = q & "<tr>"
        q = q & "<td width=100>"& station_number &"</td><td width=100>"& pass_internal_code &"</td><td width=100>"& station_start_time &"</td><td width=100>"& last_name &"</td><td width=100>"& first_name &"</td><td width=100>"& pass_phone &"</td><td width=100>"& station_city &"</td><td width=100>"& station_street &"</td><td width=100>"& station_house &"</td><td width=100><nobr>"& station_remark &"</nobr></td>"
        q = q & "</tr>"

        a = a & "<station>"
        a = a & " <station_index>"& c &"</station_index>"
        a = a & " <station_start_time>"& station_start_time &"</station_start_time>"
        a = a & " <station_street>"& station_street &"</station_street>"
        a = a & " <station_house>"& station_house &"</station_house>"
        a = a & " <station_city>"& station_city &"</station_city>"
        a = a & " <station_remark>"& station_remark &"</station_remark>"
        a = a & " <pass_qty>"& pass_qty &"</pass_qty>"
        a = a & " <passenger>"
        a = a & "  <pass_internal_code>"& pass_internal_code &"</pass_internal_code>"
        a = a & "  <last_name>"& last_name &"</last_name>"
        a = a & "  <first_name>"& first_name &"</first_name>"
        a = a & "  <pass_phone>"& pass_phone &"</pass_phone>"
        a = a & "  <pass_phone_2></pass_phone_2>"
        a = a & "  <pass_department_name>"& pass_department_name &"</pass_department_name>"
        a = a & " </passenger>"
        a = a & "</station>"
      loop
      q = q & "</table>"
      a = a & " </line>"
      a = a & "</root>"

      a = replace(a, "|", "") : q = replace(q, "|", "")
      if itslocal then rr q

      du = addlog("upload/yitresponse.txt", now & " - " & orderid & "-" & a & "<br>")
      a = curl(a)

      DataToSend = "XMLString=" & Server.URLEncode(a)
      lena = len(DataToSend)

      if itslocal or instr(siteurl, "test") > 0 then
        r = ""
      else
        Set o = CreateObject("Microsoft.XMLHTTP")
        u = "http://net.y-it.co.il:8080/wapdb/ws_YITNet_set_visa?$source_account=8&$source_token=ZCKAJ-NBHTT-6RTYD-UUSDF&$target_account="& target_account &"&$target_token=YYDDE-KKSJD-22TDG-WERSD&$visa_xml="& a
        o.Open "POST", u, False
        o.setRequestHeader "Content-Length", lena
        o.setRequestHeader "Content-Type", "text/xml; charset=windows-1255"
        o.setRequestHeader "SOAPAction", "http://sap.com/xi/WebService/soap1.1"
        o.Send(DataToSend)
        r = o.responseTEXT
        'rr r & "moovex: " & j & "<br>"
      end if
      '---update orderid
      yit_response_text = getfromto(r, "", "<response_txt>", "</response_txt>", false, false)
      yit_response_text = replace(yit_response_text, "'", "")
      yit_response_code = getfromto(r, "", "<response_code>", "</response_code>", false, false)
      h = setval(thehistory, "yit_response_text", yit_response_text)
      h = setval(h, "yit_response_code", yit_response_code)
      s = "update orders set history = '"& h &"' where id = "& orderid
      closers(rs) : set rs = conn.execute(sqlfilter(s))
      set o = nothing
      du = addlog("upload/yitresponse.txt", now & " - order: " & orderid & " response: ," & yit_response_text & " code: " & yit_response_code)
    end if
  loop
end function

du = sendyit("392,")
%>
