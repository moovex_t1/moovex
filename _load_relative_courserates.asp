<!--#include file="include.asp"-->
<%

'course,4
'site,����
'supplier,���
'passengers,5
'ratecode,A12
'addnight,10%
'addweekend,20%
'entrance,30
'entrancearea,33
'stop,12
'dur_s,10
'dur_p,5
'����,56
'�����,102
'---,
'��� �����,125
'course,5
'entrance,35
'stop,12
'����,56
'�������,102
'����,125
'---,

'=============GET==============================================
if getfield("action") = "get" then
  server.scripttimeout = 100000 'seconds
  response.clear
  response.ContentType = "application/csv"
  response.AddHeader "Content-Disposition", "filename=1.csv"

  tablename = "courserates"
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & courses_1.code) as courseidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join courses courses_1 on " & tablename & ".courseid = courses_1.id)"
  s = replace(s, " from ", ", cstr('' & suppliers_1.title) as supplieridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join suppliers suppliers_1 on " & tablename & ".supplierid = suppliers_1.id)"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","courserates.id,courserates.courseid,courserates.from1,courserates.to1,courserates.areaid1,courserates.areaid2,courserates.duration,courserates.supplierid,courserates.passengers,courserates.price,courserates.addnight,courserates.addweekend,courserates.returndiscount,courserates.ordertypeidsv,courserates.ordertypeids,courserates.siteid,courserates.data1",1,1,1)
  s = s & " and courseid > 0"
  's = s & " and instr(cstr(courserates.data1), 'relativeprice=on') > 0"
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  if getfield("last") = "on" then s = session("courserates_lastlistsql")
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    rr "course," & ctxt(rs("courseidlookup")) & "," & vbcrlf
    rr "site," & ctxt(rs("siteidlookup")) & "," & vbcrlf
    rr "supplier," & ctxt(rs("supplieridlookup")) & "," & vbcrlf
    rr "passengers," & ctxt(rs("passengers")) & "," & vbcrlf
    rr "duration," & ctxt(rs("duration")) & "," & vbcrlf
    rr "addnight," & ctxt(rs("addnight")) & "," & vbcrlf
    rr "addweekend," & ctxt(rs("addweekend")) & "," & vbcrlf
    d = rs("data1") & ""
    rr "ratecode," & getval(d, "ratecode") & "," & vbcrlf
    rr "entrance," & getval(d, "entrance") & "," & vbcrlf
    rr "entrancearea," & getval(d, "entrancearea") & "," & vbcrlf
    if specs = "laufer" then
      rr "stop2," & getval(d, "stop2") & "," & vbcrlf
      rr "stop3," & getval(d, "stop3") & "," & vbcrlf
      rr "stop3b," & getval(d, "stop3b") & "," & vbcrlf
      rr "stop4b," & getval(d, "stop4b") & "," & vbcrlf
    elseif specs = "metropolin" then
      rr "stop1," & getval(d, "stop1") & "," & vbcrlf
      rr "stop2," & getval(d, "stop2") & "," & vbcrlf
    else
      rr "stop," & getval(d, "stop") & "," & vbcrlf
    end if
    rr "dur_s," & getval(d, "dur_s") & "," & vbcrlf
    rr "dur_p," & getval(d, "dur_p") & "," & vbcrlf
    s = "select coursestations.stationid as id, stations.title as title from coursestations, stations where coursestations.courseid = " & rs("courseid") & " and coursestations.stationid = stations.id order by coursestations.minutes"
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      i = rs1("id") : t = rs1("title") : t = """" & replace(ctxt(t), """", """""") & """"
      rs1.movenext : if rs1.eof then exit do
      rr ctxt(t) & ","
      rr getval(d, "r" & i) & ","
      rr getval(d, "dur_" & i) & ","
      rr getval(d, "ratecode" & i) & ","
      rr vbcrlf
    loop
    rr "---," & vbcrlf
    rs.movenext
  loop
  rend

end if

'=============LOAD=================================================
if getfield("action") = "put" then
  response.clear : response.buffer = false : server.scripttimeout = 100000 'seconds
  c = 0 : f = getfield("file") : if f = "" then f = "1.csv"
  line = 0 : courseid = 0 : data1 = ""

  set file = fs.opentextfile(server.mappath(f))
  do until file.atendofstream
    line = line + 1
    a = file.readline & ",,,,,,,"
    f = trim(lcase(left(a,instr(a,",")-1))) : a = mid(a,instr(a,",")+1)
    v = trim(lcase(left(a,instr(a,",")-1))) : a = mid(a,instr(a,",")+1)
    v2 = trim(lcase(left(a,instr(a,",")-1))) : a = mid(a,instr(a,",")+1)
    v3 = trim(lcase(left(a,instr(a,",")-1))) : a = mid(a,instr(a,",")+1)
    v4 = trim(lcase(left(a,instr(a,",")-1))) : a = mid(a,instr(a,",")+1)
    if instr(f,"---") > 0 then
      s = "delete * from courserates where courseid = " & courseid & " and supplierid = " & supplierid & " and passengers = " & passengers
      set rs = conn.execute(sqlfilter(s))
      if data1 <> "" then data1 = setval(data1, "relativeprice", "on")
      f = "" : v = ""
      f = f & "courseid," : v = v & courseid & ","
      f = f & "from1," : v = v & "0,"
      f = f & "to1," : v = v & "0,"
      f = f & "areaid1," : v = v & "0,"
      f = f & "areaid2," : v = v & "0,"
      f = f & "activeproduction," : v = v & "'on',"
      f = f & "active," : v = v & "'on',"
      f = f & "special," : v = v & "'on',"
      f = f & "duration," : v = v & "'" & duration & "',"
      f = f & "supplierid," : v = v & supplierid & ","
      f = f & "passengers," : v = v & passengers & ","
      f = f & "price," : v = v & max & ","
      f = f & "addnight," : v = v & "'" & addnight & "',"
      f = f & "addweekend," : v = v & "'" & addweekend & "',"
      f = f & "returndiscount," : v = v & "'',"
      f = f & "ordertypeidsv," : v = v & "'',"
      f = f & "ordertypeids," : v = v & "'',"
      f = f & "siteid," : v = v & siteid & ","
      f = f & "data1," : v = v & "'" & data1 & "',"
      f = left(f, len(f) - 1): v = left(v, len(v) - 1)
      s = "insert into courserates(" & f & ") values(" & v & ")"
      'rr s & "<hr>"
      set rs = conn.execute(sqlfilter(s))
      c = c + 1
    elseif f = "course" then
      courseid = 0
      max = 0
      data1 = ""
      passengers = 0
      supplierid = 0
      duration = ""
      siteid = 0
      addnight = ""
      addweekend = ""
      s = "select id from courses where code = '" & v & "' or cstr(code & ' ' & title) = '" & v & "'"
      set rs = conn.execute(sqlfilter(s))
      if rs.eof then rr "Line " & line & " - Course not found: " & v & "<br>" else courseid = rs("id")
    elseif instr(",entrance,entrancearea,stop,stop1,stop2,stop3,stop3b,stop4b,dur_s,dur_p,ratecode,", "," & f & ",") > 0 then
      data1 = setval(data1, f, v)
    elseif f = "addnight" then
      addnight = v
    elseif f = "addweekend" then
      addweekend = v
    elseif f = "passengers" then
      passengers = v
    elseif f = "duration" then
      duration = v
    elseif f = "site" then
      s = "select id from sites where title = '" & chtm(v) & "'"
      set rs = conn.execute(sqlfilter(s))
      if rs.eof then rr "Line " & line & " - Site not found: " & v & "<br>" else siteid = rs("id")
    elseif f = "supplier" then
      s = "select id from suppliers where title = '" & chtm(v) & "'"
      set rs = conn.execute(sqlfilter(s))
      if rs.eof then rr "Line " & line & " - Supplier not found: " & v & "<br>" else supplierid = rs("id")
    else
      f = replace(f, "``", chtm(""""))
      'f = replace(f, "`", "'")
      s = "select id from stations where siteid in(" & siteid & ",0) and (title = '" & f & "' or title = '" & chtm(ctxt(f)) & "')"
      set rs = conn.execute(sqlfilter(s))
      if rs.eof then
        rr "Line " & line & " - Station not found: " & f & "<br>"
      else
        v = cdbl("0" & strfilter(v, "0123456789.")) : if v > max then max = v
        data1 = setval(data1, "r" & rs("id"), v)
        data1 = setval(data1, "dur_" & rs("id"), v2)
        data1 = setval(data1, "ratecode" & rs("id"), v3)
      end if  
    end if
  loop
  file.close
  rr "OK " & c
end if
%>
