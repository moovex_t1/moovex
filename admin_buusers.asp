<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 12/04/2013 10:17:47

function buusersdelitem(i)
  buusersdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function buusersinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("username") <> "" then t = getfield("username")
  f = f & "username," : v = v & "'" & t & "',"

  t = "1" : if getfield("userlevel") <> "" then t = getfield("userlevel")
  f = f & "userlevel," : v = v & "'" & t & "',"

  t = "" : if getfield("usergroup") <> "" then t = getfield("usergroup")
  f = f & "usergroup," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  t = session("usersiteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
']
  f = f & "siteid," : v = v & t & ","

'[  t = "" : if getfield("perm") <> "" then t = getfield("perm")
  t = "prices,editusers," : if getfield("perm") <> "" then t = getfield("perm")
']
  f = f & "perm," : v = v & "'" & t & "',"

  t = "" : if getfield("supplierids") <> "" then t = getfield("supplierids")
  f = f & "supplierids," : v = v & "'" & t & "',"

  t = "" : if getfield("unitids") <> "" then t = getfield("unitids")
  f = f & "unitids," : v = v & "'" & t & "',"

  t = "" : if getfield("subusers") <> "" then t = getfield("subusers")
  f = f & "subusers," : v = v & "'" & t & "',"

  t = "" : if getfield("ordertypeids") <> "" then t = getfield("ordertypeids")
  f = f & "ordertypeids," : v = v & "'" & t & "',"

  t = "" : if getfield("firstname") <> "" then t = getfield("firstname")
  f = f & "firstname," : v = v & "'" & t & "',"

  t = "" : if getfield("lastname") <> "" then t = getfield("lastname")
  f = f & "lastname," : v = v & "'" & t & "',"

  t = "" : if getfield("email") <> "" then t = getfield("email")
  f = f & "email," : v = v & "'" & t & "',"

  t = "" : if getfield("phone") <> "" then t = getfield("phone")
  f = f & "phone," : v = v & "'" & t & "',"

'[  t = "" : if getfield("pass") <> "" then t = getfield("pass")
  t = createpassword(8) : if getfield("pass") <> "" then t = getfield("pass")
  t = encrypt(t,"1")
']
  f = f & "pass," : v = v & "'" & t & "',"

  t = "" : if getfield("picture") <> "" then t = getfield("picture")
  f = f & "picture," : v = v & "'" & t & "',"

  t = "" : if getfield("allowedips") <> "" then t = getfield("allowedips")
  f = f & "allowedips," : v = v & "'" & t & "',"

'[  t = date : if getfield("expires") <> "" then t = getfield("expires")
  t = dateadd("yyyy",10,date) : if getfield("expires") <> "" then t = getfield("expires")
']
  f = f & "expires," : v = v & sqldate(t) & ","

  t = "" : if getfield("remarks") <> "" then t = getfield("remarks")
  f = f & "remarks," : v = v & "'" & t & "',"

  f = f & "updateduserid," : v = v & session("userid") & ","

  f = f & "updateddate," : v = v & sqldate(now) & ","

  t = "" : if getfield("sessionvalues") <> "" then t = getfield("sessionvalues")
  f = f & "sessionvalues," : v = v & "'" & t & "',"

  t = "" : if getfield("starred") <> "" then t = getfield("starred")
  f = f & "starred," : v = v & "'" & t & "',"

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into buusers(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from buusers"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  buusersinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table buusers (id autoincrement primary key,username text(30),userlevel text(2),usergroup text(50),siteid number,perm memo,supplierids memo,unitids memo,subusers memo,ordertypeids memo,firstname text(30),lastname text(30),email text(60),phone text(50),pass text(30),picture text(100),allowedips memo,expires datetime,remarks memo,updateduserid number,updateddate datetime,sessionvalues memo,starred memo,data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "buusers" : session("tablename") = "buusers"
tablefields = "id,username,userlevel,usergroup,siteid,perm,supplierids,unitids,subusers,ordertypeids,firstname,lastname,email,phone,pass,picture,allowedips,expires,remarks,updateduserid,updateddate,sessionvalues,starred,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
if session("userlevel") < "5" then locked = session("userid") & ","
if session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"
if specs = "shebain" then hidecontrols = hidecontrols & "unitids,"
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = buusersinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  'if not isidn(getfield("idn")) then rr(" <center><font color=ff0000><b> ~Invalid ID Number~ </b></font></center>")
  'if not isemail(getfield("email")) then rr(" <center><font color=ff0000><b> ~Invalid Email~ </b></font></center>")

  msg = msg & " ~Item Updated~"
  sq = "select * from buusers where username = '" & getfield("username") & "' and id<>" & getfield("id")
  closers(rs) : set rs = conn.execute(sqlfilter(sq))
  if not rs.eof then
    errorend("_back~User Name in use~")
  elseif session("userlevel") < 5 and cint(getfield("userlevel")) > cint(session("userlevel")) then
    errorend("~Cannot created a stronger user than self~")
  end if

']
  s = "update " & tablename & " set "
  if okedit("username") then s = s & "username='" & getfield("username") & "',"
  if okedit("userlevel") then s = s & "userlevel='" & getfield("userlevel") & "',"
  if okedit("usergroup") then s = s & "usergroup='" & getfield("usergroup") & "',"
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  if okedit("perm") then s = s & "perm='" & getfield("perm") & "',"
  if okedit("supplierids") then s = s & "supplierids='" & getfield("supplierids") & "',"
  if okedit("unitids") then s = s & "unitids='" & getfield("unitids") & "',"
  if okedit("subusers") then s = s & "subusers='" & getfield("subusers") & "',"
  if okedit("ordertypeids") then s = s & "ordertypeids='" & getfield("ordertypeids") & "',"
  if okedit("firstname") then s = s & "firstname='" & getfield("firstname") & "',"
  if okedit("lastname") then s = s & "lastname='" & getfield("lastname") & "',"
  if okedit("email") then s = s & "email='" & getfield("email") & "',"
  if okedit("phone") then s = s & "phone='" & getfield("phone") & "',"
'[  if okedit("pass") then s = s & "pass='" & getfield("pass") & "',"
  if hasmatch(specs, "jerusalem,muni,brener,") and okedit("pass") then s = s & "pass='" & encrypt(getfield("pass"),"1") & "',"
  'if instr(siteurl,"localhost") = 0 and passwordstrength(getfield("pass")) <> "" then errorend(passwordstrength(getfield("pass")))
  'if okedit("pass") then s = s & "pass='" & encrypt(getfield("pass"),"1") & "',"
']
  if okedit("picture") then s = s & "picture='" & getfield("picture") & "',"
'[  if okedit("allowedips") then s = s & "allowedips='" & getfield("allowedips") & "',"
  if okedit("allowedips") then s = s & "allowedips='" & replace(getfield("allowedips")," ","") & "',"
']
  if okedit("expires") then s = s & "expires=" & sqldate(getfield("expires")) & ","
  if okedit("remarks") then s = s & "remarks='" & getfield("remarks") & "',"
  t = session("userid") : if ulev > 4 then t = getfield("updateduserid")
  if okedit("updateduserid") then s = s & "updateduserid=0" & t & ","
  t = now : if ulev > 4 then t = replace(getfield("updateddate"),".","/")
  if okedit("updateddate") then s = s & "updateddate=" & sqldate(t) & ","
  if okedit("sessionvalues") then s = s & "sessionvalues='" & getfield("sessionvalues") & "',"
'[  if okedit("starred") then s = s & "starred='" & getfield("starred") & "',"
']
  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(username,userlevel,usergroup,siteid,perm,supplierids,unitids,subusers,ordertypeids,firstname,lastname,email,phone,pass,picture,allowedips,expires,remarks,updateduserid,updateddate,sessionvalues,starred,data1)"
  s = s & " SELECT username,userlevel,usergroup,siteid,perm,supplierids,unitids,subusers,ordertypeids,firstname,lastname,email,phone,pass,picture,allowedips,expires,remarks," & session("userid") & "," & sqldate(now) & ",sessionvalues,starred,data1"
  s = s & " FROM " & tablename & " where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  request_action = "form"
  request_id = theid
'[  msg = msg & " ~Record duplicated~"
  msg = msg & " ~Record duplicated~"
  u = getfield("username") : c = 0
  do
    if isnumeric(right(u,1)) then u = left(u,len(u)-1) else exit do
  loop
  do
    c = c + 1
    s = "select * from buusers where username = '" & u & c & "'"
    closers(rs) : set rs = conn.execute(sqlfilter(s))
    if rs.eof then exit do
  loop
  s = "update buusers set username = '" & u & c & "', pass = '" &   encrypt(createpassword(6),"1") & "' where id = " & request_id
  closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
']
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = buusersdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = buusersdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  csv(0) = csv(0) & "~buusers_username~^"
  csv(0) = csv(0) & "~buusers_userlevel~^"
  csv(0) = csv(0) & "~buusers_usergroup~^"
  csv(0) = csv(0) & "~buusers_siteid~^"
  csv(0) = csv(0) & "~buusers_perm~^"
  csv(0) = csv(0) & "~buusers_supplierids~^"
  csv(0) = csv(0) & "~buusers_unitids~^"
  csv(0) = csv(0) & "~buusers_subusers~^"
  csv(0) = csv(0) & "~buusers_ordertypeids~^"
  csv(0) = csv(0) & "~buusers_firstname~^"
  csv(0) = csv(0) & "~buusers_lastname~^"
  csv(0) = csv(0) & "~buusers_email~^"
  csv(0) = csv(0) & "~buusers_phone~^"
  csv(0) = csv(0) & "~buusers_pass~^"
  csv(0) = csv(0) & "~buusers_picture~^"
  csv(0) = csv(0) & "~buusers_allowedips~^"
  csv(0) = csv(0) & "~buusers_expires~^"
  csv(0) = csv(0) & "~buusers_remarks~^"
  csv(0) = csv(0) & "~buusers_updateduserid~^"
  csv(0) = csv(0) & "~buusers_updateddate~^"
  csv(0) = csv(0) & "~buusers_sessionvalues~^"
  csv(0) = csv(0) & "~buusers_starred~^"
  csv(0) = csv(0) & "~buusers_data1~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Theusername = rs("username") : if isnull(Theusername) then Theusername = ""
  Theuserlevel = rs("userlevel") : if isnull(Theuserlevel) then Theuserlevel = ""
  Theusergroup = rs("usergroup") : if isnull(Theusergroup) then Theusergroup = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Theperm = rs("perm") : if isnull(Theperm) then Theperm = ""
  Thesupplierids = rs("supplierids") : Thesupplierids = formatidlist(Thesupplierids)
  Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
  Thesubusers = rs("subusers") : Thesubusers = formatidlist(Thesubusers)
  Theordertypeids = rs("ordertypeids") : Theordertypeids = formatidlist(Theordertypeids)
  Thefirstname = rs("firstname") : if isnull(Thefirstname) then Thefirstname = ""
  Thelastname = rs("lastname") : if isnull(Thelastname) then Thelastname = ""
  Theemail = rs("email") : if isnull(Theemail) then Theemail = ""
  Thephone = rs("phone") : if isnull(Thephone) then Thephone = ""
  Thepass = rs("pass") : if isnull(Thepass) then Thepass = ""
  Thepicture = rs("picture") : if isnull(Thepicture) then Thepicture = ""
  Theallowedips = rs("allowedips") : if isnull(Theallowedips) then Theallowedips = ""
  Theexpires = rs("expires") : if isnull(Theexpires) then Theexpires = "1/1/1900"
  Theexpires = replace(theexpires,".","/")
  Theremarks = rs("remarks") : if isnull(Theremarks) then Theremarks = ""
  Theupdateduserid = rs("updateduserid") : if isnull(Theupdateduserid) then Theupdateduserid = 0
  Theupdateduserid = replace(theupdateduserid,",",".")
  Theupdateddate = rs("updateddate") : if isnull(Theupdateddate) then Theupdateddate = ""
  Thesessionvalues = rs("sessionvalues") : if isnull(Thesessionvalues) then Thesessionvalues = ""
  Thestarred = rs("starred") : if isnull(Thestarred) then Thestarred = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""
  Theupdateduseridlookup = rs("updateduseridlookup") : if isnull(Theupdateduseridlookup) then Theupdateduseridlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    csv(rc) = csv(rc) & replace(theusername,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(theuserlevel,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(theusergroup,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & thesiteidlookup & "^"
    csv(rc) = csv(rc) & replace(theperm,vbcrlf," ") & "^"
      t = "" : d = formatidlist(thesupplierids)
      sq = "select * from suppliers where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    csv(rc) = csv(rc) & t & "^"
      t = "" : d = formatidlist(theunitids)
      sq = "select * from units where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    csv(rc) = csv(rc) & t & "^"
      t = "" : d = formatidlist(thesubusers)
      sq = "select * from buusers where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("username")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    csv(rc) = csv(rc) & t & "^"
      t = "" : d = formatidlist(theordertypeids)
      sq = "select * from ordertypes where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    csv(rc) = csv(rc) & t & "^"
    csv(rc) = csv(rc) & replace(thefirstname,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(thelastname,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(theemail,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(thephone,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(thepass,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(thepicture,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(theallowedips,vbcrlf," ") & "^"
    t = "" : if isdate(theexpires) then if year(theexpires) <> 1900 then t = theexpires
    csv(rc) = csv(rc) & t & "^"
    csv(rc) = csv(rc) & replace(theremarks,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & theupdateduseridlookup & "^"
    t = "" : if isdate(theupdateddate) then if year(theupdateddate) <> 1900 then t = theupdateddate
    csv(rc) = csv(rc) & t & "^"
    csv(rc) = csv(rc) & replace(thesessionvalues,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(thestarred,vbcrlf," ") & "^"
    csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      siteid = session("usersiteid")
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "username" then
'[          if len(b) > 30 then b = left(b,30)
'[          if okedit("username") then s = s & "username='" & b & "',"
          b = replace(b," ",".")
          if len(b) > 30 then b = left(b,30)
          if okedit("username") then s = s & "username='" & b & "',"

          sq = "select id from buusers where username = '" & b & "'"
          set rs1 = conn.execute(sqlfilter(sq))
          if not rs1.eof then thisid = rs1("id")
']
        elseif csvff(i) = "userlevel" then
          if len(b) > 2 then b = left(b,2)
          if okedit("userlevel") then s = s & "userlevel='" & b & "',"
        elseif csvff(i) = "usergroup" then
          if len(b) > 50 then b = left(b,50)
          if okedit("usergroup") then s = s & "usergroup='" & b & "',"
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("siteid") then s = s & "siteid=" & v & ","
          if okedit("siteid") then s = s & "siteid=" & v & ","
          if session("usersiteid") = "0" then siteid = v
']
        elseif csvff(i) = "perm" then
          if okedit("perm") then s = s & "perm='" & b & "',"
        elseif csvff(i) = "supplierids" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from suppliers where lcase(cstr('' & title)) = '" & lcase(t) & "'"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("supplierids") then s = s & "supplierids='" & v & "',"
'[        elseif csvff(i) = "unitids" then
'[          v = "" : b = b & ","
'[          do until b = ""
'[            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
'[            if t <> "" then
'[              sq = "select * from units where lcase(cstr('' & title)) = '" & lcase(t) & "'"
'[              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
'[              if not rs1.eof then v = v & rs1("id") & ","
'[            end if
'[          loop
'[          if okedit("unitids") then s = s & "unitids='" & v & "',"
        elseif csvff(i) = "unitids" then
          v = "" : b = trim(b) & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from units where lcase(cstr('' & title)) = '" & lcase(t) & "' and siteid = " & siteid
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if rs1.eof then
                sq = "insert into units(title,siteid) values('" & t & "'," & siteid & ")"
                closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
                sq = "select max(id) as id from units"
                closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              end if
              v = v & rs1("id") & ","
            end if
          loop
          if okedit("unitids") then s = s & "unitids='" & v & "',"
']
        elseif csvff(i) = "subusers" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from buusers where lcase(cstr('' & username)) = '" & lcase(t) & "'"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("subusers") then s = s & "subusers='" & v & "',"
        elseif csvff(i) = "ordertypeids" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from ordertypes where lcase(cstr('' & title)) = '" & lcase(t) & "'"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("ordertypeids") then s = s & "ordertypeids='" & v & "',"
        elseif csvff(i) = "firstname" then
          if len(b) > 30 then b = left(b,30)
          if okedit("firstname") then s = s & "firstname='" & b & "',"
        elseif csvff(i) = "lastname" then
          if len(b) > 30 then b = left(b,30)
          if okedit("lastname") then s = s & "lastname='" & b & "',"
        elseif csvff(i) = "email" then
          if len(b) > 60 then b = left(b,60)
          if okedit("email") then s = s & "email='" & b & "',"
        elseif csvff(i) = "phone" then
          if len(b) > 50 then b = left(b,50)
          if okedit("phone") then s = s & "phone='" & b & "',"
        elseif csvff(i) = "pass" then
'[          if len(b) > 30 then b = left(b,30)
'[          if okedit("pass") then s = s & "pass='" & b & "',"
          if len(b) > 30 then b = left(b,30)
          b = encrypt(b,"1")
          if okedit("pass") then s = s & "pass='" & b & "',"
']
        elseif csvff(i) = "picture" then
          if len(b) > 100 then b = left(b,100)
          if okedit("picture") then s = s & "picture='" & b & "',"
        elseif csvff(i) = "allowedips" then
          if okedit("allowedips") then s = s & "allowedips='" & b & "',"
        elseif csvff(i) = "expires" then
          if not isdate(b) then b = "1/1/1900"
          if okedit("expires") then s = s & "expires=" & sqldate(b) & ","
        elseif csvff(i) = "remarks" then
          if okedit("remarks") then s = s & "remarks='" & b & "',"
        elseif csvff(i) = "updateduserid" then
        elseif csvff(i) = "updateddate" then
        elseif csvff(i) = "sessionvalues" then
          if okedit("sessionvalues") then s = s & "sessionvalues='" & b & "',"
        elseif csvff(i) = "starred" then
          if okedit("starred") then s = s & "starred='" & b & "',"
        elseif csvff(i) = "data1" then
          if okedit("data1") then s = s & "data1='" & b & "',"
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if okedit("updateduserid") then s = s & "updateduserid=" & session("userid") & ","
      if okedit("updateddate") then s = s & "updateddate=" & sqldate(now) & ","
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = buusersinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
'[  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
  if getfield("action3") = "close" then rend
']
end if
'['-------------more actions-------------------------------------
'-------------multisend-------------------------------------
if getfield("action") = "multisend" then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  thesubject = getfield("subject")
  thebody = getfield("body") : if instr(thebody,"<") + instr(thebody,">") = 0 then thebody = replace(thebody,vbcrlf,"<br>")
  f = getfield("file1") : if f <> "" then thebody = thebody & "<br><br>Attatched File: <a href='" & siteurl & f & "'>" & siteurl & f & "</a>"
  rr("<center><div style='border:1px solid #000000; background:eeeeee; overflow:auto; width:500; height:50;'>")

  if not isemail(getuseremail(session("userid"))) then du = showerror("~To send emails, you must have a valid email defined in your user page~")

  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    sq = "select * from buusers where id = " & b
    closers(rs) : set rs = conn.execute(sqlfilter(sq))
    'ff = "username,userlevel,allowedips,firstname,lastname,expires,email,phone,pass,remarks,picture,permissions,usergroup,updateduserid,updateddate,parentid,"
    ff = "username,firstname,lastname,email,phone,pass,usergroup,"
    thissubject = thesubject
    thisbody = thebody
    on error resume next
    do until ff = ""
      f = left(ff,instr(ff,",")-1) : ff = mid(ff,instr(ff,",")+1)
      v = rs(f) : if isnull(v) then v = ""
      if f = "pass" then v = decrypt(v, "1")
      thissubject = replace(thissubject,"*" & f & "*",v)
      thisbody = replace(thisbody,"*" & f & "*",v)
    loop
    on error goto 0
    if rs("email") <> "" then
      x = sendemail(getuseremail(session("userid")), rs("email"),"","", thissubject, thisbody)
      rr(rs("email") & " OK<br>")
    end if
    if getfield("sms") = "on" then
      du = sendsms(getuserphone(session("userid")), rs("phone") & "", thissubject & " " & thisbody)
    end if
    c = c + 1
  loop
  rr("</div></center>")
  msg = c & " ~messages~ ~Were Sent~"
end if
']
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","buusers.id,buusers.username,buusers.userlevel,buusers.usergroup,buusers.siteid,buusers.perm,buusers.supplierids,buusers.unitids,buusers.subusers,buusers.ordertypeids,buusers.firstname,buusers.lastname,buusers.email,buusers.phone,buusers.pass,buusers.picture,buusers.allowedips,buusers.expires,buusers.remarks,buusers.updateduserid,buusers.updateddate,buusers.sessionvalues,buusers.starred,buusers.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Theusername = rs("username") : if isnull(Theusername) then Theusername = ""
  Theuserlevel = rs("userlevel") : if isnull(Theuserlevel) then Theuserlevel = ""
  Theusergroup = rs("usergroup") : if isnull(Theusergroup) then Theusergroup = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Theperm = rs("perm") : if isnull(Theperm) then Theperm = ""
  Thesupplierids = rs("supplierids") : Thesupplierids = formatidlist(Thesupplierids)
  Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
  Thesubusers = rs("subusers") : Thesubusers = formatidlist(Thesubusers)
  Theordertypeids = rs("ordertypeids") : Theordertypeids = formatidlist(Theordertypeids)
  Thefirstname = rs("firstname") : if isnull(Thefirstname) then Thefirstname = ""
  Thelastname = rs("lastname") : if isnull(Thelastname) then Thelastname = ""
  Theemail = rs("email") : if isnull(Theemail) then Theemail = ""
  Thephone = rs("phone") : if isnull(Thephone) then Thephone = ""
  Thepass = rs("pass") : if isnull(Thepass) then Thepass = ""
  Thepicture = rs("picture") : if isnull(Thepicture) then Thepicture = ""
  Theallowedips = rs("allowedips") : if isnull(Theallowedips) then Theallowedips = ""
  Theexpires = rs("expires") : if isnull(Theexpires) then Theexpires = "1/1/1900"
  Theexpires = replace(theexpires,".","/")
  Theremarks = rs("remarks") : if isnull(Theremarks) then Theremarks = ""
  Theremarks = replace(theremarks,"<br>",vbcrlf)
  Theupdateduserid = rs("updateduserid") : if isnull(Theupdateduserid) then Theupdateduserid = 0
  Theupdateduserid = replace(theupdateduserid,",",".")
  Theupdateddate = rs("updateddate") : if isnull(Theupdateddate) then Theupdateddate = ""
  Thesessionvalues = rs("sessionvalues") : if isnull(Thesessionvalues) then Thesessionvalues = ""
  Thestarred = rs("starred") : if isnull(Thestarred) then Thestarred = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "username,userlevel,usergroup,siteid,perm,supplierids,supplierids_select,supplierids_trigger,unitids,unitids_select,unitids_trigger,subusers,subusers_select,subusers_trigger,ordertypeids,ordertypeids_select,ordertypeids_trigger,firstname,lastname,email,phone,pass,picture,allowedips,expires,expires_day,expires_month,expires_year,expires_hour,expires_insertdate,expires_removedate,remarks,updateduserid,updateddate,updateddate_day,updateddate_month,updateddate_year,updateddate_hour,sessionvalues,starred,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><!img src='icons/hire-me.png' ~iconsize~> ~buusers_buusers~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("username") then'form:username
    rr("<tr valign=top id=username_tr0><td class=form_item1 id=username_tr1><span id=username_caption>~buusers_username~</span><td class=form_item2 id=username_tr2>")
'[    rr("<input type=text maxlength=30 name=username onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theusername & """ dir=~langdir~>")
    rr("<input type=text maxlength=30 name=username autocomplete=off onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theusername & """ dir=~langdir~>")
    'if session("userlevel") = "5" and theusername <> "" and request_id <> session("userid") then
    '  rr("<input type=button value=""Login as " & chtm(theusername) & """ onclick='vbscript: if msgbox(""~Login~?"",vbyesno) = 6 then document.location = ""admin.asp?action=login&username=" & curl(theusername) & "&pass=" & curl(thepass) & """'>")
    'end if
']
  end if'form:username

  if oksee("userlevel") then'form:userlevel
    rr("<tr valign=top id=userlevel_tr0><td class=form_item1 id=userlevel_tr1><span id=userlevel_caption>~buusers_userlevel~</span><td class=form_item2 id=userlevel_tr2>")
'[    rr("<input type=text maxlength=2 name=userlevel onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theuserlevel & """ dir=~langdir~>")
    rr("<select name=userlevel>")
    if Theuserlevel = "0" then t = "selected" else t = ""
    rr("<option value=0 " & t & ">~No Access~")
    if Theuserlevel = "1" then t = "selected" else t = ""
    rr("<option value=1 " & t & ">~Active~")
    if Theuserlevel = "5" then t = "selected" else t = ""
    'if instr(siteurl, "/localhost1/") > 0 or instr(siteurl, "/127.0.0.1/") > 0 or t = "selected" then rr("<option value=5 " & t & ">~Developer~")
    rr("<option value=5 " & t & ">~Developer~")
    rr("</select>")
']
  end if'form:userlevel

  if oksee("usergroup") then'form:usergroup
    rr(" <span id=usergroup_caption class=form_item3>~buusers_usergroup~</span> ")
'[    a = ""
'[    rr("<select name=usergroup dir=~langdir~>")
    a = getparam("usergroups") : if hasmatch(specs, "jerusalem,") then a = "Admin,Support,Rakaz,Moked,"
    rr("<select name=usergroup dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : checkform'>")
']
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Theusergroup = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:usergroup
  end if'form:usergroup

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr(" <span id=siteid_caption class=form_item3>~buusers_siteid~</span> ")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~buusers_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

'[  if oksee("perm") then'form:perm
'[    rr("<tr valign=top id=perm_tr0><td class=form_item1 id=perm_tr1><span id=perm_caption>~buusers_perm~</span><td class=form_item2 id=perm_tr2>")
'[    c = 0 : s = "" : a = "_perm_,"
'[    cc = countstring(a,",")
'[    do until a = ""
'[      c = c + 1
'[      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
'[      rr("<input type=checkbox name=perm_c" & c)
'[      if instr("," & theperm & ",","," & b & ",") > 0 then rr(" checked")
'[      rr(" onclick='vbscript:populateperm'>")
'[      rr(b & " ")
'[      s = s & "if f1.perm_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
'[    loop

  if hasmatch(specs, "imi,") then
    rr("<tr valign=top><td class=form_item1>~Station~<td class=form_item2>")
    rr selectlookupauto("f1.data1stationid",getval(thedata1, "stationid"), "stations,title,", 300)
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""stationid"", f1.data1stationid.value)" & vbcrlf
  end if
  if hasmatch(specs, "shebain,") then
    rr("<tr valign=top><td class=form_item1>~stations_stations~<td class=form_item2>")
    rr selectlookupmultiauto("f1.data1stationids",getval(thedata1, "stationids") , "icons/home.png", "stations,title,", 300, okwritehere)
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""stationids"", f1.data1stationids.value)" & vbcrlf
  end if

  if oksee("perm") then
    hideloadcsv = "" : if session("userlevel") <> "5" or instr(theusergroup,"Admin") = 0 then hideloadcsv = "on"
    rr("<tr valign=top id=perm_tr0><td class=form_item1 id=perm_tr1><span id=perm_caption>~buusers_perm~</span><td class=form_item2 id=perm_tr2>")
    c = 0 : s = "" : a = ""
    a = a & "prices.,loadcsv.,"
    if instr(",manager,teamleader,adminassistant,admin,", "," & lcase(theusergroup) & ",") > 0 then a = a & "setshifts.,dupshifts.,approverejectorder.,"
    if session("userlevel") = "5" and instr(",admin,", "," & lcase(theusergroup) & ",") > 0 then a = a & "editsettings.,"
    if instr(",supplier,supplieradmin,admin,adminassistant,manager,approver,", "," & lcase(theusergroup) & ",") > 0 then a = a & "seeboard.,"
    if instr(",supplier,supplieradmin,", "," & lcase(theusergroup) & ",") = 0 then a = a & "addglobalworkers.,"
    if specs = "egged" and instr(",adminassistant,", "," & lcase(theusergroup) & ",") > 0 then a = a & "specialorder.,"
    if instr(",manager,", "," & lcase(theusergroup) & ",") > 0 then a = a & "editworkers.,addworkers.,produceorders.,sendtosupplier.,cancelorder.,approvedoneorder.,createneworders.,editsentorders.,"
    if instr(",adminassistant,", "," & lcase(theusergroup) & ",") > 0 then a = a & "produceorders.,units,+units.,workers,+workers.,weekplan,+weekplan.,suppliers,+suppliers.,shifts,+shifts.,stations,+stations.,areas,+areas.,courses,+courses.,courserates,+courserates.,additions,+additions.,drivers,+drivers.,cars,+cars.,foodsuppliers,+foodsuppliers.,holidays,+holidays.,complains,+complains.,"
    if specs = "rafael" then a = a & "allnatbag.,"
    if instr(",admin,", "," & lcase(theusergroup) & ",") > 0 then a = a & "setshiftsretro.,editusers.,"

    if specs = "iai" and instr(",manager,approver,admin,adminassistant,", "," & lcase(theusergroup) & ",") > 0 then a = a & "roshminhal.,approver2.,approver3.,"
    if hasmatch(specs, "jerusalem,muni,brener,") and instr(siteurl, "/gedera.moovex.net/") = 0 then a = nomatch(a, "prices.,addglobalworkers.,approverejectorder.,")
    if hasmatch(specs, "jerusalem,muni,brener,") then hidecontrols = hidecontrols & "productionshiftids,subusers,ordertypeids,"
    rr "<div style='background:#ffffff; border1:1px solid #aaaaaa; width:300; height1:125; overflow1:auto;'>"
    cc = countstring(a,",")
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
      breakafter = "" : if instr(b, ".") then b = replace(b, ".", "") : breakafter = "on"
      rr "<span" : if b = "loadcsv" and hideloadcsv = "on" then rr " style='position:absolute; visibility:hidden;'>" else rr ">"
      rr("<input type=checkbox name=perm_c" & c)
      if instr("," & theperm & ",","," & b & ",") > 0 then rr(" checked")
      rr(" onclick='vbscript:populateperm'>")
      t = "~" & b & "_" & b & "~"
      if b = "allnatbag" then t = "�� ����� ����"
      if left(b,1) = "+" then rr " ~Edit~ " else rr("<span style='width:100; overflow:hidden;'>" & t & "</span>")
      if breakafter = "on" then rr "<br>"
      rr "</span>"
      s = s & "if f1.perm_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
    loop
    rr "</div>"

    if hasmatch(specs, "rafael,iec,shufersal,jerusalem,") and theusergroup = "Admin" then
      rr("<tr valign=top><td class=form_item1>����� ����� ������<td class=form_item2>")
      sq = "select id,title from sites order by title"
      rr(selectlookupmultiauto("f1.siteids",getval(thedata1, "siteids") , "icons/i_earth.gif", "sites,title,:" & sq, 300, okwritehere))
      checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""siteids"", f1.siteid.value & "","" & f1.siteids.value)" & vbcrlf
    end if

    'if instr(",manager,", "," & lcase(theusergroup) & ",") > 0 then 
    if oksee("productionshiftids") then
      rr("<tr valign=top><td class=form_item1>~Produce for shifts~<td class=form_item2>")
      sq = "select id,title from shifts"
      if thesiteid <> "0" then sq = sq & " where siteid = " & thesiteid
      sq = sq & " order by title"
      rr(selectlookupmultiauto("f1.data1productionshiftids",getval(thedata1, "productionshiftids") , "icons/refresh.png", "shifts,title,:" & sq, 300, okwritehere))
      checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""productionshiftids"", f1.data1productionshiftids.value)" & vbcrlf
    end if

']
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub populateperm" & vbcrlf)
    rr("    t = """"" & vbcrlf)
    rr(s & vbcrlf)
    rr("    f1.perm.value = t" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("</script>" & vbcrlf)


    rr("<input type=hidden name=perm value=""" & Theperm & """>")

  end if'form:perm

  if oksee("supplierids") then'form:supplierids
    rr("<tr valign=top id=supplierids_tr0><td class=form_item1 id=supplierids_tr1><span id=supplierids_caption>~buusers_supplierids~</span><td class=form_item2 id=supplierids_tr2>")
'[    rr(selectlookupmultiauto("f1.supplierids",thesupplierids, "icons/i_key.gif", "suppliers,title,", 300, okwritehere))
    t = "select * from suppliers"
    if thesiteid <> "0" then t = t & " where id in(" & thesupplierids & "-1) or (siteid = " & thesiteid & " or siteid = 0)"
    t = t & " order by title"
    rr(selectlookupmultiauto("f1.supplierids",thesupplierids, "icons/i_key.gif", "suppliers,title,:" & t, 300, okwritehere))
']
  end if'form:supplierids

  if oksee("unitids") then'form:unitids
    rr("<tr valign=top id=unitids_tr0><td class=form_item1 id=unitids_tr1><span id=unitids_caption>~buusers_unitids~</span><td class=form_item2 id=unitids_tr2>")
'[    rr(selectlookupmultiauto("f1.unitids",theunitids, "icons/i_shield.gif", "units,title,", 300, okwritehere))
'[  end if'form:unitids
    t = "select id,title,siteid from units"
    if thesiteid <> "0" and crosssite = "" then t = t & " where id in(" & theunitids & "-1) or (siteid = " & thesiteid & " or siteid = 0) order by title"
    rr(selectlookupmultiauto("f1.unitids",theunitids, "icons/i_shield.gif", "units,title,siteid,:" & t, 300, okwritehere))
  end if'form:unitids

  if specs = "imi" then
    rr("<tr valign=top><td class=form_item1>������<td class=form_item2>")
    rr "<input type=text name=factories value='" & getval(thedata1, "factories") & "' style='width:300;'> *����� �������"
    checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""factories"", f1.factories.value)" & vbcrlf
  end if
']

  if oksee("subusers") then'form:subusers
    rr("<tr valign=top id=subusers_tr0><td class=form_item1 id=subusers_tr1><span id=subusers_caption>~buusers_subusers~</span><td class=form_item2 id=subusers_tr2>")
'[    rr(selectlookupmultiauto("f1.subusers",thesubusers, "icons/hire-me.png", "buusers,username,", 300, okwritehere))
    t = "select id,username from buusers"
    if thesiteid <> "0" then t = t & " where id in(" & thesubusers & "-1) or (siteid = " & thesiteid & " or siteid = 0)"
    rr(selectlookupmultiauto("f1.subusers",thesubusers, "icons/hire-me.png", "buusers,username,:" & t, 300, okwritehere))

    if instr(",rotem,icl,", "," & specs & ",") > 0 and theusergroup = "Approver" then
      rr("<tr valign=top><td class=form_item1>~reasons_reasons~<td class=form_item2>")
      rr(selectlookupmultiauto("f1.data1reasonids",getval(thedata1, "reasonids") , "icons/bookmark.png", "reasons,title,", 300, okwritehere))
      checkformadd = checkformadd & "f1.data1.value = setval(f1.data1.value, ""reasonids"", f1.data1reasonids.value)" & vbcrlf
    end if
']
  end if'form:subusers

  if oksee("ordertypeids") then'form:ordertypeids
    rr("<tr valign=top id=ordertypeids_tr0><td class=form_item1 id=ordertypeids_tr1><span id=ordertypeids_caption>~buusers_ordertypeids~</span><td class=form_item2 id=ordertypeids_tr2>")
'[    rr(selectlookupmultiauto("f1.ordertypeids",theordertypeids, "icons/order-1.png", "ordertypes,title,", 300, okwritehere))
    t = ":select id,title from ordertypes where id in(" & theordertypeids & "-1) or active = 'on' order by title"
    rr(selectlookupmultiauto("f1.ordertypeids",theordertypeids, "icons/order-1.png", "ordertypes,title," & t, 300, okwritehere))
']
  end if'form:ordertypeids

  if oksee("firstname") then'form:firstname
    rr("<tr valign=top id=firstname_tr0><td class=form_item1 id=firstname_tr1><span id=firstname_caption>~buusers_firstname~</span><td class=form_item2 id=firstname_tr2>")
    rr("<input type=text maxlength=30 name=firstname onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thefirstname & """ dir=~langdir~>")
  end if'form:firstname

  if oksee("lastname") then'form:lastname
    rr("<tr valign=top id=lastname_tr0><td class=form_item1 id=lastname_tr1><span id=lastname_caption>~buusers_lastname~</span><td class=form_item2 id=lastname_tr2>")
    rr("<input type=text maxlength=30 name=lastname onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thelastname & """ dir=~langdir~>")
  end if'form:lastname

  if oksee("email") then'form:email
    rr("<tr valign=top id=email_tr0><td class=form_item1 id=email_tr1><span id=email_caption>~buusers_email~</span><td class=form_item2 id=email_tr2>")
'[    rr("<input type=text maxlength=60 name=email onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theemail & """ dir=~langdir~>")
    rr("<input type=text maxlength=60 name=email autocomplete=off onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theemail & """ dir=~langdir~>")
']

  end if'form:email

  if oksee("phone") then'form:phone
    rr("<tr valign=top id=phone_tr0><td class=form_item1 id=phone_tr1><span id=phone_caption>~buusers_phone~</span><td class=form_item2 id=phone_tr2>")
    rr("<input type=text maxlength=50 name=phone onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thephone & """ dir=~langdir~>")
  end if'form:phone

  if oksee("pass") then'form:pass
'[    rr("<tr valign=top id=pass_tr0><td class=form_item1 id=pass_tr1><span id=pass_caption>~buusers_pass~</span><td class=form_item2 id=pass_tr2>")
'[    rr("<input type=text maxlength=30 name=pass onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thepass & """ dir=~langdir~>")
    if hasmatch(specs, "jerusalem,muni,brener,") then
      rr("<tr valign=top id=pass_tr0><td class=form_item1 id=pass_tr1><span id=pass_caption>~buusers_pass~</span><td class=form_item2 id=pass_tr2>")
      rr("<input type=password maxlength=30 name=pass onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & decrypt(Thepass,1) & """ dir=~langdir~>")
    elseif okwritehere then
      rr("<tr valign=top id=pass_tr0><td class=form_item1 id=pass_tr1><span id=pass_caption>~buusers_pass~</span><td class=form_item2 id=pass_tr2>")
      'rr("<input type=password maxlength=30 name=pass onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' autocomplete=off value=""" & decrypt(Thepass,"1") & """ dir=~langdir~>")
      rr(" <input type=button id=buttonresetpassword value=""~Reset Password~"" onclick='vbscript:if msgbox(""~Reset Password~?"",vbyesno) = 6 then f1.action2.value = ""stay"" : f1.action3.value = ""resetpassword"" : checkform'>")
      if session("username") = "kiki" and getfield("reveal") = "on" then rr decrypt(thepass, "1")
      if request_action3 = "resetpassword" and getfield("token") = session("token") then
        p = "ag23" & createpassword(4)
        s = "update buusers set pass = '" & encrypt(p, "1") & "' where id = " & theid
        set rs1 = conn.execute(sqlfilter(s))
        rr " " & p & " - ~the user will be requested to change his password upon next login~"
        if specs <> "iai" then
          subject = translate(getparam("applicationdisplay") & " - ~Reset Password~")
          body = translate("~Your password has been reset. Your new password is~: " & p & "<br><br>(~the user will be requested to change his password upon next login~)")
          x = sendemail(getparam("siteemail"), theemail, "", "", subject, body)
        end if
      end if
    end if
']
  end if'form:pass

  if oksee("picture") then'form:picture
    rr("<tr valign=top id=picture_tr0><td class=form_item1 id=picture_tr1><span id=picture_caption>~buusers_picture~</span><td class=form_item2 id=picture_tr2>")
    rr("<table border=0 cellpadding=0 cellspacing=0 align=~langside2~><tr><td><div id=picture_preview>")
    if lcase(right(thepicture,4)) = ".jpg" or lcase(right(thepicture,4)) = ".gif" or lcase(right(thepicture,4)) = ".png" then
      rr("<a href='" & thepicture & "' target=_blank><img src='" & thepicture & "' width=200 border=0></a>")
    else
      rr("<a href='" & thepicture & "' target=_blank><font style='font-size:18;'><b>" & ucase(mid(thepicture,instrrev(thepicture,".")+1)) & "</a>")
    end if
    rr("</div></table>")
    if okwritehere then rr("<nobr><table align=~langside~><tr><td><iframe id=picture_upload frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=picture&previewdiv=picture_preview></iframe></table>")
    rr("<input type=text maxlength=100 id=picture name=picture style='width:200;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' value=""" & Thepicture & """ dir=ltr><br>")
  end if'form:picture

  if oksee("allowedips") then'form:allowedips
    rr("<tr valign=top id=allowedips_tr0><td class=form_item1 id=allowedips_tr1><span id=allowedips_caption>~buusers_allowedips~</span><td class=form_item2 id=allowedips_tr2>")
    rr("<input type=text maxlength=255 name=allowedips onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theallowedips & """ dir=~langdir~>")
  end if'form:allowedips
  if oksee("allowedips") then rr(" <img src=icons/i_help.gif style='cursor:hand;' alt='~buusers_allowedips_help~'>")

  if oksee("expires") then'form:expires
    rr("<tr valign=top id=expires_tr0><td class=form_item1 id=expires_tr1><span id=expires_caption>~buusers_expires~<img src=must.gif></span><td class=form_item2 id=expires_tr2>")
    rr(SelectDate("f1.expires*", theexpires))
  end if'form:expires

  if oksee("remarks") then'form:remarks
    rr("<tr valign=top id=remarks_tr0><td class=form_item1 id=remarks_tr1><span id=remarks_caption>~buusers_remarks~</span><td class=form_item2 id=remarks_tr2>")
    rr("<textarea id=qtextarea1 name=remarks style='width:301; height:20;' onkeyup='vbscript:resizeq1'>")
    rr(theremarks)
    rr("</textarea>" & vbcrlf)
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub resizeq1" & vbcrlf)
    rr("    dim d,h,i" & vbcrlf)
    rr("    d = document.getelementbyid(""qtextarea1"").value" & vbcrlf)
    rr("    h = 22" & vbcrlf)
    rr("    for i = 1 to len(d)" & vbcrlf)
    rr("      if mid(d,i,2) = vbcrlf then h = h + 15" & vbcrlf)
    rr("    next" & vbcrlf)
    rr("    if clng(h) < clng(len(d) / 2) then h = 16 + cint(len(d)/3)" & vbcrlf)
    rr("    if h > 150 then h = 150" & vbcrlf)
    rr("    document.getelementbyid(""qtextarea1"").style.height = h" & vbcrlf)
    if childpage then rr("    parent.document.getelementbyid(""fr" & tablename & """).height = document.body.scrollheight + 20" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("  resizeq1" & vbcrlf)
    rr("</script>" & vbcrlf)
  end if'form:remarks

  if oksee("updateduserid") then'form:updateduserid
    rr("<tr valign=top id=updateduserid_tr0><td class=form_item1 id=updateduserid_tr1><span id=updateduserid_caption>~buusers_updateduserid~</span><td class=form_item2 id=updateduserid_tr2>")
    if ulev > 4 then
      rr("<select name=updateduserid dir=~langdir~>")
      sq = "select * from buusers order by username"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      rr("<option value=0>")
      do until rs1.eof
        rr("<option value=""" & rs1("id") & """")
        if cstr(Theupdateduserid) = cstr(rs1("id")) then rr(" selected")
        rr(">" & rs1("username"))
        rs1.movenext
      loop
      rr("</select>")'form:updateduserid
    else
      sq = "select * from buusers where id = " & theupdateduserid
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      if not rs1.eof then rr("<b>" & rs1("username") & "</b>")
    end if'form:updateduserid
  end if'form:updateduserid

  if oksee("updateddate") then'form:updateddate
    rr(" <span id=updateddate_caption class=form_item3>~buusers_updateddate~</span> ")
    if ulev > 4 then
      rr(SelectDate("f1.updateddate*:", theupdateddate))
    else
      rr(theupdateddate)
    end if
  end if'form:updateddate

  if oksee("sessionvalues") then'form:sessionvalues
    rr("<span id=sessionvalues_caption></span> ")
    rr("<input type=hidden name=sessionvalues value=""" & Thesessionvalues & """>")
  end if'form:sessionvalues

'[  if oksee("starred") then'form:starred
'[    rr("<span id=starred_caption></span> ")
'[    rr("<input type=hidden name=starred value=""" & Thestarred & """>")
'[  end if'form:starred
']

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
  end if'form:data1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonduplicate value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
'[  rr("  sub checkform" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr checkformadd
']
  rr("    ok = true" & vbcrlf)
  if okedit("expires") then rr("    if instr(f1.expires.value,""1/1/1900"") = 0 and f1.expires.value <> """" then validfield(""expires"") else invalidfield(""expires"") : ok = false" & vbcrlf)
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.username.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".username" : session(tablename & "_hidelistcolumns") = "pass,picture,allowedips,remarks,updateduserid,updateddate,sessionvalues,starred,data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".username"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><!img src='icons/hire-me.png' ~iconsize~> ~buusers_buusers~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
'[if session(tablename & "_advanced") = "y" then
']
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  rr(" <nobr><img id=usergroup_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=usergroup dir=~langdir~ style='width:150;'>")
'[  rr("<option value='' style='color:bbbbfe;'>~buusers_usergroup~")
'[  aa = "" : a = aa
  rr("<option value='' style='color:bbbbfe;'>~buusers_usergroup~")
  'sq = "select distinct usergroup from buusers order by usergroup"
  'closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
  'a = "" : do until rs1.eof : a = a & rs1("usergroup") & "," : rs1.movenext : loop
  a = getparam("usergroups") : if hasmatch(specs, "jerusalem,") then a = "Admin,Support,Rakaz,Moked,"

']
  'sq = "select distinct usergroup from buusers order by usergroup"
  'set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof : a = a & rs1("usergroup") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if session(tablename & "_usergroup") = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select></nobr>")'search:usergroup
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~buusers_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
'[  end if'search:siteid
  end if'search:siteid
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"
']
  rr(" <input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

'[end if
'[  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
'[  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
'[  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y>~Advanced Search~</a>")
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
']

  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"
  s = replace(s, " from ", ", cstr('' & buusers_1.username) as updateduseridlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join buusers buusers_1 on " & tablename & ".updateduserid = buusers_1.id)"

'[  '-sqladd
  if session(tablename & "_letter") <> "" then s = s & " and lcase(buusers.username) like '" & lcase(session(tablename & "_letter")) & "%'"
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","buusers.id,buusers.username,buusers.userlevel,buusers.usergroup,buusers.siteid,buusers.perm,buusers.supplierids,buusers.unitids,buusers.subusers,buusers.ordertypeids,buusers.firstname,buusers.lastname,buusers.email,buusers.phone,buusers.pass,buusers.picture,buusers.allowedips,buusers.expires,buusers.remarks,buusers.updateduserid,buusers.updateddate,buusers.sessionvalues,buusers.starred,buusers.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_usergroup") <> "" then s = s & " and lcase(" & tablename & ".usergroup) = '" & lcase(session(tablename & "_usergroup")) & "'"
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".username like '%" & w & "%' or"
        s = s & " " & tablename & ".userlevel like '%" & w & "%' or"
        s = s & " " & tablename & ".usergroup like '%" & w & "%' or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".perm like '%" & w & "%' or"
        t = "select top 50 id from suppliers where (cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".supplierids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        t = "select top 50 id from units where (cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".unitids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        t = "select top 50 id from buusers where (cstr('' & username)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".subusers), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        t = "select top 50 id from ordertypes where (cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".ordertypeids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        s = s & " " & tablename & ".firstname like '%" & w & "%' or"
        s = s & " " & tablename & ".lastname like '%" & w & "%' or"
        s = s & " " & tablename & ".email like '%" & w & "%' or"
        s = s & " " & tablename & ".phone like '%" & w & "%' or"
'[        s = s & " " & tablename & ".pass like '%" & w & "%' or"
']
        s = s & " " & tablename & ".picture like '%" & w & "%' or"
        s = s & " " & tablename & ".allowedips like '%" & w & "%' or"
        s = s & " " & tablename & ".remarks like '%" & w & "%' or"
'[        s = s & " cstr('' & buusers_1.username) & ' ' like '%" & w & "%' or"
']
'[        s = s & " " & tablename & ".sessionvalues like '%" & w & "%' or"
']
'[        s = s & " " & tablename & ".starred like '%" & w & "%' or"
']
'[        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
']
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if

  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("username") then f = "buusers.username" : f2 = "username" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("username") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_username~</a> " & a2)
  if oksee("userlevel") then f = "buusers.userlevel" : f2 = "userlevel" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("userlevel") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_userlevel~</a> " & a2)
  if oksee("usergroup") then f = "buusers.usergroup" : f2 = "usergroup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("usergroup") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_usergroup~</a> " & a2)
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_siteid~</a> " & a2)
  end if
  if oksee("perm") then rr("<td class=list_title><nobr>~buusers_perm~")
  if oksee("supplierids") then rr("<td class=list_title><nobr>~buusers_supplierids~")
  if oksee("unitids") then rr("<td class=list_title><nobr>~buusers_unitids~")
  if oksee("subusers") then rr("<td class=list_title><nobr>~buusers_subusers~")
  if oksee("ordertypeids") then rr("<td class=list_title><nobr>~buusers_ordertypeids~")
  if oksee("firstname") then f = "buusers.firstname" : f2 = "firstname" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("firstname") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_firstname~</a> " & a2)
  if oksee("lastname") then f = "buusers.lastname" : f2 = "lastname" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("lastname") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_lastname~</a> " & a2)
  if oksee("email") then f = "buusers.email" : f2 = "email" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("email") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_email~</a> " & a2)
  if oksee("phone") then f = "buusers.phone" : f2 = "phone" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("phone") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_phone~</a> " & a2)
  if oksee("pass") then f = "buusers.pass" : f2 = "pass" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("pass") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_pass~</a> " & a2)
  if oksee("picture") then f = "buusers.picture" : f2 = "picture" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("picture") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_picture~</a> " & a2)
  if oksee("allowedips") then rr("<td class=list_title><nobr>~buusers_allowedips~")
  if oksee("expires") then f = "buusers.expires" : f2 = "expires" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("expires") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_expires~</a> " & a2)
  if oksee("remarks") then rr("<td class=list_title><nobr>~buusers_remarks~")
  if oksee("updateduserid") and session(tablename & "containerfield") <> "updateduserid" then
    f = "buusers_1.username" : f2 = "updateduseridlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_updateduserid~</a> " & a2)
  end if
  if oksee("updateddate") then f = "buusers.updateddate" : f2 = "updateddate" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("updateddate") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~buusers_updateddate~</a> " & a2)
  if oksee("sessionvalues") then rr("<td class=list_title><nobr>~buusers_sessionvalues~")
  if oksee("starred") then rr("<td class=list_title><nobr>~buusers_starred~")
  if oksee("data1") then rr("<td class=list_title><nobr>~buusers_data1~")

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Theusername = rs("username") : if isnull(Theusername) then Theusername = ""
      Theuserlevel = rs("userlevel") : if isnull(Theuserlevel) then Theuserlevel = ""
      Theusergroup = rs("usergroup") : if isnull(Theusergroup) then Theusergroup = ""
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Theperm = rs("perm") : if isnull(Theperm) then Theperm = ""
      Thesupplierids = rs("supplierids") : Thesupplierids = formatidlist(Thesupplierids)
      Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
      Thesubusers = rs("subusers") : Thesubusers = formatidlist(Thesubusers)
      Theordertypeids = rs("ordertypeids") : Theordertypeids = formatidlist(Theordertypeids)
      Thefirstname = rs("firstname") : if isnull(Thefirstname) then Thefirstname = ""
      Thelastname = rs("lastname") : if isnull(Thelastname) then Thelastname = ""
      Theemail = rs("email") : if isnull(Theemail) then Theemail = ""
      Thephone = rs("phone") : if isnull(Thephone) then Thephone = ""
      Thepass = rs("pass") : if isnull(Thepass) then Thepass = ""
      Thepicture = rs("picture") : if isnull(Thepicture) then Thepicture = ""
      Theallowedips = rs("allowedips") : if isnull(Theallowedips) then Theallowedips = ""
      Theexpires = rs("expires") : if isnull(Theexpires) then Theexpires = "1/1/1900"
      Theexpires = replace(theexpires,".","/")
      Theremarks = rs("remarks") : if isnull(Theremarks) then Theremarks = ""
      Theupdateduserid = rs("updateduserid") : if isnull(Theupdateduserid) then Theupdateduserid = 0
      Theupdateduserid = replace(theupdateduserid,",",".")
      Theupdateddate = rs("updateddate") : if isnull(Theupdateddate) then Theupdateddate = ""
      Thesessionvalues = rs("sessionvalues") : if isnull(Thesessionvalues) then Thesessionvalues = ""
      Thestarred = rs("starred") : if isnull(Thestarred) then Thestarred = ""
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""
      Theupdateduseridlookup = rs("updateduseridlookup") : if isnull(Theupdateduseridlookup) then Theupdateduseridlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = theusername
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/hire-me.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("userlevel") then rr("<td class=list_item valign=top align=><nobr>" & theuserlevel)
      if oksee("usergroup") then rr("<td class=list_item valign=top align=><nobr>" & theusergroup)
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
      theperm = theperm : if isnull(theperm) then theperm = ""
      theperm = replace(theperm,"<br>"," ") : theperm = replace(theperm,"<","[") : theperm = replace(theperm,">","]") : if len(theperm) > 30 then theperm = left(theperm,30) & "..."
      if oksee("perm") then rr("<td class=list_item valign=top align=><nobr>" & theperm)
      t = "" : d = formatidlist(thesupplierids)
      sq = "select * from suppliers where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("title")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("supplierids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      t = "" : d = formatidlist(theunitids)
      sq = "select * from units where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("title")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("unitids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      t = "" : d = formatidlist(thesubusers)
      sq = "select * from buusers where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("username")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("subusers") then rr("<td class=list_item valign=top align=><nobr>" & t)
      t = "" : d = formatidlist(theordertypeids)
      sq = "select * from ordertypes where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("title")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("ordertypeids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      if oksee("firstname") then rr("<td class=list_item valign=top align=><nobr>" & thefirstname)
      if oksee("lastname") then rr("<td class=list_item valign=top align=><nobr>" & thelastname)
      if oksee("email") then rr("<td class=list_item valign=top align=><nobr>" & theemail)
      if oksee("phone") then rr("<td class=list_item valign=top align=><nobr>" & thephone)
'[      if oksee("pass") then rr("<td class=list_item valign=top align=><nobr>" & thepass)
      t = "" : for i = 1 to len(thepass) : t = t & "*" : next
      if oksee("pass") then rr("<td class=list_item valign=top align=><nobr>" & t)
']
      if oksee("picture") then
        rr("<td class=list_item valign=top align=><nobr>")
        p = thepicture : p1 = p : if instr(p1,"/") > 0 then p1 = mid(p1,instr(p1,"/")+1)
        rr("<a href='" & thepicture & "' target=_blank>")
        if lcase(right(p,4)) = ".jpg" or lcase(right(p,4)) = ".gif" or lcase(right(p,4)) = ".png" then rr("<img src=""" & p & """ style='width:100; border:1px solid #000000;'></a>") else rr(p1 & "</a>")
      end if
      theallowedips = theallowedips : if isnull(theallowedips) then theallowedips = ""
      theallowedips = replace(theallowedips,"<br>"," ") : theallowedips = replace(theallowedips,"<","[") : theallowedips = replace(theallowedips,">","]") : if len(theallowedips) > 30 then theallowedips = left(theallowedips,30) & "..."
      if oksee("allowedips") then rr("<td class=list_item valign=top align=><nobr>" & theallowedips)
      if oksee("expires") then rr("<td class=list_item valign=top align=><nobr>") : if year(theexpires) <> 1900 then rr(theexpires)
      theremarks = theremarks : if isnull(theremarks) then theremarks = ""
      theremarks = replace(theremarks,"<br>"," ") : theremarks = replace(theremarks,"<","[") : theremarks = replace(theremarks,">","]") : if len(theremarks) > 30 then theremarks = left(theremarks,30) & "..."
      if oksee("remarks") then rr("<td class=list_item valign=top align=><nobr>" & theremarks)
      if oksee("updateduserid") and session(tablename & "containerfield") <> "updateduserid" then rr("<td class=list_item valign=top align=><nobr>" & theupdateduseridlookup)
      if oksee("updateddate") then rr("<td class=list_item valign=top align=><nobr>" & theupdateddate)
      thesessionvalues = thesessionvalues : if isnull(thesessionvalues) then thesessionvalues = ""
      thesessionvalues = replace(thesessionvalues,"<br>"," ") : thesessionvalues = replace(thesessionvalues,"<","[") : thesessionvalues = replace(thesessionvalues,">","]") : if len(thesessionvalues) > 30 then thesessionvalues = left(thesessionvalues,30) & "..."
      if oksee("sessionvalues") then rr("<td class=list_item valign=top align=><nobr>" & thesessionvalues)
      thestarred = thestarred : if isnull(thestarred) then thestarred = ""
      thestarred = replace(thestarred,"<br>"," ") : thestarred = replace(thestarred,"<","[") : thestarred = replace(thestarred,">","]") : if len(thestarred) > 30 then thestarred = left(thestarred,30) & "..."
      if oksee("starred") then rr("<td class=list_item valign=top align=><nobr>" & thestarred)
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("userlevel") and session(tablename & "containerfield") <> "userlevel" then rr("<td class=list_total1>") 'userlevel
  if oksee("usergroup") and session(tablename & "containerfield") <> "usergroup" then rr("<td class=list_total1>") 'usergroup
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid
  if oksee("perm") and session(tablename & "containerfield") <> "perm" then rr("<td class=list_total1>") 'perm
  if oksee("supplierids") and session(tablename & "containerfield") <> "supplierids" then rr("<td class=list_total1>") 'supplierids
  if oksee("unitids") and session(tablename & "containerfield") <> "unitids" then rr("<td class=list_total1>") 'unitids
  if oksee("subusers") and session(tablename & "containerfield") <> "subusers" then rr("<td class=list_total1>") 'subusers
  if oksee("ordertypeids") and session(tablename & "containerfield") <> "ordertypeids" then rr("<td class=list_total1>") 'ordertypeids
  if oksee("firstname") and session(tablename & "containerfield") <> "firstname" then rr("<td class=list_total1>") 'firstname
  if oksee("lastname") and session(tablename & "containerfield") <> "lastname" then rr("<td class=list_total1>") 'lastname
  if oksee("email") and session(tablename & "containerfield") <> "email" then rr("<td class=list_total1>") 'email
  if oksee("phone") and session(tablename & "containerfield") <> "phone" then rr("<td class=list_total1>") 'phone
  if oksee("pass") and session(tablename & "containerfield") <> "pass" then rr("<td class=list_total1>") 'pass
  if oksee("picture") and session(tablename & "containerfield") <> "picture" then rr("<td class=list_total1>") 'picture
  if oksee("allowedips") and session(tablename & "containerfield") <> "allowedips" then rr("<td class=list_total1>") 'allowedips
  if oksee("expires") and session(tablename & "containerfield") <> "expires" then rr("<td class=list_total1>") 'expires
  if oksee("remarks") and session(tablename & "containerfield") <> "remarks" then rr("<td class=list_total1>") 'remarks
  if oksee("updateduserid") and session(tablename & "containerfield") <> "updateduserid" then rr("<td class=list_total1>") 'updateduserid
  if oksee("updateddate") and session(tablename & "containerfield") <> "updateddate" then rr("<td class=list_total1>") 'updateddate
  if oksee("sessionvalues") and session(tablename & "containerfield") <> "sessionvalues" then rr("<td class=list_total1>") 'sessionvalues
  if oksee("starred") and session(tablename & "containerfield") <> "starred" then rr("<td class=list_total1>") 'starred
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
'[  rr("</tr></form>" & bottombar & "</table>")'list
  rr(" <img src=icons/i_letter.gif id=sendmessageicon> <input type=button class=groovybutton value='~Send Message~' onclick='vbscript: me.style.visibility = ""hidden"" : sendmessageicon.style.visibility = ""hidden"" : dmessage.style.visibility = ""visible"" : dmessage.style.position = ""relative"" : dmessage.style.top = ""0px"" '>")
  rr("<tr><td class=list_multi align=~langside~ colspan=99>")
  rr("<table width=100% id=dmessage style='visibility:hidden; position:absolute; top:-500;'>" & bottombar)
  rr("<tr valign=top><td class1=form_item1><nobr>~Subject~<td class1=form_item2 width=100% >")
  rr("<input type=text name=subject style=width:500; onkeyup='vbscript: charcount.innerhtml = len(flist.subject.value) + len(flist.body.value) + 1'>")
  rr("<tr valign=top id=dletter1><td class1=form_item1><nobr>~Body~<td class1=form_item2>")
  rr("<textarea name=body style='width:500; height:150;' onkeyup='vbscript: charcount.innerhtml = len(flist.subject.value) + len(flist.body.value) + 1'></textarea>")
 'rr(" <img src=icons/i_help.gif style='cursor:hand;' alt='Example: Hello *firstname* *lastname*, your Username for the system is *username*, your password is *pass* ; Available fields: username, userlevel, allowedips, firstname, lastname, expires, email, phone, pass, remarks, picture, permissions, usergroup, updateduserid, updateddate, parentid'>")
  rr("<tr valign=top id=dletter2><td class1=form_item1><nobr>~Attach File~<td class1=form_item2>")
  rr("<table border=0 cellpadding=0 cellspacing=0 align=left><tr><td><div id=preview_file1>")
  if lcase(right(thefile1,4)) = ".jpg" or lcase(right(thefile1,4)) = ".gif" then
    t = "<a href=#aaa onclick='vbscript: set imagepreview = window.open(`xxx`,`imagepreview`,`toolbar=0,titlebar=1,menubar=0,location=0,status=0,scrollbars=yes,left=50,top=50,width=700,height=500,resizable=1`) : imagepreview.focus'><img src=`xxx` width=100 border=0></a>"
    t = replace(t,"xxx",thefile1)
    rr(replace(t,"`",""""))
  else
    rr("<font style='font-size:18;'><b>" & ucase(mid(thefile1,instrrev(thefile1,".")+1)))
  end if
  rr("</div></table>")
  rr("<input type=text maxlength=200 id=file1 name=file1 style=width:200; value=""" & Thefile1 & """ dir=ltr> ")
  rr("<iframe style='position:relative; top:4;' name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=22 scrolling=no src=upload.asp?box=flist.file1&previewdiv=preview_file1></iframe>")
  rr("<input type=checkbox name=sms>SMS (<span id=charcount>0</span>) chars ")
  rr("<input type=submit id=flistsubmit style='width:0; height:0;'>") 'only this updates the htmlarea on submit
  rr("<input type=button onclick='vbscript:if msgbox(""~Send Message to all checked~?"",vbyesno) = 6 then flist.action.value = ""multisend"" : flistsubmit.click' value=""~Send Message~"">")
  rr("</table>")
  rr("</tr></form>" & bottombar & "</table>")'list
']
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
