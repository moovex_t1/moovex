<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 23/02/2016 12:39:53

function create_mossad_station(byval title, byval address) 'jerusalem
  dim xy,lat,lon,v,t,i,sq,rs1
  xy = getaddressxy(address) & ","
  lat = getlistitem(xy, 1) : lon = getlistitem(xy, 2)
  if lat = "" then
    v = 0
  else
    t = "���� | " & title
    i = getonefield("select id from stations where title = '" & t & "'")
    if i = "" then
      sq = "insert into stations(title) values('" & t & "')"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      sq = "select max(id) as id from stations"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      i = rs1("id")
    end if
    sq = "update stations set"
    sq = sq & " title='" & t & "'"
    sq = sq & ",inmap='" & address & "'"
    sq = sq & ",special=''"
    sq = sq & ",siteid=0" & getfield("siteid")
    sq = sq & ",markerx='" & lat & "'"
    sq = sq & ",markery='" & lon & "'"
    sq = sq & ",data1='|source=units|'"
    sq = sq & " where id = " & i
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))

    tt = getidlist("select distinct courseid from coursestations where stationid = " & i)
    do until tt = ""
      t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
      du = rebuildcoursefromweekplan(t)
    loop
   
  end if
  create_mossad_station = i
end function

function unitsdelitem(i)
  unitsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function unitsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  t = session("usersiteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
']
  f = f & "siteid," : v = v & t & ","

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "bunitid" then t = session(tablename & "containerid")
  if getfield("bunitid") <> "" then t = getfield("bunitid")
  f = f & "bunitid," : v = v & t & ","

  t = "" : if getfield("workericon") <> "" then t = getfield("workericon")
  f = f & "workericon," : v = v & "'" & t & "',"

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into units(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from units"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  unitsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table units (id autoincrement primary key,title text(200),siteid number,bunitid number,workericon text(20),data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "units" : session("tablename") = "units"
tablefields = "id,title,siteid,bunitid,workericon,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
if session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"

additionals = ""
t1 = "" : t2 = "" : if hasmatch(specs, "jerusalem,") then t1 = "����," : t2 = "���� ����,"
if hasmatch(specs, "jerusalem,muni,brener,") then additionals = additionals & "�� ������,�����,����,�����,���,��� ����," & t1 & "��� ����,��� ���� ���� ������,�����," & t2 & "�����,������,"
if hasmatch(specs, "jerusalem,muni,brener,") then additionals = additionals & "����� ����,��� ��� 1,����� ��� ��� 1,���� ��� ��� 1,����� ��� ��� 1,��� ��� 2,����� ��� ��� 2,���� ��� ��� 2,����� ��� ��� 2,�����,"
if hasmatch(specs, "jerusalem,muni,brener,") then hidecontrols = hidecontrols & "workericon,bunitid,"
if hasmatch(specs, "misgav,") then additionals = "������,��� ����,��� ������,���� �����,�����,���� �����,������,�� ������,"
if specs = "iec" then additionals = "��� �����,"
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = unitsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  msg = msg & " ~Item Updated~"

  if hasmatch(specs, "jerusalem,muni,brener,") then
    i = getidlist("select id from units where id <> " & request_id & " and trim(lcase(title)) = '" & trim(lcase(getfield("title"))) & "'")
    if i <> "" then du = errorend("_back��� ���� ���� ��� �� �� ���")
    semel = getfield("��� ����") ': if len(semel) <> 6 or semel <> strfilter(semel, "0123456789") then du = errorend("_back��� ���� ��� ���� ��� 6 ����� ������� ����")
    i = getidlist("select id from units where id <> " & request_id & " and instr(cstr('' & data1), '|��� ����=" & semel & "|') > 0")
    if i <> "" then du = errorend("_back��� ���� ���� ��� �� ��� ���� ���")
  end if
']

  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  if okedit("bunitid") then s = s & "bunitid=0" & getfield("bunitid") & ","
  if okedit("workericon") then s = s & "workericon='" & getfield("workericon") & "',"
'[  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  data1 = getonefield("select data1 from units where id = " & request_id)
  data1 = setval(data1, "semel", getfield("semel"))
  data1 = setval(data1, "manager", getfield("manager"))
  t = getfield("address") : if t = "" then t = getfield("�����")
  data1 = setval(data1, "address", t)
  data1 = setval(data1, "city", getfield("city"))
  data1 = setval(data1, "phone", getfield("phone"))
  data1 = setval(data1, "remarks", getfield("remarks"))
  for z = 1 to countstring(additionals, ",")
    f = getlistitem(additionals, z) : data1 = setval(data1, f, getfield(f))
  next
  if hasmatch(specs, "jerusalem,muni,brener,") then
    data1 = setval(data1, "�����", getfield("supplierids"))
    data1 = setval(data1, "suppliersfilter", replace("," & getfield("supplierids"), ",", "SID"))
    t = getfield("����") : t = strfilter(t, "0123456789") : t = getonefield("select id from stations where id = 0" & t)
    if getfield("�����") <> "" and getfield("�����") <> getfield("ktovetwas") or (t = "" and getfield("�����") <> "") then
      t = create_mossad_station(getfield("title"), getfield("�����"))
  	  data1 = setval(data1, "����", t)
    end if
  end if

  if okedit("data1") then s = s & "data1='" & data1 & "',"
']

  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = unitsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = unitsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""

      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~units_title~^"
  if oksee("siteid") then csv(0) = csv(0) & "~units_siteid~^"
  if oksee("bunitid") then csv(0) = csv(0) & "~units_bunitid~^"
  if oksee("workericon") then csv(0) = csv(0) & "~units_workericon~^"
'[  if oksee("data1") then csv(0) = csv(0) & "~units_data1~^"
  'if specs = "modiin" then
	'  if oksee("data1") then csv(0) = csv(0) & "���^"'semel
	'  if oksee("data1") then csv(0) = csv(0) & "����^"'manager
	'  if oksee("data1") then csv(0) = csv(0) & "�����^"'address
	'  if oksee("data1") then csv(0) = csv(0) & "���^"'city
	'  if oksee("data1") then csv(0) = csv(0) & "�����^"'phone
	'  if oksee("data1") then csv(0) = csv(0) & "�����^"'remarks
  'end if
  for z = 1 to countstring(additionals, ",")
    f = getlistitem(additionals, z) : csv(0) = csv(0) & f & "^"
  next
']
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thebunitid = rs("bunitid") : if isnull(Thebunitid) then Thebunitid = 0
  Thebunitid = replace(thebunitid,",",".")
  Theworkericon = rs("workericon") : if isnull(Theworkericon) then Theworkericon = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""
  Thebunitidlookup = rs("bunitidlookup") : if isnull(Thebunitidlookup) then Thebunitidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
    if oksee("bunitid") then csv(rc) = csv(rc) & thebunitidlookup & "^"
    if oksee("workericon") then csv(rc) = csv(rc) & replace(theworkericon,vbcrlf," ") & "^"
'[    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    'if specs = "modiin" then
		'  csv(rc) = csv(rc) & tocsv(getval(thedata1, "semel")) & "^"
		'  csv(rc) = csv(rc) & tocsv(getval(thedata1, "manager")) & "^"
		'  csv(rc) = csv(rc) & tocsv(getval(thedata1, "address")) & "^"
		'  csv(rc) = csv(rc) & tocsv(getval(thedata1, "city")) & "^"
		'  csv(rc) = csv(rc) & tocsv(getval(thedata1, "phone")) & "^"
		'  csv(rc) = csv(rc) & tocsv(getval(thedata1, "remarks")) & "^"
    'end if
	  for z = 1 to countstring(additionals, ",")
	    f = getlistitem(additionals, z) : v = getval(thedata1, f)
      if hasmatch(specs, "jerusalem,muni,brener,") and f = "�����" and v = strfilter(v, "0123456789,") then v = getfieldlist("select title from suppliers where id in(" & v & "0)")
      csv(rc) = csv(rc) & tocsv(v) & "^"
	  next
']
    rs.movenext
  loop
'[  response.clear
'[  response.ContentType = "application/csv"
  if specs = "jerusalem" then exporttoexcel
  response.clear
  response.ContentType = "application/csv"
']
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      siteid = session("usersiteid")
      title = ""
      data1 = ""
      data1_additionals = ""
      data1_semel = ""
      data1_manager = ""
      data1_address = ""
      data1_city = ""
      data1_phone = ""
      data1_remarks = ""
      data1_supplierids = ""
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "title" then
          if len(b) > 200 then b = left(b,200)
'[          if okedit("title") then s = s & "title='" & b & "',"
          if okedit("title") then s = s & "title='" & b & "',"
          sq = "select id from units where title = '" & b & "' and siteid = " & siteid
          set rs1 = conn.execute(sqlfilter(sq))
          if not rs1.eof then thisid = rs1("id")
          title = b
']
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("siteid") then s = s & "siteid=" & v & ","
          if okedit("siteid") then s = s & "siteid=" & v & ","
          if session("usersiteid") = "0" then siteid = v
']
        elseif csvff(i) = "bunitid" then
          if b = "" then
            v = 0
          else
            sq = "select * from bunits where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("bunitid") then s = s & "bunitid=" & v & ","
        elseif csvff(i) = "workericon" then
          if len(b) > 20 then b = left(b,20)
          if okedit("workericon") then s = s & "workericon='" & b & "',"
        elseif csvff(i) = "data1" then
'[          if okedit("data1") then s = s & "data1='" & b & "',"
          data1 = b
        elseif csvff(i) = "semel" or csvff(i) = "���" then
          data1_semel = b
        elseif csvff(i) = "manager" or csvff(i) = "����" then
          data1_manager = b
        elseif csvff(i) = "address" or csvff(i) = "�����" then
          data1_address = b
        elseif csvff(i) = "city" or csvff(i) = "���" then
          data1_city = b
        elseif csvff(i) = "phone" or csvff(i) = "�����" then
          data1_phone = b
        elseif csvff(i) = "remarks" or csvff(i) = "�����" then
          data1_remarks = b
        elseif csvff(i) = "supplierids" or csvff(i) = "�����" or csvff(i) = "����� ����" then
          data1_supplierids = b

']
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
'[        elseif formdatafield <> "" and instr(b,"=") > 0 then
'[          formdatavalue = formdatavalue & b & "|"
'[        end if
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
        for z = 1 to countstring(additionals, ",")
          f = getlistitem(additionals, z) : if csvff(i) = f then data1_additionals = setval(data1_additionals, f, tocsv(b))
        next
']

      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
'[        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = unitsinsertrecord
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = unitsinsertrecord

        '----data1
        'if data1 = "" then data1 = getonefield("select data1 from units where id = " & theid)
        data1 = getonefield("select data1 from units where id = " & theid)
        if hasmatch(specs, "jerusalem,muni,brener,") then
          if title <> "" and data1_address <> "" and data1_address <> getval(data1, "address") then
            t = create_mossad_station(title, data1_address)
            data1 = setval(data1, "����", t)
          end if
        end if
        for z = 1 to countstring(additionals, ",")
          f = getlistitem(additionals, z) : v = getval(data1_additionals, f) : if v <> "" then data1 = setval(data1, f, v)
        next
        if data1_semel <> "" then data1 = setval(data1, "semel", data1_semel)
        if data1_manager <> "" then data1 = setval(data1, "manager", data1_manager)
        if data1_address <> "" then data1 = setval(data1, "address", data1_address)
        if data1_city <> "" then data1 = setval(data1, "city", data1_city)
        if data1_phone <> "" then data1 = setval(data1, "phone", data1_phone)
        if data1_remarks <> "" then data1 = setval(data1, "remarks", data1_remarks)
        if data1_supplierids <> "" then
          tt = data1_supplierids : tt = replace(tt,";",",") & "," : ii = ""
          do until tt = ""
            t = trim(left(tt,instr(tt,",")-1)) : tt = mid(tt,instr(tt,",")+1)
            i = getonefield("select id from suppliers where title = '" & t & "'")
            if i <> "" then ii = ii & i & ","
          loop
          t = "supplierids" : if hasmatch(specs, "jerusalem,muni,brener,") then t = "�����"
          data1 = setval(data1, t, ii)
        end if
        s = s & "data1='" & data1 & "',"
        '-----
']
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","units.id,units.title,units.siteid,units.bunitid,units.workericon,units.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thebunitid = rs("bunitid") : if isnull(Thebunitid) then Thebunitid = 0
  Thebunitid = replace(thebunitid,",",".")
  Theworkericon = rs("workericon") : if isnull(Theworkericon) then Theworkericon = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "title,siteid,bunitid,workericon,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><!img src='icons/i_shield.gif' ~iconsize~> ~units_units~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~units_title~</span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=200 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  end if'form:title

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~units_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~units_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

  if oksee("bunitid") and session(tablename & "containerfield") <> "bunitid" then
    rr("<tr valign=top id=bunitid_tr0><td class=form_item1 id=bunitid_tr1><span id=bunitid_caption>~units_bunitid~</span><td class=form_item2 id=bunitid_tr2>")
    if getfield("box") = "bunitid" then thebunitid = getfield("boxid")
    rr("<select name=bunitid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~units_bunitid~")
    sq = "select * from bunits order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thebunitid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:bunitid
  elseif okedit("bunitid") then
    rr("<input type=hidden name=bunitid value=" & thebunitid & ">")
'[  end if'form:bunitid
  end if'form:bunitid

  '---additionals
  for z = 1 to countstring(additionals, ",")
    f = getlistitem(additionals, z) : v = getval(thedata1, f)
    if instr(f, "������") then v = ctxt(v)

    if f = "����" then
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2 height=30>")
      rr "<input type=hidden name=ktovetwas value='" & getval(thedata1, "�����") & "'>"
      rr "<input type=hidden name='" & f & "' value=" & v & ">"
      t = gettitle("stations", v) : if t <> "" then rr "<img src=marker.png width=16 height=20> " & t

    elseif f = "�����" then
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<textarea name='" & f & "' style='width:300; height:100;'>" & replace(v, "<br>", vbcrlf) & "</textarea>"

    elseif hasmatch(specs, "jerusalem,muni,brener,") and f = "�����" then
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select style='width:150;' name=" & f & " onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'>"
      rr("<option value='' style='color:bbbbfe;'>�����")
      sq = "select id,title from misc where type1 = 'neighborhood'"
      sq = sq & " and (instr(cstr('' & contents), '|siteid=" & thesiteid & "|') + instr(cstr('' & contents), '|siteid=0|') > 0 or instr(cstr('' & contents), '|siteid=') = 0)"
      sq = sq & " order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      oneselected = ""
      do until rs1.eof
        rr("<option value=""" & rs1("title") & """")
        if v = cstr(rs1("title")) then rr(" selected") : oneselected = "on"
        rr(">")
        rr(rs1("title"))
        rs1.movenext
      loop
      if oneselected = "" then rr "<option value='" & v & "' selected>" & v
      rr("</select></nobr>")

    elseif hasmatch(specs, "jerusalem,muni,brener,") and f = "�����" then
      rr("<tr valign=top><td class=form_item1>����� ����<td class=form_item2>")
      rr "<input type=hidden name=" & f & " value='" & v & "'>"
      rr(selectlookupmultiauto("f1.supplierids", v, "icons/i_key.gif", "suppliers,title,", 300, okwritehere))

    elseif f = "��� ������" then
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select style='width:150;' name='" & f & "'><option value=''>"
      a = ""
      set rs1 = conn.execute(sqlfilter("select data1 from units"))
      do until rs1.eof
        t = getval(rs1("data1"), f) : t = trim(getlistitem(t,1)) : if not hasmatch(a, t) and t <> "" then a = a & t & ","
        rs1.movenext
      loop
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        rr "<option value='" & b & "'" & iif(v = b, " selected", "") & ">" & b
      loop
      rr "</select>"

    elseif f = "��� ����" then
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select style='width:150;' name='" & f & "'><option value=''>"
      a = sugmosadtypes : if hasmatch(specs, "jerusalem,muni,brener,") then a = getfieldlist("select title from misc where type1 = 'unittypes' order by title")
      if not hasmatch(a, v) then a = a & replace(v, ",", "") & ","
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        rr "<option value='" & b & "'" & iif(v = b, " selected", "") & ">" & b
      loop
      rr "</select>"

    elseif f = "���� ����" then
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select style='width:150;' name='" & f & "'><option value=''>"
      a = "��� �����, ��� �����,"
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        rr "<option value='" & b & "'" & iif(v = b, " selected", "") & ">" & b
      loop
      rr "</select>"

    elseif f = "����" then
      rr "&nbsp;<span style='width:80;'><b>" & f & "</b></span>&nbsp;"
      rr "<select style='width:150;' name='" & f & "'><option value=''>"
      a = "����,����,�����,"
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        rr "<option value='" & b & "'" & iif(v = b, " selected", "") & ">" & b
      loop
      rr "</select>"

    elseif hasmatch(specs, "jerusalem,muni,brener,") and hasmatch(f, "��� ��� 1,����� ��� ��� 1,���� ��� ��� 1,����� ��� ��� 1,��� ��� 2,����� ��� ��� 2,���� ��� ��� 2,����� ��� ��� 2,��� ����,��� ���� ���� ������,���,�����,") then
      if hasmatch(f, "��� ��� 1,��� ��� 2,��� ����,�����,") then rr "<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>" else rr "&nbsp;<span><b><nobr>" & f & "</nobr></b></span>&nbsp;"
	    rr "<input type=text name='" & f & "' style='width:100; direction:~langdir~;' value='" & v & "'>"

    else
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<input type=text name='" & f & "' style='width:300; direction:~langdir~;' value='" & v & "'>"
    end if
  next

  if hasmatch(specs, "jerusalem,muni,brener,") then
    rr "<tr valign=top><td class=form_item1>���� ������� ������<td class=form_item2>"
    rr getonefield("select count(id) as x from workers where unitid = " & theid & " and workertypeid <> " & escortid & " and eligible = 'on'")
  end if

']

  if oksee("workericon") then'form:workericon
    rr("<tr valign=top id=workericon_tr0><td class=form_item1 id=workericon_tr1><span id=workericon_caption>~units_workericon~</span><td class=form_item2 id=workericon_tr2>")
'[    rr("<input type=text maxlength=20 name=workericon onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theworkericon & """ dir=~langdir~>")
    rr "<input type=radio name=workericon value=''"
    if theworkericon = "" then rr " checked"
    rr ">(~Per Business Unit~)<br>"
    for i = 0 to 8
      rr "<input type=radio name=workericon value=worker" & i & ".png"
      if theworkericon = "worker" & i & ".png" then rr " checked"
      rr "><img src=worker" & i & ".png><br>"
    next
']
  end if'form:workericon

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
'[  end if'form:data1
  end if'form:data1

  'if specs = "modiin" then
  '  rr("<tr valign=top><td class=form_item1>���<td class=form_item2>")
  '  rr "<input type=text name=semel style='width:300;' value='" & getval(thedata1, "semel") & "'>"
  '  rr("<tr valign=top><td class=form_item1>����<td class=form_item2>")
  '  rr "<input type=text name=manager style='width:300;' value='" & getval(thedata1, "manager") & "'>"
  '  rr("<tr valign=top><td class=form_item1>�����<td class=form_item2>")
  '  rr "<input type=text name=address style='width:300;' value='" & getval(thedata1, "address") & "'>"
  '  rr("<tr valign=top><td class=form_item1>���<td class=form_item2>")
  '  rr "<input type=text name=city style='width:300;' value='" & getval(thedata1, "city") & "'>"
  '  rr("<tr valign=top><td class=form_item1>�����<td class=form_item2>")
  '  rr "<input type=text name=phone style='width:300;' value='" & getval(thedata1, "phone") & "'>"
  '  rr("<tr valign=top><td class=form_item1>�����<td class=form_item2>")
  '  rr "<input type=text name=remarks style='width:300;' value='" & getval(thedata1, "remarks") & "'>"
  'end if

']

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
'[  rr("    ok = true" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  'if hasmatch(specs, "jerusalem,muni,brener,") then rr " msgbox f1.supplierids.value : f1.data1.value = setval(f1.data1.value, ""�����"", f1.supplierids.value)" & vbcrlf
']
  rr(checkformadd)
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = "data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

'[  '-------------listsession-------------
  if getfield("eshkolfilter_allchecked") = "on" then session(tablename & "_eshkolfilter") = ""
  if getfield("sugmosadfilter_allchecked") = "on" then session(tablename & "_sugmosadfilter") = ""
']
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><!img src='icons/i_shield.gif' ~iconsize~> ~units_units~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~units_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
'[  end if'search:siteid
  end if'search:siteid
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"

  if hasmatch(specs, "jerusalem,muni,brener,") then
    if oksee("additionals_�����") then
      rr(" <nobr> ����� ���� ")
      sq = "select * from suppliers where title <> ''"
      if session("usersiteid") <> "0" then sq = sq & " and siteid in(" & session("usersiteid") & ",0)"
      sq = sq & " order by title"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      a = "-1:(~None~),"
      do until rs1.eof
        a = a & rs1("id") & ":" & replace(rs1("title"),",",";") & ","
        rs1.movenext
      loop
      rr " " & multiselect("suppliersfilter", "~Suppliers~", a, session(tablename & "_suppliersfilter"), 150)
      rr "</nobr>"
    end if
    if oksee("additionals_�����") then rr " <nobr>����� <input type=text name=neighborhoodfilter value='" & session(tablename & "_neighborhoodfilter") & "' style='width:150; height:22;'></nobr>"
    if oksee("additionals_����") then rr " <nobr>���� <input type=text name=migdarfilter value='" & session(tablename & "_migdarfilter") & "' style='width:150; height:22;;'></nobr>"
    if oksee("additionals_����_����") then rr " <nobr>���� ���� <input type=text name=shitatfilter value='" & session(tablename & "_shitatfilter") & "' style='width:150; height:22;;'></nobr>"
    if oksee("additionals_���") and specs <> "jerusalem" then rr " <nobr>��� <input type=text name=cityfilter value='" & session(tablename & "_cityfilter") & "' style='width:150; height:22;'></nobr>"

    if oksee("additionals_�����") then
      'rr " <nobr>����� <input type=text name=eshkolfilter value='" & session(tablename & "_eshkolfilter") & "' style='width:150; height:22;;'></nobr>"
      v = session(tablename & "_eshkolfilter")
      a = "n:(~None~),7:7,8:8,9:9,"
      rr "<nobr> &nbsp;����� "
      rr multiselect("eshkolfilter", "�����", a, v, 150)
      rr "&nbsp;</nobr>"
    end if

    if oksee("additionals_���_����") then
      v = session(tablename & "_sugmosadfilter")
      a = sugmosadtypes : if hasmatch(specs, "jerusalem,muni,brener,") then a = getfieldlist("select title from misc where type1 = 'unittypes' order by title")
      a = "n:(~None~)," & a
      rr "<nobr> &nbsp;��� ���� "
      rr multiselect("sugmosadfilter", "��� ����", a, v, 150)
      rr "&nbsp;</nobr>"
    end if
  end if

']
'[  if session(tablename & "containerfield") <> "bunitid" then
  if oksee("bunitid") then
']
    rr(" <nobr><img id=bunitid_filtericon src=icons/login.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=bunitid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~units_bunitid~")
    sq = "select * from bunits order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_bunitid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:bunitid
  end if'search:bunitid
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
'[  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  e = ""
  e = e & additionals : if e <> "" then e = replace("," & left(e,len(e)-1), ",", ",additionals_") : e = mid(e,2) & ","
  t = mid(tablefields,3) & e : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
']
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"
  s = replace(s, " from ", ", cstr('' & bunits_1.title) as bunitidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join bunits bunits_1 on " & tablename & ".bunitid = bunits_1.id)"

'[  '-sqladd
  t = session(tablename & "_neighborhoodfilter") : if t <> "" and t <> "0" then s = s & " and instr(cstr('' & " & tablename & ".data1), '|�����=" & t & "|') > 0"
  t = session(tablename & "_cityfilter") : if t <> "" and t <> "0" then s = s & " and instr(cstr('' & " & tablename & ".data1), '|���=" & t & "|') > 0"
  t = session(tablename & "_migdarfilter") : if t <> "" and t <> "0" then s = s & " and instr(cstr('' & " & tablename & ".data1), '|����=" & t & "|') > 0"
  t = session(tablename & "_shitatfilter") : if t <> "" and t <> "0" then s = s & " and instr(cstr('' & " & tablename & ".data1), '|���� ����=" & t & "|') > 0"
  tt = session(tablename & "_suppliersfilter") : a = ""
  do until tt = ""
    t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
    a = a & " + instr(cstr('' & " & tablename & ".data1), 'SID" & t & "SID')"
  loop
  if a <> "" then s = s & " and " & mid(a,4) & " > 0"

  tt = session(tablename & "_sugmosadfilter") : a = ""
  do until tt = ""
    t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
    if t = "n" then
      a = a & " or instr(cstr('' & " & tablename & ".data1), '|��� ����=') = 0"
    else
      a = a & " or instr(cstr('' & " & tablename & ".data1), '|��� ����=" & t & "|') > 0"
    end if
  loop
  if a <> "" then s = s & " and (" & mid(a,5) & ")"

  tt = session(tablename & "_eshkolfilter") : a = ""
  do until tt = ""
    t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
    if t = "n" then
      a = a & " or instr(cstr('' & " & tablename & ".data1), '|�����=') = 0"
    else
      a = a & " or instr(cstr('' & " & tablename & ".data1), '|�����=" & t & "|') > 0"
    end if
  loop
  if a <> "" then s = s & " and (" & mid(a,5) & ")"


  if specs = "jerusalem" then s = s & " and trim(" & tablename & ".title) <> ''"

']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","units.id,units.title,units.siteid,units.bunitid,units.workericon,units.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_bunitid") <> "" and session(tablename & "containerfield") <> "bunitid" then s = s & " and " & tablename & ".bunitid = " & session(tablename & "_bunitid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
        s = s & " cstr('' & bunits_1.title) & ' ' like '%" & w & "%' or"
        s = s & " " & tablename & ".workericon like '%" & w & "%' or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------
  '-------------toprow-----------------------------

  rr("<tr class=list_title>")
  if oksee("title") then f = "units.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~units_title~</a> " & a2)
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~units_siteid~</a> " & a2)
  end if
  if oksee("bunitid") and session(tablename & "containerfield") <> "bunitid" then
    f = "bunits_1.title" : f2 = "bunitidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~units_bunitid~</a> " & a2)
  end if
  if oksee("workericon") then f = "units.workericon" : f2 = "workericon" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("workericon") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~units_workericon~</a> " & a2)
'[  if oksee("data1") then rr("<td class=list_title><nobr>~units_data1~")
  'if specs = "modiin" then
  '  rr "<td class=list_title><nobr>���" 'semel
  '  rr "<td class=list_title><nobr>����" 'manager
  '  rr "<td class=list_title><nobr>�����" 'address
  '  rr "<td class=list_title><nobr>���" 'city
  '  rr "<td class=list_title><nobr>�����" 'phone
  '  rr "<td class=list_title><nobr>�����" 'remarks
  'end if

  for z = 1 to countstring(additionals, ",")
    t = getlistitem(additionals, z) : t = "additionals_" & replace(t, " ", "_")
    if oksee(t) then rr "<td class=list_title><nobr>" & getlistitem(additionals, z)
  next
']
  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
'[  '-------------rows--------------------------
  allidscheckscript = ""
  if specs = "jerusalem" and rsrc > 0 and ulev > 1 and okedit("list_checkboxes") then
    checkall = checkall & "flist.allpagecheck.checked = v" & vbcrlf
    checkall = checkall & "flist.allpagecheck2.checked = v" & vbcrlf
    rr "<tr><td colspan=99 style='height:40;'>&nbsp;&nbsp;"
    rr(" <input type=checkbox name=allpagecheck2 onclick='vbscript: checkall(me.checked)'>~All Page~")
    rr " <input type=checkbox name=allidscheck2 onclick='vbscript: flist.allidscheck.checked = me.checked : checkall(me.checked)'>~All Results~ (" & rsrc & ")"
    allidscheckscript = " onclick='vbscript: flist.allidscheck2.checked = me.checked : checkall(me.checked)' "
  end if
']
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Thebunitid = rs("bunitid") : if isnull(Thebunitid) then Thebunitid = 0
      Thebunitid = replace(thebunitid,",",".")
      Theworkericon = rs("workericon") : if isnull(Theworkericon) then Theworkericon = ""
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""
      Thebunitidlookup = rs("bunitidlookup") : if isnull(Thebunitidlookup) then Thebunitidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/i_shield.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
      if oksee("bunitid") and session(tablename & "containerfield") <> "bunitid" then rr("<td class=list_item valign=top align=><nobr>" & thebunitidlookup)
'[      if oksee("workericon") then rr("<td class=list_item valign=top align=><nobr>" & theworkericon)
      t = theworkericon : if t <> "" then t = "<img src=" & t & ">"
      if oksee("workericon") then rr("<td class=list_item valign=top align=><nobr>" & t)
']
'[      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
'[      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
'[      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
		  'if specs = "modiin" then
		  '  rr "<td class=list_item valign=top><nobr>" & getval(thedata1, "semel")
		  '  rr "<td class=list_item valign=top><nobr>" & getval(thedata1, "manager")
		  '  rr "<td class=list_item valign=top><nobr>" & getval(thedata1, "address")
		  '  rr "<td class=list_item valign=top><nobr>" & getval(thedata1, "city")
		  '  rr "<td class=list_item valign=top><nobr>" & getval(thedata1, "phone")
		  '  rr "<td class=list_item valign=top><nobr>" & getval(thedata1, "remarks")
		  'end if
      '----additionals
      for z = 1 to countstring(additionals, ",")
        t = getlistitem(additionals, z) : t = "additionals_" & replace(t, " ", "_")
        f = getlistitem(additionals, z) : v = getval(thedata1, f)
        if instr(f, "������") then v = ctxt(v)
        if oksee(t) then 
          rr "<td class=list_item><nobr>"
          if f = "����" then
            rr gettitle("stations", v)
          elseif f = "�����" then
            tt = v : tt = uniquelist(tt) : a = ""
            do until tt = ""
              t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
              t = gettitle("suppliers", t) : if t <> "" then a = a & ", " & t
            loop
            rr mid(a,3)
          else
            rr v
          end if
        end if
      next
']
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid
  if oksee("bunitid") and session(tablename & "containerfield") <> "bunitid" then rr("<td class=list_total1>") 'bunitid
  if oksee("workericon") and session(tablename & "containerfield") <> "workericon" then rr("<td class=list_total1>") 'workericon
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
'[  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck " & allidscheckscript & ">")
']
  rr("~All Results~ (" & rsrc & ")")

  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
