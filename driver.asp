<!--#include file="include.asp"-->
<%
'-----LOGIN-----------------------------------------------------------
'?action=login&driveridn=1234567&pass=123
'567
'  replies with just the driverid or "E Driver not found"
'
'-----NEXTORDERS-----------------------------------------------------------
'?action=nextorders&driveridn=567
'8945,24/05/2016 12:00:00,Jerusalem - Tel Aviv,5543,John Smith,Ben yehuda street 12,053-4435112,
'8946,24/05/2016 13:00:00,Jerusalem - Tel Aviv,5543,John Smith,Ben yehuda street 12,053-4435112,
'  explain: orderid, date+time, course, workerid, name, address, phone
'  or "E No orders found"
'
'-----PICKED-----------------------------------------------------------
'?action=picked&orderid=567&nfc=ADADAD&v=on
'?action=picked&orderid=567&workerid=5543&v=no
'OK 5543
'  or "E Worker not found". you can provide either nfc tag text or workerid(from the nextorders response)
'  you can give value for v either "on" or "no"
'
'-----COMPLETED-----------------------------------------------------------
'?action=completed&orderid=567&nfc=ADADAD&v=on
'?action=completed&orderid=567&workerid=5543&v=no
'OK 5543
'  or "E Worker not found". you can provide either nfc tag text or workerid(from the nextorders response)
'  you can give value for v either "on" or "no"
'
'-----BEGINRIDE-----------------------------------------------------------
'?action=beginride&orderid=4987&driveridn=487
'OK
'E order not found
'
'-----ENDRIDE-----------------------------------------------------------
'?action=endride&orderid=4987&driverid=487
'OK
'E order not found
'
'-----DISTRESS-----------------------------------------------------------
'?action=distress&orderid=4987&driverid=487
'OK
'E order not found
'
'-----MYLOCATION-----------------------------------------------------------
'?action=mylocation&orderid=4987&driveridn=487&lat=31.344466&lon=32.567755
'OK
'
'-----MARKSTATION-----------------------------------------------------------
'?action=markstation&orderid=4987&lat=31.344466&lon=32.567755
'OK
'
'-----GETLOCATION-----------------------------------------------------------
'?action=getlocation&orderid=4987
'{"lat":31.34455,"lng":43.43566767}
'
'-----GETCOURSEPLAN--------------------------------------------------
'?action=getcourseplan&orderid=4987
'{"stations": [{"name": "station name", "lat": lat, "lng": lng},{...}..]}
'

'---------------------------------------
if getfield("action") = "c" then
	s = "create table driverlocations(orderid number, date1 datetime, lat number, lon number)"
	if session("usingsql") = "1" then
  	s = "create table driverlocations(orderid numeric(10,0), date1 datetime, lat float, lon float)"
  end if
	closers(rs) : set rs = conn.execute(sqlfilter(s))
	rr "ok" : rend
end if

'----------------------------------------------
'if getfield("action") = "login" then
'  i = getonefield("select id from drivers where idn = '" & getfield("driveridn") & "' and pass <> '' and pass = '" & getfield("pass") & "'")
'  if i = "" then
'    rr "E Driver not found"
'  else
'    rr i
'  end if
'  rend
'end if

'http://localhost/tc/driver.asp?action=login&idn=313951154&pass=12345
'{"driverid":"27","token":"nj5v42pq9s"}
if getfield("action") = "login" then
  idn = getfield("idn") : if idn = "" then idn = getfield("driveridn")
  i = getonefield("select id from drivers where idn = '" & idn & "' and pass <> '' and pass = '" & getfield("pass") & "'")
  if i = "" then
    rr "{""sucess"":false,""driverid"":"""",""token"":""""}"
  else
    token = createpassword(10)
    s = "delete * from misc where type1 = 'drivertoken' and num1 = " & i
    set rs = conn.execute(sqlfilter(s))
    s = "insert into misc(title,num1,date1,type1) values('" & token & "'," & i & "," & sqldate(now) & ",'drivertoken')"
    set rs = conn.execute(sqlfilter(s))
    rr "{""success"":true,""driverid"":""" & i & """,""token"":""" & token & """}"
  end if
  rend
end if

'if NOT itslocal and (getfield("token") = "" or getfield("token") <> session("drivertoken")) then rr "Invalid Token" : rend

x = 0
if itslocal then x = 1
if getfield("action") = "picked" and getfield("backto") = "mob.asp" then x = 1
if x = 0 then
  s = "select id from misc where type1 = 'drivertoken'"
  s = s & " and num1 = 0" & getfield("driverid")
  s = s & " and title = '" & getfield("token") & "'"
  s = s & " and date1 >= " & sqldate(dateadd("h", -24, now))
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "Invalid Token" : rend
end if

'----------------------------------------------
if getfield("action") = "nextorder" then
  if not isnumeric(getfield("driverid")) then rr "E invalid driverid" : rend
  s = "select id,date1,courseid,from1,to1 from orders where driverid = " & getfield("driverid")
  s = s & " and status in('Sent', 'Received')"
  s = s & " order by date1"
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "E order not found" : rend
  rr rs("id") & ","
  t = rs("date1") & " - "
  if cstr(rs("courseid")) = "0" then
    t = t & gettitle("stations", rs("from1")) & " - " & gettitle("stations", rs("to1"))
  else
    t = t & getcode("courses", rs("courseid")) & " " & gettitle("courses", rs("courseid"))
  end if
  rr cutf8(t) : rend
end if

'----------------------------------------------
if getfield("action") = "nextorders" then
  'http://localhost/trans/driver.asp?action=nextorders&driverid=4
  if not isnumeric(getfield("driverid")) then rr "E invalid driverid" : rend
  s = "select top 50 id,date1,courseid,from1,to1,pids,altstations,history from orders where driverid = " & getfield("driverid")
  's = s & " and date1 >= " & sqldate(dateadd("h", -24, now))
  d1 = getfield("d1") : if d1 = "" then d1 = date & " 00:00:00"
  d2 = getfield("d2") : if d2 = "" then d2 = dateadd("d", 1, date) & " 10:00:00"
  s = s & " and date1 >= " & sqldate(d1)
  s = s & " and date1 <= " & sqldate(d2)
  a = "" : tt = getfield("statuses") & "," : if tt = "," then tt = "sent,received,approved,rejected," 
  do until tt = ""
    t = left(tt,instr(tt,",")-1) : tt = mid(tt,instr(tt,",")+1)
    if t <> "" then t = ucase(left(t,1)) & lcase(mid(t,2)) : a = a & ",'" & t & "'"
  loop
  s = s & " and status in(" & mid(a,2) & ")"
  s = s & " order by date1"
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "E No orders found" : rend
  r = ""
  do until rs.eof
    thepids = rs("pids") & ""
    thehistory = rs("history") & ""
    thealtstations = rs("altstations") & ""
	  r = r & rs("id") & ","
	  r = r & rs("date1") & ","
    if hasmatch(specs, "trans,unilever,jerusalem,muni,brener,") then r = r & "*" 'show COMPLETED checkbox
	  if cstr(rs("courseid")) = "0" then
	    r = r & tocsv(gettitle("stations", rs("from1")) & " - " & gettitle("stations", rs("to1"))) & ","
	  else
	    r = r & tocsv(getcode("courses", rs("courseid")) & " " & gettitle("courses", rs("courseid"))) & ","
	  end if
    pp = bothmatch(getval(ctxt(thehistory), "sortworkers"), thepids) & thepids : pp = formatidlistunique(pp)
    pp = nomatch(pp, session("escortworkerids"))
    do until pp = ""
      p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
      s = "select firstname,lastname,phone1,address from workers where id = " & p
      set rs1 = conn.execute(sqlfilter(s))
      if not rs1.eof then
        r = r & p & ","
        r = r & tocsv(rs1("firstname") & " " & rs1("lastname")) & ","
        a = getval(thealtstations, p & "altaddress") : if a = "" then a = rs1("address") & ""
        r = r & tocsv(a) & ","
        r = r & tocsv(rs1("phone1")) & ","
      end if
    loop
    r = r & vbcrlf
    rs.movenext
  loop
  rr cutf8(r) : rend
end if

if getfield("action") = "nextordersjson" then
  'http://localhost/trans/driver.asp?action=nextordersjson&driverid=8
  'http://localhost/trans/driver.asp?action=nextordersjson&driverid=4
  if not isnumeric(getfield("driverid")) then rr "E invalid driverid" : rend
  s = "select top 50 id,date1,courseid,from1,to1,direction,pids,altstations,history,carid,data1 from orders where driverid = " & getfield("driverid")
  's = s & " and date1 >= " & sqldate(dateadd("h", -24, now))

  if FALSE then'(specs = "jerusalem" and instr(siteurl, "/gedera.moovex.net/") = 0) or instr(siteurl, "/givatayim.moovex.net/") > 0 then
    'if hour(now) <= 10 then
    '  s = s & " and date1 >= " & sqldate(date & " 00:00:00")
    '  s = s & " and date1 <= " & sqldate(date & " 10:59:59")
    'else
    '  s = s & " and date1 >= " & sqldate(date & " 11:00:00")
    '  s = s & " and date1 <= " & sqldate(date & " 23:59:59")
    'end if
  else
    s = s & " and date1 >= " & sqldate(date & " 00:00:00")
    s = s & " and date1 <= " & sqldate(dateadd("d", 1, date) & " 23:59:59")
  end if
  s = s & " and status in('Sent', 'Received')"
  s = s & " order by date1"
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "E No orders found" : rend
  r = ""
  r = r & "{""orders"":["
  do until rs.eof
    date1 = rs("date1")
    pids = rs("pids") & ""
    history = rs("history") & ""
    altstations = rs("altstations") & ""
    data1 = rs("data1") & ""
    direction = rs("direction") & ""
    courseid = rs("courseid") & "" : if courseid = "" then courseid = "0"
    from1 = rs("from1") & "" : if from1 = "" then from1 = "0"
    to1 = rs("to1") & "" : if to1 = "" then to1 = "0"
    fromlat = "" : fromlon = "" : tolat = "" : tolon = ""

	  r = r & "{""id"":""" & rs("id") & """"
	  r = r & ",""date"":""" & rs("date1") & """"
    r = r & ",""direction"":""" & tocsv(rs("direction")) & """"

    if courseid <> "0" and (from1 = "0" or to1 = "0") then
      t = courseplan(courseid, date1, pids, altstations, direction, histroy, data1)
      tt = courseplanallstations : from1 = getlistitem(tt,1) : to1 = getlistitem(tt,-1)
    end if


    set rs1 = conn.execute(sqlfilter("select title,markerx,markery from stations where id = 0" & from1))
    if not rs1.eof then
	    r = r & ",""from"":""" & tocsv(rs1("title")) & """"
      r = r & ",""fromlat"":""" & tocsv(rs1("markery")) & """" : fromlat = rs1("markery")
      r = r & ",""fromlon"":""" & tocsv(rs1("markerx")) & """" : fromlon = rs1("markerx")
    end if
    set rs1 = conn.execute(sqlfilter("select title,markerx,markery from stations where id = 0" & to1))
    if not rs1.eof then
      r = r & ",""to"":""" & tocsv(rs1("title")) & """"
      r = r & ",""tolat"":""" & tocsv(rs1("markery")) & """" : tolat = rs1("markery")
      r = r & ",""tolon"":""" & tocsv(rs1("markerx")) & """" : tolon = rs1("markerx")
    end if

    a = ""
    if hasmatch(specs, "jerusalem,muni,brener,dsw,") then
      a = gettitle("courses", rs("courseid"))
      t = getonefield("select data1 from courses where id = 0" & rs("courseid"))
      t = getval(t, "arabicname") : if t <> "" then a = t 'ctxt(t)
    end if
	  r = r & ",""course"":""" & tocsv(getcode("courses", rs("courseid")) & " " & a) & """"
    r = r & ",""car"":""" & tocsv(gettitle("cars", rs("carid"))) & """"

    pp = bothmatch(getval(ctxt(history), "sortworkers"), pids) & pids : pp = formatidlistunique(pp)
    pp = nomatch(pp, session("escortworkerids"))
    r = r & ",""passengers"":["
    didfirst = ""
    do until pp = ""
      p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
      s = "select firstname,lastname,phone1,phone2,address,data1 from workers where id = " & p
      set rs1 = conn.execute(sqlfilter(s))
      if not rs1.eof then
        if didfirst = "on" then r = r & ","
        didfirst = "on"
        t = ""
        workerdata1 = rs1("data1")
        if hasmatch(specs, "jerusalem,muni,brener,") then t = ", ""phoneHome"": ""#12#"", ""phonePerson1"": ""#13#"", ""namePerson1"": ""#14#"", ""phonePerson2"": ""#15#"", ""namePerson2"": ""#16#"", ""arabicname"": ""#17#"", ""arrabicaddress"": ""#18#"""
	      r = r & "{""name"": ""#1#"", ""id"": ""#2#"", ""address"": ""#3#"", ""phone"": ""#4#"", ""lat"": ""#5#"", ""lon"": ""#6#""" & t & "}"
	      r = replace(r, "#1#", tocsv(rs1("firstname") & " " & rs1("lastname")))
	      r = replace(r, "#2#", tocsv(p))
        a = getval(altstations, p & "altaddress") : if a = "" then a = rs1("address") & ""
	      r = replace(r, "#3#", tocsv(a))
	      r = replace(r, "#4#", tocsv(rs1("phone1")))
        t = getval(workerdata1, "y") : if t = "" then if direction = "A" then t = fromlat else t = tolat
        r = replace(r, "#5#", tocsv(t))
        t = getval(workerdata1, "x") : if t = "" then if direction = "A" then t = fromlon else t = tolon
        r = replace(r, "#6#", tocsv(t))
        if hasmatch(specs, "jerusalem,muni,brener,") then
	        r = replace(r, "#12#", tocsv(rs1("phone2")))
	        r = replace(r, "#13#", tocsv(getval(workerdata1, "����� ��� ��� 1")))
	        r = replace(r, "#14#", tocsv(getval(workerdata1, "�� ��� ��� 1")))
	        r = replace(r, "#15#", tocsv(getval(workerdata1, "����� ��� ��� 2")))
	        r = replace(r, "#16#", tocsv(getval(workerdata1, "�� ��� ��� 2")))
          r = replace(r, "#17#", tocsv(getval(workerdata1, "�� ������")))
          r = replace(r, "#18#", tocsv(getval(workerdata1, "����� ������")))
        end if
      end if
    loop
    r = r & "]}"
    rs.movenext
    if not rs.eof then r = r & ","
  loop
  r = r & "]}"
  rr cutf8(r) : rend
end if

'---jerusalem get workerid from driverid (yishut shel oved..)
if getfield("action") = "getworkerid" then
  i = getonefield("select top 1 id from workers where code = '" & getfield("code") & "'")
  rr i
  rend
end if

'----------------------------------------------
if getfield("action") = "picked" or getfield("action") = "completed" or getfield("action") = "noshow" then
  if not isnumeric(getfield("orderid")) then rr "E Invalid order ID" : rend
  s = "select id,pids,history from orders where id = 0" & getfield("orderid")
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "E Invalid order ID" : rend
  h = rs("history") & ""
  pids = rs("pids") & ""

  w = getfield("workerid") : n = getfield("nfc") : n = replace(n, " ", "")
  if w = "" and n <> "" then
    w = getonefield("select id from workers where instr('' & cstr('' & workers.data1), '|nfcid=" & n & "|') > 0")
    h = setval(h, w & getfield("action") & "by" , "nfc")
  end if
  v = getfield("v") : if v <> "no" then v = "on"
  w = getonefield("select id from workers where id = 0" & w)
  if w <> "" then
	  h = setval(h, w & getfield("action"), v)
	  h = setval(h, w & getfield("action") & "time" , now)
	  s = "update orders set history = '" & h & "' where id = " & getfield("orderid")
	  set rs = conn.execute(sqlfilter(s))
	  rr "OK " & w
  else
    rr "E Worker not found"
  end if
  if getfield("backto") <> "" then rred getfield("backto")
  rend
end if

'----------------------------------------------
if specs = "jerusalem" and getfield("action") = "distress" then
  'driver.asp?action=distress&orderid=41414&description=444
  t = getonefield("select supplierid from orders where id = 0" & getfield("orderid"))
  t = getonefield("select data1 from suppliers where id = 0" & t)
  t = getval(t, "rakaz")
  t = getonefield("select phone from buusers where username = '" & t & "'")
  m = "���� ����� ����� ������ " & getfield("orderid") & " " & getfield("description")
  du = sendsms(getparam("sitephone"), t, m)
  deb t
end if

if hasmatch(getfield("action"), "beginride,endride,distress,") then
  if not isnumeric(getfield("driverid")) then rr "E invalid driverid" : rend
  if not isnumeric(getfield("orderid")) then rr "E invalid orderid" : rend
  s = "select id,pids,history from orders where id = " & getfield("orderid") & " and driverid = " & getfield("driverid")
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "E order not found" : rend
  h = rs("history")
  h = setval(h, getfield("action"), now)
  if getfield("action") = "beginride" then
    pp = rs("pids") : pp = bothmatch(pp, session("escortworkerids"))
    do until pp = ""
      p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
      h = setval(h, p & "picked", "on")
      h = setval(h, p & "pickedtime", now)
    loop
  end if
  t = "" : if getfield("action") = "endride" then t = ",status='Done'"
  s = "update orders set history = '" & h & "' " & t & " where id = " & getfield("orderid")
  set rs = conn.execute(sqlfilter(s))
  rr "OK" : rend
end if

'----------------------------------------------
if getfield("action") = "mylocation" then
  'if not isnumeric(getfield("driverid")) then rr "E invalid driverid" : rend
  if not isnumeric(getfield("orderid")) then rr "E invalid orderid" : rend
  if not isnumeric(getfield("lat")) then rr "E invalid lat" : rend
  if not isnumeric(getfield("lon")) then rr "E invalid lon" : rend
  s = "insert into driverlocations(orderid, date1, lat, lon) values("
  s = s & getfield("orderid") & ","
  s = s & sqldate(now) & ","
  s = s & getfield("lat") & ","
  s = s & getfield("lon") & ")"
  set rs = conn.execute(sqlfilter(s))
  rr "OK<br>"

  '---send sms
  if specs = "jerusalem" then
    s = "select pids,history,altstations,direction from orders where id = 0" & getfield("orderid")
    set rs = conn.execute(sqlfilter(s))
    history = rs("history") : alerted = getval(history, "alerted")
    pp = rs("pids") ': if rs("direction") = "B" then pp = ""
    altstations = rs("altstatoins") & ""
    do until pp = ""
      p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
      if NOT hasmatch(alerted, p) then
        s = "select data1,from1 from workers where id = 0" & p
        set rs = conn.execute(sqlfilter(s))
        if not rs.eof then
          data1 = rs("data1") : phone = getval(data1, "����� ��� ��� 1") : if not isphone(phone) then phone = getval(data1, "����� ��� ��� 2")
          from1 = getval(altstations, p & "from1") : if not isnumeric(from1) then from1 = rs("from1")
          lat = "" : lon = ""
          s = "select markery, markerx from stations where id = 0" & from1
          set rs = conn.execute(sqlfilter(s))
          if not rs.eof then lat = rs("markery") : lon = rs("markerx")
          if isphone(phone) and isnumeric(lat) and isnumeric(lon) then
            a = getleginfo(getfield("lat") & "," & getfield("lon") & "," & lat & "," & lon & ",")
            x = getval(a, "legdura") : if not isnumeric(x) then x = "9999999"
            if cdbl(x) <= 600 then
              m = "����� ���� ����� �� ���� ���� ���� ����"
              du = sendsms(getparam("sitephone"), phone, m)
              alerted = alerted & p & ","
              history = setval(history, "alerted", alerted)
              s = "update orders set history = '" & history & "' where id = 0" & getfield("orderid")
              set rs = conn.execute(sqlfilter(s))
              rr "sms sent to " & getworkername(p) & "<br>"
            end if
          end if
        end if
      end if
    loop
  end if
  rend
end if

'----------------------------------------------
if getfield("action") = "markstation" then
  if not isnumeric(getfield("orderid")) then rr "E invalid orderid" : rend
  if not isnumeric(getfield("lat")) then rr "E invalid lat" : rend
  if not isnumeric(getfield("lon")) then rr "E invalid lon" : rend
  s = "insert into driverlocations(orderid, date1, lat, lon) values("
  s = s & "-" & getfield("orderid") & ","
  s = s & sqldate(now) & ","
  s = s & getfield("lat") & ","
  s = s & getfield("lon") & ")"
  set rs = conn.execute(sqlfilter(s))
  rr "OK" : rend
end if

'----------------------------------------------
if getfield("action") = "getlocation" then
  'http://localhost/trans/driver.asp?action=getlocation&orderid=8264
  rclear
  response.ContentType = "text/json"
  s = "select lat,lon from driverlocations where orderid = 0" & getfield("orderid") & " and lat <> 0 and lon <> 0 order by date1 desc"
  set rs = conn.execute(sqlfilter(s))
  if not rs.eof then rr "{""lat"":" & rs("lat") & ",""lng"":" & rs("lon") & "}"
  rend
end if

'----------------------------------------------
if getfield("action") = "getcourseplan" then
  'http://localhost/trans/driver.asp?action=getcourseplan&orderid=10819
  rclear
  response.ContentType = "text/json"
  s = "select date1,direction,history,pids,altstations from orders where id = 0" & getfield("orderid")
  set rs = conn.execute(sqlfilter(s))
  r = ""
  if not rs.eof then
    date1 = rs("date1")
    pids = rs("pids")
    history = rs("history")
    direction = rs("direction")
    altstations = rs("altstations")
    if getval(history, "stationcourse") = "" then history = convert2geo(getfield("orderid"))
    d = rs("date1")
    ii = getval(history, "stationcourse")
    du = courseplantaxi(date1, history, pids, direction, altstations)

    '{"stations": [{"name": "111", "lat": 222, "lng": 333}, {"name": "111", "lat": 222, "lng": 333}, {"name": "111", "lat": 222, "lng": 333}]}
    c = 0 : r = ""
    t = "false" : if getval(history, "beginride") <> "" then t = "true"
    r = r & "{""beginride"": """ & t & """"
    r = r & ",""stations"": ["
    do until ii = ""
      i = left(ii,instr(ii,",")-1) : ii = mid(ii,instr(ii,",")+1)
      c = c + 1
      s = "select id,title,markerx,markery from stations where id = 0" & i
      set rs1 = conn.execute(sqlfilter(s))
      if not rs1.eof then
        'r = r & tocsv(rs1("title")) & ","
        'r = r & tocsv(rs1("markery")) & ","
        'r = r & tocsv(rs1("markerx")) & ","
        r = r & "{""name"": ""#1#"", ""lat"": ""#2#"", ""lng"": ""#3#"", ""timeAtStation"": ""#4#"", ""id"": ""#5#"", ""on"": [#6#], ""off"": [#7#]}" : if ii <> "" then r = r & ","
        r = replace(r, "#1#", tocsv(rs1("title")))
        r = replace(r, "#2#", tocsv(rs1("markery")))
        r = replace(r, "#3#", tocsv(rs1("markerx")))
        r = replace(r, "#4#", tocsv(datediff("s", "1/1/1970 00:00:00", d)))
        r = replace(r, "#5#", tocsv(rs1("id")))
        jj = rs("pids") : jj = nomatch(jj, session("escortworkerids")) : a1 = "" : a2 = ""
        do until jj = ""
          j = left(jj,instr(jj,",")-1) : jj = mid(jj,instr(jj,",")+1)
          if getval(courseplantaxibuf, j & "on") = cstr(rs1("id")) then a1 = a1 & ",""" & j & """"
          if getval(courseplantaxibuf, j & "off") = cstr(rs1("id")) then a2 = a2 & ",""" & j & """"
        loop
        if a1 <> "" then a1 = mid(a1,2)
        if a2 <> "" then a2 = mid(a2,2)
        r = replace(r, "#6#", a1)
        r = replace(r, "#7#", a2)
      end if
      x = "0" & getval(history, "legdur" & c)
      d = dateadd("s", x, d)
    loop
    r = r & "]"
    r = r & ",""passengers"": ["
    jj = rs("pids")
    tt = getval(courseplantaxibuf, "pp") : jj = uniquelist(tt & jj)
    jj = nomatch(jj, session("escortworkerids"))
    do until jj = ""
      j = left(jj,instr(jj,",")-1) : jj = mid(jj,instr(jj,",")+1)
      t = "" : if specs = "jerusalem" then t = ",""phone"": ""#PHONE#"", ""nfc"": ""#NFC#"""
      r = r & "{""name"": ""#1#"", ""id"": ""#2#"", ""on"": ""#3#"", ""off"": ""#4#"", ""status"": ""#5#""" & t & "}" : if jj <> "" then r = r & ","
      r = replace(r, "#1#", tocsv(getworkername(j)))
      r = replace(r, "#2#", tocsv(j))
      t = getval(courseplantaxibuf, j & "on") : if t = "" then t = "-1"
      r = replace(r, "#3#", tocsv(t))
      t = getval(courseplantaxibuf, j & "off") : if t = "" then t = "-1"
      r = replace(r, "#4#", tocsv(t))
      t = ""
      if getval(history, j & "picked") = "on" then t = "picked"
      if getval(history, j & "noshow") = "on" then t = "noshow"
      if getval(history, j & "completed") = "on" then t = "completed"
      r = replace(r, "#5#", tocsv(t))
      if specs = "jerusalem" then
        d = getonefield("select data1 from workers where id = 0" & j)
        r = replace(r, "#PHONE#", tocsv(getval(d,"����� ��� ��� 1")))
        r = replace(r, "#NFC#", tocsv(getval(d,"nfcid")))
      end if
    loop
    r = r & "]"
    r = r & "}"
  end if
  r = cutf8(r)
  rr r : rend
end if

'----------------------------------------------
if getfield("action") = "createworkerstation" then
  workerid = getonefield("select id from workers where id = 0" & getfield("workerid"))
  n = getxyaddress(getfield("lat"),getfield("lon")) : n = chtm(n)
  if workerid = "" then
    rr "Erorr - worker does not exist"
  elseif n = "" then
    rr "Erorr - this geo location does not interpet"
  else
    stid = getonefield("select id from stations where title = '" & n & "'")
    if stid <> "" then
      s = "update workers set from1 = " & stid & " where id = " & getfield("workerid")
      set rs1 = conn.execute(sqlfilter(s))
      rr "OK, worker set to existing station '" & n & "', id " & stid
    else
      s = "insert into stations(title,inmap,special,siteid,markerx,markery,data1) values("
      s = s & "'" & n & "',"
      s = s & "'',"
      s = s & "'',"
      s = s & "0,"
      s = s & "'" & getfield("lon") & "',"
      s = s & "'" & getfield("lat") & "',"
      s = s & "'')"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
      s = "select max(id) as id from stations"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
      stid = rs1("id")
      s = "update workers set from1 = " & stid & " where id = " & getfield("workerid")
      set rs1 = conn.execute(sqlfilter(s))
      rr "OK, created new station '" & n & "', id " & stid
    end if
  end if
  rend
end if
'----------------------------------------------
if getfield("action") = "abortride" then
  s = "select pids,history from orders where id = 0" & getfield("orderid") & " and driverid = 0" & getfield("driverid")
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then
    rr "Error - cannot find this order for this driver"
  else
    h = rs("history") & ""
    pp = rs("pids") & ""
    do until pp = ""
      p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
      h = setval(h, p & "picked", "")
      h = setval(h, p & "pickedtime", "")
      h = setval(h, p & "completed", "")
      h = setval(h, p & "completedtime", "")
    loop
    s = "update orders set history = '" & h & "' where id = " & getfield("orderid")
    set rs = conn.execute(sqlfilter(s))
    s = "delete * from driverlocations where orderid = " & getfield("orderid")
    set rs = conn.execute(sqlfilter(s))
    rr "OK - passenger on/off info discarded"
  end if
  rend
end if
'-------------------------------
if getfield("action") = "absent" then
  'http://127.0.0.1/trans/driver.asp?orderid=41357&driverid=357&workerid=7191&action=absent&title=AAA&description=*1495;*1493;*1500;*1492;1234
  driverid = getfield("driverid") : if driverid = "" and getfield("driveridn") <> "" then driverid = getonefield("select id from drivers where idn = '" & getfield("driveridn") & "'")
  w = getfield("workerid")
  s = "select pids,history from orders where id = 0" & getfield("orderid") & " and driverid = 0" & driverid
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then
    rr "Error - cannot find this order for this driver"
  elseif w = "" or strfilter(w, "0123456789") <> w then
    rr "Error - invalid workerid"
  elseif NOT hasmatch(rs("pids"), w) then
    rr "Error - worker is not on this ride"
  else
    h = rs("history") & ""
    t = getfield("title") : t = replace(t, "*", "&#")
    h = setval(h, w & "absent1", t)
    t = getfield("description") : t = replace(t, "*", "&#")
    h = setval(h, w & "absent2", t)
    h = setval(h, w & "picked", "")
    h = setval(h, w & "pickedtime", "")
    h = setval(h, w & "completed", "")
    h = setval(h, w & "completedtime", "")
    s = "update orders set history = '" & h & "' where id = " & getfield("orderid")
    set rs = conn.execute(sqlfilter(s))
    rr "OK - absent info added for passenger."
  end if
  rend
end if

'-----------------------------updatedriverdetails
if getfield("action") = "updatedriverdetails" and getfield("id") <> "" then
  'driver.asp?action=updatedriverdetails&id=8&title=����+�����&phone=1122&email=3344
  a = ""
  if getfield("title") <> "" then a = a & ",title='" & replace(getfield("title"), "*", "&#") & "'"
  if getfield("phone") <> "" then a = a & ",phone='" & replace(getfield("phone"), "*", "&#") & "'"
  if getfield("email") <> "" then a = a & ",email='" & replace(getfield("email"), "*", "&#") & "'"
  if a <> "" then
    s = "update drivers set " & mid(a,2) & " where id = " & getfield("id")
    set rs = conn.execute(sqlfilter(s))
    rr "OK" : rend
  end if
end if

'-------------getdriverdetails
'driver.asp?action=getdriverdetails&driverid=123
if getfield("action") = "getdriverdetails" then
  s = "select title,phone,email from drivers where id = 0" & getfield("driverid")
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "E invalid driverid" : rend
  r = "{""driverid"":""#driverid#"", ""phone"":""#phone#"", ""email"":""#email#"", ""firstname"":""#firstname#"",""lastname"":""#lastname#""}"
  t = rs("title") & " " : firstname = trim(left(t,instr(t," ")-1)) : lastname = trim(mid(t, instr(t," ")+1))
  r = replace(r, "#driverid#", getfield("driverid"))
  r = replace(r, "#phone#", tocsv(rs("phone")))
  r = replace(r, "#email#", tocsv(rs("email")))
  r = replace(r, "#firstname#", tocsv(firstname))
  r = replace(r, "#lastname#", tocsv(lastname))
  rclear
  response.ContentType = "text/json"
  rr cutf8(r)
  rend
end if


'---------------------------
'driver.asp?action=findpassenger&s=coh
if getfield("action") = "findpassenger" then
  s = "select id,firstname,lastname,code from workers where instr(firstname & ' ' & lastname & ' ' & code, '" & getfield("s") & "') > 0"
  set rs = conn.execute(sqlfilter(s))
  r = ""
  r = r & "["
  do until rs.eof
    r = r & "{""id"": """ & rs("id") & """, ""name"": """ & tocsv(rs("firstname")) & " " & tocsv(rs("lastname")) & " " & tocsv(rs("code")) & """}"
    rs.movenext
    if not rs.eof then r = r & ","
  loop
  r = r & "]"
  rr cutf8(r)
  rend
end if

'--------------------------------
'driver.asp?action=addpassenger&orderid=239942&driverid=432&workerid=7066&stationid=123
'station is not mandatory
if getfield("action") = "addpassenger" then

  s = "select id,direction,from1,to1,pids,altstations from orders where id = " & getfield("orderid") & " and driverid = " & getfield("driverid")
  set rs = conn.execute(sqlfilter(s))
  if rs.eof then rr "E order not found for this driver" : rend

  i = getonefield("select id from workers where id = 0" & getfield("workerid"))
  if i = "" then rr "E workerid not found" : rend

  stid = getfield("stationid")
  if stid = "" then stid = getonefield("select from1 from workers where id = " & getfield("workerid"))

  i = getonefield("select id from stations where id = 0" & stid)
  if i = "" then rr "E stationid not found" : rend

  pids = rs("pids") : pids = uniquelist(pids & getfield("workerid") & ",")
  altstations = rs("altstations")
  if rs("direction") = "A" then
    altstations = setval(altstations, getfield("workerid") & "from1", getfield("stationid"))
    altstations = setval(altstations, getfield("workerid") & "to1", rs("to1"))
  else
    altstations = setval(altstations, getfield("workerid") & "from1", getfield("stationid"))
    altstations = setval(altstations, getfield("workerid") & "to1", rs("from1"))
  end if
  s = "update orders set pids = '" & pids & "', altstations = '" & altstations & "' where id = " & getfield("orderid")
  set rs = conn.execute(sqlfilter(s))
  du = getorderstationcourse(getfield("orderid"))
  rr "OK" : rend
end if
'-----------------------------------
'driver.asp?action=driverevent&driverid=16&description=hello&imageurl=1.gif&orderid=11023
if getfield("action") = "driverevent" then
  driver = getonefield("select title from drivers where id = 0" & getfield("driverid"))
  if driver = "" then rr "E driverid not found" : rend
  s = "insert into misc(type1,num1,date1,title,file1,contents) values"
  s = s & "('driverevent'"
  s = s & ",0" & getfield("orderid")
  s = s & "," & sqldate(now)
  s = s & ",'" & tocsv(driver) & "'"
  s = s & ",'" & getfield("imageurl") & "'"
  s = s & ",'" & getfield("description") & "')"
  set rs = conn.execute(sqlfilter(s))
  rr "OK" : rend
end if


%>


