<html><meta http-equiv=""X-UA-Compatible"" content=""IE=11"" />
<link href="http://code.google.com/apis/maps/documentation/javascript/examples/standard.css" rel="stylesheet" type="text/css" />
<style type="text/css">
BODY { font-family: Verdana, Tahoma, Arial, sans-serif;font-size: 11px;margin: 0px;padding: 0px;color: #000; }
#container {
	padding:8px 0px 8px 5px;
	vertical-align:middle;
	background-color:#FFFFFF;
}
#ipbwrapper { text-align: left;width: 100%;margin-left: auto;margin-right: auto; }
#macroslist {background-color:#FFFFFF}
#logostrip { padding: 0px;margin: 0px; height: 52px; background-color: #FFFFFF;border: 1px solid #345487;background-image: url(http://gsak.net/board/style_images/1/tile_back.gif); }
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
}
.maintitle {
background-image:url(http://gsak.net/board/style_images/1/tile_back.gif);
color:#FFFFFF;
font-weight:bold;
padding:8px 0px 8px 5px;
vertical-align:middle;
}
.logotitle {
color:#FFFFFF;
font-weight:bold;
position: relative;
left: 80px;
top: -12px;
font-size: 24px;
}
.topic A:link{
	color:#4466bb;
	font-weight:bold;
	vertical-align:middle;
	text-decoration: none;
}
.topic A:visited {
	color:#4466bb;
	font-weight:bold;
	vertical-align:middle;
	text-decoration: none;
}
.topic A:active {text-decoration: none}
.topic A:hover {
	text-decoration: underline;
	color: white;
	background: #FFFFFF;
 }
pre {
	background1-color: #FFFFFF;
}
.style3 {
	font-size: 10px;
	font-style: italic;
}
</style>

<! HEBREW: &allow_bidi=true >
<% t = "" : if session("elang") = "2" then t = "&allow_bidi=true" %>
<script src="http://maps.google.com/maps?file=api&amp;v=2.x&language=iw&key=ABQIAAAAW1ZRKj6gcuzT61BaHVlesxRAhDohLv5jwM4Pix1t8mvLBb1pGxRl79Ile7CKFC6CQLXS5BlvCuylhg<%=t%>" type="text/javascript"></script>
<script type="text/javascript">
var COLORS = [["red", "#ff0000"], ["green","#008000"],["blue", "#000080"], ["purple", "#800080"], ["orange", "#ff8800"]];
var options = {};
var lineCounter_ = 0;
var shapeCounter_ = 0;
var markerCounter_ = 0;
var colorIndex_ = 0;
var featureTable_;
var map;
var outputFileMode = 'csv';
var geocoder = null;

var fillColor = "#0000FF"; // blue fill
var lineColor = "#FF00FF"; // black line
var opacity = .5;
var lineWeight = 3;

var currpoly = [];
var poly = [];

MyTopoInit();

var WMS_TOPO_MAP = WMSCreateMap( 'Topo', 'USGS Topographic', 'Imagery by USGS / Service by TerraServer', 'http://www.terraserver-usa.com/ogcmap6.ashx', 'DRG', 'image/jpeg', false, 1.0, 4, 17, [], 't' );
//var NRCAN_TOPO_TILES = WMSCreateMap('Cdn', 'Canadian Topographic', 'Maps by NRCan.gc.ca','http://wms.cits.rncan.gc.ca/cgi-bin/cubeserv.cgi', 'PUB_50K:CARTES_MATRICIELLES/RASTER_MAPS', 'image/png', false, 1.0, 6, 17, [], 't' );
var MYTOPO_MAP = new GMapType(mytopoLayer, G_SATELLITE_MAP.getProjection(), "MyTopo", G_PHYSICAL_MAP);

function WMSCreateMap( name, tip, copyright, baseUrl, layer, format, transparent, opacity, minResolution, maxResolution, extraTileLayers, urlArg )
    {
    var tileLayer = new GTileLayer( new GCopyrightCollection( copyright ), minResolution, maxResolution );
    tileLayer.baseUrl = baseUrl;
    tileLayer.layer = layer;
    tileLayer.format = format;
    tileLayer.transparent = transparent;
    tileLayer.getTileUrl = WMSGetTileUrl;
    tileLayer.getOpacity = function () { return opacity; };
    tileLayer.getCopyright = function () { return { prefix: '', copyrightTexts: [ copyright ] }; };
    var tileLayers = [];
    for ( var i in extraTileLayers )
   tileLayers.push( extraTileLayers[i] );
    tileLayers.push( tileLayer );
    return new GMapType( tileLayers, G_SATELLITE_MAP.getProjection(), name, { urlArg: 'o', alt: tip} );
    }


function WMSGetTileUrl( tile, zoom )
    {
    var southWestPixel = new GPoint( tile.x * 256, ( tile.y + 1 ) * 256 );
    var northEastPixel = new GPoint( ( tile.x + 1 ) * 256, tile.y * 256 );
    var southWestCoords = G_NORMAL_MAP.getProjection().fromPixelToLatLng( southWestPixel, zoom );
    var northEastCoords = G_NORMAL_MAP.getProjection().fromPixelToLatLng( northEastPixel, zoom );
    var bbox = southWestCoords.lng() + ',' + southWestCoords.lat() + ',' + northEastCoords.lng() + ',' + northEastCoords.lat();
    var transparency = this.transparent ? '&TRANSPARENT=TRUE' : '';
    return this.baseUrl + '?VERSION=1.1.1&REQUEST=GetMap&LAYERS=' + this.layer + '&STYLES=&SRS=EPSG:4326&BBOX=' + bbox + '&WIDTH=256&HEIGHT=256&FORMAT=' + this.format + '&BGCOLOR=0xFFFFFF&EXCEPTIONS=INIMAGE' + transparency;
    }

function MyTopoInit()
{
GetMyTopoTile=function(a,b,c) {
	var lURL=this.myBaseURL;
	lURL+=b;
	lURL+="/"+a.x;
	lURL+="/"+a.y+".png";
	return lURL;
	}

var mytopoCopy = new GCopyright(1,new GLatLngBounds(new GLatLng(-90, -180), new GLatLng(90, 180)),0,'Topo maps (c) MyTopo.com')
var mytopoc = new GCopyrightCollection('');
mytopoc.addCopyright(mytopoCopy);
		
var tileMytopo = new GTileLayer(mytopoc,6,16);
tileMytopo.myLayers='topoG';
tileMytopo.myFormat='image/png';
tileMytopo.myBaseURL='http://maps.mytopo.com/mytopo/tilecache.py/1.0.0/topoG/';

tileMytopo.getTileUrl=GetMyTopoTile;
		
mytopoLayer=[tileMytopo];
}

function getColor(named) {
  return COLORS[(colorIndex_++) % COLORS.length][named ? 0 : 1];
}


function startShape() {
  //select("shape_b");
  var color = getColor(false);
  var poly = new GPolygon([], color, 2, 0.7, "#0000FF", 0.2);
  startDrawing(poly, "Polygon " + (++shapeCounter_), function() {
    logCoordinates(poly);
    var cell = this;
    cell.innerHTML = " (" + poly.getVertexCount() + " points)";
  }, color);
}


function editShape() {
  <%
  a = "Polygon\n"
  if scriptname = "admin_areas.asp" then
    sq = "select * from polygons where areaid = " & request_id & " order by id desc"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      a = a & rs1("latitude") & "," & rs1("longitude") & "\n"
      rs1.movenext
    loop
  end if
  %>

  var points = [];
  var a = '<%=a%>';
  pointsLines = a.split(/ |\n/);
  var poly = new GPolygon([], "#FF0000", 2, 0.7, "#FF0000", 0.2);
  map.addOverlay(poly);

  //Read in coordinates

	for ( var i=0, len=pointsLines.length; i<len; ++i ){
		if (pointsLines[i].search(/,|\t/) != -1) {
			pointsCoords = pointsLines[i].split(/,|\t/);
			if (parseFloat(pointsCoords[0]) != 0 && parseFloat(pointsCoords[1]) != 0) {
				poly.insertVertex(0, new GLatLng(parseFloat(pointsCoords[0]),parseFloat(pointsCoords[1])));
	}}}

  var name = "Polygon <%=request_id%>";
  var cells = addFeatureEntry(name, "#FF0000");

  poly.enableEditing({onEvent: "mouseover"});
  poly.disableEditing({onEvent: "mouseout"});
  poly.Nametag = name;
  
  currpoly = poly;
  logCoordinates(poly);
  cells.desc.innerHTML = " (" + poly.getVertexCount() + " points)";
  GEvent.bind(poly, "lineupdated", cells.desc, function() {
  	logCoordinates(poly);
  	var cell = this;
  	cell.innerHTML = " (" + poly.getVertexCount() + " points)";
  	});
  GEvent.addListener(poly, "click", function(latlng, index) {
  	currpoly = poly;
    if (typeof index == "number") {
      poly.deleteVertex(index);
    } else {
      //poly.setFillStyle({color: "#0000FF"});
      poly.setStrokeStyle({weight: 2});
      //poly.redraw(true);
      cells.desc.innerHTML = " (" + poly.getVertexCount() + " points)";
    }
    });
}


function addFeatureEntry(name, color) {
  currentRow_ = document.createElement("tr");
  var colorCell = document.createElement("td");
  currentRow_.appendChild(colorCell);
  colorCell.style.backgroundColor = color;
  colorCell.style.width = "1em";
  var nameCell = document.createElement("td");
  currentRow_.appendChild(nameCell);
  nameCell.innerHTML = name;
  var descriptionCell = document.createElement("td");
  currentRow_.appendChild(descriptionCell);
  featureTable_.appendChild(currentRow_);
  return {desc: descriptionCell, color: colorCell};
}

function startDrawing(poly, name, onUpdate, color) {
	poly.Nametag = name;
  map.addOverlay(poly);
  currpoly = poly;
  var cells = addFeatureEntry(name, color);
  poly.enableDrawing(options);
  poly.enableEditing({onEvent: "mouseover"});
  poly.disableEditing({onEvent: "mouseout"});
        
  GEvent.addListener(poly, "endline", function() {
    currpoly = poly;
    logCoordinates(poly);
    cells.desc.innerHTML = " (" + poly.getVertexCount() + " points)";
    GEvent.bind(poly, "lineupdated", cells.desc, onUpdate);
    GEvent.addListener(poly, "click", function(latlng, index) {
    	currpoly = poly;
      if (typeof index == "number") {
        poly.deleteVertex(index);
      } else {
        //poly.setFillStyle({color: "#0000FF"});
        poly.setStrokeStyle({weight: 2});
        //poly.redraw(true);
        cells.desc.innerHTML = " (" + poly.getVertexCount() + " points)";
      }
    });
  });
}

function continueDraw() {
	currpoly.deleteVertex((currpoly.getVertexCount()) - 1);
	currpoly.enableDrawing(options);
}

function getUrlVars() {
	var gets = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		gets[key] = value;
	});
	return gets;
}

function initialize() {
  if (GBrowserIsCompatible()) {
    map = new GMap2(document.getElementById("map"));

    // Add map types //
		map.addMapType(G_PHYSICAL_MAP);
		//map.addMapType( WMS_TOPO_MAP );
		//map.addMapType( NRCAN_TOPO_TILES );
		//map.addMapType(MYTOPO_MAP);

<%

  '���� ���� �� ����� ����� �� �����. �� �� ���� ����� �����, �� �� �� ���� ������� ���� �� ��� ���� 
  if request("action2") = "find" and request("searchaddress") <> "" then
    searchgeo = 0
    set objHttp = CreateObject("Msxml2.ServerXMLHTTP")
    murl ="http://maps.googleapis.com/maps/api/geocode/xml?"
    murl = murl & "address=" & request("searchaddress")
    murl = murl & "&sensor=true"
    murl = murl & "&language=iw"
    objHttp.open "GET",murl, false
    objHttp.setRequestHeader "charset","windows-1255"
    objHttp.Send
    a = objHttp.responsetext
    if instr(a,"<status>OK</status>") = 0 then
      rr "alert(""Search Address Not Found"");"
    else
      zoomlevel = 15
      latitude = getfromto(a,"","<lat>","</lat>",false,false)
      longitude = getfromto(a,"","<lng>","</lng>",false,false)
      searchgeo = 1
      point1 = ""
      point1 = point1 & "var point1 = new GLatLng(" & latitude & "," & longitude & ");"
      point1 = point1 & "marker = new GMarker(point1,{draggable:true, autoPan:true});"
      point1 = point1 & "map.addOverlay(marker);"
      'point1 = point1 & "f1.markerx.value = marker.getLatLng().lng();"
      'point1 = point1 & "f1.markery.value = marker.getLatLng().lat();"
    end if

  elseif themarkerx <> "" and themarkery <> "" then
    point1 = ""
    point1 = point1 & "var point1 = new GLatLng(" & themarkery & "," & themarkerx & ");"
    point1 = point1 & "marker = new GMarker(point1,{draggable:true, autoPan:true});"
    point1 = point1 & "map.addOverlay(marker);"
    zoomlevel = 13
    latitude = themarkery
    longitude = themarkerx

  elseif scriptname = "admin_areas.asp" then
    sq = "select * from polygons where areaid = " & request_id & " order by id"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    if not rs1.eof then
      zoomlevel = rs1("zoomlevel")
      latitude = rs1("latitude")
      longitude = rs1("longitude")
      havedata = 1
    end if
  end if
  if zoomlevel = "" then
    zoomlevel = 13

    'DEFAULT MAP LOCATION: tel aviv
    latitude = 32.074284
    longitude = 34.810181
    if getparam("MapDefaultLocation") <> "" then
      set objHttp = CreateObject("Msxml2.ServerXMLHTTP")
      murl ="http://maps.googleapis.com/maps/api/geocode/xml?"
      murl = murl & "address=" & getparam("MapDefaultLocation")
      murl = murl & "&sensor=true"
      murl = murl & "&language=iw"
      objHttp.open "GET",murl, false
      objHttp.setRequestHeader "charset","windows-1255"
      objHttp.Send
      a = objHttp.responsetext
      if instr(a,"<status>OK</status>") > 0 then
        latitude = getfromto(a,"","<lat>","</lat>",false,false)
        longitude = getfromto(a,"","<lng>","</lng>",false,false)
      end if
    end if


  end if

%>
    map.setCenter(new GLatLng(<%=latitude%>,<%=longitude%>), <%=zoomlevel%>);
    map.Language = 'iw';

    // Get bounds from URL if present
    var getbounds = getUrlVars();
    var bounds = new GLatLngBounds();
    
    if((getbounds['minlat'] != undefined) && (getbounds['minlon'] != undefined) && (getbounds['maxlat'] != undefined) && (getbounds['maxlon'] != undefined)) {
    	var minpoint = new GLatLng(getbounds['minlat'],getbounds['minlon']);
    	bounds.extend(minpoint);
    	var maxpoint = new GLatLng(getbounds['maxlat'],getbounds['maxlon']);
    	bounds.extend(maxpoint);
    	map.setZoom(map.getBoundsZoomLevel(bounds));
    	map.setCenter(bounds.getCenter());
     }
        
    //map.addControl(new GSmallMapControl());
    //map.addControl(new GLargeMapControl3D());
    //map.addControl(new GMapTypeControl());
    //map.addControl(new GHierarchicalMapTypeControl(true));

    //map.enableScrollWheelZoom();
	  map.addControl(new GScaleControl());
    map.addControl(new GMapTypeControl());
    map.addControl(new GSmallMapControl());

    map.clearOverlays();
    geocoder = new GClientGeocoder();
    featureTable_ = document.getElementById("featuretbody");
    <%rr point1%>
    <%if havedata = 1 and searchgeo <> 1 then%>editShape();<%end if%>
    drawothers();
  }
}

function drawothers(){
  <%'------------show other polygons in blue------------------
  t = "select id from areas" : if scriptname = "admin_areas.asp" and searchgeo = 0 then t = t & " where id <> " & request_id
  pp = getidlist(t)
  do until pp = ""
    p = left(pp,instr(pp,",")-1) : pp = mid(pp,instr(pp,",")+1)
    a = "Polygon\n"
    sq = "select * from polygons where areaid = " & p & " order by id desc"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      a = a & rs1("latitude") & "," & rs1("longitude") & "\n"
      rs1.movenext
    loop
    rr "  a = '" & a & "'; var points" & p & " = []; pointsLines = a.split(/ |\n/);"
    rr "  var poly" & p & " = new GPolygon([], ""#0000FF"", 1, 1, ""#0000FF"", 0.1); map.addOverlay(poly" & p & ");"
    rr "	for ( var i=0, len=pointsLines.length; i<len; ++i ){"
    rr "    if (pointsLines[i].search(/,|\t/) != -1) {"
    rr "  	  pointsCoords = pointsLines[i].split(/,|\t/);"
    rr "		  if (parseFloat(pointsCoords[0]) != 0 && parseFloat(pointsCoords[1]) != 0) {"
    rr "			  poly" & p & ".insertVertex(0, new GLatLng(parseFloat(pointsCoords[0]),parseFloat(pointsCoords[1])));"
    rr "  }}}"
  loop

  if scriptname = "admin_areas.asp" then
    sq = "select title,markerx,markery from stations where areaid = " & request_id & " and markerx <> '' and markery <> ''"
    set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr "var point1 = new GLatLng(" & rs1("markery") & "," & rs1("markerx") & ");"
      rr "marker = new GMarker(point1,{title:""" & chtm(rs1("title")) & """});"
      rr "map.addOverlay(marker);"
      rs1.movenext
    loop
  end if

  '--------------------------------------------------------------%>
}


function logCoordinates(poly){
	var currentPoly = document.getElementById("activepoly");
	//currentPoly.innerHTML = '<b>' + poly.Nametag + '</b>'; 
 	var coordtext = "";
 	//alert(outputFileMode);
 	
	if (outputFileMode == 'gpx') {
		var header = "&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;\n" +
		"&lt;gpx version=\"1.1\" creator=\"http://gsak.net\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.topografix.com/GPX/1/1\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\"&gt;\n" +
		"&lt;name&gt;Region name&lt;/name&gt;&lt;description&gt;Region description&lt;/description&gt;\n" +
		"&lt;rte&gt;\n";
		var footer = "&lt;/rte&gt;\n&lt;/gpx&gt;";
	} 
	
	if (outputFileMode == 'csv') {
		var header = name + "";
		var footer = "";
	}

	if (outputFileMode == 'tsv') {
		var header = name + "";
		var footer = "";
	}


	// print coords header
	coordtext =  header;
	// loop to print coords
	for (var i = 0; i<(poly.getVertexCount()); i++) {
		var vert = poly.getVertex(i);
		var lat = vert.lat();
		lat = lat.toFixed(6);
		var longi = vert.lng();
		longi = longi.toFixed(6);
		if (outputFileMode == 'gpx') {
			coordtext += "&lt;rtept lat=\"" + lat + "\" lon=\"" + longi + "\"&gt;" + "&lt;/rtept&gt;\n";
		}
		if (outputFileMode == 'csv') {
			coordtext += lat + "," + longi + "\n";
		}
		if (outputFileMode == 'tsv') {
			coordtext += lat + "\t" + longi + "\n";
		}
	}

	coordtext +=  footer;

	var ourtag = document.getElementById("coords");

	if (ourtag.tagName == "PRE" && "outerHTML" in ourtag){
		ourtag.outerHTML = '<pre id="coords">' + coordtext + '</pre>';

	} else {
		ourtag.innerHTML = coordtext;
	}
document.f1.polygons.value=coordtext;
}

// Toggle output file type

	function toggleOutputMode(){
		outputFileMode = document.getElementById("outputMode_gpx").checked;
		logCoordinates(currpoly);
  }

	function autoSelect(el) {
    if(el && (el.tagName === "TEXTAREA" || (el.tagName === "INPUT" && el.type === "text"))) {
        el.select();
        return;
    }
    
    if (el && window.getSelection) { // FF, Safari, Opera
        var sel = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(el);
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (el) { // IE
        document.selection.empty();
        var range = document.body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
}

function showAddress(address) {
     if (geocoder) {
       geocoder.getLatLng(address,
         function(point) {
           if (!point) {
             alert(address + " not found");
           } else {
	     clearMap();
             map.setCenter(point, 13);
           }
         }
       );
     }
   }

 // Clear current Map
function clearMap(){
  map.clearOverlays();
	shapeCounter_ = 0;
	
// Clear featureTable_ tbody
	while (featureTable_.childNodes.length > 0) {
     featureTable_.removeChild(featureTable_.firstChild);
  }
	
	colorIndex_ = 0;
	
  }

function closePolygon(){
	var closePoint = new GLatLng(poly.getVertex(0).lat(),poly.getVertex(0).lng());
  poly.insertVertex(poly.getVertexCount() + 1,  closePoint);
  logCoordinates(poly);
    
   }

function removeLast(poly) {
	poly.deleteVertex(poly.getVertexCount() - 1);
	poly.enableDrawing(options);
  logCoordinates(poly);
}

function lognewcoords(){
if(document.f1.polygons.value==""){alert("Must create polygon in order to save it for this service area");return;}
document.f1.zoomlevel.value=map.getZoom();
document.f1.updatepolygons.value = 1;
document.f1.action2.value = "stay";
document.f1.submit(true);
}

//------------add marker-------------------------------------------------------
var marker;
function follow(){

  var dog = true;
  var noMore = false;

  var mouseMove = GEvent.addListener(map, 'mousemove', function(cursorPoint){
    if(noMore){
      //f1.markerx.value = marker.getLatLng().lng();
      //f1.markery.value = marker.getLatLng().lat();
    }

    else{
      noMore = true;
      marker = new GMarker(cursorPoint,{draggable:true, autoPan:true});
      map.addOverlay(marker);
      //marker.setImage("http://maps.google.com/mapfiles/marker.png");
    }

    if(dog){
      marker.setLatLng(cursorPoint);
    }

  });

  var mapClick = GEvent.addListener(map, 'click', function(){
    dog = false;
    GEvent.removeListener(mouseMove);
    GEvent.removeListener(mapClick);
  });

}


//-----------------------------------------------------------------------------

</script>

<body onLoad="javascript:initialize()" onUnload="javascript:GUnload">

  <%if scriptname = "admin_stations.asp" and point1 = "" then%>
    <a id=addmarker href=#aa onmousedown='javascript:document.getElementById("addmarker").style.visibility = "hidden"; follow();'><img src=http://maps.google.com/mapfiles/marker.png border=0> <%rr "~Click to locate station~"%></a>
  <%end if%>

  <table width="97%" border="1" cellspacing="0" cellpadding="0" dir=ltr>
  <tr><td colspan="7"><div id="map" style="width:100%; height: 500px"></div>
  <tr><td dir=ltr style='padding:5;'><nobr>
    <%t = getfield("searchaddress") : if t = "" then t = theinmap : if t = "" then t = thetitle%>
    Search <input type=text value="<%=t%>" id=searchaddress name=searchaddress onkeypress='vbscript:if window.event.keycode = 13 then document.location = "?action=form&id=<%=request_id%>&action2=find&searchaddress=" & curl(me.value)'>
    <input type=button value='Find' style='width:120;' onclick='vbscript:document.location = "?action=form&id=<%=request_id%>&action2=find&searchaddress=" & curl(searchaddress.value)'>
    <%if scriptname = "admin_areas.asp" then%>
      <input type="button" value="Remove Polygon" style='font-weight:bold;' onClick='javascript:document.f1.polygons.value=""; document.f1.updatepolygons.value = 1; clearMap();drawothers();'>
      <input type="button" value="Start Polygon" style='font-weight:bold;' onClick='javascript:clearMap();drawothers();startShape(); document.f1.polygons.value="";'>
      <input type="button" value="Save Polygon" style='font-weight:bold;' onClick='javascript:lognewcoords();'>
      <tr><td colspan="7" dir=ltr>
      <span id='activepoly'></span>
      <table id ="featuretable" style='visibility:hidden; position:absolute;'><tbody id="featuretbody"></tbody></table>
      <span id="coords" style='visibility:hidden; position:absolute;' ></span>
    <%end if%>

    <%if scriptname = "admin_stations.asp" then%>

      <input type=button style='font-weight:bold;' value='Remove Marker' onclick='javascript:removemarker();'>
      <script language=javascript>
      function removemarker()
      {
      f1.markerx.value = "";
      f1.markery.value = "";
      f1.action2.value = "stay";
      f1.submit();
      }
      </script>

      <input type=button style='font-weight:bold;' value='Save Marker' onclick='javascript:savemarker();'>
      <script language=javascript>
      function savemarker()
      {
      f1.markerx.value = marker.getLatLng().lng();
      f1.markery.value = marker.getLatLng().lat();
      f1.action2.value = "stay";
      f1.submit();
      }
      </script>


    <%end if%>
  </table>


