<?php
session_start();
/*header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=ISO-8859-1");*/

require_once 'conn.php';
require_once 'function.php';
$outp = "";

if($_GET['period']==='1')
{
	$query="SELECT * FROM orders where date1 between DateSerial(Year(Date()), Month(Date()),Day(Date())-3) and DateSerial(Year(Date()), Month(Date()),Day(Date())+4) and status <> 'Draft' order by (status = 'New'), date1,status";
}else
{
	if($_GET['period']==='2')
	{
		$query="SELECT * FROM orders where date1 between (Date() - WeekDay(Date())+1) and (Date() - WeekDay(Date())+ 8) ";
	}
	if($_GET['period']==='3')
	{
		$query="SELECT * FROM orders where date1 between DateSerial(Year(Date()), Month(Date()),1) and DateSerial(Year(Date()), Month(Date()),Day(Date())+1)  ";
	}
	if($_GET['period']==='4')
	{

		$query="SELECT * FROM orders where date1 between DateSerial(Year(Date()), Month(Date())-1,1) and DateSerial(Year(Date()), Month(Date()),1) ";
	}
	
	$query.=" and status <> 'Draft' order by date1";
}
	$stmt = $dbh->prepare($query);
	$stmt->execute();
	$data=array();
	foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row)
	{
		$date1 = new DateTime($row['date1']);$date1 = $date1->format('d/m/y H:i');
		$date2 = new DateTime($row['date2']);$date2 = $date2->format('d/m/y H:i');
		//if($row['date1b']> cdate('1/1/2000') ){$date1b = new DateTime($row['date1b']);$date1b = $date1b->format('d/m/Y H:i');}
		
		if(!empty($row['status'])){
					if($row['status']==='Draft'){$status='טיוטה';}
					elseif($row['status']==='Canceled'){$status='בוטל';}
					elseif($row['status']==='Done'){$status='בוצע';}
					elseif($row['status']==='Approved'){$status='אושר';}
					elseif($row['status']==='New'){$status='חדש';}
					elseif($row['status']==='Received'){$status='התקבל';}
					elseif($row['status']==='Sent'){$status='נשלח';}
					elseif($row['status']==='Rejected'){$status='נדחה';}
					elseif($row['status']==='Done Approved'){$status='בוצע ואושר';}}
		if(!empty($row['direction'])){
					if($row['direction']==='A'){$direction='איסוף';}
					elseif($row['direction']==='B'){$direction='פיזור';}
					elseif($row['direction']==='E'){$direction='הצמדה';}}
		if($row['courseid']!=0){
				$course=getTitle($row["courseid"],"courses");
		}else{$course=getTitle($row["from1"],"stations")."-".getTitle($row["to1"],"stations");}
		$course=cleanString($course);
		$site=getTitle($row['siteid'],"sites");$site=cleanString($site);
		if($row['remarks']!==''){$remarks=ansiToUtf($row['remarks']);}else{$remarks='';};
		$rowSupplier=getTitle($row["supplierid"],"suppliers"); $supplier=cleanString($rowSupplier);
		$pids=substr($row['pids'],0,-1);$passengers=explode(",",$pids);
		$passengersDetail='';
		
		if(!empty($passengers)){
			for($i=0;$i<sizeof($passengers);$i++){
				if($passengersDetail!=''){ $passengersDetail.=',';}
				if($passengers[$i]){
				 $passengersDetail .= '{"name":"'  . getName($passengers[$i]). '",';
				 $passengersDetail .= '"address":"'  .  getColumn($passengers[$i],"workers","address"). '",';
				 $passengersDetail .= '"phone":"'. getColumn($passengers[$i],"workers","phone1")     . '"}'; }
			}
		}
		if($passengersDetail!==''){$addPassengersDetail='"passengersDetail":['. $passengersDetail. '],';}else{$addPassengersDetail='';}
		
		
		if ($outp != "") {$outp .= ",";}
		 $outp .= '{"id":"'  . $row["id"] . '",';
		 $outp .= '"date1":"'   . $date1        . '",';
		 $outp .= '"date2":"'   . $date2        . '",';
		 $outp .= '"status":"'. $status. '",';
		 $outp .= '"direction":"'. $direction. '",';
		 $outp .= '"ordertype":"'. getTitle($row["ordertypeid"],"ordertypes"). '",';
		 $outp .= '"course":"'. $course. '",';
		 $outp .= '"courseid":"'. $row["courseid"]. '",';
		 $outp .= '"supplier":"'. $supplier. '",';
		 $outp .= '"supplierid":"'.$row["supplierid"]. '",';
		 $outp .= '"car":"'. getTitle($row["carid"],"cars"). '",';
		 $outp .= '"driver":"'. getTitle($row["driverid"],"drivers"). '",';
		 $outp .= '"price":"'. $row["price"]. '",';
		 $outp .= '"reasons":"'. getTitle($row["reasonid"],"reasons"). '",';
		 //$outp .= '"remarks":"'.$remarks.'",';
		 $outp .= '"production":"'. $row["production"]. '",';
		 $outp .= '"updated":"'.getName($row["userid"]). '",';
		 $outp .= '"updatedid":"'.$row["userid"]. '",';
		 $outp .= '"pids":"'. $row["pids"]. '",';
		 $outp .= '"pcount":"'. round($row["pcount"]). '",';
		 $outp .= $addPassengersDetail;
		
		 $outp .= '"site":"'. $site     . '"}'; 
    }
	
	$outp ='{"records":['.$outp.']}';
	
	 echo ($outp);
?>
