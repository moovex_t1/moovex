

<!DOCTYPE html>
<html ng-app="angularTable">
    <head>
        <title></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="js/angular.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/dirPagination.js"></script>
		<script src="js/appTable.js"></script>
		<script>
		
		</script>
		<style>
		.clearboth{clear:both;}
		.nav{margin-right:3em;}
		.search{margin:0.3em 0.5em;}
		li label {margin-top:1em; margin-right:1em;}
		th {color:#5E84B5;text-align:center;}
		.btn {margin-right:1em;}
		.new{color:#4285F4;font-weight:strong;}
		

		</style>
	</head>

	<body  dir="rtl" ng-controller="ordersCtrl">
	<?php
		if(isset($_GET['orderid']))
		{ 
			if($_GET['status']==='Approved'){$updatedStatus='אושרה';
			}elseif($_GET['status']==='Canceled'){$updatedStatus='בוטלה';
			}elseif($_GET['status']==='Sent'){$updatedStatus='נשלחה לספק';}
			echo 
				'<center><div class="alert alert-info">
					הזמנה  '.$_GET['orderid'].'&nbsp;&nbsp;'.$updatedStatus.
				'</div></center>';
		
			}

	?>
		<ul class="nav nav-tabs">
		
			 <li role="presentation" class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				 בחר תקופה <span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
					<li><a ng-click="periodUpdate(2)">3 ימים</a></li>
					<li><a ng-click="periodUpdate(2)">שבוע</a></li>
					<li><a ng-click="periodUpdate(3)">חודש</a></li>
					<li><a ng-click="periodUpdate(4)">חודש קודם</a></li>
					
				</ul>

			  </li>
			  <li>
			  <a  ng-click="periodUpdate(1)" role="button"><span class="glyphicon glyphicon-repeat"></span></a>
			  </li>
			  <li>
			   <a  href="login.php" style="float: left;" role="button">התנתק</a>
			  </li>

		</ul>

		<div> 

	
			<div class="table-responsive">
				<center>
					<table class="table table-striped table-hover" style="width:90%;">
					  <thead>
						  <tr>
							
							<th ng-click="sort('id')">{{headers.id}}
								 <span class="glyphicon sort-icon" ng-show="sortKey=='id'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
							</th>        
							<th ng-click="sort('date1')">{{headers.date1}}
								<span class="glyphicon sort-icon" ng-show="sortKey=='date1'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
							</th>
							<th ng-click="sort('status')">{{headers.status}}
								<span class="glyphicon sort-icon" ng-show="sortKey=='status'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
							</th>
							<th  ng-click="sort('direction')">{{headers.direction}}
								<span class="glyphicon sort-icon" ng-show="sortKey=='direction'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
							</th>
							<th class=" hidden-xs" ng-click="sort('ordertype')">{{headers.ordertype}}
								<span class="glyphicon sort-icon" ng-show="sortKey=='ordertype'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
							</th>
							<th ng-click="sort('course')">{{headers.course}}
								<span class="glyphicon sort-icon" ng-show="sortKey=='course'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
							</th>
							</th>
							<th class="hidden-xs" ng-click="sort('car')">{{headers.car}}
								<span class="glyphicon sort-icon" ng-show="sortKey=='car'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
							</th>
							<th class="hidden-xs" ng-click="sort('price')">{{headers.price}}
								<span class="glyphicon sort-icon" ng-show="sortKey=='price'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
							</th>
						  </tr>
					  </thead>
					  <tbody> 
						 <tr> 
							
							 <td><input style='width:100%' ng-model="search.id" /></td>
							 <td><input style='width:100%' ng-model="search.date1" /></td>
							 <td><input style='width:100%' ng-model="search.status" /></td>
							 <td ><input style='width:100%' ng-model="search.direction" /></td>
							 <td class="hidden-xs"><input style='width:100%' ng-model="search.ordertype" /></td>
							 <td><input style='width:100%' ng-model="search.course" /></td>
							 <td class="hidden-xs"><input style='width:100%' ng-model="search.car" /></td>
							 <td class="hidden-xs"><input style='width:100%' ng-model="search.price" /></td>
						  </tr>
						  <tr  ng-switch="order.status" dir-paginate-start="order in orders|orderBy:sortKey:reverse|filter:search|itemsPerPage:10" ng-click="order.expanded = !order.expanded">
								
								<td>{{ order.id }}</td>
								<td>{{ order.date1}}</td>
								<td><span ng-switch-when="חדש" class="new">{{ order.status}}</span><span ng-switch-default>{{ order.status}}</span></td>
								<td>{{ order.direction}}</td>
								<td class="hidden-xs">{{ order.ordertype}}</td>
								<td>{{ order.course}}</td>
								<td class="hidden-xs">{{ order.car}}</td>
								<td class="hidden-xs">{{ order.price}}</td>
						  </tr>
						  <tr ng-if="order.expanded" dir-paginate-end="" >
								 <td colspan="100%">									
											<form id="update" name="update" action="update.php" method="post">
												<input type="hidden" name="date1"  ng-value="order.date1">
												<input type="hidden" name="order_id"  ng-value="order.id">
												<input type="hidden" name="price"  ng-value="order.price">
												<input type="hidden" name="pids"  ng-value="order.pids">
												<input type="hidden" name="course"  ng-value="order.course">
												<input type="hidden" name="supplierid"  ng-value="order.supplierid">
												<input type="hidden" name="updatedid"  ng-value="order.updatedid">
												
												<div class="container panel panel-default">
													<div class="panel-body">
														<div class="row visible-xs" ng-show="order.direction">
															<div class="col-xs-4"><label>{{headers.direction}}</label></div>
															<div class="col-xs-8">{{order.direction}}</div>
														</div>
														<div class="row visible-xs" ng-show="order.ordertype">
															<div class="col-xs-4"><label>{{headers.ordertype}}</label></div>
															<div class="col-xs-8">{{order.ordertype}}</div>
														</div>
														<div class="row visible-xs" ng-show="order.car">
															<div class="col-xs-4"><label>{{headers.car}}</label></div>
															<div class="col-xs-8">{{order.car}}</div>
														</div>
														<div class="row visible-xs" ng-show="order.price">
															<div class="col-xs-4"><label>{{headers.price}}</label></div>
															<div class="col-xs-8">{{order.price}}</div>
														</div>
														<div class="row" ng-show="order.date2">
															<div class="col-xs-4 col-md-3"><label>{{headers.date2}}</label></div>
															<div class="col-xs-8 col-md-9" >{{order.date2}}</div>	
														</div>
														<div class="row" ng-show="order.supplier">
															<div class="col-xs-4 col-md-3"><label>{{headers.supplier}}</label></div>
															<div class="col-xs-8 col-md-9" >{{order.supplier}}</div>
														</div>
														<div class="row" ng-show="order.driver">
															<div class="col-xs-4 col-md-3"><label>{{headers.driver}}</label></div>
															<div class="col-xs-8 col-md-9" >{{order.driver}}</div>	
														</div>
														<div class="row" ng-show="order.site">
															<div class="col-xs-4 col-md-3"><label>{{headers.site}}</label></div>
															<div class="col-xs-8 col-md-9" >{{order.site}}</div>
															
														</div>
														<div class="row" ng-show="order.reasons">
															<div class="col-xs-4 col-md-3"><label>{{headers.reasons}}</label></div>
															<div class="col-xs-8 col-md-9" >{{order.reasons}}</div>
														</div>
														<div class="row" ng-show="order.pcount">
															<div class="col-xs-4 col-md-3"><label>{{headers.pcount}}</label></div>
															<div class="col-xs-8 col-md-9" >{{order.pcount}}</div>
														</div>
													<!--	<div class="col-xs-4 col-md-3" ng-show="order.passengersDetail"><label>{{headers.passengers}}</label></div>
														<div class="row" ng-show="order.passengersDetail" ng-repeat="p in order.passengersDetail">
															<div class="col-xs-4 col-md-3"><label></label></div>
															<div class="col-xs-8 col-md-9">
															 <strong>{{p.name}}</strong> {{p.address}}<br/> {{p.phone}}
															</div>
														</div>-->
														<div class="row" ng-show="order.passengersDetail" ng-repeat="p in order.passengersDetail">
															<div class="col-xs-4 col-md-3"><label>{{}}</label></div>
															<div class="col-xs-8 col-md-9" >
															 <strong>{{p.name}}</strong> {{p.address}}<br/> {{p.phone}}
															</div>
														</div>
														<p>
														<div class="row" ng-show="order.remarks">
															<div class="col-xs-4 col-md-3"><label>{{headers.remarks}}</label></div>
															<div class="col-xs-8 col-md-9" >{{order.remarks}}</div>
														</div>
														
														<div class="row" ng-show="order.updated">
															<div class="col-xs-4 col-md-3"><label>{{headers.updated}}</label></div>
															<div class="col-xs-8 col-md-9" >{{order.updated}}</div>
														</div>
														
														
													
													</div>
												</div>
												
												<center><div ng-switch="order.status">
														<!--<input class="btn btn-primary" type="submit" name="send" value="שלח" ng-switch-when='אושר' class="btn btn-primary">-->
														<input class="btn btn-primary" type="submit" name="send" value="שלח" ng-switch-when='חדש' class="btn btn-primary">
														<input class="btn btn-warning" type="submit" name="cancel" value="בטל" ng-switch-when="חדש" class="btn btn-primary">
														<input class="btn btn-warning" type="submit" name="cancel" value="בטל" ng-switch-when="נשלח" class="btn btn-primary">
														
														
													
												</div>
												
												</center>
											</form>
								 </td>
						  </tr>
					  <tbody>
					</table>
				</center>
			</div>
					<center>
					<dir-pagination-controls
						   max-size="5"
						   direction-links="true"
						   boundary-links="true" >
					</dir-pagination-controls>
					</center>
		</div>
	
	</body>
</html>