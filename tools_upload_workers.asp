<!--#include file="include.asp"-->
<%
'--------------------admin user
if session("userid") = "" then
  session("userid") = "999999"
  session("username") = "kiki"
  session("userlevel") = "5"
  session("usergroup") = "admin"
  session("usergroup0") = "admin"
  session("usersiteid") = "0"
  session("useremail") = ""
end if

admin_top = true : admin_login = true%>
<!--#include file="top.asp"-->
<%

tablefields = "id,firstname,lastname,active,eligible,specialrides,sms,idn,code,phone1,phone2,phone3,email,pass,adusername,workertypeid,unitid,template,address,courseid1,citystationid,from1,from2,to1,to2,shiftids,ordertypeids,color,remarks,siteid,data1,"

'---translate
worker_name = "����"
workers_name = "������"
site_name = "���"

if hasmatch(specs, "shufersal,") then
 site_name = "����"
end if
'---

function insertworkersrecord
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("code") <> "" then t = getfield("code")
  f = f & "code," : v = v & "'" & t & "',"

  t = "on" : if getfield("active") <> "" then t = getfield("active")
  f = f & "active," : v = v & "'" & t & "',"

  t = "on" : if getfield("eligible") <> "" then t = getfield("eligible")
  f = f & "eligible," : v = v & "'" & t & "',"

  t = "on" : if getfield("specialrides") <> "" then t = getfield("specialrides")
  f = f & "specialrides," : v = v & "'" & t & "',"

  t = "on" : if getfield("sms") <> "" then t = getfield("sms")
  f = f & "sms," : v = v & "'" & t & "',"

  t = "" : if getfield("firstname") <> "" then t = getfield("firstname")
  f = f & "firstname," : v = v & "'" & t & "',"

  t = "" : if getfield("lastname") <> "" then t = getfield("lastname")
  f = f & "lastname," : v = v & "'" & t & "',"

  t = "0" : if getfield("workertypeid") <> "" then t = getfield("workertypeid")
  f = f & "workertypeid," : v = v & "" & t & ","

  t = "0" : if getfield("from1") <> "" then t = getfield("from1")
  f = f & "from1," : v = v & "" & t & ","

  t = "0" : if getfield("to1") <> "" then t = getfield("to1")
  f = f & "to1," : v = v & "" & t & ","

  t = "0" : if getfield("courseid1") <> "" then t = getfield("courseid1")
  f = f & "courseid1," : v = v & "" & t & ","

  t = "0" : if getfield("siteid") <> "" then t = getfield("siteid")
  f = f & "siteid," : v = v & "" & t & ","

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into workers(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from workers"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  insertworkersrecord = rs("id")
end function

'---
function setweekplanbyshifts2(byval w, byval shiftids)
  dim s,rs,rs1,data1,dd,d,t,f,v,ii
  s = "delete * from weekplan where workerid = " & w & " and shiftid in(0" & shiftids & ")"
  set rs = conn.execute(sqlfilter(s))
  s = "select id,hour1,hour1b,ways,days,data1 from shifts where id in(0" & shiftids & ")"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    ii = getidlist("select id from weekplan where workerid = " & w & " and shiftid = " & rs("id"))
    if ii = "" then
      data1 = rs("data1") & ""
      dd = rs("days") : dd = bothmatch(dd, "1,2,3,4,5,6,7,")
      do until dd = ""
        d = left(dd,instr(dd,",")-1) : dd = mid(dd,instr(dd,",")+1)
        f = "" : v = ""
        f = f & "day1," : v = v & "'" & d & "',"
        f = f & "workerid," : v = v & w & ","
        f = f & "ways," : v = v & "'" & rs("ways") & "',"
        f = f & "shiftid," : v = v & rs("id") & ","
        f = f & "hour1," : v = v & "'" & rs("hour1") & "',"
        f = f & "hour1b," : v = v & "'" & rs("hour1b") & "',"
        f = f & "from1," : v = v & "0" & getval(data1, "from1") & ","
        f = f & "to1," : v = v & "0" & getval(data1, "to1") & ","
        f = f & "date1," : v = v & sqldate("1/1/1900") & ","
        f = f & "date2," : v = v & sqldate("1/1/1900") & ","
        f = left(f, len(f) - 1): v = left(v, len(v) - 1)
        s = "insert into weekplan(" & f & ") values(" & v & ")"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
      loop
    end if
    rs.movenext
  loop
end function
'--
function beforeloadcsv
  if hasmatch(specs, "jr,") then du = ConvertUnicodeFileToANSI(loadcsvfile)
  Dim s, a, b, c, t, tid, already, snart, x, gg, g1, g2

  beforeloadcsv = 0
  server.scripttimeout = 100000 'seconds
  if loadcsvfile = "" then loadcsvfile = getfield("loadcsv")

  set thefile = fs.opentextfile(server.mappath(loadcsvfile))
  csvdd = thefile.readall & vbcrlf
  thefile.close
  if instr(csvdd, chr(10)) > 0 or instr(csvdd, chr(13)) > 0 then csvdd = replace(csvdd, chr(10), chr(13) & chr(10))
  if specs = "mh" and instr(csvdd, """") > 0 then csvdd = replace(csvdd, """", "")

  s = "select * from bumodules where title = 'workers'"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if not rs.eof then tid = rs("id") else errorend("can't find table definitions")

  snart = "|"
  a = tablefields
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    snart = snart & translate("~workers_workers_" & b & "~") & "=" & b & "|"
  loop

  t = left(csvdd, instr(csvdd, chr(10)) - 1)
  a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
  a = a & "," : c = 0
  a = replace(a, chr(10), "") : a = replace(a, chr(13), "") 'because shufersal (&vbcrlf ������� ������ �� ���� �����)

  if specs = "mh" then
    gg = ""
    gg = gg & "��� ,siteid,"
    gg = gg & "�� ����,firstname,"
    gg = gg & "�� �����,lastname,"
    gg = gg & "���� ����,code,"
    gg = gg & "��� ����,workertypeid,"
    gg = gg & "�����,unitid,"
    gg = gg & "�����,address,"
    gg = gg & "���,citystationid,"
    gg = gg & "�����,to1,"
    gg = gg & "������,email,"
    gg = gg & "����� ����,phone1,"
    gg = gg & "����� ������,phone3,"
    do until gg = ""
      g1 = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
      g2 = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
      a = replace("," & a, "," & g1 & ",", "," & g2 & ",", 1, 1, 1) : a = mid(a,2)
    loop
  end if

  if specs = "jr" then
    gg = ""
    gg = gg & "���,siteid,"
    gg = gg & "�� ����,firstname,"
    gg = gg & "�� �����,lastname,"
    gg = gg & "����,active,"
    gg = gg & "����,eligible,"
    gg = gg & "���� ����,code,"
    gg = gg & "��� ����,workertypeid,"
    gg = gg & "�����,unitid,"
    gg = gg & "�����,address,"
    gg = gg & "���,citystationid,"
    gg = gg & "�����,to1,"
    gg = gg & "������,email,"
    gg = gg & "����� ����,phone1,"
    gg = gg & "����� ������,phone3,"
    do until gg = ""
      g1 = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
      g2 = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
      a = replace("," & a, "," & g1 & ",", "," & g2 & ",", 1, 1, 1) : a = mid(a,2)
    loop
  end if

  if specs = "shufersal" then
    gg = ""
    gg = gg & "��-��.�.,siteid,"
    gg = gg & "���� ��-���,unitid,"    
    gg = gg & "��.����,code,"
    gg = gg & "��. �.�.,idn,"
    gg = gg & "�� ����,firstname,"
    gg = gg & "�� �����,lastname,"
    gg = gg & "��� ���,citystationid,"
    gg = gg & "��� ����,street,"
    gg = gg & "���#,house,"
    gg = gg & "����� ����,phone1,"
    gg = gg & "���� ������,eligible,"
    do until gg = ""
      g1 = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
      g2 = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
      a = replace("," & a, "," & g1 & ",", "," & g2 & ",", 1, 1, 1) : a = mid(a,2)
    loop
  end if

  if specs = "rail" then
    gg = ""
    gg = gg & "���� ���� ����,code,"
    gg = gg & "�� ����,firstname,"    
    gg = gg & "�� �����,lastname,"
    gg = gg & "���� ������,unitid,"
    gg = gg & "����,workertypeid,"
    gg = gg & "����� �������� �����,to1,"
    gg = gg & "���� �����,phone3,"
    gg = gg & "���� ����,phone1,"
    gg = gg & "����� ����,phone2,"
    gg = gg & "���� ������,email,"
    gg = gg & "��� ������,eligible,"
    do until gg = ""
      g1 = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
      g2 = left(gg,instr(gg,",")-1) : gg = mid(gg,instr(gg,",")+1)
      a = replace("," & a, "," & g1 & ",", "," & g2 & ",", 1, 1, 1) : a = mid(a,2)
    loop
  end if

  do
    c = c + 1 : if instr(a,",") = 0 then exit do
    if left(a,1) = """" and instr(2,a,"""") >= 2 then
      x = 1
      do until x >= len(a)
        x = x + 1
        if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
        if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
      loop
      b = left(a,x) : a = mid(a,x+2)
      b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
    else
      b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
    end if
    b = replace(b,"'","''")

    if left(b,1) = """" and right(b,1) = """" then b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
    b = lcase(chtm(b))
    t = left(b,1) : if t = "" then t = " "
    if asc(t) = 239 then b = mid(b,4) 'utf codepage

    csvff(c) = b : t = getval(snart,b) : if b <> "" and t <> "" then csvff(c) = t
    if instr("," & already, "," & b & ",") > 0 then csvff(c) = "" else already = already & csvff(c) & ","
  loop
  s = "select * from bumodulefields where tid = " & tid & " and tcontrol = 'formdata'"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  formdatafield = "" : if not rs.eof then formdatafield = rs("tname")
  csvidlist = getidlist("select id from workers" )
end function

function ConvertUnicodeFileToANSI(byval fn)
  Dim strText
  set st = CreateObject("ADODB.Stream")
  st.Open
  st.Type = 1 'binary
  st.LoadFromFile server.mappath(fn)
  st.Type = 2 'text
  st.Charset = "UNICODE"
  strText = st.ReadText(-1) 'read all
  st.Position = 0
  st.SetEOS
  st.Charset = "ISO-8859-8" 'Use current ANSI codepage.
  st.WriteText strText, 0 'adWriteChar
  'fn = "upload/jr_workers.csv"
  st.SaveToFile server.mappath(fn), 2 'create/overwrite
  st.Close
End function

function FixCsvFile(byval fn)
  Dim strText
  set st = CreateObject("ADODB.Stream")
  st.Open
  st.Type = 1 'binary
  st.LoadFromFile server.mappath(fn)
  st.Type = 2 'text
  'st.Charset = "UNICODE"
  st.Charset = "ISO-8859-8"
  strText = st.ReadText(-1) 'read all
  st.Position = 0
  st.SetEOS
  st.Charset = "ISO-8859-8" 'Use current ANSI codepage.
  st.WriteText strText, 0 'adWriteChar
  st.SaveToFile server.mappath(fn), 2 'create/overwrite
  st.Close
End function

'--------------------------------------------------------------------------------------------
if getfield("action") = "upload" Then
  'http://127.0.0.1/Moovex/tools_admin_upload.asp?action=uploadcsv
  loadcsvfile = getfield("file")
  beforeloadcsv()
  rr "<script language=vbscript> progress.style.top = 200 </script>" : response.flush
  rsrc = countstring(csvdd, vbcrlf)
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
      rsc = line : rr "<script language=vbscript> progresstext.innerhtml = ""<b>~Processing~..</b> " & rsc & "/" & rsrc & """ : progressbar.style.width = " & fix(400 * rsc / rsrc) & " </script>" : response.flush      
      s = "" : thisid = "0" : dothis = "on"

      a = replace(a, chr(253), "")
      a = replace(a, chr(254), "")

      unactiveworker = false
      activeworker = false
      newly = ""
      thexy = ""
      dubleworker = ""
      siteid = "0"
      usersiteid = session("usersiteid")
      docreatecity = ""
      docreatestations = ""
      dofindstations = ""
      remarks = ""
      Subject = ""
      Body = ""
      onmap = ""

      'special cases
      street = ""
      house = ""
      birthdate = ""
      class1 = ""
      mother = ""
      father = ""
      shiftname = ""
      shiftcode = ""
      groupcode = ""

      if hasmatch(specs, "mh,") then docreatecity = true
      if hasmatch(specs, "jr,") then docreatestations = true
      if hasmatch(specs, "mh,") then dofindstations = true
      if hasmatch(specs, "mh,jr,") and getparam("UseGoogleMaps") = "y" then getxy = true

      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)

        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b

        elseif csvff(i) = "siteid" then
          do
            if left(b, 1) = "0" then b = mid(b,2) else exit do
          loop
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          s = s & "siteid=" & v & ","
          siteid = v : sitename = b

        elseif csvff(i) = "idn" then
          if len(b) > 20 then b = left(b,20)
          s = s & "idn='" & b & "',"
          workeridn = b

        elseif csvff(i) = "code" then
          if specs = "xxx" then 
            ''---remove leading zeros
            'do
            '  if left(b, 1) = "0" then b = mid(b,2) else exit do
            'loop
            '---add leading zeros
            for k = len(b) to 8 : b = "0" & b : next
          end if
          do
            if left(b, 1) = "0" then b = mid(b,2) else exit do
          loop
          if len(b) > 20 then b = left(b,20)
          s = s & "code='" & b & "',"
          sq = "select siteid,id,data1 from workers where code = '" & b & "' "
          'sq = sq & " and siteid = 1"
          sq = sq & " order by id desc"
          set rs1 = conn.execute(sqlfilter(sq))
          cc = 0 : z = 0
          do until rs1.eof
            cc = cc + 1
            z = rs1("id")
            rs1.movenext
          loop
          if cc = 1 then thisid = z
          if cc > 1 then thisid = z : showerror("Code '" & b & "' exists " & cc & " times")
          workercode = b
          thedata1 = getonefield("select data1 from workers where id = " & thisid )
          workerdate1 = thedata1

        elseif csvff(i) = "firstname" then
          b = ctxt(b) : b = replace(b, "'", "`") : b = replace(b, """", "``")
          b = strfilter(b,"��������������������������� -.`abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
          if len(b) > 50 then b = left(b,50)
          s = s & "firstname='" & b & "',"
          firstname = b

        elseif csvff(i) = "lastname" then
          b = ctxt(b) : b = replace(b, "'", "`") : b = replace(b, """", "``")
          b = strfilter(b,"��������������������������� -.`abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789*")
          if len(b) > 20 then b = left(b,20)
          s = s & "lastname='" & b & "',"
          lastname = b

        'elseif csvff(i) = "active" then
        '  if len(b) > 2 then b = left(b,2)
        '  s = s & "active='" & lcase(b) & "',"
        '  active = b

        elseif csvff(i) = "eligible" then
          if len(b) > 2 then b = left(b,2)
          s = s & "eligible='" & lcase(b) & "',"
          eligible = b

        elseif csvff(i) = "specialrides" then
          if len(b) > 2 then b = left(b,2)
          s = s & "specialrides='" & lcase(b) & "',"
          specialrides = b

        elseif csvff(i) = "sms" then
          if len(b) > 2 then b = left(b,2)
          s = s & "sms='" & lcase(b) & "',"
          sms = b

        elseif csvff(i) = "workertypeid" then
          if b = "" then
            v = 0
          else
            sq = "select * from workertypes where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            if v = 0 then
              sq = "insert into workertypes(title) values('" & b & "')"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from workertypes"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              v = rs1("id")
            end if
          end if
          s = s & "workertypeid=" & v & ","
          workertypeid = v

        elseif csvff(i) = "phone1" then
          if len(b) > 20 then b = left(b,20)
          if left(b,1) <> "0" then b = "0" & b
          if left(b,4) <> "-" then b = left(b,3) & "-" & right(b,7)
          s = s & "phone1='" & strfilter(ctxt(b),"+-0123456789") & "',"

        elseif csvff(i) = "phone2" then
          if len(b) > 20 then b = left(b,20)
          s = s & "phone2='" & strfilter(ctxt(b),"+-0123456789") & "',"

        elseif csvff(i) = "phone3" then
          if len(b) > 20 then b = left(b,20)
          s = s & "phone3='" & strfilter(ctxt(b),"+-0123456789") & "',"

        elseif csvff(i) = "email" then
          if len(b) > 70 then b = left(b,70)
          s = s & "email='" & b & "',"

        elseif csvff(i) = "pass" then
          if len(b) > 20 then b = left(b,20)
          s = s & "pass='" & b & "',"

        elseif csvff(i) = "adusername" then
          if len(b) > 80 then b = left(b,80)
          s = s & "adusername='" & b & "',"

        elseif csvff(i) = "citystationid" then
          if b = "" then
            v = 0
          else
            city = b : city = replace(city, "&#38;quot", """")
            if getparam("UploadWorkerStationsAsGlobal") = "y" then siteid = 0
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(city) & "' "'and siteid = " & siteid
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            if v = 0 and docreatecity = true then
              if len(b) > 50 then b = left(b,50)
              sq = "insert into stations(title,inmap,special,markerx,markery,siteid) values('" & city & "','" & city & "','on','"& markerx &"', '"& markery &"', " & siteid & ")"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from stations"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              v = rs1("id")
            end if
          end if
          s = s & "citystationid=" & v & ","
          citystationid = v

        elseif csvff(i) = "address" then
          if left(b, 1) = ";" then b = replace(left(b, 1), ";", "")
          b = replace(b, "#@#", " - ") : b = replace(b, "&#38;quot", """")
          if hasmatch(specs, "jr,") then 
            b = replace(b, " ; ", ", ")
            b = replace(b, ";", ",")
          end if
          s = s & "address='" & b & "',"
          address = b          

        elseif csvff(i) = "from1" then
          if b = "" then
            v = 0
          else
            from1 = b
            if getparam("UploadWorkerStationsAsGlobal") = "y" then siteid = 0
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "' and siteid in(" & siteid & ",0) order by siteid"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            if v = 0 and docreatestations = true then
              if len(b) > 50 then b = left(b,50)
              sq = "insert into stations(title,inmap,siteid) values('" & b & "','" & b & "'," & siteid & ")"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from stations"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              v = rs1("id")
            end if
          end if
          s = s & "from1=" & v & ","
          from1 = v

        elseif csvff(i) = "to1" then
          if b = "" then
            v = 0
          else
            to1 = b
            if getparam("UploadWorkerStationsAsGlobal") = "y" then siteid = 0
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "' and siteid in(0," & siteid & ") order by siteid"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            if v = 0 and docreatestations = true then
              if len(b) > 50 then b = left(b,50)
              sq = "insert into stations(title,inmap,siteid) values('" & b & "','" & b & "'," & siteid & ")"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from stations"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              v = rs1("id")
            end if
          end if
          s = s & "to1=" & v & ","
          to1 = v

        elseif csvff(i) = "unitid" then
          if b = "" then
            v = 0
          else
            if len(b) > 200 then b = left(b,200)
            sq = "select * from units where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
            if v = 0 then
              sq = "insert into units(title,siteid) values('" & b & "'," & siteid & ")"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from units"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              v = rs1("id")
            end if
          end if
          s = s & "unitid=" & v & ","
          unitid = v

        elseif csvff(i) = "shiftids" then
          s = s & "shiftids='" & b & "',"

        elseif csvff(i) = "remarks" then
          s = s & "remarks='" & b & "',"

'-------Special Cases-------------------------------------------------------

        elseif csvff(i) = "street" then
          street = b

        elseif csvff(i) = "house" then
          house = b

        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next

      if hasmatch(specs, "shufersal,") then 
        b = street & " " & house & ", " & city
        s = s & "address='" & b & "',"
        address = street & " " & house
      end if
      if instr(siteurl, "/shufersal/") > 0 and hasmatch(sitename, "885,939,940,965,") then newly = "skip" ' dont update
      if instr(siteurl, "/smarlog/") > 0 and NOT hasmatch(sitename, "885,939,940,965,") then newly = "skip" ' dont update
      if specs = "mh" and siteid = "3" then newly = "skip"

'---------------------------------------------------------------------------------------------------

      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      theworker = workercode & " - " & firstname & " " & lastname
			waseligible = getonefield("select eligible from workers where code = '" & workercode & "'")

      if instr("," & csvidlist,"," & thisid & ",") > 0 and newly <> "skip" then 
        theid = thisid 
        newly = ""
      elseif newly <> "skip" then
        theid = insertworkersrecord
        csvidlist = csvidlist & theid & ","
        newly = "on"
      else
        theid = "0"
      end if

'---------------------------------------------------------------------------------------------------
      if newly = "on" then
        if getxy = true then
          thexy = GetAddressXY(address)
          if instr(thexy, ",") > 0 then
            markerx = left(thexy, instr(thexy,",")-1)
            markery = mid(thexy, instr(thexy,",")+1)
          end if
        end if

        if hasmatch(specs, "shufersal,") then
          s = replacefromto(s, "", ",active='", "',", ",")
          s = replacefromto(s, "", ",eligible='", "',", ",")
          s = replacefromto(s, "", ",specialrides='", "',", ",")
          s = replacefromto(s, "", ",sms='", "',", ",")
          s = replacefromto(s, "", ",email='", "',", ",")
          b = lcase(eligible) : if b = 1 or b = 2 then b = "on" else b = ""

          s = s & "eligible='" & b & "',"
          s = s & "active='on',"
          s = s & "specialrides='on',"
          s = s & "sms='" & b & "',"
          s = s & "email='" & workeridn & "',"
          s = s & "pass='12345',"

          workertypeid = getonefield("select id from workertypes where lcase(cstr('' & title)) = '" & lcase(sitename) & "'")
          if workertypeid = "" then v = 0 else v = workertypeid
          if v = 0 then
            sq = "insert into workertypes(title) values('" & sitename & "')"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            sq = "select max(id) as id from workertypes"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            v = rs1("id")
          end if
          s = s & "workertypeid=" & v & ","
        end if

        if hasmatch(specs, "jr,") then
          thedata1 = setval(thedata1,"x", markerx)
          thedata1 = setval(thedata1,"y", markery)
          '---          
          cs = findcloseststation(address, "", theid, siteid)
          s = s & "from1=" & cs & ","
          if getparam("UploadWorkerStationsAsGlobal") = "y" then siteid = 0
          sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(address) & "' and siteid in(" & siteid & ",0) order by siteid"
          closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
          if rs1.eof then v = 0 else v = rs1("id")

          if v = 0 and docreatestations = true then
            sq = "insert into stations(title,inmap,markerx,markery,siteid) values('" & address & "','" & address & "','"& markerx &"','"& markery &"'," & siteid & ")"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            sq = "select max(id) as id from stations"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            v = rs1("id")
            du = findarea(v & ",")
          end if
          s = s & "from2=" & v & ","
        end if

        if hasmatch(specs, "shufersal,jr,") then
          newworkers = newworkers & theworker & " �" & site_name & ": " & sitename
          if onmap = "no" then newworkers = newworkers & " <b>(�� ���� ����� ����)</b> "
          newworkers = newworkers & "<br>"
        end if
      end if

'---------------------------------------------------------------------------------------------------
      if newly = "" then
        if hasmatch(specs, "jr,shufersal,") then
          s = replacefromto(s, "", ",firstname=", ",", ",")
          s = replacefromto(s, "", ",lastname=", ",", ",")
					s = replacefromto(s, "", ",phone1=", ",", ",")
					s = replacefromto(s, "", ",phone2=", ",", ",")
					s = replacefromto(s, "", ",phone3=", ",", ",")
          s = replacefromto(s, "", ",active=", ",", ",")
          s = replacefromto(s, "", ",eligible=", ",", ",")
          s = replacefromto(s, "", ",address='", "',", ",")
          s = replacefromto(s, "", ",citystationid=", ",", ",")
          s = replacefromto(s, "", ",workertypeid=", ",", ",")
          s = replacefromto(s, "", ",unitid=", ",", ",")
          s = replacefromto(s, "", ",email='", "',", ",")
          s = replacefromto(s, "", ",from1=", ",", ",")
          s = replacefromto(s, "", ",from2=", ",", ",")
          s = replacefromto(s, "", ",to1=", ",", ",")
        end if

        if hasmatch(specs, "shufersal,") then
          s = replacefromto(s, "", ",email='", "',", ",")
          s = s & "email='" & workeridn & "',"
          s = s & "pass='12345',"
        end if

        if instr(siteurl, "/shufersal/") > 0 then
          workertypeid = getonefield("select id from workertypes where lcase(cstr('' & title)) = '" & lcase(sitename) & "'")
          if workertypeid = "" then v = 0 else v = workertypeid
          if v = 0 then
            sq = "insert into workertypes(title) values('" & sitename & "')"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            sq = "select max(id) as id from workertypes"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            v = rs1("id")
          end if
          s = s & "workertypeid=" & v & ","
        end if

        if hasmatch(specs, "jr,") then
          s = replacefromto(s, "", ",from1=", ",", ",")
          addresswas = getonefield("select address from workers where id = "& theid)
          if address <> addresswas and getxy = true then
            s = s & "address='" & address & "'," ' ����� ���� - �� ����� ����� ����� ���� ����
            thexy = GetAddressXY(address)
            if instr(thexy, ",") > 0 then
              markerx = left(thexy, instr(thexy,",")-1)
              markery = mid(thexy, instr(thexy,",")+1)
            end if

            cs = findcloseststation(address, "", theid, siteid)
            s = s & "from1=" & cs & ","
            newworkers = newworkers & theworker & " ������ �����: " & address & "<br>"

            thedata1 = setval(thedata1,"x", markerx)
            thedata1 = setval(thedata1,"y", markery)
            if getparam("UploadWorkerStationsAsGlobal") = "y" then siteid = 0
            sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(address) & "' and siteid in(" & siteid & ",0) order by siteid"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")

            if v = 0 and docreatestations = true then
              sq = "insert into stations(title,inmap,markerx,markery,siteid) values('" & address & "','" & address & "','"& markerx &"','"& markery &"'," & siteid & ")"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from stations"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              v = rs1("id")
              du = findarea(v & ",")
            end if
            s = s & "from2=" & v & ","
          end if

          'wasunitid = getonefield("select unitid from workers where id = "& theid)
          'wasunit = gettitle("units", wasunitid)
          'if instr(wasunit, "|") > 0 or instr(wasunit, "\") > 0 then
          '  s = replacefromto(s, "", ",active=", ",", ",")
          '  s = replacefromto(s, "", ",eligible=", ",", ",")
          '  s = replacefromto(s, "", ",unitid=", ",", ",")
          'end if 

          'if wasunitid <> unitid and instr(wasunit, "|") = 0 then 
          '  newworkers = newworkers & theworker & " ��� ������ " & gettitle("units",unitid) & "<br>"
          'end if
        end if

        '---
        workersite = getonefield("select siteid from workers where id = " & theid)
        if hasmatch(specs, "shufersal,") and cstr(workersite) <> cstr(siteid) and waseligible = "on" then
          diffsiteworkers = diffsiteworkers & theworker & " �" & site_name & ": " & sitename & "<br>"
        end if
      end if

      '-------------------------------------------------------
      if hasmatch(specs, "mh,") then activeworker = true
      if activeworker = true then
        s = replacefromto(s, "", ",active=", ",", ",")
        s = replacefromto(s, "", ",eligible=", ",", ",")
        s = replacefromto(s, "", ",specialrides=", ",", ",")
        s = replacefromto(s, "", ",sms=", ",", ",")
        s = s & "active='on',eligible='on',specialrides='on',sms='',"
      end if

      if unactiveworker = true then
        s = replacefromto(s, "", ",active=", ",", ",")
        s = replacefromto(s, "", ",eligible=", ",", ",")
        s = replacefromto(s, "", ",specialrides=", ",", ",")
        s = replacefromto(s, "", ",sms=", ",", ",")
        s = s & "active='',eligible='',specialrides='',sms='',"
      end if
'---------------------------------------------------------------------------------------------------

      s = s & "data1='" & thedata1 & " ',"
      if newly = "skip" then s = ""

'---update----------------------------------------------------------------------------------------------------------------------------
      if s <> "" then
        s = "update workers set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if

    updatedids = updatedids & theid & ","
    adminemail = adminemail & getfieldlist("select email from buusers where usergroup = 'Admin' and siteid = 0" & siteid)
  loop
'-------------------------------------------------------------------------------------------------------------------------------------

  if createstationsworkerids <> "" then
    t = 500
    du = createstations(createstationsworkerids, t, "|")
  end if

  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
  rr "<script language=vbscript> progress.style.top = -1000 </script>" : response.flush  

'---after update----------------------------------------------------------------------------------------------------------------------
  'tomail = getfieldlist("select email from buusers where usergroup = 'Admin' and siteid = 0" & siteid)
  tomail = getparam("ProductionLogEmail")
  if specs = "shufersal" then tomail = tomail & ",seffi@moovex.net,rafi@moovex.net"
  tocc = ""
  tobcc = ""

  'if newworkers = "" and diffsiteworkers = "" then
  '  subject = specs & " - ����� " & workers_name
  '  body = "���� ��,����� ����� ������ ������ �����.�������, �� ����� ������ ����� ��� ������ ������ ������."
  '  x = sendemail(getparam("siteemail"), tomail, tocc, tobcc, subject, body)
  'end if

  if newworkers <> "" then
    subject = specs & " - ����� " & workers_name
    body = "���� " & workers_name & " ���� ������<br>"
    'if specs <> "jr" then body = body & "�� ����� ������ �� ��� ���� �" & workers_name & " ��� ���� ���� ����� ���� ���.<br><br>"
    body = body & newworkers
    x = sendemail(getparam("siteemail"), tomail, tocc, tobcc, subject, body)
  end if

  if diffsiteworkers <> "" then
		subject = specs & " - ����� " & workers_name
    body = "������ ������ ����� �" & site_name & " ���<br>"
    body = body & "�� ����� ������ �� ��� ���� �" & workers_name &" ��� ���� ���� ����� ���� ���.<br><br>"
    body = body & diffsiteworkers
    x = sendemail(getparam("siteemail"), tomail, tocc, tobcc, subject, body)
  end if

  '---
  if updatedids <> "" then

    ss = ""
    ss = ss & " and eligible = 'on'"
    if specs = "shufersal" then ss = ss & " and instr(data1, 'manual') = 0"
   
    if hasmatch(specs, "jr,") then
      pipeunits = getidlist("select id from units where instr(title, '|') > 0 ")
      ss = ss & " and unitid not in(" & pipeunits & "0)"
    end if

    notonfile = getidlist("select id from workers where id not in(" & updatedids & "0) " & ss) : notonfile1 = notonfile
    if notonfile <> "" then
      do until notonfile = ""
        b = left(notonfile,instr(notonfile,",")-1) : notonfile = mid(notonfile,instr(notonfile,",")+1)
        subject = specs & " - " & workers_name & " ������ ��� ����� ����� �����"
        body = getcode("workers", b) & " - " & getworkername(b) & "<br>"
        thebody = thebody & body
      loop
      s = "update workers set active = '', specialrides = '', eligible = '', sms = '' where id not in(" & updatedids & "0)" & ss
      set rs1 = conn.execute(sqlfilter(s))
      x = sendemail(getparam("siteemail"), tomail, tocc, tobcc, subject, thebody)
    end if
  end if
  deb msg
end if

'--------form-------------------------------------------------------------------------
if getfield("file") = "" then
  rr "<table border=0 cellspacing=5 cellpadding=5 align=center>"
  rr "<tr><td><b>����� ���� ������</b></td></tr>"
  rr "<form name=f1>"
  rr "<input type=hidden name=action value=upload>"
  rr "<tr><td>"
  rr "<span style='position:relative; height:30; top:5;'>"
  rr "<iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=f1.file></iframe>"
  rr "</span>"
  rr "<input type=text maxlength=200 id=file name=file style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;"
  rr "<input type=button style='height:30; width:100; font-weight:bold;' onclick='vbscript: if right(lcase(f1.file.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then f1.submit' style='color:00aa00;' value='~Load CSV~'>"
  rr "</form>"
  rr "</td></tr>"
  rr "</table>"
  rend
end if

dim findcloseststationdistance
function findcloseststation(byval address, byval mode, byval workerid, byval workersiteid)
  findcloseststation = 0 : findcloseststationdistance = -1
  dim s,sq,a,b,c,d,e,v,x,t,rs,rs1,rs2,y,xx,yy,n,m,a1,a2,u,st,ii,i,sitecoursestations,kk
  du = getsdict("")
  a = trim("" & address)
  if a <> "" then
    t = GetAddressXY(a)
    if instr(t, ",") > 0 then
      x = left(t, instr(t,",")-1)
      y = mid(t, instr(t,",")+1)
      if getfield("action") <> "loadcsv" then rr " - " & x & "," & y
      if getfield("stationtype") = "connected" then du = getconnectedstations()
      '---find area
      if mode = "findareastation" then
        s = "select id, title from areas"
        set rs1 = conn.execute(sqlfilter(s))
        do until rs1.eof
          xx = getfieldlist("select longitude from polygons where areaid = " & rs1("id"))
          yy = getfieldlist("select latitude from polygons where areaid = " & rs1("id"))
          if inpolygon(x, y, xx, yy) then
            n = translate(rs1("title") & " ~Area~") : if session("elang") = "2" then n = "���� " & rs1("title")
            s = "select id from stations where title = '" & n & "'"
            set rs2 = conn.execute(sqlfilter(s))
            v = 0 : if not rs2.eof then v = rs2("id") : if isnull(v) then v = 0
            if cstr(v) = "0" then
              sq = "insert into stations(title,special,areaid,siteid) values('" & n & "','on'," & rs1("id") & ",0)"
              closers(rs2) : set rs2 = conn.execute(sqlfilter(sq))
              sq = "select max(id) as id from stations"
              closers(rs2) : set rs2 = conn.execute(sqlfilter(sq))
              v = rs2("id")
            end if
            rr " <span style='color:#ff0000;'>" & n & "</span>"
            findcloseststation = v
          end if
          rs1.movenext
        loop

      '---find closest station
      else
        a = getval(sdict, "sids") : v = 0 : m = -1
        if hasmatch(specs, "routes,comfordeglo,") and session("usersiteid") <> "0" then ii = getidlist("select id from stations where siteid in(0," & session("usersiteid") & ")") : a = bothmatch(a, ii)
        do until a = ""
          b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
          i = getval(sdict, b & "x") : j = getval(sdict, b & "y") : s = getval(sdict, b & "s")
          d = (abs(cdbl(i - x)) ^ 2 + abs(cdbl(j - y)) ^ 2) ^ 0.5
          e = 0
          if sitecoursestations <> "" then
            t = getval(sitecoursestations, workersiteid)
            if instr("," & t, "," & b & ",") > 0 then e = 1
          else
            if s = "0" or s = workersiteid & "" then e = 1
          end if
          if getfield("stationtype") = "connected" and not hasmatch(session("connectedstations"), b) then e = 0
          if kk <> "" and not hasmatch(kk, b) then e = 0
          if (m = -1 or d < m) and e = 1 then
            m = d : v = b
            findcloseststation = v
            'findcloseststationdistance = calcdistance(i,j,x,y)
            findcloseststationdistance = findwalk(j,i,y,x)
          end if
        loop
      end if

      '---find walking distance
      if getparam("UseGoogleMaps") = "y" and cstr(v) <> "0" and cstr(v) <> "" and cstr(workerid) <> "0" then
        du = findwalkingdistance(workerid, v, address)
      end if

    end if
  end if
end function

dim workerxys
function createstations(byval ids, byval maxmeters, byval stationtype)
  dim du,www,che,ww,xa,xb,xc,ya,yb,yc,addedstations,cc,ys,xs,rs,rs1,rs2,title,siteid
  dim a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,walk
  maxmeters = strfilter(maxmeters, "0123456789") : if maxmeters = "" then maxmeters = 1000
  addedstations = "" : cc = 0
  workerxys = "|"
  s = "select id, data1 from workers where id in(" & ids & "0)"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    d = ctxt("" & rs("data1")) : x = getval(d, "x") : y = getval(d, "y")
    workerxys = workerxys & rs("id") & "x=" & x & "|" & rs("id") & "y=" & y & "|"
    if x = "" or y = "" then
       du = showerror(getworkername(rs("id")) & " - ~cannot find GEO location~")
       ids = nomatch(ids, rs("id"))
    end if
    rs.movenext
  loop
  ww = ids

  '---
  do until ww = ""
    w = left(ww,instr(ww,",")-1) : ww = mid(ww,instr(ww,",")+1)
    workercity = gettitle("stations", getonefield("select citystationid from workers where id = " & w))
    if specs = "jr" and hasmatch(workercity, "����� �����,���,�����,����,��� ����,����,����,������,��� ���,���� ������,�� �����,") then

      '---find existing with pipe
      du = getsdict("") : a = getidlist("select id from stations where instr('' & cstr('' & title), '') > 0" )
      '--
      if specs = "jr" then du = getsdict("") : a = getidlist("select id from stations where instr('' & cstr('' & title), '|') > 0" )
      '--
      v = 0 : m = 99999999
      do until a = ""
        b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
        d = calcdistance(getval(workerxys, w & "y"), getval(workerxys, w & "x"), getval(sdict, b & "y"), getval(sdict, b & "x"))
        if d < m then m = d : v = b
      loop
      if v > 0 then m = findwalk(getval(workerxys, w & "y"), getval(workerxys, w & "x"), getval(sdict, v & "y"), getval(sdict, v & "x"))
      if v > 0 and m >= 0 and m <= cdbl(maxmeters) then
        s = "update workers set from1 = " & v & " where id = " & w
        closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
        addedstations = addedstations & v & ","
        rr getworkername(w) & " - added to station - " & gettitle("stations", v) & "<br>"

      else
        title = ""
        if stationtype = "|" then
          du = showerror(getworkername(w) & " - Cannot find a STATION close enough")
          s = "update workers set from1 = 0 where id = " & w
          closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
        else
          lat = getval(workerxys, w & "y") : lon = getval(workerxys, w & "x")
          if cdbl(maxmeters) > 1 then title = getxyaddress(lat, lon)
          if title = "" then title = tocsv(getonefield("select address from workers where id = " & w))
        end if
      end if
 
    else
      currentaddress = "" : thexy = ""
      currentaddress = getonefield("select address from workers where id = "& w)
      sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(currentaddress) & "'"' and siteid in(" & siteid & ",0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      if rs1.eof then v1 = 0 else v1 = rs1("id")
      if v1 = 0 and docreatestations = true then
        '---
        thexy = GetAddressXY(currentaddress)
        if instr(thexy, ",") > 0 then
          markerx = left(thexy, instr(thexy,",")-1)
          markery = mid(thexy, instr(thexy,",")+1)
        end if
        '---
        sq = "insert into stations(title,inmap,markerx,markery,siteid) values('" & currentaddress & "','" & currentaddress & "','"& markerx &"','"& markery &"',0)"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
        sq = "select max(id) as id from stations"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
        v1 = rs1("id")
      end if       
      s = "update workers set from1 = "& v1 &" where id = " & w
      closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
    end if

    if getparam("UseGoogleMaps") = "y" then
      s = "select id,from1,to1,address,data1 from workers where id = " & w
      set rs1 = conn.execute(sqlfilter(s))
      du = findwalkingdistance(rs1("id"), rs1("from1"), rs1("address") & "")
      if hasmatch(specs, "jerusalem,muni,") and cstr(rs1("from1")) <> "0" and cstr(rs1("to1")) <> "0" then
        data1 = rs1("data1")
        t = stationcourse(rs1("from1") & "," & rs1("to1") & ",", "", 0, now, "", "")
        t = getval(t, "legdisa") : if isnumeric(t) then t = fix(cdbl(t) / 1000)
        data1 = setval(data1, "������� ����� �����", t)
        s = "update workers set data1 = '" & data1 & "' where id = " & w
        set rs1 = conn.execute(sqlfilter(s))
      end if
    end if
  loop

  if request_action = "createstations" then
    rr "OK - ~total~ " & countstring(addedstations, ",") & " ~Stations~, " & cc & " ~New~<br>"
    session("map_workerids") = ids
    session("map_stationids") = addedstations
    rr "<a href=map.asp>~Map~</a>"
  end if
end function

function findwalkingdistance(byval workerid, byval stationid, byval address)
  dim a1,a2,s,sq,rs,rs1,rs2,u,a,e,data1,x,y,j,k
  e = ""
  a1 = address
  d = getonefield("select data1 from workers where id = " & workerid) : x = getval(d, "x") : y = getval(d, "y")
  if isnumeric(x) and isnumeric(y) then a1 = y & "," & x
  if useescorts = "on" then stationid = "0" & getonefield("select to1 from workers where id = " & workerid)
  s = "select markerx, markery, title, inmap from stations where id = " & stationid
  set rs = conn.execute(sqlfilter(s))
  if NOT rs.eof then j = rs("markerx") : k = rs("markery") : a2 = rs("inmap") : if a2 = "" then a2 = rs("title")
  if isnumeric(j) and isnumeric(k) then a2 = k & "," & j
  'if isnumeric(x) and isnumeric(y) and isnumeric(j) and isnumeric(k) then
  '  e = calcdistance(x,y,j,k)
  if a1 <> "" and a2 <> "" then
    u = "http://maps.googleapis.com/maps/api/distancematrix/xml?mode=walking&language=us-EN&sensor=false"
    u = u & "&origins=" & curlall(cutf8(a1))
    u = u & "&destinations=" & curlall(cutf8(a2))
    u = u & "&client=gme-moovex"
    u = u & "&signature=" & getpage("http://axisis.net/trans/_maps_url_signiture.php?u=" & curlall(u))
    a = getpage(u)
    a = utf2ansi(a)
    e = getfromto(a, "<distance>", "<value>", "</value>", false, false)
  end if
  if e <> "" then
    rr " walking" & e & "<br>"
    s = "select data1 from workers where id = " & workerid
    set rs = conn.execute(sqlfilter(s))
    if NOT rs.eof then
      data1 = rs("data1") & ""
      data1 = setval(data1, "walk", e)
      s = "update workers set data1 = '" & data1 & "' where id = " & workerid
      set rs1 = conn.execute(sqlfilter(s))
    end if
  end if
  findwalkingdistance = e
end function

function findwalk(lat1, lon1, lat2, lon2)
  dim u, a
  findwalk = -1
  u = "http://maps.googleapis.com/maps/api/distancematrix/xml?mode=walking&language=us-EN&sensor=false"
  u = u & "&origins=" & lat1 & "," & lon1
  u = u & "&destinations=" & lat2 & "," & lon2
  u = u & "&client=gme-moovex"
  u = u & "&signature=" & getpage("http://axisis.net/trans/_maps_url_signiture.php?u=" & curlall(u))
  a = getpage(u)
  a = utf2ansi(a)
  a = getfromto(a, "<distance>", "<value>", "</value>", false, false)
  if isnumeric(a) then findwalk = cdbl(a)
end function

function findarea(byval ids)
  dim s,rs,y,x,aa,v,m,b,i,a,j,d,c,sq,rs1
  du = getsdict("")
  s = "select id,markerx,markery,title from stations where id in(" & ids & "-1)"
  s = s & okreadsql("stations")
  set rs = server.createobject("adodb.recordset")
  if isobject(rs) then if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 0, 1, 1 'read only
  set rs = conn.execute(sqlfilter(s))
  c = 0
  do until rs.eof
    y = rs("markery") & "" : x = rs("markerx") & ""
    if isnumeric(y) and isnumeric(x) then
      aa = getval(sdict, "sids") : v = 0 : m = 99999999
      do until aa = ""
        b = left(aa,instr(aa,",")-1) : aa = mid(aa,instr(aa,",")+1)
        i = getval(sdict, b & "x") : j = getval(sdict, b & "y")
        a = getval(sdict, b & "a")
        if isnumeric(i) and isnumeric(j) and isnumeric(a) and a <> "0" then
          'd = (abs(cdbl(i - x)) ^ 2 + abs(cdbl(j - y)) ^ 2) ^ 0.5
          d = calcdistance(y, x, j, i)
          if d < m then m = d : v = b
        end if
      loop
      if v > 0 then
        c = c + 1
        a = getval(sdict, v & "a")
        s = "update stations set areaid = 0" & a & " where id = " & rs("id")
        set rs1 = conn.execute(sqlfilter(s))
      end if
    end if
    rs.movenext
  loop
end function

rrbottom()
rend()
%>