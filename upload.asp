<!--#include file="include.asp"-->
<%
if session("userid") = "" then x = errorend("Access Denied")
session.timeout = 240 'minutes
server.scripttimeout = 240 * 60 'seconds
rr("<body rightmargin=0 leftmargin=0 bottommargin=0 topmargin=0 scroll=no>")
'------show form-----------------------------------------------------------------------
t = ""
t = t & "&box=" & getfield("box")
t = t & "&previewdiv=" & getfield("previewdiv")
rr("<form name=f1 method=POST enctype=multipart/form-data action=?act=upload" & t & ">")
rr("<input type=file name=file1 style='font-family:arial; font-size:11px; height:30px; margin-left:-2; width:0px;' onchange='vbscript: f1.submit'>")
rr("</form>")

'-----------the upload------------------------------------------------------------------
if getfield("act") = "upload" then
  'UploadAllowedExtentions = getparam("UploadAllowedExtentions")
  UploadAllowedExtentions = "jpg,gif,csv,doc,txt,pdf,ppt,xls,rtf,docx,pptx,xlsx,zip,rar,png," 'or "all"
  if highsecurity = "on" and specs <> "rafael" then UploadAllowedExtentions = "csv,"
  if specs = "iai" then UploadAllowedExtentions = "csv,pdf,"
  UploadAllowedSize = 5000000 'getparam("UploadAllowedSize")
  box = getfield("box") : if instr(box,".") = 0 then box = "f1." & box
  previewdiv = getfield("previewdiv")
  uploadfolder = "upload/"

  ''-----------create folder------------
  'path = getfield("path") : path = replace(path, "..", "/\ACCESS DENIED/\")
  'if path <> "" then
  '  uploadfolder = uploadfolder & "/" & path & "/" : uploadfolder = replace(uploadfolder,"//","/")
  '  a = uploadfolder
  '  t = ""
  '  do until a = ""
  '    b = left(a,instr(a,"/")-1) : a = mid(a,instr(a,"/")+1)
  '    t = t & b & "/"
  '    if not fs.folderexists(server.mappath(t)) then fs.createfolder(server.mappath(t))
  '  loop
  'end if

  '-----save file------------------
  Set chunk = CreateObject("ADODB.Stream") : chunk.Type = 1
  Set tempchunk = CreateObject("ADODB.Stream") : tempchunk.Type = 1
  Set full = CreateObject("ADODB.Stream") : full.Type = 1'1=binary /2=text
  full.open
  TotalSize = Request.TotalBytes
  ChunkSize = 65536
  If ChunkSize > TotalSize Then ChunkSize = TotalSize
  BytesLeft = TotalSize
  BytesRead = 0
  c = 0
  Do While BytesLeft > 0
    c = c + 1
    'If BytesLeft < ChunkSize Then ChunkSize = BytesLeft
    If BytesLeft < ChunkSize + 200 Then ChunkSize = BytesLeft 'make sure finish separator is nut split into 2 chunks!
    chunk.open
    chunk.Write Request.BinaryRead(ChunkSize)
    BytesLeft = BytesLeft - ChunkSize
    BytesRead = BytesRead + ChunkSize
    chunk.position = 0
    if c = 1 then
      '---find seperator
      chunk.position = 0
      t = chunk.read
      x = instrb(1, t, stringtobinary(vbcrlf)) - 1
      sep = leftb(t,x)

      '---find filename
      chunk.position = 0
      t = chunk.read
      x = instrb(1, t, stringtobinary("filename=""")) + 9
      y = instrb(x, t, stringtobinary(vbcrlf)) - 2
      chunk.position = x
      t = chunk.read(y - x)
      f = binarytostring(t)
      f = mid(f,instrrev(f,"\")+1)

      '---crop from data start-------
      chunk.position = 0
      t = chunk.read
      x = instrb(1, t, stringtobinary("filename="))
      x = instrb(x, t, stringtobinary(vbcrlf & vbcrlf)) + 3
      chunk.position = x
      tempchunk.open : chunk.copyto tempchunk : chunk.close : chunk.open : tempchunk.position = 0 : tempchunk.copyto chunk : tempchunk.close
    end if

    '----crop until ending seperator--------
    chunk.position = 0
    t = chunk.read
    y = instrb(1, t, sep)
    if y > 3 then
      chunk.position = 0 : chunk.copyto full, y - 3 'only until seperator
    else
      chunk.position = 0 : chunk.copyto full        'all chunk
    end if
    chunk.close

    'response.write BytesRead & "." 
    'response.flush
  Loop

  '-------------limitations--------------------------------------
  f = replace(f,",",".")
  f = uploadfolder & f
  f = replace(f, "..", "/\ACCESS DENIED/\")
  f = server.mappath(f)
  'f = replace(f,"\httpsdocs\","\httpdocs\") '---SSL routing
  f = freefilename(f)

  UploadAllowedExtentions = "," & lcase(UploadAllowedExtentions) & ","
  UploadAllowedExtentions = replace(UploadAllowedExtentions," ","")
  if instr(UploadAllowedExtentions,"," & lcase(mid(f,instrrev(f,".")+1)) & ",") = 0 and UploadAllowedExtentions <> ",all," then
    t = UploadAllowedExtentions : t = mid(t,2) : t = left(t,len(t)-1) : t = replace(t,",",", ")
    rr("<script language=vbscript>msgbox ""you can only upload files of these types: " & t & """</script>")
    response.end
  end if
  'UploadDisallowedExtentions = "," & lcase(UploadDisallowedExtentions) & ","
  'UploadDisallowedExtentions = replace(UploadDisallowedExtentions," ","")
  'if instr(UploadDisallowedExtentions,"," & mid(lcase(f),instrrev(f,".")+1) & ",") > 0 then
  '  t = UploadDisallowedExtentions : t = mid(t,2) : t = left(t,len(t)-1) : t = replace(t,",",", ")
  '  rr("<script language=vbscript>msgbox ""you cannot upload files of these types: " & t & "."" & vbcrlf & ""You can upload a ZIP of the files instead.""</script>")
  '  response.end
  'end if

  x = full.size
  if x > cdbl(UploadAllowedSize) then
    rr("<script language=vbscript>msgbox ""uploaded file too big (max " & UploadAllowedSize & ")""</script>")
    response.end
  end if

  '---check contents
  if highsecurity = "on" then
    full.position = 0 : a = full.read(full.size) : b = BinaryToString(a)
    b = replace(b," ","") : b = replace(b,chr(9),"") : b = replace(b,chr(10),"") : b = replace(b,chr(13),"")
    if instr(1, b, "<script", 1) > 0 then
      rr "<script language=vbscript> msgbox ""Invalid File Uploaded"" </script>"
      response.end
    end if
  end if
  Function BinaryToString(byval Binary)
    Dim I, S, st
    set st = Server.CreateObject("ADODB.Stream") : st.open
    For I = 1 To LenB(Binary)
      st.writetext(Chr(AscB(MidB(Binary, I, 1))))
    Next
    st.position = 0 : SimpleBinaryToString = st.readtext(st.size) : st.close
  End Function

  '-----------save------------
  full.SaveToFile f
  f = uploadfolder & mid(f,instrrev(f,"\")+1)
  full.close

  '--------update parent
  rr("<script language=vbscript>")
  rr("f = """ & f & """")
  rr(":msgbox ""Uploaded "" & f")
  rr(":parent." & box & ".value = f")
  if previewdiv <> "" and (right(f,4) = ".jpg" or right(f,4) = ".gif" or right(f,4) = ".png") then
    t = "<a href=`" & f & "` target=_blank><img src=`" & f & "` width=100 border=0></a>"
    rr(":parent." & previewdiv & ".innerhtml = """ & replace(t,"`","""""") & """")
    '''rr(":parent.document.getelementbyid(""strechmyiframe"").click")
    rr(":x = setTimeout(""parent.document.getelementbyid(""""strechmyiframe"""").click"",100,""vbscript"")")
  elseif previewdiv <> "" then
    rr(":parent." & previewdiv & ".innerhtml = ""<a href='" & f & "' target=_blank><font style='font-size:18;'><b>" & ucase(mid(f,instrrev(f,".")+1)) & "</a>""")
  end if
  rr(":on error resume next:parent.document.getelementbyid(""strechmyiframe"").click" & vbcrlf)

  '---insert name into title
  't = f : t = mid(t,instrrev(t,"/")+1) : if instr(t,"_") > 0 then t = left(t,instr(t,"_")-1)
  'rr(":if instr(parent.document.location,""admin_attachments.asp"") > 0 and parent.f1.title.value = """" then parent.f1.title.value = """ & t & """")

  rr("</script>")

end if

'--------binary functions -------------------------------------------------------------------
function stringtobinary(s)
  dim i, b
  for i = 1 to len(s)
    b = b & chrb(asc(mid(s,i,1)))
  next
  stringtobinary = B
end function

Function BinaryToString(Binary)
  Dim TempString 
  On Error Resume Next
  TempString = RSBinaryToString(Binary)
  If Len(TempString) <> LenB(Binary) then'Conversion error
    TempString = MBBinaryToString(Binary)
  end if
  BinaryToString = TempString
End Function

Function MBBinaryToString(Binary)
  dim cl1, cl2, cl3, pl1, pl2, pl3
  Dim L', nullchar
  cl1 = 1
  cl2 = 1
  cl3 = 1
  L = LenB(Binary)
  Do While cl1<=L
    pl3 = pl3 & Chr(AscB(MidB(Binary,cl1,1)))
    cl1 = cl1 + 1
    cl3 = cl3 + 1
    if cl3>300 then
      pl2 = pl2 & pl3
      pl3 = ""
      cl3 = 1
      cl2 = cl2 + 1
      if cl2>200 then
        pl1 = pl1 & pl2
        pl2 = ""
        cl2 = 1
      End If
    End If
  Loop
  MBBinaryToString = pl1 & pl2 & pl3
End Function

Function RSBinaryToString(xBinary)
	Dim Binary
	if vartype(xBinary) = 8 then Binary = MultiByteToBinary(xBinary) else Binary = xBinary
  Dim RS, LBinary
  Const adLongVarChar = 201
  Set RS = CreateObject("ADODB.Recordset")
  LBinary = LenB(Binary)
	if LBinary>0 then
		RS.Fields.Append "mBinary", adLongVarChar, LBinary
		RS.Open
		RS.AddNew
			RS("mBinary").AppendChunk Binary 
		RS.Update
		RSBinaryToString = RS("mBinary")
	Else
		RSBinaryToString = ""
	End If
End Function

Function MultiByteToBinary(MultiByte)
  Dim RS, LMultiByte, Binary
  Const adLongVarBinary = 205
  Set RS = CreateObject("ADODB.Recordset")
  LMultiByte = LenB(MultiByte)
	if LMultiByte>0 then
		RS.Fields.Append "mBinary", adLongVarBinary, LMultiByte
		RS.Open
		RS.AddNew
			RS("mBinary").AppendChunk MultiByte & ChrB(0)
		RS.Update
		Binary = RS("mBinary").GetChunk(LMultiByte)
	End If
  MultiByteToBinary = Binary
End Function

%>


