<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 03/11/2014 15:04:09

function shiftsdelitem(i)
  shiftsdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function shiftsinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "on" : if getfield("active") <> "" then t = getfield("active")
  f = f & "active," : v = v & "'" & t & "',"

  t = "00:00" : if getfield("hour1") <> "" then t = getfield("hour1")
  f = f & "hour1," : v = v & "'" & t & "',"

  t = "00:00" : if getfield("hour1b") <> "" then t = getfield("hour1b")
  f = f & "hour1b," : v = v & "'" & t & "',"

  t = "AB" : if getfield("ways") <> "" then t = getfield("ways")
  f = f & "ways," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  t = session("usersiteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
']
  f = f & "siteid," : v = v & t & ","

  t = "" : if getfield("unitids") <> "" then t = getfield("unitids")
  f = f & "unitids," : v = v & "'" & t & "',"

'[  t = "1,2,3,4,5,6,7,8,9," : if getfield("days") <> "" then t = getfield("days")
  t = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays") : if getfield("days") <> "" then t = getfield("days")
']
  f = f & "days," : v = v & "'" & t & "',"

  t = "" : if getfield("courseids") <> "" then t = getfield("courseids")
  f = f & "courseids," : v = v & "'" & t & "',"

  t = "" : if getfield("special") <> "" then t = getfield("special")
  f = f & "special," : v = v & "'" & t & "',"

  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into shifts(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from shifts"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  shiftsinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table shifts (id autoincrement primary key,title text(50),active text(2),hour1 text(5),hour1b text(5),ways text(2),siteid number,unitids memo,days text(50),courseids memo,special text(2),data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "shifts" : session("tablename") = "shifts"
tablefields = "id,title,active,hour1,hour1b,ways,siteid,unitids,days,courseids,special,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
if session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"
session("shiftdata1s") = ""
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = shiftsinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
  msg = msg & " ~Item Updated~"
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("active") then s = s & "active='" & getfield("active") & "',"
  if okedit("hour1") then s = s & "hour1='" & getfield("hour1") & "',"
  if okedit("hour1b") then s = s & "hour1b='" & getfield("hour1b") & "',"
  if okedit("ways") then s = s & "ways='" & getfield("ways") & "',"
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  if okedit("unitids") then s = s & "unitids='" & getfield("unitids") & "',"
  if okedit("days") then s = s & "days='" & getfield("days") & "',"
  if okedit("courseids") then s = s & "courseids='" & getfield("courseids") & "',"
  if okedit("special") then s = s & "special='" & getfield("special") & "',"
'[  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  sq = "select data1 from " & tablename & " where id = " & request_id
  set rs1 = conn.execute(sqlfilter(sq))
  data1 = "" & rs1("data1")
  data1 = setval(data1, "shiftids", getfield("shiftids"))
  data1 = setval(data1, "cityids", getfield("cityids"))
  data1 = setval(data1, "lockhour", getfield("lockhour"))
  data1 = setval(data1, "daysminimum", strfilter(getfield("daysminimum"), "0123456789"))
  data1 = setval(data1, "daysahead", strfilter(getfield("daysahead"), "0123456789"))
  data1 = setval(data1, "defaultfood", getfield("defaultfood"))
  t = uniquelist(getfield("defaultfood") & "," & getfield("foodsupplierids")) : if left(t,1) = "," then t = mid(t,2)
  data1 = setval(data1, "foodsupplierids", t)
  data1 = setval(data1, "reportok", getfield("reportok"))
  data1 = setval(data1, "notmobile", getfield("notmobile"))
  data1 = setval(data1, "notforworkers", getfield("notforworkers"))
  data1 = setval(data1, "overtime", getfield("overtime"))
  data1 = setval(data1, "from1", getfield("from1"))
  data1 = setval(data1, "to1", getfield("to1"))
  data1 = setval(data1, "loaddef", getfield("loaddef"))
  data1 = setval(data1, "loadmom", getfield("loadmom"))
  data1 = setval(data1, "loadshort", getfield("loadshort"))
  data1 = setval(data1, "loaddep", getfield("loaddep"))
  if okedit("data1") then s = s & "data1='" & data1 & "',"
']
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'['-------------afterupdate-------------

if request_action2 = "setweekplanbyshifts" then
  s = "select id,shiftids from workers where instr(cstr(',' & shiftids), '," & request_id & ",') > 0"
  set rs = conn.execute(sqlfilter(s))
  c = 0
  do until rs.eof
    c = c + 1
    du = setweekplanbyshifts(rs("id"), rs("shiftids"))
    rs.movenext
  loop
  request_action = "form"
  msg = msg & " ����� ������ ������ ���� " & c & " �������"
end if

if request_action2 = "copydays" then
  s = "update courses set days = '" & getfield("days") & "' where id in(" & getfield("courseids") & "0)"
  set rs = conn.execute(sqlfilter(s))
  request_action = "form"
  msg = msg & " ��� ������� ������"
end if
']
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = shiftsdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = shiftsdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
'[      s = ""
      s = ""
      if okedit("days") then s = s & ",days = '" & sortlist(getfield("listupdate_days"), "number") & "'"
']
      if okedit("active") then s = s & ",active = '" & getfield("listupdate_active_" & b) & "'"
      if okedit("special") then s = s & ",special = '" & getfield("listupdate_special_" & b) & "'"
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~shifts_title~^"
  if oksee("active") then csv(0) = csv(0) & "~shifts_active~^"
  if oksee("hour1") then csv(0) = csv(0) & "~shifts_hour1~^"
  if oksee("hour1b") then csv(0) = csv(0) & "~shifts_hour1b~^"
  if oksee("ways") then csv(0) = csv(0) & "~shifts_ways~^"
  if oksee("siteid") then csv(0) = csv(0) & "~shifts_siteid~^"
  if oksee("unitids") then csv(0) = csv(0) & "~shifts_unitids~^"
'[  if oksee("days") then csv(0) = csv(0) & "~shifts_days~^"
  if oksee("days") then csv(0) = csv(0) & "~shifts_days~^"
  if specs = "misgav" or brenerclasses then
    csv(0) = csv(0) & "���� �����^"
    csv(0) = csv(0) & "���� ����^"
  end if
']
  if oksee("courseids") then csv(0) = csv(0) & "~shifts_courseids~^"
  if oksee("special") then csv(0) = csv(0) & "~shifts_special~^"
  if oksee("data1") then csv(0) = csv(0) & "~shifts_data1~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
  Thehour1b = rs("hour1b") : if isnull(Thehour1b) then Thehour1b = ""
  Theways = rs("ways") : if isnull(Theways) then Theways = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
  Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
  Thecourseids = rs("courseids") : Thecourseids = formatidlist(Thecourseids)
  Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("active") then csv(rc) = csv(rc) & replace(theactive,vbcrlf," ") & "^"
    if oksee("hour1") then csv(rc) = csv(rc) & replace(thehour1,vbcrlf," ") & "^"
    if oksee("hour1b") then csv(rc) = csv(rc) & replace(thehour1b,vbcrlf," ") & "^"
    if oksee("ways") then csv(rc) = csv(rc) & replace(theways,vbcrlf," ") & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
      t = "" : d = formatidlist(theunitids)
      sq = "select * from units where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    if oksee("unitids") then csv(rc) = csv(rc) & t & "^"
'[    if oksee("days") then csv(rc) = csv(rc) & replace(thedays,vbcrlf," ") & "^"
    if oksee("days") then csv(rc) = csv(rc) & replace(thedays,vbcrlf," ") & "^"
    if specs = "misgav" or brenerclasses then
      csv(rc) = csv(rc) & tocsv(gettitle("stations", getval(thedata1, "from1"))) & "^"
      csv(rc) = csv(rc) & tocsv(gettitle("stations", getval(thedata1, "to1"))) & "^"
    end if
']
      t = "" : d = formatidlist(thecourseids)
      sq = "select * from courses where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & " " & rs1("code")
        t = t & " " & rs1("title")
        t = t & ","
        rs1.movenext
      loop
      t = mid(t,2)
    if oksee("courseids") then csv(rc) = csv(rc) & t & "^"
    if oksee("special") then csv(rc) = csv(rc) & replace(thespecial,vbcrlf," ") & "^"
    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      siteid = session("usersiteid")
      data1 = ""
      data1_from1 = ""
      data1_to1 = ""
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "title" then
          if len(b) > 50 then b = left(b,50)
          if okedit("title") then s = s & "title='" & b & "',"
        elseif csvff(i) = "active" then
          if len(b) > 2 then b = left(b,2)
          if okedit("active") then s = s & "active='" & b & "',"
        elseif csvff(i) = "hour1" then
'[          if len(b) > 5 then b = left(b,5)
'[          if okedit("hour1") then s = s & "hour1='" & b & "',"
          if len(b) > 5 then b = left(b,5)
          if len(b) = 4 then b = "0" & b
          if not ishour(b) then b = "00:00"
          if okedit("hour1") then s = s & "hour1='" & b & "',"
']
        elseif csvff(i) = "hour1b" then
'[          if len(b) > 5 then b = left(b,5)
'[          if okedit("hour1b") then s = s & "hour1b='" & b & "',"
          if len(b) > 5 then b = left(b,5)
          if len(b) = 4 then b = "0" & b
          if not ishour(b) then b = "00:00"
          if okedit("hour1b") then s = s & "hour1b='" & b & "',"
				elseif csvff(i) = "from1" or csvff(i) = "���� �����" then
				  sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
				  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
				  if rs1.eof then v = 0 else v = rs1("id")
				  data1_from1 = v
				elseif csvff(i) = "to1" or csvff(i) = "���� ����" then
				  sq = "select * from stations where lcase(cstr('' & title)) = '" & lcase(b) & "'"
				  closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
				  if rs1.eof then v = 0 else v = rs1("id")
          data1_to1 = v
']
        elseif csvff(i) = "ways" then
          if len(b) > 2 then b = left(b,2)
          if okedit("ways") then s = s & "ways='" & b & "',"
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
'[          if okedit("siteid") then s = s & "siteid=" & v & ","
          if okedit("siteid") then s = s & "siteid=" & v & ","
          if session("usersiteid") = "0" then siteid = v
']
        elseif csvff(i) = "unitids" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
'[              sq = "select * from units where lcase(cstr('' & title)) = '" & lcase(t) & "'"
              sq = "select * from units where lcase(cstr('' & title)) = '" & lcase(t) & "' and siteid = " & siteid
']
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("unitids") then s = s & "unitids='" & v & "',"
        elseif csvff(i) = "days" then
          if len(b) > 50 then b = left(b,50)
          if okedit("days") then s = s & "days='" & b & "',"
        elseif csvff(i) = "courseids" then
          v = "" : b = b & ","
          do until b = ""
            t = trim(left(b,instr(b,",")-1)) : b = mid(b,instr(b,",")+1)
            if t <> "" then
              sq = "select * from courses where lcase(cstr('' & code)) & ' ' & lcase(cstr('' & title)) = '" & lcase(t) & "'"
              closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
              if not rs1.eof then v = v & rs1("id") & ","
            end if
          loop
          if okedit("courseids") then s = s & "courseids='" & v & "',"
        elseif csvff(i) = "special" then
          if len(b) > 2 then b = left(b,2)
          if okedit("special") then s = s & "special='" & b & "',"
        elseif csvff(i) = "data1" then
'[          if okedit("data1") then s = s & "data1='" & b & "',"
          if okedit("data1") then s = s & "data1='" & b & "',"
          data1 = b

']
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next

      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
'[        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = shiftsinsertrecord
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = shiftsinsertrecord
        if data1 = "" then data1 = getonefield("select data1 from workers where id = " & theid)
        if data1_from1 <> "" then data1 = setval(data1, "from1", data1_from1)
        if data1_to1 <> "" then data1 = setval(data1, "to1", data1_to1)
        s = s & "data1='" & data1 & "',"
']
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","shifts.id,shifts.title,shifts.active,shifts.hour1,shifts.hour1b,shifts.ways,shifts.siteid,shifts.unitids,shifts.days,shifts.courseids,shifts.special,shifts.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
  Thehour1b = rs("hour1b") : if isnull(Thehour1b) then Thehour1b = ""
  Theways = rs("ways") : if isnull(Theways) then Theways = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
  Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
  Thecourseids = rs("courseids") : Thecourseids = formatidlist(Thecourseids)
  Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "title,active,hour1,hour1b,ways,siteid,unitids,unitids_select,unitids_trigger,days,courseids,courseids_select,courseids_trigger,special,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/refresh.png' ~iconsize~> ~shifts_shifts~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~shifts_title~<img src=must.gif></span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  end if'form:title

  if oksee("active") then'form:active
    rr("<tr valign=top id=active_tr0><td class=form_item1 id=active_tr1><span id=active_caption>~shifts_active~</span><td class=form_item2 id=active_tr2>")
    rr("<input type=checkbox name=active")
    if theactive = "on" then rr(" checked")
    rr(">")
  end if'form:active

  if oksee("hour1") then'form:hour1
    rr("<tr valign=top id=hour1_tr0><td class=form_item1 id=hour1_tr1><span id=hour1_caption>~shifts_hour1~<img src=must.gif></span><td class=form_item2 id=hour1_tr2>")
'[    rr("<input type=text maxlength=5 name=hour1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thehour1 & """ dir=ltr>")
    rr("<input type=text maxlength=5 name=hour1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:50;' value=""" & Thehour1 & """ dir=ltr>")
']
  end if'form:hour1

  if oksee("hour1b") then'form:hour1b
    rr("<tr valign=top id=hour1b_tr0><td class=form_item1 id=hour1b_tr1><span id=hour1b_caption>~shifts_hour1b~<img src=must.gif></span><td class=form_item2 id=hour1b_tr2>")
'[    rr("<input type=text maxlength=5 name=hour1b onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thehour1b & """ dir=ltr>")
'[  end if'form:hour1b
    rr("<input type=text maxlength=5 name=hour1b onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:50;' value=""" & Thehour1b & """ dir=ltr>")
  end if'form:hour1b
  rr("<tr valign=top><td class=form_item1>~Lock Hour~<td class=form_item2>")
  rr "<input type=text name=lockhour dir=ltr value='" & getval(thedata1, "lockhour") & "' style='width:50;'> <span style='font-size:10; color:#aaaaaa;'> ��� ���� �������� �����</span>"
  rr("<tr valign=top><td class=form_item1>������� ���� �����<td class=form_item2>")
  rr "<input type=text name=daysminimum dir=ltr value='" & getval(thedata1, "daysminimum") & "' style='width:50;'> <span style='font-size:10; color:#aaaaaa;'> 0 = ����, 1 = ���, ���'</span>"
  rr("<tr valign=top><td class=form_item1>������� ���� �����<td class=form_item2>")
  rr "<input type=text name=daysahead dir=ltr value='" & getval(thedata1, "daysahead") & "' style='width:50;'> <span style='font-size:10; color:#aaaaaa;'> 0 = ����, 1 = ���, ���'</span>"
']

  if oksee("ways") then'form:ways
    rr("<tr valign=top id=ways_tr0><td class=form_item1 id=ways_tr1><span id=ways_caption>~shifts_ways~</span><td class=form_item2 id=ways_tr2>")
'[    a = "AB,A,B,"
    waysoptions = "AB,A,B,X,"
    a = waysoptions
']
    rr("<select name=ways dir=~langdir~>")
    do until a = ""
      b = left(a,instr(a,",")-1)
      a = mid(a,instr(a,",")+1)
      rr("<option value=""" & b & """")
      if Theways = b then rr(" selected")
      rr(">" & b)
    loop
    rr("</select>")'form:ways
  end if'form:ways

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~shifts_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
'[    rr("<select name=siteid dir=~langdir~>")
    rr("<select name=siteid dir=~langdir~ onchange='vbscript: f1.action2.value = ""stay"" : checkform'>")
']
    rr("<option value=0 style='color:bbbbfe;'>~shifts_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

  if oksee("unitids") then'form:unitids
    rr("<tr valign=top id=unitids_tr0><td class=form_item1 id=unitids_tr1><span id=unitids_caption>~shifts_unitids~</span><td class=form_item2 id=unitids_tr2>")
'[    rr(selectlookupmultiauto("f1.unitids",theunitids, "icons/i_shield.gif", "units,title,", 300, okwritehere))
    t = "" : if cstr(thesiteid) <> "0" then t = ":select id,title from units where siteid = " & thesiteid & " order by title"
    rr(selectlookupmultiauto("f1.unitids",theunitids, "icons/i_shield.gif", "units,title," & t, 300, okwritehere))
']
  end if'form:unitids

  if oksee("days") then'form:days
    rr("<tr valign=top id=days_tr0><td class=form_item1 id=days_tr1><span id=days_caption>~shifts_days~</span><td class=form_item2 id=days_tr2>")
'[    c = 0 : s = "" : a = "1,2,3,4,5,6,7,8,9,"
    c = 0 : s = "" : a = "1,2,3,4,5,6,7," & getparam("ExtraWeekDays")
']
    cc = countstring(a,",")
    do until a = ""
      c = c + 1
      b = left(a,instr(a,",") - 1) : a = mid(a,instr(a,",")+1)
      rr("<input type=checkbox name=days_c" & c)
      if instr("," & thedays & ",","," & b & ",") > 0 then rr(" checked")
      rr(" onclick='vbscript:populatedays'>")
'[      rr(b & " ")
      rr("~weekday" & b & "~&nbsp;&nbsp;&nbsp;")
']
      s = s & "if f1.days_c" & c & ".checked then t = t & """ & b & ",""" & vbcrlf
    loop
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub populatedays" & vbcrlf)
    rr("    t = """"" & vbcrlf)
    rr(s & vbcrlf)
    rr("    f1.days.value = t" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("</script>" & vbcrlf)
    rr("<input type=hidden name=days value=""" & Thedays & """>")
'[  end if'form:days
  end if'form:days

  if specs = "misgav" or brenerclasses then
    rr("<tr valign=top><td class=form_item1>���� �����<td class=form_item2>")
    thefrom1 = getval(thedata1, "from1")
    rr("<select name=from1 dir=~langdir~>")
    rr("<option value=0>")
    sq = "select id,title from stations order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(thefrom1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")

    rr("<tr valign=top><td class=form_item1>���� ����<td class=form_item2>")
    theto1 = getval(thedata1, "to1")
    rr("<select name=to1 dir=~langdir~>")
    rr("<option value=0>")
    sq = "select id,title from stations order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(theto1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")
  end if

']

  if oksee("courseids") then'form:courseids
    rr("<tr valign=top id=courseids_tr0><td class=form_item1 id=courseids_tr1><span id=courseids_caption>~shifts_courseids~</span><td class=form_item2 id=courseids_tr2>")
'[    rr(selectlookupmultiauto("f1.courseids",thecourseids, "icons/upcoming-work.png", "courses,code,title,", 300, okwritehere))
    t = ":select id,code,title from courses where id in(" & thecourseids & "0) or active = 'on'"
    if thesiteid <> "0" then t = t & " and siteid = " & thesiteid
    t = t & " order by code,title"
    rr(selectlookupmultiauto("f1.courseids",thecourseids, "icons/upcoming-work.png", "courses,code,title," & t, 300, okwritehere))

    t = "select id from courses where 0=0"
    if session("usersiteid") <> "0" then t = t & "and id in(" & thecourseids & "0) or siteid = " & thesiteid
    t = getidlist(t)
    if countstring(t,",") <= 120 then
      rr " <a href=#aa onclick='vbscript: if msgbox(""~Select all~?"",vbyesno) = 6 then f1.action2.value = ""stay"" : f1.courseids.value = """ & t & """ : checkform'>~Select All~</a>"
    end if

    if specs = "iai" and session("usergroup") = "admin" then
      rr(" <input type=button value=""���� ��� ������ �������� �������"" onclick='vbscript:f1.action2.value = ""copydays"" : checkform'>")
    end if

']
  end if'form:courseids

  if oksee("special") then'form:special
    rr("<tr valign=top id=special_tr0><td class=form_item1 id=special_tr1><span id=special_caption>~shifts_special~</span><td class=form_item2 id=special_tr2>")
    rr("<input type=checkbox name=special")
    if thespecial = "on" then rr(" checked")
    rr(">")
  end if'form:special

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
'[  end if'form:data1
  end if'form:data1

  if specs = "techjet" then
    rr("<tr valign=top id=shiftids_tr0><td class=form_item1 id=shiftids_tr1><span id=shiftids_caption>������ �������</span><td class=form_item2 id=shiftids_tr2>")
    theshiftids = getval(thedata1, "shiftids")
    t = ":select id,title from shifts where id <> " & request_id & " and (id in(" & theshiftids & "0)"
    if thesiteid = "0" then t = t & " or 0=0)" else t = t & " or siteid = " & thesiteid & ")"
    t = t & " order by title"
    rr(selectlookupmultiauto("f1.shiftids",theshiftids, "icons/refresh.png", "shifts,title," & t, 300, okwritehere))
  end if

  if getparam("ShiftFood") = "y" then
    thefoodsupplierids = getval(thedata1, "foodsupplierids")
    defaultfood = getval(thedata1, "defaultfood")
    rr("<tr valign=top id=foodsupplierid_tr0><td class=form_item1 id=foodsupplierid_tr1><span id=foodsupplierid_caption>���� ����� ����</span><td class=form_item2 id=foodsupplierid_tr2>")
    rr " <select name=defaultfood dir=~langdir~ style='width:300;'>"
    rr "<option value='0' style='color:bbbbfe;'>~Without Food~"
    sq = "select id,title from foodsuppliers order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr "<option value=""" & rs1("id") & """"
      if defaultfood = cstr(rs1("id")) then rr " selected"
      rr ">"
      rr rs1("title") & " (~Code~ ~FoodAbbreviation~" & rs1("id") & ")"
      rs1.movenext
    loop
    rr "</select>"
    rr("<tr valign=top id=foodsupplierid_tr0><td class=form_item1 id=foodsupplierid_tr1><span id=foodsupplierid_caption>�������� ����</span><td class=form_item2 id=foodsupplierid_tr2>")
    t = ":select id,title from foodsuppliers order by title"
    rr(selectlookupmultiauto("f1.foodsupplierids",thefoodsupplierids, "icons/basket.png", "foodsuppliers,title," & t, 300, okwritehere))
  end if

  if specs = "rafael" then
    rr("<tr valign=top><td class=form_item1>���� ���<td class=form_item2>")
    theto1 = getval(thedata1, "to1")
    s = "select id,title from stations where 0=0"
    's = s & " and id in(" & getval(session("mainstations"), "a") & "-1)"
    s = s & " and siteid = 0" & thesiteid
    s = s & " and special = 'on'"
    s = s & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(s))
    rr("<select name=to1 dir=~langdir~>")
    rr("<option value=0>")
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(theto1) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")
    rr("<tr valign=top id=reportok_tr0><td class=form_item1 id=reportok_tr1><span id=reportok_caption>���� ���� ������</span><td class=form_item2 id=reportok_tr2>")
    rr "<input type=checkbox name=reportok" : if getval(thedata1, "reportok") = "on" then rr " checked"
    rr ">"
    rr("<tr valign=top id=notmobile_tr0><td class=form_item1 id=notmobile_tr1><span id=notmobile_caption>�� ���� �������</span><td class=form_item2 id=notmobile_tr2>")
    rr "<input type=checkbox name=notmobile" : if getval(thedata1, "notmobile") = "on" then rr " checked"
    rr ">"
    rr("<tr valign=top id=notforworkers_tr0><td class=form_item1 id=notforworkers_tr1><span id=notforworkers_caption>�� ���� ����� ������</span><td class=form_item2 id=notforworkers_tr2>")
    rr "<input type=checkbox name=notforworkers" : if getval(thedata1, "notforworkers") = "on" then rr " checked"
    rr ">"

    rr "<tr><td colspan=2><hr>"
    rr("<tr valign=top><td class=form_item1>���� �����<td class=form_item2>")
    rr "<input type=checkbox name=loaddef" : if getval(thedata1, "loaddef") = "on" then rr " checked"
    rr ">"
    rr("<tr valign=top><td class=form_item1>���� ������<td class=form_item2>")
    rr "<input type=checkbox name=loadmom" : if getval(thedata1, "loadmom") = "on" then rr " checked"
    rr ">"
    rr("<tr valign=top><td class=form_item1>���� ������ ��� �����<td class=form_item2>")
    rr "<input type=checkbox name=loadshort" : if getval(thedata1, "loadshort") = "on" then rr " checked"
    rr ">"
    rr("<tr valign=top><td class=form_item1>���� ������ ���<td class=form_item2>")
    rr "<input type=text name=loaddep dir=ltr value='" & getval(thedata1, "loaddep") & "' style='width:300;'> <span style='font-size:10; color:#aaaaaa;'> ���� ����� ���� ������� �� �����, ����� �������</span>"

  end if

  if specs = "sanmina" then
    rr("<tr valign=top id=overtime_tr0><td class=form_item1 id=overtime_tr1><span id=overtime_caption>���� ������ ����</span><td class=form_item2 id=overtime_tr2>")
    rr "<input type=text name=overtime value=""" & getval(thedata1, "overtime") & """>"
  end if

  if hasmatch(specs, "tikshoov,sheba,trellidor,") then
    rr("<tr valign=top id=cityids_tr0><td class=form_item1 id=cityids_tr1><span id=cityids_caption>���� ������� ��� '�����'</span><td class=form_item2 id=cityids_tr2>")
    thecityids = getval(thedata1, "cityids")
    t = ":select id,title from stations where id in(" & thecityids & "0) or special = 'on'"
    if specs = "tikshoov" then
      du = getconnectedstations()
      t = ":select id,title from stations where id in(" & thecityids & "0) or id in(" & session("connectedstations") & "-1)"
    end if
    if cstr(thesiteid) <> "0" then t = t & " and siteid in(" & thesiteid & ",0)"
    t = t & " order by title"
    rr(selectlookupmultiauto("f1.cityids",thecityids, "icons/home.png", "stations,title," & t, 300, okwritehere))
  end if

']

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
'[    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
'[    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if (specs = "misgav" or brenerclasses) and session("usergroup") = "admin" then rr(" <input type=button class=groovybutton value=""���� �������"" onclick='vbscript: f1.action2.value = ""setweekplanbyshifts"" : checkform'>")
']

    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  if okedit("title") then rr("    if f1.title.value <> """" then validfield(""title"") else invalidfield(""title"") : ok = false" & vbcrlf)
'[  if okedit("hour1") then rr("    if f1.hour1.value <> """" then validfield(""hour1"") else invalidfield(""hour1"") : ok = false" & vbcrlf)
'[  if okedit("hour1b") then rr("    if f1.hour1b.value <> """" then validfield(""hour1b"") else invalidfield(""hour1b"") : ok = false" & vbcrlf)
  if okedit("hour1") then rr("    if len(f1.hour1.value) = 4 then f1.hour1.value = ""0"" & f1.hour1.value" & vbcrlf)
  if okedit("hour1") then rr("    if len(f1.hour1.value) = 5 and mid(f1.hour1.value,3,1) = "":"" and isnumeric(left(f1.hour1.value,2)) and isnumeric(right(f1.hour1.value,2)) and instr(f1.hour1.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1.value,2),""0123456789"")) <= 59 then validfield(""hour1"") else invalidfield(""hour1"") : ok = false" & vbcrlf)
  if okedit("hour1b") then rr("    if len(f1.hour1b.value) = 4 then f1.hour1b.value = ""0"" & f1.hour1b.value" & vbcrlf)
  if okedit("hour1b") then rr("    if len(f1.hour1b.value) = 5 and mid(f1.hour1b.value,3,1) = "":"" and isnumeric(left(f1.hour1b.value,2)) and isnumeric(right(f1.hour1b.value,2)) and instr(f1.hour1b.value,""-"") = 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(left(f1.hour1b.value,2),""0123456789"")) <= 23 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) >= 0 and cint(""0"" & strfilter(right(f1.hour1b.value,2),""0123456789"")) <= 59 then validfield(""hour1b"") else invalidfield(""hour1b"") : ok = false" & vbcrlf)
']
  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = "data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/refresh.png' ~iconsize~> ~shifts_shifts~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
  rr(" <nobr><img id=ways_filtericon src=icons/i_v.gif style='filter:alpha(opacity=50); margin-top:8;'>")
  rr(" <select name=ways dir=~langdir~ style='width:150;'>")
  rr("<option value='' style='color:bbbbfe;'>~shifts_ways~")
'[  aa = "AB,A,B," : a = aa
  aa = waysoptions : a = aa
']
  'sq = "select distinct ways from shifts order by ways"
  'set rs1 = conn.execute(sqlfilter(sq))
  'do until rs1.eof : a = a & rs1("ways") & "," : rs1.movenext : loop : a = replace(a,",,",",") : if left(a,1) = "," then a = mid(a,2)
  do until a = ""
    b = left(a,instr(a,",")-1)
    a = mid(a,instr(a,",")+1)
    rr("<option value=""" & b & """")
    if session(tablename & "_ways") = b then rr(" selected")
    rr(">" & b)
  loop
  rr("</select></nobr>")'search:ways
  if session(tablename & "containerfield") <> "siteid" then
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~shifts_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
'[  end if'search:siteid
  end if'search:siteid
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"
']
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","shifts.id,shifts.title,shifts.active,shifts.hour1,shifts.hour1b,shifts.ways,shifts.siteid,shifts.unitids,shifts.days,shifts.courseids,shifts.special,shifts.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_ways") <> "" then s = s & " and lcase(" & tablename & ".ways) = '" & lcase(session(tablename & "_ways")) & "'"
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".hour1 like '%" & w & "%' or"
        s = s & " " & tablename & ".hour1b like '%" & w & "%' or"
        s = s & " " & tablename & ".ways like '%" & w & "%' or"
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
        t = "select top 50 id from units where (cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".unitids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        s = s & " " & tablename & ".days like '%" & w & "%' or"
        t = "select top 50 id from courses where (cstr('' & code) & ' ' & cstr('' & title)  like '%" & w & "%')"
        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
        a = "" : do until rs1.eof : a = a & " + instr(cstr(',' & " & tablename & ".courseids), '," & rs1("id") & ",')" : rs1.movenext : loop
        if a <> "" then s = s & mid(a,3) & " > 0 or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("title") then f = "shifts.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_title~</a> " & a2)
  if oksee("active") then f = "shifts.active" : f2 = "active" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("active") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_active~</a> " & a2)
  if oksee("hour1") then f = "shifts.hour1" : f2 = "hour1" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("hour1") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_hour1~</a> " & a2)
  if oksee("hour1b") then f = "shifts.hour1b" : f2 = "hour1b" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("hour1b") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_hour1b~</a> " & a2)
  if oksee("ways") then f = "shifts.ways" : f2 = "ways" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("ways") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_ways~</a> " & a2)
  if oksee("ways") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_ways~</a> " & a2)
  if specs = "misgav" or brenerclasses then
    rr "<td class=list_title><nobr>���� ����"
  end if
']
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_siteid~</a> " & a2)
  end if
  if oksee("unitids") then rr("<td class=list_title><nobr>~shifts_unitids~")
  if oksee("days") then f = "shifts.days" : f2 = "days" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("days") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_days~</a> " & a2)
  if oksee("courseids") then rr("<td class=list_title><nobr>~shifts_courseids~")
  if oksee("special") then f = "shifts.special" : f2 = "special" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("special") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~shifts_special~</a> " & a2)
'[  if oksee("data1") then rr("<td class=list_title><nobr>~shifts_data1~")
  if specs = "rafael" then rr("<td class=list_title><nobr>��� ������") 'reportok
  if specs = "rafael" then rr("<td class=list_title><nobr>�� ���� �������") 'notmobile
  if specs = "rafael" then rr("<td class=list_title><nobr>����") 'loaddef
  if specs = "rafael" then rr("<td class=list_title><nobr>�����") 'loadmom
  if specs = "rafael" then rr("<td class=list_title><nobr>��� �����") 'loadshort
  if specs = "rafael" then rr("<td class=list_title><nobr>���") 'loaddep
  if oksee("data1") then rr("<td class=list_title><nobr>~shifts_data1~")
']

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
      Thehour1 = rs("hour1") : if isnull(Thehour1) then Thehour1 = ""
      Thehour1b = rs("hour1b") : if isnull(Thehour1b) then Thehour1b = ""
      Theways = rs("ways") : if isnull(Theways) then Theways = ""
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Theunitids = rs("unitids") : Theunitids = formatidlist(Theunitids)
      Thedays = rs("days") : if isnull(Thedays) then Thedays = ""
      Thecourseids = rs("courseids") : Thecourseids = formatidlist(Thecourseids)
      Thespecial = rs("special") : if isnull(Thespecial) then Thespecial = ""
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/refresh.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("active") then
      elseif session(tablename & "containerfield") = "active" then
        rr("<input type=hidden name=listupdate_active_" & theid & " value=" & theactive & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "active" then
        rr("<input type=checkbox name=listupdate_active_" & theid & "")
        if theactive = "on" then rr(" checked")
        rr(">")
      else
        if theactive = "on" then rr("v")
      end if
      if oksee("hour1") then rr("<td class=list_item valign=top align=><nobr>" & thehour1)
      if oksee("hour1b") then rr("<td class=list_item valign=top align=><nobr>" & thehour1b)
'[      if oksee("ways") then rr("<td class=list_item valign=top align=><nobr>" & theways)
      if oksee("ways") then rr("<td class=list_item valign=top align=><nobr>" & theways)
      if specs = "misgav" or brenerclasses then
        rr "<td class=list_item valign=top align=><nobr>" & gettitle("stations", getval(thedata1, "to1"))
      end if
']
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
      t = "" : d = formatidlist(theunitids)
      sq = "select * from units where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("title")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("unitids") then rr("<td class=list_item valign=top align=><nobr>" & t)
'[      if oksee("days") then rr("<td class=list_item valign=top align=><nobr>" & thedays)
      t = thedays : if t <> "" then t = "," & t : t = left(t, len(t)-1) : t = replace(t, ",", "~, ~weekday") & "~" : t = mid(t,4)
      if oksee("days") then rr("<td class=list_item valign=top align=><nobr>" & t)
']
      t = "" : d = formatidlist(thecourseids)
      sq = "select * from courses where id in(" & d & "0)"
      closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
      do until rs1.eof
        t = t & ","
        t = t & " " & rs1("code")
        t = t & " " & rs1("title")
        rs1.movenext
      loop
      if t <> "" then t = mid(t,2)
      t = replace(t,"<br>"," ") : if len(t) > 30 then t = left(t,30) & "..."
      if oksee("courseids") then rr("<td class=list_item valign=top align=><nobr>" & t)
      if oksee("special") and session(tablename & "containerfield") <> "special" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("special") then
      elseif session(tablename & "containerfield") = "special" then
        rr("<input type=hidden name=listupdate_special_" & theid & " value=" & thespecial & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "special" then
        rr("<input type=checkbox name=listupdate_special_" & theid & "")
        if thespecial = "on" then rr(" checked")
        rr(">")
      else
        if thespecial = "on" then rr("v")
      end if
'[      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
'[      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
'[      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
      if specs = "rafael" then rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1,"reportok"))
      if specs = "rafael" then rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1,"notmobile"))
      if specs = "rafael" then rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1,"loaddef"))
      if specs = "rafael" then rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1,"loadmom"))
      if specs = "rafael" then rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1,"loadshort"))
      if specs = "rafael" then rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1,"loaddep"))
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
']
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_total1>") 'active
  if oksee("hour1") and session(tablename & "containerfield") <> "hour1" then rr("<td class=list_total1>") 'hour1
  if oksee("hour1b") and session(tablename & "containerfield") <> "hour1b" then rr("<td class=list_total1>") 'hour1b
  if oksee("ways") and session(tablename & "containerfield") <> "ways" then rr("<td class=list_total1>") 'ways
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid
  if oksee("unitids") and session(tablename & "containerfield") <> "unitids" then rr("<td class=list_total1>") 'unitids
  if oksee("days") and session(tablename & "containerfield") <> "days" then rr("<td class=list_total1>") 'days
  if oksee("courseids") and session(tablename & "containerfield") <> "courseids" then rr("<td class=list_total1>") 'courseids
  if oksee("special") and session(tablename & "containerfield") <> "special" then rr("<td class=list_total1>") 'special
'[  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1
  if specs = "rafael" then rr("<td class=list_total1>") 'reportok
  if specs = "rafael" then rr("<td class=list_total1>") 'notmobile
  if specs = "rafael" then rr("<td class=list_total1>") 'loaddef
  if specs = "rafael" then rr("<td class=list_total1>") 'loadmom
  if specs = "rafael" then rr("<td class=list_total1>") 'loadshort
  if specs = "rafael" then rr("<td class=list_total1>") 'loaddep  
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1
']

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
