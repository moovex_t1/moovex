<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 18/04/2017 08:52:58

function sitesdelitem(i)
  sitesdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end function

function sitesinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "on" : if getfield("active") <> "" then t = getfield("active")
  f = f & "active," : v = v & "'" & t & "',"

'[  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  t = "|SetShiftsUntil=17:00|WorkerSetShiftsUntil=17:00|WeekendSetShiftsUntil=13:00|WeekendWorkerSetShiftsUntil=13:00|" : if getfield("data1") <> "" then t = getfield("data1")
']
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into sites(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from sites"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  sitesinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table sites (id autoincrement primary key,title text(50),active text(2),data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "sites" : session("tablename") = "sites"
tablefields = "id,title,active,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 0
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'['-------------start--------------------------------------
if session("userlevel") < "5" then
  disablecontrols = disablecontrols & "title,"
  hidecontrols = hidecontrols & "buttondelete,buttonmultidelete,buttonmultiupdate,buttonsavereturn,buttonback,active,"
end if
session("siteregions") = "" : session("sitecountries") = "" 'filters in orders list
session("sitestationsratios") = ""
']
'locked = "1,2,3,"
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = sitesinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  msg = msg & " ~Item Updated~"
  if not ishour(getfield("setshiftsuntil")) then du = errorend("_back~Invalid~ ~setshiftsuntil~")
  if not ishour(getfield("workersetshiftsuntil")) then du = errorend("_back~Invalid~ ~workersetshiftsuntil~")
  if not ishour(getfield("weekendsetshiftsuntil")) then du = errorend("_back~Invalid~ ~weekendsetshiftsuntil~")
  if not ishour(getfield("weekendworkersetshiftsuntil")) then du = errorend("_back~Invalid~ ~weekendworkersetshiftsuntil~")
  session("mainstations") = ""
']
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("active") then s = s & "active='" & getfield("active") & "',"
  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = sitesdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = sitesdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      if okedit("active") then s = s & ",active = '" & getfield("listupdate_active_" & b) & "'"
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~sites_title~^"
  if oksee("active") then csv(0) = csv(0) & "~sites_active~^"
  if oksee("data1") then csv(0) = csv(0) & "~sites_data1~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("active") then csv(rc) = csv(rc) & replace(theactive,vbcrlf," ") & "^"
    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    rs.movenext
  loop
  response.clear
  response.ContentType = "application/csv"
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
'[      s = "" : thisid = "0"
      s = "" : thisid = "0"
      data1 = ""
']
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "title" then
          if len(b) > 50 then b = left(b,50)
          if okedit("title") then s = s & "title='" & b & "',"
        elseif csvff(i) = "active" then
          if len(b) > 2 then b = left(b,2)
          if okedit("active") then s = s & "active='" & b & "',"
'[        elseif csvff(i) = "data1" then
'[          if okedit("data1") then s = s & "data1='" & b & "',"
        elseif csvff(i) = "data1" then
          data1 = mergeval(data1, b)
        elseif csvff(i) = "��� ���" or csvff(i) = "sitecode" then'iec
          data1 = setval(data1, "sitecode", b)
        elseif csvff(i) = "���� ����" or csvff(i) = "costcenter" then'iec
          data1 = setval(data1, "costcenter", b)
']
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"

      if s <> "" then
'[        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = sitesinsertrecord
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = sitesinsertrecord
        if okedit("data1") and data1 <> "" then
          olddata1 = getonefield("select data1 from " & tablename & " where id = 0" & theid)
          data1 = mergeval(olddata1, data1)
          s = s & "data1='" & data1 & "',"
        end if
']
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","sites.id,sites.title,sites.active,sites.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "title,active,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock


  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><!img src='icons/world.png' ~iconsize~> ~sites_sites~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~sites_title~</span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
  end if'form:title

  if oksee("active") then'form:active
    rr("<tr valign=top id=active_tr0><td class=form_item1 id=active_tr1><span id=active_caption>~sites_active~</span><td class=form_item2 id=active_tr2>")
    rr("<input type=checkbox name=active")
    if theactive = "on" then rr(" checked")
    rr(">")
  end if'form:active

  if oksee("data1") then'form:data1
'[    rr("<span id=data1_caption></span> ")
'[    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")

    if hasmatch(specs, "jerusalem,muni,brener,") then
      for i = 1 to 3
        rr("<tr valign=top><td class=form_item1>�� ��� ����� " & i & "<td class=form_item2>")
        rr "<input type=text name=dynamicfield" & i & "name value='" & getval(thedata1, "dynamicfield" & i & "name") & "' style='width:300;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""dynamicfield" & i & "name"", me.value)'><br>"
      next
      rr "<span style='font-size:10;'>���� ������ ����� ����� �� ���� ������</span>"
    end if

    if specs = "free" then
      rr("<tr valign=top><td class=form_item1>���� ����<td class=form_item2>")
      rr "<input type=text name=clientnumber value='" & getval(thedata1, "clientnumber") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""clientnumber"", me.value)'><br>"
    end if

    if specs = "shufersal" then
      rr("<tr valign=top><td class=form_item1>�� ����<td class=form_item2>")
      rr "<input type=text name=branchname value='" & getval(thedata1, "branchname") & "' style='width:300;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""branchname"", me.value)'><br>"
    end if

    rr("<tr valign=top><td class=form_item1>~setshiftsuntil~<td class=form_item2>")
    rr "<input type=text name=setshiftsuntil value='" & getval(thedata1, "setshiftsuntil") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""setshiftsuntil"", me.value)'><br>"
    rr("<tr valign=top><td class=form_item1>~workersetshiftsuntil~<td class=form_item2>")
    rr "<input type=text name=workersetshiftsuntil value='" & getval(thedata1, "workersetshiftsuntil") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""workersetshiftsuntil"", me.value)'><br>"
    rr("<tr valign=top><td class=form_item1>~weekendsetshiftsuntil~<td class=form_item2>")
    rr "<input type=text name=weekendsetshiftsuntil value='" & getval(thedata1, "weekendsetshiftsuntil") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""weekendsetshiftsuntil"", me.value)'><br>"
    rr("<tr valign=top><td class=form_item1>~weekendworkersetshiftsuntil~<td class=form_item2>")
    rr "<input type=text name=weekendworkersetshiftsuntil value='" & getval(thedata1, "weekendworkersetshiftsuntil") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""weekendworkersetshiftsuntil"", me.value)'><br>"

    if specs = "rafael" then
      rr("<tr valign=top><td class=form_item1>����� ����� ������� �������� ���� ����<td class=form_item2>")
      rr "<input type=text name=specialorderuntil value='" & getval(thedata1, "specialorderuntil") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""specialorderuntil"", me.value)'><br>"
      rr("<tr valign=top><td class=form_item1>��� ����� ����� ����<td class=form_item2>")
      rr "<input type=text name=unitcode value='" & getval(thedata1, "unitcode") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""unitcode"", me.value)'><br>"
      rr("<tr valign=top><td class=form_item1>��� ����� ����� ����<td class=form_item2>")
      rr "<input type=text name=costtype value='" & getval(thedata1, "costtype") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""costtype"", me.value)'><br>"
      rr("<tr valign=top><td class=form_item1>����� ����� ������<td class=form_item2>")
      rr "<input type=text name=constantunit value='" & getval(thedata1, "constantunit") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""constantunit"", me.value)'><br>"
      rr("<tr valign=top><td class=form_item1>����� ����� ����� ������ �����<td class=form_item2>")
      rr "<input type=text name=alertspecialordersminutes value='" & getval(thedata1, "alertspecialordersminutes") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""alertspecialordersminutes"", me.value)'><br>"
    end if

    if specs = "iec" then
      a = "��� ���" : f = "sitecode"
      rr("<tr valign=top><td class=form_item1>" & a & "<td class=form_item2>")
      rr "<input type=text name=" & f & " value='" & getval(thedata1, f) & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'><br>"
      a = "���� ����" : f = "costcenter"
      rr("<tr valign=top><td class=form_item1>" & a & "<td class=form_item2>")
      rr "<input type=text name=" & f & " value='" & getval(thedata1, f) & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'><br>"
    end if

    rr("<tr valign=top><td class=form_item1>~stations_main~<td class=form_item2>")
    rr "<select name=mainstationid onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""mainstationid"", me.value)'>"
    rr "<option value=0 style='color:bbbbfe;'>"
    sq = "select id,title from stations where siteid = 0 or siteid = " & request_id & " order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr "<option value=""" & rs1("id") & """"
      if cstr(getval(thedata1, "mainstationid")) = cstr(rs1("id")) then rr(" selected")
      rr ">" & rs1("title")
      rs1.movenext
    loop
    rr("</select>")

    if specs = "iec" then
      a = "����� ���" : f = "sratio"
      rr("<tr valign=top><td class=form_item1>" & a & "<td class=form_item2>")
      rr "<input type=text name=" & f & " value='" & getval(thedata1, f) & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'>"
      rr " <span style='color:#aaaaaa; font-size:11;'> ����� ��� �����, ���� 0.8 ����� ����� ����� ����</span>"
    end if

    rr("<tr valign=top><td class=form_item1>~Day Hours~<td class=form_item2>")
    rr "<input type=text name=dayhours value='" & getval(thedata1, "dayhours") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""dayhours"", me.value)'>"
    rr " <span style='font-size:9; color:#aaaaaa;'>05:00-20:59"
    rr("<tr valign=top><td class=form_item1>~Weekend Hours~<td class=form_item2>")
    rr "<input type=text name=weekendhours value='" & getval(thedata1, "weekendhours") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""weekendhours"", me.value)'>"
    rr " <span style='font-size:9; color:#aaaaaa;'>13:00-06:00"

    rr "<tr valign=top><td class=form_item1>SetShiftsLockHours "
    rr " <img src=icons/i_help.gif alt='~param_SetShiftsLockHours~'>"
    rr "<td class=form_item2>"
    v = getval(thedata1, "SetShiftsLockHours")
    rr "<select name=SetShiftsLockHours onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""SetShiftsLockHours"", me.value)'>"
    rr "<option value=''" & iif(v = "", " selected", "") & ">~System Default~"
    rr "<option value='y'" & iif(v = "y", " selected", "") & ">On"
    rr "<option value='n'" & iif(v = "n", " selected", "") & ">Off"
    rr "</select>"

    rr "<tr valign=top><td class=form_item1>LockSetShiftsUntilLastProducedUntil "
    rr " <img src=icons/i_help.gif alt='~param_LockSetShiftsUntilLastProducedUntil~'>"
    rr "<td class=form_item2>"
    v = getval(thedata1, "LockSetShiftsUntilLastProducedUntil")
    rr "<select name=LockSetShiftsUntilLastProducedUntil onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""LockSetShiftsUntilLastProducedUntil"", me.value)'>"
    rr "<option value=''" & iif(v = "", " selected", "") & ">~System Default~"
    rr "<option value='y'" & iif(v = "y", " selected", "") & ">On"
    rr "<option value='n'" & iif(v = "n", " selected", "") & ">Off"
    rr "</select>"

    if getparam("UseGoogleMaps") = "y" then
      rr("<tr valign=top><td class=form_item1>StationCourseAddSeconds<td class=form_item2>")
      rr "<input type=text name=StationCourseAddSeconds value='" & getval(thedata1, "StationCourseAddSeconds") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""StationCourseAddSeconds"", me.value)'>"
      rr("<tr valign=top><td class=form_item1>GeoProdMaxShiftGap<td class=form_item2>")
      rr "<input type=text name=GeoProdMaxShiftGap value='" & getval(thedata1, "GeoProdMaxShiftGap") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""GeoProdMaxShiftGap"", me.value)'>"
      rr("<tr valign=top><td class=form_item1>GeoProdMaxPassengers<td class=form_item2>")
      rr "<input type=text name=GeoProdMaxPassengers value='" & getval(thedata1, "GeoProdMaxPassengers") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""GeoProdMaxPassengers"", me.value)'>"
      rr("<tr valign=top><td class=form_item1>GeoProdMaxDuration<td class=form_item2>")
      rr "<input type=text name=GeoProdMaxDuration value='" & getval(thedata1, "GeoProdMaxDuration") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""GeoProdMaxDuration"", me.value)'>"
      rr("<tr valign=top><td class=form_item1>PassengerDurationRatio<td class=form_item2>")
      rr "<input type=text name=PassengerDurationRatio value='" & getval(thedata1, "PassengerDurationRatio") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""PassengerDurationRatio"", me.value)'>"
      rr("<tr valign=top><td class=form_item1>MinimumPassengersForProductionOrder<td class=form_item2>")
      rr "<input type=text name=MinimumPassengersForProductionOrder value='" & getval(thedata1, "MinimumPassengersForProductionOrder") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""MinimumPassengersForProductionOrder"", me.value)'>"

      f = "SameAreaFrom" : v = getval(thedata1, f)
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select name=" & f & " onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'>"
      rr "<option value=''" & iif(v = "", " selected", "") & ">~System Default~"
      rr "<option value='y'" & iif(v = "y", " selected", "") & ">On"
      rr "<option value='n'" & iif(v = "n", " selected", "") & ">Off"
      rr "</select>"
      f = "NearAreaFrom" : v = getval(thedata1, f)
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select name=" & f & " onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'>"
      rr "<option value=''" & iif(v = "", " selected", "") & ">~System Default~"
      rr "<option value='y'" & iif(v = "y", " selected", "") & ">On"
      rr "<option value='n'" & iif(v = "n", " selected", "") & ">Off"
      rr "</select>"
      f = "SameAreaTo" : v = getval(thedata1, f)
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select name=" & f & " onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'>"
      rr "<option value=''" & iif(v = "", " selected", "") & ">~System Default~"
      rr "<option value='y'" & iif(v = "y", " selected", "") & ">On"
      rr "<option value='n'" & iif(v = "n", " selected", "") & ">Off"
      rr "</select>"
      f = "NearAreaTo" : v = getval(thedata1, f)
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select name=" & f & " onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'>"
      rr "<option value=''" & iif(v = "", " selected", "") & ">~System Default~"
      rr "<option value='y'" & iif(v = "y", " selected", "") & ">On"
      rr "<option value='n'" & iif(v = "n", " selected", "") & ">Off"
      rr "</select>"
      f = "SameTo" : v = getval(thedata1, f)
      rr("<tr valign=top><td class=form_item1>" & f & "<td class=form_item2>")
      rr "<select name=" & f & " onchange='vbscript: f1.data1.value = setval(f1.data1.value, """ & f & """, me.value)'>"
      rr "<option value=''" & iif(v = "", " selected", "") & ">~System Default~"
      rr "<option value='y'" & iif(v = "y", " selected", "") & ">On"
      rr "<option value='n'" & iif(v = "n", " selected", "") & ">Off"
      rr "</select>"
    end if
    
    if specs = "teva" then
      rr("<tr valign=top><td class=form_item1>�����<td class=form_item2>")
      rr "<input type=text name=country value='" & getval(thedata1, "country") & "' style='width:205;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""country"", me.value)'><br>"
      rr("<tr valign=top><td class=form_item1>����<td class=form_item2>")
      rr "<input type=text name=region value='" & getval(thedata1, "region") & "' style='width:205;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""region"", me.value)'><br>"
    end if

    if specs = "iai" then
      rr "<tr><td colspan=2><hr>"
      rr("<tr valign=top><td class=form_item1>��� ����� ����� ����� ����<td class=form_item2>")
      rr "<input type=text name=igumhour value='" & getval(thedata1, "igumhour") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""igumhour"", me.value)'><br>"
      rr("<tr valign=top><td class=form_item1>������� ���� ���� ����� ��� �����<td class=form_item2>")
      rr "<input type=text name=igumahead value='" & getval(thedata1, "igumahead") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""igumahead"", me.value)'><br>"
      rr("<tr valign=top><td class=form_item1>���� ����� ����<td class=form_item2>")
      rr "<input type=text name=okusagepercent value='" & getval(thedata1, "okusagepercent") & "' style='width:105;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""okusagepercent"", me.value)'><br>"
      rr("<tr valign=top><td class=form_item1>��� ��� '���� ����'<td class=form_item2>")
      rr "<input type=checkbox name=showreportcatch" : if getval(thedata1, "showreportcatch") = "on" then rr " checked"
      rr " onclick='vbscript: if me.checked then f1.data1.value = setval(f1.data1.value, ""showreportcatch"", ""on"") else f1.data1.value = setval(f1.data1.value, ""showreportcatch"", """")'>"
      rr("<tr valign=top><td class=form_item1>��� ���� ����� �����<td class=form_item2>")
      rr "<input type=checkbox name=sendlicense" : if getval(thedata1, "sendlicense") = "on" then rr " checked"
      rr " onclick='vbscript: if me.checked then f1.data1.value = setval(f1.data1.value, ""sendlicense"", ""on"") else f1.data1.value = setval(f1.data1.value, ""sendlicense"", """")'>"
      rr("<tr valign=top><td class=form_item1>��� WBS ����<td class=form_item2>")
      rr "<input type=checkbox name=wbsmust" : if getval(thedata1, "wbsmust") = "on" then rr " checked"
      rr " onclick='vbscript: if me.checked then f1.data1.value = setval(f1.data1.value, ""wbsmust"", ""on"") else f1.data1.value = setval(f1.data1.value, ""wbsmust"", """")'>"
      'rr("<tr valign=top><td class=form_item1>����� ������ ������<td class=form_item2>")
      'rr "<input type=text name=approvers2 value='" & getval(thedata1, "approvers2") & "' style='width:405; direction:ltr;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""approvers2"", me.value)'><br>"
      'rr("<tr valign=top><td class=form_item1>����� ������ �������<td class=form_item2>")
      'rr "<input type=text name=approvers3 value='" & getval(thedata1, "approvers3") & "' style='width:405; direction:ltr;' onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""approvers3"", me.value)'><br>"
    end if

']
  end if'form:data1

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=9>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)
  rr(checkformadd)

  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
'[elseif ulev > 0 then
elseif ulev > 0 then
  if session("usersiteid") <> "0" then x = rred("?action=form&id=" & session("usersiteid"))
']
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : du = rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = "data1,"
    session(tablename & "_firstcome") = "y"
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><!img src='icons/world.png' ~iconsize~> ~sites_sites~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")

  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"

  '-sqladd
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","sites.id,sites.title,sites.active,sites.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql

  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
  if oksee("title") then f = "sites.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~sites_title~</a> " & a2)
  if oksee("active") then f = "sites.active" : f2 = "active" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("active") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~sites_active~</a> " & a2)
'[  if oksee("data1") then rr("<td class=list_title><nobr>~sites_data1~")
  if specs = "iec" then
    rr("<td class=list_title><nobr>��� ���") 'sitecode
    rr("<td class=list_title><nobr>���� ����") 'costcenter
  end if
  if oksee("data1") then rr("<td class=list_title><nobr>~sites_data1~")
']

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
  '-------------rows--------------------------
  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Theactive = rs("active") : if isnull(Theactive) then Theactive = ""
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
      rr("<img src=icons/world.png ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("active") then
      elseif session(tablename & "containerfield") = "active" then
        rr("<input type=hidden name=listupdate_active_" & theid & " value=" & theactive & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "active" then
        rr("<input type=checkbox name=listupdate_active_" & theid & "")
        if theactive = "on" then rr(" checked")
        rr(">")
      else
        if theactive = "on" then rr("v")
      end if
'[      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
'[      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
'[      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
      if specs = "iec" then
        rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1, "sitecode"))
        rr("<td class=list_item valign=top align=><nobr>" & getval(thedata1, "costcenter"))
      end if
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)

']
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("active") and session(tablename & "containerfield") <> "active" then rr("<td class=list_total1>") 'active
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
