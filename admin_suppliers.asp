<!--#include file="include.asp"-->
<%'serverfunctions
'-------------functions---------------------------------
'BU generated 21/08/2014 09:13:24

function suppliersdelitem(i)
  suppliersdelitem = 0
  dim rs, s
  if instr("," & locked,"," & i & ",") > 0 then exit function
  if not okwrite(tablename,i) then errorend("~Access Denied~")
  s = "delete * from " & tablename & " where id = " & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "delete * from drivers where supplierid=" & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "delete * from courserates where supplierid=" & i
  closers(rs) : set rs = conn.execute(sqlfilter(s))

end function

function suppliersinsertrecord
  if ulev < 2 then errorend("~Access Denied~")
  dim f, t, v, s, rs
  f = "" : v = ""

  t = "" : if getfield("title") <> "" then t = getfield("title")
  f = f & "title," : v = v & "'" & t & "',"

  t = "" : if getfield("address") <> "" then t = getfield("address")
  f = f & "address," : v = v & "'" & t & "',"

  t = "" : if getfield("phone1") <> "" then t = getfield("phone1")
  f = f & "phone1," : v = v & "'" & t & "',"

  t = "" : if getfield("fax") <> "" then t = getfield("fax")
  f = f & "fax," : v = v & "'" & t & "',"

  t = "" : if getfield("email") <> "" then t = getfield("email")
  f = f & "email," : v = v & "'" & t & "',"

  t = "" : if getfield("contac1tname") <> "" then t = getfield("contac1tname")
  f = f & "contac1tname," : v = v & "'" & t & "',"

  t = "" : if getfield("payment") <> "" then t = getfield("payment")
  f = f & "payment," : v = v & "'" & t & "',"

  t = "0" : if session(tablename & "containerid") <> "" and session(tablename & "containerfield") = "siteid" then t = session(tablename & "containerid")
'[  if getfield("siteid") <> "" then t = getfield("siteid")
  t = session("usersiteid")
  if getfield("siteid") <> "" then t = getfield("siteid")
']
  f = f & "siteid," : v = v & t & ","

  t = "50" : if getfield("priority") <> "" then t = getfield("priority")
  f = f & "priority," : v = v & t & ","

'[  t = "" : if getfield("data1") <> "" then t = getfield("data1")
  t = "|activeproduction=on|activespecial=on|" : if getfield("data1") <> "" then t = getfield("data1")
']
  f = f & "data1," : v = v & "'" & t & "',"

  f = left(f, len(f) - 1): v = left(v, len(v) - 1)
  s = "insert into suppliers(" & f & ") values(" & v & ")"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from suppliers"
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  suppliersinsertrecord = rs("id")
end function
'serverfunctions%>
<%
'-------------declarations-------------------------------
's = "create table suppliers (id autoincrement primary key,title text(50),address memo,phone1 memo,fax memo,email memo,contac1tname text(50),payment text(100),siteid number,priority number,data1 memo)"
'closers(rs) : set rs = conn.execute(sqlfilter(s))
tablename = "suppliers" : session("tablename") = "suppliers"
tablefields = "id,title,address,phone1,fax,email,contac1tname,payment,siteid,priority,data1,"

request_action = getfield("action") : request_action2 = getfield("action2") : request_action3 = getfield("action3") : request_id = getfield("id") : request_ids = formatidlist(getfield("ids"))
if getfield("containerfield") <> "" then session(tablename & "containerfield") = getfield("containerfield")
if getfield("containerid") <> "" then session(tablename & "containerid") = getfield("containerid") else if session(tablename & "containerid") = "" then session(tablename & "containerid") = "0"
if session(tablename & "containerid") = "0" then
  session(tablename & "containerfield") = ""
  childpage = false : session("childpage") = ""
else
  childpage = true : session("childpage") = "on"
end if
if getfield("monthview") <> "" then session(tablename & "_monthview") = getfield("monthview")
admin_top = true : admin_login = true
%>
<!--#include file="top.asp"-->
<%
'-------------permissions--------------------------------
ulev = 0
if session("usergroup") = "supplier" then ulev = 0
if session("usergroup") = "supplieradmin" then ulev = 0
if session("usergroup") = "manager" then ulev = 0
if session("usergroup") = "teamleader" then ulev = 0
if session("usergroup") = "approver" then ulev = 0
if session("usergroup") = "approvedone" then ulev = 0
if session("usergroup") = "finance" then ulev = 4
if session("usergroup") = "adminassistant" then ulev = 0
if session("usergroup") = "admin" then ulev = 4
if session("userlevel") = "5" then ulev = 5

'-------------start--------------------------------------
'['locked = "1,2,3,"
if appname = "paz" then locked = "9,10,"
if session("usersiteid") <> "0" then disablecontrols = disablecontrols & "siteid,"
if session("userlevel") < "5" then hidecontrols = hidecontrols & "buttondelete,buttonmultidelete,"
if hasmatch(specs, "jerusalem,muni,brener,") then hidecontrols = hidecontrols & "siteid,"
if specs = "jerusalem" then hidecontrols = hidecontrols & "addnight,addweekend,nightpercentonbase,returndiscount,ordertypeids,supplierreduction,payment,priority,"
']
'-------------insert-------------------------------------
if request_action = "insert" and ulev > 1 and okedit("buttonadd") Then
  request_id = suppliersinsertrecord
  request_action = "form"
  request_action2 = "newitem"
'-------------update-------------------------------------
elseif request_action = "update" and ulev > 1 Then
  if not okwrite(tablename,request_id) then errorend("~Access Denied~")
'[  msg = msg & " ~Item Updated~"
  msg = msg & " ~Item Updated~"

  if hasmatch(specs, "jerusalem,muni,brener,") then
    i = getidlist("select id from suppliers where id <> " & request_id & " and trim(lcase(title)) = '" & trim(lcase(getfield("title"))) & "'")
    if i <> "" then du = errorend("_back��� ����� ���� ���� �� �� ���")
  end if
']
  s = "update " & tablename & " set "
  if okedit("title") then s = s & "title='" & getfield("title") & "',"
  if okedit("address") then s = s & "address='" & getfield("address") & "',"
  if okedit("phone1") then s = s & "phone1='" & getfield("phone1") & "',"
  if okedit("fax") then s = s & "fax='" & getfield("fax") & "',"
  if okedit("email") then s = s & "email='" & getfield("email") & "',"
  if okedit("contac1tname") then s = s & "contac1tname='" & getfield("contac1tname") & "',"
  if okedit("payment") then s = s & "payment='" & getfield("payment") & "',"
  if okedit("siteid") then s = s & "siteid=0" & getfield("siteid") & ","
  if okedit("priority") then s = s & "priority=0" & getfield("priority") & ","
'[  if okedit("data1") then s = s & "data1='" & getfield("data1") & "',"
  sq = "select data1 from " & tablename & " where id = " & request_id
  set rs1 = conn.execute(sqlfilter(sq))
  data1 = "" & rs1("data1")
  data1 = setval(data1, "activeproduction", getfield("activeproduction"))
  data1 = setval(data1, "activespecial", getfield("activespecial"))
  data1 = setval(data1, "supplierreduction", getfield("supplierreduction"))
  data1 = setval(data1, "madadtype", getfield("madadtype"))
  data1 = setval(data1, "madadbasedate", getfield("madadbasedate"))
  data1 = setval(data1, "addnight", getfield("addnight"))
  data1 = setval(data1, "addweekend", getfield("addweekend"))
  data1 = setval(data1, "returndiscount", getfield("returndiscount"))
  data1 = setval(data1, "ordertypeidsv", replace(getfield("ordertypeidsv"),"|","/"))
  data1 = setval(data1, "ordertypeids", getfield("ordertypeids"))
  data1 = setval(data1, "suppliercode", getfield("suppliercode"))
  data1 = setval(data1, "supplierordernumber", getfield("supplierordernumber"))
  data1 = setval(data1, "addvia", getfield("addvia"))
  data1 = setval(data1, "addstop", getfield("addstop"))
  data1 = setval(data1, "nightpercentonbase", getfield("nightpercentonbase"))
  data1 = setval(data1, "contact1", getfield("contact1"))
  data1 = setval(data1, "contact1p", getfield("contact1p"))
  data1 = setval(data1, "contact1e", getfield("contact1e"))
  data1 = setval(data1, "contact2", getfield("contact2"))
  data1 = setval(data1, "contact2p", getfield("contact2p"))
  data1 = setval(data1, "contact2e", getfield("contact2e"))
  data1 = setval(data1, "contact3", getfield("contact3"))
  data1 = setval(data1, "contact3p", getfield("contact3p"))
  data1 = setval(data1, "contact3e", getfield("contact3e"))
  data1 = setval(data1, "���� ����", getfield("���� ����"))
  data1 = setval(data1, "rakaz", getfield("rakaz"))
  data1 = setval(data1, "fax2", getfield("fax2"))
  data1 = setval(data1, "remarks", getfield("remarks"))
  data1 = setval(data1, "yitaccount", getfield("yitaccount"))
  s = s & "data1='" & data1 & "',"
']
  s = left(s, len(s) - 1) & " "
  s = s & "where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
end if
'-------------afterupdate-------------
if instr(request_action2,"addlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  box = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id & "&box=" & box
  rred(ctxt(url))

elseif instr(request_action2,"editlookup,") > 0 then
  a = request_action2
  action = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  url = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  if not childpage then session("backto") = "admin_" & tablename & ".asp?action=form&id=" & request_id
  rred(ctxt(url))

elseif request_action2 = "stay" then
  request_action = "form"

elseif request_action2 = "duplicate" and okedit("buttonduplicate") then
  s = "insert into " & tablename & "(title,address,phone1,fax,email,contac1tname,payment,siteid,priority,data1)"
  s = s & " SELECT title,address,phone1,fax,email,contac1tname,payment,siteid,priority,data1"
  s = s & " FROM " & tablename & " where id = " & request_id
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  s = "select max(id) as id from " & tablename
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  theid = rs("id")

  s = "insert into drivers(title,active,idn,supplierid,phone,licensetype,licenseexpire,remarks,siteid)"
  s = s & " SELECT title,active,idn," & theid & ",phone,licensetype,licenseexpire,remarks,siteid"
  s = s & " FROM drivers where supplierid=" & request_id
  closers(rs1) : set rs1 = conn.execute(sqlfilter(s))

  s = "insert into courserates(courseid,from1,to1,areaid1,areaid2,activeproduction,active,special,duration,supplierid,passengers,price,addnight,addweekend,returndiscount,ordertypeidsv,ordertypeids,siteid,data1)"
  s = s & " SELECT courseid,from1,to1,areaid1,areaid2,activeproduction,active,special,duration," & theid & ",passengers,price,addnight,addweekend,returndiscount,ordertypeidsv,ordertypeids,siteid,data1"
  s = s & " FROM courserates where supplierid=" & request_id
  closers(rs1) : set rs1 = conn.execute(sqlfilter(s))

  request_action = "form"
  request_id = theid
  msg = msg & " ~Record duplicated~"
end if

'-------------delete-------------------------------------
if request_action = "delete" and okedit("buttondelete") then
  x = suppliersdelitem(request_id)
  msg = msg & " ~Item Deleted~"
end if

'-------------multidelete-------------------------------------
if request_action = "multidelete" and okedit("buttonmultidelete") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    x = suppliersdelitem(b)
    c = c + 1
  loop
  msg = msg & " " & c & " ~Items Deleted~"

'-------------multiupdate-------------------------------------
elseif request_action = "multiupdate" and okedit("buttonmultiupdate") then
  ids = request_ids : if getfield("allidscheck") = "on" then ids = getidlist(session(tablename & "_lastlistsql"))
  hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")
  c = 0
  do until ids = ""
    b = left(ids,instr(ids,",")-1) : ids = mid(ids,instr(ids,",")+1)
    if okwrite(tablename,b) then
      s = ""
      t = getfield("listupdate_priority_" & b) : if t = "" then t = getfield("multiupdate_priority")
      if okedit("priority") then s = s & ",priority = " & t
      if s <> "" then
        s = "update " & tablename & " set " & mid(s,2) & " where id = " & b
        closers(rs) : set rs = conn.execute(sqlfilter(s))
        c = c + 1
      end if
    end if
  loop
  msg = msg & " " & c & " ~Items Updated~"
end if
'-------------csv-------------------------------------
if request_action = "csv" and okedit("buttoncreatecsv") then
  dim csv() : redim preserve csv(1)
  csv(0) = "_ID^"
  if oksee("title") then csv(0) = csv(0) & "~suppliers_title~^"
  if oksee("address") then csv(0) = csv(0) & "~suppliers_address~^"
  if oksee("phone1") then csv(0) = csv(0) & "~suppliers_phone1~^"
  if oksee("fax") then csv(0) = csv(0) & "~suppliers_fax~^"
  if oksee("email") then csv(0) = csv(0) & "~suppliers_email~^"
  if oksee("contac1tname") then csv(0) = csv(0) & "~suppliers_contac1tname~^"
  if oksee("payment") then csv(0) = csv(0) & "~suppliers_payment~^"
  if oksee("siteid") then csv(0) = csv(0) & "~suppliers_siteid~^"
  if oksee("priority") then csv(0) = csv(0) & "~suppliers_priority~^"
  if oksee("data1") then csv(0) = csv(0) & "~suppliers_data1~^"
  csv(0) = translate(csv(0))
  if getfield("allidscheck") = "on" or request_ids = "" then
    s = session(tablename & "_lastlistsql")
  else
    s = session(tablename & "_lastlistsql")
    s = left(s,instrrev(s,"order by ")-1) & " and " & tablename & ".id in(" & request_ids & "0) " & mid(s,instrrev(s,"order by "))
    if instr(lcase(s)," where ") = 0 then s = replace(s," and "," where ",1,1,1)
  end if
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  rc = 0
  do until rs.eof
    rc = rc + 1 : redim preserve csv(rc)
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theaddress = rs("address") : if isnull(Theaddress) then Theaddress = ""
  Thephone1 = rs("phone1") : if isnull(Thephone1) then Thephone1 = ""
  Thefax = rs("fax") : if isnull(Thefax) then Thefax = ""
  Theemail = rs("email") : if isnull(Theemail) then Theemail = ""
  Thecontac1tname = rs("contac1tname") : if isnull(Thecontac1tname) then Thecontac1tname = ""
  Thepayment = rs("payment") : if isnull(Thepayment) then Thepayment = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
  Thepriority = replace(thepriority,",",".") : Thepriority = formatnumber(thepriority,0,true,false,false)
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

    csv(rc) = csv(rc) & theid & "^"
    if oksee("title") then csv(rc) = csv(rc) & replace(thetitle,vbcrlf," ") & "^"
    if oksee("address") then csv(rc) = csv(rc) & replace(theaddress,vbcrlf," ") & "^"
    if oksee("phone1") then csv(rc) = csv(rc) & replace(thephone1,vbcrlf," ") & "^"
    if oksee("fax") then csv(rc) = csv(rc) & replace(thefax,vbcrlf," ") & "^"
    if oksee("email") then csv(rc) = csv(rc) & replace(theemail,vbcrlf," ") & "^"
    if oksee("contac1tname") then csv(rc) = csv(rc) & replace(thecontac1tname,vbcrlf," ") & "^"
    if oksee("payment") then csv(rc) = csv(rc) & replace(thepayment,vbcrlf," ") & "^"
    if oksee("siteid") then csv(rc) = csv(rc) & thesiteidlookup & "^"
    if oksee("priority") then csv(rc) = csv(rc) & replace(thepriority,vbcrlf," ") & "^"
    if oksee("data1") then csv(rc) = csv(rc) & replace(thedata1,vbcrlf," ") & "^"
    rs.movenext
  loop
'[  response.clear
'[  response.ContentType = "application/csv"
  if specs = "jerusalem" then exporttoexcel
  response.clear
  response.ContentType = "application/csv"
']
  n = appname & "_" & tablename & "_" & year(now) & "-" & month(now) & "-" & day(now) & "-" & hour(now) & "-" & minute(now)
  response.AddHeader "Content-Disposition", "filename=" & n & ".csv"
  for i = 0 to rc
    csv(i) = ctext(csv(i))
    csv(i) = replace(csv(i),"""","""""")
    csv(i) = """" & replace(csv(i),"^",""",""")
    if right(csv(i),1) = """" then csv(i) = left(csv(i),len(csv(i))-1)
    response.write(csv(i) & vbcrlf)
    response.flush
  next
  rend()
end if
'-------------loadcsv-------------------------------------
if request_action = "loadcsv" and getfield("loadcsv") <> "" and ulev > 1 and okedit("buttonloadcsv") Then
  loadcsvprepare()
  c = 0 : line = 0
  do until csvdd = ""
    line = line + 1
    if instr(csvdd,vbcrlf) = 0 then exit do
    a = left(csvdd,instr(csvdd,vbcrlf)-1) : csvdd = mid(csvdd,instr(csvdd,vbcrlf)+2)
    a = a & "," : formdatavalue = ""
    t = a : t = replace(t," ","") : t = replace(t,",","")
    if countstring(a, ",") > 2 and t <> "" then
      s = "" : thisid = "0"
      for i = 1 to countstring(a,",")
        if instr(a,",") = 0 then exit for
        if left(a,1) = """" and instr(2,a,"""") >= 2 then
          x = 1
          do until x >= len(a)
            x = x + 1
            if mid(a,x,1) = """" and mid(a,x+1,1) <> """" then exit do
            if mid(a,x,1) = """" and mid(a,x+1,1) = """" then x = x + 1
          loop
          b = left(a,x) : a = mid(a,x+2)
          b = mid(b,2) : b = left(b,len(b)-1) : b = replace(b,"""""","""")
        else
          b = trim(left(a,instr(a,",")-1)) : a = mid(a,instr(a,",")+1)
        end if
        b = chtm(b)
        if lcase(csvff(i)) = "id" or lcase(csvff(i)) = "_id" then
          thisid = b
        elseif csvff(i) = "title" then
          if len(b) > 50 then b = left(b,50)
          if okedit("title") then s = s & "title='" & b & "',"
        elseif csvff(i) = "address" then
          if okedit("address") then s = s & "address='" & b & "',"
        elseif csvff(i) = "phone1" then
          if okedit("phone1") then s = s & "phone1='" & b & "',"
        elseif csvff(i) = "fax" then
          if okedit("fax") then s = s & "fax='" & b & "',"
        elseif csvff(i) = "email" then
          if okedit("email") then s = s & "email='" & b & "',"
        elseif csvff(i) = "contac1tname" then
          if len(b) > 50 then b = left(b,50)
          if okedit("contac1tname") then s = s & "contac1tname='" & b & "',"
        elseif csvff(i) = "payment" then
          if len(b) > 100 then b = left(b,100)
          if okedit("payment") then s = s & "payment='" & b & "',"
        elseif csvff(i) = "siteid" then
          if b = "" then
            v = 0
          else
            sq = "select * from sites where lcase(cstr('' & title)) = '" & lcase(b) & "'"
            closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
            if rs1.eof then v = 0 else v = rs1("id")
          end if
          if okedit("siteid") then s = s & "siteid=" & v & ","
        elseif csvff(i) = "priority" then
          b = strfilter(b,"-.0123456789")
          if not isnumeric(b) then b = 0
          if okedit("priority") then s = s & "priority=" & b & ","
        elseif csvff(i) = "data1" then
          if okedit("data1") then s = s & "data1='" & b & "',"
        elseif formdatafield <> "" and csvff(i) <> "" then
          t = "" : if mid(csvff(i),2,1) <> " " then t = "T "
          formdatavalue = formdatavalue & t & csvff(i) & "=" & b & "|"
        elseif formdatafield <> "" and instr(b,"=") > 0 then
          formdatavalue = formdatavalue & b & "|"
        end if
      next
      if formdatafield <> "" and formdatavalue <> "" then s = s & formdatafield & " = '" & replace(formdatavalue,"'","''") & "',"
      if s <> "" then
        if instr("," & csvidlist,"," & thisid & ",") > 0 then theid = thisid else theid = suppliersinsertrecord
        s = "update " & tablename & " set " & s
        s = left(s, len(s) - 1) & " "
        s = s & "where id = " & theid
        if okwrite(tablename,theid) then
          closers(rs) : set rs = conn.execute(sqlfilter(s))
          c = c + 1
        end if
      end if
    end if
  loop
  msg = msg & " ~CSV Loaded~: " & c & " ~Records Updated~"
end if
'-------------more actions-------------------------------------
'-------------form---------------------------------------------
if request_action = "form" and ulev > 0 then
  if getfield("backto") <> "" then session("backto") = ctxt(getfield("backto"))
  s = "select * from " & tablename & " where id = " & request_id
  if session("usingsql") = "1" then s = replace(s,"*","suppliers.id,suppliers.title,suppliers.address,suppliers.phone1,suppliers.fax,suppliers.email,suppliers.contac1tname,suppliers.payment,suppliers.siteid,suppliers.priority,suppliers.data1",1,1,1)
  s = s & okreadsql(tablename)'form
  closers(rs) : set rs = conn.execute(sqlfilter(s))
  if rs.eof then errorend("~Access Denied~")
  okwritehere = okwrite(tablename,request_id) : if instr("," & locked, "," & request_id & ",") > 0 then okwritehere = false
  Theid = rs("id")
  Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
  Theaddress = rs("address") : if isnull(Theaddress) then Theaddress = ""
  Theaddress = replace(theaddress,"<br>",vbcrlf)
  Thephone1 = rs("phone1") : if isnull(Thephone1) then Thephone1 = ""
  Thefax = rs("fax") : if isnull(Thefax) then Thefax = ""
  Theemail = rs("email") : if isnull(Theemail) then Theemail = ""
  Thecontac1tname = rs("contac1tname") : if isnull(Thecontac1tname) then Thecontac1tname = ""
  Thepayment = rs("payment") : if isnull(Thepayment) then Thepayment = ""
  Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
  Thesiteid = replace(thesiteid,",",".")
  Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
  Thepriority = replace(thepriority,",",".") : Thepriority = formatnumber(thepriority,0,true,false,false)
  Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
  formcontrols = "title,address,phone1,fax,email,contac1tname,payment,siteid,priority,data1,"
  formbuttons = "buttonsavereturn,buttonsave,buttonduplicate,buttondelete,buttonback,buttonprint,"
  if instr("," & locked,"," & request_id & ",") > 0 then locker = textlock

  if not childpage and getfield("printversion") <> "on" then
    rr("<div class=subtablinks>")
    t = "000000" : if request_action2 <> "showchildpage" then t = "bb0000"
'[    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "> <span style=" & t & "> ~" & tablename & "_" & tablename & "~ " & request_id & "</font></a>")
    rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "> <span style=" & t & "><b>~Supplier~ " & thetitle & "</b></font></a>")
']
    t = "000000" : if getfield("childpage") = "cars" then t = "bb0000"
    if getpermission("cars") >= 1 then rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=cars&containerfield=supplierid> <font color=" & t & "> ~cars_cars~ </font></a>")
    t = "000000" : if getfield("childpage") = "drivers" then t = "bb0000"
    if getpermission("drivers") >= 1 then rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=drivers&containerfield=supplierid> <font color=" & t & "> ~drivers_drivers~ </font></a>")
    t = "000000" : if getfield("childpage") = "courserates" then t = "bb0000"
    if getpermission("courserates") >= 1 then rr(" &nbsp;&nbsp; <img src=arrow_small_~langside2~.gif border=0> <a href=?action=form&id=" & request_id & "&action2=showchildpage&childpage=courserates&containerfield=supplierid> <font color=" & t & "> ~courserates_courserates~ </font></a>")
    rr("<span id=childchildren></span></div>")
    if getfield("action2") = "showchildpage" then
      t = "admin_" & getfield("childpage") & ".asp?containerfield=" & getfield("containerfield") & "&containerid=" & request_id & "&action=search&action2=clear"
'[      rr("<br><iframe id=fr" & getfield("childpage") & " name=fr" & getfield("childpage") & " style='width:100%; height:1000;' frameborder=0 scrolling=no src=" & t & "></iframe>")
      rr("<br><iframe id=fr" & getfield("childpage") & " name=fr" & getfield("childpage") & " style='width:100%; height:2000;' frameborder=0 scrolling=no src=" & t & "></iframe>")
']
      rrbottom : rend
    end if
  end if
  rr("<center><table class=form_table cellspacing=5>" & formtopbar)
  rr("<form action=? method=post name=f1>")
  rr("<input type=hidden name=action value=update><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  rr("<input type=hidden name=id value=" & request_id & ">")
  rr("<tr><td class=form_name id=tr0_form_title colspan=99><img src='icons/i_key.gif' ~iconsize~> ~suppliers_suppliers~ - ~Edit~")
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",5000,""vbscript"") </" & "script>")
  x = lockthisrecord(tablename,request_id,session("username"),okwritehere)

  '-------------controls--------------------------
  if oksee("title") then'form:title
    rr("<tr valign=top id=title_tr0><td class=form_item1 id=title_tr1><span id=title_caption>~suppliers_title~</span><td class=form_item2 id=title_tr2>")
    rr("<input type=text maxlength=50 name=title onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thetitle & """ dir=~langdir~>")
'[  end if'form:title
  end if'form:title

  if hasmatch(specs, "jerusalem,muni,brener,") then
    rr("<tr valign=top><td class=form_item1><span>���</span><td class=form_item2>")
    rr "<select name=rakaz>" 'onchange='vbscript: f1.data1.value = setval(f1.data1.value, ""rakaz"", me.value)'>"
    rr("<option value=0 style='color:bbbbfe;'>���")
    sq = "select id,username from buusers where usergroup = 'Rakaz' order by username"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("username") & """")
      if cstr(getval(thedata1, "rakaz")) = cstr(rs1("username")) then rr(" selected")
      rr(">")
      rr(rs1("username"))
      rs1.movenext
    loop
    rr("</select>")
  end if

  if hasmatch(specs ,"rafael,iec,") then
    rr("<tr valign=top><td class=form_item1>��� ���<td class=form_item2>")
    rr "<input type=text name=suppliercode style='width:100; direction:ltr;' value='" & getval(thedata1, "suppliercode") & "'>"
    rr("<tr valign=top><td class=form_item1>���� ����� ���<td class=form_item2>")
    rr "<input type=text name=supplierordernumber style='width:100; direction:ltr;' value='" & getval(thedata1, "supplierordernumber") & "'>"
  end if

']

  if oksee("address") then'form:address
    rr("<tr valign=top id=address_tr0><td class=form_item1 id=address_tr1><span id=address_caption>~suppliers_address~</span><td class=form_item2 id=address_tr2>")
    rr("<textarea id=qtextarea1 name=address style='width:301; height:20;' onkeyup='vbscript:resizeq1'>")
    rr(theaddress)
    rr("</textarea>" & vbcrlf)
    rr("<script language=vbscript>" & vbcrlf)
    rr("  sub resizeq1" & vbcrlf)
    rr("    dim d,h,i" & vbcrlf)
    rr("    d = document.getelementbyid(""qtextarea1"").value" & vbcrlf)
    rr("    h = 22" & vbcrlf)
    rr("    for i = 1 to len(d)" & vbcrlf)
    rr("      if mid(d,i,2) = vbcrlf then h = h + 15" & vbcrlf)
    rr("    next" & vbcrlf)
    rr("    if clng(h) < clng(len(d) / 2) then h = 16 + cint(len(d)/3)" & vbcrlf)
    rr("    if h > 150 then h = 150" & vbcrlf)
    rr("    document.getelementbyid(""qtextarea1"").style.height = h" & vbcrlf)
    if childpage then rr("    parent.document.getelementbyid(""fr" & tablename & """).height = document.body.scrollheight + 20" & vbcrlf)
    rr("  end sub" & vbcrlf)
    rr("  resizeq1" & vbcrlf)
    rr("</script>" & vbcrlf)
  end if'form:address

  if oksee("phone1") then'form:phone1
    rr("<tr valign=top id=phone1_tr0><td class=form_item1 id=phone1_tr1><span id=phone1_caption>~suppliers_phone1~</span><td class=form_item2 id=phone1_tr2>")
    rr("<input type=text maxlength=30000 name=phone1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thephone1 & """ dir=ltr>")
  end if'form:phone1

  if oksee("fax") then'form:fax
    rr("<tr valign=top id=fax_tr0><td class=form_item1 id=fax_tr1><span id=fax_caption>~suppliers_fax~</span><td class=form_item2 id=fax_tr2>")
    rr("<input type=text maxlength=30000 name=fax onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thefax & """ dir=ltr>")
'[  end if'form:fax
  end if'form:fax
  if hasmatch(specs, "jerusalem,muni,brener,") then
    rr("<tr valign=top><td class=form_item1>���<td class=form_item2>")
    rr "<input type=text name=fax2 style='width:300; direction:ltr;' value='" & getval(thedata1, "fax2") & "'>"
    rr("<tr valign=top><td class=form_item1>�����<td class=form_item2>")
    rr "<textarea name=remarks style='width:300; height:60;'>" & getval(thedata1, "remarks") & "</textarea>"
  end if

']

  if oksee("email") then'form:email
    rr("<tr valign=top id=email_tr0><td class=form_item1 id=email_tr1><span id=email_caption>~suppliers_email~</span><td class=form_item2 id=email_tr2>")
    rr("<input type=text name=email onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Theemail & """ dir=ltr onchange='vbscript: document.getelementbyid(""email_mailto"").innerhtml = ""<a href=mailto:"" & me.value & ""><img src=icons/i_letter.gif border=0></a>""'>")
    rr(" <span id=email_mailto><a href=mailto:" & Theemail & "><img src=icons/i_letter.gif border=0></a></span>")
  end if'form:email

  if oksee("contac1tname") then'form:contac1tname
    rr("<tr valign=top id=contac1tname_tr0><td class=form_item1 id=contac1tname_tr1><span id=contac1tname_caption>~suppliers_contac1tname~</span><td class=form_item2 id=contac1tname_tr2>")
    rr("<input type=text maxlength=50 name=contac1tname onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thecontac1tname & """ dir=~langdir~>")
  end if'form:contac1tname

  if oksee("payment") then'form:payment
    rr("<tr valign=top id=payment_tr0><td class=form_item1 id=payment_tr1><span id=payment_caption>~suppliers_payment~</span><td class=form_item2 id=payment_tr2>")
    rr("<input type=text maxlength=100 name=payment onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:300;' value=""" & Thepayment & """ dir=~langdir~>")
  end if'form:payment

  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    rr("<tr valign=top id=siteid_tr0><td class=form_item1 id=siteid_tr1><span id=siteid_caption>~suppliers_siteid~</span><td class=form_item2 id=siteid_tr2>")
    if getfield("box") = "siteid" then thesiteid = getfield("boxid")
    rr("<select name=siteid dir=~langdir~>")
    rr("<option value=0 style='color:bbbbfe;'>~suppliers_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(Thesiteid) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select>")'form:siteid
  elseif okedit("siteid") then
    rr("<input type=hidden name=siteid value=" & thesiteid & ">")
  end if'form:siteid

  if oksee("priority") then'form:priority
    rr("<tr valign=top id=priority_tr0><td class=form_item1 id=priority_tr1><span id=priority_caption>~suppliers_priority~<img src=must.gif></span><td class=form_item2 id=priority_tr2>")
'[    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=priority value=""" & Thepriority & """>")
    rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=priority value=""" & Thepriority & """>")
    rr " <img src=icons/i_help.gif alt='~1 = first priority, 100 = last priority~'>"
']
  end if'form:priority

  if oksee("data1") then'form:data1
    rr("<span id=data1_caption></span> ")
    rr("<input type=hidden name=data1 value=""" & Thedata1 & """>")
'[  end if'form:data1
  end if'form:data1

  rr "<tr><td colspan=2 style='height:1px; background:#cccccc;'>"

  'if hasmatch(specs, "tnuva,kmc,egged,jr,") then
    rr("<tr valign=top><td class=form_item1>YIT Account<td class=form_item2>")
    rr "<input type=text name=yitaccount style='width:100; direction:ltr;' value='" & getval(thedata1, "yitaccount") & "'> ���� ����� YIT"
  'end if

  rr "<tr valign=top><td class=form_item1>~Active~<td class=form_item2>"
  rr " <input type=checkbox name=activeproduction" : if getval(thedata1, "activeproduction") = "on" then rr " checked"
  rr ">~Active~ ~Production~"
  rr " <input type=checkbox name=activespecial" : if getval(thedata1, "activespecial") = "on" then rr " checked"
  rr ">~Active~ ~Special Ride~"
  rr("<tr valign=top id=supplierreduction_tr0><td class=form_item1>~Total supplier discount~<td class=form_item2>")
  rr "<input type=text name=supplierreduction style='width:50; direction:ltr;' value='" & getval(thedata1, "supplierreduction") & "'> ~Percent discount on all rates~"

  if specs = "rafael" then
    rr("<tr valign=top><td class=form_item1>����� ����<td class=form_item2>")
    rr "<input type=text name=madadtype style='width:100; direction:ltr;' value='" & getval(thedata1, "madadtype") & "'> �� ���� ������"
    rr("<tr valign=top><td class=form_item1>����� ����� ����<td class=form_item2>")
    rr selectdate("f1.madadbasedate", getval(thedata1, "madadbasedate"))
  end if

  if hasmatch(specs, "jerusalem,muni,brener,") then
    rr("<tr valign=top id=contact1_tr0><td class=form_item1 id=contact1_tr1><span id=contact1_caption>��� ��� 1</span><td class=form_item2 id=contact1_tr2>")
    rr("<input type=text name=contact1 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:~langdir~;' value=""" & getval(thedata1, "contact1") & """ dir=ltr>")
    rr(" <b>�����</b> ")
    rr("<input type=text name=contact1p onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:ltr;' value=""" & getval(thedata1, "contact1p") & """ dir=ltr>")
    rr(" <b>����</b> ")
    rr("<input type=text name=contact1e onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:ltr;' value=""" & getval(thedata1, "contact1e") & """ dir=ltr>")

    rr("<tr valign=top id=contact2_tr0><td class=form_item1 id=contact2_tr1><span id=contact2_caption>��� ��� 2</span><td class=form_item2 id=contact2_tr2>")
    rr("<input type=text name=contact2 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:~langdir~;' value=""" & getval(thedata1, "contact2") & """ dir=ltr>")
    rr(" <b>�����</b> ")
    rr("<input type=text name=contact2p onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:ltr;' value=""" & getval(thedata1, "contact2p") & """ dir=ltr>")
    rr(" <b>����</b> ")
    rr("<input type=text name=contact2e onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:ltr;' value=""" & getval(thedata1, "contact2e") & """ dir=ltr>")

    rr("<tr valign=top id=contact3_tr0><td class=form_item1 id=contact3_tr1><span id=contact3_caption>��� ��� 3</span><td class=form_item2 id=contact3_tr2>")
    rr("<input type=text name=contact3 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:~langdir~;' value=""" & getval(thedata1, "contact3") & """ dir=ltr>")
    rr(" <b>�����</b> ")
    rr("<input type=text name=contact3p onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:ltr;' value=""" & getval(thedata1, "contact3p") & """ dir=ltr>")
    rr(" <b>����</b> ")
    rr("<input type=text name=contact3e onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:150; direction:ltr;' value=""" & getval(thedata1, "contact3e") & """ dir=ltr>")
  end if

  if hasmatch(specs, "jerusalem,") then
    f = "���� ����" : v = getval(thedata1, f)
    rr "<tr valign=top><td class=form_item1>���� ����<td class=form_item2>"
    rr "<select name='" & f & "'><option value=''>"
    a = "��� �����, ��� �����,"
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      rr "<option value='" & b & "'" & iif(v = b, " selected", "") & ">" & b
    loop
    rr "</select>"
  end if

  rr("<tr valign=top id=addnight_tr0><td class=form_item1 id=addnight_tr1><span id=addnight_caption>~courserates_addnight~</span><td class=form_item2 id=addnight_tr2>")
  rr("<input type=text maxlength=10 name=addnight onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & getval(thedata1, "addnight") & """ dir=ltr>")
  rr " <img src=icons/i_help.gif alt='~Can specify fixed price(20.40) or additional percent (12%)~'>"

  rr("<tr valign=top id=addweekend_tr0><td class=form_item1 id=addweekend_tr1><span id=addweekend_caption>~courserates_addweekend~</span><td class=form_item2 id=addweekend_tr2>")
  rr("<input type=text maxlength=10 name=addweekend onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & getval(thedata1, "addweekend") & """ dir=ltr>")
  rr " <img src=icons/i_help.gif alt='~Can specify fixed price(20.40) or additional percent (12%)~ - ~If ride is on weekend, night rate will not apply~'>"

  'if specs = "iai" then
    rr "<tr valign=top id=nightpercentonbase_tr0><td class=form_item1 id=nightpercentonbase_tr1><span id=nightpercentonbase_caption>��� �����</span><td class=form_item2 id=nightpercentonbase_tr2>"
    rr "<input type=checkbox maxlength=10 name=nightpercentonbase"
    if getval(thedata1, "nightpercentonbase") = "on" then rr " checked"
    rr ">~Calculate weekend/night addition over base price before other additions~"
  'end if

  rr("<tr valign=top id=returndiscount_tr0><td class=form_item1 id=returndiscount_tr1><span id=returndiscount_caption>~courserates_returndiscount~</span><td class=form_item2 id=returndiscount_tr2>")
  rr("<input type=text maxlength=10 name=returndiscount onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & getval(thedata1, "returndiscount") & """ dir=ltr>")
  rr " <img src=icons/i_help.gif alt='~Can specify fixed price(20.40) or additional percent (12%)~'>"

  if specs = "rail" then
    rr "<tr valign=top><td class=form_item1>����� ������ ����<td class=form_item2>"
    rr "<input type=text maxlength=10 name=addvia onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & getval(thedata1, "addvia") & """ dir=ltr>"
    rr "<tr valign=top><td class=form_item1>����� ������ �� ���� ��� �����<td class=form_item2>"
    rr "<input type=text maxlength=10 name=addstop onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:100;' value=""" & getval(thedata1, "addstop") & """ dir=ltr>"
  end if

  rr("<tr valign=top id=ordertypeids_tr0><td class=form_item1 id=ordertypeids_tr1><span id=ordertypeids_caption>~courserates_ordertypeids~</span><td class=form_item2 id=ordertypeids_tr2>")
  rr("<input type=hidden name=ordertypeidsv value=""" & replace(getval(thedata1, "ordertypeidsv"),"/","|") & """>")
  rr "<span style='position:absolute;'><img src=blank.gif width=370 height=1> <img src=icons/i_help.gif style='margin-top:2;' alt='���� ������� �� ����� ������ �������� ���. �� �� ���� ���, ���� ���� ���� ��� ���� ������. ��� ��� ����� ���� ����� ����� ���� (20.40) �� ����� ����� (12%)'></span>"
  t = ":select id,title from ordertypes where id in(" & getval(thedata1, "ordertypeids") & "-1) or active = 'on' order by title"
  rr(selectlookupmultiauto("f1.ordertypeids", getval(thedata1, "ordertypeids"), "icons/order-1.png", "ordertypes,title," & t, 300, okwritehere))


']

  '-------------updatebar--------------------
  rr("<tr id=tr0_update_bar><td class=update_bar colspan=99>")
  if okwritehere then
    rr(" <input type=submit class=groovybutton style='width:0; height:0; visibility:hidden;' id=fsubmit>") 'only this updates the htmlarea
    rr(" <input type=button class=groovybutton id=buttonsavereturn value=""~SaveReturn~"" onclick='vbscript: f1.action2.value = """" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonsave value=""~Save~"" onclick='vbscript:f1.action2.value = ""stay"" : checkform'>")
    rr(" <input type=button class=groovybutton id=buttonduplicate value=""~Duplicate~"" onclick='vbscript:if msgbox(""~Duplicate ?~"",vbyesno) = 6 then f1.action2.value=""duplicate"":checkform'>")
    if instr("," & locked,"," & request_id & ",") = 0 then rr(" <input type=button class=groovybutton id=buttondelete value=""~Delete~"" onclick='vbscript:if msgbox(""~Delete ?~"",vbyesno) = 6 then document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """'>")
  end if
  t = "~Back~" : tt = "document.location = ""?id=" & request_id & """" : if request_action2 = "newitem" then t = "~Cancel~" : tt = "document.location = ""?action=delete&id=" & request_id & "&token=" & session("token") & """"
  rr(" <input type=button class=groovybutton id=buttonback value=""" & t & """ onclick='vbscript: " & tt & "'>")
  rr("</tr></form>" & bottombar)'form

  if not okwritehere then disablecontrols = disablecontrols & formcontrols
  x = disablecontrolsnow(formcontrols,disablecontrols,hidecontrols)

  t = "admin_cars.asp?containerfield=supplierid&containerid=" & request_id : if getfield("carschildpageid") <> "" then t = t & "&action=form&id=" & getfield("carschildpageid") else t = t & "&action=search&action2=clear"
  if getpermission("cars") >= 1 and childpage then rr("<tr><td colspan=99><iframe id=frcars width=100% height=1 src=" & t & " frameborder=0></iframe>")

  t = "admin_drivers.asp?containerfield=supplierid&containerid=" & request_id : if getfield("driverschildpageid") <> "" then t = t & "&action=form&id=" & getfield("driverschildpageid") else t = t & "&action=search&action2=clear"
  if getpermission("drivers") >= 1 and childpage then rr("<tr><td colspan=99><iframe id=frdrivers width=100% height=1 src=" & t & " frameborder=0></iframe>")

  t = "admin_courserates.asp?containerfield=supplierid&containerid=" & request_id : if getfield("courserateschildpageid") <> "" then t = t & "&action=form&id=" & getfield("courserateschildpageid") else t = t & "&action=search&action2=clear"
  if getpermission("courserates") >= 1 and childpage then rr("<tr><td colspan=99><iframe id=frcourserates width=100% height=1 src=" & t & " frameborder=0></iframe>")
  rr("</table>")

  '-------------checkform-------------
  rr("<" & "script language=vbscript>" & vbcrlf)
  rr("  sub checkform" & vbcrlf)
  rr("    ok = true" & vbcrlf)

'[  if okedit("priority") and session(tablename & "containerfield") <> "priority" then
'[    rr("    if isnumeric(f1.priority.value) then" & vbcrlf)
'[    rr("      if cdbl(f1.priority.value) >= 0 and cdbl(f1.priority.value) <= 100 then" & vbcrlf)
'[    rr("        validfield(""priority"")" & vbcrlf)
'[    rr("      else" & vbcrlf)
'[    rr("        invalidfield(""priority"")" & vbcrlf)
'[    rr("        ok = false" & vbcrlf)
'[    rr("      end if " & vbcrlf)
'[    rr("    else" & vbcrlf)
'[    rr("      invalidfield(""priority"")" & vbcrlf)
'[    rr("      ok = false" & vbcrlf)
'[    rr("    end if" & vbcrlf)
'[  end if
']

  rr("    if not ok then" & vbcrlf)
  rr("      document.location = ""#top"" : msgbox ""~Invalid inputs (marked in red)~""" & vbcrlf)
  rr("    else" & vbcrlf)
  rr("      on error resume next" & vbcrlf)
  rr("      if f1.target = """" then" & vbcrlf)
  a = formbuttons

  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    rr("document.getElementById(""" & b & """).disabled = true" & vbcrlf)
  loop
  rr("      end if" & vbcrlf)
  rr("      on error goto 0" & vbcrlf)
  x = enablecontrolsnow(disablecontrols)
  rr("      f1.fsubmit.click" & vbcrlf)
  rr("    end if" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub invalidfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = ""ff0000""" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("  sub validfield(a)" & vbcrlf)
  rr("    document.getElementById(a & ""_caption"").style.background = """"" & vbcrlf)
  rr("  end sub" & vbcrlf)
  rr("</" & "script>" & vbcrlf)
  if pdamode = "" then rr("<script language=vbscript>on error resume next : f1.title.focus : on error goto 0</script>")

'-------------list---------------------------------------------
elseif ulev > 0 then
  if not childpage and session("backto") <> "" then a = session("backto") : session("backto") = "" : rred(a & "&containerid=0&boxid=" & request_id)
  if session(tablename & "_firstcome") = "" or request_action2 = "clear" then
    For Each sessionitem in Session.Contents
      if left(sessionitem, len(tablename) + 1) = tablename & "_" and right(sessionitem,6) <> "_order" and right(sessionitem,16) <> "_hidelistcolumns" then session(sessionitem) = ""
    Next
    if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title" : session(tablename & "_hidelistcolumns") = "address,payment,data1,"
'[    session(tablename & "_firstcome") = "y"
    session(tablename & "_firstcome") = "y"
    if session("usergroup0") = "rakaz" then session(tablename & "_rakazfilter") = session("username")'jerusalem,muni,brener,
']
  end if
  if request_action = "search" then
    For i = 1 to Request.Form.Count : session(tablename & "_" & Request.Form.Key(i)) = chtm(Request.Form.Item(i)) : Next
    For i = 1 to Request.Querystring.Count : session(tablename & "_" & Request.Querystring.Key(i)) = chtm(Request.Querystring.Item(i)) : Next
    session(tablename & "_page") = 0
  end if

  '-------------listsession-------------
  if getfield("advanced") <> "" then session(tablename & "_advanced") = getfield("advanced")
  If getfield("page") <> "" Then session(tablename & "_page") = getfield("page")
  if isnumeric("-" & session(tablename & "_page")) then session(tablename & "_page") = cdbl(session(tablename & "_page")) else session(tablename & "_page") = 0
  If getfield("lines") <> "" Then session(tablename & "_lines") = getfield("lines")
  if isnumeric("-" & session(tablename & "_lines")) and trim(session(tablename & "_lines")) <> "0" then session(tablename & "_lines") = cdbl(session(tablename & "_lines")) else session(tablename & "_lines") = 20
  if cdbl(session(tablename & "_lines")) > 100 then session(tablename & "_lines") = 100
  if getfield("order") <> "" then session(tablename & "_order") = getfield("order") : session(tablename & "_page") = 0
  if session(tablename & "_order") = "" then session(tablename & "_order") = tablename & ".title"
  if getfield("groupseperator1") <> "" then session(tablename & "_groupseperator") = getfield("groupseperator")
  if getfield("groupseperatorfield") <> "" then session(tablename & "_groupseperatorfield") = getfield("groupseperatorfield")
  hidecolumns = hidecontrols : hidecontrols = hidecontrols & session(tablename & "_hidelistcolumns")

  '-------------listform-------------
  rr("<table class=list_table width=100% cellspacing=0 cellpadding=5 align=center>" & listtopbar)
  rr("<form action=? method=post name=f2>")

  t1 = "" : t2 = ""
  if childpage then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?action3=minimized""'" : t2 = "<img src=up.gif border=0>"
  if childpage and request_action3 = "minimized" then t1 = " style='cursor:hand;' onclick='vbscript: document.location = ""?""'" : t2 = "<img src=down.gif border=0>"
  rr("<tr><td class=list_name id=tr0_list_title colspan=99" & t1 & "><img src='icons/i_key.gif' ~iconsize~> ~suppliers_suppliers~ " & t2)
  rr(" <font id=msgdiv class=msg>" & msg & "</font><" & "script language=vbscript> x = setTimeout(""msgdiv.innerhtml = dummyemptystring"",3000,""vbscript"") </" & "script>")
  if childpage and request_action3 = "minimized" then rend

  rr("<tr id=tr0_search_bar><td colspan=99 class=search_bar>")
  if ulev > 1 and okedit("buttonadd") then rr("<input type=button class=groovybutton id=buttonadd value=""~Add~"" onclick='vbscript: window.document.location=""?action=insert&token=" & session("token") & """'> ")
  rr("<input type=hidden name=action value=search>")
  rr("<img src=find.gif style='margin-top:8;'> <input type=text style='width:150;' name=string value=""" & session(tablename & "_string") & """>")
if session(tablename & "_advanced") = "y" then
  'rr("<select name=stringoption>")
  'rr("<option value=all") : if session(tablename & "_stringoption") = "all" then rr(" selected")
  'rr(">~All words~")
  'rr("<option value=any") : if session(tablename & "_stringoption") = "any" then rr(" selected")
  'rr(">~Any word~")
  'rr("<option value=exact") : if session(tablename & "_stringoption") = "exact" then rr(" selected")
  'rr(">~Exactly~")
  'rr("</select>")
'[  if session(tablename & "containerfield") <> "siteid" then
  if oksee("siteid") then
']
    rr(" <nobr><img id=siteid_filtericon src=icons/world.png style='filter:alpha(opacity=50); margin-top:8;'>")
    rr(" <select style='width:150;' name=siteid dir=~langdir~>")
    rr("<option value='' style='color:bbbbfe;'>~suppliers_siteid~")
    sq = "select * from sites order by title"
    closers(rs1) : set rs1 = conn.execute(sqlfilter(sq))
    do until rs1.eof
      rr("<option value=""" & rs1("id") & """")
      if cstr(session(tablename & "_siteid")) = cstr(rs1("id")) then rr(" selected")
      rr(">")
      rr(rs1("title") & " ")
      rs1.movenext
    loop
    rr("</select></nobr>")'search:siteid
'[  end if'search:siteid
  end if'search:siteid
  if session("usersiteid") <> "0" then hidecontrols = hidecontrols & "siteid,"

  if hasmatch(specs, "jerusalem,muni,brener,") then
    rr(" ��� <select name=rakazfilter>")
    rr("<option value='' style='color:bbbbfe;'>���")
    s = "select username from buusers where usergroup = 'Rakaz' order by username"
    set rs1 = conn.execute(sqlfilter(s))
    do until rs1.eof
      b = rs1("username") & ""
      rr("<option value=""" & b & """")
      if session(tablename & "_rakazfilter") = b then rr(" selected")
      rr(">" & b)
      rs1.movenext
    loop
    rr("</select>")
  end if

  if hasmatch(specs, "jerusalem,") then
    f = "���� ����" : v = session(tablename & "_shitatfilter")
    rr "<nobr> ���� ���� "
    rr "<select name=shitatfilter><option value=''>"
    a = "��� �����, ��� �����,"
    do until a = ""
      b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
      rr "<option value='" & b & "'" & iif(v = b, " selected", "") & ">" & b
    loop
    rr "</select></nobr>"
  end if

']
  rr(" <nobr><input type=text name=lines style='width:35;' value=" & session(tablename & "_lines") & " dir=ltr> ~Lines~</nobr>")
  rr("<input type=hidden name=groupseperator1 value=on><input type=checkbox name=groupseperator") : if session(tablename & "_groupseperator") = "on" then rr(" checked")
  rr(">~Groups~")
'[  t = mid(tablefields,3) : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
  e = ""
  if hasmatch(specs, "jerusalem,muni,brener,") then e = e & "���,���,��� ��� 1,����� 1,���� 1,��� ��� 2,����� 2,���� 2,��� ��� 3,����� 3,���� 3,"
  if hasmatch(specs, "jerusalem,") then e = e & "���� ����,"
  e = e & additionals : if e <> "" then e = replace("," & left(e,len(e)-1), ",", ",additionals_") : e = mid(e,2) & ","
  t = mid(tablefields,3) & e : a = hidecolumns : do until a = "" : b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : t = replace(t,"," & b & ",", ",") : loop : t = mid(t,2)
']
  rr("<nobr><iframe id=frhidelistcolumns style='position:absolute; visibility:hidden; top:-1000; border:2px solid #aaaaaa;' scrolling=yes frameborder=0 width=200 height=200")
  rr(" onblur='vbscript: me.style.visibility = ""hidden"" : me.style.top = -1000'></iframe>")
  rr("<input type=hidden name=hidelistcolumns value=" & session(tablename & "_hidelistcolumns") & ">")
  rr(" <input type=button class=groovybutton id=buttonhidelistcolumns onclick='vbscript: document.getelementbyid(""frhidelistcolumns"").style.visibility = ""visible"" : document.getelementbyid(""frhidelistcolumns"").style.top = """" : if document.getelementbyid(""frhidelistcolumns"").src = """" then document.getelementbyid(""frhidelistcolumns"").src = ""?action=hidelistcolumns&table=" & tablename & "&columns=" & t & "&hidelistcolumns=" & session(tablename & "_hidelistcolumns") & """' value=""~Columns~..""></nobr>")

end if
  rr(" <input type=button class=groovybutton id=buttonclear value=""~Clear Search~"" onclick='vbscript:document.location=""?action=search&action2=clear""'> ")
  rr(" <input type=submit class=groovybutton id=buttonsearch value=""    ~Show~    ""> ")
  if session(tablename & "_advanced") <> "y" then rr(" <a href=?advanced=y><nobr>~Advanced Search~</nobr></a>")
  if session(tablename & "_showids") <> "" then rr " <span style='color:#ff0000;'>(" & countstring(session(tablename & "_showids"), ",") & " ~Items~)</span>"
  rr("</tr></form>")'search

  '-------------sql------------------------------
  s = "select " & tablename & ".* from (" & tablename & ")"
  s = replace(s, " from ", ", cstr('' & sites_1.title) as siteidlookup from ",1,1,1) : s = replace(s, "(" & tablename & "", "((" & tablename & "",1,1,1) : s = s & " left join sites sites_1 on " & tablename & ".siteid = sites_1.id)"

'[  '-sqladd
  if specs = "jerusalem" then s = s & " and trim(" & tablename & ".title) <> ''"
  t = session(tablename & "_shitatfilter") : if t <> "" then s = s & " and instr(cstr('' & suppliers.data1), '|���� ����=" & t & "|') > 0"
  if session(tablename & "_rakazfilter") <> "" then 'jerusalem
    s = s & " and instr(cstr('' & " & tablename & ".data1), '|rakaz=" & session(tablename & "_rakazfilter") & "|') > 0"
  end if
']
  if session(tablename & "_showids") <> "" then s = s & " and " & tablename & ".id in(" & session(tablename & "_showids") & "-1)"
  s = s & sqladditions
  if session("usingsql") = "1" then s = replace(s,tablename & ".*","suppliers.id,suppliers.title,suppliers.address,suppliers.phone1,suppliers.fax,suppliers.email,suppliers.contac1tname,suppliers.payment,suppliers.siteid,suppliers.priority,suppliers.data1",1,1,1)
  if session(tablename & "containerid") <> "0" then s = s & " and " & tablename & "." & session(tablename & "containerfield") & " = " & session(tablename & "containerid")
  s = s & okreadsql(tablename)'sql
  if session(tablename & "_siteid") <> "" and session(tablename & "containerfield") <> "siteid" then s = s & " and " & tablename & ".siteid = " & session(tablename & "_siteid")
  if session(tablename & "_string") <> "" then
    s = s & " and ("
    ww = session(tablename & "_string") : ww = replacechars(ww, "!@#$%^&*()_+-=<>{};':""|\/?.,<>", "                               ")
    ww = strfilter(ww,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789��������������������������� ")
    if len(ww) < 2 then session(tablename & "_string") = "" : response.clear : errorend("_back~Search string must include at least 2 characters")
    ww = ww & " "
    do until ww = ""
      w = trim(left(ww,instr(ww," ")-1)) : ww = ltrim(mid(ww,instr(ww," ")+1))
      
'[      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
      if w = "" then w = "5k6k7hf9d"
      if session(tablename & "_stringoption") = "exact" then w = left(w & " " & ww, len(w & " " & ww)-1): ww = ""
']
      s = s & "("
      if isnumeric(w) then s = s & " " & tablename & ".id = " & w & " or"
'[        t = "select distinct supplierid from drivers where (title like '%" & w & "%')"
'[        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
'[        ids = "" : do until rs1.eof : ids = ids & rs1("supplierid") & "," : rs1.movenext :loop : ids = ids & "-1"
'[        s = s & " " & tablename & ".id in(" & ids & ") or"
']
'[        t = "select distinct supplierid from cars where (code like '%" & w & "%' or title like '%" & w & "%')"
'[        closers(rs1) : set rs1 = conn.execute(sqlfilter(t))
'[        ids = "" : do until rs1.eof : ids = ids & rs1("supplierid") & "," : rs1.movenext :loop : ids = ids & "-1"
'[        s = s & " " & tablename & ".id in(" & ids & ") or"
']
        s = s & " " & tablename & ".title like '%" & w & "%' or"
        s = s & " " & tablename & ".address like '%" & w & "%' or"
        s = s & " " & tablename & ".phone1 like '%" & w & "%' or"
        s = s & " " & tablename & ".fax like '%" & w & "%' or"
        s = s & " " & tablename & ".email like '%" & w & "%' or"
        s = s & " " & tablename & ".contac1tname like '%" & w & "%' or"
'[        s = s & " " & tablename & ".payment like '%" & w & "%' or"
']
        s = s & " cstr('' & sites_1.title) & ' ' like '%" & w & "%' or"
        if isnumeric(w) then s = s & " " & tablename & ".priority = " & w & " or"
        s = s & " " & tablename & ".data1 like '%" & w & "%' or"
      if right(s,2) = "or" then s = left(s,len(s)-2)
      s = s & ")"
      if session(tablename & "_stringoption") = "any" then s = s & " or " else s = s & " and"
    Loop
    s = left(s,len(s)-4) & ")"
  end if
  s = replace(s," and "," where ",1,1,1)
  s = replace(s,"(" & tablename & ")",tablename,1,1,1)
  s = s & " order by " & session(tablename & "_order")
  session(tablename & "_lastlistsql") = s
  if rs.state = 1 then rs.close
  rs.open sqlfilter(s), conn, 3, 1, 1
  rsrc = rs.recordcount : if session("usingsql") = "2" then rsrc = 0 : if not rs.eof then rsArray = rs.GetRows() : rsrc = UBound(rsArray, 2) + 1 : rs.movefirst
  '-------------fieldsums--------------------------

  '-------------toprow-----------------------------
  rr("<tr class=list_title>")
'[  if oksee("title") then f = "suppliers.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
'[  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~suppliers_title~</a> " & a2)
  if oksee("title") then f = "suppliers.title" : f2 = "title" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("title") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~suppliers_title~</a> " & a2)
  rr("<td class=list_title><nobr>~Active~ ~Production~")'activeproduction
  rr("<td class=list_title><nobr>~Active~ ~Special Ride~")'activespecial
  if specs = "iec" then rr("<td class=list_title><nobr>~supplier_code~")'suppliercode
']
  if oksee("address") then rr("<td class=list_title><nobr>~suppliers_address~")
  if oksee("phone1") then rr("<td class=list_title><nobr>~suppliers_phone1~")
'[  if oksee("fax") then rr("<td class=list_title><nobr>~suppliers_fax~")
  if oksee("fax") then rr("<td class=list_title><nobr>~suppliers_fax~")
  if hasmatch(specs, "jerusalem,muni,brener,") and oksee("additionals_���") then rr("<td class=list_title><nobr>���")'fax2
']
  if oksee("email") then f = "suppliers.email" : f2 = "email" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("email") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~suppliers_email~</a> " & a2)
  if oksee("contac1tname") then f = "suppliers.contac1tname" : f2 = "contac1tname" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("contac1tname") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~suppliers_contac1tname~</a> " & a2)
  if oksee("payment") then f = "suppliers.payment" : f2 = "payment" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("payment") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~suppliers_payment~</a> " & a2)
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then
    f = "sites_1.title" : f2 = "siteidlookup" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
    rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~suppliers_siteid~</a> " & a2)
  end if
  if oksee("priority") then f = "suppliers.priority" : f2 = "priority" : a1 = f : a2 = "&nbsp;&nbsp;&nbsp;" : if session(tablename & "_order") = f & " desc" then a2 = "<img src=up.gif>" else if session(tablename & "_order") = f then a2 = "<img src=down.gif>" : a1 = f & "+desc"
  if oksee("priority") then rr("<td class=list_title><nobr><a class=list_title_link href=?order=" & a1 & "&groupseperatorfield=" & f2 & ">~suppliers_priority~</a> " & a2)
'[  if oksee("data1") then rr("<td class=list_title><nobr>~suppliers_data1~")
  if hasmatch(specs, "jerusalem,muni,brener,") then 
    if oksee("additionals_���") then rr("<td class=list_title><nobr>���")'rakaz
    if oksee("additionals_���_���_1") then rr("<td class=list_title><nobr>��� ��� 1")'contact1
    if oksee("additionals_�����_1") then rr("<td class=list_title><nobr>����� 1")
    if oksee("additionals_�����_1") then rr("<td class=list_title><nobr>���� 1")
    if oksee("additionals_���_���_2") then rr("<td class=list_title><nobr>��� ��� 2")
    if oksee("additionals_�����_2") then rr("<td class=list_title><nobr>����� 2")
    if oksee("additionals_����_2") then rr("<td class=list_title><nobr>���� 2")
    if oksee("additionals_���_���_3") then rr("<td class=list_title><nobr>��� ��� 3")
    if oksee("additionals_�����_3") then rr("<td class=list_title><nobr>����� 3")
    if oksee("additionals_����_3") then rr("<td class=list_title><nobr>���� 3")
  end if
  if hasmatch(specs, "jerusalem,") then
    if oksee("additionals_����_����") then rr("<td class=list_title><nobr>���� ����")
  end if
  if oksee("data1") then rr("<td class=list_title><nobr>~suppliers_data1~")
']

  rr("</tr><form action=? method=post name=flist><input type=hidden name=action><input type=hidden name=action2><input type=hidden name=action3><input type=hidden name=token value=" & session("token") & ">")
  if rs.eof and request_action = "search" and request_action2 <> "clear" then rr("<tr><td colspan=99 bgcolor=ffffff>~No Match~")
'[  '-------------rows--------------------------
  allidscheckscript = ""
  if specs = "jerusalem" and rsrc > 0 and ulev > 1 and okedit("list_checkboxes") then
    checkall = checkall & "flist.allpagecheck.checked = v" & vbcrlf
    checkall = checkall & "flist.allpagecheck2.checked = v" & vbcrlf
    rr "<tr><td colspan=99 style='height:40;'>&nbsp;&nbsp;"
    rr(" <input type=checkbox name=allpagecheck2 onclick='vbscript: checkall(me.checked)'>~All~")
    rr " <input type=checkbox name=allidscheck2 onclick='vbscript: flist.allidscheck.checked = me.checked'>~All Results~ (" & rsrc & ")"
    allidscheckscript = " onclick='vbscript: flist.allidscheck2.checked = me.checked' "
  end if
']

  rc = 0
  do until rs.eof
    rc = rc + 1
    showthis = false
    if rc > (session(tablename & "_page") * session(tablename & "_lines")) and rc <= (session(tablename & "_page") * session(tablename & "_lines")) + session(tablename & "_lines") then showthis = true
    if showthis then
      Theid = rs("id")
      Thetitle = rs("title") : if isnull(Thetitle) then Thetitle = ""
      Theaddress = rs("address") : if isnull(Theaddress) then Theaddress = ""
      Thephone1 = rs("phone1") : if isnull(Thephone1) then Thephone1 = ""
      Thefax = rs("fax") : if isnull(Thefax) then Thefax = ""
      Theemail = rs("email") : if isnull(Theemail) then Theemail = ""
      Thecontac1tname = rs("contac1tname") : if isnull(Thecontac1tname) then Thecontac1tname = ""
      Thepayment = rs("payment") : if isnull(Thepayment) then Thepayment = ""
      Thesiteid = rs("siteid") : if isnull(Thesiteid) then Thesiteid = 0
      Thesiteid = replace(thesiteid,",",".")
      Thepriority = rs("priority") : if isnull(Thepriority) then Thepriority = 0
      Thepriority = replace(thepriority,",",".") : Thepriority = formatnumber(thepriority,0,true,false,false)
      Thedata1 = rs("data1") : if isnull(Thedata1) then Thedata1 = ""
      Thesiteidlookup = rs("siteidlookup") : if isnull(Thesiteidlookup) then Thesiteidlookup = ""

      okwritehere = okwrite(tablename,theid) : if instr("," & locked, "," & theid & ",") > 0 then okwritehere = false
    end if
    if showthis then
      if session(tablename & "_groupseperator") = "on" and session(tablename & "_groupseperatorfield") <> "" then
        t = session(tablename & "_groupseperatorfield")
        if instr("^" & groupseperators, "^" & rs(t) & "^") = 0 then
          rr("<tr><td colspan=99 class=groupseperator>" & rs(t) & "&nbsp;")
          groupseperators = groupseperators & rs(t) & "^"
        end if
      end if
      if thiscolor = "" or thiscolor = list_item2 then thiscolor = list_item1 else thiscolor = list_item2
      rr("<tr bgcolor=ffffff id=tr" & rc)
      rr(" onmouseover='vbscript:tr" & rc & ".style.background=""f6f6f6"" : dpop" & rc & ".style.visibility = ""visible""'")
      rr(" onmouseout='vbscript:tr" & rc & ".style.background=""ffffff"" : dpop" & rc & ".style.visibility = ""hidden""'")
      rr(">")
      rr("<td class=list_item valign=top><nobr>")
      'pop = "123"
      if pop = "" then rr("<div id=dpop" & rc & "></div>") else rr("<div id=dpop" & rc & " style='position:relative; visibility:hidden; padding-~langside~:40;'><div style='position:absolute; padding:3px; top:15; background:ffffff; border:3px solid #" & list_item_hover & ";'>" & pop & "</div></div>")
      i = theid : if okedit("list_checkboxes") then rr("<input type=checkbox style=height:14; id=c" & i & " name=ids value=" & theid & ">")
      checkall = checkall & "flist.c" & i & ".checked = v" & vbcrlf
      firstcoltitle = thetitle
      if isnull(firstcoltitle) then firstcoltitle = ""
      if len(cstr(firstcoltitle)) = 0 then firstcoltitle = "---"
      if cstr(request_id) = cstr(theid) then firstcoltitle = "<b>" & firstcoltitle & "</b>"
      if instr("," & locked,"," & theid & ",") > 0 then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if not okwritehere then firstcoltitle = "<font color=cc0000>" & firstcoltitle & "</font>"
      if okedit("list_recordlinks") then rr("<a class=list_item_link href=?action=form&id=" & theid & ">")
'[      rr("<img src=icons/i_key.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      rr("<img src=icons/i_key.gif ~iconsize~ border=0 style='vertical-align:top;'> " & firstcoltitle & "</a>")
      rr "<td class=list_item valign=top><nobr>" : if getval(thedata1, "activeproduction") = "on" then rr "v"
      rr "<td class=list_item valign=top><nobr>" : if getval(thedata1, "activespecial") = "on" then rr "v"
      if specs = "iec" then rr "<td class=list_item valign=top><nobr>" & getval(thedata1, "suppliercode")
']
      theaddress = theaddress : if isnull(theaddress) then theaddress = ""
      theaddress = replace(theaddress,"<br>"," ") : theaddress = replace(theaddress,"<","[") : theaddress = replace(theaddress,">","]") : if len(theaddress) > 30 then theaddress = left(theaddress,30) & "..."
      if oksee("address") then rr("<td class=list_item valign=top align=><nobr>" & theaddress)
      thephone1 = thephone1 : if isnull(thephone1) then thephone1 = ""
      thephone1 = replace(thephone1,"<br>"," ") : thephone1 = replace(thephone1,"<","[") : thephone1 = replace(thephone1,">","]") : if len(thephone1) > 30 then thephone1 = left(thephone1,30) & "..."
      if oksee("phone1") then rr("<td class=list_item valign=top align=><nobr>" & thephone1)
      thefax = thefax : if isnull(thefax) then thefax = ""
      thefax = replace(thefax,"<br>"," ") : thefax = replace(thefax,"<","[") : thefax = replace(thefax,">","]") : if len(thefax) > 30 then thefax = left(thefax,30) & "..."
'[      if oksee("fax") then rr("<td class=list_item valign=top align=><nobr>" & thefax)
      if oksee("fax") then rr("<td class=list_item valign=top align=><nobr>" & thefax)
      if hasmatch(specs, "jerusalem,muni,brener,") and oksee("additionals_���") then rr("<td class=list_item><nobr>" & getval(thedata1, "fax2"))'fax2
']
      t = theemail : if isemail(t) then t = "<a href=mailto:" & t & ">" & t & "</a>"
      if oksee("email") then rr("<td class=list_item valign=top align=><nobr>" & t)
      if oksee("contac1tname") then rr("<td class=list_item valign=top align=><nobr>" & thecontac1tname)
      if oksee("payment") then rr("<td class=list_item valign=top align=><nobr>" & thepayment)
      if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_item valign=top align=><nobr>" & thesiteidlookup)
      if oksee("priority") and session(tablename & "containerfield") <> "priority" then rr("<td class=list_item valign=top onmousedown='vbscript:c" & theid & ".checked = true'><nobr>")
      if not oksee("priority") then
      elseif session(tablename & "containerfield") = "priority" then
        rr("<input type=hidden name=listupdate_priority_" & theid & " value=" & thepriority & ">")
      elseif okwritehere and session(tablename & "containerfield") <> "priority" then
        rr("<input type=text maxlength=10 onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' style='width:70; direction:ltr; text-align:left;' name=listupdate_priority_" & theid & " value=""" & thepriority & """>")
      else
        rr(thepriority)
      end if
'[      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
'[      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
'[      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
      if hasmatch(specs, "jerusalem,muni,brener,") then
        if oksee("additionals_���") then rr "<td class=list_item valign=top><nobr>" & getval(thedata1, "rakaz")
        if oksee("additionals_���_���_1") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact1"))
        if oksee("additionals_�����_1") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact1p"))
        if oksee("additionals_�����_1") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact1e"))
        if oksee("additionals_���_���_2") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact2"))
        if oksee("additionals_�����_2") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact2p"))
        if oksee("additionals_����_2") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact2e"))
        if oksee("additionals_���_���_3") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact3"))
        if oksee("additionals_�����_3") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact3p"))
        if oksee("additionals_����_3") then rr("<td class=list_item><nobr>" & getval(thedata1, "contact3e"))
      end if

      if hasmatch(specs, "jerusalem,") then
        if oksee("additionals_����_����") then rr("<td class=list_item><nobr>" & getval(thedata1, "���� ����"))
      end if
      thedata1 = thedata1 : if isnull(thedata1) then thedata1 = ""
      thedata1 = replace(thedata1,"<br>"," ") : thedata1 = replace(thedata1,"<","[") : thedata1 = replace(thedata1,">","]") : if len(thedata1) > 30 then thedata1 = left(thedata1,30) & "..."
      if oksee("data1") then rr("<td class=list_item valign=top align=><nobr>" & thedata1)
']
    end if
    if rc > (session(tablename & "_page") + 1) * session(tablename & "_lines") then exit do
    rs.movenext
  loop

  '-------------bottom-----------------------
  x = rrpagesbar(session(tablename & "_lines"),session(tablename & "_page"), rsrc)
  '-------------fieldsumsrow--------------------------
  rr("<tr id=tr0_totals>")
  rr("<td class=list_total1><!total>")
  if oksee("address") and session(tablename & "containerfield") <> "address" then rr("<td class=list_total1>") 'address
  if oksee("phone1") and session(tablename & "containerfield") <> "phone1" then rr("<td class=list_total1>") 'phone1
  if oksee("fax") and session(tablename & "containerfield") <> "fax" then rr("<td class=list_total1>") 'fax
  if oksee("email") and session(tablename & "containerfield") <> "email" then rr("<td class=list_total1>") 'email
  if oksee("contac1tname") and session(tablename & "containerfield") <> "contac1tname" then rr("<td class=list_total1>") 'contac1tname
  if oksee("payment") and session(tablename & "containerfield") <> "payment" then rr("<td class=list_total1>") 'payment
  if oksee("siteid") and session(tablename & "containerfield") <> "siteid" then rr("<td class=list_total1>") 'siteid
  if oksee("priority") and session(tablename & "containerfield") <> "priority" then rr("<td class=list_total1>") 'priority
  if oksee("data1") and session(tablename & "containerfield") <> "data1" then rr("<td class=list_total1>") 'data1

  rr("<" & "script language=vbscript>sub checkall(v)" & vbcrlf & checkall & "end sub</" & "script>")
  rr("<tr id=tr0_list_multi><td colspan=99 class=list_multi>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allpagecheck onclick='vbscript:checkall(me.checked)'>~All~")
'[  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck>")
  if ulev > 1 and okedit("list_checkboxes") then rr(" <input type=checkbox name=allidscheck " & allidscheckscript & ">")
']

  rr("~All Results~ (" & rsrc & ")")
  if ulev > 1 then

    rr(" <input type=button class=groovybutton id=buttonmultiupdate value='~Update~' onclick='vbscript:if msgbox(""~Update Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multiupdate"" : flist.action2.value = """" : flist.submit'>")
  end if
  if ulev > 1 then rr(" <input type=button class=groovybutton id=buttonmultidelete onclick='vbscript:if msgbox(""~Delete Checked~?"",vbyesno) = 6 then me.disabled = true : flist.action.value = ""multidelete"" : flist.action2.value = """" : flist.submit' value='~Delete Checked~'>")

  rr(" <input type=button class=groovybutton id=buttoncreatecsv onclick='vbscript:if msgbox(""~Create CSV~ ?"",vbyesno) = 6 then flist.target = ""_new"" : flist.action.value = ""csv"" : flist.submit : flist.target = """" ' value='~Create CSV~'>")
  if ulev > 1 and okedit("buttonloadcsv") then
    rr(" <span style='position:relative; height:30; top:5;'><iframe name=r1 frameborder=0 marginwidth=0 marginheight=0 width=70 height=32 scrolling=no src=upload.asp?box=flist.loadcsv></iframe></span>")
    rr("<input type=text maxlength=200 id=loadcsv name=loadcsv style='width:102; height:30;' onkeypress='vbscript:if window.event.keycode = 13 then window.event.keycode = 0' dir=ltr>&nbsp;")
    rr("<input type=button class=groovybutton id=buttonloadcsv onclick='vbscript: if right(lcase(flist.loadcsv.value),4) <> "".csv"" then msgbox ""~Must first upload CSV file~"" else if msgbox(""~Load CSV~ ?"",vbyesno) = 6 then flist.action.value = ""loadcsv"" : flist.submit' value='~Load CSV~'>")
  end if
  rr("</tr></form>" & bottombar & "</table>")'list
  x = disablecontrolsnow("",disablecontrols,hidecontrols)
end if

if childpage then rr("<button id=strechmyiframe style='width:0; height:0;' onclick='vbscript: parent.document.getelementbyid(""fr" & tablename & """).style.height = document.body.scrollheight + 20 : parent.document.getelementbyid(""fr" & tablename & """).style.width = document.body.scrollwidth : on error resume next : parent.document.getelementbyid(""strechmyiframe"").click '><" & "script language=vbscript> document.getelementbyid(""strechmyiframe"").click </" & "script>")
rrbottom()
rend()
%>
