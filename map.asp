<!--#include file="include.asp"-->
<%
embed = "" : if hasmatch(specs, "jerusalem,") then embed = getfield("embed")
draggable = "draggable: true," : if embed = "on" then draggable = ""
admin_login = true
legend = ""

rr "<html>"
rr "<style>.infomsg {display:none;}</style>"
rr "<meta http-equiv=""X-UA-Compatible"" content=""IE=11"" />"
rr "<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1255"">"
rr "<meta http-equiv=""Content-Language"" content=""he"">"
rr "<body rightmargin=0 leftmargin=0 bottommargin=0 topmargin=0 scroll=no>"
%>
<!--#include file="top.asp"-->
<%
'==================basic map with marker============================
'<body rightmargin=0 leftmargin=0 bottommargin=0 topmargin=0 scroll=no>
'<div id=map_canvas style='width:100%; height:100%;'></div><br>
'<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&language=iw'></script>
'<script type='text/javascript'>
'  var directionsService = new google.maps.DirectionsService;
'  var mapOptions = {zoom: 8, center: new google.maps.LatLng(32.074284, 34.810181), mapTypeId: google.maps.MapTypeId.ROADMAP};
'  var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
'  var geocoder = new google.maps.Geocoder();
'  geocoder.geocode({address: '�����'}, function(results, status){
'    if (status == google.maps.GeocoderStatus.OK){var marker = new google.maps.Marker({position: results[0].geometry.location, map: map, title:'���� �����'});}});
'</script>
'===================================================================
if getfield("action") = "savegeo" and getfield("loc") <> "" and session("usergroup") = "admin" then
  session("stationcoursecache") = ""
  a = getfield("loc")
  do until a = ""
    b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1) : i = mid(b,2)
    lat = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    lon = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
    if left(b,1) = "s" then
      title = getxyaddress(lat,lon)
      if not okwrite("stations", i) then du = errorend("_back�� ���� ����� ����, ��� �����")
      s = "update stations set markery = " & lat & ", markerx = " & lon
      if specs = "jerusalem" and title <> "" then s = s & ", title = '" & chtm(title) & "'" : updatedtitle = true
      s = s & " where id = " & i
      set rs = conn.execute(sqlfilter(s))
      if specs = "jerusalem" and title = "" then rr "<script>alert('�� ���� �� ���� ������ ������, ��� ���� ��� ����� ������ �����');</script>"
    elseif left(b,1) = "w" then
      d = getonefield("select data1 from workers where id = " & i)
      d = setval(d, "y", lat) : d = setval(d, "x", lon)
      title = getxyaddress(lat,lon)
      s = "update workers set data1 = '" & d & "'"
      if specs = "jerusalem" and title <> "" then s = s & ", address = '" & chtm(title) & "'" : updatedtitle = true
      s = s & " where id = " & i
      set rs = conn.execute(sqlfilter(s))
      if specs = "jerusalem" and title = "" then rr "<script>alert('�� ���� �� ���� ������ ������, ��� ���� ��� ����� ���� ����� ������ ������');</script>"
    end if
  loop
  'if getfield("scriptname") = "admin_workers.asp" and updatedtitle then rr "<script>parent.location.reload(false);</script>" : rend
end if

'===============================
if getfield("clear") = "on" then
  session("map_orderids") = ""
  session("map_courseids") = ""
  session("map_stationids") = ""
  session("map_workerids") = ""
end if
okwritehere = true
if getfield("workerids") <> "" then session("map_workerids") = getfield("workerids")
if getfield("workerids") = "-" then session("map_workerids") = ""
if getfield("stationids") <> "" then session("map_stationids") = getfield("stationids")
if getfield("stationids") = "-" then session("map_stationids") = ""
if (getfield("courseids") <> "" and getfield("courseids") <> "-") or getfield("courseids_select") <> "" then
  session("map_orderids") = ""
  session("map_courseids") = getfield("courseids")
  session("map_stationids") = uniquelist(session("map_stationids") & getidlist("select stationid from coursestations where courseid in(" & getfield("courseids") & "-1)"))
  if countstring(session("map_courseids"), ",") > 10 then session("map_courseids") = "" : session("map_stationids") = "" : du = errorend("Please choose up to 10 courses")
end if
if getfield("orderids") <> "" then session("map_orderids") = getfield("orderids")
if getfield("orderids") = "-" then session("map_orderids") = ""
if getfield("orderids") <> "" and getfield("orderids") <> "-" then
  session("map_courseids") = ""
  if countstring(session("map_orderids"), ",") > 10 then session("map_orderids") = "" : session("map_stationids") = "" : du = errorend("Please choose up to 10 orders")
  ii = ""
  s = "select history from orders where id in(" & session("map_orderids") & "0)"
  set rs = conn.execute(sqlfilter(s))
  do until rs.eof
    ii = ii & getval(rs("history"), "stationcourse")
    rs.movenext
  loop
  session("map_stationids") = uniquelist(ii)
end if

if getfield("action2") = "withstations" then session("map_stationids") = uniquelist(getidlist("select from1 from workers where id in(" & session("map_workerids") & "-1)"))
if getfield("courseids") = "-" then session("map_courseids") = ""
thecourseids = session("map_courseids")
theworkerids = session("map_workerids")
thestationids = session("map_stationids")
theorderids = session("map_orderids")
dim colors(10) : dim colors2(10) : colorindex = 0 : colorindexmax = 10
colors(1) =  "FF7777" : colors(2) =  "7777FF" : colors(3) =  "aaFFaa" : colors(4) =  "FF77FF" : colors(5) =  "FFDD00" : colors(6) =  "CC7744" : colors(7) =  "CCCCCC" : colors(8) =  "008000" : colors(9) =  "800000" : colors(10) =  "FF9B21"
colors2(1) = "FF0000" : colors2(2) = "0000FF" : colors2(3) = "66dd66" : colors2(4) = "FF00FF" : colors2(5) = "EEBB00" : colors2(6) = "CC4444" : colors2(7) = "777777" : colors2(8) = "008000" : colors2(9) = "800000" : colors(10) =  "FFC98C"
t = 96 : if embed = "on" then t = 100
rr "<div id=map_canvas style='width:100%; height:" & t & "%; z-index:500;'></div>" & vbcrlf
rr "<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&language=" & iif(session("elang") = "2", "iw", "en") & "&v=3&key=" & googlemapskey & "'></script>" & vbcrlf
rr "<script type='text/javascript'>" & vbcrlf

'----------------------
rr "window.onload = function() {" & vbcrlf
rr "  var directionsService = new google.maps.DirectionsService;" & vbcrlf
if session("mapdefaultlocation") = "" then
  '13|jerusalem
  t = getparam("MapDefaultLocation") : if instr(t,"|") = 0 then t = "|" & t
  z = left(t,instr(t,"|")-1) : t = mid(t,instr(t,"|")+1) : if not isnumeric(z) then z = 8
  if t <> strfilter(t, " 0123456789.,-") then t = getaddresslatlon(t)
  if t <> strfilter(t, " 0123456789.,-") or strfilter(t,"0123456789.,") = "" then t = "32.074284,34.810181"
  session("mapdefaultlocation") = t
  session("mapdefaultzoom") = z
end if

t = session("mapdefaultlocation")
z = session("mapdefaultzoom")
if countlist(theworkerids) = 1 then
  d = getonefield("select data1 from workers where id = " & getlistitem(theworkerids,1))
  x = getval(d, "x") : y = getval(d, "y") : if x <> "" and y <> "" then t = y & "," & x : z = 15
end if
z = 13
rr "  var mapOptions = {zoom: " & z & ", center: new google.maps.LatLng(" & t & "), mapTypeId: google.maps.MapTypeId.ROADMAP, streetViewControl: false"
if embed = "on" then rr ",disableDoubleClickZoom: true, draggable: false, scrollwheel: false, panControl: true, mapTypeControl: false, zoomControl: false"
rr "};" & vbcrlf
rr "  var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);" & vbcrlf

'------courses
a = thecourseids : colorindex = 0
do until a = ""
  b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  colorindex = colorindex + 1 : if colorindex > colorindexmax then colorindex = 1
  legend = legend & "<span style='color:#" & colors(colorindex) & "; font-weight:bold;'>" & getcode("courses", b) & "</span> "

  t = ""

  if hasmatch(specs, "jerusalem,muni,brener,") then
    coursedata1 = getonefield("select data1 from courses where id = 0" & b)
    if getval(coursedata1, "ways") = "B" then t = " desc"
  end if

  s = "select coursestations.stationid, stations.title, stations.inmap, stations.markerx, stations.markery from coursestations left join stations on coursestations.stationid = stations.id where courseid = " & b & " order by minutes" & t
  closers(rs) : rs.open sqlfilter(s), conn, 3, 1, 1 : ccc = rs.recordcount
  c = 0 : w = "" : p = "" : mm = "" : cc = 0
  do until rs.eof
    c = c + 1 : cc = cc + 1
    x = rs("markerx") : if isnull(x) then x = ""
    y = rs("markery") : if isnull(y) then y = ""
    if x <> "" and y <> "" then
      z = "new google.maps.LatLng(" & y & "," & x & ")"
    else
      z = "'" & rs("inmap") & "'"
    end if
    t = rs("title") : t = replace(t,"\", "") : t = tocsv(t)
    mm = mm & "var marker" & rs("stationid") & " = new google.maps.Marker({position: " & z & ", map: map, " & draggable & " title:'" & t & "', icon: ""http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" & cc & "|" & colors(colorindex) & "|000000"" });" & vbcrlf
    if draggable <> "" then
  	  mm = mm & "google.maps.event.addListener(marker" & rs("stationid") & ", ""dragend"", function(event) {" & vbcrlf
  	  mm = mm & " f2.loc.value += 's" & rs("stationid") & ",' + event.latLng.lat() + ',' + event.latLng.lng() + ',';" & vbcrlf
  	  mm = mm & "});" & vbcrlf
    end if

    if hasmatch(specs, "jerusalem,muni,brener,") then
      t = rs("title") : t = replace(t,"\", "") : t = tocsv(t)
      info = "<b><u>" & replace(t, ";", ",") & "</b></u><br><br><table cellpadding=2 style=""font-size:12;"">"
      ii = getidlist("select workerid from weekplan where courseid = " & b)
      sq = "select firstname,lastname,address,phone1,data1,from1 from workers where id in(" & ii & "-1)"
      sq = sq & " and workertypeid <> " & escortid
      sq = sq & " and (from1 = 0" & rs("stationid") & " or to1 = 0" & rs("stationid")
      if cc = 1 then sq = sq & " or from1 not in(" & getidlist("select stationid from coursestations where courseid = " & b) & "-1)"
      sq = sq & ")"
      sq = sq & " order by firstname, lastname"
      set rs1 = conn.execute(sqlfilter(sq))
      j = 0
      do until rs1.eof
        workerdata1 = rs1("data1")
        j = j + 1 : if j >= 50 then info = info & "<tr><td colspan=9><b>.....</b>" : exit do
        info = info & "<tr valign=top><td><img src=worker0.png>"
        info = info & "<td><nobr>" & tocsv(rs1("firstname") & " " & rs1("lastname"))
        if rs1("from1") & "" <> rs("stationid") & "" then info = info & " <b>(��� ����)</b>"
        info = info & "<td><nobr>" & tocsv(rs1("address"))
        info = info & "<td><nobr>" & tocsv(rs1("phone1"))
        info = info & "<td><nobr>(" & tocsv(getval(workerdata1, "��� ��� 1")) & ")"
        info = info & "<td><nobr>" & tocsv(getval(workerdata1, "�� ��� ��� 1"))
        info = info & "<td><nobr>" & tocsv(getval(workerdata1, "����� ��� ��� 1"))
        rs1.movenext
      loop
      info = info & "</table>"
      mm = mm & "var contentString = '<div style=""float:right; width:600; direction:rtl; text-align:right;"">" & info & "</div>';"
      mm = mm & "var infowindow" & rs("stationid") & " = new google.maps.InfoWindow({content: contentString});"
      mm = mm & "marker" & rs("stationid") & ".opacity = 1;"
      mm = mm & "marker" & rs("stationid") & ".addListener('click', function() {if(marker" & rs("stationid") & ".opacity == 1) {infowindow" & rs("stationid") & ".open(map, marker" & rs("stationid") & "); marker" & rs("stationid") & ".opacity = 0.9;} else {infowindow" & rs("stationid") & ".close(); marker" & rs("stationid") & ".opacity = 1;};});"
    end if

    plottedstations = plottedstations & rs("stationid") & ","
    rs.movenext

    last = false : if rs.eof or c = 9 then last = true
    if c = 1 then
      p = p & "origin: " & z & ","
    elseif last then
      p = p & "destination: " & z & ","
    else
      if w = "" then w = "1" : p = p & "waypoints: ["
      t = "true" : if hasmatch(specs,"routes,iai,adama,bazan,flextronics,teva,vishay,iwi,hatama,") or instr(siteurl, "/gushetzion.moovex.net/") > 0 then t = "false" 'u-turn off...
      p = p & "{location: " & z & ", stopover:" & t & "},"
    end if

    if rs.eof or last then
      if c > 2 then p = replace(p, ",destination:", "],destination:")
      rr "directionsService.route({" & vbcrlf
      rr p
      rr "optimizeWaypoints: true," & vbcrlf
      rr "travelMode: google.maps.DirectionsTravelMode.DRIVING" & vbcrlf
      rr "}, function(result) {" & vbcrlf
      rr "var directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true, polylineOptions: {strokeColor: '#" & colors2(colorindex) & "', strokeOpacity: 0.5, strokeWeight: 5}});" & vbcrlf
      rr "directionsRenderer.setMap(map);" & vbcrlf
      rr "directionsRenderer.setDirections(result);" & vbcrlf
      rr "});" & vbcrlf
      'if c = 9 and not rs.eof then rs.moveprevious : rs.moveprevious : cc = cc - 2
      c = 0 : p = "" : w = ""
    end if
  loop
  rr mm
loop

'------orders
a = theorderids : colorindex = 0
do until a = ""
  b = left(a,instr(a,",")-1) : a = mid(a,instr(a,",")+1)
  colorindex = colorindex + 1 : if colorindex > colorindexmax then colorindex = 1
  legend = legend & "<span style='color:#" & colors(colorindex) & "; font-weight:bold;'>" & b & "</span> "

  pids = "" : altstations = "" : history = ""
  s = "select pids,altstations,history from orders where id = 0" & b
  set rs = conn.execute(sqlfilter(s))
  if not rs.eof then pids = rs("pids") & "" : altstations = rs("altstations") & "" : history = rs("history") & ""

  kk = getval(history, "stationcourse") ': ccc = countstring(kk, ",")
  orig = kk
  c = 0 : cc = 0 : w = "" : p = "" : mm = ""
  do until kk = ""
    k = left(kk,instr(kk,",")-1) : kk = mid(kk,instr(kk,",")+1)
    c = c + 1 : cc = cc + 1
    set rs = conn.execute("select markerx, markery, inmap, title from stations where id = 0" & k)
    if not rs.eof then
      x = rs("markerx") : if isnull(x) then x = ""
      y = rs("markery") : if isnull(y) then y = ""
      if x <> "" and y <> "" then
        z = "new google.maps.LatLng(" & y & "," & x & ")"
      else
        z = "'" & rs("inmap") & "'"
      end if

      title = rs("title") : title = replace(title,"\", "") : title = tocsv(title)
      yy = pids
      do until yy = ""
        y = left(yy,instr(yy,",")-1) : yy = mid(yy,instr(yy,",")+1)
        if getval(altstations, y & "from1") = k then
          t = replace(tocsv(getworkername(y)), "\", "")
          title = title & "\n" & t
        end if
      loop

      mm = mm & "var marker" & k & " = new google.maps.Marker({position: " & z & ", map: map, " & draggable & " title:'" & title & "', icon: ""http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" & cc & "|" & colors(colorindex) & "|000000"" });" & vbcrlf
      if draggable <> "" then
        mm = mm & "google.maps.event.addListener(marker" & k & ", ""dragend"", function(event) {" & vbcrlf
        mm = mm & " f2.loc.value += 's" & k & ",' + event.latLng.lat() + ',' + event.latLng.lng() + ',';" & vbcrlf
        mm = mm & "});" & vbcrlf
      end if

      plottedstations = plottedstations & k & ","

      last = false : if kk = "" or c = 9 then last = true
      if c = 1 then
        p = p & "origin: " & z & ","
      elseif last then
        p = p & "destination: " & z & ","
      else
        if w = "" then w = "1" : p = p & "waypoints: ["
        t = "true" : if hasmatch(specs,"routes,iai,adama,bazan,flextronics,teva,vishay,iwi,hatama,") or instr(siteurl, "/gushetzion.moovex.net/") > 0 then t = "false" 'u-turn off...
        p = p & "{location: " & z & ", stopover:" & t & "},"
      end if
      if kk = "" or last then
        if c > 2 then p = replace(p, ",destination:", "],destination:")
        rr "directionsService.route({" & vbcrlf
        rr p
        rr "optimizeWaypoints: true," & vbcrlf
        if avoidtolls then rr "avoidTolls: true," & vbcrlf
        rr "travelMode: google.maps.DirectionsTravelMode.DRIVING" & vbcrlf
        rr "}, function(result) {" & vbcrlf
        rr "var directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true, polylineOptions: {strokeColor: '#" & colors2(colorindex) & "', strokeOpacity: 0.5, strokeWeight: 5}});" & vbcrlf
        rr "directionsRenderer.setMap(map);" & vbcrlf
        rr "directionsRenderer.setDirections(result);" & vbcrlf
        rr "});" & vbcrlf
        'if c = 9 and kk <> "" then kk = getlistitem(orig, cc - 2) & "," & getlistitem(orig, cc - 1) & "," & kk : cc = cc - 2
        c = 0 : p = "" : w = ""
      end if
    end if
  loop
  rr mm
loop

'---stations
t = nomatch(thestationids, plottedstations)
s = "select id,title,markerx,markery from stations where id in(" & t & "-1) and markerx <> '' and markery <> ''"
set rs = conn.execute(sqlfilter(s))
do until rs.eof
  n = rs("title")
  x = rs("markerx") : y = rs("markery")
  'rr "var marker = new google.maps.Marker({position: new google.maps.LatLng(" & y & "," & x & "), map: map, title:'" & tocsv(n) & "', icon: ""http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=S|8080ff|000000"" });" & vbcrlf
  rr "var marker" & rs("id") & " = new google.maps.Marker({position: new google.maps.LatLng(" & y & "," & x & "), map: map, " & draggable & " title:'" & tocsv(n) & "', icon: ""stationmarker.png"" });" & vbcrlf
  if draggable <> "" then
  	rr "google.maps.event.addListener(marker" & rs("id") & ", ""dragend"", function(event) {" & vbcrlf
  	rr " f2.loc.value += 's" & rs("id") & ",' + event.latLng.lat() + ',' + event.latLng.lng() + ',';" & vbcrlf
    if getfield("editstation") = "on" then
      rr " parent.document.f1.markery.value = event.latLng.lat();" & vbcrlf
      rr " parent.document.f1.markerx.value = event.latLng.lng();" & vbcrlf
    end if
	  rr "});" & vbcrlf
  end if
  if countstring(thestationids, ",") > 1 then
	  rr "marker" & rs("id") & ".addListener('click', function() {" & vbcrlf
	  rr " if(marker" & rs("id") & ".icon == 'stationmarker.png') {;" & vbcrlf
	  rr " marker" & rs("id") & ".setIcon('stationmarker2.png');" & vbcrlf
	  rr " f2.sss.value += '" & rs("id") & ",';" & vbcrlf 
	  rr " } else {" & vbcrlf
	  rr " marker" & rs("id") & ".setIcon('stationmarker.png');" & vbcrlf
	  rr " var a = f2.sss.value;" & vbcrlf
	  rr " a = ',' + a;" & vbcrlf
	  rr " a = a.replace('" & rs("id") & ",', '');" & vbcrlf
	  rr " a = a.substring(1);" & vbcrlf
	  rr " f2.sss.value = a;" & vbcrlf
	  rr " }" & vbcrlf
	  rr "});" & vbcrlf
  end if
  rs.movenext
loop

'-------------workers
's = "select workers.firstname, workers.lastname, workers.from1, stations.markerx, stations.markery from workers left join stations on workers.from1 = stations.id where workers.id in(" & theworkerids & "-1)"
'set rs = conn.execute(sqlfilter(s))
'do until rs.eof
'  markerx = rs("markerx") : if isnull(markerx) then markerx = ""
'  markery = rs("markery") : if isnull(markery) then markery = ""
'  n = rs("firstname") & " " & rs("lastname")
'  if cstr(markerx) <> "" and cstr(markery) <> "" then
'    rr "var marker = new google.maps.Marker({position: new google.maps.LatLng(" & markery & "," & markerx & "), map: map, title:'" & n & "'});" & vbcrlf
'  end if
'  rs.movenext
'loop

s = "select id,firstname, lastname, address, data1 from workers where id in(" & theworkerids & "-1)"
set rs = conn.execute(sqlfilter(s))
do until rs.eof
  data1 = rs("data1") & ""
  a = "" & rs("address") : a = replace(a, """", "")
  n = rs("firstname") & " " & rs("lastname") & " - " & a : n = replace(n, """", "")
  t = getval(data1, "walk") : if isnumeric(t) then n = n & " (" & t & "m)"
  'rr "var geocoder = new google.maps.Geocoder();" & vbcrlf
  'rr "geocoder.geocode({address: '" & a & "'}, function(results, status){" & vbcrlf
  'rr "if (status == google.maps.GeocoderStatus.OK){var marker = new google.maps.Marker({position: results[0].geometry.location, map: map, title:'" & n & "'});}});" & vbcrlf
  d = ctxt("" & data1) : x = getval(d,"x") : y = getval(d,"y")
  if x <> "" and y <> "" then
    rr "var markerw" & rs("id") & " = new google.maps.Marker({position: new google.maps.LatLng(" & y & "," & x & "), map: map, " & draggable & " title:'" & n & "'});" & vbcrlf
    if draggable <> "" then
  		rr "google.maps.event.addListener(markerw" & rs("id") & ", ""dragend"", function(event) {" & vbcrlf
  		rr " f2.loc.value += 'w" & rs("id") & ",' + event.latLng.lat() + ',' + event.latLng.lng() + ',';" & vbcrlf
		  rr "});" & vbcrlf
    end if
  end if
  rs.movenext
loop
rr "}"
rr "</script>"

rr legend
rr "<form name=f2 action=? method=post style='display:inline;'>"
rr "<input type=hidden name=action value=savegeo>"
rr "<input type=hidden id=loc name=loc>"
rr "<input type=hidden id=sss name=sss>"
rr "<input type=hidden name=scriptname value='" & getfield("scriptname") & "'>"
rr "<div style='text-align:center;'>"
if session("usergroup") = "admin" and draggable <> "" then rr "<a href=#aa style='padding:20; white-space:nowrap; font-family:arial; color:#000000;' onclick='f2.submit();'>~Save~ ~Geo Location~</a>"
'if getfield("scriptname") <> "admin_workers.asp" then
  rr "<a style='padding:20; white-space:nowrap; font-family:arial; color:#000000; cursor:hand; text-decoration:underline;' target=_top onclick='var a = (f2.sss.value == """" ? """ & session("map_stationids") & """ : f2.sss.value); document.location = ""admin_stations.asp?action=search&action2=clear&showids="" + a'>~Station management~</a>"
  rr "<a style='padding:20; white-space:nowrap; font-family:arial; color:#000000;' href=map.asp?courseids=-&workerids=-&stationids=-&orderids=-><b>~Clear~ ~Map~</b></a>"
  rr "<a style='padding:20; white-space:nowrap; font-family:arial; color:#000000;' href=map.asp?courseids=->~Clear~ ~Courses~</a>"
  rr "<a style='padding:20; white-space:nowrap; font-family:arial; color:#000000;' href=map.asp?workerids=->~Clear~ ~Workers~</a>"
  rr "<a style='padding:20; white-space:nowrap; font-family:arial; color:#000000;' href=map.asp?stationids=->~Clear~ ~Stations~</a>"
  rr "<a style='padding:20; white-space:nowrap; font-family:arial; color:#000000;' href=map.asp?orderids=->~Clear~ ~Orders~</a>"
  
'end if
rr "</div></form>"

%>
